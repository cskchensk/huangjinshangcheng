package cn.ug.login.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.feign.ChannelService;
import cn.ug.feign.MemberUserService;
import cn.ug.feign.SmsService;
import cn.ug.login.bean.type.VerificationCodeType;
import cn.ug.login.service.VerificationCodeService;
import cn.ug.msg.bean.SmsBean;
import cn.ug.msg.bean.type.SmsType;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static cn.ug.login.bean.type.VerificationCodeType.*;

/**
 * 验证码服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("verificationCode")
public class VerificationCodeController extends BaseController {

    @Resource
    private VerificationCodeService verificationCodeService;

    @Resource
    private MemberUserService memberUserService;

    @Resource
    private SmsService smsService;

    @Resource
    private ChannelService channelService;

    @Value("${spring.cloud.config.profile}")
    private String environMent;

    /**
     * 获取验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员 4:渠道成员
     * @param loginSource	登录来源 1：PC 2：Android App 3：IOS App
     * @param type	        类型 1：注册验证码 2：修改密码验证码 3：忘记密码验证码 4：验证身份验证码
     * @param loginName	    用户名
     * @return			    是否登录成功
     */
    @RequestMapping("create")
    public SerializeObject create(Integer loginType, Integer loginSource, Integer type, String loginName) {
        /*if (!environMent.equalsIgnoreCase("beta")){
            return new SerializeObject(ResultType.NORMAL);
        }*/

        switch (VerificationCodeType.valueOf(type)) {
            case REGISTER: {
                return register(loginSource, loginName);
            }
            case UPDATE_PASSWORD: {
                return updatePassword(loginType, loginSource, loginName);
            }
            case FORGET_PASSWORD: {
                if (loginType != 4)
                   return forgetPassword(loginType, loginSource, loginName);

                return channelForgetPassword(loginType, loginSource, loginName);
            }
            case SHORTCUT_LOGIN: {
                return shortcutLogin(loginType, loginSource, loginName);
            }
            case VERIFY_IDENTIDY: {
                return verifyIdentidy(loginType, loginSource, loginName);
            }
            case ADMIN_LOGIN: {
                return adminLogin(loginType, loginSource, loginName);
            }
            default:
                break;
        }
        return new SerializeObjectError("00000002");
    }

    /**
     * 验证验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员
     * @param loginSource	登录来源 1：PC 2：Android App 3：IOS App
     * @param type	        类型 1：注册验证码 2：修改密码验证码 3：忘记密码验证码 4：验证身份验证码
     * @param loginName	    用户名
     * @param code	        验证码
     * @return			    是否登录成功
     */
    @RequestMapping("check")
    public SerializeObject check(Integer loginType, Integer loginSource, Integer type, String loginName, String code) {
        if(StringUtils.isBlank(loginName) || StringUtils.isBlank(code)) {
            return new SerializeObjectError("13000102");
        }
        if(verificationCodeService.check(VerificationCodeType.valueOf(type), loginName, code)) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError("13000102");
    }

    /**
     * 注册验证码
     * @param loginSource	注册来源 1：PC 2：Android App 3：IOS App
     * @param loginName	    用户名
     * @return			    是否登录成功
     */
    private SerializeObject register(Integer loginSource, String loginName) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }

        // 判断用户是否已经存在
        SerializeObject obj = memberUserService.findByLoginName(loginName);
        if(null != obj && obj.getCode() == ResultType.NORMAL && null != obj.getData()) {
            return new SerializeObjectError("13000100");
        }

        String code = verificationCodeService.put(REGISTER, loginName);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.REGISTER_VERIFICATION_CODE, loginName);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        obj = smsService.send(smsBean);
        return obj;
    }

    /**
     * 修改密码验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员
     * @param loginSource	操作来源 1：PC 2：Android App 3：IOS App
     * @param loginName	    用户ID
     * @return			    是否登录成功
     */
    private SerializeObject updatePassword(Integer loginType, Integer loginSource, String loginName) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }

        // 判断用户是否已经存在
        SerializeObject obj = memberUserService.find(null, loginName);
        if(null == obj || obj.getCode() != ResultType.NORMAL || null == obj.getData()) {
            return new SerializeObjectError("13000103");
        }

        String code = verificationCodeService.put(UPDATE_PASSWORD, loginName);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.UPDATE_PASSWORD_VERIFICATION_CODE, loginName);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        obj = smsService.send(smsBean);
        return obj;
    }

    /**
     * 忘记密码验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员
     * @param loginSource	操作来源 1：PC 2：Android App 3：IOS App
     * @param loginName	    用户名
     * @return			    是否登录成功
     */
    private SerializeObject forgetPassword(Integer loginType, Integer loginSource, String loginName) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }

        // 如果用户不存在
        SerializeObject obj = memberUserService.findByLoginName(loginName);
        if(null == obj || obj.getCode() != ResultType.NORMAL || null == obj.getData()) {
            return new SerializeObjectError("13000103");
        }

        String code = verificationCodeService.put(FORGET_PASSWORD, loginName);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.FORGET_PASSWORD_VERIFICATION_CODE, loginName);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        obj = smsService.send(smsBean);
        return obj;
    }

    /**
     * 忘记密码验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员 4：渠道成员
     * @param loginSource	操作来源 1：PC 2：Android App 3：IOS App
     * @param mobile	    用户名
     * @return			    是否登录成功
     */
    private SerializeObject channelForgetPassword(Integer loginType, Integer loginSource, String mobile) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(mobile)) {
            return new SerializeObjectError("00000002");
        }

        // 如果用户不存在
        SerializeObject obj = channelService.findChannelStaffByMobile(mobile);
        if(null == obj || obj.getCode() != ResultType.NORMAL || null == obj.getData()) {
            return obj;
        }

        String code = verificationCodeService.put(FORGET_PASSWORD, mobile);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.FORGET_PASSWORD_VERIFICATION_CODE, mobile);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        obj = smsService.send(smsBean);
        return obj;
    }

    private SerializeObject shortcutLogin(Integer loginType, Integer loginSource, String loginName) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }

        String code = verificationCodeService.put(SHORTCUT_LOGIN, loginName);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.REGISTER_VERIFICATION_CODE, loginName);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        SerializeObject obj = smsService.send(smsBean);
        return obj;
    }

    /**
     * 验证身份验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员
     * @param loginSource	操作来源 1：PC 2：Android App 3：IOS App
     * @param loginName	    用户名
     * @return			    是否登录成功
     */
    private SerializeObject verifyIdentidy(Integer loginType, Integer loginSource, String loginName) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }

        // 如果用户不存在
        SerializeObject obj = memberUserService.findByLoginName(loginName);
        if(null == obj || obj.getCode() != ResultType.NORMAL || null == obj.getData()) {
            return new SerializeObjectError("13000103");
        }

        String code = verificationCodeService.put(VERIFY_IDENTIDY, loginName);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.FORGET_PASSWORD_VERIFICATION_CODE, loginName);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        obj = smsService.send(smsBean);
        return obj;
    }

    /**
     * 管理员登陆验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员
     * @param loginSource	操作来源 1：PC 2：Android App 3：IOS App
     * @param loginName	    用户名
     * @return			    是否登录成功
     */
    private SerializeObject adminLogin(Integer loginType, Integer loginSource, String loginName) {
        if(null == loginSource || loginSource <= 0 || StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }

        String code = verificationCodeService.put(ADMIN_LOGIN, loginName);
        // 发送短信
        SmsBean smsBean = new SmsBean(SmsType.FORGET_PASSWORD_VERIFICATION_CODE, loginName);
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("code", code);
        smsBean.setParamMap(paramMap);

        SerializeObject obj = smsService.send(smsBean);
        return obj;
    }

}
