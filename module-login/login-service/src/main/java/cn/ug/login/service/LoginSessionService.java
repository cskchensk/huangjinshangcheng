package cn.ug.login.service;


import cn.ug.analyse.bean.ChannelUserBean;
import cn.ug.bean.LoginBean;

public interface LoginSessionService {

    LoginBean put(String key, LoginBean value);

    LoginBean get(String key);

    LoginBean remove(String key);

    ChannelUserBean getchannelUserInfo(String key);
}
