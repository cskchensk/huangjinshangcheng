package cn.ug.login.service;


import cn.ug.login.bean.type.VerificationCodeType;

/**
 * 验证码缓存管理
 * @author 宋文涛
 */
public interface VerificationCodeService {

    /**
     * 将指定 key 映射到value
     * @param type		验证码类型
     * @param key		主键
     * @return			键值
     */
    String put(VerificationCodeType type, String key);

    /**
     * 获取缓存信息
     * @param key		主键
     * @return			键值
     */
    String get(VerificationCodeType type, String key);

    /**
     * 验证验证码
     * @param key		主键
     * @return			键值
     */
    boolean check(VerificationCodeType type, String key, String code);

}
