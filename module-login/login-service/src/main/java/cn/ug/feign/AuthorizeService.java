package cn.ug.feign;

import cn.ug.account.api.AuthorizeServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACCOUNT-SERVICE")
public interface AuthorizeService extends AuthorizeServiceApi {

}