package cn.ug.feign;

import cn.ug.member.api.MemberUserServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MEMBER-SERVICE")
public interface MemberUserService extends MemberUserServiceApi {

}