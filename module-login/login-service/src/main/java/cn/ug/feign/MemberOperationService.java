package cn.ug.feign;

import cn.ug.member.api.MemberOperationServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MEMBER-SERVICE")
public interface MemberOperationService extends MemberOperationServiceApi {
}
