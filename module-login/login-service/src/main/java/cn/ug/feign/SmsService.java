package cn.ug.feign;

import cn.ug.msg.api.SmsServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MSG-SERVICE")
public interface SmsService extends SmsServiceApi {

}