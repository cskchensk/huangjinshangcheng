package cn.ug.feign;

import cn.ug.member.api.MemberThirdServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MEMBER-SERVICE")
public interface MemberThirdService extends MemberThirdServiceApi{
}
