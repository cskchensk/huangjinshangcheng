package cn.ug.feign;

import cn.ug.analyse.api.ChannelServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="ANALYSE-SERVICE")
public interface ChannelService extends ChannelServiceApi {

}
