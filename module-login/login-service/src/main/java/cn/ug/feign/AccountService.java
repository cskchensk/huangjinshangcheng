package cn.ug.feign;

import cn.ug.account.api.AccountServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACCOUNT-SERVICE")
public interface AccountService extends AccountServiceApi {

}