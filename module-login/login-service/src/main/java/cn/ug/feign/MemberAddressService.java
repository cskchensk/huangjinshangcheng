package cn.ug.feign;

import cn.ug.member.api.MemberAddressServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MEMBER-SERVICE")
public interface MemberAddressService extends MemberAddressServiceApi {

}