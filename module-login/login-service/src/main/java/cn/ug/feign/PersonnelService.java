package cn.ug.feign;

import cn.ug.account.api.PersonnelServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACCOUNT-SERVICE")
public interface PersonnelService extends PersonnelServiceApi {

}