package cn.ug.login.bean.type;

import cn.ug.bean.type.BaseType;

/**
 * 在线类型
 * @author kaiwotech
 */
public class OnlineType extends BaseType {
	/** 在线 */
	public static final int ONLINE		= 1;
	/** 离开 */
	public static final int AWAY		= 2;
	/** 忙碌 */
	public static final int BUSY 		= 3;
	/** 请勿打扰 */
	public static final int DONOTDISTURB= 4;
	/** 隐身 */
	public static final int INVISIBLE	= 5;
	/** 离线 */
	public static final int OFFLINE		= 6;
}
