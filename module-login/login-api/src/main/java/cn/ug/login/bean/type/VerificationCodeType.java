package cn.ug.login.bean.type;

/**
 * 验证码类型
 */
public enum VerificationCodeType {

    /** 注册验证码 */
    REGISTER(1),
    /** 修改密码验证码 */
    UPDATE_PASSWORD(2),
    /** 忘记密码验证码 */
    FORGET_PASSWORD(3),
    /** 验证身份验证码 */
    VERIFY_IDENTIDY(4),
    /** 管理员登陆 **/
    ADMIN_LOGIN(5),
    /**快捷登陆**/
    SHORTCUT_LOGIN(6);

    private int result = 1;

    VerificationCodeType(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public static VerificationCodeType valueOf(Integer v) {
        switch (v) {
            case 1 : {
                return REGISTER;
            }
            case 2 : {
                return UPDATE_PASSWORD;
            }
            case 3 : {
                return FORGET_PASSWORD;
            }
            case 4 : {
                return VERIFY_IDENTIDY;
            }
            case 5 : {
                return ADMIN_LOGIN;
            }
            case 6 : {
                return SHORTCUT_LOGIN;
            }

            default:{
                return REGISTER;
            }
        }

    }

}
