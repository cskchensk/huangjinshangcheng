package cn.ug.login.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("verificationCode")
public interface VerificationCodeServiceApi {

    /**
     * 验证验证码
     * @param loginType	    登录类型 1：系统维护账号 2：普通用户 3：会员
     * @param loginSource	登录来源 1：PC 2：Android App 3：IOS App
     * @param type	        类型 1：注册验证码 2：修改密码验证码 3：忘记密码验证码 4：验证身份验证码
     * @param loginName	    用户名
     * @param code	        验证码
     * @return			    是否操作成功
     */
    @RequestMapping(value = "check", method = GET)
    SerializeObject check(@RequestParam("loginType") Integer loginType, @RequestParam("loginSource") Integer loginSource,
                          @RequestParam("type") Integer type, @RequestParam("loginName") String loginName,
                          @RequestParam("code") String code);

}
