package cn.ug.login.bean.type;

import cn.ug.bean.type.BaseType;

/**
 * 登录来源
 * @author kaiwotech
 */
public class LoginSource extends BaseType {

	/** PC */
	public static final int WEB 			= 1;
	/** Android App */
	public static final int APP_ANDROID		= 2;
	/** IOS App */
	public static final int APP_IOS 		= 3;

}
