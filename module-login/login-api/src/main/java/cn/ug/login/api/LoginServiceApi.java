package cn.ug.login.api;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("user")
public interface LoginServiceApi {

    /**
     * 登录
     * @return			是否登录成功
     */
    @RequestMapping(value = "login", method = GET)
    SerializeObject login(@RequestParam Map<String, Object> param);

    /**
     * 退出登录
     * @param accessToken	登录成功后分配的Key
     * @return			    是否操作成功
     */
    @RequestMapping(value = "logout", method = GET)
    SerializeObject logout(@RequestParam("accessToken") String accessToken);

    /**
     * 检测是否在线、是否登录超时
     * @param accessToken	登录成功后分配的Key
     * @return			    是否在线
     */
    @RequestMapping(value = "isOnline", method = GET)
    SerializeObject<LoginBean> isOnline(@RequestParam("accessToken") String accessToken);

}
