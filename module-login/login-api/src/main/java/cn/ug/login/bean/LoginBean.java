package cn.ug.login.bean;


import cn.ug.login.bean.type.LoginSource;
import cn.ug.login.bean.type.LoginType;
import cn.ug.login.bean.type.OnlineType;

/**
 * 登录账号信息
 * @author kaiwotech
 */
public class LoginBean implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int loginType 			= LoginType.ADMINISTRATOR;
	private int loginSource 		= LoginSource.WEB;
	private String accessToken		= "";
	private String refreshToken		= "";
	private String id 				= "";
	private String loginName 		= "";
	private String headImage		= "";
	private String name 			= "";
	private String mobile 			= "";
	private String ip 				= "";
	private String hostName 		= "";
	private long loginTime			= 0;
	private long updateTimeMillis 	= 0;
	private int onlineType 			= OnlineType.ONLINE;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public int getLoginType() {
		return loginType;
	}

	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}

	public int getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(int loginSource) {
		this.loginSource = loginSource;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}

	public String getHeadImage() {
		return headImage;
	}

	public void setHeadImage(String headImage) {
		this.headImage = headImage;
	}

	public long getUpdateTimeMillis() {
		return updateTimeMillis;
	}

	public void setUpdateTimeMillis(long updateTimeMillis) {
		this.updateTimeMillis = updateTimeMillis;
	}

	public int getOnlineType() {
		return onlineType;
	}

	public void setOnlineType(int onlineType) {
		this.onlineType = onlineType;
	}
}