package cn.ug.help.service.impl;

import static cn.ug.config.CacheType.OBJECT;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.help.bean.HelpCentreBean;
import cn.ug.help.mapper.HelpCentreMapper;
import cn.ug.help.mapper.entity.HelpCentre;
import cn.ug.help.service.HelpCentreService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;

@Service
public class HelpCentreServiceImpl extends BaseServiceImpl implements HelpCentreService {
	@Autowired
	private HelpCentreMapper helpCentreMapper;
	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Override
	public boolean save(HelpCentreBean entityBean) {
		if (entityBean != null) {
			HelpCentre entity = dozerBeanMapper.map(entityBean, HelpCentre.class);
			if (StringUtils.isBlank(entityBean.getId())) {
				entity.setId(UF.getRandomUUID());
				entity.setAddTime(LocalDateTime.now());
				return helpCentreMapper.insert(entity) > 0 ? true : false;
			} else {
				entity.setModifyTime(LocalDateTime.now());
				return helpCentreMapper.update(entity) > 0 ? true : false;
			}
		}
		return false;
	}

	@Override
	public boolean remove(String id) {
		if (StringUtils.isNoneBlank(id)) {
			return helpCentreMapper.delete(id) > 0 ? true : false;
		}
		return false;
	}

	@Override
	public boolean removeInBatch(String[] id) {
		if (id != null && id.length > 0) {
			return helpCentreMapper.deleteByIds(id) > 0 ? true : false;
		}
		return false;
	}

	@Override
	public HelpCentreBean get(String id) {
		if (StringUtils.isNoneBlank(id)) {
			HelpCentre entity = helpCentreMapper.findById(id);
			if (entity == null) {
				return null;
			}
			HelpCentreBean entityBean = dozerBeanMapper.map(entity, HelpCentreBean.class);
			entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
			entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
			return entityBean;
		}
		return null;
	}

	@Override
	public List<HelpCentreBean> listRecords(String title, String category, int offset, int size) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (StringUtils.isNoneBlank(title)) {
			params.put("title", title);
		}
		if (StringUtils.isNoneBlank(category)) {
			params.put("category", category);
		}
		params.put("offset", offset);
		params.put("size", size);
		List<HelpCentreBean> data = new ArrayList<HelpCentreBean>();
		List<HelpCentre> list = helpCentreMapper.query(params);
		if (list == null || list.size() == 0) {
			return null;
		}
		for (HelpCentre entity : list) {
			HelpCentreBean bean = dozerBeanMapper.map(entity, HelpCentreBean.class);
			if (entity.getAddTime() != null) {
				bean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
			}
			if (entity.getModifyTime() != null) {
				bean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
			}
			data.add(bean);
		}
		return data;
	}

	@Override
	public int countRecords(String title, String category) {
		Map<String, Object> params = new HashMap<String, Object>();
		if (StringUtils.isNoneBlank(title)) {
			params.put("title", title);
		}
		if (StringUtils.isNoneBlank(category)) {
			params.put("category", category);
		}
		return helpCentreMapper.count(params);
	}

    @Override
    public boolean modifyStatus(String[] id, int status) {
        if (id == null || id.length == 0) {
	        return false;
        }
        if (status == 2 || status == 1) {
	        return helpCentreMapper.updateStatus(id, status) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<HelpCentreBean> listDisplayRecords(String title) {
        List<HelpCentreBean> data = new ArrayList<HelpCentreBean>();
        List<HelpCentre> list = helpCentreMapper.queryDisplayList(title);
        if (list == null || list.size() == 0) {
            return null;
        }
        for (HelpCentre entity : list) {
            HelpCentreBean bean = dozerBeanMapper.map(entity, HelpCentreBean.class);
            if (entity.getAddTime() != null) {
                bean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
            }
            if (entity.getModifyTime() != null) {
                bean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
            }
            data.add(bean);
        }
        return data;
    }
}