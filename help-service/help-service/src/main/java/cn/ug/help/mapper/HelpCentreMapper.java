package cn.ug.help.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import cn.ug.help.mapper.entity.HelpCentre;
import cn.ug.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

@Component
public interface HelpCentreMapper extends BaseMapper<HelpCentre>  {
    int count(Map<String, Object> params);
    List<HelpCentre> queryDisplayList(@Param("title")String title);
    int updateStatus(@Param("id")String[] id, @Param("status")int status);
}