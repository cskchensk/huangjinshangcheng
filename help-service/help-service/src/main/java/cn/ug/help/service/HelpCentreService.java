package cn.ug.help.service;

import java.util.List;

import cn.ug.bean.base.DataTable;
import cn.ug.help.bean.HelpCentreBean;

public interface HelpCentreService {
	boolean save(HelpCentreBean entityBean);
	boolean remove(String id);
	boolean removeInBatch(String[] id);
	HelpCentreBean get(String id);
	List<HelpCentreBean> listRecords(String title, String category, int offset, int size);
	int countRecords(String title, String category);
	boolean modifyStatus(String[] id, int status);
	List<HelpCentreBean> listDisplayRecords(String title);
}