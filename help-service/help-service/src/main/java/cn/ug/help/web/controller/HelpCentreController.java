package cn.ug.help.web.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.ug.core.login.LoginHelper;
import cn.ug.util.PaginationUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.help.bean.HelpCentreBean;
import cn.ug.help.service.HelpCentreService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;

@RestController
@RequestMapping("faq")
public class HelpCentreController extends BaseController {
	@Autowired
	private Config config;
	@Autowired
	private HelpCentreService helpCentreService;
	
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public SerializeObject get(@RequestHeader String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        HelpCentreBean entity = helpCentreService.get(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }
	
	@SuppressWarnings("rawtypes")
	@RequiresPermissions("help:faq:update")
    @RequestMapping(method = RequestMethod.POST)
    public SerializeObject update(@RequestHeader String accessToken, HelpCentreBean entity) {
        if(null == entity || StringUtils.isBlank(entity.getTitle())) {
            return new SerializeObjectError("21000001");
        }
        if (StringUtils.isBlank(entity.getContent())) {
        	return new SerializeObjectError("21000002");
        }
        if (StringUtils.isBlank(entity.getCategory())) {
        	return new SerializeObjectError("21000003");
        }
        entity.setCreateUserId(LoginHelper.getLoginId());
        entity.setCreateUserName(LoginHelper.getName());
        if (helpCentreService.save(entity)) {
        	return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
        	return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }
	
	@SuppressWarnings("rawtypes")
	@RequiresPermissions("help:faq:delete")
    @RequestMapping(method = RequestMethod.PUT)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        if (helpCentreService.removeInBatch(id)) {
        	return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
        	return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @RequiresPermissions("help:faq:display")
    @RequestMapping(value = "/display" , method =  RequestMethod.POST)
    public SerializeObject display(@RequestHeader String accessToken, Integer status, String[] id) {
        status = status == null ? 1 : status;
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        if (helpCentreService.modifyStatus(id, status)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }
	
	@RequiresPermissions("help:faq")
    @RequestMapping(value = "/list" , method =  RequestMethod.GET)
    public SerializeObject<DataTable<HelpCentreBean>> list(@RequestHeader String accessToken, Page page, String title, String category) {
        title = UF.toString(title);
        category = UF.toString(category);
        if (StringUtils.isNotBlank(title)) {
            title = StringUtils.replace(title, "%", "\\%");
        }
        int total = helpCentreService.countRecords(title, category);
        page.setTotal(total);
        if (total > 0) {
            List<HelpCentreBean> list = helpCentreService.listRecords(title, category, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<HelpCentreBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<HelpCentreBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<HelpCentreBean>()));
    }

    @RequestMapping(value = "/display/list" , method =  RequestMethod.GET)
    public SerializeObject<List<HelpCentreBean>> list(String title, String category) {
        title = UF.toString(title);
        String type = UF.toString(category);
        if (StringUtils.isNotBlank(title)) {
            title = StringUtils.replace(title, "%", "\\%");
        }
	    List<HelpCentreBean> list = helpCentreService.listDisplayRecords(title);
        if (list != null && list.size() > 0) {
            list.forEach(bean ->{
                bean.setStatus(0);
            });
        }
        List<HelpCentreBean> beans = new ArrayList<HelpCentreBean>();
        if (list != null && list.size() > 0 && StringUtils.isNotBlank(type)) {
            list.forEach(bean ->{
                if (StringUtils.equals(type, bean.getCategory())) {
                    beans.add(bean);
                }
            });
            return new SerializeObject<>(ResultType.NORMAL, beans);
        }
	    return new SerializeObject<>(ResultType.NORMAL, list);
    }

    @RequestMapping(value = "/category/list" , method =  RequestMethod.GET)
    public SerializeObject<Set<String>> listCategory() {
        List<HelpCentreBean> list = helpCentreService.listDisplayRecords("");
        Set<String> categories = new HashSet<String>();
        if (list != null && list.size() > 0) {
            list.forEach(bean ->{
                categories.add(bean.getCategory());
            });
        }
        return new SerializeObject<>(ResultType.NORMAL, categories);
    }
}