package cn.ug;

import org.dozer.DozerBeanMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import cn.ug.config.Config;

@SpringBootApplication
@EnableEurekaClient
@MapperScan("cn.ug.help.mapper")
public class HelpServiceApplication {

	@Bean
	@ConfigurationProperties(prefix = "config")
	public Config config(){
		return new Config();
	}

	@Bean
	public DozerBeanMapper dozerBeanMapper(){
		return new DozerBeanMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(HelpServiceApplication.class, args);
	}
}