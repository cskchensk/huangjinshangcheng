package cn.ug.mall.bean;

import java.util.List;

public class BusinessIntroductionBean {

    private Integer showTag;
    private List<BusinessIntroduction> businessIntroductions;

    public Integer getShowTag() {
        return showTag;
    }

    public void setShowTag(Integer showTag) {
        this.showTag = showTag;
    }

    public List<BusinessIntroduction> getBusinessIntroductions() {
        return businessIntroductions;
    }

    public void setBusinessIntroductions(List<BusinessIntroduction> businessIntroductions) {
        this.businessIntroductions = businessIntroductions;
    }
}
