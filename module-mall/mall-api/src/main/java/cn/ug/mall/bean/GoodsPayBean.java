package cn.ug.mall.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class GoodsPayBean implements Serializable {
    private long goodsId;
    private BigDecimal totalGram;
    private int totalNum;

    public long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(long goodsId) {
        this.goodsId = goodsId;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }
}
