package cn.ug.mall.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/process")
public interface ProcessTemplateApi {

    @GetMapping("/find")
    SerializeObject find(@RequestParam("id") Integer id);
}
