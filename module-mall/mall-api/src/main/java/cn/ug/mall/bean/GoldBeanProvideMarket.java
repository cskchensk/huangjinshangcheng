package cn.ug.mall.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 金豆发放营销设置
 */
public class GoldBeanProvideMarket implements Serializable {

    /**
     * 启用完善资料赠金豆
     * 0:关闭
     * 1：启用
     */
    private Integer supplementStatus = 0;

/************************启用邀请好友赠送金豆**************************************/
    /**
     * 启用邀请好友赠送金豆
     * 0:关闭
     * 1：打开
     */
    private Integer inviteFriendsStatus;

    /**
     * 邀请人获得金豆
     */
    private Integer inviterGiveGoldBean;

    /**
     * 被邀请人获得金豆
     */
    private Integer inviteeGiveGoldBean;

    /**
     * 每日上限
     * 0:不限
     * 1：限制
     */
    private Integer everyDayUpperStatus = 0;

    /**
     * 限制次数
     */
    private Integer upperNum;

    /************************启用签到赠送金豆体系**************************************/
    /**
     * 签到赠送金豆体系
     * 0:关闭
     * 1:开启
     */
    private Integer signStatus;

    /**
     * 签到类型
     * 1:每日签到金豆不同
     * 2:每日签到金豆相同
     */
    private Integer signType;

    /**
     * 签到天数对应获得的金豆数量
     */
    private List<Integer> signDaysToGoldBeanList;

    /**
     * 签满
     * 1.前6天获得金豆数成2
     * 2.自定义金豆数量
     */
    private Integer signFullType;
    /**
     * 自定义金豆数
     */
    private Integer signFullGoldBeanNum;
    /**
     * 额外奖励状态
     * 0：没有(默认)
     * 1：有
     */
    private Integer extraAwardStatus = 0;


    /**
     * 每日签到可得金豆（签到类型为2的时候）
     */
    private Integer goldBeanNum;

    public Integer getSupplementStatus() {
        return supplementStatus;
    }

    public void setSupplementStatus(Integer supplementStatus) {
        this.supplementStatus = supplementStatus;
    }

    public Integer getInviteFriendsStatus() {
        return inviteFriendsStatus;
    }

    public void setInviteFriendsStatus(Integer inviteFriendsStatus) {
        this.inviteFriendsStatus = inviteFriendsStatus;
    }

    public Integer getInviterGiveGoldBean() {
        return inviterGiveGoldBean;
    }

    public void setInviterGiveGoldBean(Integer inviterGiveGoldBean) {
        this.inviterGiveGoldBean = inviterGiveGoldBean;
    }

    public Integer getInviteeGiveGoldBean() {
        return inviteeGiveGoldBean;
    }

    public void setInviteeGiveGoldBean(Integer inviteeGiveGoldBean) {
        this.inviteeGiveGoldBean = inviteeGiveGoldBean;
    }

    public Integer getEveryDayUpperStatus() {
        return everyDayUpperStatus;
    }

    public void setEveryDayUpperStatus(Integer everyDayUpperStatus) {
        this.everyDayUpperStatus = everyDayUpperStatus;
    }

    public Integer getUpperNum() {
        return upperNum;
    }

    public void setUpperNum(Integer upperNum) {
        this.upperNum = upperNum;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public Integer getSignType() {
        return signType;
    }

    public void setSignType(Integer signType) {
        this.signType = signType;
    }

    public List<Integer> getSignDaysToGoldBeanList() {
        return signDaysToGoldBeanList;
    }

    public void setSignDaysToGoldBeanList(List<Integer> signDaysToGoldBeanList) {
        this.signDaysToGoldBeanList = signDaysToGoldBeanList;
    }

    public Integer getSignFullType() {
        return signFullType;
    }

    public void setSignFullType(Integer signFullType) {
        this.signFullType = signFullType;
    }

    public Integer getSignFullGoldBeanNum() {
        return signFullGoldBeanNum;
    }

    public void setSignFullGoldBeanNum(Integer signFullGoldBeanNum) {
        this.signFullGoldBeanNum = signFullGoldBeanNum;
    }

    public Integer getExtraAwardStatus() {
        return extraAwardStatus;
    }

    public void setExtraAwardStatus(Integer extraAwardStatus) {
        this.extraAwardStatus = extraAwardStatus;
    }

    public Integer getGoldBeanNum() {
        return goldBeanNum;
    }

    public void setGoldBeanNum(Integer goldBeanNum) {
        this.goldBeanNum = goldBeanNum;
    }
}
