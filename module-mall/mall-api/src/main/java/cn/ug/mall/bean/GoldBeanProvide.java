package cn.ug.mall.bean;

import java.io.Serializable;

/**
 * 金豆发放基础设置
 */
public class GoldBeanProvide  implements Serializable {
    /**
     * 注册
     */
    private Integer registerGoldBean;
    /**
     * 认证绑卡
     */
    private Integer tiedCardGoldBean;
    /**
     * 首次体验金购买成功
     */
    private Integer firstBuyExperienceGold;
    /**
     * 首次实物金购买成功
     */
    private Integer firstBuyEntityGold;
    /**
     * 首次回租提单成功
     */
    private Integer firstLeaseBack;
    /**
     * 首次金饰购买成功
     */
    private Integer firstBuyGoldOrnament;

    public Integer getRegisterGoldBean() {
        return registerGoldBean;
    }

    public void setRegisterGoldBean(Integer registerGoldBean) {
        this.registerGoldBean = registerGoldBean;
    }

    public Integer getTiedCardGoldBean() {
        return tiedCardGoldBean;
    }

    public void setTiedCardGoldBean(Integer tiedCardGoldBean) {
        this.tiedCardGoldBean = tiedCardGoldBean;
    }

    public Integer getFirstBuyExperienceGold() {
        return firstBuyExperienceGold;
    }

    public void setFirstBuyExperienceGold(Integer firstBuyExperienceGold) {
        this.firstBuyExperienceGold = firstBuyExperienceGold;
    }

    public Integer getFirstBuyEntityGold() {
        return firstBuyEntityGold;
    }

    public void setFirstBuyEntityGold(Integer firstBuyEntityGold) {
        this.firstBuyEntityGold = firstBuyEntityGold;
    }

    public Integer getFirstLeaseBack() {
        return firstLeaseBack;
    }

    public void setFirstLeaseBack(Integer firstLeaseBack) {
        this.firstLeaseBack = firstLeaseBack;
    }

    public Integer getFirstBuyGoldOrnament() {
        return firstBuyGoldOrnament;
    }

    public void setFirstBuyGoldOrnament(Integer firstBuyGoldOrnament) {
        this.firstBuyGoldOrnament = firstBuyGoldOrnament;
    }
}
