package cn.ug.mall.bean;

import java.util.List;

/**
 * 投资版块配置
 */
public class InvestBoardBean {

    //产品展示区
    private List<ModuleTitle> moduleTitleList;
    //业务介绍区是否显示 1显示，0隐藏
    private Integer businessShowTag;
    //业务介绍区配置
    private List<BusinessIntroduction> businessIntroductions;
    //投资动态是否显示 1显示，0隐藏
    private Integer investDynamicShowTag;
    //投资动态读取条数
    private Integer investReadNum;

    public List<ModuleTitle> getModuleTitleList() {
        return moduleTitleList;
    }

    public void setModuleTitleList(List<ModuleTitle> moduleTitleList) {
        this.moduleTitleList = moduleTitleList;
    }

    public Integer getBusinessShowTag() {
        return businessShowTag;
    }

    public void setBusinessShowTag(Integer businessShowTag) {
        this.businessShowTag = businessShowTag;
    }

    public List<BusinessIntroduction> getBusinessIntroductions() {
        return businessIntroductions;
    }

    public void setBusinessIntroductions(List<BusinessIntroduction> businessIntroductions) {
        this.businessIntroductions = businessIntroductions;
    }

    public Integer getInvestDynamicShowTag() {
        return investDynamicShowTag;
    }

    public void setInvestDynamicShowTag(Integer investDynamicShowTag) {
        this.investDynamicShowTag = investDynamicShowTag;
    }

    public Integer getInvestReadNum() {
        return investReadNum;
    }

    public void setInvestReadNum(Integer investReadNum) {
        this.investReadNum = investReadNum;
    }
}
