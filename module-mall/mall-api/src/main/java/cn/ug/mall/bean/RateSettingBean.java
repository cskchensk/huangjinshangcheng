package cn.ug.mall.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 费率设置bean
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class RateSettingBean implements Serializable {

    /**
     * 是否有手续费
     */
    private Integer                  isFee;
    /**
     * 收费方式
     * 1：按固定费用收费 单位(元)
     * 2：按费率扣除 千分比
     */
    private String                   takeWay;
    /**
     * 周期
     * 1:每天
     * 2:每月
     */
    private Integer                  period;
    /**
     * 前几笔
     */
    private Integer                  number;
    /**
     * 前 number 收费或费率
     */
    private BigDecimal               agoRate;
    /**
     * 后 number 收费会费率
     */
    private BigDecimal               laterRate;
    /**
     * 是否有会员折扣
     */
    private Integer                  isDiscount;
    /**
     * 会员折扣列表
     */
    private List<MemberDiscountBean> list;

    public Integer getIsFee() {
        return isFee;
    }

    public void setIsFee(Integer isFee) {
        this.isFee = isFee;
    }

    public String getTakeWay() {
        return takeWay;
    }

    public void setTakeWay(String takeWay) {
        this.takeWay = takeWay;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getAgoRate() {
        return agoRate;
    }

    public void setAgoRate(BigDecimal agoRate) {
        this.agoRate = agoRate;
    }

    public BigDecimal getLaterRate() {
        return laterRate;
    }

    public void setLaterRate(BigDecimal laterRate) {
        this.laterRate = laterRate;
    }

    public Integer getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(Integer isDiscount) {
        this.isDiscount = isDiscount;
    }

    public List<MemberDiscountBean> getList() {
        return list;
    }

    public void setList(List<MemberDiscountBean> list) {
        this.list = list;
    }


    /**
     *
     */

}
