package cn.ug.mall.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 加工费模板
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class ProcessTemplateBean  implements Serializable {

    private Integer    id;
    private String     name;
    /**
     * 类型 1：金条 2:金饰
     */
    private Integer    type;
    private BigDecimal processCost;
    private Integer    disCount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Integer getDisCount() {
        return disCount;
    }

    public void setDisCount(Integer disCount) {
        this.disCount = disCount;
    }
}
