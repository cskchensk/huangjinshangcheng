package cn.ug.mall.bean;

import java.io.Serializable;

/**
 * 对公账号配置设置
 *
 * @Author zhangweijie
 * @Date 2019/8/27 0027
 * @time 上午 9:43
 **/
public class BroughtToAccountBean implements Serializable {


    private static final long serialVersionUID = 9015798693355102018L;

    /**
     * 对公账户全称
     */
    private String accountName;

    /**
     * 对公账户账号
     */
    private String bankAccount;

    /**
     * 对公账户开户行
     */
    private String openBank;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getOpenBank() {
        return openBank;
    }

    public void setOpenBank(String openBank) {
        this.openBank = openBank;
    }
}
