package cn.ug.mall.bean;

import java.io.Serializable;

/**
 * 会员等级折扣bean
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class MemberDiscountBean implements Serializable {

    /**
     * id
     */
    private Integer id;

    /**
     * 会员等级id
     */
    private Integer levelId;

    /**
     * 等级名称
     */
    private Integer levelName;

    /**
     * 折扣 百分比
     */
    private Integer discount;

}
