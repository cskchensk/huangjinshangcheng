package cn.ug.mall.bean;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * 商品图片表
 * @author kaiwotech
 */
public class LogisticsBean extends BaseBean implements Serializable {

    /** 订单号 */
    private String orderNo;
    /** 物流单号 */
    private String serialNo;
    /** 物流公司 */
    private String company;
    /** 物流公司联系电话 */
    private String phone;
    /** 收货时间 */
    private String receiveTimeString;
    /** 状态:1:待收货；2：已收货 */
    private int status;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReceiveTimeString() {
        return receiveTimeString;
    }

    public void setReceiveTimeString(String receiveTimeString) {
        this.receiveTimeString = receiveTimeString;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}