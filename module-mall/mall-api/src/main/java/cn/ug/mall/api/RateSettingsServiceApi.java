package cn.ug.mall.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.ModuleTitle;
import cn.ug.mall.bean.PrivilegeTemplateBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("/setting")
public interface RateSettingsServiceApi {

    @GetMapping
    SerializeObject get(@RequestParam("keyName")String keyName);

    /**
     * 内部接口调用
     * 根据type查询启用的优惠模板
     * @param type 1.买金优惠模板 2.回租优惠模板
     * @return
     */
    @RequestMapping(value = "/privilegeTemplate/findONTemplateByType" , method = GET)
    SerializeObject<PrivilegeTemplateBean> findONTemplateByType(@RequestParam("type")Integer type);

    /**
     * 获取投资版块配置
     * @return
     */
    @RequestMapping(value = "/investArea/query", method = GET)
    SerializeObject findInvest();

    /**
     * 获取产品展示区配置
     * @return
     */
    @RequestMapping(value = "/investArea/productModule", method = GET)
    SerializeObject<List<ModuleTitle>> findProductModule();
}
