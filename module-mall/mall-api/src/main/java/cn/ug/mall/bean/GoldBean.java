package cn.ug.mall.bean;

import java.io.Serializable;

/**
 * 金豆基础值
 */
public class GoldBean implements Serializable {

    /**
     * 金豆参数设置
     * 金豆换算方式 每xxx个金豆等于goldGram克黄金
     */
    private Integer goldBeanNum;

    /**
     * 金豆参数设置
     * 金豆换算方式 每xxx个金豆等于goldGram克黄金
     */
    private Integer goldGram;

    /**
     * 金豆过期设置
     * 0.一直有效，永不过期
     * 1.存在有效期 每年xx:xx日扣减xx金豆数量
     */
    private Integer expireType;

    /**
     * 每年扣减日期  格式为 月:日
     */
    private String abatementDate;

    /**
     * 扣减数量的类型
     * 1.一半
     * 2.全部
     */
    private Integer abatementType;

    /**
     * 是否开启短信通知
     * 0.不开启
     * 1.开启
     */
    private Integer isSmsMessage = 0;

    /**
     * 金豆兑换设置类型
     * 0.不可兑换
     * 1.可兑换
     */
    private Integer exchangeType;

    /**
     * 金豆起兑数量
     */
    private Integer exchangeOrigin;

    //*************************金豆抵扣**********************
    /**
     * 金豆抵扣起始值
     * 至少需要xx个金豆才可用于抵扣
     */
    private Integer goldBeanMin;

    /**
     * 每笔订单至多可抵xx金豆
     */
    private Integer goldBeanMax;


    public Integer getGoldBeanNum() {
        return goldBeanNum;
    }

    public void setGoldBeanNum(Integer goldBeanNum) {
        this.goldBeanNum = goldBeanNum;
    }

    public Integer getGoldGram() {
        return goldGram;
    }

    public void setGoldGram(Integer goldGram) {
        this.goldGram = goldGram;
    }

    public Integer getExpireType() {
        return expireType;
    }

    public void setExpireType(Integer expireType) {
        this.expireType = expireType;
    }

    public String getAbatementDate() {
        return abatementDate;
    }

    public void setAbatementDate(String abatementDate) {
        this.abatementDate = abatementDate;
    }

    public Integer getAbatementType() {
        return abatementType;
    }

    public void setAbatementType(Integer abatementType) {
        this.abatementType = abatementType;
    }

    public Integer getIsSmsMessage() {
        return isSmsMessage;
    }

    public void setIsSmsMessage(Integer isSmsMessage) {
        this.isSmsMessage = isSmsMessage;
    }

    public Integer getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(Integer exchangeType) {
        this.exchangeType = exchangeType;
    }

    public Integer getExchangeOrigin() {
        return exchangeOrigin;
    }

    public void setExchangeOrigin(Integer exchangeOrigin) {
        this.exchangeOrigin = exchangeOrigin;
    }

    public Integer getGoldBeanMin() {
        return goldBeanMin;
    }

    public void setGoldBeanMin(Integer goldBeanMin) {
        this.goldBeanMin = goldBeanMin;
    }

    public Integer getGoldBeanMax() {
        return goldBeanMax;
    }

    public void setGoldBeanMax(Integer goldBeanMax) {
        this.goldBeanMax = goldBeanMax;
    }
}
