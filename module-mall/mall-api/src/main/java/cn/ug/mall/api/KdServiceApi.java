package cn.ug.mall.api;

import cn.ug.bean.Result;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("kd")
public interface KdServiceApi {

    /**
     * 查询订单信息
     * @param code
     * @param postId
     * @return
     */
    @GetMapping("/queryOrderTrace")
    SerializeObject<Result> queryOrderTrace(@RequestParam("code")String code, @RequestParam("postId") String postId);

}
