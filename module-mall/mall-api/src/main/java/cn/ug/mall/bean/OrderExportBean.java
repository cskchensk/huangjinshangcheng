package cn.ug.mall.bean;

public class OrderExportBean extends OrderBean {
    private int index;

    private String mallOrderType;

    private String mallOrderStatus;

    private String exportPayTime;

    private String exportAddTime;

    private String exportSendTime;

    private String exportCloseTime;
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMallOrderType() {
        return mallOrderType;
    }

    public void setMallOrderType(String mallOrderType) {
        this.mallOrderType = mallOrderType;
    }

    public String getMallOrderStatus() {
        return mallOrderStatus;
    }

    public void setMallOrderStatus(String mallOrderStatus) {
        this.mallOrderStatus = mallOrderStatus;
    }

    public String getExportPayTime() {
        return exportPayTime;
    }

    public void setExportPayTime(String exportPayTime) {
        this.exportPayTime = exportPayTime;
    }

    public String getExportAddTime() {
        return exportAddTime;
    }

    public void setExportAddTime(String exportAddTime) {
        this.exportAddTime = exportAddTime;
    }

    public String getExportSendTime() {
        return exportSendTime;
    }

    public void setExportSendTime(String exportSendTime) {
        this.exportSendTime = exportSendTime;
    }

    public String getExportCloseTime() {
        return exportCloseTime;
    }

    public void setExportCloseTime(String exportCloseTime) {
        this.exportCloseTime = exportCloseTime;
    }
}
