package cn.ug.mall.bean;

import java.io.Serializable;

public class FeeBean implements Serializable {
    /**商品ID**/
    private long goodsId;
    /**商品名称**/
    private String name;
    // 运费
    private double freight;
    // 加工费
    private double processFee;

    public long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(long goodsId) {
        this.goodsId = goodsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getFreight() {
        return freight;
    }

    public void setFreight(double freight) {
        this.freight = freight;
    }

    public double getProcessFee() {
        return processFee;
    }

    public void setProcessFee(double processFee) {
        this.processFee = processFee;
    }
}
