package cn.ug.mall.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 买金优惠设置
 */
public class GoldPrivilegeBean implements Serializable{

    /**
     * 类型  1.不优惠  2.优惠
     */
    private Integer type;

    /**
     * 优惠类型：1.现金优惠 2.奖励金豆
     */
    private Integer privilegeType;

    /**
     * 选择框
     * 1.首克优惠，首克后不继续优惠
     * 2.首克优惠  后续继续优惠
     */
    private Integer selectType;

    /**
     * 首克优惠金额
     */
    private BigDecimal firstGramPrivilegeMoney;

    /**
     * 首克后每克优惠 xxx 元
     */
    private BigDecimal firstGramNextPrivilegeMoney;

    /**
     * 最高优惠至 xx 克
     */
    private Integer maxPrivilegeGram;

    /**
     * 首克奖励xx金豆
     */
    private Integer firstGramPrivilegeBean;

    /**
     * 首克后每克奖励 xxx 金豆
     */
    private Integer firstGramNextPrivilegeBean;

    /**
     * 首克优惠后续继续优惠-首克优惠金额
     */
    private BigDecimal firstGramSecPrivilegeMoney;

    /**
     * 首克优惠后续继续优惠-首克优惠金豆
     */
    private Integer firstGramSecPrivilegeBean;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPrivilegeType() {
        return privilegeType;
    }

    public void setPrivilegeType(Integer privilegeType) {
        this.privilegeType = privilegeType;
    }

    public Integer getSelectType() {
        return selectType;
    }

    public void setSelectType(Integer selectType) {
        this.selectType = selectType;
    }

    public BigDecimal getFirstGramPrivilegeMoney() {
        return firstGramPrivilegeMoney;
    }

    public void setFirstGramPrivilegeMoney(BigDecimal firstGramPrivilegeMoney) {
        this.firstGramPrivilegeMoney = firstGramPrivilegeMoney;
    }

    public BigDecimal getFirstGramNextPrivilegeMoney() {
        return firstGramNextPrivilegeMoney;
    }

    public void setFirstGramNextPrivilegeMoney(BigDecimal firstGramNextPrivilegeMoney) {
        this.firstGramNextPrivilegeMoney = firstGramNextPrivilegeMoney;
    }

    public Integer getMaxPrivilegeGram() {
        return maxPrivilegeGram;
    }

    public void setMaxPrivilegeGram(Integer maxPrivilegeGram) {
        this.maxPrivilegeGram = maxPrivilegeGram;
    }

    public Integer getFirstGramPrivilegeBean() {
        return firstGramPrivilegeBean;
    }

    public void setFirstGramPrivilegeBean(Integer firstGramPrivilegeBean) {
        this.firstGramPrivilegeBean = firstGramPrivilegeBean;
    }

    public Integer getFirstGramNextPrivilegeBean() {
        return firstGramNextPrivilegeBean;
    }

    public void setFirstGramNextPrivilegeBean(Integer firstGramNextPrivilegeBean) {
        this.firstGramNextPrivilegeBean = firstGramNextPrivilegeBean;
    }

    public BigDecimal getFirstGramSecPrivilegeMoney() {
        return firstGramSecPrivilegeMoney;
    }

    public void setFirstGramSecPrivilegeMoney(BigDecimal firstGramSecPrivilegeMoney) {
        this.firstGramSecPrivilegeMoney = firstGramSecPrivilegeMoney;
    }

    public Integer getFirstGramSecPrivilegeBean() {
        return firstGramSecPrivilegeBean;
    }

    public void setFirstGramSecPrivilegeBean(Integer firstGramSecPrivilegeBean) {
        this.firstGramSecPrivilegeBean = firstGramSecPrivilegeBean;
    }
}
