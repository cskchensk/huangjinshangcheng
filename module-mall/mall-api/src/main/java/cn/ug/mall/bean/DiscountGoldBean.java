package cn.ug.mall.bean;/**
 * Create by zhangweijie on 2019/9/10 0010 下午 16:45
 */

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhangweijie
 * @Date 2019/9/10 0010
 * @time 下午 16:45
 **/
public class DiscountGoldBean implements Serializable {
    private static final long serialVersionUID = 3374093087833053028L;

    /**
     * 结算方式  1.以金额发放
     */
    private Integer closeType = 1;
    /**
     * 回租奖励计算金价 1.以购买价格结算
     */
    private Integer leaseCalculateGoldType = 1;
    /**
     * 回租期限  key-value
     */
    private List<LeaseDay> leaseDayList;


    public Integer getCloseType() {
        return closeType;
    }

    public void setCloseType(Integer closeType) {
        this.closeType = closeType;
    }

    public Integer getLeaseCalculateGoldType() {
        return leaseCalculateGoldType;
    }

    public void setLeaseCalculateGoldType(Integer leaseCalculateGoldType) {
        this.leaseCalculateGoldType = leaseCalculateGoldType;
    }

    public List<LeaseDay> getLeaseDayList() {
        return leaseDayList;
    }

    public void setLeaseDayList(List<LeaseDay> leaseDayList) {
        this.leaseDayList = leaseDayList;
    }
}
