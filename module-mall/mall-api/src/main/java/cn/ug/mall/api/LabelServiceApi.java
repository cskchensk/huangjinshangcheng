package cn.ug.mall.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("label")
public interface LabelServiceApi {

    /**
     * 查询商品标签
     *
     * @return
     */
    @GetMapping("/find/label")
    SerializeObject findLabelById(@RequestParam("id") int id);
}
