package cn.ug.mall.bean;

import java.io.Serializable;

/**
 * @author zhaohg
 * @date 2018/07/06.
 */
public class GoodsImgBean implements Serializable {

    /**
     * 图片id
     */
    private Long    id;
    /**
     * 产品id
     */
    private Long    goodsId;
    /**
     * 图片URL
     */
    private String  url;
    /**
     * 排序
     */
    private Integer sort;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

}
