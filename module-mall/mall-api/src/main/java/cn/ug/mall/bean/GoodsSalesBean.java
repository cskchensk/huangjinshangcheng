package cn.ug.mall.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品销量
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class GoodsSalesBean implements Serializable {

    /**
     * 商品id
     */
    private Long id;

    /**
     * skuId
     */
    private Long       skuId;
    /**
     * 商品名称
     */
    private String     name;
    /**
     * 商品编号
     */
    private String     goodsCode;
    /**
     * 主图
     */
    private String     imgUrl;
    /**
     * 上下架状态
     */
    private Integer    shelveType;
    /**
     * 上架时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date       shelveTime;
    /**
     * 下架时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date       unShelveTime;
    /**
     * 销量
     */
    private Integer    sales;
    /**
     * 商品单价
     */
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getShelveType() {
        return shelveType;
    }

    public void setShelveType(Integer shelveType) {
        this.shelveType = shelveType;
    }

    public Date getShelveTime() {
        return shelveTime;
    }

    public void setShelveTime(Date shelveTime) {
        this.shelveTime = shelveTime;
    }

    public Date getUnShelveTime() {
        return unShelveTime;
    }

    public void setUnShelveTime(Date unShelveTime) {
        this.unShelveTime = unShelveTime;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
