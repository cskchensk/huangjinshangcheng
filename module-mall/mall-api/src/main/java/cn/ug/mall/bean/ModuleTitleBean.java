package cn.ug.mall.bean;

import java.util.List;

public class ModuleTitleBean {
    List<ModuleTitle> moduleTitles;

    public List<ModuleTitle> getModuleTitles() {
        return moduleTitles;
    }

    public void setModuleTitles(List<ModuleTitle> moduleTitles) {
        this.moduleTitles = moduleTitles;
    }
}
