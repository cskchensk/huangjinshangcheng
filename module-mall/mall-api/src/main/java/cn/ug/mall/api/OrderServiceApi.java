package cn.ug.mall.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.OrderBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

@RequestMapping("/new/order")
public interface OrderServiceApi {

    @PostMapping
    SerializeObject pay(@RequestParam("memberId")String memberId,
                        @RequestParam("orderId")String orderId,
                        @RequestParam("quantity")int quantity,
                        @RequestParam("skuId")long skuId,
                        @RequestParam("username")String username,
                        @RequestParam("expressFee")BigDecimal expressFee,
                        @RequestParam("processCost")BigDecimal processCost,
                        @RequestParam("orderType")int orderType,
                        @RequestParam("payChannel")int payChannel,
                        @RequestParam("status")int status,
                        @RequestParam("addressId")long addressId,
                        @RequestParam("source")int source,
                        @RequestParam("interestNowAmount")BigDecimal interestNowAmount,
                        @RequestParam("principalNowAmount")BigDecimal principalNowAmount,
                        @RequestParam("principalHistoryAmount")BigDecimal principalHistoryAmount,
                        @RequestParam("freezeAmount")BigDecimal freezeAmount);

    @PostMapping("/fail")
    SerializeObject fail(@RequestParam("orderNO")String orderNO,
                         @RequestParam("closeRemark")String closeRemark);

    @GetMapping("/record")
    SerializeObject<OrderBean> getOrder(@RequestParam("orderNO")String orderNO);

    @PostMapping("/succeed")
    SerializeObject succeed(@RequestParam("orderNO")String orderNO);

    @PostMapping("/handling")
    SerializeObject handling(@RequestParam("orderNO")String orderNO);

    @PostMapping("/validate")
    SerializeObject validateOrder(@RequestParam("orderNO")String orderNO);

    @GetMapping("/goods/names")
    SerializeObject<List<String>> listGoodsNames(@RequestParam("orderNO")String orderNO);

    @GetMapping("/successful/num")
    SerializeObject getSuccessfulNum(@RequestParam("memberId")String memberId);
}