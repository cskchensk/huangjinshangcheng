package cn.ug.mall.bean;

import java.math.BigDecimal;

public class TemplateBean {
    /**
     * 单笔购买满额
     */
    private Integer limit;

    /**
     * 优惠额度
     */
    private BigDecimal privilegeLimit;

    /**
     * 回租期限
     */
    private Integer leaseDay;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public BigDecimal getPrivilegeLimit() {
        return privilegeLimit;
    }

    public void setPrivilegeLimit(BigDecimal privilegeLimit) {
        this.privilegeLimit = privilegeLimit;
    }

    public Integer getLeaseDay() {
        return leaseDay;
    }

    public void setLeaseDay(Integer leaseDay) {
        this.leaseDay = leaseDay;
    }
}
