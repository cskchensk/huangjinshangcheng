package cn.ug.mall.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.MemberGoodsOrderDetailBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("order")
public interface MallGoodsOrderServiceApi {

    /**
     *  支付时根据订单编号获取订单信息
     * @param orderNO 订单编号
     * @return
     */
    @GetMapping("/info")
    SerializeObject<MemberGoodsOrderDetailBean> getOrderDetail(@RequestParam("orderNO")String orderNO);

    /**
     * 支付成功之后将商品订单标记成支付成功
     * @param orderNO 订单编号
     * @return
     */
    @PostMapping("/succeed")
    SerializeObject succeedOrder(@RequestParam("orderNO")String orderNO);

    @PostMapping("/fail")
    public SerializeObject failOrder(@RequestParam("orderNO")String orderNO, @RequestParam("reason")String reason);
}
