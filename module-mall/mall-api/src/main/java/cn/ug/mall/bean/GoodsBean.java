package cn.ug.mall.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 商品
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class GoodsBean implements Serializable {

    /**
     * 商品id
     */
    private Long               id;
    /**
     * 商品名称
     */
    private String             name;
    /**
     * 副标题
     */
    private String             title;
    private List<String>       labels;
    /**
     * 品牌id
     */
    private Long               brandId;
    private String             brandName;
    /**
     * 分类id
     */
    private Integer            categoryId;
    private String             categoryName;
    /**
     * 加工模板id
     */
    private Integer            processId;
    private String             processName;
    /**
     * 加工费
     */
    private BigDecimal         processCost;
    /**
     * 加工费折扣
     */
    private Integer            discount;
    /**
     * 商品类型         1:金条 2:金饰
     */
    private Integer            type;
    /**
     * 商品编号
     */
    private String             goodsCode;
    /**
     * 主图
     */
    private String             imgUrl;
    /**
     * 商品品质
     */
    private String             quality;
    /**
     * 最低价格
     */
    private BigDecimal         minPrice;
    /**
     * 最高价格
     */
    private BigDecimal         maxPrice;
    /**
     * 市场最低价
     */
    private BigDecimal         minMarketPrice;
    /**
     * 市场最高价
     */
    private BigDecimal         maxMarketPrice;
    /**
     * 最小克重
     */
    private BigDecimal         minGram;
    /**
     * 最小克重
     */
    private BigDecimal         maxGram;
    /**
     * 库存
     */
    private Integer            stock;
    /**
     * 是否限购 0:不限 1:限购数量 2:限购克重
     */
    private Integer            isQuota;
    /**
     * 限购数量
     */
    private Integer            quotaNum;
    /**
     * 限购克重
     */
    private BigDecimal         quotaWeight;
    /**
     * 是否有限购时间
     */
    private Integer            isPrompt;
    /**
     * 限购开始时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")//yyyyMMddHHmmss
    private Date               startPrompt;
    /**
     * 限购结束时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date               endPrompt;
    /**
     * 操作人
     */
    private String             operatorId;
    /**
     * 审核人
     */
    private String             auditorId;
    /**
     * 审核状态
     */
    private Integer            auditType;
    /**
     * 上下架状态
     */
    private Integer            shelveType;
    /**
     * 上架时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date               shelveTime;
    /**
     * 下架时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date               unShelveTime;
    /**
     * 提交时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm")
    private Date               addTime;
    /**
     * 下架备注
     */
    private String             downRemark;
    /**
     * 销量
     */
    private Integer            sales;
    /**
     * 购买须知
     */
    private String             notice;
    /**
     * 保养提示
     */
    private String             hint;
    /**
     * 商品规格
     */
    private List<GoodsSkuBean> skuList;

    /**
     * 商品详情图
     */
    private List<String>    imgList;
    /**
     * 商品详情图片 banner
     */
    private List<String>    pictureList;
    /**
     * 商品标签
     */
    private List<LabelBean> labelList;
    private BigDecimal      freight;

    /**
     * 提金换金最大克重
     */
    private BigDecimal takeInMaxGram;
    /**
     * 提金换金折扣
     */
    private Integer takeInDiscount;

    /**
     * 售价
     */
    private BigDecimal sellingPrice;

    /**
     * 市场价
     */
    private BigDecimal marketPrice;

    private Integer isRecommend;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinMarketPrice() {
        return minMarketPrice;
    }

    public void setMinMarketPrice(BigDecimal minMarketPrice) {
        this.minMarketPrice = minMarketPrice;
    }

    public BigDecimal getMaxMarketPrice() {
        return maxMarketPrice;
    }

    public void setMaxMarketPrice(BigDecimal maxMarketPrice) {
        this.maxMarketPrice = maxMarketPrice;
    }

    public BigDecimal getMinGram() {
        return minGram;
    }

    public void setMinGram(BigDecimal minGram) {
        this.minGram = minGram;
    }

    public BigDecimal getMaxGram() {
        return maxGram;
    }

    public void setMaxGram(BigDecimal maxGram) {
        this.maxGram = maxGram;
    }

    public Integer getIsQuota() {
        return isQuota;
    }

    public void setIsQuota(Integer isQuota) {
        this.isQuota = isQuota;
    }

    public Integer getQuotaNum() {
        return quotaNum;
    }

    public void setQuotaNum(Integer quotaNum) {
        this.quotaNum = quotaNum;
    }

    public BigDecimal getQuotaWeight() {
        return quotaWeight;
    }

    public void setQuotaWeight(BigDecimal quotaWeight) {
        this.quotaWeight = quotaWeight;
    }

    public Integer getIsPrompt() {
        return isPrompt;
    }

    public void setIsPrompt(Integer isPrompt) {
        this.isPrompt = isPrompt;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Date getStartPrompt() {
        return startPrompt;
    }

    public void setStartPrompt(Date startPrompt) {
        this.startPrompt = startPrompt;
    }

    public Date getEndPrompt() {
        return endPrompt;
    }

    public void setEndPrompt(Date endPrompt) {
        this.endPrompt = endPrompt;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(String auditorId) {
        this.auditorId = auditorId;
    }

    public Integer getAuditType() {
        return auditType;
    }

    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    public Integer getShelveType() {
        return shelveType;
    }

    public void setShelveType(Integer shelveType) {
        this.shelveType = shelveType;
    }

    public Date getShelveTime() {
        return shelveTime;
    }

    public void setShelveTime(Date shelveTime) {
        this.shelveTime = shelveTime;
    }

    public Date getUnShelveTime() {
        return unShelveTime;
    }

    public void setUnShelveTime(Date unShelveTime) {
        this.unShelveTime = unShelveTime;
    }

    public String getDownRemark() {
        return downRemark;
    }

    public void setDownRemark(String downRemark) {
        this.downRemark = downRemark;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public List<GoodsSkuBean> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<GoodsSkuBean> skuList) {
        this.skuList = skuList;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public List<String> getPictureList() {
        return pictureList;
    }

    public void setPictureList(List<String> pictureList) {
        this.pictureList = pictureList;
    }

    public List<LabelBean> getLabelList() {
        return labelList;
    }

    public void setLabelList(List<LabelBean> labelList) {
        this.labelList = labelList;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public BigDecimal getTakeInMaxGram() {
        return takeInMaxGram;
    }

    public void setTakeInMaxGram(BigDecimal takeInMaxGram) {
        this.takeInMaxGram = takeInMaxGram;
    }

    public Integer getTakeInDiscount() {
        return takeInDiscount;
    }

    public void setTakInDiscount(Integer takeInDiscount) {
        this.takeInDiscount = takeInDiscount;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }
}
