package cn.ug.mall.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 订单跟踪
 *
 * @author zhaohg
 * @date 2018/07/17.
 */
public class OrderLogisticsBean implements Serializable {

    private String context;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   time;

    public OrderLogisticsBean() {

    }

    public OrderLogisticsBean(String context, Date time) {
        this.context = context;
        this.time = time;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
