package cn.ug.mall.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class LeaseDay implements Serializable {
    private int leaseDay;

    private BigDecimal yearsIncome;

    public int getLeaseDay() {
        return leaseDay;
    }

    public void setLeaseDay(int leaseDay) {
        this.leaseDay = leaseDay;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }
}
