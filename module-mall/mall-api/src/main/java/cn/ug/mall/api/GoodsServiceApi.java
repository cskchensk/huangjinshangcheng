package cn.ug.mall.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.FeeBean;
import cn.ug.mall.bean.GoodsSkuBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/goods")
public interface GoodsServiceApi {

    @PostMapping(value = "/validation/payable")
    SerializeObject validate(@RequestParam("skuId") Long skuId,
                             @RequestParam("quantity") int quantity,
                             @RequestParam("memberId") String memberId,
                             @RequestParam("type") int type);

    @GetMapping("/sku")
    SerializeObject<GoodsSkuBean> getSku(@RequestParam("skuId") long skuId);

    @GetMapping("/fee")
    SerializeObject<FeeBean> getFee(@RequestParam("skuId") long skuId,
                                    @RequestParam("quantity")int quantity,
                                    @RequestParam("type")int type);


}