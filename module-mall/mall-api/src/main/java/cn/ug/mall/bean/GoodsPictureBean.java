package cn.ug.mall.bean;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商品图片表
 * @author kaiwotech
 */
public class GoodsPictureBean extends BaseBean implements Serializable {
    /** 产品id */
    private String goodsId;
    /** 图片URL */
    private String imgUrl;
    /** 排序 */
    private int sort;

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }
}