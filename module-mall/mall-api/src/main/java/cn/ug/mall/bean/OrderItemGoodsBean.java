package cn.ug.mall.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单商品规格
 * @author zhaohg
 * @date 2018/07/17.
 */
public class OrderItemGoodsBean  implements Serializable {

    private Long orderId;
    private Long goodsId;
    private String goodsName;
    private String imgUrl;
    private Integer processId;
    private String goodsCode;
    private Integer goodsType;
    private BigDecimal goodsPrice;//单价
    private BigDecimal goodsGram;//重量
    private Integer num;//总数量
    private BigDecimal gram;//总重量
    private BigDecimal price;//总价格
    private BigDecimal processCost;//商品加工费

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }

    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public BigDecimal getGoodsGram() {
        return goodsGram;
    }

    public void setGoodsGram(BigDecimal goodsGram) {
        this.goodsGram = goodsGram;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getGram() {
        return gram;
    }

    public void setGram(BigDecimal gram) {
        this.gram = gram;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }
}
