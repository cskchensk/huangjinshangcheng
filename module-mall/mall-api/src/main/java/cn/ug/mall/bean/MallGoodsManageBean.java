package cn.ug.mall.bean;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author  ywl
 * @date 2018-04-18
 */
public class MallGoodsManageBean   extends BaseBean implements Serializable {

    /** 商品名称 **/
    private String name;
    /** 库存 **/
    private Integer stock;
    /** 商品价格 **/
    private BigDecimal price;
    /** 商品货号 **/
    private String code;
    /** 重量 **/
    private BigDecimal weight;
    /** 主图URL **/
    private String thumbnailUrl;
    /** 上架状态：1：待上架；2：上架；3：下架 **/
    private Integer saleStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Integer getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(Integer saleStatus) {
        this.saleStatus = saleStatus;
    }
}
