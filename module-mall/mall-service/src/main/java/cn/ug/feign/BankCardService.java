package cn.ug.feign;

import cn.ug.pay.api.BankCardServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("PAY-SERVICE")
public interface BankCardService extends BankCardServiceApi {
}
