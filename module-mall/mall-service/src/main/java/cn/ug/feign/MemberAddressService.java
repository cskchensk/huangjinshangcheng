package cn.ug.feign;

import cn.ug.hystrix.MemberAddressServiceHystrix;
import cn.ug.member.api.MemberAddressServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="MEMBER-SERVICE", fallback=MemberAddressServiceHystrix.class)
public interface MemberAddressService extends MemberAddressServiceApi {
}
