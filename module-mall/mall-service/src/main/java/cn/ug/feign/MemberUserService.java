package cn.ug.feign;

import cn.ug.member.api.MemberUserServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author zhaohg
 * @date 2018/07/20.
 */
@FeignClient(name = "MEMBER-SERVICE")
//@FeignClient(name = "MEMBER-SERVICE",url = "http://localhost:8102/")
public interface MemberUserService extends MemberUserServiceApi {
}
