package cn.ug.feign;

import cn.ug.pay.api.PayTbillServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

//@FeignClient(value = "PAY-SERVICE",url = "http://localhost:8117/")
@FeignClient(value = "PAY-SERVICE")
public interface PayTbillService extends PayTbillServiceApi {
}
