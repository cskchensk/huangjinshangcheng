package cn.ug.feign;

import cn.ug.pay.api.PayGoldBeanRecordServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("PAY-SERVICE")
public interface PayGoldBeanRecordService extends PayGoldBeanRecordServiceApi {
}
