package cn.ug.feign;

import cn.ug.account.api.VersionServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

//@FeignClient(value = "ACCOUNT-SERVICE",url = "http://localhost:8100/")
@FeignClient(value = "ACCOUNT-SERVICE")
public interface VersionService extends VersionServiceApi {
}
