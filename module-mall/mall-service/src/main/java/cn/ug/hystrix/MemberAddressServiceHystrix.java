package cn.ug.hystrix;

import cn.ug.bean.base.SerializeObject;
import cn.ug.feign.MemberAddressService;
import cn.ug.member.bean.AddressBean;
import cn.ug.member.bean.MemberLogisticsBean;
import cn.ug.member.bean.response.MemberAddressBaseBean;
import cn.ug.member.bean.response.MemberAddressBean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@RequestMapping(value = "/mall/hystrix")
public class MemberAddressServiceHystrix implements MemberAddressService {

    @Override
    public SerializeObject<MemberAddressBaseBean> findByMemberId(String memberId) {
        return null;
    }

    @Override
    public SerializeObject<MemberAddressBean> get(String id) {
        return null;
    }

    @Override
    public SerializeObject<AddressBean> getAddress(long addressId) {
        return null;
    }

    @Override
    public SerializeObject<MemberLogisticsBean> getLogisticsAddress(long addressId) {
        return null;
    }

    @Override
    public SerializeObject<Integer> getAddressNum(String memberId) {
        return null;
    }
}
