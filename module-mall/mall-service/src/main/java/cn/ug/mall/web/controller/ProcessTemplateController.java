package cn.ug.mall.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.service.ProcessTemplateService;
import cn.ug.mall.web.submit.ProcessSearchSubmit;
import cn.ug.mall.web.submit.ProcessSubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 加工费模板
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
@RestController
@RequestMapping("/process")
public class ProcessTemplateController {

    @Autowired
    private ProcessTemplateService processTemplateService;

    /**
     * 添加加工费模板
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject save(@RequestHeader String accessToken, ProcessSubmit submit) {
        return processTemplateService.insert(submit);
    }

    /**
     * 获取加工费模板
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject findById(@RequestHeader String accessToken, @PathVariable("id") Integer id) {
        if (id > 0) {
            return processTemplateService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 更新加工费模板
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject update(@RequestHeader String accessToken, ProcessSubmit submit) {
        if (submit.getId() > 0) {
            return processTemplateService.update(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 加工费模板list
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject list(@RequestHeader String accessToken, ProcessSearchSubmit submit) {
        return processTemplateService.findList(submit);
    }

    /**
     * 获取加工费模板
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/find", method = GET)
    public SerializeObject find(Integer id) {
        if (id > 0) {
            return processTemplateService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

}
