package cn.ug.mall.web.controller;

import cn.ug.bean.Result;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.mall.service.impl.KD100Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/kd")
public class KdController {
    @Resource
    private KD100Service kd100Service;

    @RequestMapping(value = "/queryOrderTrace", method = GET)
    public SerializeObject<Result> queryOrderTrace(String code, String postId){
        return new SerializeObject<>(ResultType.NORMAL, "00000001",kd100Service.queryOrderTrace(code,postId));
    }
}
