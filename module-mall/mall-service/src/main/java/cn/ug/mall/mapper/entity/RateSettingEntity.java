package cn.ug.mall.mapper.entity;

import java.io.Serializable;

/**
 * 费率参数配置
 * 买金买金
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class RateSettingEntity implements Serializable {

    private String keyName;

    private String keyValue;

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }
}

