package cn.ug.mall.web.submit;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public class BrandSubmit {
    private Long   id;
    private String name;
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
