package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.web.submit.BrandSearchSubmit;
import cn.ug.mall.web.submit.BrandSubmit;

/**
 * 品牌
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public interface BrandService {

    /**
     * 添加品牌
     *
     * @param submit
     * @return
     */
    SerializeObject insert(BrandSubmit submit);

    /**
     * 查找品牌
     * @param id
     * @return
     */
    SerializeObject findById(Long id);
    /**
     * 更新品牌
     *
     * @param submit
     * @return
     */
    SerializeObject update(BrandSubmit submit);

    /**
     * 品牌列表
     *
     * @return
     */
    SerializeObject findList(BrandSearchSubmit submit);


    SerializeObject search();

    SerializeObject deleteById(long id);
}
