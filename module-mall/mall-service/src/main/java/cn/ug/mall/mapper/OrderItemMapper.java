package cn.ug.mall.mapper;

import cn.ug.mall.bean.OrderItemGoodsBean;
import cn.ug.mall.mapper.entity.OrderItemEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 订单规格
 * @author zhaohg
 * @date 2018/07/10.
 */
@Mapper
public interface OrderItemMapper {

    int insert(OrderItemEntity entity);

    int update(OrderItemEntity entity);

    OrderItemEntity findById(Long id);

    List<OrderItemEntity> findListByOrderId(@Param("orderId") Long orderId);

    int selectNumByMemberId(@Param("goodsId") Long goodsId, @Param("memberId") String memberId);


    List<OrderItemGoodsBean> getOrderItemGoodsBeanByOrderId(@Param("orderId") long orderId);

    List<String> selectGoodsNames(@Param("orderNO")String orderNO);

    List<OrderItemGoodsBean> getOrderItemGoodsBeanByOrderIds(@Param("ids")List<Long> ids);
}
