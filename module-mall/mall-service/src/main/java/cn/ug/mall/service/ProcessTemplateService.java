package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.web.submit.ProcessSearchSubmit;
import cn.ug.mall.web.submit.ProcessSubmit;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public interface ProcessTemplateService {

    /**
     * 添加加工费模板
     *
     * @param submit
     * @return
     */
    SerializeObject insert(ProcessSubmit submit);

    /**
     * 获取加工费模板
     * @param id
     * @return
     */
    SerializeObject findById(Integer id);

    /**
     * 更新加工费模板
     *
     * @param submit
     * @return
     */
    SerializeObject update(ProcessSubmit submit);

    /**
     * 加工费模板列表
     *
     * @return
     */
    SerializeObject findList(ProcessSearchSubmit submit);

}
