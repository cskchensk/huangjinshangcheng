package cn.ug.mall.mapper;

import cn.ug.mall.bean.GoodsOrderBean;
import cn.ug.mall.bean.MemberGoodsOrderBean;
import cn.ug.mall.bean.MemberGoodsOrderDetailBean;
import cn.ug.mall.mapper.entity.MallGoodsOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MallGoodsOrderMapper {
    List<MemberGoodsOrderBean> queryForListByMemberId(Map<String, Object> params);
    int queryForCountByMemberId(Map<String, Object> params);

    MemberGoodsOrderDetailBean queryForDetailByOrderNO(@Param("orderNO")String orderNO);
    int expiredGoodsOrder(@Param("orderNOs") String[] orderNOs);
    List<MemberGoodsOrderBean> queryForExpiredGoodsOrder();
    int succeedGoodsOrder(@Param("orderNO")String orderNO, @Param("successTime")String successTime);
    int failGoodsOrder(@Param("orderNO")String orderNO, @Param("reason")String reason);
    int addRemark(@Param("orderNO")String orderNO, @Param("remark")String remark);
    int modifyStatus(@Param("orderNO")String orderNO, @Param("status")int status);
    int insert(MallGoodsOrder order);

    List<GoodsOrderBean> query(Map<String, Object> params);
    int count(Map<String, Object> params);
}