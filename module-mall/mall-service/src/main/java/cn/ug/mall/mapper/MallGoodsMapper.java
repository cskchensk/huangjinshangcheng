package cn.ug.mall.mapper;

import cn.ug.mall.bean.MallGoodsBean;
import cn.ug.mall.bean.MallGoodsMobileBean;
import cn.ug.mall.bean.MallGoodsMobileParam;
import cn.ug.mall.bean.MallGoodsParam;
import cn.ug.mall.mapper.entity.MallGoods;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ywl
 * @date 2018-04-18
 */
@Component
public interface MallGoodsMapper  extends BaseMapper<MallGoods>{

    List<MallGoodsBean> findList(MallGoodsParam mallGoodsParam);

    int updateSaleStatus(Map<String,Object> param);

    int updateStock(Map<String,Object> param);

    List<MallGoodsMobileBean> findMallGoodsList(MallGoodsMobileParam mallGoodsMobileParam);

}
