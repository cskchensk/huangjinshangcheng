package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.web.submit.LabelSubmit;

/**
 * @author zhaohg
 * @date 2018/07/12.
 */
public interface LabelService {
    /**
     * 添加标签
     * @param submit
     * @return
     */
    SerializeObject insert(LabelSubmit submit);

    SerializeObject deleteById(int id);

    SerializeObject update(LabelSubmit submit);

    SerializeObject findById(int id);

    SerializeObject findList();

}
