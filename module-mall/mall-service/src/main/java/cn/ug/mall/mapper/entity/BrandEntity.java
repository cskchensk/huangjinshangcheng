package cn.ug.mall.mapper.entity;

import java.util.Date;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public class BrandEntity {

    /**
     * 品牌id
     */
    private Long    id;
    /**
     * 品牌名称
     */
    private String  name;
    /**
     * 品牌备注
     */
    private String  remark;
    /**
     * 是否删除 -1删除 0有效
     */
    private Integer isDel;
    /**
     * 创建时间
     */
    private Date    addTime;
    /**
     * 修改时间
     */
    private Date    modifyTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}
