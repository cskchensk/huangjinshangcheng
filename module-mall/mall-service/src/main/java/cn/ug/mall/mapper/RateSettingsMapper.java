package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.RateSettingEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 费率参数设置
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
@Mapper
public interface RateSettingsMapper {

    /**
     * 添加
     *
     * @param rate
     * @return
     */
    int insert(RateSettingEntity rate);

    /**
     * 更新
     *
     * @param rate
     * @return
     */
    int update(RateSettingEntity rate);

    /**
     * 查找
     *
     * @param keyName
     * @return
     */
    RateSettingEntity findByKeyName(String keyName);

}
