package cn.ug.mall.service;



import cn.ug.mall.bean.GoodsPictureBean;

import java.util.List;

/**
 * 图片管理
 * @author kaiwotech
 */
public interface GoodsPictureService {

	/**
	 * 添加
	 * @param goodsId		产品ID
	 * @param imageList		图片
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(String goodsId, List<String> imageList);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);

	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除通过商品ID
	 * @param goodsId	产品ID
	 * @return		操作影响的行数
	 */
	int deleteByGoodsIds(String goodsId);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	GoodsPictureBean findById(String id);

	/**
	 * 根据商品ID, 查找列表
	 * @param goodsId	商品ID
	 * @return			列表
	 */
	List<GoodsPictureBean> findListByGoodsId(String goodsId);

}
