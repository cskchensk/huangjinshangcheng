package cn.ug.mall.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.mall.bean.GoodsSalesBean;
import cn.ug.mall.mapper.GoodsMapper;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.service.GoodsSalesService;
import cn.ug.mall.web.submit.SalesSearchSubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/18.
 */
@Service
public class GoodsSalesServiceImpl extends BaseServiceImpl implements GoodsSalesService {

    @Resource
    private GoodsMapper     goodsMapper;

    @Override
    public SerializeObject getGoodsSales(SalesSearchSubmit submit) {

        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = goodsMapper.getGoodsSalesCount(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        if (count > 0) {
            List<GoodsSalesBean> goodsList = goodsMapper.getGoodsSalesList(querySubmit);
            return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, goodsList));

        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, null));
    }


}
