package cn.ug.mall.service;

import cn.ug.mall.bean.GoodsOrderBean;
import cn.ug.mall.bean.MemberGoodsOrderBean;
import cn.ug.mall.bean.MemberGoodsOrderDetailBean;
import cn.ug.mall.mapper.entity.MallGoodsOrder;

import java.util.List;

public interface MallGoodsOrderService {
	List<MemberGoodsOrderBean> listRecords(String memberId, int status, int offset, int size);
	int countRecords(String memberId, int status);
	MemberGoodsOrderDetailBean getOrderDetail(String orderNO);
	boolean expiredGoodsOrder(String[] orderNOs);
	List<MemberGoodsOrderBean> listExpiredGoodsOrder();
	boolean addOrder(MallGoodsOrder order);
	boolean succeedGoodsOrder(String orderNO);
	boolean failGoodsOrder(String orderNO, String reason);
	boolean addRemark(String orderNO, String remark);

	List<GoodsOrderBean> listOrders(int status, String orderNO, String userName, String userMobile, String goodsName,
									String startDate, String endDate, int offset, int size);
	int countOrders(int status, String orderNO, String userName, String userMobile, String goodsName,
									String startDate, String endDate);
}