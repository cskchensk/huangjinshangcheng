package cn.ug.mall.mapper.entity;

import java.io.Serializable;

/**
 * 会员等级折扣
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class MemberDiscountEntity implements Serializable {
    /**
     * 会员等级id
     */
    private Integer levelId;
    /**
     * 会员等级
     */
    private String  levelName;
    /**
     * 会员折扣
     * 百分比
     */
    private Integer discount;

    /**
     * 类型 1：买金 2：买金 3：充值 4：提现 5：提金 6：换金
     */
    private Integer type;

    public Integer getLevelId() {
        return levelId;
    }

    public void setLevelId(Integer levelId) {
        this.levelId = levelId;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
