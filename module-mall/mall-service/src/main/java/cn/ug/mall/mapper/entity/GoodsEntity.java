package cn.ug.mall.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品
 *
 * @author zhaohg
 * @date 2018/07/05.
 */
public class GoodsEntity implements Serializable {

    public static final int SHELVE_UP   = 2;
    public static final int SHELVE_DOWN = 3;

    public static final int        AUDIT_PASS   = 2;
    public static final int        AUDIT_REJECT = 3;
    /**
     * 商品id
     */
    private             Long       id;
    /**
     * 商品名称
     */
    private             String     name;
    /**
     * 副标题
     */
    private             String     title;
    /**
     * 标签
     */
    private             String     label;
    /**
     * 品牌id
     */
    private             Long       brandId;
    private             String     brandName;
    /**
     * 分类id
     */
    private             Integer    categoryId;
    private             String     categoryName;
    /**
     * 加工模板
     */
    private             Integer    processId;
    private             String     processName;
    /**
     * 加工费
     */
    private             BigDecimal processCost;
    /**
     * 商品类型         1:金条 2:金饰
     */
    private             Integer    type;
    /**
     * 商品编号
     */
    private             String     goodsCode;
    /**
     * 商品库存
     */
    private             Integer    stock;
    /**
     * 商品图
     */
    private             String     imgUrl;
    /**
     * 商品品质
     */
    private             String     quality;
    /**
     * 最低价格
     */
    private             BigDecimal minPrice;
    /**
     * 最高价格
     */
    private             BigDecimal maxPrice;
    /**
     * 市场最低价
     */
    private             BigDecimal minMarketPrice;
    /**
     * 市场最高价
     */
    private             BigDecimal maxMarketPrice;
    /**
     * 最小克重
     */
    private             BigDecimal minGram;
    /**
     * 最小克重
     */
    private             BigDecimal maxGram;
    /**
     * 是否限购 0:不限 1:限购数量 2:限购克重
     */
    private             Integer    isQuota;
    /**
     * 限购数量
     */
    private             Integer    quotaNum;
    /**
     * 限购克重
     */
    private             BigDecimal quotaWeight;
    /**
     * 限购时间 0:不限 1:限购
     */
    private             Integer    isPrompt;
    /**
     * 限购开始时间
     */
    private             Date       startPrompt;
    /**
     * 限购结束时间
     */
    private             Date       endPrompt;
    /**
     * 操作人
     */
    private             String     operatorId;
    /**
     * 审核人
     */
    private             String     auditorId;
    /**
     * 审核状态 1:待审核 2：审核通过 3:审核未通过
     */
    private             Integer    auditType;
    /**
     * 上下架状态 1: 待上架,2:上架，3:下架
     */
    private             Integer    shelveType;
    /**
     * 上架时间
     */
    private             Date       shelveTime;
    /**
     * 下架时间
     */
    private             Date       unShelveTime;
    /**
     * 下架备注
     */
    private             String     downRemark;
    /**
     * 销量
     */
    private             Long       sales;
    /**
     * 用户是否删除 0:有效，1:无效
     */
    private             Integer    isDel;
    /**
     * 购买须知
     */
    private             String     notice;
    /**
     * 保养提示
     */
    private             String     hint;
    /**
     * 是否定时上架
     */
    private             Integer    isUp;
    /**
     * 定时上架时间
     */
    private             Date       upTime;
    /**
     * 是否推荐商品 0：否，1：是
     */
    private             Integer    isRecommend;
    /**
     * 修改时间
     */
    private             Date       modifyTime;
    /**
     * 添加时间
     */
    private             Date       addTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinMarketPrice() {
        return minMarketPrice;
    }

    public void setMinMarketPrice(BigDecimal minMarketPrice) {
        this.minMarketPrice = minMarketPrice;
    }

    public BigDecimal getMaxMarketPrice() {
        return maxMarketPrice;
    }

    public void setMaxMarketPrice(BigDecimal maxMarketPrice) {
        this.maxMarketPrice = maxMarketPrice;
    }

    public BigDecimal getMinGram() {
        return minGram;
    }

    public void setMinGram(BigDecimal minGram) {
        this.minGram = minGram;
    }

    public BigDecimal getMaxGram() {
        return maxGram;
    }

    public void setMaxGram(BigDecimal maxGram) {
        this.maxGram = maxGram;
    }

    public Integer getIsQuota() {
        return isQuota;
    }

    public void setIsQuota(Integer isQuota) {
        this.isQuota = isQuota;
    }

    public Integer getQuotaNum() {
        return quotaNum;
    }

    public void setQuotaNum(Integer quotaNum) {
        this.quotaNum = quotaNum;
    }

    public BigDecimal getQuotaWeight() {
        return quotaWeight;
    }

    public void setQuotaWeight(BigDecimal quotaWeight) {
        this.quotaWeight = quotaWeight;
    }

    public Integer getIsPrompt() {
        return isPrompt;
    }

    public void setIsPrompt(Integer isPrompt) {
        this.isPrompt = isPrompt;
    }

    public Date getStartPrompt() {
        return startPrompt;
    }

    public void setStartPrompt(Date startPrompt) {
        this.startPrompt = startPrompt;
    }

    public Date getEndPrompt() {
        return endPrompt;
    }

    public void setEndPrompt(Date endPrompt) {
        this.endPrompt = endPrompt;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(String auditorId) {
        this.auditorId = auditorId;
    }

    public Integer getAuditType() {
        return auditType;
    }

    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    public Integer getShelveType() {
        return shelveType;
    }

    public void setShelveType(Integer shelveType) {
        this.shelveType = shelveType;
    }

    public Date getShelveTime() {
        return shelveTime;
    }

    public void setShelveTime(Date shelveTime) {
        this.shelveTime = shelveTime;
    }

    public Date getUnShelveTime() {
        return unShelveTime;
    }

    public void setUnShelveTime(Date unShelveTime) {
        this.unShelveTime = unShelveTime;
    }

    public String getDownRemark() {
        return downRemark;
    }

    public void setDownRemark(String downRemark) {
        this.downRemark = downRemark;
    }

    public Long getSales() {
        return sales;
    }

    public void setSales(Long sales) {
        this.sales = sales;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Integer getIsUp() {
        return isUp;
    }

    public void setIsUp(Integer isUp) {
        this.isUp = isUp;
    }

    public Date getUpTime() {
        return upTime;
    }

    public void setUpTime(Date upTime) {
        this.upTime = upTime;
    }

    public Integer getIsRecommend() {
        return isRecommend;
    }

    public void setIsRecommend(Integer isRecommend) {
        this.isRecommend = isRecommend;
    }
}
