package cn.ug.mall.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.enums.PayTypeEnum;
import cn.ug.mall.bean.CategoryBean;
import cn.ug.mall.bean.FeeBean;
import cn.ug.mall.bean.GoodsBean;
import cn.ug.mall.bean.GoodsSkuBean;
import cn.ug.mall.mapper.entity.OtherSubmit;
import cn.ug.mall.service.CategoryService;
import cn.ug.mall.service.GoodsSalesService;
import cn.ug.mall.service.GoodsService;
import cn.ug.mall.web.submit.GoodsSearchSubmit;
import cn.ug.mall.web.submit.GoodsSkuSubmit;
import cn.ug.mall.web.submit.GoodsSubmit;
import cn.ug.mall.web.submit.SalesSearchSubmit;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 商品管理
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @Autowired
    private GoodsSalesService goodsSalesService;

    @Autowired
    private CategoryService categoryService;

    /**
     * 添加商品
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject save(@RequestHeader String accessToken, GoodsSubmit submit) {
        submit.checkAndReduce();
        return goodsService.insert(submit);
    }

    @GetMapping("/sku")
    public SerializeObject<GoodsSkuBean> getSku(long skuId) {
        GoodsSkuBean entity = goodsService.getGoodsSku(skuId);
        return new SerializeObject<GoodsSkuBean>(ResultType.NORMAL, "00000001", entity);
    }

    @GetMapping("/fee")
    public SerializeObject<FeeBean> getFee(long skuId, int quantity, int type) {
        FeeBean entity = goodsService.getFee(skuId, quantity, type);
        return new SerializeObject<FeeBean>(ResultType.NORMAL, "00000001", entity);
    }

    /**
     * 更新商品
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject update(@RequestHeader String accessToken, GoodsSubmit submit) {
        submit.checkAndReduce();
        return goodsService.update(submit);
    }

    /**
     * 查找商品
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject findById(@RequestHeader String accessToken, @PathVariable("id") Long id) {
        if (id > 0) {
            return goodsService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 删除商品
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete/{id}", method = GET)
    public SerializeObject delete(@RequestHeader String accessToken, @PathVariable("id") Long id) {
        if (id > 0) {
            return goodsService.delete(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 商品上下架列表
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/shelve/list", method = GET)
    public SerializeObject findList(@RequestHeader String accessToken, GoodsSearchSubmit submit) {
        submit.setAuditType(2);//审核通过
        return goodsService.findList(submit);
    }

    /**
     * 上架 立即上架 定时上架
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/shelve/update", method = POST)
    public SerializeObject updateShelve(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit.getId() > 0) {
            return goodsService.updateShelveType(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 下架备注
     */
    @RequestMapping(value = "/down/remark", method = POST)
    public SerializeObject downGoods(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit.getId() > 0) {
            return goodsService.downGoods(submit);
        }
        return new SerializeObjectError("00000005");

    }

    /**
     *设置商品推荐(1是，0否)
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/recommend/update", method = POST)
    public SerializeObject updateRecommend(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit == null ||submit.getId() == null || submit.getIsRecommend() == null
                || (submit.getIsRecommend() != 0 && submit.getIsRecommend() != 1)) {
            return new SerializeObjectError("00000002");
        }
        return goodsService.updateRecommend(submit);
    }

    /**
     * 审核商品列表 待审核列表
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/audit/list", method = GET)
    public SerializeObject findAuditList(@RequestHeader String accessToken, GoodsSearchSubmit submit) {
        return goodsService.findList(submit);
    }

    /**
     * 未审核列表
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/unAudit/list", method = GET)
    public SerializeObject unAuditList(@RequestHeader String accessToken, GoodsSearchSubmit submit) {
        submit.setAuditType(1);
        return goodsService.findList(submit);
    }

    /**
     * 提交审核
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/audit/update", method = POST)
    public SerializeObject updateAudit(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit.getGoodsId() > 0) {
            return goodsService.updateAuditType(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 查商品规格
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/sku/list", method = GET)
    public SerializeObject updateStock(@RequestHeader String accessToken, Long id) {
        return goodsService.findSkuList(id);
    }

    /**
     * 更新商品库存
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/stock/update", method = POST)
    public SerializeObject updateStock(@RequestHeader String accessToken, GoodsSkuSubmit submit) {
        if (submit.getGoodsId() > 0) {
            return goodsService.updateStock(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 商品审核详细列表
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/audit/detail", method = POST)
    public SerializeObject auditDetailList(@RequestHeader String accessToken, Long id) {
        if (id > 0) {
            return goodsService.auditDetailListByGoodsId(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 商品销量统计
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/sales", method = GET)
    public SerializeObject getGoodsSales(@RequestHeader String accessToken, SalesSearchSubmit submit) {
        return goodsSalesService.getGoodsSales(submit);
    }


    /**
     * 提金/换金验证是否可支付
     * type 4：提金  5：换金
     * usableAmount: 用户可用黄金克重
     */
    @PostMapping(value = "/validation/payable")
    public SerializeObject validate(Long skuId, int quantity, String memberId, int type) {
        if (skuId == null) {
            return new SerializeObjectError("20300200");
        }
        if (quantity <= 0 || StringUtils.isBlank(memberId) || (type != PayTypeEnum.GOLD_EXTRACTION.getType() && type != PayTypeEnum.GOLD_CHANGE.getType())) {
            return new SerializeObjectError("00000002");
        }
        return goodsService.validateGoods(skuId, quantity, memberId, type);
    }

    /** ******* 移动端接口 ***/
    /**
     * 商品详细
     *
     * @return
     */
    @RequestMapping(value = "/{id}", method = GET)
    public SerializeObject getGoodsById(@PathVariable("id") Long id) {
        if (id > 0) {
            return goodsService.getGoodsById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 商品list-app端接口（3.5.0以前）
     *
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject getGoodsList(GoodsSearchSubmit submit) {
        return goodsService.getGoodsList(submit);
    }

    /**
     *商品list-app端接口（推荐列表，3.5.0）
     * @param submit
     * @return
     */
    @RequestMapping(value = "/recommendList", method = GET)
    public SerializeObject getRecommendGoods(GoodsSearchSubmit submit) {
        List resultList = new ArrayList();
        SerializeObject categoryData  = categoryService.findList();
        List<CategoryBean> categoryBeans = (List<CategoryBean>)categoryData.getData();
        if (!CollectionUtils.isEmpty(categoryBeans)) {
            for (CategoryBean categoryBean : categoryBeans) {
                submit.setCategoryId(categoryBean.getId().intValue());
                SerializeObject data = goodsService.getRecommendGoods(submit);
                List<GoodsBean> list = (List<GoodsBean>)data.getData();
                if (!CollectionUtils.isEmpty(list)) {
                    Map map = new HashMap();
                    map.put("categoryName", categoryBean.getName());
                    map.put("categoryGoods", list);
                    resultList.add(map);
                }
            }
        }
        return new SerializeObject(ResultType.NORMAL,resultList);
    }

    /**
     * 获取某类型的商品-app端接口
     * @param submit
     * @return
     */
    @RequestMapping(value = "/categoryList", method = GET)
    public SerializeObject getCategoryGoods(GoodsSearchSubmit submit) {
        return goodsService.getCategoryGoods(submit);
    }
}
