package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.PutGoldBean;
import cn.ug.mall.web.submit.PutGoldSubmit;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/20.
 */
public interface PutGoldService {

    SerializeObject insert(PutGoldSubmit submit);


    SerializeObject putGoldList(PutGoldSubmit submit);


    List<PutGoldBean> export(PutGoldSubmit submit);
}
