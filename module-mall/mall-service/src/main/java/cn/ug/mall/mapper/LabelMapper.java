package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.LabelEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/12.
 */
@Mapper
public interface LabelMapper {

    int insert(LabelEntity entity);


    int update(LabelEntity entity);

    int deleteById(Integer id);


    LabelEntity findById(Integer id);

    List<LabelEntity> findList();


    List<LabelEntity> findListByIds(@Param("ids") List<String> ids);
}
