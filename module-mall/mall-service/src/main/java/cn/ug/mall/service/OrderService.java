package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.OrderBean;
import cn.ug.mall.mapper.entity.OtherSubmit;
import cn.ug.mall.web.submit.OrderSearchSubmit;
import cn.ug.mall.web.submit.OrderSubmit;
import com.alibaba.fastjson.JSONArray;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/10.
 */
public interface OrderService {

    /**
     * 订单添加
     * @param submit
     * @return
     */
    SerializeObject insert(OrderSubmit submit, long skuId, int quantity);

    /**
     * 订单
     * @param id
     * @return
     */
    SerializeObject findById(Long id);

    SerializeObject findBySerial(String serial);
    /**
     * 订单列表
     * @param submit
     * @return
     */
    SerializeObject findList(OrderSearchSubmit submit);

    /**
     * 关闭订单及关闭原因
     * @param submit
     * @return
     */
    SerializeObject closeOrder(OtherSubmit submit);

    /**
     * 查询订单备注
     * @param orderId
     * @return
     */
    SerializeObject findRemark(long orderId);

    /**
     * 备注订单
     * @param submit
     * @return
     */
    SerializeObject remarkOrder(OtherSubmit submit);

    /**
     * 订单列表 (move)
     * @param searchSubmit
     * @return
     */
    SerializeObject getOrderList(OrderSearchSubmit searchSubmit);

    /**
     * 订单发货
     * @param submit
     * @return
     */
    SerializeObject orderDelivery(OtherSubmit submit);

    /**
     * 订单跟踪
     * @param id
     * @return
     */
    SerializeObject orderTrack(long id);

    boolean failOrder(String orderNO, String closeRemark);
    OrderBean getOrder(String orderNO);

    boolean succeedOrder(String orderNO);
    boolean handlingOrder(String orderNO);

    SerializeObject pay(JSONArray infos, long addressId, int source, String memberId, String cellphone, int couponId);

    boolean changeAddress(String orderNO, long addressId);

    SerializeObject validateOrder(String orderNO);


    List<String> selectGoodsNames(String orderNO);

    boolean updateNeedCloseOrder();

    boolean read(String orderNO);
    int countUnreadNum(String memberId, int status);
    int countSuccessfulNum(String memberId);
}
