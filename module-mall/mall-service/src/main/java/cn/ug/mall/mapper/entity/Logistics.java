package cn.ug.mall.mapper.entity;


import cn.ug.mapper.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * 物流信息
 * @author kaiwotech
 */
public class Logistics extends BaseEntity implements java.io.Serializable {

	/** 订单号 */
	private String orderNo;
	/** 物流单号 */
	private String serialNo;
	/** 物流公司 */
	private String company;
	/** 物流公司联系电话 */
	private String phone;
	/** 收货时间 */
	private LocalDateTime receiveTime;
	/** 状态:1:待收货；2：已收货 */
	private int status;

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(LocalDateTime receiveTime) {
		this.receiveTime = receiveTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
