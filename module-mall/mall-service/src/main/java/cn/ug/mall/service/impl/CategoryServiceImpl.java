package cn.ug.mall.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.bean.CategoryBean;
import cn.ug.mall.mapper.CategoryMapper;
import cn.ug.mall.mapper.entity.CategoryEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.service.CategoryService;
import cn.ug.mall.web.submit.CategorySearchSubmit;
import cn.ug.mall.web.submit.CategorySubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Service
public class CategoryServiceImpl extends BaseServiceImpl implements CategoryService {

    @Resource
    private CategoryMapper categoryMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public SerializeObject insert(CategorySubmit submit) {
        //todo 判断
        CategoryEntity entity = dozerBeanMapper.map(submit, CategoryEntity.class);
        int success = categoryMapper.insert(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findById(Integer id) {
        //todo 判断
        CategoryEntity entity = categoryMapper.findById(id);
        if (entity != null) {
            CategoryBean bean = dozerBeanMapper.map(entity, CategoryBean.class);
            return new SerializeObject<>(ResultType.NORMAL, "00000001", bean);
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject update(CategorySubmit submit) {
        //todo 判断
        CategoryEntity entity = dozerBeanMapper.map(submit, CategoryEntity.class);
        int success = categoryMapper.update(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findList(CategorySearchSubmit submit) {

        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = categoryMapper.count(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<CategoryBean> list = new ArrayList<>(count);
        if (count > 0) {
            List<CategoryEntity> categoryList = categoryMapper.findList(querySubmit);
            for (CategoryEntity entity : categoryList) {
                CategoryBean bean = dozerBeanMapper.map(entity, CategoryBean.class);
                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public SerializeObject findList() {

        List<CategoryBean> list = new ArrayList<>();
        List<CategoryEntity> categoryList = categoryMapper.searchList();
        if(categoryList != null) {
            for (CategoryEntity entity : categoryList) {
                CategoryBean bean = dozerBeanMapper.map(entity, CategoryBean.class);
                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001",  list);
    }
}
