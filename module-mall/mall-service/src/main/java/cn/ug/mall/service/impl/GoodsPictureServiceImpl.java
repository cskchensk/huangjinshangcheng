package cn.ug.mall.service.impl;

import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.core.ensure.Ensure;
import cn.ug.mall.bean.GoodsPictureBean;
import cn.ug.mall.mapper.GoodsPictureMapper;
import cn.ug.mall.mapper.entity.GoodsPicture;
import cn.ug.mall.service.GoodsPictureService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class GoodsPictureServiceImpl extends BaseServiceImpl implements GoodsPictureService {
	
	@Resource
	private GoodsPictureMapper goodsPictureMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(String goodsId, List<String> imageList) {
		// 数据完整性校验
		if(StringUtils.isBlank(goodsId)) {
			Ensure.that(true).isTrue("00000002");
		}
		// 清除老数据
		deleteByGoodsIds(goodsId);
		// 添加新数据
		if(null != imageList && !imageList.isEmpty()) {
			int index = imageList.size();
			for (String imgUrl : imageList) {
				GoodsPicture goodsPicture = new GoodsPicture();
				goodsPicture.setId(UF.getRandomUUID());
				goodsPicture.setGoodsId(goodsId);
				goodsPicture.setSort(index);
				goodsPicture.setImgUrl(imgUrl);

				int rows = goodsPictureMapper.insert(goodsPicture);

				index--;
			}
		}

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return goodsPictureMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return goodsPictureMapper.deleteByIds(id);
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public GoodsPictureBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		GoodsPicture entity = goodsPictureMapper.findById(id);
		if(null == entity) {
			return null;
		}

		GoodsPictureBean entityBean = dozerBeanMapper.map(entity, GoodsPictureBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@RemoveCache(cleanAll = true)
	public int deleteByGoodsIds(String goodsId) {
		if(StringUtils.isBlank(goodsId)) {
			return 0;
		}
		String[] ids = {goodsId};
		return goodsPictureMapper.deleteByGoodsIds(ids);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<GoodsPictureBean> findListByGoodsId(String goodsId) {
		return query(goodsId);
	}

	/**
	 * 获取数据列表
     * @param goodsId 		所属商品ID
	 * @return				列表
	 */
	private List<GoodsPictureBean> query(String goodsId){
		List<GoodsPictureBean> dataList = new ArrayList<>();
		List<GoodsPicture> list = goodsPictureMapper.query(
				getParams()
						.put("goodsId", goodsId)
						.toMap());
		for (GoodsPicture o : list) {
			GoodsPictureBean objBean = dozerBeanMapper.map(o, GoodsPictureBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}

}

