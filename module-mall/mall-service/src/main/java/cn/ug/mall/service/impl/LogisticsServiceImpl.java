package cn.ug.mall.service.impl;

import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.core.ensure.Ensure;
import cn.ug.mall.bean.LogisticsBean;
import cn.ug.mall.mapper.LogisticsMapper;
import cn.ug.mall.mapper.MallGoodsOrderMapper;
import cn.ug.mall.mapper.entity.Logistics;
import cn.ug.mall.service.LogisticsService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.SerialNumberWorker;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class LogisticsServiceImpl extends BaseServiceImpl implements LogisticsService {
	
	@Resource
	private LogisticsMapper logisticsMapper;
	@Autowired
	private MallGoodsOrderMapper mallGoodsOrderMapper;
	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(LogisticsBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getOrderNo())) {
			Ensure.that(true).isTrue("00000002");
		}
		Logistics entity = dozerBeanMapper.map(entityBean, Logistics.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		int rows = logisticsMapper.insert(entity);
		Ensure.that(rows).isLt(1,"00000005");
		int result =  mallGoodsOrderMapper.modifyStatus(entityBean.getOrderNo(), 3);
		Ensure.that(result).isLt(1,"00000005");
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return logisticsMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return logisticsMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanAll = true)
	public void receive(String orderNo) {
		if(StringUtils.isBlank(orderNo)){
			Ensure.that(true).isTrue("00000002");
		}
		LogisticsBean logisticsBean = findByOrderNo(orderNo);
		if(null == logisticsBean){
			Ensure.that(true).isTrue("00000002");
		}

		String[] id = {logisticsBean.getId()};

		int rows = logisticsMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("receiveTime", UF.getDateTime())
						.put("status", 2)
						.toMap()
		);
		Ensure.that(rows).isLt(1,"00000002");
		int result =  mallGoodsOrderMapper.modifyStatus(orderNo, 4);
		Ensure.that(result).isLt(1,"00000005");
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public LogisticsBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		Logistics entity = logisticsMapper.findById(id);
		if(null == entity) {
			return null;
		}

		LogisticsBean entityBean = dozerBeanMapper.map(entity, LogisticsBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		entityBean.setReceiveTimeString(UF.getFormatDateTime(entity.getReceiveTime()));
		return entityBean;
	}

	@Override
	@RemoveCache(cleanAll = true)
	public int deleteByOrderNo(String orderNo) {
		if(StringUtils.isBlank(orderNo)) {
			return 0;
		}
		String[] ids = {orderNo};
		return logisticsMapper.deleteByOrderNo(ids);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public LogisticsBean findByOrderNo(String orderNo) {
		List<LogisticsBean> list = query(orderNo);
		if(null == list || list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}

	/**
	 *
	 * 获取数据列表
     * @param orderNo 		订单号
	 * @return				列表
	 */
	private List<LogisticsBean> query(String orderNo){
		List<LogisticsBean> dataList = new ArrayList<>();
		List<Logistics> list = logisticsMapper.query(
				getParams(null, null, null)
						.put("orderNo", orderNo)
						.toMap());
		for (Logistics o : list) {
			LogisticsBean objBean = dozerBeanMapper.map(o, LogisticsBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			objBean.setReceiveTimeString(UF.getFormatDateTime(o.getReceiveTime()));
			dataList.add(objBean);
		}
		return dataList;
	}

}

