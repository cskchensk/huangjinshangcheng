package cn.ug.mall.web.submit;

import cn.ug.mall.bean.EntityGoldBean;
import cn.ug.mall.bean.LeaseDay;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 费率设置bean
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class RateSettingSubmit implements Serializable {

    /**
     * 最大提金克重
     */
    private Integer takeMaxGram;
    /**
     * 是否有会员折扣
     */
    private Integer takeIsDiscount;
    /**
     * 会员折扣
     */
    private Integer takeDiscount;

    private Integer inMaxGram;
    private Integer inIsDiscount;
    private Integer inDiscount;

    private BigDecimal fee;
    /**最小充值金额或最小提现金额
     * */

    private BigDecimal minAmount;
    /**提现到账时间
     * */
    private String doneTime;
    /**是否收手续费0：不收；1：收
     * */
    private int isFee;
    /**收费方式1：按固定费用收费 单位(元)，2：按费率扣除 千分比
     * */
    private int feeWay;
    /**收费周期：1：日；2：月
     * */
    private int feePeriod;
    /**前几笔
     * */
    private int beforeNum;
    /**前几笔收费或费率
     * */
    private BigDecimal beforeAmount;
    /**之后收费或费率
     * */
    private BigDecimal afterAmount;
    /**
     * 是否有会员折扣：0--没有；1--有
     * */
    private int hasDiscount;
    /**会员折扣
     * */
    private BigDecimal discount;

    /**
     * 每日最大购买克重
     */
    private Integer buyMaxGramPerDay;
    /**
     * 每日最大卖出克重
     */
    private Integer sellMaxGramPerDay;
    /**
     * 待支付截止时间
     */
    private Integer unpaidTime;
    /**
     * 是否限制登录密码或交易密码:0--不限制；1--限制
     * */
    private int isLimited;
    /**
     * 错误次数
     * */
    private int wrongTimes;
    /**
     * 限制时间
     * */
    private int minutes;

    /**
     * 买金状态是否启用 0不启用  1启用
     */
    private Integer buyMaxGramStatus;
    /**
     * 卖金状态是否启用 0不启用  1启用
     */
    private Integer sellMaxGramStatus;

    /**
     * 实物金参数配置
     */
    private String entityGoldSubmit;

    /**
     * 结算方式  1.以金额发放    2.以金豆发放
     */
    private Integer closeType;

    /**
     * 回租期限  key-value
     */
    private List<LeaseDay> leaseDayList;

    /**
     * 买金优惠
     */
    private String goldPrivilegeBean;

    /**
     * 金豆设置基础bean
     */
    private String goldBean;
    /**
     * 金豆发放基础bean
     */
    private String GoldBeanProvide;
    /**
     * 金豆发放营销bean
     */
    private String goldBeanProvideMarket;

    /**
     * 对公转账bean字段
     */
    private String broughtToAccount;

    /**
     * 折扣金配置
     */
    private String discountGold;

    public int getHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(int hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public int getIsLimited() {
        return isLimited;
    }

    public void setIsLimited(int isLimited) {
        this.isLimited = isLimited;
    }

    public int getWrongTimes() {
        return wrongTimes;
    }

    public void setWrongTimes(int wrongTimes) {
        this.wrongTimes = wrongTimes;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public Integer getTakeMaxGram() {
        return takeMaxGram;
    }

    public void setTakeMaxGram(Integer takeMaxGram) {
        this.takeMaxGram = takeMaxGram;
    }

    public Integer getTakeIsDiscount() {
        return takeIsDiscount;
    }

    public void setTakeIsDiscount(Integer takeIsDiscount) {
        this.takeIsDiscount = takeIsDiscount;
    }

    public Integer getTakeDiscount() {
        return takeDiscount;
    }

    public void setTakeDiscount(Integer takeDiscount) {
        this.takeDiscount = takeDiscount;
    }

    public Integer getInMaxGram() {
        return inMaxGram;
    }

    public void setInMaxGram(Integer inMaxGram) {
        this.inMaxGram = inMaxGram;
    }

    public Integer getInIsDiscount() {
        return inIsDiscount;
    }

    public void setInIsDiscount(Integer inIsDiscount) {
        this.inIsDiscount = inIsDiscount;
    }

    public Integer getInDiscount() {
        return inDiscount;
    }

    public void setInDiscount(Integer inDiscount) {
        this.inDiscount = inDiscount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public String getDoneTime() {
        return doneTime;
    }

    public void setDoneTime(String doneTime) {
        this.doneTime = doneTime;
    }

    public int getIsFee() {
        return isFee;
    }

    public void setIsFee(int isFee) {
        this.isFee = isFee;
    }

    public int getFeeWay() {
        return feeWay;
    }

    public void setFeeWay(int feeWay) {
        this.feeWay = feeWay;
    }

    public int getFeePeriod() {
        return feePeriod;
    }

    public void setFeePeriod(int feePeriod) {
        this.feePeriod = feePeriod;
    }

    public int getBeforeNum() {
        return beforeNum;
    }

    public void setBeforeNum(int beforeNum) {
        this.beforeNum = beforeNum;
    }

    public BigDecimal getBeforeAmount() {
        return beforeAmount;
    }

    public void setBeforeAmount(BigDecimal beforeAmount) {
        this.beforeAmount = beforeAmount;
    }

    public BigDecimal getAfterAmount() {
        return afterAmount;
    }

    public void setAfterAmount(BigDecimal afterAmount) {
        this.afterAmount = afterAmount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Integer getBuyMaxGramPerDay() {
        return buyMaxGramPerDay;
    }

    public void setBuyMaxGramPerDay(Integer buyMaxGramPerDay) {
        this.buyMaxGramPerDay = buyMaxGramPerDay;
    }

    public Integer getSellMaxGramPerDay() {
        return sellMaxGramPerDay;
    }

    public void setSellMaxGramPerDay(Integer sellMaxGramPerDay) {
        this.sellMaxGramPerDay = sellMaxGramPerDay;
    }

    public Integer getUnpaidTime() {
        return unpaidTime;
    }

    public void setUnpaidTime(Integer unpaidTime) {
        this.unpaidTime = unpaidTime;
    }

    public Integer getBuyMaxGramStatus() {
        return buyMaxGramStatus;
    }

    public void setBuyMaxGramStatus(Integer buyMaxGramStatus) {
        this.buyMaxGramStatus = buyMaxGramStatus;
    }

    public Integer getSellMaxGramStatus() {
        return sellMaxGramStatus;
    }

    public void setSellMaxGramStatus(Integer sellMaxGramStatus) {
        this.sellMaxGramStatus = sellMaxGramStatus;
    }

    public String getEntityGoldSubmit() {
        return entityGoldSubmit;
    }

    public void setEntityGoldSubmit(String entityGoldSubmit) {
        this.entityGoldSubmit = entityGoldSubmit;
    }

    public Integer getCloseType() {
        return closeType;
    }

    public void setCloseType(Integer closeType) {
        this.closeType = closeType;
    }

    public List<LeaseDay> getLeaseDayList() {
        return leaseDayList;
    }

    public void setLeaseDayList(List<LeaseDay> leaseDayList) {
        this.leaseDayList = leaseDayList;
    }

    public String getGoldPrivilegeBean() {
        return goldPrivilegeBean;
    }

    public void setGoldPrivilegeBean(String goldPrivilegeBean) {
        this.goldPrivilegeBean = goldPrivilegeBean;
    }

    public String getGoldBean() {
        return goldBean;
    }

    public void setGoldBean(String goldBean) {
        this.goldBean = goldBean;
    }

    public String getGoldBeanProvide() {
        return GoldBeanProvide;
    }

    public void setGoldBeanProvide(String goldBeanProvide) {
        GoldBeanProvide = goldBeanProvide;
    }

    public String getGoldBeanProvideMarket() {
        return goldBeanProvideMarket;
    }

    public void setGoldBeanProvideMarket(String goldBeanProvideMarket) {
        this.goldBeanProvideMarket = goldBeanProvideMarket;
    }

    public String getBroughtToAccount() {
        return broughtToAccount;
    }

    public void setBroughtToAccount(String broughtToAccount) {
        this.broughtToAccount = broughtToAccount;
    }

    public String getDiscountGold() {
        return discountGold;
    }

    public void setDiscountGold(String discountGold) {
        this.discountGold = discountGold;
    }
}
