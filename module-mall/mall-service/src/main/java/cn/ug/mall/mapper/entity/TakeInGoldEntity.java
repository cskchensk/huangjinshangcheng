package cn.ug.mall.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zhaohg
 * @date 2018/07/20.
 */
public class TakeInGoldEntity implements Serializable {

    private String userId;
    private String mobile;
    private String realName;
    private String idCard;
    private BigDecimal totalTakeGram;
    private BigDecimal totalInGram;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public BigDecimal getTotalTakeGram() {
        return totalTakeGram;
    }

    public void setTotalTakeGram(BigDecimal totalTakeGram) {
        this.totalTakeGram = totalTakeGram;
    }

    public BigDecimal getTotalInGram() {
        return totalInGram;
    }

    public void setTotalInGram(BigDecimal totalInGram) {
        this.totalInGram = totalInGram;
    }
}
