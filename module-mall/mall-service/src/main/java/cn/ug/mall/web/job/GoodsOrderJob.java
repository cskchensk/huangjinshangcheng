package cn.ug.mall.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.mall.bean.MemberGoodsOrderBean;
import cn.ug.mall.service.GoodsService;
import cn.ug.mall.service.MallGoodsOrderService;
import cn.ug.mall.service.MallGoodsService;
import cn.ug.util.UF;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class GoodsOrderJob {
    private Log log = LogFactory.getLog(GoodsOrderJob.class);
    @Autowired
    private MallGoodsOrderService mallGoodsOrderService;
    @Autowired
    private MallGoodsService mallGoodsService;
    @Autowired
    private RedisGlobalLock redisGlobalLock;

    @Autowired
    private GoodsService goodsService;

    @Scheduled(cron = "0 */1 * * * ?")
    public void expiredGoodsOrder() {
        String key = "GoodsOrderJob:expiredGoodsOrder:" + UF.getFormatDateTime("yyyy-MM-dd HH:mm", UF.getDateTime());
        try {
            if(!redisGlobalLock.lock(key, 1, TimeUnit.HOURS)) {
                return;
            }
            List<MemberGoodsOrderBean> beans = mallGoodsOrderService.listExpiredGoodsOrder();
            if (beans != null && beans.size() > 0) {
                List<String> orderNOsList = new ArrayList<String>();
                Map<String, Integer> goodsStock = new HashMap<String, Integer>();
                for (MemberGoodsOrderBean bean : beans) {
                    orderNOsList.add(bean.getOrderNO());
                    if (goodsStock.get(bean.getGoodsId()) != null) {
                        goodsStock.put(bean.getGoodsId(), goodsStock.get(bean.getGoodsId())+1);
                    } else {
                        goodsStock.put(bean.getGoodsId(), 1);
                    }
                }
                boolean result = mallGoodsOrderService.expiredGoodsOrder(orderNOsList.toArray(new String[orderNOsList.size()]));
                if (result) {
                    for(String goodsId : goodsStock.keySet()){
                        mallGoodsService.updateStock(goodsId, goodsStock.get(goodsId), 1);
                    }
                }
                log.info("处理过期商城订单[result = " + result + "]");
            }
        } finally {
            redisGlobalLock.unlock(key);
        }
    }

    /**
     * 商品定时上架
     */
    @Scheduled(cron = "0 */1 * * * ?")
    public void shelvesGoods() {
        String key = "GoodsShelvesJob:shelvesGoods:" + UF.getFormatDateTime("yyyy-MM-dd HH:mm", UF.getDateTime());
        try {
            if(!redisGlobalLock.lock(key, 1, TimeUnit.HOURS)) {
                return;
            }
            int result = goodsService.updateNeedShelvesGoods();
            log.info("处理商品定时上架[result = " + result + "]");

        } finally {
            redisGlobalLock.unlock(key);
        }
    }
}