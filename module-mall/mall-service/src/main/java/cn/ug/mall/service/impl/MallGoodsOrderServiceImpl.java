package cn.ug.mall.service.impl;

import cn.ug.core.ensure.Ensure;
import cn.ug.mall.bean.GoodsOrderBean;
import cn.ug.mall.bean.MemberGoodsOrderBean;
import cn.ug.mall.bean.MemberGoodsOrderDetailBean;
import cn.ug.mall.mapper.MallGoodsMapper;
import cn.ug.mall.mapper.MallGoodsOrderMapper;
import cn.ug.mall.mapper.entity.MallGoods;
import cn.ug.mall.mapper.entity.MallGoodsOrder;
import cn.ug.mall.service.MallGoodsOrderService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MallGoodsOrderServiceImpl implements MallGoodsOrderService {
	@Autowired
	private MallGoodsOrderMapper mallGoodsOrderMapper;
	@Autowired
	private MallGoodsMapper mallGoodsMapper;

	@Override
	public List<MemberGoodsOrderBean> listRecords(String memberId, int status, int offset, int size) {
		if (StringUtils.isNotBlank(memberId)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("memberId", memberId);
			params.put("status", status);
			params.put("offset", offset);
			params.put("size", size);
			return mallGoodsOrderMapper.queryForListByMemberId(params);
		}
		return null;
	}

	@Override
	public int countRecords(String memberId, int status) {
		if (StringUtils.isNotBlank(memberId)) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("memberId", memberId);
			params.put("status", status);
			return mallGoodsOrderMapper.queryForCountByMemberId(params);
		}
		return 0;
	}

	@Override
	public MemberGoodsOrderDetailBean getOrderDetail(String orderNO) {
		if (StringUtils.isNotBlank(orderNO)) {
			return mallGoodsOrderMapper.queryForDetailByOrderNO(orderNO);
		}
		return null;
	}

	@Override
	public boolean expiredGoodsOrder(String[] orderNOs) {
		if (orderNOs != null && orderNOs.length > 0) {
			return mallGoodsOrderMapper.expiredGoodsOrder(orderNOs) > 0 ? true : false;
		}
		return false;
	}

	@Override
	public List<MemberGoodsOrderBean> listExpiredGoodsOrder() {
		return mallGoodsOrderMapper.queryForExpiredGoodsOrder();
	}

	@Override
	public boolean addOrder(MallGoodsOrder order) {
		if (order != null) {
			if (StringUtils.isBlank(order.getId())) {
				MallGoods goods = mallGoodsMapper.findById(order.getGoodsId());
				if (goods == null) {
					return false;
				}
				order.setId(UF.getRandomUUID());
				order.setAddTime(UF.getDateTime());
				int rows = mallGoodsOrderMapper.insert(order);
				Ensure.that(rows).isLt(1,"00000005");
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("stock", goods.getStock()-1);
				params.put("id", goods.getId());
				return mallGoodsMapper.updateStock(params) > 0 ? true : false;
			}
		}
		return false;
	}

	@Override
	public boolean succeedGoodsOrder(String orderNO) {
		if (StringUtils.isNotBlank(orderNO)) {
			return mallGoodsOrderMapper.succeedGoodsOrder(orderNO, UF.getFormatDateTime(UF.getDateTime())) > 0 ? true : false;
		}
		return false;
	}

	@Override
	public boolean failGoodsOrder(String orderNO, String reason) {
		if (StringUtils.isNotBlank(orderNO)) {
			int rows = mallGoodsOrderMapper.failGoodsOrder(orderNO, reason);
			Ensure.that(rows).isLt(1,"00000005");
			MemberGoodsOrderDetailBean order = mallGoodsOrderMapper.queryForDetailByOrderNO(orderNO);
			if (order != null && StringUtils.isNotBlank(order.getGoodsId())) {
				MallGoods goods = mallGoodsMapper.findById(order.getGoodsId());
				if (goods != null) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("stock", goods.getStock()+1);
					params.put("id", goods.getId());
					return mallGoodsMapper.updateStock(params) > 0 ? true : false;
				}
			}
		}
		return false;
	}

	@Override
	public boolean addRemark(String orderNO, String remark) {
		if (StringUtils.isNotBlank(orderNO)) {
			return mallGoodsOrderMapper.addRemark(orderNO, remark) > 0 ? true : false;
		}
		return false;
	}

	@Override
	public List<GoodsOrderBean> listOrders(int status, String orderNO, String userName, String userMobile, String goodsName, String startDate, String endDate, int offset, int size) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", status);
		if (StringUtils.isNotBlank(orderNO)) {
			params.put("orderNO", orderNO);
		}
		if (StringUtils.isNotBlank(userName)) {
			params.put("userName", userName);
		}
		if (StringUtils.isNotBlank(userMobile)) {
			params.put("userMobile", userMobile);
		}
		if (StringUtils.isNotBlank(goodsName)) {
			params.put("goodsName", goodsName);
		}
		if (StringUtils.isNotBlank(startDate)) {
			params.put("startDate", startDate);
		}
		if (StringUtils.isNotBlank(endDate)) {
			params.put("endDate", endDate);
		}
		params.put("offset", offset);
		params.put("size", size);
		return mallGoodsOrderMapper.query(params);
	}

	@Override
	public int countOrders(int status, String orderNO, String userName, String userMobile, String goodsName, String startDate, String endDate) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", status);
		if (StringUtils.isNotBlank(orderNO)) {
			params.put("orderNO", orderNO);
		}
		if (StringUtils.isNotBlank(userName)) {
			params.put("userName", userName);
		}
		if (StringUtils.isNotBlank(userMobile)) {
			params.put("userMobile", userMobile);
		}
		if (StringUtils.isNotBlank(goodsName)) {
			params.put("goodsName", goodsName);
		}
		if (StringUtils.isNotBlank(startDate)) {
			params.put("startDate", startDate);
		}
		if (StringUtils.isNotBlank(endDate)) {
			params.put("endDate", endDate);
		}
		return mallGoodsOrderMapper.count(params);
	}
}