package cn.ug.mall.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.service.BrandService;
import cn.ug.mall.web.submit.BrandSearchSubmit;
import cn.ug.mall.web.submit.BrandSubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 品牌
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 添加品牌
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject save(@RequestHeader String accessToken, BrandSubmit submit) {
        return brandService.insert(submit);
    }

    @RequestMapping(value = "/delete/{id}", method = GET)
    public SerializeObject deleteLabel(@RequestHeader String accessToken, @PathVariable("id") Long id) {
        if (id > 0) {
            return brandService.deleteById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 查找品牌
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject update(@RequestHeader String accessToken, @PathVariable("id") Long id) {
        if (id > 0) {
            return brandService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 更新品牌
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject update(@RequestHeader String accessToken, BrandSubmit submit) {
        if (submit.getId() > 0) {
            return brandService.update(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 分类品牌 分页
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject list(@RequestHeader String accessToken, BrandSearchSubmit submit) {
        return brandService.findList(submit);
    }

    /**
     * 分类品牌 不分页
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/search", method = GET)
    public SerializeObject search(@RequestHeader String accessToken) {
        return brandService.search();
    }

}
