package cn.ug.mall.service;


import cn.ug.mall.bean.LogisticsBean;

/**
 * 物流信息
 * @author kaiwotech
 */
public interface LogisticsService {

	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(LogisticsBean entityBean);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);

	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除通过订单号
	 * @param orderNo	订单号
	 * @return		操作影响的行数
	 */
	int deleteByOrderNo(String orderNo);

	/**
	 * 确认收货
	 * @param orderNo	订单号
	 * @return		操作影响的行数
	 */
	void receive(String orderNo);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	LogisticsBean findById(String id);

	/**
	 * 根据订单号查找
	 * @param orderNo	订单号
	 * @return			列表
	 */
	LogisticsBean findByOrderNo(String orderNo);

}
