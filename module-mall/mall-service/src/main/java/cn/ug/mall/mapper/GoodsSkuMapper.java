package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.GoodsSkuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Mapper
public interface GoodsSkuMapper {

    /**
     * 添加商品规格
     * @param entity
     * @return
     */
    int insert(GoodsSkuEntity entity);

    /**
     * 更新商品规格
     * @param entity
     * @return
     */
    int update(GoodsSkuEntity entity);

    /**
     * 删除商品规格
     * @param id skuid
     * @return
     */
    int deleteById(long id);

    /**
     * 删除商品规格
     * @param goodsId
     * @return
     */
    int deleteByGoodsId(@Param("goodsId") long goodsId);

    /**
     * 更新规格库存
     * @param skuEntity
     * @return
     */
    int updateStock(GoodsSkuEntity skuEntity);
    /**
     * 查找商品库存和
     * @param goodsId
     * @return
     */
    int getSumStockByGoodsId(@Param("goodsId") long goodsId);

    /**
     * 查找单个规格
     * @param id
     * @return
     */
    GoodsSkuEntity findById(long id);
    /**
     * 查询商品规格列表
     * @param goodsId
     * @return
     */
    List<GoodsSkuEntity> findListByGoodsId(long goodsId);

    /**
     *
     * @param ids
     * @return
     */
    List<GoodsSkuEntity> findByIds(@Param("ids") List<Long> ids);


    int updateGoodsStockSales(@Param("id") long id, @Param("num") int num);

    /**
     * 查询最小规格的数据
     * @param goodsId
     * @return
     */
    GoodsSkuEntity findMinGoodsId(long goodsId);

}
