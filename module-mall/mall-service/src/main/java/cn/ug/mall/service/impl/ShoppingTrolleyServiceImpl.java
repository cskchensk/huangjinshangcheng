package cn.ug.mall.service.impl;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.enums.RateKeyEnum;
import cn.ug.mall.mapper.GoodsMapper;
import cn.ug.mall.mapper.GoodsSkuMapper;
import cn.ug.mall.mapper.RateSettingsMapper;
import cn.ug.mall.mapper.ShoppingTrolleyMapper;
import cn.ug.mall.mapper.entity.GoodsEntity;
import cn.ug.mall.mapper.entity.GoodsSkuEntity;
import cn.ug.mall.mapper.entity.RateSettingEntity;
import cn.ug.mall.mapper.entity.ShoppingTrolley;
import cn.ug.mall.service.ShoppingTrolleyService;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ShoppingTrolleyServiceImpl implements ShoppingTrolleyService {
    @Autowired
    private ShoppingTrolleyMapper shoppingTrolleyMapper;
    @Autowired
    private GoodsSkuMapper goodsSkuMapper;
    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private RateSettingsMapper rateSettingsMapper;

    @Override
    public SerializeObject save(ShoppingTrolley shoppingTrolley) {
        GoodsSkuEntity skuEntity = goodsSkuMapper.findById(shoppingTrolley.getSkuId());
        if (skuEntity == null || skuEntity.getGoodsId() == 0) {
            return new SerializeObjectError("20300200");
        }
        GoodsEntity goodsEntity = goodsMapper.findById(skuEntity.getGoodsId());
        if (goodsEntity == null) {
            return new SerializeObjectError("20300200");
        }
        shoppingTrolley.setGram(skuEntity.getGram());
        shoppingTrolley.setPrice(skuEntity.getPrice());
        shoppingTrolley.setImgUrl(goodsEntity.getImgUrl());
        shoppingTrolley.setName(goodsEntity.getName());
        ShoppingTrolley trolley = shoppingTrolleyMapper.selectBySkuId(shoppingTrolley.getMemberId(), shoppingTrolley.getSkuId());
        int raws = 0;
        if (trolley != null && trolley.getId() > 0) {
            raws = shoppingTrolleyMapper.updateQuantity(trolley.getId(), trolley.getQuantity()+shoppingTrolley.getQuantity());
        } else {
            List<ShoppingTrolley> list = shoppingTrolleyMapper.queryForList(shoppingTrolley.getMemberId());
            if (list != null && list.size() >= 30) {
                return new SerializeObjectError("20300209");
            }
            raws = shoppingTrolleyMapper.insert(shoppingTrolley);
        }
        if (raws > 0) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @Override
    public boolean deleteByIds(Long[] id) {
        if (id != null && id.length > 0) {
            return shoppingTrolleyMapper.deleteByIds(id) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<ShoppingTrolley> queryForList(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            List<ShoppingTrolley> list = shoppingTrolleyMapper.queryForList(memberId);
            if (list != null && list.size() > 0) {
                RateSettingEntity rate = rateSettingsMapper.findByKeyName(RateKeyEnum.FREIGHT.getKey());
                double freight = 0;
                if (rate != null && StringUtils.isNotBlank(rate.getKeyValue())) {
                    JSONObject rateJson = JSONObject.parseObject(rate.getKeyValue());
                    freight = rateJson.getDouble("fee");
                }
                for (ShoppingTrolley shoppingTrolley : list) {
                    shoppingTrolley.setFreight(new BigDecimal(freight));
                }
            }
            return list;
        }
        return null;
    }

    @Override
    public boolean updateQuantity(long id, int quantity) {
        if (id > 0 && quantity > 0) {
            return shoppingTrolleyMapper.updateQuantity(id, quantity) > 0 ? true : false;
        }
        return false;
    }
}
