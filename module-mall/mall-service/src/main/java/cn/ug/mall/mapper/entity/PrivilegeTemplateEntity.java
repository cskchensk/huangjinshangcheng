package cn.ug.mall.mapper.entity;

import java.io.Serializable;

public class PrivilegeTemplateEntity implements Serializable {
    private Integer    id;
    /**
     * 模板类型
     * 1.买金优惠模板
     * 2.回租优惠模板
     */
    private Integer type;

    /**
     * 优惠方案类型
     * 1.不设置优惠
     * 2.按照单笔购买总克重优惠
     * 3.按照会员等级优惠
     */
    private Integer schemeType;

    /**
     * 方案名称
     */
    private String schemeName;

    /**
     * 优惠方式
     * 1.优惠金额
     * 2.优惠比例
     * 3.奖励金豆
     */
    private Integer way;

    /**
     * 状态 1:启用 2:停用
     */
    private Integer status;

    /**
     * json数据
     */
    private String data;

    private Integer deleted;

    private String modifyTime;

    private String addTime;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSchemeType() {
        return schemeType;
    }

    public void setSchemeType(Integer schemeType) {
        this.schemeType = schemeType;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}
