package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.GoodsAuditEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Mapper
public interface GoodsAuditMapper {

    /**
     * 添加审核
     * @param entity
     * @return
     */
    int insert(GoodsAuditEntity entity);


    /**
     * 审核list
     * @param goodsId
     * @return
     */
    List<GoodsAuditEntity> findByGoodsIds(@Param("goodsId") long goodsId);
}
