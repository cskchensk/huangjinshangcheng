package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.mapper.entity.BrandEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Mapper
public interface BrandMapper {

    /**
     * 添加品牌
     * @param entity
     * @return
     */
    int insert(BrandEntity entity);

    /**
     * 更新品牌
     * @param entity
     * @return
     */
    int update(BrandEntity entity);

    /**
     * 删除品牌
     * @param entity
     * @return
     */
    int delete(BrandEntity entity);

    /**
     * 查找品牌
     * @param id
     * @return
     */
    BrandEntity findById(Long id);

    /**
     * 品牌行数
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);

    /**
     * 品牌list
     * @return
     */
    List<BrandEntity> findList(QuerySubmit submit);


    List<BrandEntity> searchList();
    /**
     * 删除id
     * @param id
     * @return
     */
    int deleteById(long id);
}
