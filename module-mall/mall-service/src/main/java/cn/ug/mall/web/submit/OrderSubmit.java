package cn.ug.mall.web.submit;

import cn.ug.mall.bean.OrderItemBean;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/10.
 */
public class OrderSubmit {

    private Long       id;
    private String     serial;
    private String     userId;
    private String     userName;
    private BigDecimal amount;
    private BigDecimal expressFee;
    private BigDecimal processCost;
    private BigDecimal money;
    private Integer    payChannel;
    private BigDecimal totalGram;
    private Integer    orderType;
    private Integer    status;
    /**
     * 收货人地址
     */
    private Long       addressId;
    private Integer    logisticsId;
    private String     cancelReason;
    private String     closeRemark;
    private String     clientRemark;
    private String     serverRemark;
    private Integer    source;
    private BigDecimal interestHistoryAmount;
    private BigDecimal principalNowAmount;
    private BigDecimal principalHistoryAmount;
    private BigDecimal freezeAmount;

    public BigDecimal getInterestHistoryAmount() {
        return interestHistoryAmount;
    }

    public void setInterestHistoryAmount(BigDecimal interestHistoryAmount) {
        this.interestHistoryAmount = interestHistoryAmount;
    }

    public BigDecimal getPrincipalNowAmount() {
        return principalNowAmount;
    }

    public void setPrincipalNowAmount(BigDecimal principalNowAmount) {
        this.principalNowAmount = principalNowAmount;
    }

    public BigDecimal getPrincipalHistoryAmount() {
        return principalHistoryAmount;
    }

    public void setPrincipalHistoryAmount(BigDecimal principalHistoryAmount) {
        this.principalHistoryAmount = principalHistoryAmount;
    }

    public BigDecimal getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(BigDecimal freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    private List<OrderItemBean> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getExpressFee() {
        return expressFee;
    }

    public void setExpressFee(BigDecimal expressFee) {
        this.expressFee = expressFee;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Integer getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(Integer payChannel) {
        this.payChannel = payChannel;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Integer getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(Integer logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getCloseRemark() {
        return closeRemark;
    }

    public void setCloseRemark(String closeRemark) {
        this.closeRemark = closeRemark;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getClientRemark() {
        return clientRemark;
    }

    public void setClientRemark(String clientRemark) {
        this.clientRemark = clientRemark;
    }

    public String getServerRemark() {
        return serverRemark;
    }

    public void setServerRemark(String serverRemark) {
        this.serverRemark = serverRemark;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public List<OrderItemBean> getItems() {
        return items;
    }

    public void setItems(List<OrderItemBean> items) {
        this.items = items;
    }

}
