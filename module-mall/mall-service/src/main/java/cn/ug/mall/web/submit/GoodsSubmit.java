package cn.ug.mall.web.submit;

import cn.ug.core.ensure.Ensure;
import cn.ug.mall.bean.GoodsSkuBean;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public class GoodsSubmit {

    /**
     * 商品id
     */
    private Long               id;
    /**
     * 商品名称
     */
    private String             name;
    /**
     * 副标题
     */
    private String             title;
    /**
     * 品牌id
     */
    private Long               brandId;
    /**
     * 分类id
     */
    private Integer            categoryId;
    /**
     * 加工模板id
     */
    private Integer            processId;
    /**
     * 加工费
     */
    private BigDecimal         processCost;
    /**
     * 商品类型         1:金条 2:金饰
     */
    private Integer            type;
    /**
     * 列表图片
     */
    private String             imgUrl;
    /**
     * 商品品质
     */
    private String             quality;
    /**
     * 库存
     */
    private Integer            stock;
    /**
     * 标签
     */
    private Integer[]          labels;
    /**
     * 最低价格
     */
    private BigDecimal         minPrice;
    /**
     * 最高价格
     */
    private BigDecimal         maxPrice;
    /**
     * 市场最低价
     */
    private BigDecimal         minMarketPrice;
    /**
     * 市场最高价
     */
    private BigDecimal         maxMarketPrice;
    /**
     * 最小克重
     */
    private BigDecimal         minGram;
    /**
     * 最小克重
     */
    private BigDecimal         maxGram;
    /**
     * 是否限购 0:不限 1:限购数量 2:限购克重
     */
    private Integer            isQuota;
    /**
     * 限购数量
     */
    private Integer            quotaNum;
    /**
     * 限购克重
     */
    private BigDecimal         quotaWeight;
    /**
     * 是否有限购时间
     */
    private Integer            isPrompt;
    /**
     * 限购开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")// HH:mm:ss
    private Date               startPrompt;
    /**
     * 限购结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date               endPrompt;
    /**
     * 操作人
     */
    private String             operatorId;
    /**
     * 审核人
     */
    private String             auditorId;
    /**
     * 审核状态
     */
    private Integer            auditType;
    /**
     * 上下架状态
     */
    private Integer            shelveType;
    /**
     * 上架时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date               shelveTime;
    /**
     * 下架时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date               unShelveTime;
    /**
     * 销量
     */
    private Integer            sales;
    /**
     * 商品规格
     */
    private List<GoodsSkuBean> skuList;
    /**
     * 商品详情图片
     */
    private String[]           imgList;
    /**
     * 商品banner图片 banner
     */
    private String[]           pictureList;
    /**
     * 商品标签
     */
    private Integer[]          labelList;
    /**
     * 购买须知
     */
    private String             notice;
    /**
     * 保养提示
     */
    private String             hint;


    public void checkAndReduce() {
        //校验
        Ensure.that(this.name).isNull("23000101");
        Ensure.that(this.title).isNull("23000102");
        Ensure.that(this.categoryId).isNull("23000103");
        Ensure.that(this.brandId).isNull("23000104");
        Ensure.that(this.processId).isNull("23000111");
        Ensure.that(this.skuList).isNull("23000107");
        Ensure.that(this.labels == null).isTrue("23000110");
        Ensure.that(this.imgList == null).isTrue("23000112");
        Ensure.that(this.pictureList == null).isTrue("23000113");
        Ensure.that(this.imgList.length > 5).isTrue("23000114");
        Ensure.that(this.pictureList.length > 3).isTrue("23000115");


        if (CollectionUtils.isNotEmpty(skuList)) {
            this.minPrice = skuList.stream().min(Comparator.comparing(GoodsSkuBean::getPrice)).get().getPrice();
            this.maxPrice = skuList.stream().max(Comparator.comparing(GoodsSkuBean::getPrice)).get().getPrice();//最大价格
            this.minGram = skuList.stream().min(Comparator.comparing(GoodsSkuBean::getGram)).get().getGram();
            this.maxGram = skuList.stream().max(Comparator.comparing(GoodsSkuBean::getGram)).get().getGram();//最大克重
            this.minMarketPrice = skuList.stream().min(Comparator.comparing(GoodsSkuBean::getMarketPrice)).get().getMarketPrice();//最大克重
            this.maxMarketPrice = skuList.stream().max(Comparator.comparing(GoodsSkuBean::getMarketPrice)).get().getMarketPrice();
            this.stock = skuList.stream().mapToInt(GoodsSkuBean::getStock).sum();
        }

        this.labels = this.removeArrayEmpty(labels);

        if (isPrompt < 1) {
            this.startPrompt = null;
            this.endPrompt = null;
        }

        if(isQuota == 0){
            quotaNum = 0;
            quotaWeight = BigDecimal.ZERO;
        }

        if(isQuota == 1){
            quotaWeight = BigDecimal.ZERO;
        }

        if(isQuota == 2){
            quotaNum = 0;
        }
    }

    private Integer[] removeArrayEmpty(Integer[] strArray) {
        List<Integer> strList = Arrays.asList(strArray);
        List<Integer> strListNew = new ArrayList<>();
        for (int i = 0; i < strList.size(); i++) {
            if (strList.get(i) != null && !strList.get(i).equals(0)) {
                strListNew.add(strList.get(i));
            }
        }
        return strListNew.toArray(new Integer[strListNew.size()]);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinMarketPrice() {
        return minMarketPrice;
    }

    public void setMinMarketPrice(BigDecimal minMarketPrice) {
        this.minMarketPrice = minMarketPrice;
    }

    public BigDecimal getMaxMarketPrice() {
        return maxMarketPrice;
    }

    public void setMaxMarketPrice(BigDecimal maxMarketPrice) {
        this.maxMarketPrice = maxMarketPrice;
    }

    public BigDecimal getMinGram() {
        return minGram;
    }

    public void setMinGram(BigDecimal minGram) {
        this.minGram = minGram;
    }

    public BigDecimal getMaxGram() {
        return maxGram;
    }

    public void setMaxGram(BigDecimal maxGram) {
        this.maxGram = maxGram;
    }

    public Integer getIsQuota() {
        return isQuota;
    }

    public void setIsQuota(Integer isQuota) {
        this.isQuota = isQuota;
    }

    public Integer getQuotaNum() {
        return quotaNum;
    }

    public void setQuotaNum(Integer quotaNum) {
        this.quotaNum = quotaNum;
    }

    public BigDecimal getQuotaWeight() {
        return quotaWeight;
    }

    public void setQuotaWeight(BigDecimal quotaWeight) {
        this.quotaWeight = quotaWeight;
    }

    public Integer getIsPrompt() {
        return isPrompt;
    }

    public void setIsPrompt(Integer isPrompt) {
        this.isPrompt = isPrompt;
    }

    public Date getStartPrompt() {
        return startPrompt;
    }

    public void setStartPrompt(Date startPrompt) {
        this.startPrompt = startPrompt;
    }

    public Date getEndPrompt() {
        return endPrompt;
    }

    public void setEndPrompt(Date endPrompt) {
        this.endPrompt = endPrompt;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(String auditorId) {
        this.auditorId = auditorId;
    }

    public Integer getAuditType() {
        return auditType;
    }

    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    public Integer getShelveType() {
        return shelveType;
    }

    public void setShelveType(Integer shelveType) {
        this.shelveType = shelveType;
    }

    public Date getShelveTime() {
        return shelveTime;
    }

    public void setShelveTime(Date shelveTime) {
        this.shelveTime = shelveTime;
    }

    public Date getUnShelveTime() {
        return unShelveTime;
    }

    public void setUnShelveTime(Date unShelveTime) {
        this.unShelveTime = unShelveTime;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }

    public List<GoodsSkuBean> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<GoodsSkuBean> skuList) {
        this.skuList = skuList;
    }

    public String[] getImgList() {
        return imgList;
    }

    public void setImgList(String[] imgList) {
        this.imgList = imgList;
    }

    public Integer[] getLabels() {
        return labels;
    }

    public void setLabels(Integer[] labels) {
        this.labels = labels;
    }

    public String[] getPictureList() {
        return pictureList;
    }

    public void setPictureList(String[] pictureList) {
        this.pictureList = pictureList;
    }

    public Integer[] getLabelList() {
        return labelList;
    }

    public void setLabelList(Integer[] labelList) {
        this.labelList = labelList;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }
}
