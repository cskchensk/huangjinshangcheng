package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.web.submit.CategorySearchSubmit;
import cn.ug.mall.web.submit.CategorySubmit;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public interface CategoryService {

    /**
     * 添加分类
     *
     * @param submit
     * @return
     */
    SerializeObject insert(CategorySubmit submit);

    /**
     * 查找分类
     * @param id
     * @return
     */
    SerializeObject findById(Integer id);
    /**
     * 更新分类
     *
     * @param submit
     * @return
     */
    SerializeObject update(CategorySubmit submit);

    /**
     * 获取分类列表
     *
     * @return
     */
    SerializeObject findList(CategorySearchSubmit submit);

    /**
     * 获取分类列表
     *
     * @return
     */
    SerializeObject findList();
}
