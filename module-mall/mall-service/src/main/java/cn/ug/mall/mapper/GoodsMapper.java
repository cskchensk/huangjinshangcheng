package cn.ug.mall.mapper;

import cn.ug.mall.bean.GoodsSalesBean;
import cn.ug.mall.mapper.entity.OtherSubmit;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.mapper.entity.GoodsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/06.
 */
@Mapper
public interface GoodsMapper {

    /**
     * 添加商品
     * @param entity
     * @return
     */
    int insert(GoodsEntity entity);

    /**
     * 更新商品
     * @param entity
     * @return
     */
    int update(GoodsEntity entity);

    /**
     * 更新商品库存
     * @param id
     * @param stock
     * @return
     */
    int updateStock(@Param("id") Long id, @Param("stock") int stock);

    /**
     * 更新商品上下架状态
     * @param entity
     * @return
     */
    int updateShelveType(GoodsEntity entity);

    /**
     * 更新商品审核状态
     * @param submit
     * @return
     */
    int updateAuditType(OtherSubmit submit);


    /**
     * 删除商品
     * @param id
     * @return
     */
    int deleteById(Long id);

    /**
     * 查询商品
     * @param id
     * @return
     */
    GoodsEntity findById(Long id);

    /**
     * 查询商品行数
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);
    /**
     * 查询商品
     * @param submit
     * @return
     */
    List<GoodsEntity> findList(QuerySubmit submit);

    /**
     * 下架备注
     * @param submit
     */
    int downGoodsById(OtherSubmit submit);

    /**
     * 销量统计行数
     * @param querySubmit
     * @return
     */
    int getGoodsSalesCount(QuerySubmit querySubmit);

    /**
     * 销量统计list
     * @param querySubmit
     * @return
     */
    List<GoodsSalesBean> getGoodsSalesList(QuerySubmit querySubmit);

    int updateNeedShelvesGoods();

    int updateGoodsStockSales(@Param("id") long id, @Param("num") int num);

}
