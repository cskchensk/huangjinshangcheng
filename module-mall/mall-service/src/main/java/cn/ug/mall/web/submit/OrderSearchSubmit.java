package cn.ug.mall.web.submit;

import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaohg
 * @date 2018/07/10.
 */
public class OrderSearchSubmit {

    /**
     * 用户账号
     */
    private String  userName;
    /**
     * 订单号
     */
    private String  serial;
    /**
     * 商品名
     */
    private String  goodsName;
    /**
     * 订单类型
     */
    private Integer orderType;

    /**
     * 订单状态 1:待支付,2:订单关闭,3:支付成功-待发货,4:已发货-待收货,5:确认收货-已完成
     */
    private Integer status;

    private String startTime;
    private String endTime;

    private Integer pageNum  = 1;
    private Integer pageSize = 10;
    // 1-后天查询；2-app查询
    private int platform;

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(pageNum, pageSize);
        querySubmit.put("serial", serial);
        querySubmit.put("goodsName", goodsName);
        querySubmit.put("orderType", orderType);
        querySubmit.put("userName", userName);
        querySubmit.put("status", status);

        if (StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(startTime));
        }
        if (StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(endTime));
        }

    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
