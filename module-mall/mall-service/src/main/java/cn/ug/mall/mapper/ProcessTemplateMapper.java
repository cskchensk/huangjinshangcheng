package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.ProcessTemplateEntity;
import cn.ug.mall.web.submit.ProcessSearchSubmit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Mapper
public interface ProcessTemplateMapper {
    
    int insert(ProcessTemplateEntity entity);

    ProcessTemplateEntity findById(Integer id);

    int update(ProcessTemplateEntity template);

    List<ProcessTemplateEntity> findList(ProcessSearchSubmit submit);
}
