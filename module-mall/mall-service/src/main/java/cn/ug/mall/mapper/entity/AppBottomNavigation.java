package cn.ug.mall.mapper.entity;

import java.util.List;

public class AppBottomNavigation {

    private String name;
    private Integer showTag;
    private List<String> appVersions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getShowTag() {
        return showTag;
    }

    public void setShowTag(Integer showTag) {
        this.showTag = showTag;
    }

    public List<String> getAppVersions() {
        return appVersions;
    }

    public void setAppVersions(List<String> appVersions) {
        this.appVersions = appVersions;
    }
}
