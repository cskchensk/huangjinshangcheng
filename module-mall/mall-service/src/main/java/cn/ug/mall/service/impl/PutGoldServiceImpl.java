package cn.ug.mall.service.impl;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.feign.MemberUserService;
import cn.ug.mall.bean.PutGoldBean;
import cn.ug.mall.mapper.PutGoldMapper;
import cn.ug.mall.mapper.entity.PutGoldEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.service.PutGoldService;
import cn.ug.mall.web.submit.PutGoldSubmit;
import cn.ug.member.bean.response.MemberUserBean;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.Common;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 存金
 *
 * @author zhaohg
 * @date 2018/07/20.
 */
@Service
public class PutGoldServiceImpl extends BaseServiceImpl implements PutGoldService {

    @Resource
    private PutGoldMapper putGoldMapper;

    @Resource
    private MemberUserService memberUserService;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public SerializeObject insert(PutGoldSubmit submit) {
        //todo
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }

        if(submit != null && !Common.validateMobile(submit.getMobile())){
            return new SerializeObjectError<>("20300216");
        }

        SerializeObject<MemberUserBean> res = memberUserService.findById(loginBean.getId());
        if (res.getCode() == ResultType.NORMAL && res.getData() != null) {
            MemberUserBean user = res.getData();
            PutGoldEntity entity = new PutGoldEntity();
            entity.setUserId(user.getId());
            entity.setRealName(user.getName());
            entity.setIdCard(user.getIdCard());
            entity.setMobile(submit.getMobile());

            putGoldMapper.insert(entity);
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }

        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject putGoldList(PutGoldSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = putGoldMapper.count(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<PutGoldBean> list = new ArrayList<>(count);
        if (count > 0) {
            List<PutGoldEntity> entityList = putGoldMapper.findList(querySubmit);
            for (PutGoldEntity entity : entityList) {
                list.add(dozerBeanMapper.map(entity, PutGoldBean.class));
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public List<PutGoldBean> export(PutGoldSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = putGoldMapper.count(querySubmit);
        if (count > 0) {
            List<PutGoldBean> list = new ArrayList<>(count);
            List<PutGoldEntity> entityList = putGoldMapper.findExportList(querySubmit);
            for (PutGoldEntity entity : entityList) {
                list.add(dozerBeanMapper.map(entity, PutGoldBean.class));
            }
            return list;
        }
        return null;
    }
}
