package cn.ug.mall.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.service.CategoryService;
import cn.ug.mall.web.submit.CategorySearchSubmit;
import cn.ug.mall.web.submit.CategorySubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 商品分类
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 添加分类
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject save(@RequestHeader String accessToken, CategorySubmit submit) {
        return categoryService.insert(submit);
    }

    /**
     * 查找品牌
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject update(@RequestHeader String accessToken, @PathVariable("id") Integer id) {
        if (id > 0) {
            return categoryService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 更新分类
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject update(@RequestHeader String accessToken, CategorySubmit submit) {
        if (submit.getId() > 0) {
            return categoryService.update(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 分类列表
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject list(@RequestHeader String accessToken, CategorySearchSubmit submit) {
        return categoryService.findList(submit);
    }

    /**
     * 分类列表 不分页
     *
     * @return
     */
    @RequestMapping(value = "/search", method = GET)
    public SerializeObject search() {
        return categoryService.findList();
    }
}
