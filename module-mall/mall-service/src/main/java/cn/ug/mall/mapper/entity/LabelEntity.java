package cn.ug.mall.mapper.entity;

import java.util.Date;

/**
 * @author zhaohg
 * @date 2018/07/12.
 */
public class LabelEntity {

    private Integer id;
    private String  name;
    private String  url;
    private Integer isDel;
    /**
     * 修改时间
     */
    private Date    modifyTime;
    /**
     * 添加时间
     */
    private Date    addTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
