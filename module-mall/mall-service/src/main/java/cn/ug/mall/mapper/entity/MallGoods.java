package cn.ug.mall.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ywl
 * @date 2018-04-18
 */
public class MallGoods  extends BaseEntity implements Serializable {

    /** 商品名称 **/
    private String name;
    /** 分类 **/
    private String category;
    /** 品牌 **/
    private String brand;
    /** 标题 **/
    private String title;
    /** 标签 **/
    private String label;
    /** 库存 **/
    private Integer stock;
    /** 商品价格 **/
    private BigDecimal price;
    /** 商品详情 **/
    private String introduction;
    /** 保养提示 **/
    private String tips;
    /** 购买须知 **/
    private String cognition;
    /** 商品货号 **/
    private String code;
    /** 重量 **/
    private BigDecimal weight;
    /** 商品品质 **/
    private String quality;
    /** 加工费 **/
    private BigDecimal processingFee;
    /** 运费 **/
    private BigDecimal fare;
    /** 主图URL **/
    private String thumbnailUrl;
    /** 审核状态：1：待审核；2审核通过；3：审核不通过 **/
    private Integer auditStatus;
    /** 审核描述 **/
    private String auditOpinion;
    /**  审核状态**/
    private Integer saleStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public String getCognition() {
        return cognition;
    }

    public void setCognition(String cognition) {
        this.cognition = cognition;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public BigDecimal getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(BigDecimal processingFee) {
        this.processingFee = processingFee;
    }

    public BigDecimal getFare() {
        return fare;
    }

    public void setFare(BigDecimal fare) {
        this.fare = fare;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditOpinion() {
        return auditOpinion;
    }

    public void setAuditOpinion(String auditOpinion) {
        this.auditOpinion = auditOpinion;
    }

    public Integer getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(Integer saleStatus) {
        this.saleStatus = saleStatus;
    }
}
