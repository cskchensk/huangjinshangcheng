package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.NewLogisticsEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/12.
 */
@Mapper
public interface NewLogisticsMapper {

    /**
     * 添加物流信息
     * @param entity
     * @return
     */
    int insert(NewLogisticsEntity entity);
    /**
     * 更新物流信息
     * @param entity
     * @return
     */
    int update(NewLogisticsEntity entity);
    /**
     * 获取物流信息
     * @param id
     * @return
     */
    NewLogisticsEntity findById(Long id);
    /**
     * 获取物流信息
     * @param ids
     * @return
     */
    List<NewLogisticsEntity> findListByIds(Long ids);

}
