package cn.ug.mall.service;

import cn.ug.bean.base.DataTable;
import cn.ug.mall.bean.MallGoodsBean;
import cn.ug.mall.bean.MallGoodsMobileBean;
import cn.ug.mall.bean.MallGoodsMobileParam;
import cn.ug.mall.bean.MallGoodsParam;

import java.util.List;

public interface MallGoodsService {

    /**
     * 查询--后台
     * @param mallGoodsParam	    请求参数
     * @return				            分页数据
     */
    DataTable<MallGoodsBean> findList(MallGoodsParam mallGoodsParam);

    /**
     * 新增、修改--后台
     * @param mallGoodsBean
     */
    void save(MallGoodsBean mallGoodsBean);

    /**
     * 查询单个--后台
     * @param id
     * @return
     */
    MallGoodsBean findById(String id);


    /**
     * 上下架--后台
     * @param id             产品id
     * @param saleStatus    状态
     */
    int updateSaleStatus(String[] id, int saleStatus);

    /**
     * 更新库存--后台
     * @param id     产品id
     * @param number 数量
     * @param type 类型 1:增加 2：减少
     * @return
     */
    int updateStock(String id, int number, int type);

    /**
     * 获取sku列表--移动端
     * @param mallGoodsMobileParam
     * @return
     */
    List<MallGoodsMobileBean> findMallGoodsList(MallGoodsMobileParam mallGoodsMobileParam);

}
