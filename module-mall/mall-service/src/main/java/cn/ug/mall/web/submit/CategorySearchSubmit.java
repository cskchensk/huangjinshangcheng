package cn.ug.mall.web.submit;

import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public class CategorySearchSubmit {

    private String name;
    private String startTime;
    private String endTime;

    private int pageNum  = 1;
    private int pageSize = 10;

    public void setParams(QuerySubmit querySubmit) {
        if (this.pageNum < 1) {
            this.pageNum = 1;
        }
        if (this.pageSize < 10) {
            this.pageSize = 10;
        }

        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("name", this.name);

        if (StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(this.startTime));
        }
        if (StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(this.endTime));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
