package cn.ug.mall.mapper.entity;

import java.math.BigDecimal;

/**
 * 品宣信息参数配置-请求参数
 */
public class PublicityInfoBean {
    //关于我们是否显示 0-隐藏，1-显示
    private Integer aboutUsTag;
    //平台数据是否显示 0-隐藏，1-显示
    private Integer platformDataTag;
    //累计已为用户管理黄金克重（单位千克）
    private BigDecimal totalGoldGram;
    //累计服务用户人数（单位万人）
    private BigDecimal totalServeUser;
    //累计已为用户管理黄金克重，运营修正数据
    private Integer inputGoldGram;
    //累计服务用户人数，运营修正数据
    private Integer inputServeUser;
    //客服作息时间是否显示 0-隐藏，1-显示
    private Integer customerServiceScheduleTag;
    //上午时间段
    private String amTime;
    //下午时间段
    private String pmTime;
    //实物金热销标签1
    private String physicalGoldLabel1;
    //实物金热销标签2
    private String physicalGoldLabel2;
    //安稳金热销标1
    private String secureGoldLabel1;
    //安稳金热销标2
    private String secureGoldLabel2;
    //APP端显示累计已为用户管理黄金克重（克）
    private Integer appTotalGoldGram;
    //APP端显示累计服务用户人数（人）
    private Integer appTotalServeUser;

    public Integer getAboutUsTag() {
        return aboutUsTag;
    }

    public void setAboutUsTag(Integer aboutUsTag) {
        this.aboutUsTag = aboutUsTag;
    }

    public Integer getPlatformDataTag() {
        return platformDataTag;
    }

    public void setPlatformDataTag(Integer platformDataTag) {
        this.platformDataTag = platformDataTag;
    }

    public Integer getInputGoldGram() {
        return inputGoldGram;
    }

    public void setInputGoldGram(Integer inputGoldGram) {
        this.inputGoldGram = inputGoldGram;
    }

    public Integer getInputServeUser() {
        return inputServeUser;
    }

    public void setInputServeUser(Integer inputServeUser) {
        this.inputServeUser = inputServeUser;
    }

    public Integer getCustomerServiceScheduleTag() {
        return customerServiceScheduleTag;
    }

    public void setCustomerServiceScheduleTag(Integer customerServiceScheduleTag) {
        this.customerServiceScheduleTag = customerServiceScheduleTag;
    }

    public String getAmTime() {
        return amTime;
    }

    public void setAmTime(String amTime) {
        this.amTime = amTime;
    }

    public String getPmTime() {
        return pmTime;
    }

    public void setPmTime(String pmTime) {
        this.pmTime = pmTime;
    }

    public String getPhysicalGoldLabel1() {
        return physicalGoldLabel1;
    }

    public void setPhysicalGoldLabel1(String physicalGoldLabel1) {
        this.physicalGoldLabel1 = physicalGoldLabel1;
    }

    public String getPhysicalGoldLabel2() {
        return physicalGoldLabel2;
    }

    public void setPhysicalGoldLabel2(String physicalGoldLabel2) {
        this.physicalGoldLabel2 = physicalGoldLabel2;
    }

    public String getSecureGoldLabel1() {
        return secureGoldLabel1;
    }

    public void setSecureGoldLabel1(String secureGoldLabel1) {
        this.secureGoldLabel1 = secureGoldLabel1;
    }

    public String getSecureGoldLabel2() {
        return secureGoldLabel2;
    }

    public void setSecureGoldLabel2(String secureGoldLabel2) {
        this.secureGoldLabel2 = secureGoldLabel2;
    }

    public BigDecimal getTotalGoldGram() {
        return totalGoldGram;
    }

    public void setTotalGoldGram(BigDecimal totalGoldGram) {
        this.totalGoldGram = totalGoldGram;
    }

    public BigDecimal getTotalServeUser() {
        return totalServeUser;
    }

    public void setTotalServeUser(BigDecimal totalServeUser) {
        this.totalServeUser = totalServeUser;
    }

    public Integer getAppTotalGoldGram() {
        return appTotalGoldGram;
    }

    public void setAppTotalGoldGram(Integer appTotalGoldGram) {
        this.appTotalGoldGram = appTotalGoldGram;
    }

    public Integer getAppTotalServeUser() {
        return appTotalServeUser;
    }

    public void setAppTotalServeUser(Integer appTotalServeUser) {
        this.appTotalServeUser = appTotalServeUser;
    }
}
