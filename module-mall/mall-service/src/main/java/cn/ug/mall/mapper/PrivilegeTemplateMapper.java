package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.PrivilegeTemplateEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface PrivilegeTemplateMapper {

    /**
     * 添加
     *
     * @param privilegeTemplateEntity
     * @return
     */
    int insert(PrivilegeTemplateEntity privilegeTemplateEntity);

    /**
     * 更新
     *
     * @param map
     * @return
     */
    int update(Map map);

    /**
     * 查找
     *
     * @param map
     * @return
     */
    List<PrivilegeTemplateEntity> findList(Map map);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    PrivilegeTemplateEntity findById(int id);

    /**
     * 根据类型查询启用模板
     * @param type
     * @return
     */
    PrivilegeTemplateEntity findONTemplateByType(int type);
}
