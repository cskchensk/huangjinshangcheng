package cn.ug.mall.web.submit;

import java.math.BigDecimal;

/**
 * 加工费模板
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class ProcessSubmit {

    private Integer    id;
    private String     name;
    private BigDecimal processCost;
    private Integer    disCount;
    private Integer    type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Integer getDisCount() {
        return disCount;
    }

    public void setDisCount(Integer disCount) {
        this.disCount = disCount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
