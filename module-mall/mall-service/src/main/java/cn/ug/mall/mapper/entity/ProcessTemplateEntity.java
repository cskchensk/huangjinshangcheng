package cn.ug.mall.mapper.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 加工费模板
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class ProcessTemplateEntity {

    private Integer    id;
    private String     name;
    private BigDecimal processCost;
    private Integer    disCount;
    /**
     * 类型 1：金条 2:金饰
     */
    private Integer    type;
    private Date       modifyTime;
    private Date       addTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Integer getDisCount() {
        return disCount;
    }

    public void setDisCount(Integer disCount) {
        this.disCount = disCount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
