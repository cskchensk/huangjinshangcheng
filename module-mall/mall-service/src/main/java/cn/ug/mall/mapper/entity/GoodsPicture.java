package cn.ug.mall.mapper.entity;


import cn.ug.mapper.entity.BaseEntity;

/**
 * 商品图片表
 * @author kaiwotech
 */
public class GoodsPicture extends BaseEntity implements java.io.Serializable {

	/** 产品id */
	private String goodsId;
	/** 图片URL */
	private String imgUrl;
	/** 排序 */
	private int sort;

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}
}
