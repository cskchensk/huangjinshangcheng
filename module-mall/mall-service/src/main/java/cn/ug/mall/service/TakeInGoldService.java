package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.TakeInGoldBean;
import cn.ug.mall.web.submit.TakeInGoldSubmit;

import java.util.List;

/**
 * 提金换金用户管理
 * @author zhaohg
 * @date 2018/07/20.
 */
public interface TakeInGoldService {

    SerializeObject takeInGoldList(TakeInGoldSubmit submit);

    List<TakeInGoldBean> export(TakeInGoldSubmit submit);

}
