package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.mapper.entity.TakeInGoldEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/20.
 */
@Mapper
public interface TakeInGoldMapper {

    int insert(TakeInGoldEntity entity);

    TakeInGoldEntity findByUserId(@Param("userId") String userId);

    int count(QuerySubmit submit);

    List<TakeInGoldEntity> findList(QuerySubmit submit);

    List<TakeInGoldEntity> getUserGramList(QuerySubmit submit);

}
