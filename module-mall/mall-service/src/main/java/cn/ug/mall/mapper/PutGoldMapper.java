package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.PutGoldEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/20.
 */
@Mapper
public interface PutGoldMapper {

    int insert(PutGoldEntity entity);

    int count(QuerySubmit querySubmit);

    List<PutGoldEntity> findList(QuerySubmit submit);

    List<PutGoldEntity> findExportList(QuerySubmit querySubmit);
}
