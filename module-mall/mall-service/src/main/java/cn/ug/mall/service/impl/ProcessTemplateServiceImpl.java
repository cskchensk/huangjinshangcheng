package cn.ug.mall.service.impl;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.bean.ProcessTemplateBean;
import cn.ug.mall.mapper.ProcessTemplateMapper;
import cn.ug.mall.mapper.entity.ProcessTemplateEntity;
import cn.ug.mall.service.ProcessTemplateService;
import cn.ug.mall.web.submit.ProcessSearchSubmit;
import cn.ug.mall.web.submit.ProcessSubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Service
public class ProcessTemplateServiceImpl extends BaseServiceImpl implements ProcessTemplateService {

    @Resource
    private ProcessTemplateMapper processTemplateMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public SerializeObject insert(ProcessSubmit submit) {
        //todo 判断
        ProcessTemplateEntity entity = dozerBeanMapper.map(submit, ProcessTemplateEntity.class);
        int success = processTemplateMapper.insert(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findById(Integer id) {
        ProcessTemplateEntity entity = processTemplateMapper.findById(id);
        ProcessTemplateBean bean = new ProcessTemplateBean();
        if (entity != null) {
            bean = dozerBeanMapper.map(entity, ProcessTemplateBean.class);
        }

        return new SerializeObject<>(ResultType.NORMAL, bean);
    }

    @Override
    public SerializeObject update(ProcessSubmit submit) {
        ProcessTemplateEntity entity = processTemplateMapper.findById(submit.getId());
        if (entity != null) {
            ProcessTemplateEntity template = dozerBeanMapper.map(submit, ProcessTemplateEntity.class);
            processTemplateMapper.update(template);
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }

        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findList(ProcessSearchSubmit submit) {
        List<ProcessTemplateBean> list = new ArrayList<>();
        List<ProcessTemplateEntity> brandList = processTemplateMapper.findList(submit);
        if (!CollectionUtils.isEmpty(brandList)) {
            for (ProcessTemplateEntity entity : brandList) {
                ProcessTemplateBean bean = dozerBeanMapper.map(entity, ProcessTemplateBean.class);
                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
