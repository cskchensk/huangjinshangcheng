package cn.ug.mall.web.submit;

import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * @author zhaohg
 * @date 2018/07/20.
 */
public class PutGoldSubmit {

    private String userId;
    private String mobile;
    private String realName;
    private Integer type;
    private BigDecimal minGram;
    private BigDecimal maxGram;

    private String   startTime;
    private String   endTime;

    private int pageNum = 1;
    private int pageSize = 10;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getMinGram() {
        return minGram;
    }

    public void setMinGram(BigDecimal minGram) {
        this.minGram = minGram;
    }

    public BigDecimal getMaxGram() {
        return maxGram;
    }

    public void setMaxGram(BigDecimal maxGram) {
        this.maxGram = maxGram;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(pageNum, pageSize);
        querySubmit.put("realName", realName);
        querySubmit.put("mobile", mobile);
        querySubmit.put("type", type);
        querySubmit.put("minGram", minGram);
        querySubmit.put("maxGram", maxGram);

        if (StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(startTime));
        }
        if (StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(endTime));
        }
    }
}
