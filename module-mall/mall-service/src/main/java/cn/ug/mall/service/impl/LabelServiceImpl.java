package cn.ug.mall.service.impl;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.bean.LabelBean;
import cn.ug.mall.mapper.LabelMapper;
import cn.ug.mall.mapper.entity.LabelEntity;
import cn.ug.mall.service.LabelService;
import cn.ug.mall.web.submit.LabelSubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/12.
 */
@Service
public class LabelServiceImpl extends BaseServiceImpl implements LabelService {

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Resource
    private LabelMapper labelMapper;

    @Override
    public SerializeObject insert(LabelSubmit submit) {
        //todo 判断
        LabelEntity entity = dozerBeanMapper.map(submit, LabelEntity.class);
        int success = labelMapper.insert(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject deleteById(int id) {
        LabelEntity entity = labelMapper.findById(id);
        if (entity != null) {
            labelMapper.deleteById(id);
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findById(int id) {
        LabelEntity entity = labelMapper.findById(id);
        if (entity != null) {
            LabelBean bean = dozerBeanMapper.map(entity, LabelBean.class);
            return new SerializeObject<>(ResultType.NORMAL, "00000001", bean);
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject update(LabelSubmit submit) {
        LabelEntity entity = dozerBeanMapper.map(submit, LabelEntity.class);
        int success = labelMapper.update(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findList() {
        List<LabelBean> list = new ArrayList<>();
        List<LabelEntity> labelList = labelMapper.findList();
        for (LabelEntity entity : labelList) {
            LabelBean bean = dozerBeanMapper.map(entity, LabelBean.class);
            list.add(bean);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001",list);
    }
}
