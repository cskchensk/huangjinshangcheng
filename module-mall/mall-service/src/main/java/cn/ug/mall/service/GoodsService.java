package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.FeeBean;
import cn.ug.mall.bean.GoodsSkuBean;
import cn.ug.mall.mapper.entity.OtherSubmit;
import cn.ug.mall.web.submit.GoodsSearchSubmit;
import cn.ug.mall.web.submit.GoodsSkuSubmit;
import cn.ug.mall.web.submit.GoodsSubmit;

/**
 * @author zhaohg
 * @date 2018/07/06.
 */
public interface GoodsService {

    /**
     * 添加商品
     */
    SerializeObject insert(GoodsSubmit submit);

    /**
     * 更新商品
     *
     * @param submit
     * @return
     */
    SerializeObject update(GoodsSubmit submit);

    /**
     * 删除商品
     *
     * @param goodsId
     * @return
     */
    SerializeObject delete(Long goodsId);


    /**
     * 更新商品上下架状态
     * @param submit
     * @return
     */
    SerializeObject updateShelveType(OtherSubmit submit);

    /**
     * 设置商品是否推荐
     * @param submit
     * @return
     */
    SerializeObject updateRecommend(OtherSubmit submit);

    /**
     * 更新商品审核状态
     * @param submit
     * @return
     */
    SerializeObject updateAuditType(OtherSubmit submit);

    /**
     * 查询商品
     *
     * @param goodsId
     * @return
     */
    SerializeObject findById(Long goodsId);
    /**
     * 商品列表
     *
     * @param submit
     * @return
     */
    SerializeObject findList(GoodsSearchSubmit submit);

    /**
     * 商品规格列表
     * @param goodsId
     * @return
     */
    SerializeObject findSkuList(Long goodsId);
    /**
     * 更新商品库存
     * @param submit
     * @return
     */
    SerializeObject updateStock(GoodsSkuSubmit submit);

    /**
     * 商品审核详细
     * @param id
     * @return
     */
    SerializeObject auditDetailListByGoodsId(long id);

    /**
     * 下架备注
     * @param submit
     * @return
     */
    SerializeObject downGoods(OtherSubmit submit);

    SerializeObject validateGoods(long skuId, int quantity, String memberId, int type);

    GoodsSkuBean getGoodsSku(long skuId);

    FeeBean getFee(long skuId, int quantity, int type);

    /**
     * 获取商品详细 (move)
     * @param id
     * @return
     */
    SerializeObject getGoodsById(long id);

    SerializeObject getGoodsList(GoodsSearchSubmit submit);

    int updateNeedShelvesGoods();

    /**
     * 获取某个类型商品列表（app端）
     * @param submit
     * @return
     */
    SerializeObject getCategoryGoods(GoodsSearchSubmit submit);

    /**
     * 获取推荐商品列表（app端）
     * @param submit
     * @return
     */
    SerializeObject getRecommendGoods(GoodsSearchSubmit submit);
}
