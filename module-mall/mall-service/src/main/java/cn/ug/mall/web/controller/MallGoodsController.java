package cn.ug.mall.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.mall.bean.MallGoodsMobileBean;
import cn.ug.mall.bean.MallGoodsMobileParam;
import cn.ug.mall.bean.MallGoodsBean;
import cn.ug.mall.bean.MallGoodsParam;
import cn.ug.mall.service.MallGoodsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("mallGoods")
public class MallGoodsController{

    @Resource
    private MallGoodsService mallGoodsService;
    @Resource
    private Config config;

    /**
     * 查询列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param mallGoodsParam       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<MallGoodsBean>> queryBankCardList(@RequestHeader String accessToken, MallGoodsParam mallGoodsParam) {
        if(mallGoodsParam.getPageSize() <= 0) {
            mallGoodsParam.setPageSize(config.getPageSize());
        }
        DataTable<MallGoodsBean> dataTable = mallGoodsService.findList(mallGoodsParam);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询列表(后台)
     * @param id                      请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findById/{id}" , method = GET)
    public SerializeObject<MallGoodsBean> findById(@PathVariable String id) {
        MallGoodsBean mallGoodsBean = mallGoodsService.findById(id);
        return new SerializeObject<>(ResultType.NORMAL, mallGoodsBean);
    }
    /**
     * 新增、修改(后台)
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, MallGoodsBean entity) {
        mallGoodsService.save(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 产品上下架(后台)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @param saleStatus      上架状态   1:是 2:否
     * @return				    是否操作成功
     */
    @RequestMapping(value = "updateSaleStatus", method = PUT)
    public SerializeObject updateShelfState(@RequestHeader String accessToken, String[] id, int saleStatus) {
        mallGoodsService.updateSaleStatus(id, saleStatus);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 修改库存(后台)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @param number
     * @return				    是否操作成功
     */
    @RequestMapping(value = "updateStock", method = PUT)
    public SerializeObject updateStock(@RequestHeader String accessToken, String id, int number) {
        mallGoodsService.updateStock(id, number,1);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 获取列表--移动端
     * @param mallGoodsMobileParam
     * @return
     */
    @GetMapping(value = "findMallGoodsList")
    public SerializeObject<List<MallGoodsMobileBean>> findMallGoodsList(MallGoodsMobileParam mallGoodsMobileParam){
        List<MallGoodsMobileBean> list = mallGoodsService.findMallGoodsList(mallGoodsMobileParam);
        return  new SerializeObject(ResultType.NORMAL, list);
    }
}
