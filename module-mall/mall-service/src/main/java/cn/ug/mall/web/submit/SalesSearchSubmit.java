package cn.ug.mall.web.submit;

import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaohg
 * @date 2018/07/18.
 */
public class SalesSearchSubmit {

    private String name;
    private String goodsCode;

    private Integer shelveType;

    private String startTime;
    private String endTime;

    private int pageNum  = 1;
    private int pageSize = 10;

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(pageNum, pageSize);
        querySubmit.put("name", name);
        querySubmit.put("goodsCode", goodsCode);
        querySubmit.put("shelveType", shelveType);

        if (StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(startTime));
        }
        if (StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(endTime));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public Integer getShelveType() {
        return shelveType;
    }

    public void setShelveType(Integer shelveType) {
        this.shelveType = shelveType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
