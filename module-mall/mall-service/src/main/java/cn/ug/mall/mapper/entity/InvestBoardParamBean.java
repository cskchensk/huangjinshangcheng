package cn.ug.mall.mapper.entity;

import cn.ug.mall.bean.BusinessIntroduction;
import cn.ug.mall.bean.ModuleTitle;

import java.util.List;

public class InvestBoardParamBean {

    //产品展示区
    private String productModuleList;
    private List<ModuleTitle> moduleTitles;
    //业务介绍区是否显示 1显示，0隐藏
    private Integer businessShowTag;
    //业务介绍区配置
    private String businessIntroductions;
    private List<BusinessIntroduction> introductions;
    //投资动态是否显示 1显示，0隐藏
    private Integer investDynamicShowTag;
    //投资动态读取条数
    private Integer investReadNum;

    public String getProductModuleList() {
        return productModuleList;
    }

    public void setProductModuleList(String productModuleList) {
        this.productModuleList = productModuleList;
    }

    public Integer getBusinessShowTag() {
        return businessShowTag;
    }

    public void setBusinessShowTag(Integer businessShowTag) {
        this.businessShowTag = businessShowTag;
    }

    public String getBusinessIntroductions() {
        return businessIntroductions;
    }

    public void setBusinessIntroductions(String businessIntroductions) {
        this.businessIntroductions = businessIntroductions;
    }

    public Integer getInvestDynamicShowTag() {
        return investDynamicShowTag;
    }

    public void setInvestDynamicShowTag(Integer investDynamicShowTag) {
        this.investDynamicShowTag = investDynamicShowTag;
    }

    public Integer getInvestReadNum() {
        return investReadNum;
    }

    public void setInvestReadNum(Integer investReadNum) {
        this.investReadNum = investReadNum;
    }

    public List<ModuleTitle> getModuleTitles() {
        return moduleTitles;
    }

    public void setModuleTitles(List<ModuleTitle> moduleTitles) {
        this.moduleTitles = moduleTitles;
    }

    public List<BusinessIntroduction> getIntroductions() {
        return introductions;
    }

    public void setIntroductions(List<BusinessIntroduction> introductions) {
        this.introductions = introductions;
    }
}
