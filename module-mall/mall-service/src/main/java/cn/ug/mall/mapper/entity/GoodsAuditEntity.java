package cn.ug.mall.mapper.entity;

import java.util.Date;

/**
 * 商品审核
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class GoodsAuditEntity {

    /**
     * id
     */
    private Long    id;
    /**
     * 商品id
     */
    private Long    goodsId;
    /**
     * 审核状态 审核状态  1:待审核 2：审核通过 3: 审核未通过
     */
    private Integer auditType;
    /**
     * 审核备注
     */
    private String  remark;
    /**
     * 审核用户id
     */
    private String  auditorId;
    /**
     * 审核用户name
     */
    private String  auditorName;
    /**
     * 添加时间
     */
    private Date    addTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getAuditType() {
        return auditType;
    }

    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAuditorId() {
        return auditorId;
    }

    public void setAuditorId(String auditorId) {
        this.auditorId = auditorId;
    }

    public String getAuditorName() {
        return auditorName;
    }

    public void setAuditorName(String auditorName) {
        this.auditorName = auditorName;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
