package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.GoodsPicture;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 商品图片表
 * @author kaiwotech
 */
@Component
public interface GoodsPictureMapper extends BaseMapper<GoodsPicture> {

    /**
     * 删除通过商品ID
     * @param id	ID数组
     * @return		操作影响的行数
     */
    int deleteByGoodsIds(@Param(value = "id") String[] id);

}