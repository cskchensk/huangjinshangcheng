package cn.ug.mall.web.submit;

import cn.ug.mall.bean.GoodsSkuBean;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/12.
 */
public class GoodsSkuSubmit {

    private Long               goodsId;
    private List<GoodsSkuBean> skuList;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public List<GoodsSkuBean> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<GoodsSkuBean> skuList) {
        this.skuList = skuList;
    }
}
