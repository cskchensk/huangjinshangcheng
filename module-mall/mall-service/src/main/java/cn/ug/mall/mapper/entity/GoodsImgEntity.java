package cn.ug.mall.mapper.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品图片
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public class GoodsImgEntity implements Serializable {

    public static final int    GOODS_BANNER_LIST_TYPE = 1;
    public static final int    GOODS_DETAIL_LIST_TYPE  = 2;
    /**
     * 图片id
     */
    private             Long   id;
    /**
     * 产品id
     */
    private             Long   goodsId;
    /**
     * 图片URL
     */
    private             String url;
    /**
     * 排序
     */
    private             int    sort;
    /**
     * 图片类型 1:商品详情图 2:商品banner图;
     */
    private             int    imgType;
    /**
     * 是否已删 0：未 1：已
     */
    private             int    isDel;
    /**
     * 修改时间
     */
    private             Date   modifyTime;
    /**
     * 添加时间
     */
    private             Date   addTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getImgType() {
        return imgType;
    }

    public void setImgType(int imgType) {
        this.imgType = imgType;
    }

    public int getIsDel() {
        return isDel;
    }

    public void setIsDel(int isDel) {
        this.isDel = isDel;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
