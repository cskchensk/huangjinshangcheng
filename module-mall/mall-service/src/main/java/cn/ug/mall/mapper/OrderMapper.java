package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.OrderEntity;
import cn.ug.mall.mapper.entity.OrderItemEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单
 *
 * @author zhaohg
 * @date 2018/07/10.
 */
@Mapper
public interface OrderMapper {

    /**
     * 添加订单
     *
     * @param entity
     * @return
     */
    int insert(OrderEntity entity);

    /**
     * 订单更新
     *
     * @param entity
     * @return
     */
    int update(OrderEntity entity);

    /**
     * 订单详细
     *
     * @param id
     * @return
     */
    OrderEntity findById(Long id);

    /**
     * 订单行数
     *
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);

    /**
     * 订单列表
     *
     * @param submit
     * @return
     */
    List<OrderEntity> findList(QuerySubmit submit);

    /**
     * 关闭订单
     *
     * @param entity
     * @return
     */
    int closeOrder(OrderEntity entity);

    /**
     * 备注订单
     *
     * @param entity
     * @return
     */
    int updateServerRemark(OrderEntity entity);

    /**
     * 订单发货
     *
     * @param entity
     * @return
     */
    int orderDelivery(OrderEntity entity);

    double selectTotalGramByMemberId(@Param("goodsId") Long goodsId, @Param("memberId") String memberId);

    double getSomedayTradeGram(@Param("memberId") String memberId,
                               @Param("someday") String someday,
                               @Param("orderType") int orderType);

    int failOrder(@Param("orderNO") String orderNO, @Param("closeRemark") String closeRemark);

    int succeedOrder(@Param("orderNO") String orderNO);
    int handlingOrder(@Param("orderNO") String orderNO);

    OrderEntity selectByOrderNO(@Param("orderNO") String orderNO);

    /**
     * 确认收货
     *
     * @param id
     * @return
     */
    int receiveOrder(Long id);

    OrderEntity findBySerial(@Param("serial") String serial);

    List<OrderEntity> getNeedCloseOrderList();

    int updateNeedCloseOrder(@Param("ids")List<Long> ids, @Param("closeRemark") String closeRemark);

    int read(@Param("orderNO")String orderNO);
    int countUnreadNum(@Param("memberId")String memberId, @Param("status")int status);

    int countSuccessfulNum(@Param("memberId")String memberId);

}
