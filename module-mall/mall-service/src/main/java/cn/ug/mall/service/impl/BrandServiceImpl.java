package cn.ug.mall.service.impl;


import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.bean.BrandBean;
import cn.ug.mall.mapper.BrandMapper;
import cn.ug.mall.mapper.entity.BrandEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.service.BrandService;
import cn.ug.mall.web.submit.BrandSearchSubmit;
import cn.ug.mall.web.submit.BrandSubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Service
public class BrandServiceImpl extends BaseServiceImpl implements BrandService {

    @Resource
    private BrandMapper brandMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public SerializeObject insert(BrandSubmit submit) {
        //todo 判断
        BrandEntity entity = dozerBeanMapper.map(submit, BrandEntity.class);
        int success = brandMapper.insert(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findById(Long id) {
        BrandEntity entity = brandMapper.findById(id);
        if (entity != null) {
            BrandBean bean = dozerBeanMapper.map(entity, BrandBean.class);
            return new SerializeObject<>(ResultType.NORMAL, "00000001", bean);
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject update(BrandSubmit submit) {
        BrandEntity entity = dozerBeanMapper.map(submit, BrandEntity.class);
        int success = brandMapper.update(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }

    @Override
    public SerializeObject findList(BrandSearchSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = brandMapper.count(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<BrandBean> list = new ArrayList<>(count);
        if (count > 0) {
            List<BrandEntity> brandList = brandMapper.findList(querySubmit);
            for (BrandEntity entity : brandList) {
                BrandBean bean = dozerBeanMapper.map(entity, BrandBean.class);
                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public SerializeObject search() {
        List<BrandBean> list = new ArrayList<>();
        List<BrandEntity> brandList = brandMapper.searchList();
        if (brandList != null) {
            for (BrandEntity entity : brandList) {
                BrandBean bean = dozerBeanMapper.map(entity, BrandBean.class);
                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", list);
    }

    @Override
    public SerializeObject deleteById(long id) {
        BrandEntity entity = brandMapper.findById(id);
        if (entity != null) {
            brandMapper.deleteById(id);
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError( "00000005");
    }
}
