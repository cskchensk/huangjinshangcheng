package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.mapper.entity.ShoppingTrolley;

import java.util.List;

public interface ShoppingTrolleyService {
    SerializeObject save(ShoppingTrolley shoppingTrolley);
    boolean deleteByIds(Long[] id);
    List<ShoppingTrolley> queryForList(String memberId);
    boolean updateQuantity(long id, int quantity);
}
