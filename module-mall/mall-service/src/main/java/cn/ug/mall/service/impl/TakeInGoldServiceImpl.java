package cn.ug.mall.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.mall.bean.TakeInGoldBean;
import cn.ug.mall.mapper.TakeInGoldMapper;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.mall.mapper.entity.TakeInGoldEntity;
import cn.ug.mall.service.TakeInGoldService;
import cn.ug.mall.web.submit.TakeInGoldSubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 提金 换金用户管理
 *
 * @author zhaohg
 * @date 2018/07/18.
 */
@Service
public class TakeInGoldServiceImpl extends BaseServiceImpl implements TakeInGoldService {

    @Resource
    private TakeInGoldMapper takeInGoldMapper;
    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public SerializeObject takeInGoldList(TakeInGoldSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = takeInGoldMapper.count(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<TakeInGoldBean> list = new ArrayList<>();
        if (count > 0) {
            List<TakeInGoldEntity> takes = takeInGoldMapper.findList(querySubmit);
            for (TakeInGoldEntity take : takes) {
                TakeInGoldBean bean = dozerBeanMapper.map(take, TakeInGoldBean.class);
                list.add(bean);
            }
            return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, null));
    }

    @Override
    public List<TakeInGoldBean> export(TakeInGoldSubmit submit) {QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = takeInGoldMapper.count(querySubmit);
        List<TakeInGoldBean> list = new ArrayList<>();
        if (count > 0) {
            List<TakeInGoldEntity> takes = takeInGoldMapper.getUserGramList(querySubmit);
            for (TakeInGoldEntity take : takes) {
                TakeInGoldBean bean = dozerBeanMapper.map(take, TakeInGoldBean.class);
                list.add(bean);
            }
            return list;
        }
        return null;
    }

}
