package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.GoodsImgEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Mapper
public interface GoodsImgMapper {


    int insert(GoodsImgEntity entity);

    int deleteByGoodsId(@Param("goodsId") Long goodsId);

    int delete(GoodsImgEntity entity);

    List<GoodsImgEntity> findListByGoodsId(Long goodsId);

}
