package cn.ug.mall.web.submit;

/**
 * 加工费模板搜索
 *
 * @author zhaohg
 * @date 2018/07/12.
 */
public class ProcessSearchSubmit {

    private String  name;
    private Integer type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
