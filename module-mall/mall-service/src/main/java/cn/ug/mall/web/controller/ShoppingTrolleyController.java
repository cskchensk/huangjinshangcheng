package cn.ug.mall.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.bean.type.YesNo;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.feign.BankCardService;
import cn.ug.feign.MemberAddressService;
import cn.ug.mall.mapper.entity.ShoppingTrolley;
import cn.ug.mall.service.ShoppingTrolleyService;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shopping/trolley")
public class ShoppingTrolleyController extends BaseController {
    @Autowired
    private ShoppingTrolleyService shoppingTrolleyService;
    @Autowired
    private MemberAddressService memberAddressService;
    @Autowired
    private BankCardService bankCardService;

    @PostMapping
    public SerializeObject addGoods(@RequestHeader String accessToken, long skuId, int quantity){
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isBlank(loginBean.getId())) {
            return new SerializeObjectError("00000102");
        }
        if (skuId <= 0 || quantity <= 0) {
            return new SerializeObjectError("00000002");
        }
        String memberId = loginBean.getId();
        SerializeObject<Integer> numBean = memberAddressService.getAddressNum(memberId);
        if (numBean == null || numBean.getData() == null || numBean.getData() < 1) {
            return new SerializeObjectError("20300211");
        }
        SerializeObject serializeObject = bankCardService.validateBindBankCard(memberId);
        if(null == serializeObject || serializeObject.getCode() != ResultType.NORMAL) {
            return new SerializeObjectError("20300210");
        }

        ShoppingTrolley shoppingTrolley = new ShoppingTrolley();
        shoppingTrolley.setMemberId(memberId);
        shoppingTrolley.setSkuId(skuId);
        shoppingTrolley.setQuantity(quantity);
        return shoppingTrolleyService.save(shoppingTrolley);
    }

    @GetMapping("/list")
    public SerializeObject queryForList(@RequestHeader String accessToken){
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isBlank(loginBean.getId())) {
            return new SerializeObjectError("00000102");
        }
        String memberId = loginBean.getId();
        List<ShoppingTrolley> list = shoppingTrolleyService.queryForList(memberId);
        return new SerializeObject(ResultType.NORMAL, "00000001", list);
    }

    @PostMapping("/quantity")
    public SerializeObject updateQuantity(@RequestHeader String accessToken, long recordId, int quantity){
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isBlank(loginBean.getId())) {
            return new SerializeObjectError("00000102");
        }
        if (recordId <= 0 || quantity <= 0) {
            return new SerializeObjectError("00000002");
        }
        if (shoppingTrolleyService.updateQuantity(recordId, quantity)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PostMapping("/remove")
    //public SerializeObject remove(@RequestHeader String accessToken, @RequestParam(value = "recordId[]")  Long[] recordId){
    public SerializeObject remove(@RequestHeader String accessToken, String recordId){
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isBlank(loginBean.getId())) {
            return new SerializeObjectError("00000102");
        }
        if (StringUtils.isBlank(recordId)) {
            return new SerializeObjectError("00000002");
        }
        recordId = StringUtils.remove(recordId, "[");
        recordId = StringUtils.remove(recordId, "]");
        if (StringUtils.isBlank(recordId)) {
            return new SerializeObjectError("00000002");
        }
        String[] ids = StringUtils.split(recordId, ",");
        if (ids == null || ids.length == 0) {
            return new SerializeObjectError("00000002");
        }
        Long[] param = new Long[ids.length];
        for (int i = 0; i < ids.length; i++) {
            try {
                param[i] = Long.parseLong(ids[i]);
            } catch (Exception e) {
                return new SerializeObjectError("00000002");
            }
        }
        if (shoppingTrolleyService.deleteByIds(param)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }
}
