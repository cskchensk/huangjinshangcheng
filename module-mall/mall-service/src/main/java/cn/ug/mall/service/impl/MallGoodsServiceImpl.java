package cn.ug.mall.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.config.RedisGlobalLock;
import cn.ug.core.ensure.Ensure;
import cn.ug.mall.bean.*;
import cn.ug.mall.mapper.MallGoodsMapper;
import cn.ug.mall.mapper.entity.MallGoods;
import cn.ug.mall.service.GoodsPictureService;
import cn.ug.mall.service.MallGoodsService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ywl
 */
@Service
public class MallGoodsServiceImpl  implements MallGoodsService{

    @Resource
    private GoodsPictureService goodsPictureService;

    @Resource
    private MallGoodsMapper mallGoodsMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    @Override
    public DataTable<MallGoodsBean> findList(MallGoodsParam mallGoodsParam) {
        com.github.pagehelper.Page<MallGoodsParam> pages = PageHelper.startPage(mallGoodsParam.getPageNum(),mallGoodsParam.getPageSize());
        List<MallGoodsBean> dataList = mallGoodsMapper.findList(mallGoodsParam);
        return new DataTable<>(mallGoodsParam.getPageNum(), mallGoodsParam.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public void save(MallGoodsBean mallGoodsBean) {
        //校验
        Ensure.that(mallGoodsBean.getName()).isNull("23000101");
        Ensure.that(mallGoodsBean.getCategory()).isNull("23000102");
        Ensure.that(mallGoodsBean.getBrand()).isNull("23000103");
        Ensure.that(mallGoodsBean.getCode()).isNull("23000104");
        Ensure.that(mallGoodsBean.getStock()).isNull("23000105");
        Ensure.that(mallGoodsBean.getPrice()).isNull("23000106");
        Ensure.that(mallGoodsBean.getIntroduction()).isNull("23000107");
        Ensure.that(mallGoodsBean.getWeight()).isNull("23000108");
        Ensure.that(mallGoodsBean.getFare()).isNull("23000109");

        //数据后期做校验
        MallGoods mallGoods = dozerBeanMapper.map(mallGoodsBean, MallGoods.class);
        if(StringUtils.isBlank(mallGoodsBean.getId())){
            mallGoods.setId(UF.getRandomUUID());
            mallGoodsMapper.insert(mallGoods);
        }else{
            mallGoodsMapper.update(mallGoods);
        }
        //保存图片
        if(mallGoodsBean.getImg() != null){
            List<String> list= Arrays.asList(mallGoodsBean.getImg());
            goodsPictureService.save(mallGoods.getId(),list);
        }
    }

    @Override
    public MallGoodsBean findById(String id) {
        MallGoods mallGoods = mallGoodsMapper.findById(id);
        MallGoodsBean mallGoodsBean = dozerBeanMapper.map(mallGoods, MallGoodsBean.class);
        List<GoodsPictureBean> list = goodsPictureService.findListByGoodsId(id);
        if(list != null &&  !list.isEmpty()){
            mallGoodsBean.setGoodsPictureBeans(list);
        }
        return mallGoodsBean;
    }

    @Override
    public int updateSaleStatus(String[] id, int saleStatus) {
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("id",id);
        map.put("saleStatus",saleStatus);
        int num = mallGoodsMapper.updateSaleStatus(map);
        return num;
    }

    @Override
    public int updateStock(String id, int number, int type) {
        // 1、获取分布式锁防止重复调用 =====================================================
        String key = "MallGoodsStock:" + id;
        if(redisGlobalLock.lock(key)) {
            try{
                //获取当前商品库存
                MallGoods mallGoods = mallGoodsMapper.findById(id);
                Map<String,Object> param = new HashMap<String,Object>();
                param.put("id", id);
                if(type == 1){
                    param.put("stock", mallGoods.getStock() + number);
                }else{
                    param.put("stock", mallGoods.getStock() - number);
                }
                mallGoodsMapper.updateStock(param);
            }catch (Exception e){
                throw  e;
            }finally {
                // 4、释放分布式锁 ================================================================
                redisGlobalLock.unlock(key);
            }
        }else{
            // 如果没有获取锁
            Ensure.that(true).isTrue("17000706");
        }
        return 0;
    }

    @Override
    public List<MallGoodsMobileBean> findMallGoodsList(MallGoodsMobileParam mallGoodsMobileParam) {
        List<MallGoodsMobileBean> list = mallGoodsMapper.findMallGoodsList(mallGoodsMobileParam);
        return list;
    }
}

