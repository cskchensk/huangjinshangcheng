package cn.ug.mall.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.PutGoldBean;
import cn.ug.mall.bean.TakeInGoldBean;
import cn.ug.mall.service.PutGoldService;
import cn.ug.mall.service.TakeInGoldService;
import cn.ug.mall.web.submit.PutGoldSubmit;
import cn.ug.mall.web.submit.TakeInGoldSubmit;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 存金 提金 换金
 *
 * @author zhaohg
 * @date 2018/07/20.
 */
@RestController
@RequestMapping("/gold")
public class GoldController {

    @Autowired
    private PutGoldService putGoldService;

    @Autowired
    private TakeInGoldService takeInGoldService;

    /**
     * 存金
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject save(@RequestHeader String accessToken, PutGoldSubmit submit) {
        return putGoldService.insert(submit);
    }

    /**
     * 存金管理list
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/put/list", method = GET)
    public SerializeObject putGoldList(PutGoldSubmit submit) {
        return putGoldService.putGoldList(submit);
    }


    /**
     * 提金换金管理list
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/takeIn/list", method = GET)
    public SerializeObject takeInGoldList(TakeInGoldSubmit submit) {
        return takeInGoldService.takeInGoldList(submit);
    }

    /**
     * 导出存金统计
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/put/export", method = GET)
    public void putExport(HttpServletResponse response, PutGoldSubmit submit) {
        List<PutGoldBean> list = putGoldService.export(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "手机号", "姓名", "身份证号", "预存金克重"};
            String[] columns = {"index", "mobile", "realName", "idCard", "gram"};
            String fileName = "存金用户管理";
            ExportExcelController<PutGoldBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapPutGoldBeanData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    /**
     * 导出换金提金记录
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/takeIn/export", method = GET)
    public void takeInExport(HttpServletResponse response, TakeInGoldSubmit submit) {
        List<TakeInGoldBean> list = takeInGoldService.export(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "手机号", "姓名", "身份证号", "已提金总克重（克）", "已换金总克重（克）"};
            String[] columns = {"index", "mobile", "realName", "idCard", "totalTakeGram", "totalInGram"};
            String fileName = "提金换金用户管理";
            ExportExcelController<TakeInGoldBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapTakeInGoldBeanData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    private List<TakeInGoldBean> wrapTakeInGoldBeanData(List<TakeInGoldBean> list) {
        List<TakeInGoldBean> beans = new ArrayList<>();
        int index = 1;
        for (TakeInGoldBean bean : list) {
            TakeInGoldBean gold = new TakeInGoldBean();
            gold.setIndex(index++);
            gold.setMobile(bean.getMobile());
            gold.setRealName(bean.getRealName());
            gold.setIdCard(bean.getIdCard());
            gold.setTotalInGram(bean.getTotalInGram());
            gold.setTotalTakeGram(bean.getTotalTakeGram());
            beans.add(gold);
        }
        return beans;
    }

    private List<PutGoldBean> wrapPutGoldBeanData(List<PutGoldBean> list) {
        List<PutGoldBean> beans = new ArrayList<>();
        int index = 1;
        for (PutGoldBean bean : list) {
            PutGoldBean gold = new PutGoldBean();
            gold.setIndex(index++);
            gold.setMobile(bean.getMobile());
            gold.setRealName(bean.getRealName());
            gold.setIdCard(bean.getIdCard());
            gold.setGram(bean.getGram());
            beans.add(gold);
        }
        return beans;
    }


}
