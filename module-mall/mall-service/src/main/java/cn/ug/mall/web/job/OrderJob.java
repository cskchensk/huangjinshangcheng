package cn.ug.mall.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.mall.service.OrderService;
import cn.ug.util.UF;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @author zhaohg
 * @date 2018/07/24.
 */
@Component
public class OrderJob {

    private Log               log = LogFactory.getLog(OrderJob.class);
    @Autowired
    private RedisGlobalLock   redisGlobalLock;
    @Autowired
    private OrderService      orderService;


    /**
     * 定时关闭待支付订单
     * 1小时执行一次
     */
    @Scheduled(cron = "0 * */1 * * ?")
    public void closeOrder() {
        String key = "OrderJob:closeOrder:" + UF.getFormatDateTime("yyyy-MM-dd HH", UF.getDateTime());
        try {
            if (!redisGlobalLock.lock(key, 1, TimeUnit.HOURS)) {
                return;
            }
            boolean result = orderService.updateNeedCloseOrder();
            log.info("处理超24小时未支付订单[result = " + result + "]");

        } finally {
            redisGlobalLock.unlock(key);
        }
    }


}
