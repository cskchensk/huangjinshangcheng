package cn.ug.mall.mapper.entity;

import java.util.Date;

/**
 * 商品分类
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class CategoryEntity {

    /**
     * 分类id
     */
    private Long id;
    /**
     * 分类name
     */
    private String name;
    /**
     * 父id
     */
    private Long parentId;

    /**
     * 排序号
     */
    private Integer sort;
    /**
     * 备注
     */
    private String remark;
    /**
     * 是否删除 -1删除 0有效
     */
    private Integer isDel;
    /**
     * 修改时间
     */
    private Date modifyTime;
    /**
     * 添加商品
     */
    private Date addTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
