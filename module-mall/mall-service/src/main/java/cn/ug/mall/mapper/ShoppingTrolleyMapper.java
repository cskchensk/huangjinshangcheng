package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.ShoppingTrolley;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShoppingTrolleyMapper {
    int insert(ShoppingTrolley shoppingTrolley);
    int deleteByIds(@Param(value = "id") Long[] id);
    int deleteBySkuId(@Param("memberId")String memberId, @Param("skuId")long skuId);
    List<ShoppingTrolley> queryForList(@Param("memberId")String memberId);
    ShoppingTrolley selectBySkuId(@Param("memberId")String memberId, @Param("skuId")long skuId);
    int updateQuantity(@Param("id")long id, @Param("quantity")int quantity);
}
