package cn.ug.mall.web.submit;

import cn.ug.mall.bean.LeaseDay;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 实物金参数设置实体
 */
public class EntityGoldSubmit implements Serializable {

    /**
     * 回租奖励结算方式 1以现金结算 2金豆结算
     */
    private Integer leaseCloseType;

    /**
     * 回租奖励计算金价 1.回租前一日收盘价  2.回租前一日某个时间点金价
     * 类型为2的时候leaseCalculateGoldTime必填
     */
    private Integer leaseCalculateGoldType;

    /**
     * 回租前一日时间点 时间精确到分 如12:00
     */
    private String  leaseCalculateGoldTime;

    /**
     * 实物金规格  单位克
     */
    private List<Integer> specificationList;

    /**
     * 回租期限  key-value
     */
    private List<LeaseDay>  leaseDayList;

    public Integer getLeaseCloseType() {
        return leaseCloseType;
    }

    public void setLeaseCloseType(Integer leaseCloseType) {
        this.leaseCloseType = leaseCloseType;
    }

    public Integer getLeaseCalculateGoldType() {
        return leaseCalculateGoldType;
    }

    public void setLeaseCalculateGoldType(Integer leaseCalculateGoldType) {
        this.leaseCalculateGoldType = leaseCalculateGoldType;
    }

    public String getLeaseCalculateGoldTime() {
        return leaseCalculateGoldTime;
    }

    public void setLeaseCalculateGoldTime(String leaseCalculateGoldTime) {
        this.leaseCalculateGoldTime = leaseCalculateGoldTime;
    }

    public List<Integer> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(List<Integer> specificationList) {
        this.specificationList = specificationList;
    }

    public List<LeaseDay> getLeaseDayList() {
        return leaseDayList;
    }

    public void setLeaseDayList(List<LeaseDay> leaseDayList) {
        this.leaseDayList = leaseDayList;
    }
}
