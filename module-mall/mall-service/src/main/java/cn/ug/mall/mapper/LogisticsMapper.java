package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.Logistics;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 物流信息
 * @author kaiwotech
 */
@Component
public interface LogisticsMapper extends BaseMapper<Logistics> {

    /**
     * 删除通过订单号ID
     * @param orderNo	订单号数组
     * @return		操作影响的行数
     */
    int deleteByOrderNo(@Param(value = "orderNo") String[] orderNo);

}