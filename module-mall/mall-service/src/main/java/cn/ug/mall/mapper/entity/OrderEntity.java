package cn.ug.mall.mapper.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zhaohg
 * @date 2018/07/10.
 */
public class OrderEntity {

    private Long       id;
    private String     goodsName;
    private String     serial;
    private String     userId;
    /**
     * 用户账号
     */
    private String     userName;
    private BigDecimal amount;
    private BigDecimal expressFee;
    private BigDecimal processCost;
    private BigDecimal money;
    private Integer    payChannel;
    private BigDecimal totalGram;
    /**
     * 1:商城订单 2:提金订单 3:换金订单
     */
    private Integer    orderType;
    /**
     * 1:待支付,2:订单关闭,3:支付成功-待发货,4:已发货-待收货,5:确认收货-已完成
     */
    private Integer    status;
    private Long       addressId;
    private Long       logisticsId;
    private String     closeRemark;
    private Date       closeTime;
    private String     clientRemark;
    private String     serverRemark;
    private String     cancelReason;
    /**
     * 订单来源 1:ios 2:android 3:H5 4:微信小程序 5:第三方平台 6其他
     */
    private Integer    source;
    private Date       payTime;
    private Date       sendTime;
    private Date       modifyTime;
    private Date       addTime;
    private BigDecimal interestHistoryAmount;
    private BigDecimal principalNowAmount;
    private BigDecimal principalHistoryAmount;
    private BigDecimal freezeAmount;
    private int couponAmount;
    private int couponId;
    private int readed;
    private int stopPayTimeSecond;

    public int getReaded() {
        return readed;
    }

    public void setReaded(int readed) {
        this.readed = readed;
    }

    public int getStopPayTimeSecond() {
        return stopPayTimeSecond;
    }

    public void setStopPayTimeSecond(int stopPayTimeSecond) {
        this.stopPayTimeSecond = stopPayTimeSecond;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(int couponAmount) {
        this.couponAmount = couponAmount;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getExpressFee() {
        return expressFee;
    }

    public void setExpressFee(BigDecimal expressFee) {
        this.expressFee = expressFee;
    }

    public BigDecimal getProcessCost() {
        return processCost;
    }

    public void setProcessCost(BigDecimal processCost) {
        this.processCost = processCost;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public BigDecimal getInterestHistoryAmount() {
        return interestHistoryAmount;
    }

    public void setInterestHistoryAmount(BigDecimal interestHistoryAmount) {
        this.interestHistoryAmount = interestHistoryAmount;
    }

    public BigDecimal getPrincipalNowAmount() {
        return principalNowAmount;
    }

    public void setPrincipalNowAmount(BigDecimal principalNowAmount) {
        this.principalNowAmount = principalNowAmount;
    }

    public BigDecimal getPrincipalHistoryAmount() {
        return principalHistoryAmount;
    }

    public void setPrincipalHistoryAmount(BigDecimal principalHistoryAmount) {
        this.principalHistoryAmount = principalHistoryAmount;
    }

    public BigDecimal getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(BigDecimal freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public Integer getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(Integer payChannel) {
        this.payChannel = payChannel;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Long getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(Long logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getCloseRemark() {
        return closeRemark;
    }

    public void setCloseRemark(String closeRemark) {
        this.closeRemark = closeRemark;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getClientRemark() {
        return clientRemark;
    }

    public void setClientRemark(String clientRemark) {
        this.clientRemark = clientRemark;
    }

    public String getServerRemark() {
        return serverRemark;
    }

    public void setServerRemark(String serverRemark) {
        this.serverRemark = serverRemark;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
