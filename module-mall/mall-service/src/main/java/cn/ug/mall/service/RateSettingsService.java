package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.ModuleTitle;
import cn.ug.mall.bean.PrivilegeTemplateBean;
import cn.ug.mall.mapper.entity.InvestBoardParamBean;
import cn.ug.mall.mapper.entity.PublicityInfoParamBean;
import cn.ug.mall.mapper.entity.RateSettingEntity;
import cn.ug.mall.web.submit.RateSettingSubmit;

import java.util.List;

/**
 * 参数配置
 *
 * @author zhaohg
 * @date 2018/07/06.
 */
public interface RateSettingsService {

    /**
     * 添加或更新参数配置
     *
     * @return
     */

    SerializeObject operate(String keyName, RateSettingSubmit rate);

    /**
     * 获取参数配置
     *
     * @param keyName
     * @return
     */
    SerializeObject findByKeyName(String keyName,String accessToken);

    /**
     * 优惠模板
     * @return
     */
    SerializeObject save(PrivilegeTemplateBean privilegeTemplateBean);

    /**
     * 根据类型查询全部优惠模板
     * @param type
     * @return
     */
    SerializeObject findTemplateByType(Integer type);

    /**
     * 根据id查询优惠模板
     * @param id
     * @return
     */
    SerializeObject findById(Integer id);

    /**
     * 删除模板
     * @param id
     * @return
     */
    SerializeObject delete(Integer id);

    /**
     * 模板启停
     * @param id
     * @param status
     * @return
     */
    SerializeObject templateSwitch(Integer id,Integer status);

    /**
     * 查询启用的模板
     * @param type
     * @return
     */
    SerializeObject<PrivilegeTemplateBean> findONTemplateByType(Integer type);

    /**
     * 查询展示的版本号
     * @param keyName
     * @return
     */
    SerializeObject<List<String>> findShowVersions(String keyName);

    /**
     * 修改显示/隐藏的版本
     * @param keyName
     * @param showTag
     * @return
     */
    SerializeObject updateShowVesion(String keyName, Integer showTag, List<String> versions);

    /**
     * 修改keyValue
     * @param rateSettingEntity
     * @return
     */
    SerializeObject updateKeyValue(RateSettingEntity rateSettingEntity);

    /**
     * 修改品宣信息
     * @param publicityInfoParamBean
     * @return
     */
    SerializeObject updatePublicityByParam(PublicityInfoParamBean publicityInfoParamBean);

    /**
     * 获取品宣模块配置信息
     * @return
     */
    SerializeObject findPublicity();

    /**
     * 获取投资版块配置信息
     * @return
     */
    SerializeObject findInvestArea();

    /**
     * 修改投资版块配置
     * @param paramBean
     * @return
     */
    SerializeObject updateInvestAreaByParam(InvestBoardParamBean paramBean);

    SerializeObject<List<ModuleTitle>> findProductModule();
}
