package cn.ug.mall.web.submit;

/**
 * 商品分类
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class CategorySubmit {

    /**
     * 分类id
     */
    private Long    id;
    /**
     * 分类name
     */
    private String  name;
    /**
     * 父id
     */
    private Long    parentId;
    /**
     * 排序号
     */
    private Integer sort;
    /**
     * 备注
     */
    private String  remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
