package cn.ug.mall.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.web.submit.SalesSearchSubmit;

/**
 * @author zhaohg
 * @date 2018/07/18.
 */
public interface GoodsSalesService {

    SerializeObject getGoodsSales(SalesSearchSubmit submit);
}
