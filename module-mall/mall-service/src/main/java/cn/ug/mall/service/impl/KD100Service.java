package cn.ug.mall.service.impl;

import cn.ug.bean.Result;
import cn.ug.config.KD100Config;
import cn.ug.util.HttpClientUtil;
import cn.ug.util.MD5Util;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaohg
 * @date 2018/07/19.
 */
@Component
public class KD100Service {

    private static final Logger logger = LoggerFactory.getLogger(KD100Service.class);

    @Resource
    private KD100Config kd100Config;

    //订阅接口需要推送
    /**
     * 快递100查询
     * <p>
     * 正式环境请求地址：https://poll.kuaidi100.com/poll/query.do
     * 测试环境请求地址：https://poll.kuaidi100.com/test/poll/query.do
     *
     * @return
     */
    public Result queryOrderTrace(String code, String postId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("com", code);
        jsonObject.put("num", postId);
        String customer = "81C3F5320B7758FD410E58A6E17C77BA";
        String key = "kyspETzH6856";
        String sign = MD5Util.encode(jsonObject.toJSONString() + key + customer);

        Map<String, String> params = new HashMap<>();
        params.put("param", jsonObject.toJSONString());
        params.put("sign", sign);
        params.put("customer", customer);
        Result result = null;
        try {
            String resp = HttpClientUtil.sendPost(kd100Config.getQueryUrl(), params);
            result = JSON.parseObject(resp, Result.class);
            return result;
        } catch (Exception e) {
            logger.warn("快递100物流查询失败,error:{}", e.getMessage());
            return null;
        }
    }

}
