package cn.ug.mall.mapper;

import cn.ug.mall.mapper.entity.CategoryEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Mapper
public interface CategoryMapper {

    /**
     * 添加分类
     *
     * @param entity
     * @return
     */
    int insert(CategoryEntity entity);

    /**
     * 更新分类
     *
     * @param entity
     * @return
     */
    int update(CategoryEntity entity);

    /**
     * 分类列表
     *
     * @return
     */
    List<CategoryEntity> findList(QuerySubmit submit);

    /**
     * 查找分类
     *
     * @param id
     */
    CategoryEntity findById(Integer id);

    int count(QuerySubmit querySubmit);

    List<CategoryEntity> searchList();
}

