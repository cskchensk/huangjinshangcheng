package cn.ug.mall.web.submit;

import cn.ug.mall.mapper.entity.GoodsEntity;
import cn.ug.mall.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * 搜索商品
 *
 * @author zhaohg
 * @date 2018/07/09.
 */
public class GoodsSearchSubmit {


    /**
     * 商品名称
     */
    private String     name;

    /**
     * 商品货号
     */
    private String     goodsCode;
    /**
     * 最低价格
     */
    private BigDecimal minPrice;
    /**
     * 最高价格
     */
    private BigDecimal maxPrice;
    /**
     * 最小克重
     */
    private BigDecimal minGram;
    /**
     * 最小克重
     */
    private BigDecimal maxGram;

    /**
     * 审核状态 1:待审核 2：审核通过 3:审核未通过
     */
    private Integer auditType;
    /**
     * 上下架状态  1: 待上架,2:上架，3:下架
     */
    private Integer shelveType;

    private String startTime;
    private String endTime;

    /**
     * 商品状态 0: 全部 1:金条 2:金饰
     */
    private Integer type;
    /**
     * 分类id
     */
    private Integer categoryId;

    private int pageNum  = 1;
    private int pageSize = 10;

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(pageNum, pageSize);
        querySubmit.put("name", name);
        querySubmit.put("goodsCode", goodsCode);
        querySubmit.put("minPrice", minPrice);
        querySubmit.put("maxPrice", maxPrice);
        querySubmit.put("minGram", minGram);
        querySubmit.put("maxGram", maxGram);

        querySubmit.put("auditType", auditType);
        querySubmit.put("shelveType", shelveType);
        querySubmit.put("type", type);


        if (StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(startTime));
        }
        if (StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(endTime));
        }
    }

    public void setMoveParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(pageNum, pageSize);
        querySubmit.put("type", type);
        querySubmit.put("auditType", GoodsEntity.AUDIT_PASS);
        querySubmit.put("shelveType", GoodsEntity.SHELVE_UP);

    }

    public void setRecommendParams(QuerySubmit querySubmit) {
        querySubmit.put("type", type);
        querySubmit.put("isRecommend", 1);
        querySubmit.put("auditType", GoodsEntity.AUDIT_PASS);
        querySubmit.put("shelveType", GoodsEntity.SHELVE_UP);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsCode() {
        return goodsCode;
    }

    public void setGoodsCode(String goodsCode) {
        this.goodsCode = goodsCode;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinGram() {
        return minGram;
    }

    public void setMinGram(BigDecimal minGram) {
        this.minGram = minGram;
    }

    public BigDecimal getMaxGram() {
        return maxGram;
    }

    public void setMaxGram(BigDecimal maxGram) {
        this.maxGram = maxGram;
    }

    public Integer getAuditType() {
        return auditType;
    }

    public void setAuditType(Integer auditType) {
        this.auditType = auditType;
    }

    public Integer getShelveType() {
        return shelveType;
    }

    public void setShelveType(Integer shelveType) {
        this.shelveType = shelveType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
