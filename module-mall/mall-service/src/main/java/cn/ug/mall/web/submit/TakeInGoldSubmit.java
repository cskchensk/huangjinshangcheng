package cn.ug.mall.web.submit;

import cn.ug.mall.mapper.entity.QuerySubmit;

import java.math.BigDecimal;

/**
 * 提金换金
 * @author zhaohg
 * @date 2018/07/20.
 */
public class TakeInGoldSubmit {

    private String mobile;
    private String realName;
    private BigDecimal maxTakeGram;
    private BigDecimal minTakeGram;
    private BigDecimal maxInGram;
    private BigDecimal minInGram;

    private int pageNum  = 1;
    private int pageSize = 10;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public BigDecimal getMaxTakeGram() {
        return maxTakeGram;
    }

    public void setMaxTakeGram(BigDecimal maxTakeGram) {
        this.maxTakeGram = maxTakeGram;
    }

    public BigDecimal getMinTakeGram() {
        return minTakeGram;
    }

    public void setMinTakeGram(BigDecimal minTakeGram) {
        this.minTakeGram = minTakeGram;
    }

    public BigDecimal getMaxInGram() {
        return maxInGram;
    }

    public void setMaxInGram(BigDecimal maxInGram) {
        this.maxInGram = maxInGram;
    }

    public BigDecimal getMinInGram() {
        return minInGram;
    }

    public void setMinInGram(BigDecimal minInGram) {
        this.minInGram = minInGram;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(pageNum, pageSize);
        querySubmit.put("realName", realName);
        querySubmit.put("mobile", mobile);
        querySubmit.put("maxTakeGram", maxTakeGram);
        querySubmit.put("minTakeGram", minTakeGram);
        querySubmit.put("maxInGram", maxInGram);
        querySubmit.put("minInGram", minInGram);
    }
}
