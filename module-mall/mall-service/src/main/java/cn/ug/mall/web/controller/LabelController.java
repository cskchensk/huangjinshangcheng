package cn.ug.mall.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.mall.service.LabelService;
import cn.ug.mall.web.submit.LabelSubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 商品标签设置
 *
 * @author zhaohg
 * @date 2018/07/12.
 */
@RestController
@RequestMapping("/label")
public class LabelController {

    @Autowired
    private LabelService labelService;

    /**
     * 商品标签添加
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject addLabel(@RequestHeader String accessToken, LabelSubmit submit) {
        return labelService.insert(submit);
    }

    /**
     * 商品标签删除
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/delete/{id}", method = GET)
    public SerializeObject deleteLabel(@RequestHeader String accessToken, @PathVariable("id") int id) {
        if (id > 0) {
            return labelService.deleteById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 查询商品标签
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject findById(@RequestHeader String accessToken, @PathVariable("id") int id) {
        if (id > 0) {
            return labelService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 商品标签更新
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject updateLabel(@RequestHeader String accessToken, LabelSubmit submit) {
        if (submit.getId() > 0) {
            return labelService.update(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 商品标签更新
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject findList(@RequestHeader String accessToken) {
        return labelService.findList();
    }


    /**
     * 查询商品标签
     *
     * @return
     */
    @RequestMapping(value = "/find/label", method = GET)
    public SerializeObject findLabelById(int id) {
        if (id > 0) {
            return labelService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }
}
