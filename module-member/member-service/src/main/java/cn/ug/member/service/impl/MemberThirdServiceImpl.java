package cn.ug.member.service.impl;

import cn.ug.member.bean.response.MemberThirdFindBean;
import cn.ug.member.mapper.MemberThirdMapper;
import cn.ug.member.mapper.entity.MemberThird;
import cn.ug.member.service.MemberThirdService;
import cn.ug.service.impl.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MemberThirdServiceImpl  extends BaseServiceImpl implements MemberThirdService {

    private static Logger logger = LoggerFactory.getLogger(MemberThirdServiceImpl.class);

    @Autowired
    private MemberThirdMapper memberThirdMapper;

    @Override
    public void save(String partnerId, String memberId, Integer type,Integer clientType) {
        MemberThird memberThird = new MemberThird();
        memberThird.setMemberId(memberId);
        memberThird.setOpenId(partnerId);
        memberThird.setType(type);
        memberThird.setClientType(clientType);
        memberThirdMapper.insert(memberThird);
    }

    @Override
    public MemberThirdFindBean find(String partnerId, String memberId, Integer type) {
        MemberThird memberThird = new MemberThird();
        memberThird.setMemberId(memberId);
        memberThird.setOpenId(partnerId);
        memberThird.setType(type);
        return memberThirdMapper.find(memberThird);
    }

    @Override
    public void update(String partnerId, String memberId, Integer type,Integer clientType) {
        MemberThird memberThird = new MemberThird();
        memberThird.setMemberId(memberId);
        memberThird.setOpenId(partnerId);
        memberThird.setType(type);
        memberThird.setClientType(clientType);
        memberThirdMapper.updateById(memberThird);
    }

    @Override
    public void delete(String partnerId, Integer type) {
        Map map = new HashMap();
        map.put("openId",partnerId);
        map.put("type",type);
        memberThirdMapper.delete(map);
    }
}
