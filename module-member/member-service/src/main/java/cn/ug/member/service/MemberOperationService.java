package cn.ug.member.service;

import cn.ug.bean.base.SerializeObject;

import java.util.Map;

/**
 * @Author zhangweijie
 * @Date 2019/4/26 0026
 * @time 上午 10:15
 **/
public interface MemberOperationService {

    /**
     * 添加用户操作记录
     * @param memberId
     * @param type
     * @return
     */
    boolean addClick(String memberId,Integer type);

    /**
     *
     * @param memberId
     * @return
     */
    Map findLoginNum(String memberId);


    /**
     * 查询用户提单
     * @param memberId  会员id
     * @param id        提单id
     * @return
     */
    int findTbillPopup(String memberId,String id);

    /**
     * 点赞成功发送黄金红包
     * @param channelId
     * @param memberId
     * @param friendId
     * @return
     */
    SerializeObject giveLike(String channelId, String memberId,String friendId);
}
