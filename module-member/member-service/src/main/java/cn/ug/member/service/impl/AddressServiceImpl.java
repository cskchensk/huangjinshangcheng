package cn.ug.member.service.impl;


import cn.ug.account.bean.AreaBean;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.feign.AreaService;
import cn.ug.member.bean.AddressBean;
import cn.ug.member.mapper.AddressMapper;
import cn.ug.member.mapper.entity.AddressEntity;
import cn.ug.member.service.AddressService;
import cn.ug.member.web.submit.AddressSubmit;
import cn.ug.service.impl.BaseServiceImpl;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author ywl
 * @date 2018/1/14 0014
 */
@Service
public class AddressServiceImpl extends BaseServiceImpl implements AddressService {

    @Resource
    private AreaService areaService;
    @Resource
    private AddressMapper addressMapper;
    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public int count(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return addressMapper.queryForCount(memberId);
        }
        return 0;
    }

    @Override
    public SerializeObject getDefaultAddress() {

        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }

        AddressEntity entity = addressMapper.getDefaultAddressByUserId(loginBean.getId());
        if (entity == null) {
            List<AddressEntity> list = addressMapper.findListByUserId(loginBean.getId());
            if (CollectionUtils.isNotEmpty(list)) {
                entity = list.get(0);
            }
        }
        AddressBean bean = dozerBeanMapper.map(entity, AddressBean.class);
        //省份
        if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getProvince())) {
            SerializeObject<AreaBean> o = areaService.find(entity.getProvince());
            if (o.getCode() == ResultType.NORMAL) {
                AreaBean areaBean = o.getData();
                bean.setProvinceName(areaBean.getName());
            }
        }
        //城市
        if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getCity())) {
            SerializeObject<AreaBean> o = areaService.find(entity.getCity());
            if (o.getCode() == ResultType.NORMAL) {
                AreaBean areaBean = o.getData();
                bean.setCityName(areaBean.getName());
            }
        }
        //地区
        if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getArea())) {
            SerializeObject<AreaBean> o = areaService.find(entity.getArea());
            if (o.getCode() == ResultType.NORMAL) {
                AreaBean areaBean = o.getData();
                bean.setAreaName(areaBean.getName());
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", bean);
    }

    @Override
    @Transactional
    public SerializeObject insert(AddressSubmit submit) {

        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }
        int count = addressMapper.count(loginBean.getId());
        if (count >= 10) {
            return new SerializeObject<>(ResultType.NORMAL, "收货地址不能超过10个");
        }
        AddressEntity entity = dozerBeanMapper.map(submit, AddressEntity.class);
        entity.setUserId(loginBean.getId());
        entity.setIsDefault(count == 0 ? 1 : 0);

        if (count > 0 && entity.getIsDefault() > 0) {
            addressMapper.updateDefaultAddressByUserId(loginBean.getId());
        }
        int success = addressMapper.insert(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError<>("00000005");
    }

    @Override
    public SerializeObject update(AddressSubmit submit) {

        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }
        AddressEntity entity = addressMapper.findById(submit.getId(), loginBean.getId());
        if (entity == null) {
            return new SerializeObjectError<>("00000002");
        }

        if (submit.getIsDefault() == null || submit.getIsDefault() == 0) {
            submit.setIsDefault(entity.getIsDefault());
        }

        entity = dozerBeanMapper.map(submit, AddressEntity.class);
        entity.setUserId(loginBean.getId());
        if (submit.getIsDefault() > 0) {
            addressMapper.updateDefaultAddressByUserId(loginBean.getId());
        }

        int success = addressMapper.update(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError<>("00000005");
    }

    @Override
    public SerializeObject findById(Long id) {

        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }
        AddressEntity entity = addressMapper.findById(id, loginBean.getId());
        if (entity == null) {
            return new SerializeObjectError<>("00000002");
        }
        AddressBean bean = dozerBeanMapper.map(entity, AddressBean.class);
        //省份
        if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getProvince())) {
            SerializeObject<AreaBean> o = areaService.find(entity.getProvince());
            if (o.getCode() == ResultType.NORMAL) {
                AreaBean areaBean = o.getData();
                bean.setProvinceName(areaBean.getName());
            }
        }
        //城市
        if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getCity())) {
            SerializeObject<AreaBean> o = areaService.find(entity.getCity());
            if (o.getCode() == ResultType.NORMAL) {
                AreaBean areaBean = o.getData();
                bean.setCityName(areaBean.getName());
            }
        }
        //地区
        if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getArea())) {
            SerializeObject<AreaBean> o = areaService.find(entity.getArea());
            if (o.getCode() == ResultType.NORMAL) {
                AreaBean areaBean = o.getData();
                bean.setAreaName(areaBean.getName());
            }
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public AddressBean getAddress(long addressId) {
        if (addressId < 1) {
            return null;
        }
        AddressEntity entity = addressMapper.selectById(addressId);
        if (entity == null) {
            return null;
        }
        return dozerBeanMapper.map(entity, AddressBean.class);
    }

    @Override
    public SerializeObject findList() {

        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }

        List<AddressEntity> list = addressMapper.findListByUserId(loginBean.getId());
        if (CollectionUtils.isNotEmpty(list)) {
            Iterator<AddressEntity> iterator = list.iterator();
            List<AddressBean> beans = new ArrayList<>(list.size());
            while (iterator.hasNext()) {
                AddressEntity entity = iterator.next();
                AddressBean bean = dozerBeanMapper.map(entity, AddressBean.class);
                //省份
                if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getProvince())) {
                    SerializeObject<AreaBean> o = areaService.find(entity.getProvince());
                    if (o.getCode() == ResultType.NORMAL) {
                        AreaBean areaBean = o.getData();
                        bean.setProvinceName(areaBean.getName());
                    }
                }
                //城市
                if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getCity())) {
                    SerializeObject<AreaBean> o = areaService.find(entity.getCity());
                    if (o.getCode() == ResultType.NORMAL) {
                        AreaBean areaBean = o.getData();
                        bean.setCityName(areaBean.getName());
                    }
                }
                //地区
                if (org.apache.commons.lang3.StringUtils.isNotBlank(entity.getArea())) {
                    SerializeObject<AreaBean> o = areaService.find(entity.getArea());
                    if (o.getCode() == ResultType.NORMAL) {
                        AreaBean areaBean = o.getData();
                        bean.setAreaName(areaBean.getName());
                    }
                }
                beans.add(bean);
            }
            return new SerializeObject<>(ResultType.NORMAL, "00000001", beans);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", null);
    }

    @Override
    public SerializeObject deleteById(Long id) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isEmpty(loginBean.getId())) {
            return new SerializeObjectError<>("00000102");
        }
        //判断是否只有一个地址
        List<AddressEntity> addressEntityList = addressMapper.findListByUserId(loginBean.getId());
        if (CollectionUtils.isNotEmpty(addressEntityList) && addressEntityList.size() == 1) {
            return new SerializeObjectError<>("14000250");
        }

        int success = addressMapper.deleteByIdAndUserId(id, loginBean.getId());
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError<>("00000005");
    }

}
