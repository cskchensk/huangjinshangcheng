package cn.ug.member.mapper.entity;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table member_operation_record
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class MemberOperationRecord implements Serializable {
    /**
     * Database Column Remarks:
     *   主键id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     *   会员id
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.member_id
     *
     * @mbg.generated
     */
    private String memberId;

    /**
     * Database Column Remarks:
     *   引导页点击数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.guide_num
     *
     * @mbg.generated
     */
    private Integer guideNum;

    /**
     * Database Column Remarks:
     *   买金点击数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.buy_gold_num
     *
     * @mbg.generated
     */
    private Integer buyGoldNum;

    /**
     * Database Column Remarks:
     *   回租点击数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.lease_back_num
     *
     * @mbg.generated
     */
    private Integer leaseBackNum;

    /**
     * Database Column Remarks:
     *   查看优惠卷点击数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.coupon_num
     *
     * @mbg.generated
     */
    private Integer couponNum;

    /**
     * Database Column Remarks:
     *   了解更多点击数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.learn_more_num
     *
     * @mbg.generated
     */
    private Integer learnMoreNum;

    /**
     * Database Column Remarks:
     *   跳过点击数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.skip_num
     *
     * @mbg.generated
     */
    private Integer skipNum;

    /**
     * Database Column Remarks:
     *   登陆次数
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.login_num
     *
     * @mbg.generated
     */
    private Integer loginNum;

    /**
     * 首次提单引导弹框状态0 不存在  1.存在
     */
    private Integer buyGoldPendingStatus;

    /**
     * 回租到期提单引导弹框状态0 没有  1.存在
     */
    private Integer buyGoldLeasebackStatus;
    /**
     * Database Column Remarks:
     *   添加时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.add_time
     *
     * @mbg.generated
     */
    private Date addTime;

    /**
     * Database Column Remarks:
     *   更新时间
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column member_operation_record.modify_time
     *
     * @mbg.generated
     */
    private Date modifyTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table member_operation_record
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.id
     *
     * @return the value of member_operation_record.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.id
     *
     * @param id the value for member_operation_record.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.member_id
     *
     * @return the value of member_operation_record.member_id
     *
     * @mbg.generated
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.member_id
     *
     * @param memberId the value for member_operation_record.member_id
     *
     * @mbg.generated
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId == null ? null : memberId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.guide_num
     *
     * @return the value of member_operation_record.guide_num
     *
     * @mbg.generated
     */
    public Integer getGuideNum() {
        return guideNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.guide_num
     *
     * @param guideNum the value for member_operation_record.guide_num
     *
     * @mbg.generated
     */
    public void setGuideNum(Integer guideNum) {
        this.guideNum = guideNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.buy_gold_num
     *
     * @return the value of member_operation_record.buy_gold_num
     *
     * @mbg.generated
     */
    public Integer getBuyGoldNum() {
        return buyGoldNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.buy_gold_num
     *
     * @param buyGoldNum the value for member_operation_record.buy_gold_num
     *
     * @mbg.generated
     */
    public void setBuyGoldNum(Integer buyGoldNum) {
        this.buyGoldNum = buyGoldNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.lease_back_num
     *
     * @return the value of member_operation_record.lease_back_num
     *
     * @mbg.generated
     */
    public Integer getLeaseBackNum() {
        return leaseBackNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.lease_back_num
     *
     * @param leaseBackNum the value for member_operation_record.lease_back_num
     *
     * @mbg.generated
     */
    public void setLeaseBackNum(Integer leaseBackNum) {
        this.leaseBackNum = leaseBackNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.coupon_num
     *
     * @return the value of member_operation_record.coupon_num
     *
     * @mbg.generated
     */
    public Integer getCouponNum() {
        return couponNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.coupon_num
     *
     * @param couponNum the value for member_operation_record.coupon_num
     *
     * @mbg.generated
     */
    public void setCouponNum(Integer couponNum) {
        this.couponNum = couponNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.learn_more_num
     *
     * @return the value of member_operation_record.learn_more_num
     *
     * @mbg.generated
     */
    public Integer getLearnMoreNum() {
        return learnMoreNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.learn_more_num
     *
     * @param learnMoreNum the value for member_operation_record.learn_more_num
     *
     * @mbg.generated
     */
    public void setLearnMoreNum(Integer learnMoreNum) {
        this.learnMoreNum = learnMoreNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.skip_num
     *
     * @return the value of member_operation_record.skip_num
     *
     * @mbg.generated
     */
    public Integer getSkipNum() {
        return skipNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.skip_num
     *
     * @param skipNum the value for member_operation_record.skip_num
     *
     * @mbg.generated
     */
    public void setSkipNum(Integer skipNum) {
        this.skipNum = skipNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.login_num
     *
     * @return the value of member_operation_record.login_num
     *
     * @mbg.generated
     */
    public Integer getLoginNum() {
        return loginNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.login_num
     *
     * @param loginNum the value for member_operation_record.login_num
     *
     * @mbg.generated
     */
    public void setLoginNum(Integer loginNum) {
        this.loginNum = loginNum;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.add_time
     *
     * @return the value of member_operation_record.add_time
     *
     * @mbg.generated
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.add_time
     *
     * @param addTime the value for member_operation_record.add_time
     *
     * @mbg.generated
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column member_operation_record.modify_time
     *
     * @return the value of member_operation_record.modify_time
     *
     * @mbg.generated
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column member_operation_record.modify_time
     *
     * @param modifyTime the value for member_operation_record.modify_time
     *
     * @mbg.generated
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }


    public Integer getBuyGoldPendingStatus() {
        return buyGoldPendingStatus;
    }

    public void setBuyGoldPendingStatus(Integer buyGoldPendingStatus) {
        this.buyGoldPendingStatus = buyGoldPendingStatus;
    }

    public Integer getBuyGoldLeasebackStatus() {
        return buyGoldLeasebackStatus;
    }

    public void setBuyGoldLeasebackStatus(Integer buyGoldLeasebackStatus) {
        this.buyGoldLeasebackStatus = buyGoldLeasebackStatus;
    }
}