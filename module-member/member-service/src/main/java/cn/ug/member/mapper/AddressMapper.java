package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.AddressEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/14.
 */
@Mapper
public interface AddressMapper {

    int insert(AddressEntity entity);

    int update(AddressEntity entity);

    int deleteByIdAndUserId(@Param("id") Long id, @Param("userId") String userId);

    AddressEntity findById(@Param("id") Long id, @Param("userId") String userId);

    AddressEntity selectById(@Param("id") long id);

    int count(@Param("userId") String userId);
    int queryForCount(@Param("userId") String userId);

    List<AddressEntity> findListByUserId(@Param("userId") String userId);

    int updateDefaultAddressByUserId(@Param("userId") String userId);

    AddressEntity getDefaultAddressByUserId(@Param("userId") String userId);
}
