package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.MemberGroupStatistics;

import java.util.List;
import java.util.Map;

public interface MemberGroupStatisticsMapper {
    List<MemberGroupStatistics> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    int insert(MemberGroupStatistics statistics);
}
