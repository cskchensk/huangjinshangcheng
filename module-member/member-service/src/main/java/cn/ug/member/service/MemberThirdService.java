package cn.ug.member.service;

import cn.ug.member.bean.response.MemberThirdFindBean;

public interface MemberThirdService {

    /**
     * 账号绑定
     * @param partnerId
     * @param memberId
     * @param type
     */
    void save(String partnerId,String memberId,Integer type,Integer clientType);


    /**
     * 查询
     * @param partnerId
     * @param memberId
     * @param type
     * @return
     */
    MemberThirdFindBean find(String partnerId, String memberId, Integer type);

    /**
     * 修改
     * @param partnerId
     * @param memberId
     * @param type
     */
    void update(String partnerId, String memberId, Integer type,Integer clientType);

    /**
     * 删除
     * @param partnerId
     * @param type
     */
    void delete(String partnerId, Integer type);
}
