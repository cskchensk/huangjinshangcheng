package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.MemberGroup;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MemberGroupMapper {
    List<MemberGroup> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<MemberGroup> queryForEnabledList(@Param("type")int type);

    int insert(MemberGroup memberGroup);
    int update(MemberGroup memberGroup);

    MemberGroup findById(@Param("id")int id);
    int updateStatus(@Param("id")int id, @Param("status")int status);
    int delete(@Param("id")int id);

    int countRegisterAndNotBindCardNum(Map<String, Object> params);
    int countBindCardAndNotTradeNum(Map<String, Object> params);
    int countTradeNum(Map<String, Object> params);
    int countMemberTradeNum(@Param("memberId")String memberId);
}
