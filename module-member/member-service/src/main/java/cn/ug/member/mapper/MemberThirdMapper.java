package cn.ug.member.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.member.bean.response.MemberThirdFindBean;
import cn.ug.member.mapper.entity.MemberThird;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public interface MemberThirdMapper extends BaseMapper<MemberThird> {

    int insert(MemberThird memberThird);

    MemberThirdFindBean find(MemberThird memberThird);

    int updateById(MemberThird memberThird);

    int delete(Map map);
}
