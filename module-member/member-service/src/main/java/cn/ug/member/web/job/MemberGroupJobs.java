package cn.ug.member.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.member.mapper.MemberGroupRecordMapper;
import cn.ug.member.mapper.MemberGroupStatisticsMapper;
import cn.ug.member.mapper.entity.MemberGroup;
import cn.ug.member.mapper.entity.MemberGroupStatistics;
import cn.ug.member.service.MemberGroupService;
import cn.ug.util.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class MemberGroupJobs {
    private static Logger logger = LoggerFactory.getLogger(MemberGroupJobs.class);
    @Autowired
    private MemberGroupService memberGroupService;
    @Autowired
    private RedisGlobalLock redisGlobalLock;
    @Autowired
    private MemberGroupStatisticsMapper memberGroupStatisticsMapper;
    @Autowired
    private MemberGroupRecordMapper memberGroupRecordMapper;

    @Scheduled(cron = "0 0 1 * * ?")
    //@Scheduled(cron = "0 30 14 * * ?")
    public void executeRoutineCalculation() {
        logger.info("开始执行用户群组计算...");
        String key = "MemberGroupJobs:executeRoutineCalculation:" + UF.getFormatDateTime("yyyy-MM-dd", UF.getDateTime());
        if(!redisGlobalLock.lock(key, 3, TimeUnit.DAYS)) {
            return;
        }
        List<MemberGroup> groups = memberGroupService.queryForEnabledList(-1);
        if (groups != null && groups.size() > 0) {
            for (MemberGroup group : groups) {
                // 处理系统创建类型的任务
                if (group.getType() == 1) {
                    if (group.getCalculationCategory() == 1) {
                        if (group.getLabelCategory() == 1) {
                            Map<String, Object> params = new HashMap<String, Object>();
                            int num = memberGroupService.countRegisterAndNotBindCardNum(params);
                            group.setPersons(num);
                            group.setModifyTime(UF.getFormatDateTime(UF.getDateTime()));
                            if (memberGroupService.save(group)) {
                                MemberGroupStatistics memberGroupStatistics = new MemberGroupStatistics();
                                memberGroupStatistics.setAddTime(UF.getFormatDateTime(UF.getDateTime()));
                                memberGroupStatistics.setGroupId(group.getId());
                                memberGroupStatistics.setPersons(num);
                                memberGroupStatisticsMapper.insert(memberGroupStatistics);
                            }
                        } else if (group.getLabelCategory() == 2) {
                            Map<String, Object> params = new HashMap<String, Object>();
                            int num = memberGroupService.countBindCardAndNotTradeNum(params);
                            group.setPersons(num);
                            group.setModifyTime(UF.getFormatDateTime(UF.getDateTime()));
                            if (memberGroupService.save(group)) {
                                MemberGroupStatistics memberGroupStatistics = new MemberGroupStatistics();
                                memberGroupStatistics.setAddTime(UF.getFormatDateTime(UF.getDateTime()));
                                memberGroupStatistics.setGroupId(group.getId());
                                memberGroupStatistics.setPersons(num);
                                memberGroupStatisticsMapper.insert(memberGroupStatistics);
                            }
                        } else if (group.getLabelCategory() == 3) {
                            Map<String, Object> params = new HashMap<String, Object>();
                            int num = memberGroupService.countTradeNum(params);
                            group.setPersons(num);
                            group.setModifyTime(UF.getFormatDateTime(UF.getDateTime()));
                            if (memberGroupService.save(group)) {
                                MemberGroupStatistics memberGroupStatistics = new MemberGroupStatistics();
                                memberGroupStatistics.setAddTime(UF.getFormatDateTime(UF.getDateTime()));
                                memberGroupStatistics.setGroupId(group.getId());
                                memberGroupStatistics.setPersons(num);
                                memberGroupStatisticsMapper.insert(memberGroupStatistics);
                            }
                        }
                    }
                // 处理自定义类型的任务
                } else if (group.getType() == 2) {
                    if (group.getCalculationCategory() == 1) {
                        memberGroupService.execute(group);
                    }
                }
            }
        }
    }
}
