package cn.ug.member.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.member.bean.AddressBean;
import cn.ug.member.bean.MemberLogisticsBean;
import cn.ug.member.bean.response.MemberAddressBaseBean;
import cn.ug.member.bean.response.MemberAddressBean;
import cn.ug.member.service.AddressService;
import cn.ug.member.service.MemberAddressService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.io.Serializable;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("memberAddress")
public class MemberAddressController {
    @Autowired
    private AddressService addressService;
    @Resource
    private MemberAddressService memberAddressService;

    private static Logger logger = LoggerFactory.getLogger(MemberAddressController.class);

    @RequestMapping(value = "/address", method = GET)
    public SerializeObject<MemberAddressBean> get(String id) {
        MemberAddressBean addressBean = memberAddressService.findById(id);
        return new SerializeObject<MemberAddressBean>(ResultType.NORMAL, addressBean);
    }
    /**
     * 根据ID查找信息
     * @param memberId		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "findByMemberId", method = GET)
    public SerializeObject findByMemberId(String memberId) {
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObject(ResultType.ERROR, "00000002");
        }
        MemberAddressBaseBean entity = memberAddressService.findByMemberId(memberId);
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    @RequestMapping(value = "/mall/address", method = GET)
    public SerializeObject<AddressBean> getAddress(long addressId) {
        if(addressId < 1) {
            return new SerializeObject(ResultType.ERROR, "00000002");
        }
        AddressBean entity = addressService.getAddress(addressId);
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    @RequestMapping(value = "/quantity", method = GET)
    public SerializeObject<Integer> getAddressNum(String memberId) {
        int num = addressService.count(memberId);
        return new SerializeObject<>(ResultType.NORMAL, num);
    }

    @RequestMapping(value = "/logistics/address", method = GET)
    public SerializeObject<MemberLogisticsBean> getLogisticsAddress(long addressId) {
        if(addressId < 1) {
            return new SerializeObject(ResultType.ERROR, "00000002");
        }
        MemberLogisticsBean entity = memberAddressService.getAddress(addressId);
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 根据ID查找信息
     * @return			    记录集
     */
    @RequestMapping(value = "findById", method = GET)
    public SerializeObject findById(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        logger.info("----进来了---"+loginBean);
        Ensure.that(loginBean == null).isTrue("00000002");
        MemberAddressBaseBean entity = memberAddressService.findByMemberId(loginBean.getId());
        if(entity != null){
            return new SerializeObject<>(ResultType.NORMAL, entity);
        }else{
            return new SerializeObject<>(ResultType.ERROR, "00000003");
        }

    }

    /**
     * 新增修改收获地址
     * @param accessToken
     * @param memberAddressBean
     * @return
     */
    @PostMapping
    public SerializeObject save(@RequestHeader String accessToken, MemberAddressBean memberAddressBean){
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean == null) Ensure.that(true).isTrue("00000002");
        memberAddressBean.setMemberId(loginBean.getId());
        if(StringUtils.isBlank(memberAddressBean.getId())){
            memberAddressService.save(memberAddressBean);
        }else{
            memberAddressService.update(memberAddressBean);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }
}
