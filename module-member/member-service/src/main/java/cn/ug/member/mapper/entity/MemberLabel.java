package cn.ug.member.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberLabel extends BaseEntity implements Serializable {
    private int bornYear;
    private int gender;
    private String memberId;
    private String mobile;
    private String name;
    private String channelId;
    private String channelName;
    private String ip1;
    private String ip2;
    private String ip3;
    private String commonIp;
    private String nativePlace;
    private String mobilePlace;
    private String mobileCompany;
    private int rvalue;
    private int fvalue;
    private BigDecimal mvalue;
    private String rfmCategory1;
    private String rfmCategory2;
    private int rfm1;
    private int rfm2;
    private int rfmChange;
    private String label;
    private int score;
    private String recentTradeTime;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getBornYear() {
        return bornYear;
    }

    public void setBornYear(int bornYear) {
        this.bornYear = bornYear;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp1() {
        return ip1;
    }

    public void setIp1(String ip1) {
        this.ip1 = ip1;
    }

    public String getIp2() {
        return ip2;
    }

    public void setIp2(String ip2) {
        this.ip2 = ip2;
    }

    public String getIp3() {
        return ip3;
    }

    public void setIp3(String ip3) {
        this.ip3 = ip3;
    }

    public String getCommonIp() {
        return commonIp;
    }

    public void setCommonIp(String commonIp) {
        this.commonIp = commonIp;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public String getMobilePlace() {
        return mobilePlace;
    }

    public void setMobilePlace(String mobilePlace) {
        this.mobilePlace = mobilePlace;
    }

    public String getMobileCompany() {
        return mobileCompany;
    }

    public void setMobileCompany(String mobileCompany) {
        this.mobileCompany = mobileCompany;
    }

    public int getRvalue() {
        return rvalue;
    }

    public void setRvalue(int rvalue) {
        this.rvalue = rvalue;
    }

    public int getFvalue() {
        return fvalue;
    }

    public void setFvalue(int fvalue) {
        this.fvalue = fvalue;
    }

    public BigDecimal getMvalue() {
        return mvalue;
    }

    public void setMvalue(BigDecimal mvalue) {
        this.mvalue = mvalue;
    }

    public String getRfmCategory1() {
        return rfmCategory1;
    }

    public void setRfmCategory1(String rfmCategory1) {
        this.rfmCategory1 = rfmCategory1;
    }

    public String getRfmCategory2() {
        return rfmCategory2;
    }

    public void setRfmCategory2(String rfmCategory2) {
        this.rfmCategory2 = rfmCategory2;
    }

    public int getRfm1() {
        return rfm1;
    }

    public void setRfm1(int rfm1) {
        this.rfm1 = rfm1;
    }

    public int getRfm2() {
        return rfm2;
    }

    public void setRfm2(int rfm2) {
        this.rfm2 = rfm2;
    }

    public int getRfmChange() {
        return rfmChange;
    }

    public void setRfmChange(int rfmChange) {
        this.rfmChange = rfmChange;
    }

    public String getLabel() {
        return label;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRecentTradeTime() {
        return recentTradeTime;
    }

    public void setRecentTradeTime(String recentTradeTime) {
        this.recentTradeTime = recentTradeTime;
    }
}