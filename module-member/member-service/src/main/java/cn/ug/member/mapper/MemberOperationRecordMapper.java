package cn.ug.member.mapper;


import cn.ug.member.mapper.entity.MemberOperationRecord;
import org.apache.ibatis.annotations.Param;

public interface MemberOperationRecordMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table member_operation_record
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table member_operation_record
     *
     * @mbg.generated
     */
    int insertSelective(MemberOperationRecord record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table member_operation_record
     *
     * @mbg.generated
     */
    MemberOperationRecord selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table member_operation_record
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(MemberOperationRecord record);

    /**
     * 根据会员id查询操作信息
     * @param memberId
     * @return
     */
    MemberOperationRecord selectBymemberId(@Param("memberId") String memberId);
}