package cn.ug.member.web.controller;

import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.member.bean.MemberLevelBean;
import cn.ug.member.service.MemberLevelService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 会员等级
 * @author kaiwotech
 */
@RestController
@RequestMapping("level")
public class MemberLevelController extends BaseController {

    @Resource
    private MemberLevelService memberLevelService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        MemberLevelBean entity = memberLevelService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequiresPermissions("member:level:update")
    @RequestMapping(method = POST)
    public SerializeObject update(@RequestHeader String accessToken, MemberLevelBean entity) {
        if(null == entity || StringUtils.isBlank(entity.getName())) {
            return new SerializeObjectError("14000101");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = memberLevelService.save(entity);
        } else {
            val = memberLevelService.update(entity.getId(), entity);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("member:level:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = memberLevelService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("member:level:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = memberLevelService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据积分查找信息
     * @param integral	        积分
     * @return			        记录集
     */
    @RequestMapping(value = "findByIntegral", method = GET)
    public SerializeObject<MemberLevelBean> findByIntegral(Integer integral) {
        if(null == integral) {
            integral = 0;
        }
        MemberLevelBean entity = memberLevelService.findByIntegral(integral);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list" , method = GET)
    public SerializeObject<List<MemberLevelBean>> list(@RequestHeader String accessToken, Order order, Integer status, String keyword) {
        keyword = UF.toString(keyword);
        List<MemberLevelBean> list = memberLevelService.findList(order.getOrder(), order.getSort(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param status	        状态	 0：全部 1：正常  2：特殊会员组
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<MemberLevelBean>> query(@RequestHeader String accessToken, Order order, Page page, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        keyword = UF.toString(keyword);

        DataTable<MemberLevelBean> dataTable = memberLevelService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequiresPermissions("member:level")
    @RequestMapping(value = "verify",method = GET)
    public SerializeObject verify(@RequestHeader String accessToken, MemberLevelBean entity) {
        if(memberLevelService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }
}
