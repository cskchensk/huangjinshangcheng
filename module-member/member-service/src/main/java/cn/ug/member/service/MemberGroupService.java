package cn.ug.member.service;

import cn.ug.member.mapper.entity.MemberGroup;
import cn.ug.member.mapper.entity.MemberGroupRecord;
import cn.ug.member.mapper.entity.MemberGroupStatistics;

import java.util.List;
import java.util.Map;

public interface MemberGroupService {
    List<MemberGroup> query(String name, String startTime, String endTime, String order, String sort, int offset, int size);
    int count(String name, String startTime, String endTime);
    boolean save(MemberGroup memberGroup);
    MemberGroup findById(int id);
    boolean updateStatus(int id, int status);
    boolean delete(int id);

    List<MemberGroup> queryForEnabledList(int type);

    List<MemberGroupStatistics> query(int groupId, int startNum, int endNum, String startTime, String endTime, String order, String sort, int offset, int size);
    int count(int groupId, int startNum, int endNum, String startTime, String endTime);

    List<MemberGroupRecord> query(int groupId, String calculationDate);

    public void execute(MemberGroup group);
    int countRegisterAndNotBindCardNum(Map<String, Object> params);
    int countBindCardAndNotTradeNum(Map<String, Object> params);
    int countTradeNum(Map<String, Object> params);

    int getGroupId(String memberId, List<Integer> groupIds);
}
