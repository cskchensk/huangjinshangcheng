package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.MemberGroupRecord;

import java.util.List;
import java.util.Map;

public interface MemberGroupRecordMapper {
    List<MemberGroupRecord> query(Map<String, Object> params);
    int insertForRegisterUsers(Map<String, Object> params);
    int insertForBindCardUsers(Map<String, Object> params);
    int insertForTradeAndLeasebackUsers(Map<String, Object> params);
}
