package cn.ug.member.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.member.mapper.entity.ChannelShareMember;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author lvkk
 * @date 2019/11/1.
 */
@Component
public interface ChannelShareMemberMapper extends BaseMapper<ChannelShareMember> {

    List<ChannelShareMember> findJoinMember(Map paramMap);
}
