package cn.ug.member.mapper;

import cn.ug.member.bean.LabelSearchBean;
import cn.ug.member.mapper.entity.MemberLabel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberLabelMapper {
    int insert(MemberLabel label);
    int update(MemberLabel label);
    MemberLabel selectByMemberId(@Param("memberId")String memberId);
    List<MemberLabel> query(@Param("offset")int offset, @Param("size")int size, @Param("beans")List<LabelSearchBean> beans);
    int count(@Param("beans")List<LabelSearchBean> beans);
    int updateInBatch(@Param("labels")List<MemberLabel> labels);
}
