package cn.ug.member.service.impl;

import cn.ug.member.bean.MemberLevelBean;
import cn.ug.member.mapper.MemberLevelMapper;
import cn.ug.member.mapper.entity.MemberLevel;
import cn.ug.member.service.MemberLevelService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class MemberLevelServiceImpl extends BaseServiceImpl implements MemberLevelService {
	
	@Resource
	private MemberLevelMapper memberLevelMapper;

	@Resource
	private Config config;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(MemberLevelBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getName())) {
			Ensure.that(true).isTrue("14000101");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }

		MemberLevel entity = dozerBeanMapper.map(entityBean, MemberLevel.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		memberLevelMapper.insert(entity);

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int update(String id, MemberLevelBean entityBean) {
        // 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getId()) || StringUtils.isBlank(entityBean.getName())) {
			Ensure.that(true).isTrue("14000101");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }

		MemberLevel entity = memberLevelMapper.findById(entityBean.getId());
		dozerBeanMapper.map(entityBean, entity);
		memberLevelMapper.update(entity);
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return memberLevelMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return memberLevelMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return memberLevelMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public boolean exists(MemberLevelBean entityBean, String id) {
		if(null == entityBean || StringUtils.isBlank(entityBean.getName())) {
			return false;
		}

		int rows = memberLevelMapper.exists(
				getParams()
						.put("name", entityBean.getName())
						.put("id", id)
						.toMap()
		);
		return rows > 0;
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public MemberLevelBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		MemberLevel entity = memberLevelMapper.findById(id);
		if(null == entity) {
			return null;
		}

		MemberLevelBean entityBean = dozerBeanMapper.map(entity, MemberLevelBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<MemberLevelBean> findList(String order, String sort, int status, String keyword) {
		return query(order, sort, null, status, keyword);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<MemberLevelBean> query(String order, String sort, int pageNum, int pageSize, int status, String keyword){
		Page<MemberLevelBean> page = PageHelper.startPage(pageNum, pageSize);
		List<MemberLevelBean> list = query(order, sort, null, status, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public MemberLevelBean findByIntegral(int integral) {
		// 查询正常会员等级
		List<MemberLevelBean> list = query(null, null, null, 1, null);
		if(null == list || list.isEmpty()) {
			return null;
		}
		for (MemberLevelBean o : list) {
			if(integral >= o.getMinimum() && integral <= o.getMaximum()) {
				return o;
			}
		}
		return null;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int updateStatus(String[] id, int status) {
		if(id == null || id.length<=0){
			return 0;
		}

		return memberLevelMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("status", status)
						.toMap()
		);
	}

	/**
	 * 获取数据列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param name			会员名
	 * @param status		状态	 0：全部 1：正常 2：被锁定
	 * @param keyword		关键字
	 * @return				列表
	 */
	private List<MemberLevelBean> query(String order, String sort, String name, int status, String keyword){
		List<MemberLevelBean> dataList = new ArrayList<>();
		List<MemberLevel> list = memberLevelMapper.query(
				getParams(keyword, order, sort)
						.put("name", name)
						.put("status", status)
						.toMap());
		for (MemberLevel o : list) {
			MemberLevelBean objBean = dozerBeanMapper.map(o, MemberLevelBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}
}

