package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.MemberLevel;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 帐号管理
 * @author kaiwotech
 */
@Component
public interface MemberLevelMapper extends BaseMapper<MemberLevel> {
	
}