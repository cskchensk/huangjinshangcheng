package cn.ug.member.web.job;

import cn.ug.member.mapper.MemberUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ResetPasswordStatusJob {
    @Autowired
    private MemberUserMapper memberUserMapper;

    @Scheduled(cron = "0/10 1 0 * * ?")
    public void reset() {
        memberUserMapper.resetPasswordStatus();
    }
}