package cn.ug.member.service.impl;


import cn.ug.account.bean.AreaBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.feign.AreaService;
import cn.ug.member.bean.MemberLogisticsBean;
import cn.ug.member.bean.response.MemberAddressBaseBean;
import cn.ug.member.bean.response.MemberAddressBean;
import cn.ug.member.mapper.AddressMapper;
import cn.ug.member.mapper.MemberAddressMapper;
import cn.ug.member.mapper.MemberUserMapper;
import cn.ug.member.mapper.entity.AddressEntity;
import cn.ug.member.mapper.entity.MemberAddress;
import cn.ug.member.service.MemberAddressService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static cn.ug.util.ConstantUtil.MINUS;

/**
 * @author ywl
 * @date 2018/1/14 0014
 */
@Service
public class MemberAddressServiceImpl extends BaseServiceImpl implements MemberAddressService {
    @Resource
    private AddressMapper addressMapper;
    @Resource
    private AreaService areaService;
    @Resource
    private MemberAddressMapper memberAddressMapper;
    @Resource
    private MemberUserMapper memberUserMapper;
    @Resource
    private DozerBeanMapper dozerBeanMapper;


    @Override
    public int save(MemberAddressBean entityBean) {
        MemberAddress memberAddress = dozerBeanMapper.map(entityBean, MemberAddress.class);
        memberAddress.setId(UF.getRandomUUID());
        memberAddress.setIsDefault(2);
        memberAddressMapper.insert(memberAddress);
        return 0;
    }

    @Override
    public int update(MemberAddressBean entityBean) {
        MemberAddress entity = memberAddressMapper.queryDefaultMemberAddress(entityBean.getMemberId());
        MemberAddress memberAddress = dozerBeanMapper.map(entityBean, MemberAddress.class);
        memberAddressMapper.update(memberAddress);
        return 0;
    }

    @Override
    public MemberAddressBaseBean findByMemberId(String memberId) {
        MemberAddress memberAddress = memberAddressMapper.queryDefaultMemberAddress(memberId);
        if(memberAddress != null){
            MemberAddressBaseBean entity = dozerBeanMapper.map(memberAddress, MemberAddressBaseBean.class);
            //省份
            if(StringUtils.isNotBlank(memberAddress.getProvince())){
                SerializeObject<AreaBean> o = areaService.find(memberAddress.getProvince());
                if(o.getCode() == ResultType.NORMAL) {
                    AreaBean areaBean = o.getData();
                    entity.setProvinceName(areaBean.getName());
                }
            }
            //城市
            if(StringUtils.isNotBlank(memberAddress.getCity())){
                SerializeObject<AreaBean> o = areaService.find(memberAddress.getCity());
                if(o.getCode() == ResultType.NORMAL) {
                    AreaBean areaBean = o.getData();
                    entity.setCityName(areaBean.getName());
                }
            }
            //地区
            if(StringUtils.isNotBlank(memberAddress.getArea())){
                SerializeObject<AreaBean> o = areaService.find(memberAddress.getArea());
                if(o.getCode() == ResultType.NORMAL) {
                    AreaBean areaBean = o.getData();
                    entity.setAreaName(areaBean.getName());
                }
            }
            return entity;
        }
        return null;
    }

    @Override
    public MemberLogisticsBean getAddress(long addressId) {
        AddressEntity memberAddress = addressMapper.selectById(addressId);
        if(memberAddress != null){
            MemberLogisticsBean bean = new MemberLogisticsBean();
            String address = "";
            if(StringUtils.isNotBlank(memberAddress.getProvince())){
                SerializeObject<AreaBean> o = areaService.find(memberAddress.getProvince());
                if(o.getCode() == ResultType.NORMAL) {
                    AreaBean areaBean = o.getData();
                    address += areaBean.getName() + MINUS;
                }
            }
            //城市
            if(StringUtils.isNotBlank(memberAddress.getCity())){
                SerializeObject<AreaBean> o = areaService.find(memberAddress.getCity());
                if(o.getCode() == ResultType.NORMAL) {
                    AreaBean areaBean = o.getData();
                    address += areaBean.getName() + MINUS;
                }
            }
            //地区
            if(StringUtils.isNotBlank(memberAddress.getArea())){
                SerializeObject<AreaBean> o = areaService.find(memberAddress.getArea());
                if(o.getCode() == ResultType.NORMAL) {
                    AreaBean areaBean = o.getData();
                    address += areaBean.getName();
                }
            }
            if (StringUtils.isNotBlank(memberAddress.getAddress())) {
                address +=  MINUS + memberAddress.getAddress();
            }
            bean.setAddress(address);
            bean.setFullName(memberAddress.getName());
            bean.setTelephone(memberAddress.getMobile());
            return bean;
        }
        return null;
    }

    @Override
    public MemberAddressBean findById(String id) {
        if (StringUtils.isNotBlank(id)) {
            MemberAddress address = memberAddressMapper.findById(id);
            if (address != null) {
                MemberAddressBean memberAddress = dozerBeanMapper.map(address, MemberAddressBean.class);
                return memberAddress;
            }
        }
        return null;
    }
}
