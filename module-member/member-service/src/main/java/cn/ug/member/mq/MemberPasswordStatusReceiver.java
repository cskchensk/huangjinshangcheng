package cn.ug.member.mq;

import cn.ug.member.mapper.MemberUserMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static cn.ug.config.QueueName.QUEUE_MEMBER_PASSWORD_STATUS;

@Component
@RabbitListener(queues = QUEUE_MEMBER_PASSWORD_STATUS)
public class MemberPasswordStatusReceiver {
    private Log log = LogFactory.getLog(MemberPasswordStatusReceiver.class);
    @Autowired
    private MemberUserMapper memberUserMapper;

    @RabbitHandler
    public void process(MemberPasswordStatusMQ o) throws JsonProcessingException {
        log.info("解除会员登陆密码或交易锁定状态...");
        if (o != null && StringUtils.isNotBlank(o.getMemberId())) {
            if (o.getType() == 1) {
                Map<String,Object> map = new HashMap<String,Object>();
                map.put("id", o.getMemberId());
                map.put("passwordWrongTimes", 0);
                map.put("passwordLastTime", Calendar.getInstance().getTime());
                map.put("passwordStatus", 0);
                memberUserMapper.updateByPrimaryKeySelective(map);
            } else if (o.getType() == 2) {
                Map<String,Object> map = new HashMap<String,Object>();
                map.put("id", o.getMemberId());
                map.put("trdpassdWrongTimes", 0);
                map.put("trdpassdLastTime", Calendar.getInstance().getTime());
                map.put("trdpassdStatus", 0);
                memberUserMapper.updateByPrimaryKeySelective(map);
            }
        }
    }
}
