package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.MemberAddress;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author ywl
 * @date 2018/1/12 0012
 */
@Component
public interface MemberAddressMapper extends BaseMapper<MemberAddress> {

    /**
     * 查询默认收获地址
     * @param memberId  会员Id
     * @return
     */
    MemberAddress queryDefaultMemberAddress(String memberId);
    MemberAddress findById(@Param("id") String id);


    @Override
    default int exists(Map<String, Object> params) {
        return 0;
    }
}

