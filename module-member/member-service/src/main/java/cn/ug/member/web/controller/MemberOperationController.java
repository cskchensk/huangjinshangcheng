package cn.ug.member.web.controller;

import cn.ug.activity.bean.ChannelBean;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.feign.ChannelService;
import cn.ug.member.service.MemberOperationService;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author zhangweijie
 * @Date 2019/4/26 0026
 * @time 上午 10:03
 **/

@RestController
@RequestMapping("operation")
public class MemberOperationController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(MemberOperationController.class);

    @Autowired
    private MemberOperationService memberOperationService;
    @Autowired
    private ChannelService channelService;

    /**
     * 新手引导和登陆操作信息埋点和记录
     *
     * @param accessToken
     * @param type        1.登陆次数  2.引导页  3.买金点击数  4.回租点击数 5.查看优惠卷点击数 6.了解更多点击数 7.跳过点击数
     * @return
     */
    @PostMapping("/add/click")
    public SerializeObject addClick(@RequestHeader String accessToken, Integer type) {
        logger.info("用户操作请求类型： " + type);
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        if (memberOperationService.addClick(loginBean.getId(), type)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    /**
     * 查询登陆状态
     * @param accessToken
     * @return
     */
    @GetMapping("/find/login/num")
    public SerializeObject findLoginNum(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        return new SerializeObject(ResultType.NORMAL, memberOperationService.findLoginNum(loginBean.getId()));
    }

    /**
     * 查询提单弹框记录
     * @param accessToken
     * @return
     */
    @GetMapping("/find/tbill/popup/{id}")
    public SerializeObject findTbillPopup(@RequestHeader String accessToken,@PathVariable String id) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        return new SerializeObject(ResultType.NORMAL, memberOperationService.findTbillPopup(loginBean.getId(),id));
    }

    /**
     * 登陆成功后进行登陆次数添加操作
     * @param memberId
     * @return
     */
    @PostMapping("/login/add/click")
    public SerializeObject loginAddClick(String memberId) {
        logger.info("登陆操作请求： " + memberId);
        if (memberOperationService.addClick(memberId, 1)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    /**
     * 点赞接口
     * @param accessToken
     * @param memberId
     * @param friendId
     * @return
     */
    @PostMapping("/giveLike")
    public SerializeObject giveLike(@RequestHeader String accessToken, String channelId, String memberId, String friendId) {
        //参数校验
        if (StringUtils.isBlank(memberId) || StringUtils.isBlank(friendId)) {
            return new SerializeObject(ResultType.ERROR,"00000002");
        }
        if (memberId.equals(friendId)) {
            return new SerializeObject(ResultType.ERROR,"14000272");
        }

        if (StringUtils.isNotBlank(channelId)) {
            ChannelBean bean = null;
            SerializeObject<ChannelBean> channelBean = channelService.get(channelId);
            if (channelBean != null && channelBean.getCode() == ResultType.NORMAL) {
                bean = channelBean.getData();
            }
            if (bean == null) {
                return new SerializeObject(503,"14000274");
            }
        }

        return memberOperationService.giveLike(channelId,memberId,friendId);
    }



}
