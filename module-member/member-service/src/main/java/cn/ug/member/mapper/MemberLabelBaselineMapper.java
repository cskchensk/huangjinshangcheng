package cn.ug.member.mapper;

import cn.ug.member.mapper.entity.MemberLabelBaseline;

public interface MemberLabelBaselineMapper {
    MemberLabelBaseline findOne();
    MemberLabelBaseline selectRFMAvg();
    int updateByPrimaryKeySelective(MemberLabelBaseline labelBaseline);
}
