package cn.ug.member.mapper.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ChannelShareMember implements Serializable {
    private int id;
    //渠道主键
    private String channelId;
    //分享人主键
    private String memberId;
    //好友（被分享人）主键
    private String friendId;
    //添加时间
    private LocalDateTime addTime;
    //修改时间
    private LocalDateTime modifyTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }
}
