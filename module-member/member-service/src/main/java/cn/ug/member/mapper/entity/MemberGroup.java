package cn.ug.member.mapper.entity;

import java.io.Serializable;

public class MemberGroup implements Serializable {
    private int id;
    private int type;
    private String name;
    private int calculationCategory;
    private int labelCategory;
    private String addTime;
    private String modifyTime;
    private int status;
    private String createId;
    private String createName;
    private int persons;
    private String channelId;
    private String startTime;
    private String endTime;
    private String startLeasebackTime;
    private String endLeasebackTime;
    private int investmentMark;
    private int startInvestmentAmount;
    private int endInvestmentAmount;
    private int leasebackMark;
    private int startLeasebackNum;
    private int endLeasebackNum;
    private int deleted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCalculationCategory() {
        return calculationCategory;
    }

    public void setCalculationCategory(int calculationCategory) {
        this.calculationCategory = calculationCategory;
    }

    public int getLabelCategory() {
        return labelCategory;
    }

    public void setLabelCategory(int labelCategory) {
        this.labelCategory = labelCategory;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public int getPersons() {
        return persons;
    }

    public void setPersons(int persons) {
        this.persons = persons;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartLeasebackTime() {
        return startLeasebackTime;
    }

    public void setStartLeasebackTime(String startLeasebackTime) {
        this.startLeasebackTime = startLeasebackTime;
    }

    public String getEndLeasebackTime() {
        return endLeasebackTime;
    }

    public void setEndLeasebackTime(String endLeasebackTime) {
        this.endLeasebackTime = endLeasebackTime;
    }

    public int getInvestmentMark() {
        return investmentMark;
    }

    public void setInvestmentMark(int investmentMark) {
        this.investmentMark = investmentMark;
    }

    public int getStartInvestmentAmount() {
        return startInvestmentAmount;
    }

    public void setStartInvestmentAmount(int startInvestmentAmount) {
        this.startInvestmentAmount = startInvestmentAmount;
    }

    public int getEndInvestmentAmount() {
        return endInvestmentAmount;
    }

    public void setEndInvestmentAmount(int endInvestmentAmount) {
        this.endInvestmentAmount = endInvestmentAmount;
    }

    public int getLeasebackMark() {
        return leasebackMark;
    }

    public void setLeasebackMark(int leasebackMark) {
        this.leasebackMark = leasebackMark;
    }

    public int getStartLeasebackNum() {
        return startLeasebackNum;
    }

    public void setStartLeasebackNum(int startLeasebackNum) {
        this.startLeasebackNum = startLeasebackNum;
    }

    public int getEndLeasebackNum() {
        return endLeasebackNum;
    }

    public void setEndLeasebackNum(int endLeasebackNum) {
        this.endLeasebackNum = endLeasebackNum;
    }
}
