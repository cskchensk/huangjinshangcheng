package cn.ug.member.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 会员等级
 * @author kaiwotech
 */
public class MemberLevel extends BaseEntity implements java.io.Serializable {

	/** 等级名称 */
	private String name;
	/** 积分下限 */
	private Integer minimum;
	/** 积分上限 */
	private Integer maximum;
	/** 折扣（默认100%） */
	private BigDecimal discount;
	/** 状态 1：正常  2：特殊会员组 */
	private Integer status;
	/** 备注 */
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getMinimum() {
		return minimum;
	}

	public void setMinimum(Integer minimum) {
		this.minimum = minimum;
	}

	public Integer getMaximum() {
		return maximum;
	}

	public void setMaximum(Integer maximum) {
		this.maximum = maximum;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
