package cn.ug.member.mapper.entity;

import cn.ug.activity.bean.ChannelBean;
import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 * @author ywl
 * @date 2018/1/12 0012
 */
public class MemberUser extends BaseEntity implements Serializable {

    /** 姓名 **/
    private String name;
    /** 登录名 **/
    private String loginName;
    /** 密码 **/
    private String password;
    /** 交易密码 **/
    private String payPassword;
    /** 性别 **/
    private Integer gender;
    /** 年龄 **/
    private Integer age;
    /** 手机号 **/
    private String mobile;
    /** 身份证号码 **/
    private String idCard;
    /** 生日**/
    private String birthday;
    /** 用户等级 **/
    private String level;
    /** 头像 **/
    private String head;
    /** 邀请码 **/
    private String inviteCode;
    /** 状态 1：正常  2：冻结 **/
    private int status;
    /** 描述 **/
    private String description;
    /** 邀请人id **/
    private String friendId;
    /** 注册渠道 **/
    private ChannelBean channel;
    private int source;
    private String province;
    /** 渠道id **/
    private String channelId;

    /**
     * 新渠道成员id
     * @return
     */
    private String staffId;
    private int passwordWrongTimes;
    private int passwordStatus;
    private int trdpassdWrongTimes;
    private int trdpassdStatus;
    private int activityInvitationId;
    /**
     * 抽奖活动id
     */
    private Integer lotteryId;

    public int getActivityInvitationId() {
        return activityInvitationId;
    }

    public void setActivityInvitationId(int activityInvitationId) {
        this.activityInvitationId = activityInvitationId;
    }

    public int getPasswordWrongTimes() {
        return passwordWrongTimes;
    }

    public void setPasswordWrongTimes(int passwordWrongTimes) {
        this.passwordWrongTimes = passwordWrongTimes;
    }

    public int getPasswordStatus() {
        return passwordStatus;
    }

    public void setPasswordStatus(int passwordStatus) {
        this.passwordStatus = passwordStatus;
    }

    public int getTrdpassdWrongTimes() {
        return trdpassdWrongTimes;
    }

    public void setTrdpassdWrongTimes(int trdpassdWrongTimes) {
        this.trdpassdWrongTimes = trdpassdWrongTimes;
    }

    public int getTrdpassdStatus() {
        return trdpassdStatus;
    }

    public void setTrdpassdStatus(int trdpassdStatus) {
        this.trdpassdStatus = trdpassdStatus;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public ChannelBean getChannel() {
        return channel;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public void setChannel(ChannelBean channel) {
        this.channel = channel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public Integer getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(Integer lotteryId) {
        this.lotteryId = lotteryId;
    }
}
