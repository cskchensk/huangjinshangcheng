package cn.ug.member.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.member.bean.response.MemberThirdFindBean;
import cn.ug.member.service.MemberThirdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 会员第三方账号接入
 */
@RestController
@RequestMapping("memberThird")
public class MemberThirdController {

    private static Logger logger = LoggerFactory.getLogger(MemberThirdController.class);

    @Autowired
    private MemberThirdService memberThirdService;

    /**
     * 账号绑定
     *
     * @param partnerId 合作伙伴id绑定
     * @param memberId  会员id
     * @param type      类型  1微信账号 2是个推
     * @return
     */
    @RequestMapping(value = "save", method = POST)
    public SerializeObject save(String partnerId, String memberId, Integer type,Integer clientType) {
        memberThirdService.save(partnerId, memberId, type,clientType);
        return new SerializeObject(ResultType.NORMAL);
    }

    /**
     * 账号查询
     * @param partnerId
     * @param memberId
     * @param type
     * @return
     */
    @GetMapping(value = "find")
    public SerializeObject<MemberThirdFindBean> find(String partnerId, String memberId, Integer type) {
        MemberThirdFindBean memberThirdFindBean = memberThirdService.find(partnerId, memberId, type);
        return new SerializeObject<>(ResultType.NORMAL, memberThirdFindBean);
    }

    /**
     * 修改
     * @param partnerId
     * @param memberId
     * @param type
     * @return
     */
    @RequestMapping(value = "update", method = POST)
    public SerializeObject update(String partnerId, String memberId, Integer type,Integer clientType) {
        memberThirdService.update(partnerId, memberId, type,clientType);
        return new SerializeObject(ResultType.NORMAL);
    }

    /**
     * 删除
     * @param partnerId
     * @param type
     * @return
     */
    @RequestMapping(value = "delete", method = POST)
    public SerializeObject delete(String partnerId, Integer type) {
        memberThirdService.delete(partnerId, type);
        return new SerializeObject(ResultType.NORMAL);
    }
}
