package cn.ug.member.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.member.bean.AddressBean;
import cn.ug.member.web.submit.AddressSubmit;

/**
 * @author zhaohg
 * @date 2018/07/14.
 */
public interface AddressService {

    /**
     * 添加收货地址及默认地址
     * @param submit
     * @return
     */
    SerializeObject insert(AddressSubmit submit);

    /**
     * 更新收货地址及默认地址
     * @param submit
     * @return
     */
    SerializeObject update(AddressSubmit submit);

    /**
     * 获取收货地址
     * @param id
     * @return
     */
    SerializeObject findById(Long id);

    AddressBean getAddress(long addressId);

    /**
     * 用户收货地址list
     * @return
     */
    SerializeObject findList();

    /**
     * 删除收货地址
     * @param id
     * @return
     */
    SerializeObject deleteById(Long id);

    int count(String memberId);

    /**
     * 获取用户默认地址
     * @return
     */
    SerializeObject getDefaultAddress();
}
