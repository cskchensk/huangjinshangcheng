package cn.ug.member.service;

import cn.ug.member.bean.MemberLogisticsBean;
import cn.ug.member.bean.response.MemberAddressBaseBean;
import cn.ug.member.bean.response.MemberAddressBean;
import cn.ug.member.mapper.entity.MemberAddress;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author ywl
 * @date 2018/1/14 0014
 */
@Service
public interface MemberAddressService {

    /**
     * 添加
     * @param entityBean	实体
     * @return 				0:操作成功 1：不能为空 2：数据已存在
     */
    int save(MemberAddressBean entityBean);

    /**
     * 添加
     * @param entityBean	实体
     *
     * @return 				0:操作成功 1：不能为空 2：数据已存在
     */
    int update(MemberAddressBean entityBean);

    /**
     *
     * @param memberId
     */
    MemberAddressBaseBean findByMemberId(String memberId);

    MemberLogisticsBean getAddress(long addressId);

    MemberAddressBean findById(String id);

}
