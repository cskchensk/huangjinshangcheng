package cn.ug.member.service;

import cn.ug.activity.bean.ChannelBean;
import cn.ug.bean.base.DataTable;
import cn.ug.member.bean.InvitationBean;
import cn.ug.member.bean.MemberDetailBean;
import cn.ug.member.bean.MemberInfoBean;
import cn.ug.member.bean.request.*;
import cn.ug.member.bean.response.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ywl
 * @date 2018/1/12 0012
 */
public interface MemberUserService {

    /**
     * 搜索
     * @param memberUserParamBean	请求参数
     * @return				            分页数据
     */
    DataTable<MemberUserBaseBean> query(MemberUserParamBean memberUserParamBean);

    /**
     * 添加
     * @param entityBean	实体
     * @return 				0:操作成功 1：不能为空 2：数据已存在
     */
    void save(MemberUserFindBean entityBean);

    /**
     * 添加
     * @param entityBean	实体
     *
     * @return 				0:操作成功 1：不能为空 2：数据已存在
     */
    void update(MemberUserFindBean entityBean);

    /**
     * 更新状态
     * @param id			用户ID
     * @param status		用户状态
     */
    void updateStatus(String[] id, int status);

    /**
     * 查看
     * @param id			用户ID
     * @return				查询
     */
    MemberUserFindBean queryMemberUserById(String id);

    /**
     * 查询用户
     * @param id
     * @return
     */
    MemberUserBean findById(String id);
    MemberUserBean findByMobile(String mobile);
    MemberUserBean findByInviteCode(String inviteCode);

    /**
     * 查询用户
     * @param loginName
     * @return
     */
    MemberUserBean findByLoginName(String loginName);

    /**
     * 忘记会员密码
     * @param id
     * @param password
     */
    void forgetPassword(String id,String password);

    /**
     * 修改密码
     * @param password
     * @param newPassword
     */
    void updatePassword(String password, String newPassword);

    /**
     * 会员注册
     * @param mobile    用户名
     * @param password  密码
     */
    boolean register(String mobile, String password, String friendId, ChannelBean channelBean, int source, String staffId,Integer lotteryId, int amount);

    boolean register(String mobile, String friendId, ChannelBean channelBean, int source, String staffId,Integer lotteryId, int amount);
    /**
     * 设置交易密码--支付密码
     * @param payPassword
     * @param payPassword
     */
    void setPayPassword(String payPassword);

    /**
     * 修改交易密码--支付密码
     * @param payPassword  现有密码
     * @param payPassword  新密码
     */
    void updatePayPassword(String payPassword,String newPayPassword);

    /**
     * 更新会员实名信息
     * @param memberUserBaseParmBean
     */
    void updateBaseInfo(MemberUserBaseParmBean memberUserBaseParmBean);

    /**
     * 验证交易密码是否正确
     * @param payPassword
     * @param payPassword
     */
    boolean validatePayPassword(String memberId,String payPassword);

    /**
     * 身份认证
     * @param muIdentityParamBean
     * @return
     */
    boolean validateMemberIdentity(MemberUserIdentityParamBean muIdentityParamBean);

    /**
     * 更新登录信息
     * @param memberId
     * @param loginSource
     */
    void updateLoginSource(String memberId, Integer loginSource);

    List<InvitationBean> listRecords(String memberId, int type, int offset, int size);

    int countRecords(String memberId, int type);
    int sumInvitation(String memberId);
    int countTotalUsers();
    List<MemberInfoBean> listUsersId(int offset, int size);
    List<MemberInfoBean> listUsersId(List<String> mobiles);

    MemberDetailBean getMemberDetail(String memberId);

    boolean modifyPasswordWrongTimes(String memberId, int passwordWrongTimes, int status);
    boolean modifyTradePasswordWrongTimes(String memberId, int tradePasswordWrongTimes, int status);

    /**
     * 搜索用户购金列表详情
     * @param memberUserBuyGoldParamBean	请求参数
     * @return				            分页数据
     */
    DataTable<MemberUserBuyGoldBean> queryBuyGoldList(MemberUserBuyGoldParamBean memberUserBuyGoldParamBean);

    /**
     * 用户行为
     * @param memberUserBuyGoldParamBean
     * @return
     */
    DataTable<MemberUserBehaviorBean> behaviorlist(MemberUserBehaviorParamBean memberUserBuyGoldParamBean);

    /**
     * 复投用户统计总表
     * @param memberUserAgainBuyParamBean	请求参数
     * @return				            分页数据
     */
    List<MemberUserAgainBuyBean> queryAgainBuyUserList(MemberUserAgainBuyParamBean memberUserAgainBuyParamBean);

    /**
     * 复投用户姓名、手机号、渠道来源总计
     * @param memberUserBuyGoldParamBean
     * @return
     */
    Map countAgainBuyUser(MemberUserAgainBuyParamBean memberUserBuyGoldParamBean);

    /**
     * 查询复投用户复投记录
     * @param memberId
     * @param leasebackDays
     * @param productIds
     * @return
     */
    List<MemberUserAgainBuyLeasebackBean> queryAgainBuyUserLeaseback(String memberId, Integer[] leasebackDays, Integer[] productType);

    /**
     * 获取最早添加用户时间
     * @return
     */
    Date findMinAddTime();
}
