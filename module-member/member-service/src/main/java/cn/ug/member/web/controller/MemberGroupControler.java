package cn.ug.member.web.controller;

import static cn.ug.util.ConstantUtil.COMMA;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.member.bean.MemberGroupBean;
import cn.ug.member.mapper.entity.MemberGroup;
import cn.ug.member.mapper.entity.MemberGroupRecord;
import cn.ug.member.mapper.entity.MemberGroupStatistics;
import cn.ug.member.service.MemberGroupService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("group")
public class MemberGroupControler {
    @Autowired
    private MemberGroupService memberGroupService;

    @GetMapping(value = "/belong")
    public SerializeObject getGroupId(String memberId, String grougIds) {
        if (StringUtils.isBlank(memberId) || StringUtils.isBlank(grougIds)) {
            return new SerializeObjectError("00000002");
        }
        List<Integer> ids = new ArrayList<Integer>();
        String[] idsStr = StringUtils.split(grougIds, COMMA);
        for (String id : idsStr) {
            ids.add(Integer.parseInt(id));
        }
        return new SerializeObject<>(ResultType.NORMAL, memberGroupService.getGroupId(memberId, ids));
    }


    @PostMapping
    public SerializeObject save(@RequestHeader String accessToken, MemberGroup memberGroup) {
        if(null == memberGroup || StringUtils.isBlank(memberGroup.getName())) {
            return new SerializeObjectError("14000240");
        }
        memberGroup.setType(2);
        memberGroup.setCreateId(LoginHelper.getLoginId());
        memberGroup.setCreateName(LoginHelper.getLoginName());
        if (memberGroup.getId() > 0 && memberGroup.getId() < 4) {
            return new SerializeObjectError("14000243");
        }
        if (memberGroupService.save(memberGroup)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PutMapping
    public SerializeObject delete(@RequestHeader String accessToken, int id) {
        if(id < 1) {
            return new SerializeObjectError("00000002");
        }
        if (id < 4) {
            return new SerializeObjectError("14000241");
        }
        if (memberGroupService.delete(id)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PutMapping(value = "/status")
    public SerializeObject enable(@RequestHeader String accessToken, Integer status, int id) {
        status = status == null ? 0 : status;
        if(id < 1) {
            return new SerializeObjectError("00000002");
        }
        if (id < 4) {
            return new SerializeObjectError("14000242");
        }
        if (memberGroupService.updateStatus(id, status)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @GetMapping(value = "/list")
    public SerializeObject<DataTable<MemberGroup>> list(@RequestHeader String accessToken, Page page, Order order, String name, String startTime, String endTime) {
        name = UF.toString(name);
        startTime = UF.toString(startTime);
        endTime = UF.toString(endTime);
        int total = memberGroupService.count(name, startTime, endTime);
        page.setTotal(total);
        if (total > 0) {
            List<MemberGroup> list = memberGroupService.query(name, startTime, endTime, order.getOrder(), order.getSort(), PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<MemberGroup>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<MemberGroup>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<MemberGroup>()));
    }

    @GetMapping(value = "/detail")
    public SerializeObject<MemberGroup> getGroupDetail(int id) {
        if (id < 1) {
            return new SerializeObjectError("00000002");
        }
        return new SerializeObject<>(ResultType.NORMAL, memberGroupService.findById(id));
    }

    @GetMapping(value = "/enabled/list")
    public SerializeObject<List<MemberGroupBean>> list(Integer type) {
        type = type == null ? 0 : type;
        List<MemberGroup> memberGroups = memberGroupService.queryForEnabledList(type);
        List<MemberGroupBean> beans = new ArrayList<MemberGroupBean>();
        if (memberGroups != null && memberGroups.size() > 0) {
            for (MemberGroup memberGroup : memberGroups) {
                MemberGroupBean bean = new MemberGroupBean();
                bean.setId(memberGroup.getId());
                bean.setName(memberGroup.getName());
                bean.setType(memberGroup.getType());
                beans.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, beans);
    }

    @GetMapping(value = "/statistics/list")
    public SerializeObject<DataTable<MemberGroupStatistics>> list(@RequestHeader String accessToken, Page page, Order order, int groupId, Integer startNum, Integer endNum, String startTime, String endTime) {
        startNum = startNum == null ? -1 : startNum;
        endNum = endNum == null ? -1 : endNum;
        startTime = UF.toString(startTime);
        endTime = UF.toString(endTime);
        int total = memberGroupService.count(groupId, startNum, endNum, startTime, endTime);
        page.setTotal(total);
        if (total > 0) {
            List<MemberGroupStatistics> list = memberGroupService.query(groupId, startNum, endNum, startTime, endTime, order.getOrder(), order.getSort(), PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<MemberGroupStatistics>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<MemberGroupStatistics>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<MemberGroupStatistics>()));
    }

    @GetMapping(value = "/statistics/export")
    public void export(HttpServletResponse response, int groupId, String calculationDate) {
        calculationDate = UF.toString(calculationDate);
        List<MemberGroupRecord> list = memberGroupService.query(groupId, calculationDate);
        if (list == null) {
            list = new ArrayList<MemberGroupRecord>();
        }
        String fileName = "分群用户信息";
        String[] columnNames = { "序号", "手机号"};
        String[] columns = {"index",  "mobile"};
        int index = 1;
        for (MemberGroupRecord bean : list) {
            bean.setIndex(index);
            index++;
        }
        ExportExcelController<MemberGroupRecord> export = new ExportExcelController<MemberGroupRecord>();
        export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
    }
}
