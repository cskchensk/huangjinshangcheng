package cn.ug.member.mapper;

import cn.ug.member.bean.InvitationBean;
import cn.ug.member.bean.MemberDetailBean;
import cn.ug.member.bean.MemberInfoBean;
import cn.ug.member.bean.request.MemberUserAgainBuyParamBean;
import cn.ug.member.bean.request.MemberUserBehaviorParamBean;
import cn.ug.member.bean.request.MemberUserBuyGoldParamBean;
import cn.ug.member.bean.request.MemberUserParamBean;
import cn.ug.member.bean.response.*;
import cn.ug.member.mapper.entity.MemberUser;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author ywl
 * @date 2018/1/12 0012
 */
@Component
public interface MemberUserMapper extends BaseMapper<MemberUser> {


    List<MemberUserBaseBean> queryMemberUserList(MemberUserParamBean memberUserParamBean);

    MemberUserFindBean queryMemberUserById(String id); //本方法不包含密码特殊字段，安全性考虑，内部调用请用findById

    int updateStatus(Map<String,Object> map);

    MemberUser findByLoginName(String loginName);

    MemberUser findByMobile(String mobile);

    MemberUser findByInviteCode(String inviteCode);

    void updateLoginSource(@Param("id") String id, @Param("source") Integer source);

    List<InvitationBean> queryInvitationForList(Map<String, Object> params);
    int queryInvitationForCount(Map<String, Object> params);
    int queryInvitationForSum(Map<String, Object> params);
    int countTotalUsers();
    List<MemberInfoBean> queryUserIdForList(Map<String, Object> params);
    List<MemberInfoBean> queryUserIdByMobile(@Param("mobiles")List<String> mobiles);

    MemberDetailBean selectMemberDetail(@Param("memberId")String memberId);
    MemberDetailBean selectInvitationInfo(@Param("memberId")String memberId);
    int resetPasswordStatus();
    int queryInvitationForToday(@Param("memberId")String memberId);

    List<MemberUserBuyGoldBean> queryBuyGoldList(MemberUserBuyGoldParamBean memberUserBuyGoldParamBean);

    List<MemberUserBehaviorBean> behaviorlist(MemberUserBehaviorParamBean memberUserBehaviorParamBean);

    Map queryProductById(@Param("orderNo") String orderNo);

    List<MemberUserAgainBuyBean> againBuyUser(MemberUserAgainBuyParamBean memberUserBuyGoldParamBean);

    List<MemberUserAgainBuyBean> againBuyUserData(@Param("memberId")String memberId);

    Map countAgainBuyUser(MemberUserAgainBuyParamBean memberUserBuyGoldParamBean);

    List<MemberUserAgainBuyLeasebackBean> queryAgainBuyUserLeaseback(@Param("memberId")String memberId,
                                                                     @Param("leasebackDays")List<Integer> leasebackDays,
                                                                     @Param("productType")List<Integer> productType);
    //获取最早添加用户时间
    Date findMinAddTime();
}