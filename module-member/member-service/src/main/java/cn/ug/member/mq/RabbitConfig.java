package cn.ug.member.mq;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static cn.ug.config.QueueName.*;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue memberLabelQueue() {
        return new Queue(QUEUE_MEMBER_LABEL, true);
    }

    @Bean
    public Queue memberPasswordStatusQueue() {
        return new Queue(QUEUE_MEMBER_PASSWORD_STATUS, true);
    }

    @Bean
    public Queue memberPasswordStatusQueueDelayPerMessageTTL() {
        return QueueBuilder.durable(QUEUE_MEMBER_PASSWORD_STATUS_DELAY)
                .withArgument("x-dead-letter-exchange", QUEUE_MEMBER_PASSWORD_STATUS_DLX)
                .withArgument("x-dead-letter-routing-key", QUEUE_MEMBER_PASSWORD_STATUS)
                .build();
    }

    @Bean
    public DirectExchange memberPasswordStatusDelayExchange(){
        return new DirectExchange(QUEUE_MEMBER_PASSWORD_STATUS_DLX);
    }

    @Bean
    public Binding memberPasswordStatusDelayBinding(Queue memberPasswordStatusQueue, DirectExchange memberPasswordStatusDelayExchange) {
        return BindingBuilder.bind(memberPasswordStatusQueue)
                .to(memberPasswordStatusDelayExchange)
                .with(QUEUE_MEMBER_PASSWORD_STATUS);
    }
}
