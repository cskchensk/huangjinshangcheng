package cn.ug.member.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class MemberThird extends BaseEntity implements Serializable {


    private String openId;

    private String memberId;

    private Integer type;

    private Integer clientType;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getClientType() {
        return clientType;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }
}
