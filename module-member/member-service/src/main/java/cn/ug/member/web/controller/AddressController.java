package cn.ug.member.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.member.service.AddressService;
import cn.ug.member.web.submit.AddressSubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author zhaohg
 * @date 2018/07/16.
 */
@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    /**
     * 添加收货地址
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject add(AddressSubmit submit) {
        return addressService.insert(submit);
    }

    /**
     * 更新收货地址
     *
     * @param submit
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject update(AddressSubmit submit) {
        if (submit.getId() > 0) {
            return addressService.update(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 查询收货地址
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject findById(@PathVariable("id") long id) {
        if (id > 0) {
            return addressService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 删除收货地址
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete/{id}", method = DELETE)
    public SerializeObject delete(@PathVariable("id") Long id) {
        if (id > 0) {
            return addressService.deleteById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 收货地址list
     *
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject list() {
        return addressService.findList();
    }

    /**
     * 根据用户id获取默认收货地址
     */
    @RequestMapping(value = "/default", method = GET)
    public SerializeObject getDefaultAddress() {
        return addressService.getDefaultAddress();
    }
}
