package cn.ug.member.service.impl;

import cn.ug.member.bean.LabelSearchBean;
import cn.ug.member.bean.MemberLabelBean;
import cn.ug.member.mapper.MemberLabelBaselineMapper;
import cn.ug.member.mapper.MemberLabelMapper;
import cn.ug.member.mapper.entity.MemberLabel;
import cn.ug.member.mapper.entity.MemberLabelBaseline;
import cn.ug.member.service.MemberLabelService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class MemberLabelServiceImpl implements MemberLabelService {
    @Autowired
    private MemberLabelBaselineMapper memberLabelBaselineMapper;
    @Autowired
    private MemberLabelMapper memberLabelMapper;
    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public MemberLabelBaseline getRFMAvg() {
        return memberLabelBaselineMapper.selectRFMAvg();
    }

    @Override
    public boolean updateInBatch(List<MemberLabel> labels) {
        if (labels != null && labels.size() > 0) {
            return memberLabelMapper.updateInBatch(labels) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public MemberLabel getLabel(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return memberLabelMapper.selectByMemberId(memberId);
        }
        return null;
    }

    @Override
    public boolean save(MemberLabel label) {
        if (label != null) {
            if (StringUtils.isBlank(label.getId())) {
                MemberLabel memberLabel = memberLabelMapper.selectByMemberId(label.getMemberId());
                if (memberLabel == null) {
                    label.setId(UF.getRandomUUID());
                    return memberLabelMapper.insert(label) > 0 ? true : false;
                }
                return false;
            } else {
                label.setModifyTime(LocalDateTime.now());
                return memberLabelMapper.update(label) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public List<MemberLabelBean> list(int offset, int size, List<LabelSearchBean> beans) {
        Map<String, Object> params = new HashMap<String, Object>();
        List<MemberLabel> memberLabels =memberLabelMapper.query(offset, size, beans);
        if (memberLabels != null && memberLabels.size() > 0) {
            List<MemberLabelBean> result = new ArrayList<MemberLabelBean>();
            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            for (MemberLabel label : memberLabels) {
                MemberLabelBean bean = dozerBeanMapper.map(label, MemberLabelBean.class);
                if (bean.getBornYear() > 0) {
                    bean.setAge(year - bean.getBornYear());
                }
                if (bean.getGender() == 1) {
                    bean.setSex("男");
                } else if (bean.getGender() == 2) {
                    bean.setSex("女");
                }
                result.add(bean);
            }
            return result;
        }
        return null;
    }

    @Override
    public MemberLabelBaseline getLabelBaseline() {
        return memberLabelBaselineMapper.findOne();
    }

    @Override
    public boolean save(MemberLabelBaseline labelBaseline) {
        if (labelBaseline != null) {
            return memberLabelBaselineMapper.updateByPrimaryKeySelective(labelBaseline) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public int count(List<LabelSearchBean> beans) {
        return memberLabelMapper.count(beans);
    }
}
