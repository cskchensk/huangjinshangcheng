package cn.ug.member.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberLabelBaseline implements Serializable {
    private String id;
    private int rfmDays;
    private int rvalue;
    private int fvalue;
    private int mvalue;
    private BigDecimal ravg;
    private BigDecimal favg;
    private BigDecimal mavg;
    private int status;
    private String countingDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getRfmDays() {
        return rfmDays;
    }

    public void setRfmDays(int rfmDays) {
        this.rfmDays = rfmDays;
    }

    public int getRvalue() {
        return rvalue;
    }

    public void setRvalue(int rvalue) {
        this.rvalue = rvalue;
    }

    public int getFvalue() {
        return fvalue;
    }

    public void setFvalue(int fvalue) {
        this.fvalue = fvalue;
    }

    public int getMvalue() {
        return mvalue;
    }

    public void setMvalue(int mvalue) {
        this.mvalue = mvalue;
    }

    public BigDecimal getRavg() {
        return ravg;
    }

    public void setRavg(BigDecimal ravg) {
        this.ravg = ravg;
    }

    public BigDecimal getFavg() {
        return favg;
    }

    public void setFavg(BigDecimal favg) {
        this.favg = favg;
    }

    public BigDecimal getMavg() {
        return mavg;
    }

    public void setMavg(BigDecimal mavg) {
        this.mavg = mavg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCountingDate() {
        return countingDate;
    }

    public void setCountingDate(String countingDate) {
        this.countingDate = countingDate;
    }
}
