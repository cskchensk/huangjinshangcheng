package cn.ug.member.service;

import cn.ug.member.bean.LabelSearchBean;
import cn.ug.member.bean.MemberLabelBean;
import cn.ug.member.mapper.entity.MemberLabel;
import cn.ug.member.mapper.entity.MemberLabelBaseline;

import java.util.List;

public interface MemberLabelService {
    MemberLabelBaseline getLabelBaseline();
    MemberLabelBaseline getRFMAvg();
    boolean save(MemberLabelBaseline labelBaseline);

    List<MemberLabelBean> list(int offset, int size, List<LabelSearchBean> beans);
    MemberLabel getLabel(String memberId);
    int count(List<LabelSearchBean> beans);
    boolean save(MemberLabel label);
    boolean updateInBatch(List<MemberLabel> labels);
}
