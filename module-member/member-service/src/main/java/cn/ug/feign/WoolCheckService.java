package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url="https://api.253.com/open/wool/wcheck", value = "woolCheck")
public interface WoolCheckService {
    @PostMapping
    public String woolCheck(@RequestParam("mobile")String mobile,
                            @RequestParam("appId")String appId,
                            @RequestParam("appKey")String appKey);
}
