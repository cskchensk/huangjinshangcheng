package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url="http://ip.taobao.com/service/getIpInfo.php", value = "taobaoIp")
public interface IpService {

    @GetMapping
    String getIp(@RequestParam("ip")String ip);
}
