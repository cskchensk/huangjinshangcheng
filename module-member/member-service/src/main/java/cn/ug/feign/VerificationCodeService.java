package cn.ug.feign;

import cn.ug.login.api.VerificationCodeServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("LOGIN-SERVICE")
public interface VerificationCodeService  extends VerificationCodeServiceApi {


}
