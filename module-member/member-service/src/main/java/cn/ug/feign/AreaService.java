package cn.ug.feign;

import cn.ug.account.api.AreaServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACCOUNT-SERVICE")
public interface AreaService  extends AreaServiceApi {
}
