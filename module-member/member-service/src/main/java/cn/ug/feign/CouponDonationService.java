package cn.ug.feign;

import cn.ug.activity.api.CouponDonationServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACTIVITY-SERVICE")
public interface CouponDonationService extends CouponDonationServiceApi {
}
