package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url="http://apis.juhe.cn/mobile/get", value = "mobileAscription")
public interface MobileAscriptionService {

    @GetMapping
    public String getMobileAscription(@RequestParam("phone")String phone,
                                      @RequestParam("key")String key);
}
