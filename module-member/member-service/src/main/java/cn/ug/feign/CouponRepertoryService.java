package cn.ug.feign;

import cn.ug.activity.api.CouponRepertoryServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACTIVITY-SERVICE")
public interface CouponRepertoryService extends CouponRepertoryServiceApi {
}
