package cn.ug.feign;

import cn.ug.operation.api.CupJoinUserServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("OPERATION-SERVICE")
public interface CupJoinUserService extends CupJoinUserServiceApi {
}
