package cn.ug.feign;

import cn.ug.pay.api.MemberGoldServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("PAY-SERVICE")
public interface MemberGoldService extends MemberGoldServiceApi {
}
