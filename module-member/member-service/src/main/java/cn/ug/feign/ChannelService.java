package cn.ug.feign;

import cn.ug.activity.api.ChannelServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACTIVITY-SERVICE")
public interface ChannelService extends ChannelServiceApi {
}
