package cn.ug.feign;

import cn.ug.activity.api.ActivityInvitationServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACTIVITY-SERVICE")
public interface ActivityInvitationService extends ActivityInvitationServiceApi {
}
