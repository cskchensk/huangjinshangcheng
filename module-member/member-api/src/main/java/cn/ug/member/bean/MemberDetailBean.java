package cn.ug.member.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberDetailBean implements Serializable {
    private String memberId;
    private String channelId;
    private String channelName;
    private String memberName;
    private String mobile;
    /**绑卡状态：0--未绑卡；1--已绑卡*/
    private int bindCardStatus;
    private String registerTime;
    /**用户状态： 1：正常  2：冻结*/
    private int memberStatus;
    /**用户投资状态:
     * 1.潜在用户：注册未绑卡的用户
     * 2.意向用户：绑卡未购买过产品或商品的用户
     * 3.新客户：只有单次购买行为的用户
     * 4.老客户：购买次数大于1次的用户
     */
    private int investmentStatus;
    private String level;
    private String bindCardTime;
    private String firstInvestmentTime;
    private String secondInvestmentTime;
    private BigDecimal firstInvestmentAmount;
    private String firstRechargeTime;
    private int reInvestNum;
    private int friends;
    private int friendsBindCardNum;
    private int usableCoupons;
    private BigDecimal usableCouponAmount;
    private int usableTickets;
    private BigDecimal totalGram;
    private BigDecimal buyGram;
    private BigDecimal sellGram;
    private BigDecimal balance;
    private BigDecimal withdrawAmount;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getBindCardStatus() {
        return bindCardStatus;
    }

    public void setBindCardStatus(int bindCardStatus) {
        this.bindCardStatus = bindCardStatus;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public int getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(int memberStatus) {
        this.memberStatus = memberStatus;
    }

    public int getInvestmentStatus() {
        return investmentStatus;
    }

    public void setInvestmentStatus(int investmentStatus) {
        this.investmentStatus = investmentStatus;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getBindCardTime() {
        return bindCardTime;
    }

    public void setBindCardTime(String bindCardTime) {
        this.bindCardTime = bindCardTime;
    }

    public String getFirstInvestmentTime() {
        return firstInvestmentTime;
    }

    public void setFirstInvestmentTime(String firstInvestmentTime) {
        this.firstInvestmentTime = firstInvestmentTime;
    }

    public String getSecondInvestmentTime() {
        return secondInvestmentTime;
    }

    public void setSecondInvestmentTime(String secondInvestmentTime) {
        this.secondInvestmentTime = secondInvestmentTime;
    }

    public BigDecimal getFirstInvestmentAmount() {
        return firstInvestmentAmount;
    }

    public void setFirstInvestmentAmount(BigDecimal firstInvestmentAmount) {
        this.firstInvestmentAmount = firstInvestmentAmount;
    }

    public String getFirstRechargeTime() {
        return firstRechargeTime;
    }

    public void setFirstRechargeTime(String firstRechargeTime) {
        this.firstRechargeTime = firstRechargeTime;
    }

    public int getReInvestNum() {
        return reInvestNum;
    }

    public void setReInvestNum(int reInvestNum) {
        this.reInvestNum = reInvestNum;
    }

    public int getFriends() {
        return friends;
    }

    public void setFriends(int friends) {
        this.friends = friends;
    }

    public int getFriendsBindCardNum() {
        return friendsBindCardNum;
    }

    public void setFriendsBindCardNum(int friendsBindCardNum) {
        this.friendsBindCardNum = friendsBindCardNum;
    }

    public int getUsableCoupons() {
        return usableCoupons;
    }

    public void setUsableCoupons(int usableCoupons) {
        this.usableCoupons = usableCoupons;
    }

    public BigDecimal getUsableCouponAmount() {
        return usableCouponAmount;
    }

    public void setUsableCouponAmount(BigDecimal usableCouponAmount) {
        this.usableCouponAmount = usableCouponAmount;
    }

    public int getUsableTickets() {
        return usableTickets;
    }

    public void setUsableTickets(int usableTickets) {
        this.usableTickets = usableTickets;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getBuyGram() {
        return buyGram;
    }

    public void setBuyGram(BigDecimal buyGram) {
        this.buyGram = buyGram;
    }

    public BigDecimal getSellGram() {
        return sellGram;
    }

    public void setSellGram(BigDecimal sellGram) {
        this.sellGram = sellGram;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }
}
