package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 后台查询列表
 * @author ywl
 * @date 2018/1/14 0014
 */
public class MemberUserBaseBean extends BaseBean implements Serializable {

    /** 姓名 **/
    private String name;
    /**性别 1:男 2:女 3:未知**/
    private Integer gender;
    private Integer age;
    /** 手机号 **/
    private String mobile;
    /** 身份证号码 **/
    private String idCard;
    /** 状态 1：正常  2：冻结 **/
    private int status;
    /**渠道名称**/
    private String channelName;
    /**绑卡时间**/
    private String tiedCardTime;
    /** 邀请人姓名 */
    private String invitationName;

    /** 邀请人手机号 */
    private String invitationMobile;
    /**
     * 持有提单总克数=待处理提单+回租中提单+冻结中提单
     */
    private String tbTotalGram;
    /**
     * 回租提单克数
     */
    private String tblTotalGram;

    /**
     * 账户余额
     */
    private BigDecimal usableAmount;

    /**
     * 金豆余额
     */
    private Integer usableBean;

    /**
     * 最近回租时间
     */
    private String newLeasebackDate;

    /**
     * 最近回租的期限
     */
    private Integer leasebackDays;

    public String getInvitationName() {
        return invitationName;
    }

    public void setInvitationName(String invitationName) {
        this.invitationName = invitationName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getTiedCardTime() {
        return tiedCardTime;
    }

    public void setTiedCardTime(String tiedCardTime) {
        this.tiedCardTime = tiedCardTime;
    }

    public String getInvitationMobile() {
        return invitationMobile;
    }

    public void setInvitationMobile(String invitationMobile) {
        this.invitationMobile = invitationMobile;
    }

    public String getTbTotalGram() {
        return tbTotalGram;
    }

    public void setTbTotalGram(String tbTotalGram) {
        this.tbTotalGram = tbTotalGram;
    }

    public String getTblTotalGram() {
        return tblTotalGram;
    }

    public void setTblTotalGram(String tblTotalGram) {
        this.tblTotalGram = tblTotalGram;
    }

    public BigDecimal getUsableAmount() {
        return usableAmount;
    }

    public void setUsableAmount(BigDecimal usableAmount) {
        this.usableAmount = usableAmount;
    }

    public Integer getUsableBean() {
        return usableBean;
    }

    public void setUsableBean(Integer usableBean) {
        this.usableBean = usableBean;
    }

    public String getNewLeasebackDate() {
        return newLeasebackDate;
    }

    public void setNewLeasebackDate(String newLeasebackDate) {
        this.newLeasebackDate = newLeasebackDate;
    }

    public Integer getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(Integer leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
