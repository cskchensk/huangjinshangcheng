package cn.ug.member.bean.request;

import java.io.Serializable;

/**
 *  会员身份验证
 * @auther ywl
 */
public class MemberUserIdentityParamBean implements Serializable{

    /** 手机号码 **/
    private String mobile;
    /** 姓名 **/
    private String name;
    /** 身份证 **/
    private String idCard;
    /** 验证码 **/
    private String code;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
