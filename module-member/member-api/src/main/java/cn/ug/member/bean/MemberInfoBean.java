package cn.ug.member.bean;

import java.io.Serializable;

public class MemberInfoBean implements Serializable {
    private String id;
    private String mobile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
