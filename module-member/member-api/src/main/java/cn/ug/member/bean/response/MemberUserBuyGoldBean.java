package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/6/10 0010
 * @time 上午 11:45
 **/
public class MemberUserBuyGoldBean  extends BaseBean implements Serializable {
    private int index;

    /** 姓名 **/
    private String name;

    /** 手机号 **/
    private String mobile;

    /**
     * 最高购买克重总数
     */
    private Integer maxBuyGram;

    /**
     *   购金平均规格（克）
     */
    private BigDecimal averageBuyGram;

    /**
     * 最近回租结束时间
     */
    private String newLeasebackEndDate;

    /**
     * 最近回租到期时长
     */
    private Integer newLeasebackDays;

    /**
     * 上次回租结束时间
     */
    private String frontLeasebackEndDate;

    /**
     * 上次回租到期时长
     */

    private Integer frontLeasebackDays;

    /**
     * 回租中提单克数
     */
    private String leasebackGram;

    /**
     * 累计复投次数
     */
    private Integer totalRepeatNum;

    /**
     * 累计复投克重
     */
    private BigDecimal totalRepeatGram;

    /**
     * 回租总收益
     */
    private BigDecimal totalIncomeAmount;

    /**
     * 黄金买卖收益
     */
    private BigDecimal buySellIncomeAmount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getMaxBuyGram() {
        return maxBuyGram;
    }

    public void setMaxBuyGram(Integer maxBuyGram) {
        this.maxBuyGram = maxBuyGram;
    }

    public BigDecimal getAverageBuyGram() {
        return averageBuyGram;
    }

    public void setAverageBuyGram(BigDecimal averageBuyGram) {
        this.averageBuyGram = averageBuyGram;
    }

    public String getNewLeasebackEndDate() {
        return newLeasebackEndDate;
    }

    public void setNewLeasebackEndDate(String newLeasebackEndDate) {
        this.newLeasebackEndDate = newLeasebackEndDate;
    }

    public Integer getNewLeasebackDays() {
        return newLeasebackDays;
    }

    public void setNewLeasebackDays(Integer newLeasebackDays) {
        this.newLeasebackDays = newLeasebackDays;
    }

    public String getFrontLeasebackEndDate() {
        return frontLeasebackEndDate;
    }

    public void setFrontLeasebackEndDate(String frontLeasebackEndDate) {
        this.frontLeasebackEndDate = frontLeasebackEndDate;
    }

    public Integer getFrontLeasebackDays() {
        return frontLeasebackDays;
    }

    public void setFrontLeasebackDays(Integer frontLeasebackDays) {
        this.frontLeasebackDays = frontLeasebackDays;
    }

    public String getLeasebackGram() {
        return leasebackGram;
    }

    public void setLeasebackGram(String leasebackGram) {
        this.leasebackGram = leasebackGram;
    }

    public Integer getTotalRepeatNum() {
        return totalRepeatNum;
    }

    public void setTotalRepeatNum(Integer totalRepeatNum) {
        this.totalRepeatNum = totalRepeatNum;
    }

    public BigDecimal getTotalRepeatGram() {
        return totalRepeatGram;
    }

    public void setTotalRepeatGram(BigDecimal totalRepeatGram) {
        this.totalRepeatGram = totalRepeatGram;
    }

    public BigDecimal getTotalIncomeAmount() {
        return totalIncomeAmount;
    }

    public void setTotalIncomeAmount(BigDecimal totalIncomeAmount) {
        this.totalIncomeAmount = totalIncomeAmount;
    }

    public BigDecimal getBuySellIncomeAmount() {
        return buySellIncomeAmount;
    }

    public void setBuySellIncomeAmount(BigDecimal buySellIncomeAmount) {
        this.buySellIncomeAmount = buySellIncomeAmount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
