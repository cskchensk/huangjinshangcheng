package cn.ug.member.mq;

import java.io.Serializable;

public class MemberPasswordStatusMQ implements Serializable {
    private String memberId;
    /**
     * 1--登陆密码；
     * 2--交易密码；
     * */
    private int type;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
