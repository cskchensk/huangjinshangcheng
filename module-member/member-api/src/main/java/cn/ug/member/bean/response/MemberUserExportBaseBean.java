package cn.ug.member.bean.response;

public class MemberUserExportBaseBean extends MemberUserBaseBean {

    private int index;

    /** 状态 1：正常  2：冻结 **/
    private String userStatus;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
}
