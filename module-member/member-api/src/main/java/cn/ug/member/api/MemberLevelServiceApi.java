package cn.ug.member.api;

import cn.ug.member.bean.MemberLevelBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 会员等级
 * @author kaiwotech
 */
@RequestMapping("level")
public interface MemberLevelServiceApi {

    /**
     * 根据ID查找信息
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<MemberLevelBean> find(@PathVariable("id") String id);

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken       登录成功后分配的Key
     * @param param             参数集合
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    SerializeObject update(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam Map<String, Object> param);

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(method = DELETE)
    SerializeObject delete(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("id") String[] id);

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "remove", method = PUT)
    SerializeObject remove(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("id") String[] id);

    /**
     * 根据积分查找信息
     * @param integral	积分
     * @return			记录集
     */
    @RequestMapping(value = "findByIntegral", method = GET)
    SerializeObject<MemberLevelBean> findByIntegral(@RequestParam("integral") Integer integral);

    /**
     * 查询列表
     * @param order		    排序字段
     * @param sort		    排序方式 desc或asc
     * @param status	    状态	 0：全部 1：正常 2：被锁定
     * @param keyword	    关键字
     * @return			    分页数据
     */
    @RequestMapping(value = "list" , method = GET)
    SerializeObject<List<MemberLevelBean>> list(
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("status") Integer status,
            @RequestParam("keyword") String keyword);

    /**
     * 查询数据
     * @param order		    排序字段
     * @param sort		    排序方式 desc或asc
     * @param pageNum	    查询页数
     * @param pageSize	    每页记录数
     * @param status	    状态	 0：全部 1：正常  2：特殊会员组
     * @param keyword	    关键字
     * @return			    分页数据
     */
    @RequestMapping(method = GET)
    SerializeObject<DataTable<MemberLevelBean>> query(
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("pageNum") Integer pageNum,
            @RequestParam("pageSize") Integer pageSize,
            @RequestParam("status") Integer status,
            @RequestParam("keyword") String keyword);

    /**
     * 验证是否存在
     * @param accessToken	登录成功后分配的Key
     * @param param         参数集合
     * @return			    是否验证通过
     */
    @RequestMapping(value = "verify",method = GET)
    SerializeObject verify(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam Map<String, Object> param);

}
