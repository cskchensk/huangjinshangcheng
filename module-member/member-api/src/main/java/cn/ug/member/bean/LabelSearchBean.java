package cn.ug.member.bean;

import java.io.Serializable;

public class LabelSearchBean implements Serializable {
    private String key;
    // 1:大于；2：大于等于；3：等于；4：小于等于；5：小于
    private int mark;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
