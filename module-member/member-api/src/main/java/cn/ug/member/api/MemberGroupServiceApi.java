package cn.ug.member.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.member.bean.MemberGroupBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("group")
public interface MemberGroupServiceApi {

    @GetMapping(value = "/enabled/list")
    SerializeObject<List<MemberGroupBean>> list(@RequestParam("type")Integer type);

    /** grougIds 设置了奖励的用户组的id串 多个用户英文“,”分割，如：1,2,3,4
     * */
    @GetMapping(value = "/belong")
    SerializeObject getGroupId(@RequestParam("memberId")String memberId, @RequestParam("grougIds")String grougIds);
}
