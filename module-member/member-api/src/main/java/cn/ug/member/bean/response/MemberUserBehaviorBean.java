package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * @Author zhangweijie
 * @Date 2019/6/10 0010
 * @time 下午 17:25
 **/
public class MemberUserBehaviorBean extends BaseBean implements Serializable {

    private int index;

    /** 姓名 **/
    private String name;

    /** 手机号 **/
    private String mobile;
    /**
     * 平均回租周期（天）
     */
    private Integer averageLeasebackDay;
    /**
         *最高回租周期（天）
     */
    private Integer maxLeasebackDay;
    /**
     *优惠券使用次数（次)
     */
    private Integer couponNum;
    /**
     *优惠券总金额
     */
    private Integer couponGram;
    /**
     *邀请人数（人）
     */
    private Integer invitationNum;
    /**
     *邀请购金总克重（克）
     */
    private Integer invitationBuyGram;

    /**渠道名称**/
    private String channelName;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getAverageLeasebackDay() {
        return averageLeasebackDay;
    }

    public void setAverageLeasebackDay(Integer averageLeasebackDay) {
        this.averageLeasebackDay = averageLeasebackDay;
    }

    public Integer getMaxLeasebackDay() {
        return maxLeasebackDay;
    }

    public void setMaxLeasebackDay(Integer maxLeasebackDay) {
        this.maxLeasebackDay = maxLeasebackDay;
    }

    public Integer getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(Integer couponNum) {
        this.couponNum = couponNum;
    }

    public Integer getCouponGram() {
        return couponGram;
    }

    public void setCouponGram(Integer couponGram) {
        this.couponGram = couponGram;
    }

    public Integer getInvitationNum() {
        return invitationNum;
    }

    public void setInvitationNum(Integer invitationNum) {
        this.invitationNum = invitationNum;
    }

    public Integer getInvitationBuyGram() {
        return invitationBuyGram;
    }

    public void setInvitationBuyGram(Integer invitationBuyGram) {
        this.invitationBuyGram = invitationBuyGram;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
