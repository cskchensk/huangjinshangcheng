package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

public class MemberThirdFindBean extends BaseBean implements Serializable {

    private String openId;

    private String memberId;

    private Integer type;

    private Integer clientType;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getClientType() {
        return clientType;
    }

    public void setClientType(Integer clientType) {
        this.clientType = clientType;
    }
}
