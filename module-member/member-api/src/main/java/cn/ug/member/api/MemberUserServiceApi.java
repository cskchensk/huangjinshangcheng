package cn.ug.member.api;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.member.bean.MemberInfoBean;
import cn.ug.member.bean.request.MemberUserBaseParmBean;
import cn.ug.member.bean.response.MemberUserBean;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 会员服务
 * @author ywl
 */
@RequestMapping("memberUser")
public interface MemberUserServiceApi {

    @RequestMapping(value = "/total/users", method = GET)
    SerializeObject<Integer> countTotalUsers();

    @GetMapping(value = "/list/userId")
    SerializeObject<DataTable<MemberInfoBean>> listUserId(@RequestParam("page")Integer page, @RequestParam("size")Integer size);

    @GetMapping(value = "/list/userId/with/mobiles")
    SerializeObject<List<MemberInfoBean>> listUserId(@RequestParam("donationId")String donationId);

    @PostMapping("/password/wrong/times")
    SerializeObject modifyWrongTimes(@RequestParam("memberId")String memberId,
                                     @RequestParam("type")int type,
                                     @RequestParam("wrongTimes")int wrongTimes,
                                     @RequestParam("status")int status);
    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<MemberUserBean> find(
            @RequestHeader("accessToken") String accessToken,
            @PathVariable("id") String id);

    /**
     * 根据ID查找信息
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "findById", method = GET)
    SerializeObject<MemberUserBean> findById(
            @RequestParam("id") String id);


    /**
     * 查找用户
     * @param loginName     用户名
     * @return			    用户信息
     */
    @RequestMapping(value = "findByLoginName", method = GET)
    SerializeObject<MemberUserBean> findByLoginName(
            @RequestParam("loginName") String loginName);

    /**
     * 忘记密码
     * @param  id           用户ID
     * @param password      密码
     * @return			    用户信息
     */
    @RequestMapping(value = "forgetPassword", method = PUT)
    SerializeObject forgetPassword(
            @RequestParam("id") String id,
            @RequestParam("password") String password);

    /**
     * 修改密码
     * @param password      密码
     * @param newPassword   新密码
     * @return			    用户信息
     */
    @RequestMapping(value = "updatePassword", method = PUT)
    SerializeObject updatePassword(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("password") String password,
            @RequestParam("newPassword") String newPassword);

    /**
     * 注册会员
     * @param mobile    手机号码
     * @param password  密码
     * @return
     */
    @RequestMapping(value = "register", method = POST)
    public SerializeObject register(
            @RequestParam("source") int source,
            @RequestParam("mobile") String mobile,
            @RequestParam("password") String password,
            @RequestParam("inviteCode") String inviteCode,
            @RequestParam("channelId") String channelId,
            @RequestParam("staffId") String staffId,
            @RequestParam("lotteryId") Integer lotteryId);

    @RequestMapping(value = "/shortcut/register", method = POST)
    public SerializeObject shortcutRegister(@RequestParam("source") int source,
                                     @RequestParam("mobile") String mobile,
                                     @RequestParam("inviteCode") String inviteCode,
                                     @RequestParam("channelId") String channelId,
                                     @RequestParam("staffId") String staffId,
                                     @RequestParam("lotteryId") Integer lotteryId);
    /**
     * 更新会员实名信息
     * @param memberUserBaseParmBean
     * @return
     */
    @RequestMapping(value = "updateBaseInfo", method = POST)
    public SerializeObject updateBaseInfo(@RequestBody MemberUserBaseParmBean memberUserBaseParmBean);

    /**
     * 验证交易密码是否正确
     * @param memberId     会员Id
     * @param payPassword  支付密码
     * @return
     */
    @RequestMapping(value = "validatePayPassword", method = POST)
    public SerializeObject validatePayPassword(@RequestParam("memberId") String memberId,@RequestParam("payPassword") String payPassword);

    /**
     *
     * @param memberId     会员Id
     * @param loginSource  loginSource 登录来源 2：android 3:ios
     * @return
     */
    @RequestMapping(value = "updateLoginSource", method = GET)
    public SerializeObject updateLoginSource(@RequestParam("memberId") String memberId,@RequestParam("loginSource") Integer loginSource);

    /**
     * 获取用户最早添加时间
     * @return
     */
    @RequestMapping(value = "/findMinAddTime", method = GET)
    SerializeObject<Date> findMinAddTime();

}
