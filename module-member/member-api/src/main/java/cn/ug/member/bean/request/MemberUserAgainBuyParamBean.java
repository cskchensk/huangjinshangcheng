package cn.ug.member.bean.request;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberUserAgainBuyParamBean implements Serializable {

    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";
    /** 名称 **/
    private String name;
    /** 手机号 **/
    private String mobile;
    /**
     * 手机号多个以逗号隔开
     */
    private String mobiles;
    /**渠道id  多个以逗号隔开**/
    private String channelId;
    /**
     * 累计复投克重开始值
     */
    private Integer startRepeatGram;
    /**
     * 累计复投克重结束值
     */
    private Integer endRepeatGram;
    /**
     * 累计复投金额开始值
     */
    private BigDecimal startRepeatAmount;
    /**
     * 累计复投金额结束值
     */
    private BigDecimal endRepeatAmount;
    /**
     * 累计复投次数开始值
     */
    private Integer startRepeatNum;
    /**
     * 累计复投次数结束值
     */
    private Integer endRepeatNum;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getStartRepeatGram() {
        return startRepeatGram;
    }

    public void setStartRepeatGram(Integer startRepeatGram) {
        this.startRepeatGram = startRepeatGram;
    }

    public Integer getEndRepeatGram() {
        return endRepeatGram;
    }

    public void setEndRepeatGram(Integer endRepeatGram) {
        this.endRepeatGram = endRepeatGram;
    }

    public BigDecimal getStartRepeatAmount() {
        return startRepeatAmount;
    }

    public void setStartRepeatAmount(BigDecimal startRepeatAmount) {
        this.startRepeatAmount = startRepeatAmount;
    }

    public BigDecimal getEndRepeatAmount() {
        return endRepeatAmount;
    }

    public void setEndRepeatAmount(BigDecimal endRepeatAmount) {
        this.endRepeatAmount = endRepeatAmount;
    }

    public Integer getStartRepeatNum() {
        return startRepeatNum;
    }

    public void setStartRepeatNum(Integer startRepeatNum) {
        this.startRepeatNum = startRepeatNum;
    }

    public Integer getEndRepeatNum() {
        return endRepeatNum;
    }

    public void setEndRepeatNum(Integer endRepeatNum) {
        this.endRepeatNum = endRepeatNum;
    }
}
