package cn.ug.member.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Author zhangweijie
 * @Date 2019/5/6 0006
 * @time 上午 9:24
 **/
@RequestMapping("operation")
public interface MemberOperationServiceApi {

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param memberId       登录成功后分配的Key
     * @return				    是否操作成功
     */
    @PostMapping("/login/add/click")
    SerializeObject loginAddClick(@RequestParam("memberId") String memberId);
}
