package cn.ug.member.bean;

import java.io.Serializable;

public class MemberGroupBean implements Serializable {
    private int id;
    private String name;
    //类型：1-系统创建；2-自定义类型
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
