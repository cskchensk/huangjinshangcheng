package cn.ug.member.bean.request;

import cn.ug.bean.base.Page;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author ywl
 * @date 2018/1/12 0012
 */
public class MemberUserParamBean extends Page implements Serializable {

    /** 手机号码 **/
    private String name;
    /** 手机号码,多个手机号以逗号隔开 **/
    private String mobile;
    /** 邀请人手机号 **/
    private String inviteMobile;
    /** 身份证号码 **/
    private String idCard;
    /** 起始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";
    /**渠道id  多个以逗号隔开**/
    private String channelId;
    /**绑卡开始时间**/
    private String tiedCardStartData;
    /**绑卡结束时间**/
    private String tiedCardEndData;

    /**********************新增字段*************************/
    /**最近回租开始时间**/
    private String newLeasebackDataStart;
    /**最近回租结束时间**/
    private String newLeasebackDataEnd;
    /**持有提单克重最小值**/
    private Integer tbTotalGramMin;
    /**持有提单克重最大值**/
    private Integer tbTotalGramMax;
    /**账户余额最小值**/
    private BigDecimal usableAmountMin;
    /**账户余额最大值**/
    private BigDecimal usableAmountMax;
    /**
     * 批量导入的手机号
     */
    private List<String> mobiles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getInviteMobile() {
        return inviteMobile;
    }

    public void setInviteMobile(String inviteMobile) {
        this.inviteMobile = inviteMobile;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTiedCardStartData() {
        return tiedCardStartData;
    }

    public void setTiedCardStartData(String tiedCardStartData) {
        this.tiedCardStartData = tiedCardStartData;
    }

    public String getTiedCardEndData() {
        return tiedCardEndData;
    }

    public void setTiedCardEndData(String tiedCardEndData) {
        this.tiedCardEndData = tiedCardEndData;
    }

    public String getNewLeasebackDataStart() {
        return newLeasebackDataStart;
    }

    public void setNewLeasebackDataStart(String newLeasebackDataStart) {
        this.newLeasebackDataStart = newLeasebackDataStart;
    }

    public String getNewLeasebackDataEnd() {
        return newLeasebackDataEnd;
    }

    public void setNewLeasebackDataEnd(String newLeasebackDataEnd) {
        this.newLeasebackDataEnd = newLeasebackDataEnd;
    }

    public Integer getTbTotalGramMin() {
        return tbTotalGramMin;
    }

    public void setTbTotalGramMin(Integer tbTotalGramMin) {
        this.tbTotalGramMin = tbTotalGramMin;
    }

    public Integer getTbTotalGramMax() {
        return tbTotalGramMax;
    }

    public void setTbTotalGramMax(Integer tbTotalGramMax) {
        this.tbTotalGramMax = tbTotalGramMax;
    }

    public BigDecimal getUsableAmountMin() {
        return usableAmountMin;
    }

    public void setUsableAmountMin(BigDecimal usableAmountMin) {
        this.usableAmountMin = usableAmountMin;
    }

    public BigDecimal getUsableAmountMax() {
        return usableAmountMax;
    }

    public void setUsableAmountMax(BigDecimal usableAmountMax) {
        this.usableAmountMax = usableAmountMax;
    }

    public List<String> getMobiles() {
        return mobiles;
    }

    public void setMobiles(List<String> mobiles) {
        this.mobiles = mobiles;
    }
}
