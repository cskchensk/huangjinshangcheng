package cn.ug.member.bean.request;

import java.io.Serializable;

/**
 * 绑卡后更新用户基本信息
 * @auther ywl
 */
public class MemberUserBaseParmBean implements Serializable {

    /** 会员id **/
    private String id;
    /** 姓名 **/
    private String name;
    /** 性别 **/
    private Integer gender;
    /** 年龄 **/
    private Integer age;
    /** 身份证号码 **/
    private String idCard;
    /** 生日**/
    private String birthday;
    private String province;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
