package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * 收获地址
 * @author ywl
 * @date 2018/1/14 0014
 */
public class MemberAddressBean  extends BaseBean implements Serializable{

    /** 会员id **/
    private String memberId;
    /** 收货人姓名 **/
    private String name;
    /** 收货人手机号码 **/
    private String mobile;
    /** 省份 **/
    private String province;
    /** 城市 **/
    private String city;
    /** 地区 **/
    private String area;
    /** 详细地址 **/
    private String address;
    /** 是否默认 1：否 2：是 **/
    private Integer isDefault;
    /** 描述 **/
    private String description;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
