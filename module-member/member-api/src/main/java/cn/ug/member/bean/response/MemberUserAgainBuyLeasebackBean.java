package cn.ug.member.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberUserAgainBuyLeasebackBean implements Serializable {

    private int index;
    /**
     * 回租记录主键
     */
    private int id;
    /**
     *复投用户主键
     */
    private String memberId;
    /**
     * 提单订单号
     */
    private String tbillNo;
    /**
     * 回租订单号
     */
    private String orderNo;
    /**
     * 复投开始时间
     */
    private String startTime;
    /**
     * 复投到期时间
     */
    private String endTime;
    /**
     * 复投时金价
     */
    private BigDecimal goldPrice;
    /**
     * 复投回租期限
     */
    private int leasebackDays;
    /**
     * 复投产品
     */
    private String productName;
    /**
     * 复投提单克重
     */
    private Integer repeatGram;

    /**
     * 复投提单金额
     */
    private BigDecimal repeatAmount;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTbillNo() {
        return tbillNo;
    }

    public void setTbillNo(String tbillNo) {
        this.tbillNo = tbillNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getRepeatGram() {
        return repeatGram;
    }

    public void setRepeatGram(Integer repeatGram) {
        this.repeatGram = repeatGram;
    }

    public BigDecimal getRepeatAmount() {
        return repeatAmount;
    }

    public void setRepeatAmount(BigDecimal repeatAmount) {
        this.repeatAmount = repeatAmount;
    }
}
