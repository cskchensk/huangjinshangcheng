package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * @author ywl
 * @date 2018/1/12 0012
 */
public class MemberUserFindBean extends BaseBean implements Serializable {

    /** 姓名 **/
    private String name;
    /** 性别 **/
    private Integer gender;
    /** 年龄 **/
    private Integer age;
    /** 手机号 **/
    private String mobile;
    /** 身份证号码 **/
    private String idCard;
    /** 生日**/
    private String birthday;
    /** 用户等级 **/
    private String level;
    /** 头像 **/
    private String head;
    /** 邀请码 **/
    private String inviteCode;
    /** 状态 1：正常  2：冻结 **/
    private int status;
    /** 省份 **/
    private String province;
    /** 城市 **/
    private String city;
    /** 地区 **/
    private String area;
    /** 详细地址 **/
    private String address;
    /** 省份名称 **/
    private String provinceName;
    /** 城市名称 **/
    private String cityName;
    /** 地区名称 **/
    private String areaName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
