package cn.ug.member.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberUserAgainBuyBean extends BaseBean implements Serializable {

    private int index;

    private String channelId;

    /** 姓名 **/
    private String name;

    /** 手机号 **/
    private String mobile;

    /**渠道**/
    private String channelName;
    /**
     * 累计复投次数
     */
    private Integer  totalRepeatNum;
    /**
     * 累计复投克重
     */
    private Integer totalRepeatGram;

    /**
     * 累计复投金额
     */
    private BigDecimal totalRepeatAmount;

    /**
     * 单笔复投最高克重
     */
    private Integer maxRepeatGram;

    /**
     * 单笔复投最高金额
     */
    private BigDecimal maxRepeatAmount;


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Integer getTotalRepeatNum() {
        return totalRepeatNum;
    }

    public void setTotalRepeatNum(Integer totalRepeatNum) {
        this.totalRepeatNum = totalRepeatNum;
    }

    public Integer getTotalRepeatGram() {
        return totalRepeatGram;
    }

    public void setTotalRepeatGram(Integer totalRepeatGram) {
        this.totalRepeatGram = totalRepeatGram;
    }

    public BigDecimal getTotalRepeatAmount() {
        return totalRepeatAmount;
    }

    public void setTotalRepeatAmount(BigDecimal totalRepeatAmount) {
        this.totalRepeatAmount = totalRepeatAmount;
    }

    public Integer getMaxRepeatGram() {
        return maxRepeatGram;
    }

    public void setMaxRepeatGram(Integer maxRepeatGram) {
        this.maxRepeatGram = maxRepeatGram;
    }

    public BigDecimal getMaxRepeatAmount() {
        return maxRepeatAmount;
    }

    public void setMaxRepeatAmount(BigDecimal maxRepeatAmount) {
        this.maxRepeatAmount = maxRepeatAmount;
    }
}
