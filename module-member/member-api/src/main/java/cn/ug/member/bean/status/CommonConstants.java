package cn.ug.member.bean.status;

/**
 * 会员常量
 * @author ywl
 */
public class CommonConstants {

		/** 正常(未锁定) */
		public static final int UNLOCK 	= 1;
		/** 锁定 */
		public static final int LOCK	= 2;

}
