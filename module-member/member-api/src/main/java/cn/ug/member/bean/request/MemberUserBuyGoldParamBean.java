package cn.ug.member.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户购金情况实体类
 * @Author zhangweijie
 * @Date 2019/6/10 0010
 * @time 上午 11:30
 **/
public class MemberUserBuyGoldParamBean extends Page implements Serializable {
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";
    /** 名称 **/
    private String name;

    private String mobile;
    /**
     * 手机号多个以逗号隔开
     */
    private String mobiles;

    /**
     * 回租总收益
     */
    private BigDecimal totalIncomeAmountMin;

    /**
     * 回租总收益
     */
    private BigDecimal totalIncomeAmountMax;

    /**
     * 黄金买卖收益
     */
    private BigDecimal buySellIncomeAmountMin;

    /**
     * 黄金买卖收益
     */
    private BigDecimal buySellIncomeAmountMax;

    /**
     * 最高购买克重总数
     */
    private Integer  maxBuyGramMin;

    /**
     * 最高购买克重总数
     */
    private Integer  maxBuyGramMax;

    /**
     *累计复投次数
     */
    private Integer  totalRepeatNumMin;

    /**
     * 累计复投次数
     */
    private Integer  totalRepeatNumMax;

    /**
     * 上次回租结束时间
     */
    private String frontLeasebackEndDateMin;

    /**
     * 上次回租结束时间
     */
    private String frontLeasebackEndDateMax;

    /**
     * 回租中克重
     */
    private Integer leasebackGramMin;

    /**
     * 回租中克重
     */
    private Integer leasebackGramMax;



    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalIncomeAmountMin() {
        return totalIncomeAmountMin;
    }

    public void setTotalIncomeAmountMin(BigDecimal totalIncomeAmountMin) {
        this.totalIncomeAmountMin = totalIncomeAmountMin;
    }

    public BigDecimal getTotalIncomeAmountMax() {
        return totalIncomeAmountMax;
    }

    public void setTotalIncomeAmountMax(BigDecimal totalIncomeAmountMax) {
        this.totalIncomeAmountMax = totalIncomeAmountMax;
    }

    public BigDecimal getBuySellIncomeAmountMin() {
        return buySellIncomeAmountMin;
    }

    public void setBuySellIncomeAmountMin(BigDecimal buySellIncomeAmountMin) {
        this.buySellIncomeAmountMin = buySellIncomeAmountMin;
    }

    public BigDecimal getBuySellIncomeAmountMax() {
        return buySellIncomeAmountMax;
    }

    public void setBuySellIncomeAmountMax(BigDecimal buySellIncomeAmountMax) {
        this.buySellIncomeAmountMax = buySellIncomeAmountMax;
    }

    public Integer getMaxBuyGramMin() {
        return maxBuyGramMin;
    }

    public void setMaxBuyGramMin(Integer maxBuyGramMin) {
        this.maxBuyGramMin = maxBuyGramMin;
    }

    public Integer getMaxBuyGramMax() {
        return maxBuyGramMax;
    }

    public void setMaxBuyGramMax(Integer maxBuyGramMax) {
        this.maxBuyGramMax = maxBuyGramMax;
    }

    public Integer getTotalRepeatNumMin() {
        return totalRepeatNumMin;
    }

    public void setTotalRepeatNumMin(Integer totalRepeatNumMin) {
        this.totalRepeatNumMin = totalRepeatNumMin;
    }

    public Integer getTotalRepeatNumMax() {
        return totalRepeatNumMax;
    }

    public void setTotalRepeatNumMax(Integer totalRepeatNumMax) {
        this.totalRepeatNumMax = totalRepeatNumMax;
    }

    public String getFrontLeasebackEndDateMin() {
        return frontLeasebackEndDateMin;
    }

    public void setFrontLeasebackEndDateMin(String frontLeasebackEndDateMin) {
        this.frontLeasebackEndDateMin = frontLeasebackEndDateMin;
    }

    public String getFrontLeasebackEndDateMax() {
        return frontLeasebackEndDateMax;
    }

    public void setFrontLeasebackEndDateMax(String frontLeasebackEndDateMax) {
        this.frontLeasebackEndDateMax = frontLeasebackEndDateMax;
    }

    public Integer getLeasebackGramMin() {
        return leasebackGramMin;
    }

    public void setLeasebackGramMin(Integer leasebackGramMin) {
        this.leasebackGramMin = leasebackGramMin;
    }

    public Integer getLeasebackGramMax() {
        return leasebackGramMax;
    }

    public void setLeasebackGramMax(Integer leasebackGramMax) {
        this.leasebackGramMax = leasebackGramMax;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
