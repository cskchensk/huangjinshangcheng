package cn.ug.member.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InvitationBean implements Serializable {
    private String id;
    private String mobile;
    private String registerDate;
    private int bindedCard;
    //private BigDecimal tradeTotalAmount;
    //private String tradeTime;

    public InvitationBean() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public int getBindedCard() {
        return bindedCard;
    }

    public void setBindedCard(int bindedCard) {
        this.bindedCard = bindedCard;
    }
}
