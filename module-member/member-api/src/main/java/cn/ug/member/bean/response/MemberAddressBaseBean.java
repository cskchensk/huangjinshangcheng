package cn.ug.member.bean.response;

public class MemberAddressBaseBean extends  MemberAddressBean {

    /** 省份名称 **/
    private String provinceName;
    /** 城市名称 **/
    private String cityName;
    /** 地区名称 **/
    private String areaName;

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }
}
