package cn.ug.member.mq;

import java.io.Serializable;

public class MemberLabelMQ implements Serializable {
    // 会员id
    private String memberId;
    // 类型：1--注册；2--登陆；3--认证绑卡
    private int type;
    private String mobile;
    private String name;
    private String idcard;
    private String channelId;
    private String getChannelName;
    private String ip;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getGetChannelName() {
        return getChannelName;
    }

    public void setGetChannelName(String getChannelName) {
        this.getChannelName = getChannelName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
