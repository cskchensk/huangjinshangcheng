package cn.ug.member.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.member.bean.response.MemberThirdFindBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RequestMapping("memberThird")
public interface MemberThirdServiceApi {

    /**
     * 第三方账号绑定
     * @param partnerId
     * @param memberId
     * @param type
     * @return
     */
    @RequestMapping(value = "save", method = POST)
    SerializeObject save(@RequestParam("partnerId") String partnerId, @RequestParam("memberId") String memberId, @RequestParam("type") Integer type,@RequestParam("clientType")Integer clientType);

    /**
     * 账号查询
     * @param partnerId  第三方id
     * @param memberId   会员id
     * @param type       类型  1：微信账号
     * @return
     */
    @GetMapping(value = "find")
    SerializeObject<MemberThirdFindBean> find(@RequestParam("partnerId") String partnerId, @RequestParam("memberId") String memberId, @RequestParam("type") Integer type);

    /**
     * 第三方账号修改
     * @param partnerId
     * @param memberId
     * @param type
     * @return
     */
    @RequestMapping(value = "update", method = POST)
    SerializeObject update(@RequestParam("partnerId") String partnerId, @RequestParam("memberId") String memberId, @RequestParam("type") Integer type,@RequestParam("clientType")Integer clientType);

    /**
     * 第三方账号删除
     * @param partnerId
     * @param type
     * @return
     */
    @RequestMapping(value = "delete", method = POST)
    SerializeObject delete(@RequestParam("partnerId") String partnerId,  @RequestParam("type") Integer type);
}




