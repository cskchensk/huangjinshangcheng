package cn.ug.member.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

/**
 * @Author zhangweijie
 * @Date 2019/6/10 0010
 * @time 下午 17:20
 **/
public class MemberUserBehaviorParamBean extends Page implements Serializable {

    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    /** 名称 **/
    private String name;

    private String mobile;
    /**
     * 手机号多个以逗号隔开
     */
    private String mobiles;

    /**渠道id  多个以逗号隔开**/
    private String channelId;
    /**
     * 优惠券使用次数
     */
    private Integer couponNumMin;

    /**
     * 优惠券使用次数
     */
    private Integer couponNumMax;

    /**
     * 邀请人数
     */
    private Integer invitationNumMin;

    /**
     * 邀请人数
     */
    private Integer invitationNumMax;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getCouponNumMin() {
        return couponNumMin;
    }

    public void setCouponNumMin(Integer couponNumMin) {
        this.couponNumMin = couponNumMin;
    }

    public Integer getCouponNumMax() {
        return couponNumMax;
    }

    public void setCouponNumMax(Integer couponNumMax) {
        this.couponNumMax = couponNumMax;
    }

    public Integer getInvitationNumMin() {
        return invitationNumMin;
    }

    public void setInvitationNumMin(Integer invitationNumMin) {
        this.invitationNumMin = invitationNumMin;
    }

    public Integer getInvitationNumMax() {
        return invitationNumMax;
    }

    public void setInvitationNumMax(Integer invitationNumMax) {
        this.invitationNumMax = invitationNumMax;
    }
}
