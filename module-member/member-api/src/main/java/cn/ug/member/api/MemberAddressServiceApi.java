package cn.ug.member.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.member.bean.AddressBean;
import cn.ug.member.bean.MemberLogisticsBean;
import cn.ug.member.bean.response.MemberAddressBaseBean;
import cn.ug.member.bean.response.MemberAddressBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author  ywl
 * @date 2018-04-19
 */

@RequestMapping("memberAddress")
public interface MemberAddressServiceApi {

    /**
     * 根据ID查找信息
     * @param memberId		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "findByMemberId", method = GET)
    SerializeObject<MemberAddressBaseBean> findByMemberId(@RequestParam("memberId") String memberId);

    @RequestMapping(value = "/address", method = GET)
    SerializeObject<MemberAddressBean> get(@RequestParam("id")String id);

    @RequestMapping(value = "/mall/address", method = GET)
    SerializeObject<AddressBean> getAddress(@RequestParam("addressId")long addressId);

    @RequestMapping(value = "/logistics/address", method = GET)
    SerializeObject<MemberLogisticsBean> getLogisticsAddress(@RequestParam("addressId")long addressId);

    @RequestMapping(value = "/quantity", method = GET)
    SerializeObject<Integer> getAddressNum(@RequestParam("memberId")String memberId);
}
