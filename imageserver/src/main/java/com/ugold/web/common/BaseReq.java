package com.ugold.web.common;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 请求参数的基础类
 * 
 * @author dingjian
 * @date 2018年9月14日
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseReq implements Serializable {

	private static final long serialVersionUID = -224895875549002018L;

	private String accessToken;

	private String clientVersion;

	// A(Android), I(IOS)
	private String osType;

	/**
	 * 检查参数是否合法
	 *
	 * @return
	 */
	public void checkData() {
		// TODO
	}
}
