package com.ugold.web.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 服务错误
 * 
 * @author dingjian
 * @date 2018年9月21日
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IntegrationException extends RuntimeException {

	private static final long serialVersionUID = 1506433762071533808L;

	private Integer errorCode;

	private String errorMessage;

	public IntegrationException(String errorMessage) {
		this.errorCode = ErrorEnum.ERROR_DEFAULT.getErrorCode();
		this.errorMessage = errorMessage;
	}

	public IntegrationException(ErrorEnum error) {
		this.errorCode = error.getErrorCode();
		this.errorMessage = error.getErrorMessage();
	}

	public IntegrationException(ErrorEnum error, String errorMessage) {
		this.errorCode = error.getErrorCode();
		this.errorMessage = errorMessage;
	}

	public IntegrationException(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
