package com.ugold.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ugold.web.common.Result;
import com.ugold.web.common.exception.ErrorEnum;

@RestController
public class MainsiteErrorController implements ErrorController {

	/**
	 * 处理异常（仅处理404异常，其他均被ErrorAdvice处理）
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping("/error")
	public Result<?> handleError(HttpServletRequest request) {
		// （获取statusCode:401,404,500）仅处理404异常，其他均被ErrorAdvice处理
		Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
		if (statusCode != null) {
			return Result.buildFailureResult(statusCode, ErrorEnum.ERROR_SERVICE.getErrorMessage());
		}
		return Result.buildFailureResult(ErrorEnum.ERROR_SERVICE.getErrorCode(),
				ErrorEnum.ERROR_SERVICE.getErrorMessage());
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}

}
