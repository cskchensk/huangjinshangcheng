package com.ugold.web.interceptor;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 * @author dingjian
 * @date 2019/03/28
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {
	// implements WebMvcConfigurer
	// extends WebMvcConfigurationSupport

	@Value("#{'${allow.domain.name}'.split(',')}")
	private List<String> allowedDomain;

	/**
	 * 跨域处理
	 *
	 * @param registry
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		String[] domains = new String[3];
		allowedDomain.toArray(domains);
		registry.addMapping("/**").allowedOrigins("*").allowedHeaders("*").allowedMethods("*").allowCredentials(true)
				.maxAge(1800);
//		registry.addMapping("/**").allowedOrigins(domains).allowedHeaders("*").allowedMethods("*")
//				.allowCredentials(true).maxAge(1800);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {

	}
}