package com.ugold.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.ugold")
public class ImagesServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImagesServerApplication.class, args);
	}

}
