package cn.ug.util;

import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;

/**
 * @author zhaohg
 * @date 2018/08/07.
 */

public class ChannelNumberUtil {
    private static ChannelNumberUtil instance = null;

    private ChannelNumberUtil() {
    }

    /**
     * 取得ChannelNumberUtil的单例实现
     *
     * @return
     */
    public static ChannelNumberUtil getInstance() {
        if (instance == null) {
            synchronized (ChannelNumberUtil.class) {
                if (instance == null) {
                    instance = new ChannelNumberUtil();
                }
            }
        }
        return instance;
    }

    /**
     * 生成下一个成员编号
     *
     * @param sno    传最大的编号，才能返回累加后的!
     * @param prefix 生成的编号前缀
     * @return
     */
    public synchronized String nextStaffNumber(String sno, String prefix) {
        DecimalFormat df = new DecimalFormat("0000");
        if (StringUtils.isEmpty(sno) || "0".equals(sno)) {
            sno = "0000";
        }
        sno = df.format(Integer.parseInt(sno.substring(sno.length() - 4, sno.length())));
        int number = Integer.parseInt(sno);
        if (number >= 9999) {
            return null;
        }
        return prefix + df.format(1 + number);
    }

    public synchronized String nextChannelNumber(String sno) {
        DecimalFormat df = new DecimalFormat("000");
        if (StringUtils.isEmpty(sno) || "0".equals(sno)) {
            sno = "000";
        }
        int number = Integer.parseInt(sno);
        if (number >= 999) {
            return null;
        }
        return df.format(1 + number);
    }

    public static void main(String[] args) {
        System.out.println(ChannelNumberUtil.getInstance().nextChannelNumber("098"));
        System.out.println(ChannelNumberUtil.getInstance().nextStaffNumber("0070001", "007"));

        System.out.println(Integer.parseInt("00022"));
    }
}