package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.service.MemberWithdrawService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

/**
 *  提金数据统计
 *  @author ywl
 */
@RestController
@RequestMapping("memberWithdraw")
public class MemberWithdrawController{

    @Resource
    private MemberWithdrawService memberWithdrawService;

    /**
     * 按照日、周、月统计的列表
     * @param accessToken
     * @return
     */
    @GetMapping(value = "findList")
    public SerializeObject<DataTable<BaseCountBean>> findWithdrawTotalList(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        DataTable<BaseCountBean> list = memberWithdrawService.findWithdrawTotalList(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, list);
    }
    @GetMapping(value = "findTotal")
    public SerializeObject<BigDecimal> findTotal(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        BigDecimal amount = memberWithdrawService.findTotal(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, amount);
    }

   /* *//**
     * 每日走势图列表
     * @param accessToken
     * @return
     *//*
    @GetMapping(value = "findEverydayList")
    public SerializeObject<List<BaseCountBean>> findEverydayList(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        List<BaseCountBean> list = memberWithdrawService.findEverydayList(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, list);
    }*/

}
