package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.ChannelRewardsBean;
import cn.ug.analyse.mapper.entity.ChannelPeriodEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.analyse.mapper.entity.SettleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 渠道结算
 * @author zhaohg
 * @date 2018/08/11.
 */
@Mapper
public interface SettleMapper {

    /**
     * 添加结算记录
     * @param entity
     * @return
     */
    int insert(SettleEntity entity);

    /**
     * 结算记录行数
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);

    /**
     * 结算记录list
     * @param submit
     * @return
     */
    List<SettleEntity> findList(QuerySubmit submit);

    /**
     * 获取渠道周期记录
     * @param channelId
     * @param startDate
     * @param endDate
     * @return
     */
    ChannelPeriodEntity getChannelPeriodByDate(@Param("channelId") Long channelId,
                                               @Param("startDate") Date startDate,
                                               @Param("endDate") Date endDate);

    /**
     * 添加渠道周期记录
     * @param period
     * @return
     */
    int addChannelPeriod(ChannelPeriodEntity period);

    /**
     * 渠道周期记录行数
     * @param submit
     * @return
     */
    int getChannelPeriodCount(QuerySubmit submit);

    /**
     * 渠道周期记录list
     * @param submit
     * @return
     */
    List<ChannelPeriodEntity> getChannelPeriodList(QuerySubmit submit);

    BigDecimal sumOfAmount(@Param("channelId")int channelId);
    List<ChannelRewardsBean> queryForList(Map<String, Object> params);
    ChannelRewardsBean selectRewardsDetail(@Param("channelId") int channelId,
                                           @Param("month") String month);
    int queryForCount(Map<String, Object> params);
}
