package cn.ug.analyse.web.submit;

import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.Common;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public class ChannelStaffSubmit {

    private Long id;
    private Long channelId;
    private Long staffId;

    private String realName;
    private String mobile;
    private Integer isEnable;

    private Integer pageNum = 1;
    private Integer pageSize= 10;
    private String startTime;
    private String endTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void checkParams() {
        Ensure.that(channelId > 0).isFalse("00000002");
        Ensure.that(StringUtils.isNotEmpty(realName) && realName.length() < 30).isFalse("210000023");
        Ensure.that(Common.validateMobile(mobile)).isFalse("210000022");

    }

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("channelId", channelId);
        querySubmit.put("staffId", staffId);
        querySubmit.put("realName", realName);
        querySubmit.put("mobile", mobile);

        if(StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(this.startTime));
        }
        if(StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(this.endTime));
        }


    }

}
