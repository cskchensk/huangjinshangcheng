package cn.ug.analyse.mapper.entity;

/**
 * @Author zhangweijie
 * @Date 2019/6/14 0014
 * @time 下午 15:21
 **/
public class ChannelStatisticsBean extends ChannelRepBean{
    private String channelId;

    private String channelName;

    private Integer uv = 0;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Integer getUv() {
        return uv;
    }

    public void setUv(Integer uv) {
        this.uv = uv;
    }
}
