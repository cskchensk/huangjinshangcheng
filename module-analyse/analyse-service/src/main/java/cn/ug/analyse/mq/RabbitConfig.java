package cn.ug.analyse.mq;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static cn.ug.config.QueueName.*;

/**
 * 消息配置
 * @author kaiwotech
 */
@Configuration
public class RabbitConfig {

    /**
     * 会员数据分析队列
     * @return
     */
    @Bean
    public Queue memberAnalyseQueue() {
        return new Queue(QUEUE_MEMBER_ANALYSE, true);
    }


}
