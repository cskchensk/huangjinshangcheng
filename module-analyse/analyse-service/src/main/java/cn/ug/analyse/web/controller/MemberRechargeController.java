package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.request.BaseParamBean;
import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.bean.response.MemberRechargeBean;
import cn.ug.analyse.service.MemberRechargeService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 会员充值数据统计
 */
@RestController
@RequestMapping("memberRecharge")
public class MemberRechargeController {

    @Resource
    private MemberRechargeService memberRechargeService;

    /**
     * 按照日、周、月统计的列表
     * @param accessToken
     * @return
     */
    @GetMapping(value = "findList")
    public SerializeObject<DataTable<MemberRechargeBean>> findList(@RequestHeader String accessToken, BaseParamBean baseParamBean) {
        DataTable<MemberRechargeBean> list = memberRechargeService.findList(baseParamBean);
        return new SerializeObject(ResultType.NORMAL, list);
    }

    /**
     * 总额
     * @param accessToken
     * @param baseParamBean
     * @return
     */
    @GetMapping(value = "findTotal")
    public SerializeObject<MemberRechargeBean> findTotal(@RequestHeader String accessToken, BaseParamBean baseParamBean) {
        MemberRechargeBean bean = memberRechargeService.findTotal(baseParamBean);
        return new SerializeObject(ResultType.NORMAL, bean);
    }


}
