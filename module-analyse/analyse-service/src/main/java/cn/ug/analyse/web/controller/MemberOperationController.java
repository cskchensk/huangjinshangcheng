package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.request.OperationParamBean;
import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.bean.response.BaseOperationBean;
import cn.ug.analyse.service.MemberOperationService;
import cn.ug.bean.base.DataOtherTable;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.pay.bean.OperatorCostBean;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 运营总表
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 13:37
 **/

@RestController
@RequestMapping("memberOperation")
public class MemberOperationController {

    @Autowired
    private MemberOperationService memberOperationService;
    /**
     * 按照日列表
     * @param accessToken
     * @return
     */
    @GetMapping(value = "findList")
    public SerializeObject<DataTable<BaseCountBean>> findOperationTotalList(@RequestHeader String accessToken, OperationParamBean operationParamBean) {
        return new SerializeObject(ResultType.NORMAL,memberOperationService.findOperationTotalList(operationParamBean));
    }


    /**
     * 导出
     * @return
     */
    @GetMapping(value = "/export")
    public void export(OperationParamBean operationParamBean, HttpServletResponse response) {
        operationParamBean.setPageNum(1);
        operationParamBean.setPageSize(Integer.MAX_VALUE);
        DataOtherTable dataOtherTable = memberOperationService.findOperationTotalList(operationParamBean);

        List<BaseOperationBean> baseOperationBeanList = dataOtherTable.getDataList();

        if (!CollectionUtils.isEmpty(baseOperationBeanList)) {
            int index = 1;
            for (BaseOperationBean operationBean : baseOperationBeanList) {
                operationBean.setIndex(index);
                index++;
            }
        }
        String[] columnNames = {"序号", "日期", "注册人数", "绑卡人数", "首投人数", "复投人数", "买金人数", "买金克重（g）","买金金额（元）",
                "卖金人数","卖金克重（g）","人均购买金额（元）","用户提现金额（元）","用户账户余额（元）","使用黄金红包人数"};
        String[] columns = {"index", "day", "registerNum", "bdCardNum", "firstBuyNum", "againBuyNum", "buyGoldNum", "buyGoldGram", "buyGoldAmount", "soldGoldNum",
                "soldGoldGram", "averageBuyGoldAmount", "withdrawAmount", "balanceAmount", "couponNum"};
        String fileName = "运营总表";
        ExportExcelController<BaseOperationBean> export = new ExportExcelController<BaseOperationBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, baseOperationBeanList, response, ExportExcelUtil.EXCEL_FILE_2003);
    }
}
