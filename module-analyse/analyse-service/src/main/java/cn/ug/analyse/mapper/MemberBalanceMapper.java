package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.mapper.entity.MemberBalance;
import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.response.MemberAccountBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public interface MemberBalanceMapper extends BaseMapper<MemberBalance>{

    List<BaseCountBean> findList(WithdeawParamBean withdeawParamBean);

    List<MemberAccountBean> findMemberBalanceList();

    int insert(List<MemberBalance> memberBalanceList);

    BigDecimal findAmountByDay(@Param("day") String day);

    BigDecimal findBillTotal(@Param("day") String day,@Param("type") int type);
}
