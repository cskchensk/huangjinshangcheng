package cn.ug.analyse.web.job;

import cn.ug.analyse.service.MemberHoldGoldService;
import cn.ug.config.RedisGlobalLock;
import cn.ug.util.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 用户持有黄金总克重
 * @date 2018-06-11
 */
@Component
public class MembeHoldJobs implements Serializable{


    /** 业务有效期 */
    private static final long EXPIRE_DAY = 1024;

    @Resource
    private MemberHoldGoldService memberHoldGoldService;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    private static Logger logger = LoggerFactory.getLogger(MemberBalanceJobs.class);

    /**
     * 用户持有黄金总克重
     * 凌晨12点执行
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void memberHoldJob(){
        String key = "MembeHoldJobs:expiredMemberHoldJob:" + UF.getFormatDateNow();
        if(redisGlobalLock.lock(key, EXPIRE_DAY, TimeUnit.DAYS)) {
            try{
                logger.info("开始执行持有黄金统计定时任务");
                logger.info(UF.getFormatDateTime(UF.getDateTime()));
                //执行任务
                memberHoldGoldService.memberHoldJob();
            }catch (Exception e){
                throw  e;
            }finally {
                redisGlobalLock.unlock(key);
            }
        }else{
            logger.info("-------没有获取到锁-------");
            return ;
        }

    }
}
