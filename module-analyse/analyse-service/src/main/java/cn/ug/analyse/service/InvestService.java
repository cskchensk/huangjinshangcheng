package cn.ug.analyse.service;

import cn.ug.analyse.bean.GrossBean;
import cn.ug.analyse.bean.InvestBean;
import cn.ug.analyse.bean.TradingDetailBean;
import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.bean.base.SerializeObject;

import java.util.Date;
import java.util.List;

/**
 * 渠道投资人交易记录 绑卡关系
 * @author zhaohg
 * @date 2018/08/09.
 */
public interface InvestService {

    /**
     * 获取昨天一级用户
     * @param ids 渠道用户id
     * @return
     */
    List<InviteEntity> findYesterdayFirstList(List<Long> ids);

    /**
     * 获取昨天二级用户
     * @param ids 一级用户id
     * @return
     */
    List<InviteEntity> findYesterdaySecondList(List<String> ids);

    /**
     * 获取昨天绑卡的用户
     * @param ids
     * @param date
     * @return
     */
    List<InviteEntity> findYesterdayBindCardList(List<String> ids, Date date);

    /**
     * 获取昨天用户交易总量
     * @param ids
     * @param date
     * @return
     */
    GrossBean findYesterdayFirstListByUserIds(List<String> ids, Date date);


    SerializeObject findTotal(OtherSubmit submit);
    /**
     * 渠道下投资人的交易记录
     * @param submit
     * @return
     */
    SerializeObject findList(OtherSubmit submit);

    /**
     * 渠道下投资人的交易记录 导出
     * @param submit
     * @return
     */
    List<InvestBean> export(OtherSubmit submit);

    List<TradingDetailBean> queryForDetailList(int staffId, String month, int inviteType, int offset, int size);
    int queryForDetailCount(int staffId, String month, int inviteType);
}
