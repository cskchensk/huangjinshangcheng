package cn.ug.analyse.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员持有黄金克重
 */
public class MemberHold  extends BaseEntity implements Serializable{

    /** 天数 **/
    private String day;
    /** 会员id **/
    private String memberId;
    /** 金额 **/
    private BigDecimal amount;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
