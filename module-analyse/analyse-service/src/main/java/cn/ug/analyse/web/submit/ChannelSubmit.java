package cn.ug.analyse.web.submit;

import cn.ug.analyse.bean.RewardPlanBean;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.Common;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public class ChannelSubmit {

    /**
     * id
     */
    private Long       id;
    /**
     * 渠道名称
     */
    private String     name;
    /**
     * 渠道编号
     */
    private String     number;
    /**
     * 奖励方案 1:阶梯交易额 2:交易周期 3:交易频数
     */
    private Integer    planType;
    /**
     * 周期 1月 2季度 3年
     */
    private Integer    period;
    /**
     * 二级奖励比例
     */
    private BigDecimal secondReward;
    /**
     * 接力方案json
     */
    private String     settleStep;
    /**
     * 一级邀请奖励
     */
    private BigDecimal oneInviteReward;
    /**
     * 二级邀请奖励
     */
    private BigDecimal twoInviteReward;
    /**
     * 有效开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date       enableDate;
    /**
     * 无效开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date       expireDate;
    /**
     * 启用禁用 0:禁用 1:启用
     */
    private Integer    isEnable;

    /**
     * 渠道关联人id
     */
    private Long   relationId;
    private String realName;
    private String mobile;


    private List<RewardPlanBean> planList;


    private Integer pageNum  = 1;
    private Integer pageSize = 10;


    private Long channelId;

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPlanType() {
        return planType;
    }

    public void setPlanType(Integer planType) {
        this.planType = planType;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public BigDecimal getSecondReward() {
        return secondReward;
    }

    public void setSecondReward(BigDecimal secondReward) {
        this.secondReward = secondReward;
    }

    public String getSettleStep() {
        return settleStep;
    }

    public void setSettleStep(String settleStep) {
        this.settleStep = settleStep;
    }

    public BigDecimal getOneInviteReward() {
        return oneInviteReward;
    }

    public void setOneInviteReward(BigDecimal oneInviteReward) {
        this.oneInviteReward = oneInviteReward;
    }

    public BigDecimal getTwoInviteReward() {
        return twoInviteReward;
    }

    public void setTwoInviteReward(BigDecimal twoInviteReward) {
        this.twoInviteReward = twoInviteReward;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<RewardPlanBean> getPlanList() {
        return planList;
    }

    public void setPlanList(List<RewardPlanBean> planList) {
        this.planList = planList;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    //添加渠道参数验证
    public void checkAddChannelParams() {
        Ensure.that(StringUtils.isEmpty(name)).isTrue("210000018");
        Ensure.that(StringUtils.isNotEmpty(name) && name.length() < 30).isFalse("21000007");
        Ensure.that(planType != null && planType > 0 && planType < 4).isFalse("00000002");
        Ensure.that(enableDate != null).isFalse("21000008");
        Ensure.that(expireDate != null).isFalse("21000009");
        Ensure.that(StringUtils.isEmpty(realName)).isTrue("210000019");
        Ensure.that(StringUtils.isNotEmpty(realName) && realName.length() < 20).isFalse("210000016");
        Ensure.that(Common.validateMobile(mobile)).isFalse("210000017");
    }

    //验证邀请方案参数
    public void checkPlanParams() {
        Ensure.that(period != null && period > 0 && period < 4).isFalse("210000021");
        Ensure.that(CollectionUtils.isNotEmpty(planList)).isFalse("210000012");
        Ensure.that(secondReward != null && secondReward.compareTo(BigDecimal.ZERO) >= 0 ).isFalse("210000013");
    }

    //验证邀请参数
    public void checkInvitationParams() {
        Ensure.that(oneInviteReward != null
                && oneInviteReward.compareTo(BigDecimal.ZERO) >= 0
        ).isFalse("210000014");
        Ensure.that(twoInviteReward != null
                && twoInviteReward.compareTo(BigDecimal.ZERO) >= 0
        ).isFalse("210000015");
    }

    //验证 启动 禁用参数
    public void checkStatusParams() {
        Ensure.that(id == null).isTrue("00000002");
        Ensure.that(isEnable == null || isEnable < 0 || isEnable > 1).isTrue("00000002");
    }

    public void setParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("name", name);
        querySubmit.put("channelId", channelId);
    }

    public void setChannelSettleParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.getPageNum(), this.getPageSize());
        querySubmit.put("name", this.name);

    }
}
