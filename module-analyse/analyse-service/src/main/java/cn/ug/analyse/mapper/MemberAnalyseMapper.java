package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.ChannelMemberBean;
import cn.ug.analyse.bean.request.DormancyMemberParamBean;
import cn.ug.analyse.bean.request.MemberAnalyseParamBean;
import cn.ug.analyse.bean.request.MemberAngParamBean;
import cn.ug.analyse.bean.request.MemberPortrayParamBean;
import cn.ug.analyse.bean.response.*;
import cn.ug.analyse.mapper.entity.MemberAnalyse;
import cn.ug.mapper.BaseMapper;
import cn.ug.util.BigDecimalUtil;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Component
public interface MemberAnalyseMapper extends BaseMapper<MemberAnalyse>{

    List<String> findChannelIdList(MemberAnalyseParamBean entity);

    List<MemberProtrayBean> queryForMemberGram(MemberPortrayParamBean paramBean);
    int getMemberNum(MemberPortrayParamBean paramBean);

    List<MemberAnalyseBean> findList(@Param("channelIds")List<String> channelIds,
                                     @Param("startTime") String startTime,
                                     @Param("endTime") String endTime);

    List<ChannelAnalyseBean> findMemberAnalyseTotal(MemberAnalyseParamBean memberAnalyseParamBean);

    Integer findNoBindingBankCardTotal(DormancyMemberParamBean paramBean);

    Integer findNoTradeTotal(DormancyMemberParamBean paramBean);

    List<BaseBean> findMemberAccountTotal(MemberPortrayParamBean paramBean);

    List<BaseBean> findMemberTotal(MemberPortrayParamBean paramBean);

    List<BaseBean> findGramTotal(@Param("startAge")Integer startAge,
                                 @Param("endAge")Integer endAge,
                                 @Param("type")Integer type,
                                 @Param("keywords")String keywords,
                                 @Param("productType")String productType,
                                 @Param("channelId")String channelId);

    List<BaseBean> findAverageAmount(MemberPortrayParamBean paramBean);

    List<BaseBean> findMemberNumber(MemberPortrayParamBean paramBean);

    /**
     * 查询所有年龄
     * @return
     */
    List<MemberAngBean> findMemberAgeList(MemberAngParamBean memberAngParamBean);

    Integer findRegisterMember(@Param("channelId")String channelId);

    /**
     * 根据终端获取会员数量
     * @param source
     * @return
     */
    Integer findTerminalMember(@Param("source") Integer source);

    Integer findMemberAnalyseCount(Map<String,Object> param);

    List<ChannelMemberBean> queryForList(Map<String, Object> params);
    int queryForCount(Map<String, Object> params);

    /**
     * 查询用户邀请比例
     * @return
     */
    List<MemberFriendBean> queryFriendList(MemberAngParamBean memberAngParamBean);

    List<String> queryMemberUserByLeasebackDay(@Param("leasebackDay") Integer leasebackDay,@Param("character")Integer character);

    /**
     * 查询当日交易用户数量-性别-日期
     * @return
     */
    List<MemberTraderBean> queryMemberIdNumTodayTraderList(MemberAngParamBean memberAngParamBean);
}
