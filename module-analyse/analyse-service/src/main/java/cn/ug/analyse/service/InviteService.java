package cn.ug.analyse.service;

import cn.ug.analyse.bean.InvitationDetailBean;
import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.web.submit.InviteSubmit;
import cn.ug.bean.base.SerializeObject;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/08/07.
 */
public interface InviteService {

    /**
     * 批量添加用户绑定关系
     *
     * @param entity
     * @return
     */
    int insert(InviteEntity entity);

    /**
     * 查询渠道成员下的所有一二级用户
     *
     * @param staffId
     * @return
     */
    List<InviteEntity> findAllUserListByStaffId(Long staffId);

//    /**
//     * 成员下一级用户数量
//     *
//     * @param staffId
//     * @param date
//     * @return
//     */
//    int findYesterdayFirstBindCardNumByStaffId(Long staffId, LocalDate date);
//
//    /**
//     * 成员下二级用户数量
//     *
//     * @param staffId
//     * @param date
//     * @return
//     */
//    int findYesterdaySecondBindCardNumByStaffId(Long staffId, LocalDate date);



    /**
     * 渠道下绑卡详细
     *
     * @param submit
     * @return
     */
    SerializeObject inviteBindCardList(InviteSubmit submit);

    /**
     * 导出渠道下绑卡详细
     *
     * @param submit
     * @return
     */
    List<InviteEntity> inviteBindCardExport(InviteSubmit submit);

    List<InvitationDetailBean> queryForDetailList(int staffId, String month, int inviteType, int offset, int size);
    int queryForDetailCount(int staffId, String month, int inviteType);
}
