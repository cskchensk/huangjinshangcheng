package cn.ug.analyse.service;

import cn.ug.analyse.bean.ChannelRewardsBean;
import cn.ug.analyse.bean.ChannelSettleBean;
import cn.ug.analyse.bean.SettleBean;
import cn.ug.analyse.mapper.entity.ChannelPeriodEntity;
import cn.ug.analyse.web.submit.ChannelSubmit;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.analyse.web.submit.SettleSubmit;
import cn.ug.bean.base.SerializeObject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 结算周期
 *
 * @author zhaohg
 * @date 2018/08/11.
 */
public interface SettleService {

    SerializeObject findList(OtherSubmit submit);

    List<SettleBean> export(OtherSubmit submit);

    /**
     * 添加结算记录
     *
     * @param submit
     * @return
     */
    SerializeObject insert(SettleSubmit submit);

    /**
     * 渠道结算list
     * @param submit
     * @return
     */
    SerializeObject findChannelSettleList(ChannelSubmit submit);

    SerializeObject findChannelSettleTotal(ChannelSubmit submit);

    List<ChannelSettleBean> exportChannelSettleList(ChannelSubmit submit);

    /**
     * 渠道周期奖励记录
     * @param submit
     * @return
     */
    SerializeObject findChannelPeriodList(OtherSubmit submit);

    /**
     * 获取渠道周期奖励记录
     * @param channelId
     * @param startDate
     * @param endDate
     * @return
     */
    ChannelPeriodEntity getChannelPeriodByDate(Long channelId, Date startDate, Date endDate);

    /**
     * 添加渠道周期奖励记录
     * @param period
     * @return
     */
    int addChannelPeriod(ChannelPeriodEntity period);

    BigDecimal sumOfAmount(int channelId);
    List<ChannelRewardsBean> queryForList(int channelId, int offset, int size);
    ChannelRewardsBean selectRewardsDetail(int channelId, String month);
    int queryForCount(int channelId);
}
