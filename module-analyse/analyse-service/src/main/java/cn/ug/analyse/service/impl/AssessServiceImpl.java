package cn.ug.analyse.service.impl;

import cn.ug.analyse.bean.*;
import cn.ug.analyse.mapper.AssessMapper;
import cn.ug.analyse.mapper.ChannelMapper;
import cn.ug.analyse.mapper.ChannelStaffMapper;
import cn.ug.analyse.mapper.entity.AssessEntity;
import cn.ug.analyse.mapper.entity.ChannelEntity;
import cn.ug.analyse.mapper.entity.ChannelStaffEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.analyse.service.AssessService;
import cn.ug.analyse.web.submit.ChannelStaffSubmit;
import cn.ug.analyse.web.submit.ChannelSubmit;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.BigDecimalUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 统计渠道及成员业绩
 *
 * @author zhaohg
 * @date 2018/08/07.
 */
@Service
public class AssessServiceImpl extends BaseServiceImpl implements AssessService {

    @Resource
    private AssessMapper assessMapper;
    @Autowired
    private ChannelMapper channelMapper;
    @Autowired
    private ChannelStaffMapper channelStaffMapper;

    //添加渠道成员的业绩 tb_channel_assess
    //判断昨天数据有没有添加
    @Override
    public int insert(AssessEntity assess) {
        AssessEntity entity = assessMapper.findAssessByStaffIdAndDate(assess.getStaffId(), assess.getTradeDate());
        if (entity == null) {
            return assessMapper.insert(assess);
        } else {
            assess.setId(entity.getId());
            return assessMapper.update(assess);
        }
    }

    @Override
    public SerializeObject findChannelAssessList(ChannelSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = assessMapper.channelAssessCount(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<AssessBean> list = new ArrayList<>(count);
        if (count > 0) {
            list = assessMapper.findChannelAssessList(querySubmit);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public SerializeObject findChannelAssessTotal(ChannelSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);


        AssessBean bean = assessMapper.findChannelAssessTotal(querySubmit);

        Map<String,Object> map = getMapTotal(bean);


        return new SerializeObject<>(ResultType.NORMAL, "00000001", map);
    }

    private Map<String, Object> getMapTotal(AssessBean bean) {
        Map<String, Object> map = new HashMap<>();
        if(bean != null) {
            map.put("firstBindCardNum", bean.getFirstBindCardNum());
            map.put("firstBindCardAmount", bean.getFirstBindCardAmount());
            map.put("secondBindCardNum", bean.getSecondBindCardNum());
            map.put("secondBindCardAmount", bean.getSecondBindCardAmount());
            map.put("firstTradeNum", bean.getFirstTradeNum());
            map.put("firstAnnual", bean.getFirstAnnual());
            map.put("secondTradeNum", bean.getSecondTradeNum());
            map.put("secondAnnual", bean.getSecondAnnual());
        }
        return map;
    }

    @Override
    public List<AssessBean> exportChannelAssess(ChannelSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);
        querySubmit.setLimit(null, null);

        int count = assessMapper.channelAssessCount(querySubmit);
        if (count > 0) {
            return assessMapper.findChannelAssessList(querySubmit);
        }
        return null;
    }

    /**
     * 渠道成员业绩(人为视角)
     *
     * @param submit
     * @return
     */
    @Override
    public SerializeObject findStaffAssessList(ChannelStaffSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = assessMapper.staffAssessCount(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<AssessBean> list = new ArrayList<>(count);
        if (count > 0) {
            list = assessMapper.findStaffAssessList(querySubmit);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    /**
     * 渠道成员业绩 by staffId(人为视角)
     *
     * @param submit
     * @return
     */
    @Override
    public SerializeObject findStaffByIdAssessList(ChannelStaffSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = assessMapper.findStaffByIdAssessCount(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<AssessBean> list = new ArrayList<>(count);
        if (count > 0) {
            list = assessMapper.findStaffByIdAssessList(querySubmit);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public List<AssessBean> exportStaffByIdAssess(ChannelStaffSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);
        querySubmit.setLimit(null, null);

        int count = assessMapper.findStaffByIdAssessCount(querySubmit);
        if (count > 0) {
            return assessMapper.findStaffByIdAssessList(querySubmit);
        }
        return null;
    }

    @Override
    public SerializeObject findStaffAssessTotal(ChannelSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        AssessBean bean = assessMapper.findStaffAssessTotal(querySubmit);
        Map<String, Object> map = getMapTotal(bean);

        return new SerializeObject<>(ResultType.NORMAL, "00000001", map);
    }


    @Override
    public List<AssessBean> exportStaffAssess(ChannelStaffSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);
        querySubmit.setLimit(null, null);

        int count = assessMapper.staffAssessCount(querySubmit);
        if (count > 0) {
            return assessMapper.findStaffAssessList(querySubmit);
        }
        return null;
    }

    @Override
    public SerializeObject findTimeAssessList(OtherSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setTimeAssessParams(querySubmit);

        int count = assessMapper.timeAssessCount(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<AssessBean> list = new ArrayList<>(count);
        if (count > 0) {
            list = assessMapper.findTimeAssessList(querySubmit);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public SerializeObject findTimeAssessTotal(OtherSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setTimeAssessParams(querySubmit);

        AssessBean bean = assessMapper.findTimeAssessTotal(querySubmit);
        Map<String, Object> map = getMapTotal(bean);

        return new SerializeObject<>(ResultType.NORMAL, "00000001", map);
    }

    @Override
    public List<AssessBean> exportTimeAssess(OtherSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setTimeAssessParams(querySubmit);
        querySubmit.setLimit(null, null);

        int count = assessMapper.timeAssessCount(querySubmit);
        if (count > 0) {
            return assessMapper.findTimeAssessList(querySubmit);
        }
        return null;
    }

    @Override
    public AssessEntity findChannelTotalAccess(Long channelId, Date startDate, Date endDate) {
        return assessMapper.findChannelTotalAccess(channelId, startDate, endDate);
    }

    @Override
    public StaffRewardsBean getRewardsOfStaff(int staffId) {
        if (staffId < 1) {
            return null;
        }
        long id = staffId;
        ChannelStaffEntity staffEntity = channelStaffMapper.findById(id);
        if (staffEntity != null && staffEntity.getChannelId() != null && staffEntity.getChannelId() > 0) {
            ChannelEntity channelEntity = channelMapper.findById(staffEntity.getChannelId());
            if (channelEntity == null || StringUtils.isBlank(channelEntity.getPlanStep())) {
                return null;
            }
            String jsonStr = channelEntity.getPlanStep();
            List<RewardPlanBean> planList = JSONObject.parseArray(jsonStr, RewardPlanBean.class);

            List<StaffRewardsBean> rewardsBeans = assessMapper.queryForList(staffId);
            if (rewardsBeans == null && rewardsBeans.size() == 0) {
                return null;
            }
            for (StaffRewardsBean rewardsBean : rewardsBeans) {
                BigDecimal firstTradeNum = rewardsBean.getFirstRewardsOfTransaction();
                BigDecimal secondTradeNum = rewardsBean.getSecondRewardsOfTransaction();
                BigDecimal firstTradeAmount = BigDecimal.ZERO;
                for (RewardPlanBean reward : planList) {
                    BigDecimal amount = firstTradeNum;
                    BigDecimal tradeLower = BigDecimal.valueOf(reward.getTradeLower() == null ? 0 : reward.getTradeLower() * 10000);
                    BigDecimal tradeUpper = BigDecimal.valueOf(reward.getTradeUpper() == null ? 0 : reward.getTradeUpper() * 10000);
                    if (firstTradeNum.compareTo(tradeUpper) > 0) {
                        amount = tradeUpper;
                    }
                    if (amount.compareTo(tradeLower) >= 0) {
                        BigDecimal rew = reward.getReward() == null ? BigDecimal.ZERO : reward.getReward().divide(BigDecimal.valueOf(100));
                        BigDecimal lower = amount.subtract(tradeLower);
                        firstTradeAmount = firstTradeAmount.add(lower.multiply(rew));
                    }
                }
                BigDecimal secondTradeAmount = BigDecimal.ZERO;
                if (channelEntity.getSecondReward() != null) {
                    secondTradeAmount = secondTradeNum.multiply(channelEntity.getSecondReward().divide(BigDecimal.valueOf(100)))
                            .setScale(2, BigDecimal.ROUND_HALF_UP);
                }
                rewardsBean.setFirstRewardsOfTransaction(firstTradeAmount);
                rewardsBean.setSecondRewardsOfTransaction(secondTradeAmount);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            String lastMonth = format.format(calendar.getTime());
            StaffRewardsBean staffRewardsBean = new StaffRewardsBean();
            staffRewardsBean.setTotalRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setTotalAmount(BigDecimal.ZERO);
            staffRewardsBean.setTotalRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setSecondRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setSecondRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setFirstRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setFirstRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setStaffName(staffEntity.getRealName());
            BigDecimal lastMonthAmount = BigDecimal.ZERO;
            for (StaffRewardsBean rewardsBean : rewardsBeans) {
                if (StringUtils.equals(lastMonth, rewardsBean.getRewardsMonth())) {
                    lastMonthAmount = lastMonthAmount.add(rewardsBean.getFirstRewardsOfInvitation());
                    lastMonthAmount = lastMonthAmount.add(rewardsBean.getFirstRewardsOfTransaction());
                    lastMonthAmount = lastMonthAmount.add(rewardsBean.getSecondRewardsOfInvitation());
                    lastMonthAmount = lastMonthAmount.add(rewardsBean.getSecondRewardsOfTransaction());
                }
                staffRewardsBean.setTotalAmount(staffRewardsBean.getTotalAmount().add(rewardsBean.getSecondRewardsOfTransaction()));
                staffRewardsBean.setTotalAmount(staffRewardsBean.getTotalAmount().add(rewardsBean.getSecondRewardsOfInvitation()));
                staffRewardsBean.setTotalAmount(staffRewardsBean.getTotalAmount().add(rewardsBean.getFirstRewardsOfTransaction()));
                staffRewardsBean.setTotalAmount(staffRewardsBean.getTotalAmount().add(rewardsBean.getFirstRewardsOfInvitation()));

                staffRewardsBean.setFirstRewardsOfTransaction(staffRewardsBean.getFirstRewardsOfTransaction().add(rewardsBean.getFirstRewardsOfTransaction()));
                staffRewardsBean.setFirstRewardsOfInvitation(staffRewardsBean.getFirstRewardsOfInvitation().add(rewardsBean.getFirstRewardsOfInvitation()));
                staffRewardsBean.setSecondRewardsOfInvitation(staffRewardsBean.getSecondRewardsOfInvitation().add(rewardsBean.getSecondRewardsOfInvitation()));
                staffRewardsBean.setSecondRewardsOfTransaction(staffRewardsBean.getSecondRewardsOfTransaction().add(rewardsBean.getSecondRewardsOfTransaction()));

                staffRewardsBean.setTotalRewardsOfTransaction(staffRewardsBean.getTotalRewardsOfTransaction().add(rewardsBean.getFirstRewardsOfTransaction()));
                staffRewardsBean.setTotalRewardsOfTransaction(staffRewardsBean.getTotalRewardsOfTransaction().add(rewardsBean.getSecondRewardsOfTransaction()));

                staffRewardsBean.setTotalRewardsOfInvitation(staffRewardsBean.getTotalRewardsOfInvitation().add(rewardsBean.getFirstRewardsOfInvitation()));
                staffRewardsBean.setTotalRewardsOfInvitation(staffRewardsBean.getTotalRewardsOfInvitation().add(rewardsBean.getSecondRewardsOfInvitation()));
            }
            staffRewardsBean.setLastMonthAmount(BigDecimalUtil.to2Point(lastMonthAmount));
            staffRewardsBean.setFirstRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getFirstRewardsOfInvitation()));
            staffRewardsBean.setFirstRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getFirstRewardsOfTransaction()));
            staffRewardsBean.setSecondRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getSecondRewardsOfTransaction()));
            staffRewardsBean.setSecondRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getSecondRewardsOfInvitation()));
            staffRewardsBean.setTotalRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getTotalRewardsOfInvitation()));
            staffRewardsBean.setTotalAmount(BigDecimalUtil.to2Point(staffRewardsBean.getTotalAmount()));
            staffRewardsBean.setTotalRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getTotalRewardsOfTransaction()));

            return staffRewardsBean;
        }
        return null;
    }

    @Override
    public StaffRewardsBean getRewardsOfStaff(int staffId, String month) {
        if (staffId < 1) {
            return null;
        }
        long id = staffId;
        ChannelStaffEntity staffEntity = channelStaffMapper.findById(id);
        if (staffEntity != null && staffEntity.getChannelId() != null && staffEntity.getChannelId() > 0) {
            ChannelEntity channelEntity = channelMapper.findById(staffEntity.getChannelId());
            if (channelEntity == null || StringUtils.isBlank(channelEntity.getPlanStep())) {
                return null;
            }
            String jsonStr = channelEntity.getPlanStep();
            List<RewardPlanBean> planList = JSONObject.parseArray(jsonStr, RewardPlanBean.class);

            List<StaffRewardsBean> rewardsBeans = assessMapper.queryForTradingList(staffId);
            if (rewardsBeans == null && rewardsBeans.size() == 0) {
                return null;
            }
            if (StringUtils.isBlank(month)) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
                month = format.format(calendar.getTime());
            }
            BigDecimal sumOfAmount = BigDecimal.ZERO;
            for (StaffRewardsBean rewardsBean : rewardsBeans) {
                BigDecimal firstTradeNum = rewardsBean.getFirstRewardsOfTransaction();
                BigDecimal secondTradeNum = rewardsBean.getSecondRewardsOfTransaction();
                if (StringUtils.equals(month, rewardsBean.getRewardsMonth())) {
                    firstTradeNum = firstTradeNum == null ? BigDecimal.ZERO : firstTradeNum;
                    secondTradeNum = secondTradeNum == null ? BigDecimal.ZERO : secondTradeNum;
                    sumOfAmount = firstTradeNum.add(secondTradeNum);
                }
                BigDecimal firstTradeAmount = BigDecimal.ZERO;
                for (RewardPlanBean reward : planList) {
                    BigDecimal amount = firstTradeNum;
                    BigDecimal tradeLower = BigDecimal.valueOf(reward.getTradeLower() == null ? 0 : reward.getTradeLower() * 10000);
                    BigDecimal tradeUpper = BigDecimal.valueOf(reward.getTradeUpper() == null ? 0 : reward.getTradeUpper() * 10000);
                    if (firstTradeNum.compareTo(tradeUpper) > 0) {
                        amount = tradeUpper;
                    }
                    if (amount.compareTo(tradeLower) >= 0) {
                        BigDecimal rew = reward.getReward() == null ? BigDecimal.ZERO : reward.getReward().divide(BigDecimal.valueOf(100));
                        BigDecimal lower = amount.subtract(tradeLower);
                        firstTradeAmount = firstTradeAmount.add(lower.multiply(rew));
                    }
                }
                BigDecimal secondTradeAmount = BigDecimal.ZERO;
                if (channelEntity.getSecondReward() != null) {
                    secondTradeAmount = secondTradeNum.multiply(channelEntity.getSecondReward().divide(BigDecimal.valueOf(100)))
                            .setScale(2, BigDecimal.ROUND_HALF_UP);
                }
                rewardsBean.setFirstRewardsOfTransaction(firstTradeAmount);
                rewardsBean.setSecondRewardsOfTransaction(secondTradeAmount);
            }

            StaffRewardsBean staffRewardsBean = new StaffRewardsBean();
            staffRewardsBean.setTotalRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setTotalAmount(BigDecimal.ZERO);
            staffRewardsBean.setTotalRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setSecondRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setSecondRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setFirstRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setFirstRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setStaffName(staffEntity.getRealName());
            BigDecimal lastMonthAmount = BigDecimal.ZERO;
            for (StaffRewardsBean rewardsBean : rewardsBeans) {
                if (StringUtils.equals(month, rewardsBean.getRewardsMonth())) {
                    lastMonthAmount = lastMonthAmount.add(rewardsBean.getFirstRewardsOfTransaction());
                    lastMonthAmount = lastMonthAmount.add(rewardsBean.getSecondRewardsOfTransaction());

                    staffRewardsBean.setFirstRewardsOfTransaction(rewardsBean.getFirstRewardsOfTransaction());
                    staffRewardsBean.setFirstRewardsOfInvitation(rewardsBean.getFirstRewardsOfInvitation());
                    staffRewardsBean.setSecondRewardsOfInvitation(rewardsBean.getSecondRewardsOfInvitation());
                    staffRewardsBean.setSecondRewardsOfTransaction(rewardsBean.getSecondRewardsOfTransaction());
                }
                staffRewardsBean.setTotalAmount(sumOfAmount);

                staffRewardsBean.setTotalRewardsOfTransaction(staffRewardsBean.getTotalRewardsOfTransaction().add(rewardsBean.getFirstRewardsOfTransaction()));
                staffRewardsBean.setTotalRewardsOfInvitation(staffRewardsBean.getTotalRewardsOfInvitation().add(rewardsBean.getSecondRewardsOfTransaction()));
            }
            staffRewardsBean.setLastMonthAmount(BigDecimalUtil.to2Point(lastMonthAmount));
            staffRewardsBean.setFirstRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getFirstRewardsOfInvitation()));
            staffRewardsBean.setFirstRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getFirstRewardsOfTransaction()));
            staffRewardsBean.setSecondRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getSecondRewardsOfTransaction()));
            staffRewardsBean.setSecondRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getSecondRewardsOfInvitation()));
            staffRewardsBean.setTotalRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getTotalRewardsOfInvitation()));
            staffRewardsBean.setTotalAmount(BigDecimalUtil.to2Point(new BigDecimal(assessMapper.selectTotalAnnual(staffId, month))));
            staffRewardsBean.setTotalRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getTotalRewardsOfTransaction()));

            return staffRewardsBean;
        }
        return null;
    }

    @Override
    public StaffRewardsBean getInvitationRewardsOfStaff(int staffId, String month) {
        if (staffId < 1) {
            return null;
        }
        long id = staffId;
        ChannelStaffEntity staffEntity = channelStaffMapper.findById(id);
        if (staffEntity != null && staffEntity.getChannelId() != null && staffEntity.getChannelId() > 0) {
            List<StaffRewardsBean> rewardsBeans = assessMapper.queryForTradingList(staffId);
            if (rewardsBeans == null && rewardsBeans.size() == 0) {
                return null;
            }
            if (StringUtils.isBlank(month)) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MONTH, -1);
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
                month = format.format(calendar.getTime());
            }
            BigDecimal sumOfAmount = BigDecimal.ZERO;
            StaffRewardsBean staffRewardsBean = new StaffRewardsBean();
            staffRewardsBean.setTotalRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setTotalAmount(BigDecimal.ZERO);
            staffRewardsBean.setTotalRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setSecondRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setSecondRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setFirstRewardsOfTransaction(BigDecimal.ZERO);
            staffRewardsBean.setFirstRewardsOfInvitation(BigDecimal.ZERO);
            staffRewardsBean.setStaffName(staffEntity.getRealName());
            BigDecimal lastMonthAmount = BigDecimal.ZERO;
            for (StaffRewardsBean rewardsBean : rewardsBeans) {
                if (StringUtils.equals(month, rewardsBean.getRewardsMonth())) {
                    staffRewardsBean.setTotalAmount(rewardsBean.getFirstRewardsOfInvitation().add(rewardsBean.getSecondRewardsOfInvitation()));
                    staffRewardsBean.setFirstRewardsOfTransaction(rewardsBean.getFirstRewardsOfTransaction());
                    staffRewardsBean.setFirstRewardsOfInvitation(rewardsBean.getFirstRewardsOfInvitation());
                    staffRewardsBean.setSecondRewardsOfInvitation(rewardsBean.getSecondRewardsOfInvitation());
                    staffRewardsBean.setSecondRewardsOfTransaction(rewardsBean.getSecondRewardsOfTransaction());
                }
                staffRewardsBean.setTotalRewardsOfTransaction(staffRewardsBean.getTotalRewardsOfTransaction().add(rewardsBean.getFirstRewardsOfInvitation()));
                staffRewardsBean.setTotalRewardsOfInvitation(staffRewardsBean.getTotalRewardsOfInvitation().add(rewardsBean.getSecondRewardsOfInvitation()));
            }
            staffRewardsBean.setLastMonthAmount(BigDecimalUtil.to2Point(lastMonthAmount));
            staffRewardsBean.setFirstRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getFirstRewardsOfInvitation()));
            staffRewardsBean.setFirstRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getFirstRewardsOfTransaction()));
            staffRewardsBean.setSecondRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getSecondRewardsOfTransaction()));
            staffRewardsBean.setSecondRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getSecondRewardsOfInvitation()));
            staffRewardsBean.setTotalRewardsOfInvitation(BigDecimalUtil.to2Point(staffRewardsBean.getTotalRewardsOfInvitation()));
            staffRewardsBean.setTotalAmount(BigDecimalUtil.to2Point(staffRewardsBean.getTotalAmount()));
            staffRewardsBean.setTotalRewardsOfTransaction(BigDecimalUtil.to2Point(staffRewardsBean.getTotalRewardsOfTransaction()));

            return staffRewardsBean;
        }
        return null;
    }



    @Override
    public List<ChannelStaffRewardsBean> queryForStaffRewardsList(int channelId, String month, int offset, int size) {
        if (channelId > 0 && StringUtils.isNotBlank(month)) {
            long id = channelId;
            ChannelEntity channelEntity = channelMapper.findById(id);
            if (channelEntity == null || StringUtils.isBlank(channelEntity.getPlanStep())) {
                return null;
            }
            String jsonStr = channelEntity.getPlanStep();
            List<RewardPlanBean> planList = JSONObject.parseArray(jsonStr, RewardPlanBean.class);
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("channelId", channelId);
            params.put("month", month);
            params.put("offset", offset);
            params.put("size", size);
            List<ChannelStaffRewardsBean> rewardsBeans = assessMapper.queryForStaffRewardsList(params);
            if (rewardsBeans == null && rewardsBeans.size() == 0) {
                return null;
            }
            BigDecimal sumOfAmount = BigDecimal.ZERO;
            for (ChannelStaffRewardsBean rewardsBean : rewardsBeans) {
                BigDecimal firstTradeNum = rewardsBean.getFirstTransactionAmount();
                BigDecimal secondTradeNum = rewardsBean.getSecondTransactionAmount();
                BigDecimal firstTradeAmount = BigDecimal.ZERO;
                for (RewardPlanBean reward : planList) {
                    BigDecimal amount = firstTradeNum;
                    BigDecimal tradeLower = BigDecimal.valueOf(reward.getTradeLower() == null ? 0 : reward.getTradeLower() * 10000);
                    BigDecimal tradeUpper = BigDecimal.valueOf(reward.getTradeUpper() == null ? 0 : reward.getTradeUpper() * 10000);
                    if (firstTradeNum.compareTo(tradeUpper) > 0) {
                        amount = tradeUpper;
                    }
                    if (amount.compareTo(tradeLower) >= 0) {
                        BigDecimal rew = reward.getReward() == null ? BigDecimal.ZERO : reward.getReward().divide(BigDecimal.valueOf(100));
                        BigDecimal lower = amount.subtract(tradeLower);
                        firstTradeAmount = firstTradeAmount.add(lower.multiply(rew));
                    }
                }
                BigDecimal secondTradeAmount = BigDecimal.ZERO;
                if (channelEntity.getSecondReward() != null) {
                    secondTradeAmount = secondTradeNum.multiply(channelEntity.getSecondReward().divide(BigDecimal.valueOf(100)))
                            .setScale(2, BigDecimal.ROUND_HALF_UP);
                }
                rewardsBean.setFirstTransactionAmount(firstTradeAmount);
                rewardsBean.setSecondTransactionAmount(secondTradeAmount);
                rewardsBean.setSumOfTransaction(firstTradeAmount.add(secondTradeAmount));
                rewardsBean.setTotalAmount(rewardsBean.getSumOfInvitation().add(rewardsBean.getSumOfTransaction()));
            }
            return rewardsBeans;
        }
        return null;
    }

    @Override
    public int queryForStaffRewardsCount(int channelId, String month) {
        if (channelId > 0 && StringUtils.isNotBlank(month)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("channelId", channelId);
            params.put("month", month);
            return assessMapper.queryForStaffRewardsCount(params);
        }
        return 0;
    }
}
