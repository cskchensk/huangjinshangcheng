package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.response.BaseBean;
import cn.ug.analyse.bean.response.BaseOperationBean;
import cn.ug.analyse.mapper.entity.ChannelEntity;
import cn.ug.analyse.mapper.entity.ChannelStatisticsBean;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.analyse.mapper.entity.UvEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 渠道管理
 * @author zhaohg
 * @date 2018/08/06.
 */
@Mapper
public interface ChannelMapper {

    /**
     * 添加渠道
     * @param entity
     * @return
     */
    int insert(ChannelEntity entity);

    /**
     * 更新渠道
     * @param entity
     * @return
     */
    int update(ChannelEntity entity);

    /**
     * 更新邀请规则
     * @param entity
     * @return
     */
    int updateInvitation(ChannelEntity entity);

    /**
     * 更新邀请规则
     * @param entity
     * @return
     */
    int updateRewardPlan(ChannelEntity entity);

    /**
     * 更新渠道启用禁用状态
     * @param entity
     * @return
     */
    int updateChannelStatus(ChannelEntity entity);

    /**
     * 最大渠道编号
     * @return
     */
    int maxChannelNumber();

    /**
     * 查询渠道
     * @param id
     * @return
     */
    ChannelEntity findById(Long id);

    /**
     * 渠道行数
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);

    /**
     * 渠道列表
     * @param submit
     * @return
     */
    List<ChannelEntity> findList(QuerySubmit submit);


    /**
     * 添加奖励
     * @param channelId
     * @param amount
     * @return
     */
    int addChannelReward(@Param("channelId") Long channelId, @Param("amount") BigDecimal amount);

    /**
     * 结算奖励
     * @param channelId
     * @param amount
     * @return
     */
    int settleChannelReward(@Param("channelId") Long channelId, @Param("amount") BigDecimal amount);

    /**
     * 渠道结算合计
     * @param querySubmit
     * @return
     */
    ChannelEntity findTotal(QuerySubmit querySubmit);

    int updateStaffCount(@Param("channelId") Long channelId);

    /**
     * 根据渠道用户名称查询
     * @param channelName
     * @return
     */
    ChannelEntity findByName(@Param("channelName")String channelName);

    /**
     * 重置密码
     * @param id
     * @param password
     * @return
     */
    int resetPassword(@Param("id") Long id, @Param("password") String password);

    /**
     * 查询渠道
     * @return
     */
    List<ChannelStatisticsBean> findStatisticsList(@Param("channelIds") String channelIds);

    /**
     * 渠道统计
     * @param channelId
     * @return
     */
    List<BaseOperationBean> statistics(@Param("channelId") String channelId,@Param("type") Integer type);

    /**
     * 根据渠道id查询uv统计
     * @param channelId
     * @return
     */
    List<UvEntity> queryUvBychannelId(@Param("channelId") String channelId,@Param("type") Integer type);

    /**
     *查询活动关联的渠道信息
     */
    String queryActivityChannelId();

    /**
     * 查询参加活动人数根据渠道id
     * @return
     */
    Integer queryJoinNumByChannelId(@Param("channelId") String channelId);

    /**
     * 查询参加活动人数根据渠道id  按照日期分组天
     * @return
     */
    List<BaseBean> queryEveryDayJoinNumByChannelId(@Param("channelId") String channelId);
}
