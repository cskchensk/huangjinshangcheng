package cn.ug.analyse.service;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.bean.base.DataTable;

import java.math.BigDecimal;
import java.util.List;

public interface MemberWithdrawService {

    /**
     * 按照日、周、月统计的列表
     * @param withdeawParamBean
     * @return
     */
    DataTable<BaseCountBean> findWithdrawTotalList(WithdeawParamBean withdeawParamBean);

    /**
     * 每日走势图列表
     * @param withdeawParamBean
     * @return
     */
    List<BaseCountBean> findEverydayList(WithdeawParamBean withdeawParamBean);

    /**
     * 提现总额
     * @param withdeawParamBean
     * @return
     */
    BigDecimal findTotal(WithdeawParamBean withdeawParamBean);
}
