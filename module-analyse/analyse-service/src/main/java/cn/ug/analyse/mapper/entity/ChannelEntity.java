package cn.ug.analyse.mapper.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public class ChannelEntity {
    /**
     * id
     */
    private Long       id;
    /**
     * 渠道名称
     */
    private String     name;
    /**
     * 渠道编号
     */
    private String     number;
    /**
     * 奖励方案 1:阶梯交易额 2:交易周期 3:交易频数
     */
    private Integer    planType;
    /**
     * 周期
     */
    private Integer    period;
    /**
     * 二级奖励比例
     */
    private BigDecimal secondReward;
    /**
     * 接力方案json
     */
    private String     planStep;

    /**
     * 一级邀请奖励
     */
    private BigDecimal oneInviteReward;
    /**
     * 二级邀请奖励
     */
    private BigDecimal twoInviteReward;
    /**
     * 有效开始时间
     */
    private Date       enableDate;
    /**
     * 无效开始时间
     */
    private Date       expireDate;
    /**
     * 启用禁用 0:禁用 1:启用
     */
    private Integer    isEnable;
    /**
     * 修改日期
     */
    private Date       modifyTime;
    /**
     * 创建日期
     */
    private Date       addTime;
    /**
     * 渠道关联人id
     */
    private Long       relationId;


    private BigDecimal totalReward;//总奖励金额
    private BigDecimal settleReward;//已结算奖励金额
    private BigDecimal waitReward;//待结算奖励

    private Integer staffCount;

    /**
     * 登陆密码
     */
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPlanType() {
        return planType;
    }

    public void setPlanType(Integer planType) {
        this.planType = planType;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public BigDecimal getSecondReward() {
        return secondReward;
    }

    public void setSecondReward(BigDecimal secondReward) {
        this.secondReward = secondReward;
    }

    public String getPlanStep() {
        return planStep;
    }

    public void setPlanStep(String planStep) {
        this.planStep = planStep;
    }

    public BigDecimal getOneInviteReward() {
        return oneInviteReward;
    }

    public void setOneInviteReward(BigDecimal oneInviteReward) {
        this.oneInviteReward = oneInviteReward;
    }

    public BigDecimal getTwoInviteReward() {
        return twoInviteReward;
    }

    public void setTwoInviteReward(BigDecimal twoInviteReward) {
        this.twoInviteReward = twoInviteReward;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }


    public BigDecimal getTotalReward() {
        return totalReward;
    }

    public void setTotalReward(BigDecimal totalReward) {
        this.totalReward = totalReward;
    }

    public BigDecimal getSettleReward() {
        return settleReward;
    }

    public void setSettleReward(BigDecimal settleReward) {
        this.settleReward = settleReward;
    }

    public BigDecimal getWaitReward() {
        return waitReward;
    }

    public void setWaitReward(BigDecimal waitReward) {
        this.waitReward = waitReward;
    }

    public Integer getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(Integer staffCount) {
        this.staffCount = staffCount;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
