package cn.ug.analyse.mapper.entity;

import java.util.Date;

/**
 * 渠道成员
 *
 * @author zhaohg
 * @date 2018/08/06.
 */
public class ChannelStaffEntity {
    /**
     * 成员id
     */
    private Long    id;
    /**
     * 渠道id
     */
    private Long    channelId;
    /**
     * 成员编号
     */
    private String  number;
    /**
     * 成员密码
     */
    private String  password;
    /**
     * 成员姓名
     */
    private String  realName;
    /**
     * 成员手机号
     */
    private String  mobile;
    /**
     * 邀请码
     */
    private String  inviteCode;
    /**
     * 启用禁用 0:禁用 1:启用
     */
    private Integer isEnable;
    /**
     * 修改日期
     */
    private Date    modifyTime;
    /**
     * 添加日期
     */
    private Date    addTime;

    private Integer staffCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(Integer staffCount) {
        this.staffCount = staffCount;
    }
}
