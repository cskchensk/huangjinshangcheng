package cn.ug.analyse.service;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.bean.base.DataTable;

import java.math.BigDecimal;

public interface MemberBalanceService {

    /**
     * 按照日、周、月统计的列表
     * @param withdeawParamBean
     * @return
     */
    DataTable<BaseCountBean> findList(WithdeawParamBean withdeawParamBean);

    /**
     * 余额总额
     * @param withdeawParamBean
     * @return
     */
    BigDecimal findTotal(WithdeawParamBean withdeawParamBean);

    /**
     * 定时任务余额统计
     */
    void memberBalanceJob();

    void memberBalanceScript(String startTimeString,String endTimeString);
}
