package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.request.BaseParamBean;
import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.bean.response.MemberRechargeBean;
import cn.ug.analyse.service.*;
import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.ActiveMemberBean;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("export")
public class ExportController {

    @Resource
    private MemberWithdrawService memberWithdrawService;

    @Resource
    private MemberBalanceService memberBalanceService;

    @Resource
    private MemberRechargeService memberRechargeService;

    @Resource
    private MemberHoldGoldService memberHoldGoldService;

    @Resource
    private MemberAnalyseService memberAnalyseService;

    /**
     * 提现报表
     * @param withdeawParamBean
     * @return
     */
    @GetMapping(value = "exportWithdraw")
    public void exportWithdraw(HttpServletResponse response, WithdeawParamBean withdeawParamBean) {
        withdeawParamBean.setPageType(1); //不分页
        DataTable<BaseCountBean> list = memberWithdrawService.findWithdrawTotalList(withdeawParamBean);
        String[] columnNames = { "日期", "资金提现总额"};
        String [] columns = {"day","amount"};
        String fileName = "提现报表";
        ExportExcelController<BaseCountBean> export = new ExportExcelController<BaseCountBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }

    /**
     * 余额汇总报表
     * @param withdeawParamBean
     * @return
     */
    @GetMapping(value = "exportBalance")
    public void exportBalance(HttpServletResponse response, WithdeawParamBean withdeawParamBean) {
        withdeawParamBean.setPageType(1);  //不分页
        DataTable<BaseCountBean> list = memberBalanceService.findList(withdeawParamBean);
        String[] columnNames = { "日期", "用户账户余额总额（元）"};
        String [] columns = {"day","amount"};
        String fileName = "余额汇总报表";
        ExportExcelController<BaseCountBean> export = new ExportExcelController<BaseCountBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }
    /**
     * 充值提现汇总报表
     * @param baseParamBean
     * @return
     */
    @GetMapping(value = "exportRecharge")
    public void exportRecharge(HttpServletResponse response, BaseParamBean baseParamBean) {
        baseParamBean.setPageType(1);  //不分页
        DataTable<MemberRechargeBean> list = memberRechargeService.findList(baseParamBean);
        String[] columnNames = { "日期", "日新增充值金额","提现金额"};
        String [] columns = {"day","rechargeAmount","withdrawAmount"};
        String fileName = "充值提现汇总报表";
        ExportExcelController<MemberRechargeBean> export = new ExportExcelController<MemberRechargeBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }
    /**
     * 用户持有黄金量汇总报表
     * @param withdeawParamBean
     * @return
     */
    @GetMapping(value = "exportHoldGold")
    public void exportHoldGold(HttpServletResponse response,  WithdeawParamBean withdeawParamBean) {
        withdeawParamBean.setPageType(1); //不分页
        DataTable<BaseCountBean> list = memberHoldGoldService.findList(withdeawParamBean);
        String[] columnNames = { "日期", "用户持有黄金克重（克）"};
        String [] columns = {"day","amount"};
        String fileName = "用户持有黄金量汇总报表";
        ExportExcelController<BaseCountBean> export = new ExportExcelController<BaseCountBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }
    /**
     * 用户新增黄金汇总报表
     * @param withdeawParamBean
     * @return
     */
    @GetMapping(value = "exportNewHoldGold")
    public void exportNewHoldGold(HttpServletResponse response,  WithdeawParamBean withdeawParamBean) {
        withdeawParamBean.setPageType(1); //不分页
        DataTable<BaseCountBean> list = memberHoldGoldService.findNewGoldList(withdeawParamBean);
        String[] columnNames = new String[2];
        if(withdeawParamBean.getDayType() == 1){
            columnNames = new String []{ "日期", "日新增黄金克重"};
        }else if(withdeawParamBean.getDayType() == 2){
            columnNames = new String []{ "日期", "周新增黄金克重"};
        }else{
            columnNames = new String []{ "日期", "月新增黄金克重"};
        }
        String [] columns = {"day","amount"};
        String fileName = "用户新增黄金汇总报表";
        ExportExcelController<BaseCountBean> export = new ExportExcelController<BaseCountBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }
    /**
     * 活跃用户报表
     * @param financeBillParam
     * @return
     */
    @GetMapping(value = "exportBuySellActiveMember")
    public void exportBuySellActiveMember(HttpServletResponse response,  FinanceBillParam financeBillParam) {
        List<ActiveMemberBean> list = memberAnalyseService.findBuySellActiveMemberWeekList(financeBillParam);
        String[] columnNames = { "日期", "买金用户", "卖金用户"};
        String [] columns = {"day","buyNumber", "sellerNumber"};
        String fileName = "用户活跃用户分析报表";
        ExportExcelController<ActiveMemberBean> export = new ExportExcelController<ActiveMemberBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
    }

}
