package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.response.BaseCountExtendBean;
import cn.ug.analyse.bean.response.BaseOperationBean;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 15:03
 **/
@Component
public interface MemberOperationMapper extends BaseMapper {
    /**
     * 查询每天买金的人数
     * @return
     */
    List<BaseCountExtendBean> queryEverydayList(@Param("channelId")String channelId);

    /**
     *
     * @return
     */
    List<BaseOperationBean>  queryOperationList();

    /**
     * 查询黄金存量相关统计信息
     * @param paramsMap
     * @return
     */
    List<Map> queryStockList(Map paramsMap);

    /**
     *查询渠道注册绑卡余额信息
     */
    List<Map> queryAllChannelRegisterAndBdCardAndBalanceAmount();
}
