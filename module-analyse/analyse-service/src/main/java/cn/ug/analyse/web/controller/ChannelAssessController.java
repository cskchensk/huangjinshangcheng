package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.AssessBean;
import cn.ug.analyse.bean.InvestBean;
import cn.ug.analyse.service.AssessService;
import cn.ug.analyse.service.InvestService;
import cn.ug.analyse.web.submit.ChannelStaffSubmit;
import cn.ug.analyse.web.submit.ChannelSubmit;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.util.DateUtil;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 渠道及成员业绩
 *
 * @author zhaohg
 * @date 2018/08/10.
 */

@RestController
@RequestMapping("/assess")
public class ChannelAssessController {
    @Autowired
    private AssessService assessService;


    @Autowired
    private InvestService   investService;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;


    /**
     * 渠道业绩
     */
    @RequestMapping(value = "/channel/list", method = GET)
    public SerializeObject findChannelAssessList(@RequestHeader String accessToken, ChannelSubmit submit) {
        return assessService.findChannelAssessList(submit);
    }

    /**
     * 渠道业绩合计
     */
    @RequestMapping(value = "/channel/total", method = GET)
    public SerializeObject findChannelAssessTotal(@RequestHeader String accessToken, ChannelSubmit submit) {
        return assessService.findChannelAssessTotal(submit);
    }

    /**
     * 导出渠道业绩
     *
     * @param response
     * @param submit
     */
    @RequestMapping(value = "/channel/export", method = GET)
    public void exportChannelAssess(HttpServletResponse response, ChannelSubmit submit) {
        List<AssessBean> list = assessService.exportChannelAssess(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "渠道编号", "渠道名称", "奖励方案", "一级绑卡人数总和",
                    "二级绑卡人数总和", "一级年化交易总量", "二级年化交易"};
            String[] columns = {"index", "number", "name", "planName", "firstBindCardNum",
                    "secondBindCardNum", "firstAnnual", "secondAnnual"};
            String fileName = "渠道业绩";
            ExportExcelController<AssessBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapAssessData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    /**
     * 成员业绩 （人为视角）
     * AssessBean
     */
    @RequestMapping(value = "/staff/list", method = GET)
    public SerializeObject findStaffAssessList(@RequestHeader String accessToken, ChannelStaffSubmit submit) {
        if (submit.getChannelId() > 0) {
            return assessService.findStaffAssessList(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 成员业绩 合计 （人为视角）
     * @param submit
     * @return
     */
    @RequestMapping(value = "/staff/total", method = GET)
    public SerializeObject findStaffAssessTotal(@RequestHeader String accessToken, ChannelSubmit submit) {
        return assessService.findStaffAssessTotal(submit);
    }

    /**
     * 导出成员业绩 （人为视角）
     * AssessBean
     */
    @RequestMapping(value = "/staff/export", method = GET)
    public void exportStaffAssess( HttpServletResponse response, ChannelStaffSubmit submit) {
        if (submit.getChannelId() == null) {
            return;
        }
        List<AssessBean> list = assessService.exportStaffAssess(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "成员编号", "成员名称", "手机号", "一级绑卡人数",
                    "二级绑卡人数", "一级年化交易总量", "二级年化交易"};
            String[] columns = {"index", "number", "realName", "mobile", "firstBindCardNum", "secondBindCardNum",
                    "firstAnnual", "secondAnnual"};
            String fileName = "渠道成员业绩";
            ExportExcelController<AssessBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapAssessData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }
    /**
     * 成员业绩 by staffId（人为视角）
     * AssessBean
     */
    @RequestMapping(value = "/staff/detail/list", method = GET)
    public SerializeObject findStaffByIdAssessList(@RequestHeader String accessToken, ChannelStaffSubmit submit) {
        if (submit.getChannelId() > 0&& submit.getStaffId() > 0) {
            return assessService.findStaffByIdAssessList(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 导出成员业绩 by staffId（人为视角）
     * AssessBean
     */
    @RequestMapping(value = "/staff/detail/export", method = GET)
    public void exportStaffByIdAssess( HttpServletResponse response, ChannelStaffSubmit submit) {
        if (submit.getChannelId() > 0 && submit.getStaffId() == null) {
            return;
        }
        List<AssessBean> list = assessService.exportStaffByIdAssess(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "日期", "一级绑卡人数", "一级奖励", "二级绑卡人数","二级奖励", "一级年化交易总量", "二级年化交易"};
            String[] columns = {"index", "time", "firstBindCardNum", "firstBindCardAmount","secondBindCardNum",
                    "secondBindCardAmount","firstAnnual", "secondAnnual"};
            String fileName = "成员业绩";
            ExportExcelController<AssessBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapStaffAssessData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    private Collection<AssessBean> wrapStaffAssessData(List<AssessBean> list) {
        List<AssessBean> beans = new ArrayList<>();
        int index = 0;
        for (AssessBean bean : list) {
            bean.setIndex(index++);
            bean.setTime(DateUtil.format(bean.getTradeDate(), "yyyy-MM-dd"));
            beans.add(bean);
        }
        return beans;
    }

    /**
     * 导出时间业绩 （时间视角）
     * AssessBean
     */
    @RequestMapping(value = "/time/list", method = GET)
    public SerializeObject findTimeAssessList(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit.getChannelId() > 0) {
            return assessService.findTimeAssessList(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 成员业绩 合计 （时间视角）
     * @param submit
     * @return
     */
    @RequestMapping(value = "/time/total", method = GET)
    public SerializeObject findTimeAssessTotal(@RequestHeader String accessToken, OtherSubmit submit) {
        return assessService.findTimeAssessTotal(submit);
    }

    /**
     * 导出时间业绩 （时间视角）
     * AssessBean
     */
    @RequestMapping(value = "/time/export", method = GET)
    public void exportStaffAssessGroupTime(HttpServletResponse response, OtherSubmit submit) {
        if (submit.getChannelId() == null) {
            return;
        }
        List<AssessBean> list = assessService.exportTimeAssess(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "日期", "一级绑卡人数", "奖励金额（元）", "二级绑卡人数", "奖励金额（元）",
                    "一级年化交易总量", "二级年化交易总量"};
            String[] columns = {"index", "time", "firstBindCardNum", "firstBindCardAmount", "secondBindCardNum",
                    "secondBindCardAmount", "firstAnnual", "secondAnnual"};
            String fileName = "渠道成员业绩";
            ExportExcelController<AssessBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapAssessData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    private List<AssessBean> wrapAssessData(List<AssessBean> list) {
        List<AssessBean> beans = new ArrayList<>();
        int index = 0;
        for (AssessBean bean : list) {
            bean.setIndex(index++);
            bean.setPlanName("阶梯交易方案");
            bean.setTime(DateUtil.format(bean.getTradeDate(),"yyyy-MM-dd"));
            beans.add(bean);
        }
        return beans;
    }

    /**
     * 渠道下的投资人的交易记录
     */
    @RequestMapping(value = "/invest/list", method = GET)
    public SerializeObject findList(@RequestHeader String accessToken, OtherSubmit submit) {
        return investService.findList(submit);
    }

    @RequestMapping(value = "/invest/total", method = GET)
    public SerializeObject findTotal(@RequestHeader String accessToken, OtherSubmit submit) {
        return investService.findTotal(submit);
    }

    @RequestMapping(value = "/invest/export", method = GET)
    public void investBindCardExport( HttpServletResponse response, OtherSubmit submit) {
        List<InvestBean> list = investService.export(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "投资人姓名", "投资人手机号", "交易金额", "投资期限", "交易时间", "年化交易量", "邀请人",
                    "邀请人上级", "类型"};
            String[] columns = {"index", "realName", "mobile", "amount", "deadline", "time", "annual", "inviteName",
                    "superiorName", "level"};
            String fileName = "交易详细";
            ExportExcelController<InvestBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapInvestEntityData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    private List<InvestBean> wrapInvestEntityData(List<InvestBean> list) {
        List<InvestBean> beans = new ArrayList<>();
        int index = 0;
        for (InvestBean bean : list) {
            bean.setIndex(index++);
            bean.setLevel(bean.getInviteType() > 1 ?"二级":"一级");
            bean.setTime(DateUtil.format(bean.getTradeTime(), "yyyy-MM-dd"));
            beans.add(bean);
        }
        return beans;
    }

}
