package cn.ug.analyse.service.impl;

import cn.ug.analyse.bean.GrossBean;
import cn.ug.analyse.bean.InvestBean;
import cn.ug.analyse.bean.TradingDetailBean;
import cn.ug.analyse.mapper.InvestMapper;
import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.analyse.service.InvestService;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 渠道投资人交易
 *
 * @author zhaohg
 * @date 2018/08/09.
 */
@Service
public class InvestServiceImpl extends BaseServiceImpl implements InvestService {

    private static Log          log = LogFactory.getLog(InviteServiceImpl.class);
    @Resource
    private        InvestMapper investMapper;

    /**
     * 获取昨天一级绑卡用户
     * @param ids
     * @return
     */
    @Override
    public List<InviteEntity> findYesterdayFirstList(List<Long> ids) {
        return investMapper.findYesterdayFirstList(ids);
    }

    /**
     * 获取昨天二级绑卡用户
     * @param ids
     * @return
     */
    @Override
    public List<InviteEntity> findYesterdaySecondList(List<String> ids) {
        return investMapper.findYesterdaySecondList(ids);
    }

    /**
     * 获取昨天绑卡用户
     * @param ids
     * @param date
     * @return
     */
    @Override
    public List<InviteEntity> findYesterdayBindCardList(List<String> ids, Date date) {
        return investMapper.findYesterdayBindCardList(ids, date);
    }

    /**
     * 获取昨天用户交易总量 年化
     * @param ids
     * @param date
     * @return
     */
    @Override
    public GrossBean findYesterdayFirstListByUserIds(List<String> ids, Date date) {
        return investMapper.findYesterdayFirstListByUserIds(ids,date);
    }

    @Override
    public SerializeObject findTotal(OtherSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setInvestParams(querySubmit);

        Map<String, Object> map = new HashMap<>();
        InvestBean bean = investMapper.findTotal(querySubmit);
        if(bean != null){
            map.put("amount", bean.getAmount());
            map.put("annual", bean.getAnnual());
        }

        return new SerializeObject<>(ResultType.NORMAL, "00000001", map);
    }

    /**
     * 渠道下的投资人的交易记录
     * @param submit
     * @return
     */
    @Override
    public SerializeObject findList(OtherSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setInvestParams(querySubmit);

        int count = investMapper.count(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<InvestBean> list = new ArrayList<>(count);
        if (count > 0) {
            list = investMapper.findList(querySubmit);
        }

        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    /**
     * 导出渠道下的投资人的交易记录
     * @param submit
     * @return
     */
    @Override
    public List<InvestBean> export(OtherSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setInvestParams(querySubmit);
        querySubmit.setLimit(null, null);

        int count = investMapper.count(querySubmit);
        if (count > 0) {
            return investMapper.findList(querySubmit);
        }
        return null;
    }

    @Override
    public List<TradingDetailBean> queryForDetailList(int staffId, String month, int inviteType, int offset, int size) {
        if (staffId > 0 && StringUtils.isNotBlank(month)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("staffId", staffId);
            params.put("month", month);
            params.put("inviteType", inviteType);
            params.put("offset", offset);
            params.put("size", size);
            return investMapper.queryForDetailList(params);
        }
        return null;
    }

    @Override
    public int queryForDetailCount(int staffId, String month, int inviteType) {
        if (staffId > 0 && StringUtils.isNotBlank(month)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("staffId", staffId);
            params.put("month", month);
            params.put("inviteType", inviteType);
            return investMapper.queryForDetailCount(params);
        }
        return 0;
    }
}
