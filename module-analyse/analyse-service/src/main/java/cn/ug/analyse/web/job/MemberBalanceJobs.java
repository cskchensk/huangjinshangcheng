package cn.ug.analyse.web.job;

import cn.ug.analyse.service.MemberBalanceService;
import cn.ug.config.RedisGlobalLock;
import cn.ug.util.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 会员每日余额定时任务
 * @author  ywl
 */
@Component
public class MemberBalanceJobs implements Serializable{

    /** 业务有效期 */
    private static final long EXPIRE_DAY = 1024;

    @Resource
    private MemberBalanceService memberBalanceService;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    private static Logger logger = LoggerFactory.getLogger(MemberBalanceJobs.class);

    /**
     * 每日余额统计
     * 凌晨12点执行
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void memberBalanceJob(){
        String key = "MemberBalanceJobs:expiredMemberBalanceJob1:" + UF.getFormatDateNow();
        if(redisGlobalLock.lock(key, EXPIRE_DAY, TimeUnit.DAYS)) {
            try{
                logger.info("开始执行余额统计定时任务");
                logger.info(UF.getFormatDateTime(UF.getDateTime()));
                //执行任务
                memberBalanceService.memberBalanceJob();
            }catch (Exception e){
                throw  e;
            }finally {
                redisGlobalLock.unlock(key);
            }
        }else{
            logger.info("-------没有获取到锁-------");
            return ;
        }

    }

}
