package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.AssessBean;
import cn.ug.analyse.bean.ChannelStaffRewardsBean;
import cn.ug.analyse.bean.StaffRewardsBean;
import cn.ug.analyse.mapper.entity.AssessEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 渠道及人员业绩
 * @author zhaohg
 * @date 2018/08/09.
 */
@Mapper
public interface AssessMapper {

    /**
     * 添加渠道成员业绩
     * @param assess
     * @return
     */
    int insert(AssessEntity assess);

    /**
     * 更新渠道成员业绩
     * @param assess
     * @return
     */
    int update(AssessEntity assess);

    /**
     * 查询渠道成员业绩
     * @param staffId
     * @param date
     * @return
     */
    AssessEntity findAssessByStaffIdAndDate(@Param("staffId") Long staffId, @Param("date") Date date);

    /**
     * 渠道业绩行数
     * @param querySubmit
     * @return
     */
    int channelAssessCount(QuerySubmit querySubmit);

    /**
     * 渠道业绩list
     * @param querySubmit
     * @return
     */
    List<AssessBean> findChannelAssessList(QuerySubmit querySubmit);

    /**
     * 渠道业绩合计
     * @param querySubmit
     * @return
     */
    AssessBean findChannelAssessTotal(QuerySubmit querySubmit);

    /**
     * 渠道人员业绩行数
     * @param querySubmit
     * @return
     */
    int staffAssessCount(QuerySubmit querySubmit);

    /**
     * 渠道人员业绩list
     * @param querySubmit
     * @return
     */
    List<AssessBean> findStaffAssessList(QuerySubmit querySubmit);

    /**
     * 渠道人员业绩行数
     * @param querySubmit
     * @return
     */
    int findStaffByIdAssessCount(QuerySubmit querySubmit);

    /**
     * 渠道人员业绩list
     * @param querySubmit
     * @return
     */
    List<AssessBean> findStaffByIdAssessList(QuerySubmit querySubmit);

    /**
     * 渠道成员业绩合计
     * @param querySubmit
     * @return
     */
    AssessBean findStaffAssessTotal(QuerySubmit querySubmit);

    /**
     * 渠道时间业绩行数
     * @param querySubmit
     * @return
     */
    int timeAssessCount(QuerySubmit querySubmit);

    /**
     * 渠道时间业绩list
     * @param querySubmit
     * @return
     */
    List<AssessBean> findTimeAssessList(QuerySubmit querySubmit);

    /**
     * 渠道时间业绩合计
     * @param querySubmit
     * @return
     */
    AssessBean findTimeAssessTotal(QuerySubmit querySubmit);

    /**
     * 获取渠道一个周期
     * @param startDate
     * @param endDate
     * @return
     */
    AssessEntity findChannelTotalAccess(@Param("channelId") Long channelId,@Param("startDate")Date startDate, @Param("endDate")Date endDate);

    List<StaffRewardsBean> queryForList(@Param("staffId")int staffId);
    List<StaffRewardsBean> queryForTradingList(@Param("staffId")int staffId);
    double selectTotalAnnual(@Param("staffId")int staffId, @Param("month")String month);
    List<ChannelStaffRewardsBean> queryForStaffRewardsList(Map<String, Object> params);
    int queryForStaffRewardsCount(Map<String, Object> params);
}
