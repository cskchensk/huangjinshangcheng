package cn.ug.analyse.service;

import cn.ug.analyse.mapper.entity.TerminalStatisticsEntity;
import cn.ug.bean.base.SerializeObject;

public interface TerminalStatisticsService {

    SerializeObject insert(TerminalStatisticsEntity entity);

    SerializeObject update(TerminalStatisticsEntity entity);

    SerializeObject addLaunchTimes(TerminalStatisticsEntity entity);

    /**
     * 启动次数统计
     * @param tabId
     * @param startDate
     * @param endDate
     * @param system
     * @return
     */
    SerializeObject findLaunchStatistics(Integer tabId, String startDate,String endDate,String system);
}
