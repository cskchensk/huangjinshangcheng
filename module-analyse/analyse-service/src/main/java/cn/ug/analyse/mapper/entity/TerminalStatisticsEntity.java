package cn.ug.analyse.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class TerminalStatisticsEntity extends BaseEntity implements Serializable {
    //tabId: 1.系统版本，2.设备型号，3.app版本
    private Integer tabId;
    //系统版本
    private String appSystem;
    //手机型号
    private String phoneModel;
    //app版本
    private String appVersion;
    //启动日期
    private String launchDate;
    //启动次数
    private Integer launchTimes;

    public Integer getTabId() {
        return tabId;
    }

    public void setTabId(Integer tabId) {
        this.tabId = tabId;
    }

    public String getAppSystem() {
        return appSystem;
    }

    public void setAppSystem(String appSystem) {
        this.appSystem = appSystem;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Integer getLaunchTimes() {
        return launchTimes;
    }

    public void setLaunchTimes(Integer launchTimes) {
        this.launchTimes = launchTimes;
    }

    public String getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(String launchDate) {
        this.launchDate = launchDate;
    }
}
