package cn.ug.analyse.mapper;

import cn.ug.analyse.mapper.entity.TerminalStatisticsEntity;
import cn.ug.analyse.mapper.entity.TerminalStatisticsResponseEntity;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 终端统计
 *  @author lvkk
 *  @date 2019/10/21.
 */
@Component
public interface TerminalStatisticsMapper extends BaseMapper<TerminalStatisticsEntity> {

    /**
     * 添加
     * @param entity
     * @return
     */
    int insert(TerminalStatisticsEntity entity);

    /**
     * 更新
     * @param entity
     * @return
     */
    int update(TerminalStatisticsEntity entity);

    /**
     * 查询
     * @param entity
     * @return
     */
    TerminalStatisticsEntity findTerminalStatistics(TerminalStatisticsEntity entity);

    /**
     * 根据手机型号、启动日期查询
     * @param params
     * @return
     */
    TerminalStatisticsEntity findByMap(Map<String, Object> params);

    /**
     * 启动次数统计
     * @param tabId
     * @param startDate
     * @param endDate
     * @param appSystem
     * @return
     */
    List<TerminalStatisticsResponseEntity> findTerminalStatistics(@Param("tabId")int tabId, @Param("startDate")String startDate,
                                                                  @Param("endDate")String endDate, @Param("appSystem")String appSystem);
}
