package cn.ug.analyse.mapper.entity;

import cn.ug.analyse.bean.response.BaseOperationBean;

/**
 * @Author zhangweijie
 * @Date 2019/6/14 0014
 * @time 下午 17:18
 **/
public class ChannelRepBean extends BaseOperationBean {
    //渠道参与新用户
    private Integer newJoinUserNum = 0;

    //渠道参与老用户
    private Integer OldJoinUserNum = 0;

    public Integer getNewJoinUserNum() {
        return newJoinUserNum;
    }

    public void setNewJoinUserNum(Integer newJoinUserNum) {
        this.newJoinUserNum = newJoinUserNum;
    }

    public Integer getOldJoinUserNum() {
        return OldJoinUserNum;
    }

    public void setOldJoinUserNum(Integer oldJoinUserNum) {
        OldJoinUserNum = oldJoinUserNum;
    }
}
