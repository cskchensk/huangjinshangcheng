package cn.ug.analyse.service;

import cn.ug.analyse.bean.AssessBean;
import cn.ug.analyse.bean.ChannelRewardsBean;
import cn.ug.analyse.bean.ChannelStaffRewardsBean;
import cn.ug.analyse.bean.StaffRewardsBean;
import cn.ug.analyse.mapper.entity.AssessEntity;
import cn.ug.analyse.web.submit.ChannelStaffSubmit;
import cn.ug.analyse.web.submit.ChannelSubmit;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.bean.base.SerializeObject;

import java.util.Date;
import java.util.List;

/**
 * 统计渠道及成员业绩
 * @author zhaohg
 * @date 2018/08/07.
 */
public interface AssessService {

    SerializeObject findChannelAssessList(ChannelSubmit submit);

    SerializeObject findChannelAssessTotal(ChannelSubmit submit);

    List<AssessBean> exportChannelAssess(ChannelSubmit submit);

    SerializeObject findStaffAssessList(ChannelStaffSubmit submit);

    SerializeObject findStaffByIdAssessList(ChannelStaffSubmit submit);

    List<AssessBean> exportStaffByIdAssess(ChannelStaffSubmit submit);

    SerializeObject findStaffAssessTotal(ChannelSubmit submit);

    List<AssessBean> exportStaffAssess(ChannelStaffSubmit submit);

    SerializeObject findTimeAssessList(OtherSubmit submit);

    SerializeObject findTimeAssessTotal(OtherSubmit submit);

    List<AssessBean> exportTimeAssess(OtherSubmit submit);

    /**
     * 添加昨天的渠道成员业绩
     * @param assess
     * @return
     */
    int insert(AssessEntity assess);

    /**
     * 获取一个周期的渠道总业绩
     * @param startDate
     * @param endDate
     * @return
     */
    AssessEntity findChannelTotalAccess(Long channelId, Date startDate, Date endDate);

    StaffRewardsBean getRewardsOfStaff(int staffId);
    StaffRewardsBean getRewardsOfStaff(int staffId, String month);
    StaffRewardsBean getInvitationRewardsOfStaff(int staffId, String month);



    List<ChannelStaffRewardsBean> queryForStaffRewardsList(int channelId, String month, int offset, int size);
    int queryForStaffRewardsCount(int channelId, String month);
}
