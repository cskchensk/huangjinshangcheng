package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.service.MemberHoldGoldService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 会员持有黄金克重统计
 */
@RestController
@RequestMapping("memberHoldGold")
public class MemberHoldGoldController {


    @Resource
    private MemberHoldGoldService memberHoldGoldService;
    /**
     * 按照日统计列表
     * @param accessToken
     * @return
     */
    @GetMapping(value = "findList")
    public SerializeObject<DataTable<BaseCountBean>> findList(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        DataTable<BaseCountBean> list = memberHoldGoldService.findList(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, list);
    }

    @GetMapping(value = "findTotal")
    public SerializeObject<BigDecimal> findTotal(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        BigDecimal amount = memberHoldGoldService.findTotal(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, amount);
    }

    /**
     * 按照日、周、月统计的列表
     * @param accessToken
     * @return
     */
    @GetMapping(value = "findNewGoldList")
    public SerializeObject<DataTable<BaseCountBean>> findNewGoldList(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        DataTable<BaseCountBean> list = memberHoldGoldService.findNewGoldList(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, list);
    }

    /**
     *新增累计克重
     * @param accessToken
     * @param withdeawParamBean
     * @return
     */
    @GetMapping(value = "findNewGoldTotal")
    public SerializeObject<BigDecimal> findNewGoldTotal(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        BigDecimal amount = memberHoldGoldService.findNewGoldTotal(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, amount);
    }

    /**
     *脚本
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping(value = "test")
    public SerializeObject test(String startDate,String endDate) {
        memberHoldGoldService.memberHoldGoldScript(startDate, endDate);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }
}
