package cn.ug.analyse.mapper.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * 邀请关系
 * @author zhaohg
 * @date 2018/08/07.
 */
public class InviteEntity {

    private Integer index;

    private Long id;
    private Long channelId;
    private String  userId;
    private String  realName;
    private String  mobile;
    private Integer  inviteType; //1:一级用户（没有上上级） 2:二级邀请用户
    private Integer bindCardType;//是否绑卡 1绑定了 0未绑定
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date    bindCardDate;//绑卡日期

    //上级
    private String  inviteId;
    private String  inviteName;

    //上上级
    private String  superiorId;
    private String  superiorName;

    private Date modifyTime;
    private Date addTime;

    private String level;
    private String time;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getInviteType() {
        return inviteType;
    }

    public void setInviteType(Integer inviteType) {
        this.inviteType = inviteType;
    }

    public Integer getBindCardType() {
        return bindCardType;
    }

    public void setBindCardType(Integer bindCardType) {
        this.bindCardType = bindCardType;
    }

    public Date getBindCardDate() {
        return bindCardDate;
    }

    public void setBindCardDate(Date bindCardDate) {
        this.bindCardDate = bindCardDate;
    }

    public String getInviteId() {
        return inviteId;
    }

    public void setInviteId(String inviteId) {
        this.inviteId = inviteId;
    }

    public String getInviteName() {
        return inviteName;
    }

    public void setInviteName(String inviteName) {
        this.inviteName = inviteName;
    }

    public String getSuperiorId() {
        return superiorId;
    }

    public void setSuperiorId(String superiorId) {
        this.superiorId = superiorId;
    }

    public String getSuperiorName() {
        return superiorName;
    }

    public void setSuperiorName(String superiorName) {
        this.superiorName = superiorName;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "InviteEntity{" +
                "index=" + index +
                ", id=" + id +
                ", channelId=" + channelId +
                ", userId='" + userId + '\'' +
                ", realName='" + realName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", inviteType=" + inviteType +
                ", bindCardType=" + bindCardType +
                ", bindCardDate=" + bindCardDate +
                ", inviteId='" + inviteId + '\'' +
                ", inviteName='" + inviteName + '\'' +
                ", superiorId='" + superiorId + '\'' +
                ", superiorName='" + superiorName + '\'' +
                ", modifyTime=" + modifyTime +
                ", addTime=" + addTime +
                '}';
    }
}
