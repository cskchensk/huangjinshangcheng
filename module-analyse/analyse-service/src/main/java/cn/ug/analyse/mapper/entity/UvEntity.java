package cn.ug.analyse.mapper.entity;

/**
 * @Author zhangweijie
 * @Date 2019/6/17 0017
 * @time 上午 10:50
 **/
public class UvEntity {

    /** 天 **/
    private String day;
    /** uv数量 **/
    private Integer uv;
    /** 日期数量 **/
    private Integer dayNumber;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public Integer getUv() {
        return uv;
    }

    public void setUv(Integer uv) {
        this.uv = uv;
    }
}
