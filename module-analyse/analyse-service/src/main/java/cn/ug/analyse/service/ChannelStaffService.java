package cn.ug.analyse.service;

import cn.ug.analyse.bean.ChannelStaffBean;
import cn.ug.analyse.web.submit.ChannelStaffSubmit;
import cn.ug.bean.base.SerializeObject;

import java.io.File;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public interface ChannelStaffService {

    SerializeObject insert(ChannelStaffSubmit submit);

    SerializeObject resetPassword(Long id);

    SerializeObject updateStatus(ChannelStaffSubmit submit);

    SerializeObject findById(Long id);

    SerializeObject findList(ChannelStaffSubmit submit);

    /**
     * 获取所有渠道成员 包括禁用的
     * @param channelId
     * @return
     */
    List<ChannelStaffBean> findListByChannelId(Long channelId);

    List<ChannelStaffBean> export(ChannelStaffSubmit submit);

    SerializeObject insertStaffByExcel(File destFile, Long channelId);


}

