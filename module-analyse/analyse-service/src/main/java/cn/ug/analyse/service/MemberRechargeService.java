package cn.ug.analyse.service;

import cn.ug.analyse.bean.request.BaseParamBean;
import cn.ug.analyse.bean.response.MemberRechargeBean;
import cn.ug.bean.base.DataTable;

public interface MemberRechargeService {

    /**
     * 按照日统计的列表
     * @param baseParamBean
     * @return
     */
    DataTable<MemberRechargeBean> findList(BaseParamBean baseParamBean);

    /**
     * 累计总额
     * @param baseParamBean
     * @return
     */
    MemberRechargeBean findTotal(BaseParamBean baseParamBean);
}
