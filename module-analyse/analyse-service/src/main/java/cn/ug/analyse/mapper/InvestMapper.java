package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.GrossBean;
import cn.ug.analyse.bean.InvestBean;
import cn.ug.analyse.bean.TradingDetailBean;
import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 渠道投资人交易记录 绑卡记录
 *
 * @author zhaohg
 * @date 2018/08/09.
 */
@Mapper
public interface InvestMapper {

    /**
     * 查询昨天渠道邀请的一级用户
     *
     * @return
     */
    List<InviteEntity> findYesterdayFirstList(@Param("ids") List<Long> ids);

    /**
     * 查询昨天渠道邀请的二级用户
     *
     * @return
     */
    List<InviteEntity> findYesterdaySecondList(@Param("ids") List<String> ids);

    /**
     * 查询昨天绑卡用户
     *
     * @param ids
     * @return
     */
    List<InviteEntity> findYesterdayBindCardList(@Param("ids") List<String> ids, @Param("date") Date date);

    /**
     * 查询昨天用户交易总量 年化
     *
     * @param ids
     * @param date
     * @return
     */
    GrossBean findYesterdayFirstListByUserIds(@Param("ids") List<String> ids, @Param("date") Date date);

    /**
     * 渠道下的投资人的交易行数
     *
     * @param querySubmit
     * @return
     */
    int count(QuerySubmit querySubmit);

    /**
     * 渠道下的投资人的交易记录
     *
     * @param querySubmit
     * @return
     */
    List<InvestBean> findList(QuerySubmit querySubmit);

    /**
     * 渠道下的投资人的交易合计
     * @param querySubmit
     * @return
     */
    InvestBean findTotal(QuerySubmit querySubmit);

    List<TradingDetailBean> queryForDetailList(Map<String, Object> params);
    int queryForDetailCount(Map<String, Object> params);
}
