package cn.ug.analyse.mapper;

import cn.ug.analyse.mapper.entity.ChannelStaffEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 渠道成员
 * @author zhaohg
 * @date 2018/08/06.
 */
@Mapper
public interface ChannelStaffMapper {

    /**
     * 添加渠道成员
     * @param entity
     * @return
     */
    int insert(ChannelStaffEntity entity);

    /**
     * 更新渠道成员
     * @param entity
     * @return
     */
    int update(ChannelStaffEntity entity);

    /**
     * 启用禁用渠道成员
     * @param id
     * @return
     */
    int updateStaffStatusById(@Param("id") Long id, @Param("isEnable") Integer isEnable);

    /**
     * 重置密码
     * @param id
     * @param password
     * @return
     */
    int resetPassword(@Param("id") Long id, @Param("password") String password);

    /**
     * 最大渠道编号
     * @return
     */
    int maxStaffNumber(@Param("channelId") Long channelId);

    /**
     * 查询成员
     * @param id
     * @return
     */
    ChannelStaffEntity findById(Long id);

    /**
     * 渠道成员行数
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);

    /**
     * 渠道成员列表
     * @param submit
     * @return
     */
    List<ChannelStaffEntity> findList(QuerySubmit submit);

    /**
     * 获取渠道成员
     * @param ids
     * @return
     */
    List<ChannelStaffEntity> findByIds(@Param("ids") List<Long> ids);

    ChannelStaffEntity getStaffByMobile(@Param("mobile") String mobile);
}
