package cn.ug.analyse.service;

import cn.ug.analyse.bean.request.OperationParamBean;
import cn.ug.bean.base.DataOtherTable;

/**
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 13:40
 **/
public interface MemberOperationService {

    DataOtherTable findOperationTotalList(OperationParamBean operationParamBean);
}
