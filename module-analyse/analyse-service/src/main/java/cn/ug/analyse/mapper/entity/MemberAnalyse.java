package cn.ug.analyse.mapper.entity;

import java.io.Serializable;

public class MemberAnalyse implements Serializable {

    /** id **/
    private String id;
    /** 会员渠道id **/
    private String channelId;
    /** 会员id **/
    private String memberId;
    /** 类型 1:注册 2:绑卡 3:充值 4:交易 **/
    private Integer  type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
