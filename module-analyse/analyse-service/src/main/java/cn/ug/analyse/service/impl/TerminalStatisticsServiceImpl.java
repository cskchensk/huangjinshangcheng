package cn.ug.analyse.service.impl;

import cn.ug.analyse.mapper.TerminalStatisticsMapper;
import cn.ug.analyse.mapper.entity.TerminalStatisticsEntity;
import cn.ug.analyse.mapper.entity.TerminalStatisticsResponseEntity;
import cn.ug.analyse.service.TerminalStatisticsService;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TerminalStatisticsServiceImpl extends BaseServiceImpl implements TerminalStatisticsService {
    @Resource
    private TerminalStatisticsMapper terminalStatisticsMapper;

    @Override
    public SerializeObject insert(TerminalStatisticsEntity entity) {
        int rows =  terminalStatisticsMapper.insert(entity);
        if (rows > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObject<>(ResultType.ERROR, "00000005");
    }

    @Override
    public SerializeObject update(TerminalStatisticsEntity entity) {
        int rows =  terminalStatisticsMapper.update(entity);
        if (rows > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObject<>(ResultType.ERROR, "00000005");
    }

    @Override
    public SerializeObject addLaunchTimes(TerminalStatisticsEntity entity) {
        if (entity == null) {
            return new SerializeObject<>(ResultType.ERROR, "参数错误");
        }
        if (StringUtils.isBlank(entity.getPhoneModel())) {
            return new SerializeObject<>(ResultType.ERROR, "手机型号不能为空");
        }
        Map<String,Object> map = new HashMap<>();
        map.put("phoneModel",entity.getPhoneModel());
        map.put("launchDate",entity.getLaunchDate());
        //获取原启动次数
        TerminalStatisticsEntity oldEntity = terminalStatisticsMapper.findByMap(map);
        //如果没有则添加
        if (oldEntity == null) {
            entity.setId(UF.getRandomUUID());
            entity.setAddTime(LocalDateTime.now());
            entity.setModifyTime(LocalDateTime.now());
            if (StringUtils.isBlank(entity.getAppSystem()) || StringUtils.isBlank(entity.getAppVersion())) {
                return new SerializeObject<>(ResultType.ERROR, "系统和app版本号不能为空");
            }
            entity.setAppSystem(entity.getAppSystem().toUpperCase());
            entity.setLaunchTimes(1);
            int rows = terminalStatisticsMapper.insert(entity);
            Ensure.that(rows).isLt(1, "00000005");
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        int oldTimes = oldEntity.getLaunchTimes();
        TerminalStatisticsEntity updateEntity = new TerminalStatisticsEntity();
        updateEntity.setModifyTime(LocalDateTime.now());
        updateEntity.setLaunchTimes(oldTimes + 1);
        updateEntity.setId(oldEntity.getId());
        int rows = terminalStatisticsMapper.update(updateEntity);
        Ensure.that(rows).isLt(1, "00000005");
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }

    /**
     * 启动次数统计
     *
     * @param tabId
     * @param startDate
     * @param endDate
     * @param system
     * @return
     */
    @Override
    public SerializeObject findLaunchStatistics(Integer tabId, String startDate, String endDate, String system) {
        List<TerminalStatisticsResponseEntity> list = terminalStatisticsMapper.findTerminalStatistics(tabId, startDate, endDate, system);
        if (CollectionUtils.isEmpty(list)) {
            return new SerializeObject<>(ResultType.NORMAL, new ArrayList<>());
        }
        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
