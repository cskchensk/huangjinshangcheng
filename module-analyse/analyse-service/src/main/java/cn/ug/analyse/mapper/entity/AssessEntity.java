package cn.ug.analyse.mapper.entity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 业绩
 *
 * @author zhaohg
 * @date 2018/08/10.
 */
public class AssessEntity {

    private Long       id;
    private Long       channelId;
    private Long       staffId;
    /**
     * 一级绑卡
     */
    private Integer    firstBindCardNum;
    /**
     * 一级绑卡奖励
     */
    private BigDecimal firstBindCardAmount;
    /**
     * 二级绑卡
     */
    private Integer    secondBindCardNum;
    /**
     * 二级绑卡奖励
     */
    private BigDecimal secondBindCardAmount;
    /**
     * 一级年化交易总量
     */
    private BigDecimal firstTradeNum;
    /**
     * 年化交易总量是 交易额*交易期限/365 的汇总值，例如投资10000元30天的标，计算方式为10000*30/365=821.92；
     */
    private BigDecimal firstAnnual;
    /**
     * 二级年化交易总量
     */
    private BigDecimal secondTradeNum;
    /**
     * 年化交易总量是 交易额*交易期限/365 的汇总值，例如投资10000元30天的标，计算方式为10000*30/365=821.92；
     */
    private BigDecimal secondAnnual;
    /**
     * 交易日期
     */
    private Date       tradeDate;

    private Date addTime;
    private Date modifyDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public Integer getFirstBindCardNum() {
        return firstBindCardNum;
    }

    public void setFirstBindCardNum(Integer firstBindCardNum) {
        this.firstBindCardNum = firstBindCardNum;
    }

    public BigDecimal getFirstBindCardAmount() {
        return firstBindCardAmount;
    }

    public void setFirstBindCardAmount(BigDecimal firstBindCardAmount) {
        this.firstBindCardAmount = firstBindCardAmount;
    }

    public BigDecimal getSecondBindCardAmount() {
        return secondBindCardAmount;
    }

    public void setSecondBindCardAmount(BigDecimal secondBindCardAmount) {
        this.secondBindCardAmount = secondBindCardAmount;
    }

    public Integer getSecondBindCardNum() {
        return secondBindCardNum;
    }

    public void setSecondBindCardNum(Integer secondBindCardNum) {
        this.secondBindCardNum = secondBindCardNum;
    }

    public BigDecimal getFirstTradeNum() {
        return firstTradeNum;
    }

    public void setFirstTradeNum(BigDecimal firstTradeNum) {
        this.firstTradeNum = firstTradeNum;
    }

    public BigDecimal getFirstAnnual() {
        return firstAnnual;
    }

    public void setFirstAnnual(BigDecimal firstAnnual) {
        this.firstAnnual = firstAnnual;
    }

    public BigDecimal getSecondTradeNum() {
        return secondTradeNum;
    }

    public void setSecondTradeNum(BigDecimal secondTradeNum) {
        this.secondTradeNum = secondTradeNum;
    }

    public BigDecimal getSecondAnnual() {
        return secondAnnual;
    }

    public void setSecondAnnual(BigDecimal secondAnnual) {
        this.secondAnnual = secondAnnual;
    }

    public Date getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(Date modifyDate) {
        this.modifyDate = modifyDate;
    }
}
