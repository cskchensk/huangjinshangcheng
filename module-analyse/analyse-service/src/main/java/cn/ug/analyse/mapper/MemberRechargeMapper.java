package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.request.BaseParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MemberRechargeMapper extends BaseMapper {

    List<BaseCountBean> findList(BaseParamBean baseParamBean);
}
