package cn.ug.analyse.service.impl;

import cn.ug.analyse.bean.InvitationDetailBean;
import cn.ug.analyse.mapper.ChannelMapper;
import cn.ug.analyse.mapper.ChannelStaffMapper;
import cn.ug.analyse.mapper.InviteMapper;
import cn.ug.analyse.mapper.entity.ChannelEntity;
import cn.ug.analyse.mapper.entity.ChannelStaffEntity;
import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.analyse.service.InviteService;
import cn.ug.analyse.web.submit.InviteSubmit;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.service.impl.BaseServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhaohg
 * @date 2018/08/07.
 */
@Service
public class InviteServiceImpl extends BaseServiceImpl implements InviteService {
    @Resource
    private InviteMapper inviteMapper;
    @Autowired
    private ChannelStaffMapper channelStaffMapper;
    @Autowired
    private ChannelMapper channelMapper;


    @Override
    public int insert(InviteEntity entity) {
        InviteEntity user = inviteMapper.findByUserId(entity.getUserId());
        if (user == null) {
            return inviteMapper.insert(entity);
        }
        return 0;
    }

    /**
     * 查询渠道成员下的用户
     * @param staffId
     * @return
     */
    @Override
    public List<InviteEntity> findAllUserListByStaffId(Long staffId) {
        return inviteMapper.findAllUserListByStaffId(staffId);
    }
//    /**
//     * 成员下一级用户数量
//     * @param staffId
//     * @param date
//     * @return
//     */
//    @Override
//    public int findYesterdayFirstBindCardNumByStaffId(Long staffId, LocalDate date){
//        return inviteMapper.findYesterdayFirstBindCardNumByStaffId(staffId, date);
//    }
//
//    /**
//     * 成员下二级用户数量
//     * @param staffId
//     * @param date
//     * @return
//     */
//    @Override
//    public int findYesterdaySecondBindCardNumByStaffId(Long staffId, LocalDate date){
//        return inviteMapper.findYesterdaySecondBindCardNumByStaffId(staffId, date);
//    }





    /**
     * 渠道下绑卡详细列表
     *
     * @param submit
     * @return
     */
    @Override
    public SerializeObject inviteBindCardList(InviteSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = inviteMapper.inviteBindCardCount(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<InviteEntity> list = new ArrayList<>(count);
        if (count > 0) {
            list = inviteMapper.inviteBindCardList(querySubmit);
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    /**
     * 导出渠道下的绑卡详细
     *
     * @param submit
     * @return
     */
    @Override
    public List<InviteEntity> inviteBindCardExport(InviteSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);
        querySubmit.setLimit(null, null);

        int count = inviteMapper.inviteBindCardCount(querySubmit);
        if (count > 0) {
            return inviteMapper.inviteBindCardList(querySubmit);
        }
        return null;
    }

    @Override
    public List<InvitationDetailBean> queryForDetailList(int staffId, String month, int inviteType, int offset, int size) {
        if (staffId > 0 && StringUtils.isNotBlank(month)) {
            long id = staffId;
            ChannelStaffEntity staffEntity = channelStaffMapper.findById(id);
            if (staffEntity != null && staffEntity.getChannelId() != null && staffEntity.getChannelId() > 0) {
                ChannelEntity channelEntity = channelMapper.findById(staffEntity.getChannelId());
                if (channelEntity == null || StringUtils.isBlank(channelEntity.getPlanStep())) {
                    return null;
                }
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("staffId", staffId);
                params.put("month", month);
                params.put("inviteType", inviteType);
                params.put("offset", offset);
                params.put("size", size);
                List<InvitationDetailBean> list = inviteMapper.queryForDetailList(params);
                if (list != null && list.size() > 0) {
                    for (InvitationDetailBean bean : list) {
                        if (bean.getInviteType() == 1 && channelEntity.getOneInviteReward() != null) {
                            bean.setRewards(channelEntity.getOneInviteReward());
                        }
                        if (bean.getInviteType() == 2 && channelEntity.getTwoInviteReward() != null) {
                            bean.setRewards(channelEntity.getTwoInviteReward());
                        }
                    }
                }
                return list;
            }
        }
        return null;
    }

    @Override
    public int queryForDetailCount(int staffId, String month, int inviteType) {
        if (staffId > 0 && StringUtils.isNotBlank(month)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("staffId", staffId);
            params.put("month", month);
            params.put("inviteType", inviteType);
            return inviteMapper.queryForDetailCount(params);
        }
        return 0;
    }

}
