package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.ChannelSettleBean;
import cn.ug.analyse.bean.SettleBean;
import cn.ug.analyse.service.SettleService;
import cn.ug.analyse.web.submit.ChannelSubmit;
import cn.ug.analyse.web.submit.OtherSubmit;
import cn.ug.analyse.web.submit.SettleSubmit;
import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.util.DateUtil;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 渠道结算 及结算记录
 *
 * @author zhaohg
 * @date 2018/08/07.
 */
@RestController
@RequestMapping("/settle")
public class ChannelSettleController {

    @Autowired
    private SettleService   settleService;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    //结算 添加结算记录
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject addSettle(@RequestHeader String accessToken, SettleSubmit submit) {
        if (submit.getChannelId() > 0 && submit.getAmount() != null) {
            return settleService.insert(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 获取渠道结算list
     */
    @RequestMapping(value = "/channel/list", method = GET)
    public SerializeObject findChannelSettleList(@RequestHeader String accessToken, ChannelSubmit submit) {
        return settleService.findChannelSettleList(submit);
    }

    /**
     * 获取渠道结算合计
     */
    @RequestMapping(value = "/channel/total", method = GET)
    public SerializeObject findChannelSettleTotal(@RequestHeader String accessToken, ChannelSubmit submit) {
        return settleService.findChannelSettleTotal(submit);
    }

    /**
     * 获取渠道结算list
     */
    @RequestMapping(value = "/channel/export", method = GET)
    public void exportChannelSettleList( HttpServletResponse response, ChannelSubmit submit) {
        List<ChannelSettleBean> list = settleService.exportChannelSettleList(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "渠道编号", "渠道名称", "奖励方案", "合计奖励总额（元）", "待结算金额（元）", "已结算金额（元）"};
            String[] columns = {"index", "number", "name", "planName", "totalReward","waitReward","settleReward" };
            String fileName = "渠道结算";
            ExportExcelController<ChannelSettleBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapChannelSettleData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    private List<ChannelSettleBean> wrapChannelSettleData(List<ChannelSettleBean> list) {
        List<ChannelSettleBean> beans = new ArrayList<>();
        int index = 0;
        for (ChannelSettleBean bean : list) {
            ChannelSettleBean channel = dozerBeanMapper.map(bean, ChannelSettleBean.class);
            channel.setPlanName("阶梯交易方案");
            channel.setIndex(index++);
            beans.add(channel);
        }
        return beans;
    }

    /**
     * 获取渠道结算历史list
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject findList(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit.getChannelId() > 0) {
            return settleService.findList(submit);
        }
        return new SerializeObjectError("00000005");
    }


    @RequestMapping(value = "/export", method = GET)
    public void export( HttpServletResponse response, OtherSubmit submit) {
        if (submit.getChannelId() < 1) {
            return;
        }
        List<SettleBean> list = settleService.export(submit);
        if (list != null && !list.isEmpty()) {
            String[] columnNames = {"序号", "结算金额", "结算时间","操作人"};
            String[] columns = {"index", "amount", "time","settleUserName"};
            String fileName = "渠道结算记录";
            ExportExcelController<SettleBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, wrapSettleData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    private List<SettleBean> wrapSettleData(List<SettleBean> list) {
        List<SettleBean> beans = new ArrayList<>();
        int index = 0;
        for (SettleBean bean : list) {
            bean.setIndex(index++);
            bean.setTime(DateUtil.format(bean.getAddTime(), "yyyy-MM-dd"));
            beans.add(bean);
        }
        return beans;
    }


    /**
     * 渠道周期奖励记录list
     * @param submit
     * @return
     */
    @RequestMapping(value = "/period/list", method = GET)
    public SerializeObject findChannelPeriodList(@RequestHeader String accessToken, OtherSubmit submit) {
        if (submit.getChannelId() > 0) {
            return settleService.findChannelPeriodList(submit);
        }
        return new SerializeObjectError("00000005");
    }

}
