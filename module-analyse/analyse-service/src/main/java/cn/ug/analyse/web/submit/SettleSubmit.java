package cn.ug.analyse.web.submit;

import java.math.BigDecimal;

/**
 * @author zhaohg
 * @date 2018/08/13.
 */
public class SettleSubmit {

    private Long channelId;
    private BigDecimal amount;

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
