package cn.ug.analyse.service;

import cn.ug.analyse.bean.ChannelMemberBean;
import cn.ug.analyse.bean.request.*;
import cn.ug.analyse.bean.response.*;
import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.ActiveMemberBean;
import cn.ug.pay.bean.response.ActiveTotalBean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public interface MemberAnalyseService {

    /**
     * 新增数据分析
     * @param entity
     */
    void addMemberAnalyse(MemberAnalyseMsgParamBean entity);

    List<MemberAnalyseBean> findList(MemberAnalyseParamBean entity);

    MemberAnalyseBean getChannelDetail(String channelId);

    /**
     * 查询活跃用户
     * @param financeBillParam
     * @return
     */
    DataTable<ActiveMemberBean> findBuySellActiveMemberList(FinanceBillParam financeBillParam);

    /**
     *
     * @param financeBillParam
     * @return
     */
    List<ActiveMemberBean> findBuySellActiveMemberWeekList(FinanceBillParam financeBillParam);

    /**
     * 累计活跃用户数量
     * @param financeBillParam
     * @return
     */
    List<ActiveTotalBean>  findBuySellActiveMemberTotal(FinanceBillParam financeBillParam);

    /**
     * 查询休眠用户
     * @param paramBean
     * @return
     */
    Map<String,Object> findDormancyMember(DormancyMemberParamBean paramBean);

    /**
     *查询用户画像数据统计
     * @param paramBean
     * @return
     */
    List<MemberProtrayBean> findMemberProtrayList(MemberPortrayParamBean paramBean);

    /**
     * 根据年龄
     * @return
     */
    List<MemberProtrayBean> findAgeProtrayList();

    /**
     * 查询会员年龄分布
     * @return
     */
    List<MemberRatioBean> findMemberAgeList(MemberAngParamBean memberAngParamBean);

    /**
     * 查询终端会员
     * @return
     */
    List<Integer> findTerminalMember();

    /**
     * 查询会员总数
     * @return
     */
    Integer findRegisterMember(String channelId);

    List<ChannelMemberBean> queryForList(String channelId, int offset, int size);
    int queryForCount(String channelId);

    /**
     * 查询邀请比例分布
     * @return
     */
    List<MemberRatioBean> findMemberFriendList(MemberAngParamBean memberAngParamBean);

    /**
     * 查询用户交易时间段
     * @param memberAngParamBean
     * @return
     */
    MemberTraderRonseespBean findMemberIdNumTodayTraderList(MemberAngParamBean memberAngParamBean);

}
