package cn.ug.analyse.web.controller;

import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.service.InviteService;
import cn.ug.analyse.web.submit.InviteSubmit;
import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.util.DateUtil;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 渠道邀请关系
 *
 * @author zhaohg
 * @date 2018/08/07.
 */
@RestController
@RequestMapping("/invite")
public class ChannelInviteController {

    @Autowired
    private InviteService   inviteService;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    /**
     * 绑卡详细表
     */
    @RequestMapping(value = "/bindCard/list", method = GET)
    public SerializeObject findList(@RequestHeader String accessToken, InviteSubmit submit) {
        if (submit.getChannelId() > 0) {
            return inviteService.inviteBindCardList(submit);
        }
        return new SerializeObjectError("00000005");
    }

    @RequestMapping(value = "/bindCard/export", method = GET)
    public void inviteBindCardExport( HttpServletResponse response, InviteSubmit submit) {
        if (submit.getChannelId() > 0) {
            List<InviteEntity> list = inviteService.inviteBindCardExport(submit);
            if (list != null && !list.isEmpty()) {
                String[] columnNames = {"序号", "绑卡人姓名", "绑卡人手机号", "绑卡时间", "邀请人", "邀请人上级", "类型"};
                String[] columns = {"index", "realName", "mobile", "time", "inviteName", "superiorName", "level"};
                String fileName = "绑卡详细";
                ExportExcelController<InviteEntity> export = new ExportExcelController<>();
                export.exportExcel(fileName, fileName, columnNames, columns, wrapInviteEntityData(list), response, ExportExcelUtil.EXCEL_FILE_2003);
            }
        }
    }

    private List<InviteEntity> wrapInviteEntityData(List<InviteEntity> list) {
        List<InviteEntity> beans = new ArrayList<>();
        int index = 0;
        for (InviteEntity bean : list) {
            bean.setIndex(index++);
            bean.setTime(DateUtil.format(bean.getBindCardDate(),"yyyy-MM-dd"));
            bean.setLevel(bean.getInviteType() > 1 ?"二级":"一级");
            beans.add(bean);
        }
        return beans;
    }


}
