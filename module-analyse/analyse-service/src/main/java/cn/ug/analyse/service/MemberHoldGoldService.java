package cn.ug.analyse.service;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.bean.base.DataTable;

import java.math.BigDecimal;

public interface MemberHoldGoldService {

    /**
     * 按照日统计列表
     * @param withdeawParamBean
     * @return
     */
    DataTable<BaseCountBean> findList(WithdeawParamBean withdeawParamBean);

    /**
     * 累计持有总额
     * @param withdeawParamBean
     * @return
     */
    BigDecimal findTotal(WithdeawParamBean withdeawParamBean);

    /**
     * 按照日、周、月统计的列表
     * @param withdeawParamBean
     * @return
     */
    DataTable<BaseCountBean> findNewGoldList(WithdeawParamBean withdeawParamBean);

    /**
     * 累计持有克重
     * @param withdeawParamBean
     * @return
     */
    BigDecimal findNewGoldTotal(WithdeawParamBean withdeawParamBean);

    /**
     * 会员持有黄金克重
     */
    void memberHoldJob();

    void memberHoldGoldScript(String startTimeString,String endTimeString);

}
