package cn.ug.analyse.web.controller;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.service.MemberBalanceService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员每日余额
 */
@RestController
@RequestMapping("memberBalance")
public class MemberBalanceController implements Serializable{

    @Resource
    private MemberBalanceService memberBalanceService;

    /**
     * 按照日、周、月统计的列表
     * @param accessToken
     * @return
     */
    @GetMapping(value = "findList")
    public SerializeObject<DataTable<BaseCountBean>> findList(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        DataTable<BaseCountBean> list = memberBalanceService.findList(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, list);
    }

    @GetMapping(value = "findTotal")
    public SerializeObject<BigDecimal> findTotal(@RequestHeader String accessToken, WithdeawParamBean withdeawParamBean) {
        BigDecimal amount = memberBalanceService.findTotal(withdeawParamBean);
        return new SerializeObject(ResultType.NORMAL, amount);
    }

    @GetMapping(value = "test")
    public SerializeObject<BigDecimal> test(String startDate,String endDate) {
        memberBalanceService.memberBalanceScript(startDate, endDate);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }
}
