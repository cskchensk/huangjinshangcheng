package cn.ug.analyse.service;

import cn.ug.analyse.bean.ChannelBean;
import cn.ug.analyse.bean.ChannelStaffBean;
import cn.ug.analyse.bean.ChannelUserBean;
import cn.ug.analyse.web.submit.ChannelSubmit;
import cn.ug.bean.base.DataOtherTable;
import cn.ug.bean.base.SerializeObject;

import java.util.Date;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public interface ChannelService {

    SerializeObject insert(ChannelSubmit submit);

    SerializeObject updateRewardPlan(ChannelSubmit submit);

    SerializeObject updateInvitation(ChannelSubmit submit);

    SerializeObject updateChannelStatus(ChannelSubmit submit);

    SerializeObject findById(Long id);

    SerializeObject findList(ChannelSubmit submit);

    List<ChannelBean> export(ChannelSubmit submit);

    /**
     * 校验参数
     * @param channelName
     * @param passWord
     * @return
     */
    SerializeObject<ChannelUserBean> verifyLogin(String channelName, String passWord, Integer type);

    /**
     * 获取启用的 并在有效时间内的渠道
     *
     * @return
     */
    List<ChannelBean> findListByDate(Date date);

    /**
     * 重置商户登陆密码
     * @param id
     * @return
     */
    SerializeObject resetPassword(Long id);

    /**
     * 重新设置登陆密码
     * @param id
     * @param newPassWord
     * @return
     */
    SerializeObject updatePassWord(Long id,String newPassWord,Integer type,String mobile);

    /**
     * 查询根据手机用户
     * @param mobile
     * @return
     */
    SerializeObject<ChannelStaffBean> findChannelStaffByMobile(String mobile);
//    /**
//     * 累加渠道奖励
//     * @param id
//     * @param amount
//     * @return
//     */
//    int addChannelReward(Long id, BigDecimal amount);

//    /**
//     * 结算奖励
//     * @param id
//     * @param amount
//     * @return
//     */
//    int settleChannelReward(Long id, BigDecimal amount);

    /**
     * 统计
     * @param channels
     * @return
     */
    DataOtherTable statistics(String channels,int pageNum,int pageSize);

    DataOtherTable findDetail(String startTime, String endTime,String channelId,int pageNum,int pageSize);
}
