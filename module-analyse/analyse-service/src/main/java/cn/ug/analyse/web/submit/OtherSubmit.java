package cn.ug.analyse.web.submit;

import cn.ug.analyse.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;

/**
 * @author zhaohg
 * @date 2018/08/09.
 */
public class OtherSubmit {

    private Long channelId;
    private String channelName;
    private String userId;
    private String realName;
    private String mobile;
    private Integer inviteType;

    private String startTime;
    private String endTime;
    private Integer pageNum = 1;
    private Integer pageSize = 10;


    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getInviteType() {
        return inviteType;
    }

    public void setInviteType(Integer inviteType) {
        this.inviteType = inviteType;
    }

    public void setTimeAssessParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("channelId", this.channelId);
        if(StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(this.startTime));
        }
        if(StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(this.endTime));
        }
    }

    public void setInvestParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("channelId", this.channelId);
        querySubmit.put("realName", this.realName);
        querySubmit.put("inviteType", this.inviteType);
        querySubmit.put("userId", this.userId);

        if(StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(this.startTime));
        }
        if(StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(this.endTime));
        }
    }

    public void setSettleParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("channelId", this.channelId);
//        querySubmit.put("realName", this.realName);
//        querySubmit.put("mobile", this.mobile);

        if(StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(this.startTime));
        }
        if(StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(this.endTime));
        }
    }

    public void setChannelPeriodParams(QuerySubmit querySubmit) {
        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("channelId", this.channelId);

        if(StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(this.startTime));
        }
        if(StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(this.endTime));
        }
    }
}
