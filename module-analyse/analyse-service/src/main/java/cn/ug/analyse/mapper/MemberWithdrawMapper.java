package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface MemberWithdrawMapper extends BaseMapper{

    List<BaseCountBean> findList(WithdeawParamBean withdeawParamBean);

}
