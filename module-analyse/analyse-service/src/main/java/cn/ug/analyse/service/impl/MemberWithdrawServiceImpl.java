package cn.ug.analyse.service.impl;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.mapper.MemberWithdrawMapper;
import cn.ug.analyse.service.MemberWithdrawService;
import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.BigDecimalUtil;
import cn.ug.util.Common;
import cn.ug.util.Week;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MemberWithdrawServiceImpl implements MemberWithdrawService {

    @Resource
    private MemberWithdrawMapper memberWithdrawMapper;

    @Override
    public DataTable<BaseCountBean> findWithdrawTotalList(WithdeawParamBean withdeawParamBean) {
        List<BaseCountBean> resultList = new ArrayList<BaseCountBean>();

        if(StringUtils.isBlank(withdeawParamBean.getStartTime())) withdeawParamBean.setStartTime(LocalDate.now().plusDays(-6).toString());
        if(StringUtils.isBlank(withdeawParamBean.getEndTime())) withdeawParamBean.setEndTime(LocalDate.now().toString());

        //参数必须传递--起始时间、结束时间
        Ensure.that(withdeawParamBean.getStartTime()).isNull("21000001");
        Ensure.that(withdeawParamBean.getEndTime()).isNull("21000002");
        Ensure.that(withdeawParamBean.getDayType()).isNull("21000003");
        Ensure.that(withdeawParamBean.getPageType()).isNull("21000004");

        int pageNum = withdeawParamBean.getPageNum();
        int pageSize = withdeawParamBean.getPageSize();
        withdeawParamBean.setPageNum(1);
        withdeawParamBean.setPageSize(100000);
        List<BaseCountBean> list = memberWithdrawMapper.findList(withdeawParamBean);
        switch (withdeawParamBean.getDayType()){
            case 1: resultList = findEverydayList(withdeawParamBean);break; //每日
            case 2: resultList = findEvenyWeek(withdeawParamBean);break;    //每周
            case 3: resultList = findEveryMonth(withdeawParamBean);break;   //每月
        }

        //数据分页
        if(resultList != null && !resultList.isEmpty()) {
            Collections.reverse(resultList);  //倒序排列

            //查询是否分页
            if(withdeawParamBean.getPageType() == 2){
                int total = resultList.size();
                //计算
                int num = (pageNum-1)* pageSize;
                resultList = resultList.stream().skip(num).collect(Collectors.toList());
                List<BaseCountBean> pageList = resultList.stream().limit(pageSize).collect(Collectors.toList());
                return new DataTable<>(pageNum, pageSize, total, pageList);
            }else{
                return new DataTable<>(pageNum, pageSize, resultList.size(), resultList);
            }
        }else {
            return new DataTable<>(pageNum, pageSize, 0, new ArrayList<>());
        }
    }

    @Override
    public List<BaseCountBean> findEverydayList(WithdeawParamBean withdeawParamBean) {
        List<BaseCountBean> resultList = new ArrayList<BaseCountBean>();

        //参数必须传递--起始时间、结束时间
        Ensure.that(withdeawParamBean.getStartTime()).isNull("21000001");
        Ensure.that(withdeawParamBean.getEndTime()).isNull("21000002");

        List<BaseCountBean> list = memberWithdrawMapper.findList(withdeawParamBean);

        //设置开始时间
        LocalDate startTime = LocalDate.parse(withdeawParamBean.getStartTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endTime = LocalDate.parse(withdeawParamBean.getEndTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        //获取查询出的天数
        int day = Integer.valueOf(Common.until(startTime,endTime) +"") + 1;
        for(int i = 0;i< day;i++) {
            //当前日期时间字符串
            String startTimeString = startTime.toString();
            BaseCountBean entity = new BaseCountBean();
            entity.setDay(startTimeString);
            if(list != null && !list.isEmpty()){
                List<BaseCountBean> filterList = list.stream().filter(o -> o.getDay().contains(startTimeString)).collect(Collectors.toList());
                if(filterList != null && !filterList.isEmpty()){
                    entity.setAmount(filterList.get(0).getAmount());
                }else{
                    entity.setAmount(BigDecimalUtil.to2Point(BigDecimal.ZERO));
                }
            }else{
                entity.setAmount(BigDecimalUtil.to2Point(BigDecimal.ZERO));
            }
            resultList.add(entity);
            startTime = startTime.plusDays(1);
        }
        return resultList;
    }

    @Override
    public BigDecimal findTotal(WithdeawParamBean withdeawParamBean) {
        List<BaseCountBean> resultList = new ArrayList<BaseCountBean>();
        if(StringUtils.isBlank(withdeawParamBean.getStartTime())) withdeawParamBean.setStartTime(LocalDate.now().plusDays(-6).toString());
        if(StringUtils.isBlank(withdeawParamBean.getEndTime())) withdeawParamBean.setEndTime(LocalDate.now().toString());

        //参数必须传递--起始时间、结束时间
        Ensure.that(withdeawParamBean.getStartTime()).isNull("21000001");
        Ensure.that(withdeawParamBean.getEndTime()).isNull("21000002");

        int pageNum = withdeawParamBean.getPageNum();
        int pageSize = withdeawParamBean.getPageSize();
        withdeawParamBean.setPageNum(1);
        withdeawParamBean.setPageSize(100000);
        switch (withdeawParamBean.getDayType()){
            case 1: resultList = findEverydayList(withdeawParamBean);break; //每日
            case 2: resultList = findEvenyWeek(withdeawParamBean);break;    //每周
            case 3: resultList = findEveryMonth(withdeawParamBean);break;   //每月
        }

        BigDecimal amount = BigDecimal.ZERO;
        for(BaseCountBean entity: resultList){
            amount = amount.add(entity.getAmount());
        }
        return amount;
    }

    /**
     * 按周统计
     * @param withdeawParamBean
     * @return
     */
    public List<BaseCountBean> findEvenyWeek(WithdeawParamBean withdeawParamBean){
        List<BaseCountBean> resultList = new ArrayList<BaseCountBean>();

        //根据起始时间和结束时间获取周
        LocalDate startTime = LocalDate.parse(withdeawParamBean.getStartTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endTime = LocalDate.parse(withdeawParamBean.getEndTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<Week> weekList = Common.getWeekList(startTime, endTime);

        //获取数据
        List<BaseCountBean> list = memberWithdrawMapper.findList(withdeawParamBean);

        for(Week week:weekList){
            BaseCountBean baseCountBean = new BaseCountBean();
            baseCountBean.setDay(week.getDay());
            BigDecimal amount = BigDecimalUtil.to2Point(BigDecimal.ZERO);

            if(list != null && !list.isEmpty()){
                List<BaseCountBean> filterList = list.stream().filter(o -> o.getDayNumber() >= week.getStartTimeNumber() && o.getDayNumber() <= week.getEndTimeNumber()).collect(Collectors.toList());
                if(filterList != null && !filterList.isEmpty()){
                    for(BaseCountBean entity:filterList){
                        amount = amount.add(entity.getAmount());
                    }
                }
            }
            baseCountBean.setAmount(amount);
            resultList.add(baseCountBean);
        }
        return resultList;
    }

    /**
     * 按月统计
     * @param withdeawParamBean
     * @return
     */
    public List<BaseCountBean> findEveryMonth(WithdeawParamBean withdeawParamBean){
        List<BaseCountBean> resultList = new ArrayList<BaseCountBean>();
        List<String> monthList = new ArrayList<String>();

        //参数必须传递--起始时间、结束时间
        Ensure.that(withdeawParamBean.getStartTime()).isNull("21000001");
        Ensure.that(withdeawParamBean.getEndTime()).isNull("21000002");

        List<BaseCountBean> list = memberWithdrawMapper.findList(withdeawParamBean);

        //设置开始时间
        LocalDate startTime = LocalDate.parse(withdeawParamBean.getStartTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endTime = LocalDate.parse(withdeawParamBean.getEndTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM");
        String endMonth = df.format(endTime);
        while(true){
            String month  = df.format(startTime);
            monthList.add(month);
            if(month.equals(endMonth)){
                break;
            }else{
                startTime = startTime.plusMonths(1);
            }
        }

        //数据遍历
        if(monthList != null && !monthList.isEmpty()){
            for(int i=0;i<monthList.size();i++){
                BaseCountBean baseCountBean = new BaseCountBean();
                baseCountBean.setDay(monthList.get(i));
                Integer num = i;
                if(list != null && !list.isEmpty()){
                    List<BaseCountBean> filterList = list.stream().filter(o -> o.getDay().contains(monthList.get(num).toString())).collect(Collectors.toList());
                    if(filterList != null && !filterList.isEmpty()){
                        BigDecimal amount = BigDecimalUtil.to2Point(BigDecimal.ZERO);
                        for(BaseCountBean entity:filterList){
                            amount = amount.add(entity.getAmount());
                        }
                        baseCountBean.setAmount(amount);
                    }else{
                        baseCountBean.setAmount(BigDecimalUtil.to2Point(BigDecimal.ZERO));
                    }
                }else{
                    baseCountBean.setAmount(BigDecimalUtil.to2Point(BigDecimal.ZERO));
                }
                resultList.add(baseCountBean);
            }
        }
        return resultList;
    }
    public static void main(String[] args) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM");
        LocalDate startTime = LocalDate.parse("2018-05-05", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endTime = LocalDate.parse("2018-06-06", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        /*System.out.println(startTime.);*/
       /* LocalDate localDate = LocalDate.now();
        System.out.println(localDate.plusDays(-6).toString());*/
       BigDecimal str = new BigDecimal("0");
        System.out.println(BigDecimalUtil.to2Point(str));
        System.out.println(BigDecimalUtil.to2Point(BigDecimal.ZERO));
    }
}
