package cn.ug.analyse.service;

import java.util.Date;

/**
 * @author zhaohg
 * @date 2018/08/13.
 */
public interface StatService {

    void statChannelData(Date date);
}

