package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.mapper.entity.MemberHold;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public interface MemberHoldGoldMapper extends BaseMapper<MemberHold>{

    List<BaseCountBean> findList(WithdeawParamBean withdeawParamBean);

    /**
     * 定期冻结部分(未到期)
     * @return
     */
    BigDecimal findFreezeAmount(@Param("day") String day);

    /**
     * 定期已到期克重(本金+利息)
     * @return
     */
    BigDecimal findExpiredAmount(@Param("day") String day);

    /**
     * 活期利息
     * @return
     */
    BigDecimal findCurrentInterest(@Param("day") String day);

    /**
     * 查询人民币购买活期产品
     * @return
     */
    BigDecimal findMoneyCurrent(@Param("day") String day);

    /**
     * 查询昨日支出克重(卖金+提金)
     * @return
     */
    BigDecimal findPayAmount(@Param("day") String day);

    BigDecimal findByDay(@Param("day") String day);

    int getHoldGold();
}
