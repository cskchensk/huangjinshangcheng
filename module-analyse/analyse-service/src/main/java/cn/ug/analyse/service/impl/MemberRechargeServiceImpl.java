package cn.ug.analyse.service.impl;

import cn.ug.analyse.bean.request.BaseParamBean;
import cn.ug.analyse.bean.request.WithdeawParamBean;
import cn.ug.analyse.bean.response.BaseCountBean;
import cn.ug.analyse.bean.response.MemberRechargeBean;
import cn.ug.analyse.mapper.MemberRechargeMapper;
import cn.ug.analyse.mapper.MemberWithdrawMapper;
import cn.ug.analyse.service.MemberRechargeService;
import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.Common;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MemberRechargeServiceImpl implements MemberRechargeService {

    @Resource
    private MemberRechargeMapper memberRechargeMapper;

    @Resource
    private MemberWithdrawMapper memberWithdrawMapper;

    @Override
    public DataTable<MemberRechargeBean> findList(BaseParamBean baseParamBean) {
        List<MemberRechargeBean> resultList = new ArrayList<MemberRechargeBean>();

        if(StringUtils.isBlank(baseParamBean.getStartTime())) baseParamBean.setStartTime(LocalDate.now().plusDays(-6).toString());
        if(StringUtils.isBlank(baseParamBean.getEndTime())) baseParamBean.setEndTime(LocalDate.now().toString());

        //参数必须传递--起始时间、结束时间
        Ensure.that(baseParamBean.getStartTime()).isNull("21000001");
        Ensure.that(baseParamBean.getEndTime()).isNull("21000002");

        int pageNum = baseParamBean.getPageNum();
        int pageSize = baseParamBean.getPageSize();

        //获取提现金额列表
        WithdeawParamBean withdeawParamBean = new WithdeawParamBean();
        withdeawParamBean.setStartTime(baseParamBean.getStartTime());
        withdeawParamBean.setEndTime(baseParamBean.getEndTime());
        withdeawParamBean.setPageNum(1);
        withdeawParamBean.setPageSize(100000);
        List<BaseCountBean> wlist = memberWithdrawMapper.findList(withdeawParamBean);

        //获取充值金额列表
        baseParamBean.setPageNum(1);
        baseParamBean.setPageSize(100000);
        List<BaseCountBean> rList = memberRechargeMapper.findList(baseParamBean);

        //设置开始时间
        LocalDate startTime = LocalDate.parse(baseParamBean.getStartTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endTime = LocalDate.parse(baseParamBean.getEndTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        //获取查询出的天数
        int day = Integer.valueOf(Common.until(startTime,endTime) +"") + 1;
        for(int i = 0;i< day;i++) {
            //当前日期时间字符串
            String startTimeString = startTime.toString();
            BigDecimal wamount = BigDecimal.ZERO;
            BigDecimal ramount = BigDecimal.ZERO;
            MemberRechargeBean entity = new MemberRechargeBean();
            entity.setDay(startTimeString);

            List<BaseCountBean> wfilterList = wlist.stream().filter(o -> o.getDay().contains(startTimeString)).collect(Collectors.toList());
            if(wfilterList != null && !wfilterList.isEmpty()){
                wamount = wfilterList.get(0).getAmount();
            }

            List<BaseCountBean> rfilterList = rList.stream().filter(o -> o.getDay().contains(startTimeString)).collect(Collectors.toList());
            if(rfilterList != null && !rfilterList.isEmpty()){
                ramount = rfilterList.get(0).getAmount();
            }

            entity.setRechargeAmount(ramount);
            entity.setWithdrawAmount(wamount);
            resultList.add(entity);
            startTime = startTime.plusDays(1);
        }
        //
        //数据分页
        if(resultList != null && !resultList.isEmpty()) {
            Collections.reverse(resultList);  //倒序排列

            //查询是否分页
            if(baseParamBean.getPageType() == 2){
                int total = resultList.size();
                //计算
                int num = (pageNum-1)* pageSize;
                resultList = resultList.stream().skip(num).collect(Collectors.toList());
                List<MemberRechargeBean> pageList = resultList.stream().limit(pageSize).collect(Collectors.toList());
                return new DataTable<>(pageNum, pageSize, total, pageList);
            }else{
                return new DataTable<>(pageNum, pageSize, resultList.size(), resultList);
            }
        }else {
            return new DataTable<>(pageNum, pageSize, 0, new ArrayList<>());
        }
    }

    @Override
    public MemberRechargeBean findTotal(BaseParamBean baseParamBean) {
        MemberRechargeBean bean = new MemberRechargeBean();
        baseParamBean.setPageType(1);
        DataTable<MemberRechargeBean> dataList =  findList(baseParamBean);
        List<MemberRechargeBean> list = dataList.getDataList();

        BigDecimal rechargeAmount =BigDecimal.ZERO;
        BigDecimal withdrawAmount = BigDecimal.ZERO;
        if(list != null && !list.isEmpty()){
            for(MemberRechargeBean entity: list){
                rechargeAmount = rechargeAmount.add(entity.getRechargeAmount());
                withdrawAmount = withdrawAmount.add(entity.getWithdrawAmount());
            }
        }
        bean.setRechargeAmount(rechargeAmount);
        bean.setWithdrawAmount(withdrawAmount);
        return bean;
    }
}
