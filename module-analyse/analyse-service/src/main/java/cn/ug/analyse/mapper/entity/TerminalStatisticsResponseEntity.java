package cn.ug.analyse.mapper.entity;

import java.io.Serializable;

public class TerminalStatisticsResponseEntity implements Serializable {
    private String appSystem;
    private String phoneModel;
    private String appVersion;
    private Integer launchTimes;

    public String getAppSystem() {
        return appSystem;
    }

    public void setAppSystem(String appSystem) {
        this.appSystem = appSystem;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Integer getLaunchTimes() {
        return launchTimes;
    }

    public void setLaunchTimes(Integer launchTimes) {
        this.launchTimes = launchTimes;
    }
}
