package cn.ug.analyse.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberBalance extends BaseEntity implements Serializable{

    /** 日期 **/
    private String day;
    /** 会员id **/
    private String memberId;
    /** 余额 **/
    private BigDecimal amount;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
