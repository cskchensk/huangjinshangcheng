package cn.ug.analyse.mapper;

import cn.ug.analyse.bean.InvitationDetailBean;
import cn.ug.analyse.mapper.entity.InviteEntity;
import cn.ug.analyse.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 渠道邀请关系
 *
 * @author zhaohg
 * @date 2018/08/07.
 */
@Mapper
public interface InviteMapper {

    /**
     * 添加邀请绑定关系
     *
     * @param entity
     * @return
     */
    int insert(InviteEntity entity);

    /**
     * 判断绑定关系
     *
     * @param userId
     * @return
     */
    InviteEntity findByUserId(@Param("userId") String userId);

    /**
     * 查询渠道成员下的所有一二级用户
     *
     * @param staffId
     * @return
     */
    List<InviteEntity> findAllUserListByStaffId(@Param("staffId") Long staffId);

    /**
     * 渠道下的绑卡行数
     *
     * @param querySubmit
     * @return
     */
    int inviteBindCardCount(QuerySubmit querySubmit);

    /**
     * 渠道下的绑卡列表
     *
     * @param querySubmit
     * @return
     */
    List<InviteEntity> inviteBindCardList(QuerySubmit querySubmit);

    List<InvitationDetailBean> queryForDetailList(Map<String, Object> params);
    int queryForDetailCount(Map<String, Object> params);
}
