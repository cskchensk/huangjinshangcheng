package cn.ug.feign;

import cn.ug.pay.api.BillServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("PAY-SERVICE")
public interface BillService  extends BillServiceApi{
}
