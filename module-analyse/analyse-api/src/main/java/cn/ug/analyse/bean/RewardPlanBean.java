package cn.ug.analyse.bean;

import java.math.BigDecimal;

/**
 * 渠道奖励方案
 * @author zhaohg
 * @date 2018/08/06.
 */
public class RewardPlanBean {

    private Integer    index;
    private Integer    tradeLower;
    private Integer    tradeUpper;
    private BigDecimal reward;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getTradeLower() {
        return tradeLower;
    }

    public void setTradeLower(Integer tradeLower) {
        this.tradeLower = tradeLower;
    }

    public Integer getTradeUpper() {
        return tradeUpper;
    }

    public void setTradeUpper(Integer tradeUpper) {
        this.tradeUpper = tradeUpper;
    }

    public BigDecimal getReward() {
        return reward;
    }

    public void setReward(BigDecimal reward) {
        this.reward = reward;
    }
}
