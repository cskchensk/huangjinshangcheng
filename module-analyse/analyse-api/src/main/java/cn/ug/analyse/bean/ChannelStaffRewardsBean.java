package cn.ug.analyse.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ChannelStaffRewardsBean implements Serializable {
    private int staffId;
    private String staffName;
    private BigDecimal totalAmount;
    private BigDecimal sumOfInvitation;
    private BigDecimal sumOfTransaction;
    private BigDecimal firstTransactionAmount;
    private BigDecimal secondTransactionAmount;

    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public BigDecimal getFirstTransactionAmount() {
        return firstTransactionAmount;
    }

    public void setFirstTransactionAmount(BigDecimal firstTransactionAmount) {
        this.firstTransactionAmount = firstTransactionAmount;
    }

    public BigDecimal getSecondTransactionAmount() {
        return secondTransactionAmount;
    }

    public void setSecondTransactionAmount(BigDecimal secondTransactionAmount) {
        this.secondTransactionAmount = secondTransactionAmount;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getSumOfInvitation() {
        return sumOfInvitation;
    }

    public void setSumOfInvitation(BigDecimal sumOfInvitation) {
        this.sumOfInvitation = sumOfInvitation;
    }

    public BigDecimal getSumOfTransaction() {
        return sumOfTransaction;
    }

    public void setSumOfTransaction(BigDecimal sumOfTransaction) {
        this.sumOfTransaction = sumOfTransaction;
    }
}
