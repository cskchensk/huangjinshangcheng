package cn.ug.analyse.bean.request;

import java.io.Serializable;

/**
 * 用户画像
 */
public class MemberPortrayParamBean extends MemberAngParamBean implements Serializable{
    /** 性别 1：男 2：女 **/
    private Integer genderType;
    /** 来源 2:android  3:ios  **/
    //private Integer source;
    /** 年龄开始时间 **/
    private Integer startAge;
    /** 年龄结束时间 **/
    private Integer endAge;
    /** 类型 1：性别 2：年龄 3：来源  4.交易时间段 5.邀请比例**/
    private Integer type;
    /** 键 1：gender 2:age 3:source **/
    private String keywords;
    /** 可变参数 1：定期 2：活期 **/
    private String productType;
    /**渠道id**/
    private String channelId;

    /**时间**/
    private String time;
    /**
     * 1.主动用户
     * 2.被动用户
     */
    private Integer inviteType;

    public Integer getStartAge() {
        return startAge;
    }

    public void setStartAge(Integer startAge) {
        this.startAge = startAge;
    }

    public Integer getEndAge() {
        return endAge;
    }

    public void setEndAge(Integer endAge) {
        this.endAge = endAge;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getGenderType() {
        return genderType;
    }

    public void setGenderType(Integer genderType) {
        this.genderType = genderType;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getInviteType() {
        return inviteType;
    }

    public void setInviteType(Integer inviteType) {
        this.inviteType = inviteType;
    }
}
