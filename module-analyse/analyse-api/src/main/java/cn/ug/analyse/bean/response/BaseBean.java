package cn.ug.analyse.bean.response;

import java.math.BigDecimal;

/**
 * 基础变量
 */
public class BaseBean {

    /** 键 **/
    private String  keywords;
    /** 值 **/
    private BigDecimal total;

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
