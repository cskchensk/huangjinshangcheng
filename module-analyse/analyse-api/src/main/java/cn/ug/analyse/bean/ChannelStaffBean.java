package cn.ug.analyse.bean;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public class ChannelStaffBean {

    private Integer index;
    private String status;

    /**
     * 成员id
     */
    private Long    id;
    /**
     * 成员编号
     */
    private String  number;
    /**
     * 成员密码
     */
    private String  password;
    /**
     * 成员姓名
     */
    private String  realName;
    /**
     * 成员手机号
     */
    private String  mobile;
    /**
     * 邀请码
     */
    private String  inviteCode;
    /**
     * 启用禁用 0:禁用 1:启用
     */
    private Integer isEnable;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }




}
