package cn.ug.analyse.api;

import cn.ug.analyse.bean.response.ActiveMemberAnalyseBean;
import cn.ug.analyse.bean.response.MemberAnalyseBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@RequestMapping("memberAnalyse")
public interface MemberAnalyseServiceApi {

    @GetMapping(value = "/active/user")
    SerializeObject<List<ActiveMemberAnalyseBean>> listActiveUser(@RequestParam("dayType")int dayType,
                                                                  @RequestParam("startTime")String startTime,
                                                                  @RequestParam("endTime")String endTime,
                                                                  @RequestParam("channelIds")String[] channelIds);
    @GetMapping(value = "/new/user")
    SerializeObject<List<MemberAnalyseBean>> listNewUser(@RequestParam("startTime")String startTime,
                                                         @RequestParam("endTime")String endTime,
                                                         @RequestParam("channelIds")String[] channelIds);
}
