package cn.ug.analyse.bean;

import java.io.Serializable;

public class ChannelMemberBean implements Serializable {
    private String mobile;
    private String name;
    private String registerTime;
    private String bindedCardTime;
    private int index;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public String getBindedCardTime() {
        return bindedCardTime;
    }

    public void setBindedCardTime(String bindedCardTime) {
        this.bindedCardTime = bindedCardTime;
    }
}
