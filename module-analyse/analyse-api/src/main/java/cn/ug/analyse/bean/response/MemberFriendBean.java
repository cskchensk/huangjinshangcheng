package cn.ug.analyse.bean.response;

/**
 * @Author zhangweijie
 * @Date 2019/6/11 0011
 * @time 下午 16:31
 **/
public class MemberFriendBean {

    private String friendId;

    private int gender;

    private String memberId;

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
