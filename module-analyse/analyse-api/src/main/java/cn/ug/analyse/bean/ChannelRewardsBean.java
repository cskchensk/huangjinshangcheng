package cn.ug.analyse.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ChannelRewardsBean implements Serializable {
    private String rewardsMonth;
    private BigDecimal totalAmount;
    private BigDecimal rewardsOfInvitation;
    private BigDecimal rewardsOfTransaction;

    public String getRewardsMonth() {
        return rewardsMonth;
    }

    public void setRewardsMonth(String rewardsMonth) {
        this.rewardsMonth = rewardsMonth;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getRewardsOfInvitation() {
        return rewardsOfInvitation;
    }

    public void setRewardsOfInvitation(BigDecimal rewardsOfInvitation) {
        this.rewardsOfInvitation = rewardsOfInvitation;
    }

    public BigDecimal getRewardsOfTransaction() {
        return rewardsOfTransaction;
    }

    public void setRewardsOfTransaction(BigDecimal rewardsOfTransaction) {
        this.rewardsOfTransaction = rewardsOfTransaction;
    }
}
