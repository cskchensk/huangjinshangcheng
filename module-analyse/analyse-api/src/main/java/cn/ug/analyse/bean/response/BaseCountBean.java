package cn.ug.analyse.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 基础统计--每日
 */
public class BaseCountBean implements Serializable{

    /** 天 **/
    private String day;
    /** 金额 **/
    private BigDecimal amount;
    /** 日期数量 **/
    private Integer dayNumber;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(Integer dayNumber) {
        this.dayNumber = dayNumber;
    }
}
