package cn.ug.analyse.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberAnalyseBean implements Serializable {

    /** 渠道id **/
    private String channelId;
    /** 渠道名称  **/
    private String channelName;
    /** 注册会员数量 **/
    private int registerNumber;
    /** 绑卡会员数量 **/
    private int bindingBankCardNumber;
    /** 充值会员数量 **/
    private int rechargeNumber;
    /**  交易会员数量 **/
    private int tradeNumber;
    private int uv;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(int registerNumber) {
        this.registerNumber = registerNumber;
    }

    public int getBindingBankCardNumber() {
        return bindingBankCardNumber;
    }

    public void setBindingBankCardNumber(int bindingBankCardNumber) {
        this.bindingBankCardNumber = bindingBankCardNumber;
    }

    public int getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(int rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

    public int getTradeNumber() {
        return tradeNumber;
    }

    public void setTradeNumber(int tradeNumber) {
        this.tradeNumber = tradeNumber;
    }

    public int getUv() {
        return uv;
    }

    public void setUv(int uv) {
        this.uv = uv;
    }
}
