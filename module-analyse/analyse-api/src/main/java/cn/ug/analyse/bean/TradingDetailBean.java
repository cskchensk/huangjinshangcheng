package cn.ug.analyse.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradingDetailBean implements Serializable {
    private String tradeDate;
    private String mobile;
    private int inviteType;
    private BigDecimal annual;
    private BigDecimal actualAmount;
    private int investDay;

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getInviteType() {
        return inviteType;
    }

    public void setInviteType(int inviteType) {
        this.inviteType = inviteType;
    }

    public BigDecimal getAnnual() {
        return annual;
    }

    public void setAnnual(BigDecimal annual) {
        this.annual = annual;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public int getInvestDay() {
        return investDay;
    }

    public void setInvestDay(int investDay) {
        this.investDay = investDay;
    }
}
