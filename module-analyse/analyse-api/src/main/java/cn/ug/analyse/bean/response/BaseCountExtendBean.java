package cn.ug.analyse.bean.response;

/**
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 15:22
 **/
public class BaseCountExtendBean extends BaseCountBean{

    /**
     * 克重
     */
    private Integer totalGram;

    /**
     * 交易数量
     */
    private Integer traderNum;

    public Integer getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(Integer totalGram) {
        this.totalGram = totalGram;
    }

    public Integer getTraderNum() {
        return traderNum;
    }

    public void setTraderNum(Integer traderNum) {
        this.traderNum = traderNum;
    }
}
