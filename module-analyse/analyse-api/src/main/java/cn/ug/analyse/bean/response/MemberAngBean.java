package cn.ug.analyse.bean.response;

import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/6/11 0011
 * @time 下午 13:56
 **/
public class MemberAngBean {

    private BigDecimal age;

    private int gender;

    public BigDecimal getAge() {
        return age;
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}
