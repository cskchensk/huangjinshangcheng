package cn.ug.analyse.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 渠道业绩
 *
 * @author zhaohg
 * @date 2018/08/09.
 */
public class AssessBean {

    private Integer index;

    /**
     * 渠道 人员公用
     */
    private String  number;
    private String  name;
    private Integer planType;
    private String mobile;
    private String realName;

    private Long       channelId;
    private Long       staffId;
    /**
     * 一级绑卡
     */
    private Integer    firstBindCardNum;
    /**
     * 一级绑卡奖励
     */
    private BigDecimal firstBindCardAmount;
    /**
     * 二级绑卡
     */
    private Integer    secondBindCardNum;
    /**
     * 二级绑卡奖励
     */
    private BigDecimal secondBindCardAmount;
    /**
     * 一级年化交易总量
     */
    private BigDecimal firstTradeNum;
    /**
     * 年化交易总量是 交易额*交易期限/365 的汇总值，例如投资10000元30天的标，计算方式为10000*30/365=821.92；
     */
    private BigDecimal firstAnnual;
    /**
     * 二级年化交易总量
     */
    private BigDecimal secondTradeNum;
    /**
     * 年化交易总量是 交易额*交易期限/365 的汇总值，例如投资10000元30天的标，计算方式为10000*30/365=821.92；
     */
    private BigDecimal secondAnnual;
    /**
     * 交易日期
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date       tradeDate;

    private String planName;
    private String time;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getFirstBindCardNum() {
        return firstBindCardNum;
    }

    public void setFirstBindCardNum(Integer firstBindCardNum) {
        this.firstBindCardNum = firstBindCardNum;
    }

    public Integer getSecondBindCardNum() {
        return secondBindCardNum;
    }

    public void setSecondBindCardNum(Integer secondBindCardNum) {
        this.secondBindCardNum = secondBindCardNum;
    }

    public BigDecimal getFirstTradeNum() {
        return firstTradeNum;
    }

    public void setFirstTradeNum(BigDecimal firstTradeNum) {
        this.firstTradeNum = firstTradeNum;
    }

    public BigDecimal getFirstAnnual() {
        return firstAnnual;
    }

    public void setFirstAnnual(BigDecimal firstAnnual) {
        this.firstAnnual = firstAnnual;
    }

    public BigDecimal getSecondTradeNum() {
        return secondTradeNum;
    }

    public void setSecondTradeNum(BigDecimal secondTradeNum) {
        this.secondTradeNum = secondTradeNum;
    }

    public BigDecimal getSecondAnnual() {
        return secondAnnual;
    }

    public void setSecondAnnual(BigDecimal secondAnnual) {
        this.secondAnnual = secondAnnual;
    }

    public BigDecimal getFirstBindCardAmount() {
        return firstBindCardAmount;
    }

    public void setFirstBindCardAmount(BigDecimal firstBindCardAmount) {
        this.firstBindCardAmount = firstBindCardAmount;
    }

    public BigDecimal getSecondBindCardAmount() {
        return secondBindCardAmount;
    }

    public void setSecondBindCardAmount(BigDecimal secondBindCardAmount) {
        this.secondBindCardAmount = secondBindCardAmount;
    }

    public Date getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPlanType() {
        return planType;
    }

    public void setPlanType(Integer planType) {
        this.planType = planType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
