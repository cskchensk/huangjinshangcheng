package cn.ug.analyse.bean;

import java.math.BigDecimal;

/**
 * 交易量 年化量
 * @author zhaohg
 * @date 2018/08/10.
 */
public class GrossBean {

    private BigDecimal amount;
    private BigDecimal annual;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAnnual() {
        return annual;
    }

    public void setAnnual(BigDecimal annual) {
        this.annual = annual;
    }
}
