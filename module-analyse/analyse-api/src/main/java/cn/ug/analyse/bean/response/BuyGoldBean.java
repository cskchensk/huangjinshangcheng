package cn.ug.analyse.bean.response;

import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 20:09
 **/
public class BuyGoldBean {

    /** 天 **/
    private String day;

    //首投人数
    private Integer firstBuyNum;
    //复投人数
    private Integer againBuyNum;
    //买金人数
    private Integer buyGoldNum;
    //买金克重（g）
    private Integer buyGoldGram;
    //买金金额（元）
    private BigDecimal buyGoldAmount;
    //人均购买金额
    private BigDecimal averageBuyGoldAmount;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getFirstBuyNum() {
        return firstBuyNum;
    }

    public void setFirstBuyNum(Integer firstBuyNum) {
        this.firstBuyNum = firstBuyNum;
    }

    public Integer getAgainBuyNum() {
        return againBuyNum;
    }

    public void setAgainBuyNum(Integer againBuyNum) {
        this.againBuyNum = againBuyNum;
    }

    public Integer getBuyGoldNum() {
        return buyGoldNum;
    }

    public void setBuyGoldNum(Integer buyGoldNum) {
        this.buyGoldNum = buyGoldNum;
    }

    public Integer getBuyGoldGram() {
        return buyGoldGram;
    }

    public void setBuyGoldGram(Integer buyGoldGram) {
        this.buyGoldGram = buyGoldGram;
    }

    public BigDecimal getBuyGoldAmount() {
        return buyGoldAmount;
    }

    public void setBuyGoldAmount(BigDecimal buyGoldAmount) {
        this.buyGoldAmount = buyGoldAmount;
    }

    public BigDecimal getAverageBuyGoldAmount() {
        return averageBuyGoldAmount;
    }

    public void setAverageBuyGoldAmount(BigDecimal averageBuyGoldAmount) {
        this.averageBuyGoldAmount = averageBuyGoldAmount;
    }
}
