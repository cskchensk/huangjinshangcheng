package cn.ug.analyse.bean.response;

import java.util.List;

/**
 * 交易时间段数据展示
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 上午 8:59
 **/
public class MemberTraderRonseespBean {

    private List<Integer> manList;

    private List<Integer> woManList;


    public List<Integer> getManList() {
        return manList;
    }

    public void setManList(List<Integer> manList) {
        this.manList = manList;
    }

    public List<Integer> getWoManList() {
        return woManList;
    }

    public void setWoManList(List<Integer> woManList) {
        this.woManList = woManList;
    }
}
