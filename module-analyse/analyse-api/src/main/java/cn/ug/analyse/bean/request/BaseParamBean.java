package cn.ug.analyse.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class BaseParamBean  extends Page implements Serializable{

    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 是否分页 1:不分页 2：分页 **/
    private Integer pageType;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getPageType() {
        return pageType;
    }

    public void setPageType(Integer pageType) {
        this.pageType = pageType;
    }
}
