package cn.ug.analyse.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

/**
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 13:39
 **/
public class OperationParamBean extends Page implements Serializable {
    /** 日期类型 1：日 2：周 3：月 **/
    private Integer dayType;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;

    public Integer getDayType() {
        return dayType;
    }

    public void setDayType(Integer dayType) {
        this.dayType = dayType;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
