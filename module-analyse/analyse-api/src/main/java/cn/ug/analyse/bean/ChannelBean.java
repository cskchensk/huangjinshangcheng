package cn.ug.analyse.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/08/06.
 */
public class ChannelBean {

    private Integer index;
    private String status;

    /**
     * id
     */
    private Long       id;
    /**
     * 渠道名称
     */
    private String     name;
    /**
     * 渠道编号
     */
    private String     number;
    /**
     * 奖励方案 1:阶梯交易额 2:交易周期 3:交易频数
     */
    private Integer    planType;
    /**
     * 成员数
     */
    private Integer staffCount;
    /**
     * 周期
     */
    private Integer    period;
    /**
     * 二级奖励比例
     */
    private BigDecimal secondReward;
    /**
     * 奖励方案json
     */
    private String     planStep;
    /**
     * 一级邀请奖励
     */
    private BigDecimal oneInviteReward;
    /**
     * 二级邀请奖励
     */
    private BigDecimal twoInviteReward;
    /**
     * 有效开始时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date       enableDate;
    /**
     * 无效开始时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date       expireDate;
    /**
     * 启用禁用 0:禁用 1:启用
     */
    private Integer    isEnable;

    private List<RewardPlanBean> planList;
    private ChannelStaffBean staff;
    
    private String planName;

    private String enable;
    private String expire;

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPlanType() {
        return planType;
    }

    public void setPlanType(Integer planType) {
        this.planType = planType;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public BigDecimal getSecondReward() {
        return secondReward;
    }

    public void setSecondReward(BigDecimal secondReward) {
        this.secondReward = secondReward;
    }

    public String getPlanStep() {
        return planStep;
    }

    public void setPlanStep(String planStep) {
        this.planStep = planStep;
    }

    public BigDecimal getOneInviteReward() {
        return oneInviteReward;
    }

    public void setOneInviteReward(BigDecimal oneInviteReward) {
        this.oneInviteReward = oneInviteReward;
    }

    public BigDecimal getTwoInviteReward() {
        return twoInviteReward;
    }

    public void setTwoInviteReward(BigDecimal twoInviteReward) {
        this.twoInviteReward = twoInviteReward;
    }

    public Date getEnableDate() {
        return enableDate;
    }

    public void setEnableDate(Date enableDate) {
        this.enableDate = enableDate;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public Integer getStaffCount() {
        return staffCount;
    }

    public void setStaffCount(Integer staffCount) {
        this.staffCount = staffCount;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    public ChannelStaffBean getStaff() {
        return staff;
    }

    public void setStaff(ChannelStaffBean staff) {
        this.staff = staff;
    }

    public List<RewardPlanBean> getPlanList() {
        return planList;
    }

    public void setPlanList(List<RewardPlanBean> planList) {
        this.planList = planList;
    }
}
