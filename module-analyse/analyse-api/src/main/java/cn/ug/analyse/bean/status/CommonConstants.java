package cn.ug.analyse.bean.status;

/**
 * 会员统计
 * @author ywl
 */
public class CommonConstants {

	    //会员统计类型
		public static enum MemberAnalyseType{
			REGISTER("注册", 1),  BINDING_BANKCARD("绑定银行卡", 2), RECHARGE("充值",3), TRADE("交易", 4);

			private String name;
			private int index;

			private MemberAnalyseType(String name, int index)
			{
				this.name = name;
				this.index = index;
			}

			public String getName()
			{
				return this.name;
			}

			public int getIndex()
			{
				return this.index;
			}

			public static String getName(int index)
			{
				for (MemberAnalyseType c : MemberAnalyseType.values()) {
					if (c.getIndex() == index) {
						return c.name;
					}
				}
				return null;
			}
		}

	public static final class MemberPortrayType{
		public static final String GENDER = "gender";
		public static final String AGE = "age";
		public static final String SOURCE = "source";
	}

}
