package cn.ug.analyse.bean.request;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author zhangweijie
 * @Date 2019/6/11 0011
 * @time 下午 18:14
 **/
public class MemberAngParamBean {

   private String channelId;

    /**
     * 1.交易用户 2.回租用户 3.卖金用户
     */
   private Integer userType;

    /**
     * 指标类型--交易额度  查询条件选择上了就传1
     */
   private Integer traderTypeEd;

    /**
     * 指标类型--交易频次  查询条件选择上了就传1
     */
    private Integer traderTypePl;

    /**
     * 指标类型--回租时长  查询条件选择上了就传1
     */
    private Integer traderTypeHz;

    /**
     * 1.金额
     * 2.克重
     */
   private Integer traderType;

    /**
     * 最小交易金额
     */
   private BigDecimal traderMoneyMin;


    /**
     *最大交易金额
     */
    private BigDecimal traderMoneyMax;


    /**
     * 频率最小值
     */
    private Integer numMin;

    /**
     * 频率最大值
     */
    private Integer numMax;

    /**
     * 回租时长
     */
    private Integer leasebackDay;

    /**
     * 标识符
     * 1.大于  2.小于 3.等于 4.大于等于 5.小于等于
     */
    private Integer character;

    /**
     * 非前端传值  ps:userType=2  traderTypeHz=1 后台自己查询生成
     */
    private List<String> memberIds;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getTraderTypeEd() {
        return traderTypeEd;
    }

    public void setTraderTypeEd(Integer traderTypeEd) {
        this.traderTypeEd = traderTypeEd;
    }

    public Integer getTraderTypePl() {
        return traderTypePl;
    }

    public void setTraderTypePl(Integer traderTypePl) {
        this.traderTypePl = traderTypePl;
    }

    public Integer getTraderTypeHz() {
        return traderTypeHz;
    }

    public void setTraderTypeHz(Integer traderTypeHz) {
        this.traderTypeHz = traderTypeHz;
    }

    public Integer getTraderType() {
        return traderType;
    }

    public void setTraderType(Integer traderType) {
        this.traderType = traderType;
    }

    public BigDecimal getTraderMoneyMin() {
        return traderMoneyMin;
    }

    public void setTraderMoneyMin(BigDecimal traderMoneyMin) {
        this.traderMoneyMin = traderMoneyMin;
    }

    public BigDecimal getTraderMoneyMax() {
        return traderMoneyMax;
    }

    public void setTraderMoneyMax(BigDecimal traderMoneyMax) {
        this.traderMoneyMax = traderMoneyMax;
    }

    public Integer getNumMin() {
        return numMin;
    }

    public void setNumMin(Integer numMin) {
        this.numMin = numMin;
    }

    public Integer getNumMax() {
        return numMax;
    }

    public void setNumMax(Integer numMax) {
        this.numMax = numMax;
    }

    public Integer getLeasebackDay() {
        return leasebackDay;
    }

    public void setLeasebackDay(Integer leasebackDay) {
        this.leasebackDay = leasebackDay;
    }

    public Integer getCharacter() {
        return character;
    }

    public void setCharacter(Integer character) {
        this.character = character;
    }

    public List<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(List<String> memberIds) {
        this.memberIds = memberIds;
    }
}
