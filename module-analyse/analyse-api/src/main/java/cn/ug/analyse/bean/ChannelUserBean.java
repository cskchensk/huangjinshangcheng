package cn.ug.analyse.bean;

import java.io.Serializable;

public class ChannelUserBean implements Serializable {

    private Long id;
    /**商户id**/
    private Long channelId;
    /**名称**/
    private String name;
    /**类型 0渠道商 1渠道成员**/
    private Integer type;
    /**密码状态  0初始 1非初始**/
    private Integer passWrodStatus;
    /**渠道编号**/
    private String  channelNumber;
    /**邀请码**/
    private String inviteCode;
    /**手机号码**/
    private String mobile;
    /**登陆token**/
    private String accessToken;

    private String addTime;
    /** 渠道上绑定的id*/
    private Long staffId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPassWrodStatus() {
        return passWrodStatus;
    }

    public void setPassWrodStatus(Integer passWrodStatus) {
        this.passWrodStatus = passWrodStatus;
    }

    public String getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(String channelNumber) {
        this.channelNumber = channelNumber;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Long getStaffId() {
        return staffId;
    }

    public void setStaffId(Long staffId) {
        this.staffId = staffId;
    }
}
