package cn.ug.analyse.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员数据转化率百分比
 * @author ywl
 */
public class MemberAnalysePerBean implements Serializable{

    /** 绑卡转化率  绑卡会员/注册会员 **/
    private String bindingBankCardPer;
    /** 充值转化率 充值/绑卡 **/
    private String rechargePer;
    /** 交易转化率 **/
    private String tradePer;

    public String getBindingBankCardPer() {
        return bindingBankCardPer;
    }

    public void setBindingBankCardPer(String bindingBankCardPer) {
        this.bindingBankCardPer = bindingBankCardPer;
    }

    public String getRechargePer() {
        return rechargePer;
    }

    public void setRechargePer(String rechargePer) {
        this.rechargePer = rechargePer;
    }

    public String getTradePer() {
        return tradePer;
    }

    public void setTradePer(String tradePer) {
        this.tradePer = tradePer;
    }
}
