package cn.ug.analyse.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InvitationDetailBean implements Serializable {
    private String tradeDate;
    private String mobile;
    private int inviteType;
    private BigDecimal rewards;

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getInviteType() {
        return inviteType;
    }

    public void setInviteType(int inviteType) {
        this.inviteType = inviteType;
    }

    public BigDecimal getRewards() {
        return rewards;
    }

    public void setRewards(BigDecimal rewards) {
        this.rewards = rewards;
    }
}
