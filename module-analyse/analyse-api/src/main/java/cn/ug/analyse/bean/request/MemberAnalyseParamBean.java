package cn.ug.analyse.bean.request;

import cn.ug.bean.base.Page;
import cn.ug.util.Common;

import java.io.Serializable;

public class MemberAnalyseParamBean extends Page implements Serializable{

    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 渠道id **/
    private String channelId;
    /** 渠道数组 **/
    private String channelIds [];

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getChannelId() {
        return Common.converToString(getChannelIds());
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String[] getChannelIds() {
        return channelIds;
    }

    public void setChannelIds(String[] channelIds) {
        this.channelIds = channelIds;
    }
}
