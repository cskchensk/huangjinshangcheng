package cn.ug.analyse.bean.response;

/**
 * 渠道分析
 */
public class ChannelAnalyseBean {

    /** 渠道id **/
    private String channelId;
    /** 渠道名称 **/
    private String channelName;
    /** 类型 1:注册 2:绑卡 3:充值 4:交易 **/
    private Integer type;
    /** 每个渠道数量 **/
    private Integer number;
    private int uv;

    public int getUv() {
        return uv;
    }

    public void setUv(int uv) {
        this.uv = uv;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }
}
