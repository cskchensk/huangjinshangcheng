package cn.ug.analyse.bean;

import java.math.BigDecimal;

/**
 * @author zhaohg
 * @date 2018/08/23.
 */
public class ChannelSettleBean {

    private Integer index;

    private Long       id;
    private String     number;
    private Integer    planType;
    private String     name;
    private BigDecimal totalReward;
    private BigDecimal waitReward;
    private BigDecimal settleReward;

    private String planName;

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPlanType() {
        return planType;
    }

    public void setPlanType(Integer planType) {
        this.planType = planType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalReward() {
        return totalReward;
    }

    public void setTotalReward(BigDecimal totalReward) {
        this.totalReward = totalReward;
    }

    public BigDecimal getWaitReward() {
        return waitReward;
    }

    public void setWaitReward(BigDecimal waitReward) {
        this.waitReward = waitReward;
    }

    public BigDecimal getSettleReward() {
        return settleReward;
    }

    public void setSettleReward(BigDecimal settleReward) {
        this.settleReward = settleReward;
    }
}
