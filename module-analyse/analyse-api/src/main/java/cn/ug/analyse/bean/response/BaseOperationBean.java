package cn.ug.analyse.bean.response;

import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/6/13 0013
 * @time 下午 18:11
 **/
public class BaseOperationBean {
    /**
     * 1.注册人数/绑卡人数
     * 2.卖金人数/卖金克重
     * 3.用户账户总余额
     * 4.用户提现金额
     * 5.黄金红包人数
     */
    private Integer type;
    //每日注册人数
    private Integer registerNum = 0;
    //每日绑卡人数
    private Integer bdCardNum = 0;
    //每日卖金人数
    private Integer soldGoldNum = 0;
    //每日卖金克重
    private Integer soldGoldGram = 0;
    //每日用户账户总余额
    private BigDecimal balanceAmount = new BigDecimal(0);
    //每日用户提现金额
    private BigDecimal withdrawAmount = new BigDecimal(0);
    //每日使用黄金红包人数
    private Integer couponNum = 0;
    //时间
    private String day;

    /** 日期数量 **/
    private Integer dayNumber;

    /*************组装****************/
    //每日首投人数
    private Integer firstBuyNum = 0;
    //每日复投人数
    private Integer againBuyNum = 0;
    //每日买金人数
    private Integer buyGoldNum  = 0;
    //每日买金克重（g）
    private Integer buyGoldGram = 0;
    //每日买金金额（元）
    private BigDecimal buyGoldAmount = new BigDecimal(0);
    //每日人均购买金额
    private BigDecimal averageBuyGoldAmount = new BigDecimal(0);;

    private int index;

    public Integer getRegisterNum() {
        return registerNum;
    }

    public void setRegisterNum(Integer registerNum) {
        this.registerNum = registerNum;
    }

    public Integer getBdCardNum() {
        return bdCardNum;
    }

    public void setBdCardNum(Integer bdCardNum) {
        this.bdCardNum = bdCardNum;
    }

    public Integer getSoldGoldNum() {
        return soldGoldNum;
    }

    public void setSoldGoldNum(Integer soldGoldNum) {
        this.soldGoldNum = soldGoldNum;
    }

    public Integer getSoldGoldGram() {
        return soldGoldGram;
    }

    public void setSoldGoldGram(Integer soldGoldGram) {
        this.soldGoldGram = soldGoldGram;
    }

    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public Integer getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(Integer couponNum) {
        this.couponNum = couponNum;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getFirstBuyNum() {
        return firstBuyNum;
    }

    public void setFirstBuyNum(Integer firstBuyNum) {
        this.firstBuyNum = firstBuyNum;
    }

    public Integer getAgainBuyNum() {
        return againBuyNum;
    }

    public void setAgainBuyNum(Integer againBuyNum) {
        this.againBuyNum = againBuyNum;
    }

    public Integer getBuyGoldNum() {
        return buyGoldNum;
    }

    public void setBuyGoldNum(Integer buyGoldNum) {
        this.buyGoldNum = buyGoldNum;
    }

    public Integer getBuyGoldGram() {
        return buyGoldGram;
    }

    public void setBuyGoldGram(Integer buyGoldGram) {
        this.buyGoldGram = buyGoldGram;
    }

    public BigDecimal getBuyGoldAmount() {
        return buyGoldAmount;
    }

    public void setBuyGoldAmount(BigDecimal buyGoldAmount) {
        this.buyGoldAmount = buyGoldAmount;
    }

    public BigDecimal getAverageBuyGoldAmount() {
        return averageBuyGoldAmount;
    }

    public void setAverageBuyGoldAmount(BigDecimal averageBuyGoldAmount) {
        this.averageBuyGoldAmount = averageBuyGoldAmount;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(Integer dayNumber) {
        this.dayNumber = dayNumber;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
