package cn.ug.analyse.bean.response;

import java.io.Serializable;

public class ActiveMemberAnalyseBean implements Serializable {
    /** 日期 **/
    private String day;
    /* 买金会员数量* **/
    private Integer buyNumber;
    /** 卖金会员数量 **/
    private Integer sellerNumber;
    /** 数量 **/
    private Integer number;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getBuyNumber() {
        return buyNumber;
    }

    public void setBuyNumber(Integer buyNumber) {
        this.buyNumber = buyNumber;
    }

    public Integer getSellerNumber() {
        return sellerNumber;
    }

    public void setSellerNumber(Integer sellerNumber) {
        this.sellerNumber = sellerNumber;
    }
}
