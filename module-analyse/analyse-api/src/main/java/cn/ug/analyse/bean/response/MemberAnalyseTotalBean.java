package cn.ug.analyse.bean.response;

import java.io.Serializable;

public class MemberAnalyseTotalBean implements Serializable{
    /** 注册会员数量 **/
    private int registerNumber;
    /** 绑卡会员数量 **/
    private int bindingBankCardNumber;
    /** 充值会员数量 **/
    private int rechargeNumber;
    /**  交易会员数量 **/
    private int tradeNumber;

    public int getRegisterNumber() {
        return registerNumber;
    }

    public void setRegisterNumber(int registerNumber) {
        this.registerNumber = registerNumber;
    }

    public int getBindingBankCardNumber() {
        return bindingBankCardNumber;
    }

    public void setBindingBankCardNumber(int bindingBankCardNumber) {
        this.bindingBankCardNumber = bindingBankCardNumber;
    }

    public int getRechargeNumber() {
        return rechargeNumber;
    }

    public void setRechargeNumber(int rechargeNumber) {
        this.rechargeNumber = rechargeNumber;
    }

    public int getTradeNumber() {
        return tradeNumber;
    }

    public void setTradeNumber(int tradeNumber) {
        this.tradeNumber = tradeNumber;
    }
}
