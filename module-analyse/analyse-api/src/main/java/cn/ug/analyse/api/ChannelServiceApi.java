package cn.ug.analyse.api;

import cn.ug.analyse.bean.ChannelStaffBean;
import cn.ug.analyse.bean.ChannelUserBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("/channel")
public interface ChannelServiceApi {

    /**
     * 校验渠道登陆信息
     * @param channelName   登陆账号
     * @param password      登陆密码
     * @param type          0渠道商  1渠道成员
     * @return
     */
    @RequestMapping(value = "/verifyLogin", method = GET)
    SerializeObject<ChannelUserBean> verifyLogin(@RequestParam("channelName")String channelName,
                                                 @RequestParam("password")String password, @RequestParam("type")Integer type);

    /**
     * 根据id查询渠道商
     * @param id
     * @return
     */
    @RequestMapping(value = "/findById", method = GET)
    SerializeObject findById(@RequestParam("id")Long id);

    /**
     * 修改密码
     * @param id
     * @param password
     * @param type
     * @return
     */
    @PostMapping(value = "/updatePassword")
    SerializeObject updatePassword(@RequestParam("id")Long id,
                                   @RequestParam("password")String password,
                                   @RequestParam("type")Integer type,
                                   @RequestParam("mobile")String mobile);


    @RequestMapping(value = "/findChannelStaffByMobile", method = GET)
    public SerializeObject<ChannelStaffBean> findChannelStaffByMobile(@RequestParam("mobile")String mobile);
}
