package cn.ug.analyse.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class StaffRewardsBean implements Serializable {
    private String staffName;
    private BigDecimal totalAmount;
    private BigDecimal lastMonthAmount;
    private BigDecimal totalRewardsOfInvitation;
    private BigDecimal firstRewardsOfInvitation;
    private BigDecimal secondRewardsOfInvitation;
    private BigDecimal totalRewardsOfTransaction;
    private BigDecimal firstRewardsOfTransaction;
    private BigDecimal secondRewardsOfTransaction;
    private String rewardsMonth;

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getRewardsMonth() {
        return rewardsMonth;
    }

    public void setRewardsMonth(String rewardsMonth) {
        this.rewardsMonth = rewardsMonth;
    }

    public BigDecimal getLastMonthAmount() {
        return lastMonthAmount;
    }

    public void setLastMonthAmount(BigDecimal lastMonthAmount) {
        this.lastMonthAmount = lastMonthAmount;
    }

    public BigDecimal getTotalRewardsOfInvitation() {
        return totalRewardsOfInvitation;
    }

    public void setTotalRewardsOfInvitation(BigDecimal totalRewardsOfInvitation) {
        this.totalRewardsOfInvitation = totalRewardsOfInvitation;
    }

    public BigDecimal getFirstRewardsOfInvitation() {
        return firstRewardsOfInvitation;
    }

    public void setFirstRewardsOfInvitation(BigDecimal firstRewardsOfInvitation) {
        this.firstRewardsOfInvitation = firstRewardsOfInvitation;
    }

    public BigDecimal getSecondRewardsOfInvitation() {
        return secondRewardsOfInvitation;
    }

    public void setSecondRewardsOfInvitation(BigDecimal secondRewardsOfInvitation) {
        this.secondRewardsOfInvitation = secondRewardsOfInvitation;
    }

    public BigDecimal getTotalRewardsOfTransaction() {
        return totalRewardsOfTransaction;
    }

    public void setTotalRewardsOfTransaction(BigDecimal totalRewardsOfTransaction) {
        this.totalRewardsOfTransaction = totalRewardsOfTransaction;
    }

    public BigDecimal getFirstRewardsOfTransaction() {
        return firstRewardsOfTransaction;
    }

    public void setFirstRewardsOfTransaction(BigDecimal firstRewardsOfTransaction) {
        this.firstRewardsOfTransaction = firstRewardsOfTransaction;
    }

    public BigDecimal getSecondRewardsOfTransaction() {
        return secondRewardsOfTransaction;
    }

    public void setSecondRewardsOfTransaction(BigDecimal secondRewardsOfTransaction) {
        this.secondRewardsOfTransaction = secondRewardsOfTransaction;
    }
}
