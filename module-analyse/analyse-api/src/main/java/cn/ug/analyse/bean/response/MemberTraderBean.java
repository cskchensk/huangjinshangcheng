package cn.ug.analyse.bean.response;

/**
 * @Author zhangweijie
 * @Date 2019/6/12 0012
 * @time 下午 19:30
 **/
public class MemberTraderBean {
    //交易时间
    private String traderDate;
    //交易次数
    private Integer traderNum;
    //性别
    private Integer gender;

    public String getTraderDate() {
        return traderDate;
    }

    public void setTraderDate(String traderDate) {
        this.traderDate = traderDate;
    }

    public Integer getTraderNum() {
        return traderNum;
    }

    public void setTraderNum(Integer traderNum) {
        this.traderNum = traderNum;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }
}
