package cn.ug.analyse.bean.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberProtrayBean implements Serializable {

    /** 会员数量 **/
    private BigDecimal number;
    /**  账户总额 **/
    private BigDecimal accountAmount;
    /** 存量总克重 **/
    private BigDecimal gramTotal;
    /**
     * 人均购买金额
     * 分为交易用户
     * 回租用户
     * 卖金用户
     *
     * 通用于以上三种类型
     */
    private BigDecimal averageBuyAmount;
    /** 人均持有待处理提单克重* **/
    private BigDecimal averageCurrentGram;
    /** 人均持有回租中提单克重 **/
    private BigDecimal averageRegularGram;
    /** 百分比 **/
    private BigDecimal per;
    private int age;
    private int gender;
    private int source;

    /***************新增字段**************************/
    /**
     * 交易频次
     * 通用于交易用户 回租用户 卖金用户三种类型
     */
    private Integer traderNum;

    /***************以下字段后台自己方便统计时设置的  不返回给前端，响应前端时统一转换成 averageBuyAmount和traderNum***********************/
    /**
     * 回租用户 人均购买金额
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal hzAverageBuyAmount;

    /**
     * 回租用户 人均交易频次
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer hzTraderNum;

    /**
     * 卖金用户 人均购买金额
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal mjAverageBuyAmount;

    /**
     * 卖金用户 人均交易频次
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer mjTraderNum;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public BigDecimal getNumber() {
        return number;
    }

    public void setNumber(BigDecimal number) {
        this.number = number;
    }

    public BigDecimal getAccountAmount() {
        return accountAmount;
    }

    public void setAccountAmount(BigDecimal accountAmount) {
        this.accountAmount = accountAmount;
    }

    public BigDecimal getGramTotal() {
        return gramTotal;
    }

    public void setGramTotal(BigDecimal gramTotal) {
        this.gramTotal = gramTotal;
    }

    public BigDecimal getAverageBuyAmount() {
        return averageBuyAmount;
    }

    public void setAverageBuyAmount(BigDecimal averageBuyAmount) {
        this.averageBuyAmount = averageBuyAmount;
    }

    public BigDecimal getAverageCurrentGram() {
        return averageCurrentGram;
    }

    public void setAverageCurrentGram(BigDecimal averageCurrentGram) {
        this.averageCurrentGram = averageCurrentGram;
    }

    public BigDecimal getAverageRegularGram() {
        return averageRegularGram;
    }

    public void setAverageRegularGram(BigDecimal averageRegularGram) {
        this.averageRegularGram = averageRegularGram;
    }

    public BigDecimal getPer() {
        return per;
    }

    public void setPer(BigDecimal per) {
        this.per = per;
    }

    public Integer getTraderNum() {
        return traderNum;
    }

    public void setTraderNum(Integer traderNum) {
        this.traderNum = traderNum;
    }

    public BigDecimal getHzAverageBuyAmount() {
        return hzAverageBuyAmount;
    }

    public void setHzAverageBuyAmount(BigDecimal hzAverageBuyAmount) {
        this.hzAverageBuyAmount = hzAverageBuyAmount;
    }

    public Integer getHzTraderNum() {
        return hzTraderNum;
    }

    public void setHzTraderNum(Integer hzTraderNum) {
        this.hzTraderNum = hzTraderNum;
    }

    public BigDecimal getMjAverageBuyAmount() {
        return mjAverageBuyAmount;
    }

    public void setMjAverageBuyAmount(BigDecimal mjAverageBuyAmount) {
        this.mjAverageBuyAmount = mjAverageBuyAmount;
    }

    public Integer getMjTraderNum() {
        return mjTraderNum;
    }

    public void setMjTraderNum(Integer mjTraderNum) {
        this.mjTraderNum = mjTraderNum;
    }
}
