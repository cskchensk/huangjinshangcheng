package cn.ug.analyse.bean.response;

/**
 * @Author zhangweijie
 * @Date 2019/6/11 0011
 * @time 下午 14:33
 **/
public class MemberRatioBean {
    //男性比例
    private String maxRatio;
    //女性比例
    private String womanRatio;

    public String getMaxRatio() {
        return maxRatio;
    }

    public void setMaxRatio(String maxRatio) {
        this.maxRatio = maxRatio;
    }

    public String getWomanRatio() {
        return womanRatio;
    }

    public void setWomanRatio(String womanRatio) {
        this.womanRatio = womanRatio;
    }
}
