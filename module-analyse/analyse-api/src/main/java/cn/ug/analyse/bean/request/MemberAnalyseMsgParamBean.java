package cn.ug.analyse.bean.request;

import org.springframework.context.ApplicationEvent;

import java.io.Serializable;

/**
 * 会员数据事件参数
 * @author  ywl
 * @date 2018-05-14
 */
public class MemberAnalyseMsgParamBean implements Serializable {

    /** 会员id **/
    private String memberId;
    /** 类型 1:注册 2:绑卡 3:充值 4:交易 **/
    private Integer  type;
    /** 会员渠道id **/
    private String channelId;


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }
}
