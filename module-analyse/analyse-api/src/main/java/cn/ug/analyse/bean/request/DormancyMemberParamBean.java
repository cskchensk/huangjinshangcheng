package cn.ug.analyse.bean.request;

import cn.ug.util.Common;

/**
 * 休眠用户
 * @author ywl
 * @date 2018-05-18
 */
public class DormancyMemberParamBean {

    /** 渠道id **/
    private String channelId;
    /** 绑卡天数 **/
    private Integer bindingBankCardDay;
    /** 交易天数 **/
    private Integer tradeDay;
    /** 渠道id **/
    private String channelIds [];

    public String getChannelId() {
        return Common.converToString(getChannelIds());
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getBindingBankCardDay() {
        return bindingBankCardDay;
    }

    public void setBindingBankCardDay(Integer bindingBankCardDay) {
        this.bindingBankCardDay = bindingBankCardDay;
    }

    public Integer getTradeDay() {
        return tradeDay;
    }

    public void setTradeDay(Integer tradeDay) {
        this.tradeDay = tradeDay;
    }

    public String[] getChannelIds() {
        return channelIds;
    }

    public void setChannelIds(String[] channelIds) {
        this.channelIds = channelIds;
    }
}
