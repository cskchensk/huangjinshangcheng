package cn.ug.analyse.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户充值提现金额
 */
public class MemberRechargeBean implements Serializable{

    /** 天 **/
    private String day;
    /** 充值金额 **/
    private BigDecimal rechargeAmount;
    /** 提现金额 **/
    private BigDecimal withdrawAmount;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }
}
