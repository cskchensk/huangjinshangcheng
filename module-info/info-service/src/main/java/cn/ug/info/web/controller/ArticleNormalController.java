package cn.ug.info.web.controller;


import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.info.bean.ArticleBean;
import cn.ug.info.bean.status.YesStatus;
import cn.ug.info.mapper.entity.Article;
import cn.ug.info.service.ArticleService;
import cn.ug.util.DateUtils;
import cn.ug.util.StringUtils;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.ibatis.reflection.ExceptionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.util.Date;

import static org.springframework.web.bind.annotation.RequestMethod.POST;


/**
 * 活动相关接口
 */
@RestController
@RequestMapping("normal")
public class ArticleNormalController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(ArticleNormalController.class);

    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "canApply", method = POST)
        public SerializeObject canApply(String articleClassId, String title, String memberId) {
        logger.info("=======是否是活动时间接口======");
        try {
            Long nowTime = new Date().getTime();

            if (StringUtils.isNull(articleClassId) || StringUtils.isNull(title) || StringUtils.isNull(memberId)) {
                return new SerializeObject(ResultType.ERROR, "请求参数不能为空");
            }

            Article bean = articleService.getByArticleClassIdAndTitle(articleClassId, title);

            if (ObjectUtils.isEmpty(bean)) {
                logger.info("=======活动不存在======");
                return new SerializeObject(ResultType.ERROR, "活动不存在");
            }

            ZoneId zone = ZoneId.systemDefault();
            Date startDate = Date.from(bean.getStartTime().atZone(zone).toInstant());
            Date stopDate = Date.from(bean.getStopTime().atZone(zone).toInstant());
            if (startDate.getTime() > nowTime) {
                logger.info("=======活动未开始======");
                return new SerializeObject(ResultType.NORMAL,DateUtils.dateToStr(startDate,DateUtils.patter2), 1);
            }
            if (stopDate.getTime() < nowTime) {
                logger.info("=======活动已结束======");
                return new SerializeObject(ResultType.NORMAL,DateUtils.dateToStr(stopDate,DateUtils.patter2), 2);
            }


            boolean flag = articleService.canApply(memberId, DateUtils.dateToStr(startDate,DateUtils.patter), DateUtils.dateToStr(stopDate,DateUtils.patter));

            if (!flag) {
                logger.info("=======未购买且回租，======");
                return new SerializeObject(ResultType.NORMAL, "未购买且回租，", 4);
            }

            return new SerializeObject(ResultType.NORMAL, 3);


        } catch (Exception e) {
            logger.error("ArticleNormalController canApply 异常信息:{}", ExceptionUtils.getMessage(e));
            return new SerializeObject(ResultType.ERROR, "稍后再试");
        }
    }


}
