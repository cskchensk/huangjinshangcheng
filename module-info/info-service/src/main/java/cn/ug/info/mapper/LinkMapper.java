package cn.ug.info.mapper;

import cn.ug.info.mapper.entity.Link;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 友情链接
 * @author kaiwotech
 */
@Component
public interface LinkMapper extends BaseMapper<Link> {
}