package cn.ug.info.mapper;


import cn.ug.info.mapper.entity.AdvertPosition;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 广告位
 * @author kaiwotech
 */
@Component
public interface AdvertPositionMapper extends BaseMapper<AdvertPosition> {
}