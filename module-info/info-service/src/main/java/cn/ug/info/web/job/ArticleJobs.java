package cn.ug.info.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.info.service.ArticleService;
import cn.ug.util.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 活动状态设置
 * @author  ywl
 */
@Component
public class ArticleJobs implements Serializable{

    /** 业务有效期 */
    private static final long MINUTES_DAY = 4;

    @Resource
    private ArticleService articleService;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    private static Logger logger = LoggerFactory.getLogger(ArticleJobs.class);

    /**
     * 活动是否结束
     * 每五分钟执行一次
     */
    @Scheduled(cron = "0 */1 * * * ?")
    public void articleJob(){
        String key = "ArticleJobs:expiredArticleJob:" + UF.getFormatDateNow();
        if(redisGlobalLock.lock(key, MINUTES_DAY, TimeUnit.MINUTES)) {
            try{
                logger.info("开始执行活动是否结束定时任务");
                logger.info(UF.getFormatDateTime(UF.getDateTime()));
                //执行任务
                articleService.articleJob();
            }catch (Exception e){
                throw  e;
            }finally {
                redisGlobalLock.unlock(key);
            }
        }else{
            logger.info("-------没有获取到锁-------");
            return ;
        }

    }

}
