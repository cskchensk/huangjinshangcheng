package cn.ug.info.web.controller;

import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.info.bean.ArticleClassBean;
import cn.ug.info.bean.type.ArticleClassType;
import cn.ug.info.service.ArticleClassService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 信息发布分类相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("articleClass")
public class ArticleClassController extends BaseController {

    @Resource
    private ArticleClassService articleClassService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject<ArticleClassBean> find(String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        ArticleClassBean entity = articleClassService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }

        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:articleClass:update")
    @RequestMapping(method = POST)
    public SerializeObject update(String accessToken, ArticleClassBean entity) {
        if(null == entity || StringUtils.isBlank(entity.getTitle())) {
            return new SerializeObjectError("12000101");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = articleClassService.save(entity);
        } else {
            val = articleClassService.update(entity.getId(), entity);
        }

        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:articleClass:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = articleClassService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:articleClass:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = articleClassService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param code			    编号
     * @param layerMark		    层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
     * @param layer			    层级
     * @param showInNav		    是否显示
     * @param keyword		    关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list", method = GET)
    public SerializeObject<List<ArticleClassBean>> list(String accessToken, Order order, String code, String layerMark, String layer, Integer showInNav, String keyword) {
        keyword = UF.toString(keyword);
        List<ArticleClassBean> list = articleClassService.findList(order.getOrder(), order.getSort(), code, layerMark, layer, showInNav, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param code			    编号
     * @param layerMark		    层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
     * @param layer			    层级
     * @param showInNav		    是否显示
     * @param keyword		    关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<ArticleClassBean>> query(String accessToken, Order order, Page page, String code, String layerMark, String layer, Integer showInNav, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        keyword = UF.toString(keyword);

        DataTable<ArticleClassBean> dataTable = articleClassService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), code, layerMark, layer, showInNav, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequestMapping(value = "verify", method = GET)
    public SerializeObject verify(String accessToken, ArticleClassBean entity) {
        if(articleClassService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据code查找信息
     * @param accessToken		登录成功后分配的Key
     * @param code			编号
     * @return				分页数据
     */
    @RequestMapping(value = "findByCode", method = GET)
    public SerializeObject<ArticleClassBean> findByCode(String accessToken, String code) {
        if(null == code || StringUtils.isBlank(code)) {
            return new SerializeObjectError("00000002");
        }
        List<ArticleClassBean> list = articleClassService.findList(null, null, code, null, null, null, null);
        if(null == list || list.size() <= 0){
            return new SerializeObjectError("00000003");
        }
        ArticleClassBean obj = list.get(0);
        return new SerializeObject<>(ResultType.NORMAL, obj);
    }

    /**
     * 根据层级获取数据
     * @param accessToken		登录成功后分配的Key
     * @param layer				分层级( null:返回顶级数据  layer___：获取layer子节点)
     * @return			      	数据集
     */
    @RequestMapping(value = "findListByLayer", method = GET)
    public SerializeObject<List<ArticleClassBean>> findListByLayer(String accessToken, String layer) {
        List<ArticleClassBean> dataList = articleClassService.findListByLayer(layer);
        return new SerializeObject<>(ResultType.NORMAL, dataList);
    }

    /**
     * 根据层级获取数据{所有子节点}
     * @param accessToken		登录成功后分配的Key
     * @param layer				分层级( null:返回所有数据  layer%：获取layer所有子节点)
     * @return			      	数据集
     */
    @RequestMapping(value = "findListByAllLayer", method = GET)
    public SerializeObject<List<ArticleClassBean>> findListByAllLayer(String accessToken, String layer) {
        List<ArticleClassBean> dataList = articleClassService.findListByAllLayer(layer);
        return new SerializeObject<>(ResultType.NORMAL, dataList);
    }

    /**
     * 根据层级获取数据{本级}
     * @param accessToken		登录成功后分配的Key
     * @param layer				层级
     * @return			      	数据集
     */
    @RequestMapping(value = "findByLayer", method = GET)
    public SerializeObject<ArticleClassBean> findByLayer(String accessToken, String layer) {
        if(StringUtils.isBlank(layer)) {
            return new SerializeObjectError("00000002");
        }
        ArticleClassBean entity = articleClassService.findByLayer(layer);
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 获取所有层级数据
     * @param accessToken	登录成功后分配的Key
     * @return				数据集
     */
    @RequestMapping(value = "findAllList", method = GET)
    public SerializeObject<List<ArticleClassBean>> findAllList(String accessToken) {
        List<ArticleClassBean> dataList = articleClassService.findListByAllLayer(null);
        return new SerializeObject<>(ResultType.NORMAL, dataList);
    }

    /**
     * 根据编号获取数据
     * @param code			编号
     * @param layerMark		层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
     * @param showInNav		是否显示
     * @return				数据集
     */
    @RequestMapping(value = "findListByCode", method = GET)
    public SerializeObject<List<ArticleClassBean>> findListByCode(String code, String layerMark, Integer showInNav) {
        ArticleClassBean articleClassBean = articleClassService.findByCode(code);
        if(null == articleClassBean) {
            return new SerializeObject<>(ResultType.NORMAL, new ArrayList<>());
        }
        String layer = articleClassBean.getLayer();
        List<ArticleClassBean> dataList = articleClassService.findList("layer", "asc", null, layerMark, layer, showInNav, null);
        return new SerializeObject<>(ResultType.NORMAL, dataList);
    }

}
