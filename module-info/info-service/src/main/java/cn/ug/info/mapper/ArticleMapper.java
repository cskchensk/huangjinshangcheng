package cn.ug.info.mapper;


import cn.ug.info.bean.ArticleBean;
import cn.ug.info.mapper.entity.Article;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 信息发布
 * @author kaiwotech
 */
@Component
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 关闭活动
     * @param id
     * @return
     */
    boolean updateArticleStop(@Param("id") String id);

    /**
     * 阅读数增加
     * @param id
     * @return
     */
    int addReadNum(@Param("id") String id);
    /**
     * 活动是否存在
     * @param articleClassId
     * @param title
     * @param date
     * @return
     */
    Article activityTime(@Param("articleClassId")String articleClassId, @Param("title")String title, @Param("nowDate")String date );

    /**
     * 是否可以申领
     * @param memberId
     * @param startTime
     * @param stopTime
     * @return
     */
    int canApply(@Param("memberId")String memberId,@Param("startTime") String startTime,@Param("stopTime")String stopTime);
}