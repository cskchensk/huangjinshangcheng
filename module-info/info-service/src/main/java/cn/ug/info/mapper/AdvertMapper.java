package cn.ug.info.mapper;


import cn.ug.info.mapper.entity.Advert;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 广告
 * @author kaiwotech
 */
@Component
public interface AdvertMapper extends BaseMapper<Advert> {
}