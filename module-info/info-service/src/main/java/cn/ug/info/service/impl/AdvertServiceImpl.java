package cn.ug.info.service.impl;

import cn.ug.core.ensure.Ensure;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.info.bean.AdvertBean;
import cn.ug.info.mapper.AdvertMapper;
import cn.ug.info.mapper.entity.Advert;
import cn.ug.info.service.AdvertService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class AdvertServiceImpl extends BaseServiceImpl implements AdvertService {
	
	@Resource
	private AdvertMapper advertMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(AdvertBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getTitle())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }
		Advert entity = dozerBeanMapper.map(entityBean, Advert.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		advertMapper.insert(entity);

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int update(String id, AdvertBean entityBean) {
        // 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getId()) || StringUtils.isBlank(entityBean.getTitle())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }

		Advert entity = advertMapper.findById(entityBean.getId());
		dozerBeanMapper.map(entityBean, entity);
		advertMapper.update(entity);
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return advertMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return advertMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return advertMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public boolean exists(AdvertBean entityBean, String id) {
		if(null == entityBean || StringUtils.isBlank(entityBean.getTitle())) {
			return false;
		}

		int rows = advertMapper.exists(
				getParams()
						.put("title", entityBean.getTitle())
						.put("type", entityBean.getType())
						.put("id", id)
						.toMap()
		);
		return rows > 0;
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public AdvertBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		Advert entity = advertMapper.findById(id);
		if(null == entity) {
			return null;
		}

		AdvertBean entityBean = dozerBeanMapper.map(entity, AdvertBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<AdvertBean> findList(String order, String sort, String articleClassId, String advertPositionId, Integer type, String dateTimeString, String keyword){
		return query(order, sort, articleClassId, advertPositionId, type, dateTimeString, keyword);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<AdvertBean> query(String order, String sort, int pageNum, int pageSize, String articleClassId, String advertPositionId, Integer type, String dateTimeString, String keyword){
		Page<AdvertBean> page = PageHelper.startPage(pageNum, pageSize);
		List<AdvertBean> list = query(order, sort, articleClassId, advertPositionId, type, dateTimeString, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	/**
	 * 获取数据列表
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param articleClassId	分类ID
	 * @param advertPositionId	广告位ID
	 * @param type				类型
	 * @param dateTimeString	日期
	 * @param keyword			关键字
	 * @return					列表
	 */
	private List<AdvertBean> query(String order, String sort, String articleClassId, String advertPositionId, Integer type, String dateTimeString, String keyword){
		List<AdvertBean> dataList = new ArrayList<>();
		List<Advert> list = advertMapper.query(
				getParams(keyword, order, sort)
						.put("articleClassId", articleClassId)
						.put("advertPositionId", advertPositionId)
						.put("type", type)
						.put("dateTimeString", dateTimeString)
						.toMap());
		for (Advert o : list) {
			AdvertBean objBean = dozerBeanMapper.map(o, AdvertBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}
}

