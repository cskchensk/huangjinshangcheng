package cn.ug.info.service.impl;

import cn.ug.core.ensure.Ensure;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.info.bean.LinkBean;
import cn.ug.info.mapper.LinkMapper;
import cn.ug.info.mapper.entity.Link;
import cn.ug.info.service.LinkService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class LinkServiceImpl extends BaseServiceImpl implements LinkService {
	
	@Resource
	private LinkMapper linkMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(LinkBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getTitle())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }
		Link entity = dozerBeanMapper.map(entityBean, Link.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		linkMapper.insert(entity);

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int update(String id, LinkBean entityBean) {
        // 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getId()) || StringUtils.isBlank(entityBean.getTitle())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
			// 该数据已存在
			Ensure.that(true).isTrue("00000004");
		}
		Link entity = linkMapper.findById(entityBean.getId());
		dozerBeanMapper.map(entityBean, entity);

		linkMapper.update(entity);
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return linkMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return linkMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return linkMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public boolean exists(LinkBean entityBean, String id) {
		if(null == entityBean || StringUtils.isBlank(entityBean.getTitle())) {
			return false;
		}

		int rows = linkMapper.exists(
				getParams()
						.put("title", entityBean.getTitle())
						.put("id", id)
						.toMap()
		);
		return rows > 0;
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public LinkBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		Link entity = linkMapper.findById(id);
		if(null == entity) {
			return null;
		}

		LinkBean entityBean = dozerBeanMapper.map(entity, LinkBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<LinkBean> findList(String order, String sort, String keyword) {
		return query(order, sort, null, keyword);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<LinkBean> query(String order, String sort, int pageNum, int pageSize, String keyword){
		Page<LinkBean> page = PageHelper.startPage(pageNum, pageSize);
		List<LinkBean> list = query(order, sort, null, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	/**
	 * 获取数据列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param title			标题
	 * @param keyword		关键字
	 * @return				列表
	 */
	private List<LinkBean> query(String order, String sort, String title, String keyword){
		List<LinkBean> dataList = new ArrayList<>();
		List<Link> list = linkMapper.query(
				getParams(keyword, order, sort)
						.put("title", title)
						.toMap());
		for (Link o : list) {
			LinkBean objBean = dozerBeanMapper.map(o, LinkBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}
}

