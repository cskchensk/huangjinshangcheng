package cn.ug.info.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * 广告
 * @author kaiwotech
 */
public class Advert extends BaseEntity implements java.io.Serializable {

	/** 广告位ID */
	private String advertPositionId;
	/** 广告位标题 */
	private String advertPositionTitle;
	/** 广告类型 1：图片 2：flash 3：代码 4：文字 */
	private Integer type;
	/** 标题 */
	private String title;
	/** 副标题 */
	private String subTitle;
	/** 打开方式默认_blank */
	private String target;
	/** 链接地址 */
	private String link;
	/** 排序 */
	private Integer sort;
	/** 图片 */
	private String image;
	/** 广告内容 */
	private String content;
	/** 开始时间 */
	private LocalDateTime startTime;
	/** 结束时间 */
	private LocalDateTime endTime;
	/** 点击量 */
	private Integer hits;
	/** 备注 */
	private String description;

	public String getAdvertPositionId() {
		return advertPositionId;
	}

	public void setAdvertPositionId(String advertPositionId) {
		this.advertPositionId = advertPositionId;
	}

	public String getAdvertPositionTitle() {
		return advertPositionTitle;
	}

	public void setAdvertPositionTitle(String advertPositionTitle) {
		this.advertPositionTitle = advertPositionTitle;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
