package cn.ug.info.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 友情链接
 * @author kaiwotech
 */
public class Link extends BaseEntity implements java.io.Serializable {

	/** 名称 */
	private String title;
	/** 链接地址 */
	private String link;
	/** Logo图片 */
	private String image;
	/** 排序 */
	private Integer sort;
	/** 内容 */
	private String content;
	/** 备注 */
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
