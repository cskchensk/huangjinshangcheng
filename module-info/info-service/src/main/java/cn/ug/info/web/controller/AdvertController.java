package cn.ug.info.web.controller;

import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.info.bean.AdvertBean;
import cn.ug.info.bean.AdvertPositionBean;
import cn.ug.info.service.AdvertPositionService;
import cn.ug.info.service.AdvertService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 广告相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("advert")
public class AdvertController extends BaseController {

    @Resource
    private AdvertService advertService;
    @Resource
    private AdvertPositionService advertPositionService;
    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject<AdvertBean> find(String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        AdvertBean entity = advertService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:advert:update")
    @RequestMapping(method = POST)
    public SerializeObject update(String accessToken, AdvertBean entity) {
        if(null == entity || StringUtils.isBlank(entity.getTitle())) {
            return new SerializeObjectError("12000101");
        }
        if(StringUtils.isBlank(entity.getAdvertPositionId())) {
            return new SerializeObjectError("12000102");
        }
        if(null == entity.getAdvertPositionTitle() || StringUtils.isBlank(entity.getAdvertPositionTitle())) {
            AdvertPositionBean advertPositionBean = advertPositionService.findById(entity.getAdvertPositionId());
            if (null == advertPositionBean || StringUtils.isBlank(advertPositionBean.getId())) {
                return new SerializeObjectError("12000104");
            }
            entity.setAdvertPositionTitle(advertPositionBean.getTitle());
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = advertService.save(entity);
        } else {
            val = advertService.update(entity.getId(), entity);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:advert:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = advertService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:advert:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = advertService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param articleClassId	分类ID
     * @param advertPositionId	广告位ID
     * @param type				类型
     * @param dateTimeString	日期
     * @param keyword			关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list", method = GET)
    public SerializeObject<List<AdvertBean>> list(String accessToken, Order order, String articleClassId, String advertPositionId, Integer type, String dateTimeString, String keyword) {
        keyword = UF.toString(keyword);
        List<AdvertBean> list = advertService.findList(order.getOrder(), order.getSort(), articleClassId, advertPositionId, type, dateTimeString, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param articleClassId	分类ID
     * @param advertPositionId	广告位ID
     * @param type				类型
     * @param dateTimeString	日期
     * @param keyword			关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<AdvertBean>> query(String accessToken, Order order, Page page, String articleClassId, String advertPositionId, Integer type, String dateTimeString, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        keyword = UF.toString(keyword);

        DataTable<AdvertBean> dataTable = advertService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), articleClassId, advertPositionId, type, dateTimeString, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequestMapping(value = "verify", method = GET)
    public SerializeObject verify(String accessToken, AdvertBean entity) {
        if(advertService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }
}
