package cn.ug.info.service;


import cn.ug.bean.base.DataTable;
import cn.ug.info.bean.ArticleBean;
import cn.ug.info.mapper.entity.Article;

import java.util.Date;
import java.util.List;

/**
 * 信息
 * @author kaiwotech
 */
public interface ArticleService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(ArticleBean entityBean);
	
	/**
	 * 编辑
	 * @param id			ID
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int update(String id, ArticleBean entityBean);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
     * 判断数据是否存在
     * @param entityBean	数据对象
     * @param id			例外ID
     * @return				True 是 | False 否
     */
    boolean exists(ArticleBean entityBean, String id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	ArticleBean findById(String id);

	/**
	 * 获取列表
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param articleClassId	分类ID
	 * @param status			状态
	 * @param keyword			关键字
	 * @return					列表数据
	 */
	List<ArticleBean> findList(String order, String sort, String articleClassId, Integer status, String keyword, String startTime, String endTime);

	/**
	 * 搜索
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param pageNum			当前页1..N
	 * @param pageSize			每页记录数
	 * @param articleClassId	分类ID
	 * @param status			状态
	 * @param keyword			关键字
	 * @return					分页数据
	 */
	DataTable<ArticleBean> query(String order, String sort, int pageNum, int pageSize, String articleClassId, Integer status, String keyword, String startTime, String endTime);

	/**
	 * 执行定时任务
	 * @return
	 */
	boolean articleJob();

	/**
	 * 阅读数增加
	 * @param id
	 * @return
	 */
	int read(String id);

	/**
	 * 通过id，和标题获取
	 * @param articleClassId
	 * @param title
	 * @return
	 */
	Article getByArticleClassIdAndTitle(String articleClassId, String title);

	/**
	 * 是否为活动时间
	 * @param articleClassId
	 * @param title
	 * @param date
	 * @return
	 */
	Article activityTime(String articleClassId, String title, String date );

	/**
	 * 是否可以申领
	 * @param memberId
	 * @param startTime
	 * @param stopTime
	 * @return
	 */
	boolean canApply(String memberId, String startTime,String stopTime);

}
