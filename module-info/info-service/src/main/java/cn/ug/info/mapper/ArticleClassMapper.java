package cn.ug.info.mapper;


import cn.ug.info.mapper.entity.ArticleClass;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 信息发布分类
 * @author kaiwotech
 */
@Component
public interface ArticleClassMapper extends BaseMapper<ArticleClass> {
}