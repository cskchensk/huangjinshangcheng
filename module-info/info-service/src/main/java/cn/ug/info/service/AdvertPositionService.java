package cn.ug.info.service;


import cn.ug.bean.base.DataTable;
import cn.ug.info.bean.AdvertPositionBean;

import java.util.List;

/**
 * 广告位
 * @author kaiwotech
 */
public interface AdvertPositionService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(AdvertPositionBean entityBean);
	
	/**
	 * 编辑
	 * @param id			ID
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int update(String id, AdvertPositionBean entityBean);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
     * 判断数据是否存在
     * @param entityBean	数据对象
     * @param id			例外ID
     * @return				True 是 | False 否
     */
    boolean exists(AdvertPositionBean entityBean, String id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	AdvertPositionBean findById(String id);

	/**
	 * 获取列表
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param articleClassId 	分类ID
	 * @param code				编号
	 * @param keyword			关键字
	 * @return					列表数据
	 */
	List<AdvertPositionBean> findList(String order, String sort, String articleClassId, String code, String keyword);

	/**
	 * 搜索
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param pageNum			当前页1..N
	 * @param pageSize			每页记录数
	 * @param articleClassId 	分类ID
	 * @param code				编号
	 * @param keyword			关键字
	 * @return					分页数据
	 */
	DataTable<AdvertPositionBean> query(String order, String sort, int pageNum, int pageSize, String articleClassId, String code, String keyword);

}
