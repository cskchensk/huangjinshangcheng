package cn.ug.info.web.controller;

import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.info.bean.AdvertPositionBean;
import cn.ug.info.bean.ArticleClassBean;
import cn.ug.info.service.AdvertPositionService;
import cn.ug.info.service.ArticleClassService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 广告位相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("advertPosition")
public class AdvertPositionController extends BaseController {

    @Resource
    private AdvertPositionService advertPositionService;
    @Resource
    private ArticleClassService articleClassService;
    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject<AdvertPositionBean> find(String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError<>("00000002");
        }
        AdvertPositionBean entity = advertPositionService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError<>("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:advertPosition:update")
    @RequestMapping(method = POST)
    public SerializeObject update(String accessToken, AdvertPositionBean entity) {
        if(null == entity || StringUtils.isBlank(entity.getCode())) {
            return new SerializeObjectError("12000102");
        }
        if(null != entity.getArticleClassId() && StringUtils.isNotBlank(entity.getArticleClassId())) {
            ArticleClassBean articleClassBean = articleClassService.findById(entity.getArticleClassId());
            if (null == articleClassBean || StringUtils.isBlank(articleClassBean.getId())) {
                return new SerializeObjectError("12000103");
            }
            entity.setArticleClassTitle(articleClassBean.getTitle());
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = advertPositionService.save(entity);
        } else {
            val = advertPositionService.update(entity.getId(), entity);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:advertPosition:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = advertPositionService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:advertPosition:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = advertPositionService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param articleClassId    分类ID
     * @param code			    编号
     * @param keyword		    关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list", method = GET)
    public SerializeObject<List<AdvertPositionBean>> list(String accessToken, Order order, String articleClassId, String code, String keyword) {
        keyword = UF.toString(keyword);
        List<AdvertPositionBean> list = advertPositionService.findList(order.getOrder(), order.getSort(), articleClassId, code, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param articleClassId    分类ID
     * @param code			    编号
     * @param keyword		    关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<AdvertPositionBean>> query(String accessToken, Order order, Page page, String articleClassId, String code, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        keyword = UF.toString(keyword);

        DataTable<AdvertPositionBean> dataTable = advertPositionService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), articleClassId, code, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequestMapping(value = "verify", method = GET)
    public SerializeObject verify(String accessToken, AdvertPositionBean entity) {
        if(advertPositionService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据code查找信息
     * @param accessToken	登录成功后分配的Key
     * @param code			编号
     * @return				分页数据
     */
    @RequestMapping(value = "findByCode", method = GET)
    public SerializeObject<AdvertPositionBean> findByCode(String accessToken, String code) {
        if(null == code || StringUtils.isBlank(code)) {
            return new SerializeObjectError<>("00000002");
        }
        List<AdvertPositionBean> list = advertPositionService.findList(null, null, null, code, null);
        if(null == list || list.size() <= 0){
            return new SerializeObjectError<>("00000003");
        }
        AdvertPositionBean obj = list.get(0);
        return new SerializeObject<>(ResultType.NORMAL, obj);
    }
}
