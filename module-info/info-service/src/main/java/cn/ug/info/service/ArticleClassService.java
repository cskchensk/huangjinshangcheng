package cn.ug.info.service;


import cn.ug.bean.base.DataTable;
import cn.ug.info.bean.ArticleClassBean;

import java.util.List;

/**
 * 信息分类
 * @author kaiwotech
 */
public interface ArticleClassService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(ArticleClassBean entityBean);
	
	/**
	 * 编辑
	 * @param id			ID
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int update(String id, ArticleClassBean entityBean);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
     * 判断数据是否存在
     * @param entityBean	数据对象
     * @param id			例外ID
     * @return				True 是 | False 否
     */
    boolean exists(ArticleClassBean entityBean, String id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	ArticleClassBean findById(String id);

	/**
	 * 根据Layer, 查找对象
	 * @param layer		层级
	 * @return
	 */
	ArticleClassBean findByLayer(String layer);

	/**
	 * 根据code, 查找对象
	 * @param code		编码
	 * @return
	 */
	ArticleClassBean findByCode(String code);

	/**
	 * 根据层级获取数据
	 * @param layer		null:返回顶级数据  layer___：获取layer子节点
	 * @return
	 */
	List<ArticleClassBean> findListByLayer(String layer);

	/**
	 * 根据层级获取数据{所有子节点}
	 * @param layer		分层级( null:返回所有数据  layer%：获取layer所有子节点)
	 * @return
	 */
	List<ArticleClassBean> findListByAllLayer(String layer);

	/**
	 * 获取列表
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param code				编号
	 * @param layerMark			层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
	 * @param layer				层级
	 * @param showInNav			是否显示
	 * @param keyword			关键字
	 */
	List<ArticleClassBean> findList(String order, String sort, String code, String layerMark, String layer, Integer showInNav, String keyword);

	/**
	 * 搜索
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param pageNum			当前页1..N
	 * @param pageSize			每页记录数
	 * @param code				编号
	 * @param layerMark			层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
	 * @param layer				层级
	 * @param showInNav			是否显示
	 * @param keyword			关键字
	 * @return					分页数据
	 */
	DataTable<ArticleClassBean> query(String order, String sort, int pageNum, int pageSize, String code, String layerMark, String layer, Integer showInNav, String keyword);

}
