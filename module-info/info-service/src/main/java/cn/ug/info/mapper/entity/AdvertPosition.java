package cn.ug.info.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 广告位
 * @author kaiwotech
 */
public class AdvertPosition extends BaseEntity implements java.io.Serializable {

	/** 分类ID */
	private String articleClassId;
	/** 分类标题 */
	private String articleClassTitle;
	/** 编号 */
	private String code;
	/** 名称 */
	private String title;
	/** 宽度 */
	private Integer width;
	/** 高度 */
	private Integer height;
	/** 排序 */
	private Integer sort;
	/** 备注 */
	private String description;

	public String getArticleClassId() {
		return articleClassId;
	}

	public void setArticleClassId(String articleClassId) {
		this.articleClassId = articleClassId;
	}

	public String getArticleClassTitle() {
		return articleClassTitle;
	}

	public void setArticleClassTitle(String articleClassTitle) {
		this.articleClassTitle = articleClassTitle;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
