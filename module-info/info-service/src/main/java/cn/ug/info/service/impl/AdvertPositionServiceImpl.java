package cn.ug.info.service.impl;

import cn.ug.core.ensure.Ensure;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.info.bean.AdvertPositionBean;
import cn.ug.info.mapper.AdvertPositionMapper;
import cn.ug.info.mapper.entity.AdvertPosition;
import cn.ug.info.service.AdvertPositionService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class AdvertPositionServiceImpl extends BaseServiceImpl implements AdvertPositionService {
	
	@Resource
	private AdvertPositionMapper advertPositionMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(AdvertPositionBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getTitle())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }
		AdvertPosition entity = dozerBeanMapper.map(entityBean, AdvertPosition.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		advertPositionMapper.insert(entity);

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int update(String id, AdvertPositionBean entityBean) {
        // 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getId()) || StringUtils.isBlank(entityBean.getTitle())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }
		AdvertPosition entity = advertPositionMapper.findById(entityBean.getId());
		dozerBeanMapper.map(entityBean, entity);

		advertPositionMapper.update(entity);
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return advertPositionMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return advertPositionMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return advertPositionMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public boolean exists(AdvertPositionBean entityBean, String id) {
		if(null == entityBean || StringUtils.isBlank(entityBean.getTitle())) {
			return false;
		}

		int rows = advertPositionMapper.exists(
				getParams()
						.put("title", entityBean.getTitle())
						.put("code", entityBean.getCode())
						.put("articleClassId", entityBean.getArticleClassId())
						.put("id", id)
						.toMap()
		);
		return rows > 0;
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public AdvertPositionBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		AdvertPosition entity = advertPositionMapper.findById(id);
		if(null == entity) {
			return null;
		}

		AdvertPositionBean entityBean = dozerBeanMapper.map(entity, AdvertPositionBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<AdvertPositionBean> findList(String order, String sort, String articleClassId, String code, String keyword){
		return query(order, sort, articleClassId, code, keyword);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<AdvertPositionBean> query(String order, String sort, int pageNum, int pageSize, String articleClassId, String code, String keyword){
		Page<AdvertPositionBean> page = PageHelper.startPage(pageNum, pageSize);
		List<AdvertPositionBean> list = query(order, sort, articleClassId, code, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	/**
	 * 获取数据列表
	 * @param order				排序字段
	 * @param sort				排序方式 desc或asc
	 * @param articleClassId 	分类ID
	 * @param code				编号
	 * @param keyword			关键字
	 * @return					列表
	 */
	private List<AdvertPositionBean> query(String order, String sort, String articleClassId, String code, String keyword){
		List<AdvertPositionBean> dataList = new ArrayList<>();
		List<AdvertPosition> list = advertPositionMapper.query(
				getParams(keyword, order, sort)
						.put("articleClassId", articleClassId)
						.put("code", code)
						.toMap());
		for (AdvertPosition o : list) {
			AdvertPositionBean objBean = dozerBeanMapper.map(o, AdvertPositionBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}
}

