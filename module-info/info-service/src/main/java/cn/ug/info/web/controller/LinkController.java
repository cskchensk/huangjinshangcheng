package cn.ug.info.web.controller;

import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.info.bean.LinkBean;
import cn.ug.info.service.LinkService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 友情链接相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("link")
public class LinkController extends BaseController {

    @Resource
    private LinkService linkService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject<LinkBean> find(String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        LinkBean entity = linkService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:link:update")
    @RequestMapping(method = POST)
    public SerializeObject update(String accessToken, LinkBean entity) {
        if(null == entity || StringUtils.isBlank(entity.getTitle())) {
            return new SerializeObjectError("12000101");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = linkService.save(entity);
        } else {
            val = linkService.update(entity.getId(), entity);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:link:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = linkService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:link:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = linkService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list", method = GET)
    public SerializeObject<List<LinkBean>> list(String accessToken, Order order, String keyword) {
        keyword = UF.toString(keyword);
        List<LinkBean> list = linkService.findList(order.getOrder(), order.getSort(), keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<LinkBean>> query(String accessToken, Order order, Page page, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        keyword = UF.toString(keyword);

        DataTable<LinkBean> dataTable = linkService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequestMapping(value = "verify", method = GET)
    public SerializeObject verify(String accessToken, LinkBean entity) {
        if(linkService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }
}
