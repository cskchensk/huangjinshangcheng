package cn.ug.info.bean.type;

import cn.ug.bean.type.BaseType;

/**
 * 类型
 * @author kaiwotech
 */
public class ArticleShowType extends BaseType {
	/** 文字列表 */
	public static final int TEXT_LIST 		= 1;
	/** 图片文字列表 */
	public static final int IMAGE_TEXT_LIST = 2;
}
