package cn.ug.info.bean;

import cn.ug.bean.base.BaseBean;

/**
 * 信息发布分类
 * @author kaiwotech
 */
public class ArticleClassBean extends BaseBean implements java.io.Serializable {

	/** 编号 */
	private String code;
	/** 标题 */
	private String title;
	/** 副标题 */
	private String subTitle;
	/** 级别001：分类 001001：分类 1001002：分类2 */
	private String layer;
	/** 归属层级 */
	private String supLayer;
	/** 排序（从小到大升序排列） */
	private Integer sort;
	/** 关键字 */
	private String keywords;
	/** 是否显示在标题栏 1：是  2：否 */
	private Integer showInNav;
	/** 显示模式 1：文字列表 2：图片文字列表 */
	private Integer showModel;
	/** 备注 */
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getLayer() {
		return layer;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public String getSupLayer() {
		return supLayer;
	}

	public void setSupLayer(String supLayer) {
		this.supLayer = supLayer;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public Integer getShowInNav() {
		return showInNav;
	}

	public void setShowInNav(Integer showInNav) {
		this.showInNav = showInNav;
	}

	public Integer getShowModel() {
		return showModel;
	}

	public void setShowModel(Integer showModel) {
		this.showModel = showModel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
