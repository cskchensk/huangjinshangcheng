package cn.ug.info.bean.status;

import cn.ug.bean.status.BaseStatus;

/**
 * 状态类型
 * @author kaiwotech
 */
public class ShowStatus extends BaseStatus {
    /** 显示 */
    public static final int SHOW 	= 1;
    /** 不显示 */
    public static final int HIDE	= 2;
}
