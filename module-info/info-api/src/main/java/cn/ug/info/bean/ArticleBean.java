package cn.ug.info.bean;

import cn.ug.bean.base.BaseBean;


/**
 * 信息发布
 * @author kaiwotech
 */
public class ArticleBean extends BaseBean implements java.io.Serializable {
	/** 分类ID */
	private String articleClassId;
	/** 分类标题 */
	private String articleClassTitle;
	/** 标题 */
	private String title;
	/** 副标题 */
	private String subTitle;
	/** 标签（关键字） */
	private String keywords;
	/** 发布时间 */
	private String pushTimeString;
	/** 结束时间 **/
	private String stopTimeString;
	/** 开始时间 **/
	private String startTimeString;
	/** 来源 */
	private String source;
	/** 作者的email */
	private String email;
	/** 点击数 */
	private Integer hits;
	/** 是否允许评论 */
	private Integer allowReview;
	/** 跳转链接 */
	private String link;
	/** 排序 */
	private Integer sort;
	/** 状态(1：显示 2：不显示) */
	private Integer status;
	/** 图片 */
	private String image;
	/** 内容(详情) */
	private String content;
	/** 备注{文章简述} */
	private String description;
	/** 标签1 **/
	private String label1;
	/** 标签2 **/
	private String label2;
	/** 标签3 **/
	private String label3;
	/** 活动是否结束 1：进行中 2：已结束 **/
	private Integer isStop;
	/** 渠道ids **/
	private String channelIds;
	public String getArticleClassId() {
		return articleClassId;
	}

	public void setArticleClassId(String articleClassId) {
		this.articleClassId = articleClassId;
	}

	public String getArticleClassTitle() {
		return articleClassTitle;
	}

	public void setArticleClassTitle(String articleClassTitle) {
		this.articleClassTitle = articleClassTitle;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getPushTimeString() {
		return pushTimeString;
	}

	public void setPushTimeString(String pushTimeString) {
		this.pushTimeString = pushTimeString;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	public Integer getAllowReview() {
		return allowReview;
	}

	public void setAllowReview(Integer allowReview) {
		this.allowReview = allowReview;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLabel1() {
		return label1;
	}

	public void setLabel1(String label1) {
		this.label1 = label1;
	}

	public String getLabel2() {
		return label2;
	}

	public void setLabel2(String label2) {
		this.label2 = label2;
	}

	public String getLabel3() {
		return label3;
	}

	public void setLabel3(String label3) {
		this.label3 = label3;
	}

	public Integer getIsStop() {
		return isStop;
	}

	public void setIsStop(Integer isStop) {
		this.isStop = isStop;
	}

	public String getStopTimeString() {
		return stopTimeString;
	}

	public void setStopTimeString(String stopTimeString) {
		this.stopTimeString = stopTimeString;
	}

	public String getStartTimeString() {
		return startTimeString;
	}

	public void setStartTimeString(String startTimeString) {
		this.startTimeString = startTimeString;
	}

	public String getChannelIds() {
		return channelIds;
	}

	public void setChannelIds(String channelIds) {
		this.channelIds = channelIds;
	}
}
