package cn.ug.info.bean.type;

import cn.ug.bean.type.BaseType;

/**
 * 广告类型
 * @author kaiwotech
 */
public class AdvertType extends BaseType {
    /** 图片 */
    public static final int IMAGE		    = 1;
    /** flash */
    public static final int FLASH		    = 2;
    /** 代码 */
    public static final int CODE		    = 3;
    /** 文字 */
    public static final int WORDS		    = 4;
}
