package cn.ug.info.api;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.info.bean.ArticleClassBean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RequestMapping("articleClass")
public interface ArticleClassServiceApi {

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<ArticleClassBean> find(
            @RequestHeader("accessToken") String accessToken,
            @PathVariable("id") String id);

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken       登录成功后分配的Key
     * @param param             参数集合
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    SerializeObject update(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam Map<String, Object> param);

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(method = DELETE)
    SerializeObject delete(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("id") String[] id);

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "remove", method = PUT)
    SerializeObject remove(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("id") String[] id);

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序字段
     * @param sort		        排序方式 desc或asc
     * @param code			    编号
     * @param type			    类型
     * @param layerMark		    层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
     * @param layer			    层级
     * @param showInNav		    是否显示
     * @param keyword		    关键字
     * @return			        列表数据
     */
    @RequestMapping(value = "list" , method = GET)
    SerializeObject<List<ArticleClassBean>> list(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("code") String code,
            @RequestParam("type") Integer type,
            @RequestParam("layerMark") String layerMark,
            @RequestParam("layer") String layer,
            @RequestParam("showInNav") Integer showInNav,
            @RequestParam("keyword") String keyword);

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序字段
     * @param sort		        排序方式 desc或asc
     * @param pageNum	        查询页数
     * @param pageSize	        每页记录数
     * @param code			    编号
     * @param type			    类型
     * @param layerMark		    层级标志(local：本级   next：本级的下一级[为空顶级]  all：本级下的所有级[为空所有])
     * @param layer			    层级
     * @param showInNav		    是否显示
     * @param keyword		    关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    SerializeObject<DataTable<ArticleClassBean>> query(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("pageNum") Integer pageNum,
            @RequestParam("pageSize") Integer pageSize,
            @RequestParam("code") String code,
            @RequestParam("type") Integer type,
            @RequestParam("layerMark") String layerMark,
            @RequestParam("layer") String layer,
            @RequestParam("showInNav") Integer showInNav,
            @RequestParam("keyword") String keyword);

    /**
     * 验证是否存在
     * @param accessToken	登录成功后分配的Key
     * @param param         参数集合
     * @return			    是否验证通过
     */
    @RequestMapping(value = "verify",method = GET)
    SerializeObject verify(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam Map<String, Object> param);

    /**
     * 根据code查找信息
     * @param accessToken	登录成功后分配的Key
     * @param code			编号
     * @return			    记录集
     */
    @RequestMapping(value = "findByCode", method = GET)
    SerializeObject<ArticleClassBean> findByCode(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("code") String code);

    /**
     * 根据code查找信息
     * @param accessToken	登录成功后分配的Key
     * @param layer			分层级( null:返回顶级数据  layer___：获取layer子节点)
     * @return			    记录集
     */
    @RequestMapping(value = "findListByLayer", method = GET)
    SerializeObject<List<ArticleClassBean>> findListByLayer(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("layer") String layer);

    /**
     * 根据层级获取数据{所有子节点}
     * @param accessToken		登录成功后分配的Key
     * @param type				分类
     * @param layer				分层级( null:返回所有数据  layer%：获取layer所有子节点)
     * @return			      	数据集
     */
    @RequestMapping(value = "findListByAllLayer", method = GET)
    SerializeObject<List<ArticleClassBean>> findListByAllLayer(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("type") Integer type,
            @RequestParam("layer") String layer);

    /**
     * 根据层级获取数据{本级}
     * @param accessToken		登录成功后分配的Key
     * @param type				分类
     * @param layer				层级
     * @return			      	数据集
     */
    @RequestMapping(value = "findByLayer", method = GET)
    SerializeObject<ArticleClassBean> findByLayer(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("type") Integer type,
            @RequestParam("layer") String layer);

    /**
     * 获取所有层级数据
     * @param accessToken		登录成功后分配的Key
     * @return			      	数据集
     */
    @RequestMapping(value = "findAllList", method = GET)
    SerializeObject<List<ArticleClassBean>> findAllList(
            @RequestHeader("accessToken") String accessToken);

}
