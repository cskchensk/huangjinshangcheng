package cn.ug.info.bean.type;

import cn.ug.bean.type.BaseType;

/**
 * 类型
 * @author kaiwotech
 */
public class ArticleClassType extends BaseType {
	/** 系统分类 */
	public static final int SYSTEM 		= 1;
	/** 信息 */
	public static final int INFO 		= 2;
	/** 帮助分类 */
	public static final int HELP 		= 3;
	/** 商品分类 */
	public static final int PRODUCT 	= 4;
}
