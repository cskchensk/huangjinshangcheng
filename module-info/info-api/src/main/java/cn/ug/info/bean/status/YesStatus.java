package cn.ug.info.bean.status;

import cn.ug.bean.status.BaseStatus;

/**
 * 状态类型
 * @author kaiwotech
 */
public class YesStatus extends BaseStatus {
    /** 是否默认_是 */
    public static final int YES 	    = 1;
    /** 是否默认_否 */
    public static final int NO     	    = 2;
}
