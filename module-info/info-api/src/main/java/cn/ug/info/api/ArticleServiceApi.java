package cn.ug.info.api;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.info.bean.ArticleBean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RequestMapping("article")
public interface ArticleServiceApi {

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<ArticleBean> find(
            @RequestHeader("accessToken") String accessToken,
            @PathVariable("id") String id);

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken       登录成功后分配的Key
     * @param param             参数集合
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    SerializeObject update(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam Map<String, Object> param);

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(method = DELETE)
    SerializeObject delete(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("id") String[] id);

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "remove", method = PUT)
    SerializeObject remove(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("id") String[] id);

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序字段
     * @param sort		        排序方式 desc或asc
     * @param articleClassId	分类ID
     * @param status			状态
     * @param keyword			关键字
     * @param containContent	包含Content 1：是 2：否
     * @return			        列表数据
     */
    @RequestMapping(value = "list" , method = GET)
    SerializeObject<List<ArticleBean>> list(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("articleClassId") String articleClassId,
            @RequestParam("status") Integer status,
            @RequestParam("keyword") String keyword,
            @RequestParam("containContent") Integer containContent);

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序字段
     * @param sort		        排序方式 desc或asc
     * @param pageNum	        查询页数
     * @param pageSize	        每页记录数
     * @param articleClassId	分类ID
     * @param status			状态
     * @param keyword			关键字
     * @param containContent	包含Content 1：是 2：否
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    SerializeObject<DataTable<ArticleBean>> query(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("pageNum") Integer pageNum,
            @RequestParam("pageSize") Integer pageSize,
            @RequestParam("articleClassId") String articleClassId,
            @RequestParam("status") Integer status,
            @RequestParam("keyword") String keyword,
            @RequestParam("containContent") Integer containContent);

    /**
     * 验证是否存在
     * @param accessToken	登录成功后分配的Key
     * @param param         参数集合
     * @return			    是否验证通过
     */
    @RequestMapping(value = "verify",method = GET)
    SerializeObject verify(
            @RequestHeader("accessToken") String accessToken,
            @RequestParam Map<String, Object> param);
    
}
