package cn.ug.bean.status;

/**
 * 删除状态类型
 * @author kaiwotech
 */
public class DeleteStatus extends BaseStatus {
    /** 正常(未锁定) */
    public static final int NO 	= 1;
    /** 锁定 */
    public static final int YES	= 2;
}
