package cn.ug.bean;


import cn.ug.bean.type.LoginSource;
import cn.ug.bean.type.LoginType;
import cn.ug.bean.type.OnlineType;
import cn.ug.bean.type.YesNo;

import java.util.Set;

/**
 * 登录账号信息
 * @author kaiwotech
 */
public class LoginBean implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private int loginType 			= LoginType.ADMINISTRATOR;
	private int loginSource 		= LoginSource.WEB;
	private String accessToken		= "";
	private String refreshToken		= "";
	private String id 				= "";
	private String loginName 		= "";
	private String headImage		= "";
	private String name 			= "";
	private String mobile 			= "";
	private String inviteCode      = "";
	private String ip 				= "";
	private String hostName 		= "";
	private long loginTime			= 0;
	private long updateTimeMillis 	= 0;
	private int onlineType 			= OnlineType.ONLINE;

	// 扩展属性
    /** 是否绑卡 1：是 2：否 */
    private Integer bindCard        = YesNo.NO.getValue();
    /** 是否交易密码 1：是 2：否 */
    private Integer bindPayPassword = YesNo.NO.getValue();
	/** 是否设置收货地址 1：是 2：否 */
	private Integer bindAddress = YesNo.NO.getValue();

	/** 权限列表 */
	private Set<String> authorizeSet;

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public int getLoginType() {
		return loginType;
	}

	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}

	public int getLoginSource() {
		return loginSource;
	}

	public void setLoginSource(int loginSource) {
		this.loginSource = loginSource;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public long getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(long loginTime) {
		this.loginTime = loginTime;
	}

	public String getHeadImage() {
		return headImage;
	}

	public void setHeadImage(String headImage) {
		this.headImage = headImage;
	}

	public long getUpdateTimeMillis() {
		return updateTimeMillis;
	}

	public void setUpdateTimeMillis(long updateTimeMillis) {
		this.updateTimeMillis = updateTimeMillis;
	}

	public int getOnlineType() {
		return onlineType;
	}

	public void setOnlineType(int onlineType) {
		this.onlineType = onlineType;
	}

	public Set<String> getAuthorizeSet() {
		return authorizeSet;
	}

	public void setAuthorizeSet(Set<String> authorizeSet) {
		this.authorizeSet = authorizeSet;
	}

	public Integer getBindCard() {
		return bindCard;
	}

	public void setBindCard(Integer bindCard) {
		this.bindCard = bindCard;
	}

	public Integer getBindPayPassword() {
		return bindPayPassword;
	}

	public void setBindPayPassword(Integer bindPayPassword) {
		this.bindPayPassword = bindPayPassword;
	}

	public Integer getBindAddress() {
		return bindAddress;
	}

	public void setBindAddress(Integer bindAddress) {
		this.bindAddress = bindAddress;
	}

}