package cn.ug.bean.base;


import java.util.List;
import java.util.Map;

public class DataOtherTable<T> extends DataTable{

    private Map<String,Object> resultMap = null;

    public DataOtherTable(int pageNum, int pageSize, long total,List<T> dataList, Map<String, Object> resultMap) {
        this.resultMap = resultMap;
        Page page = super.getPage();
        page.setPageNum(pageNum);
        page.setPageSize(pageSize);
        page.setTotal(total);
        super.setPage(page);
        super.setDataList(dataList);
    }

    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    public void setResultMap(Map<String, Object> resultMap) {
        this.resultMap = resultMap;
    }
}
