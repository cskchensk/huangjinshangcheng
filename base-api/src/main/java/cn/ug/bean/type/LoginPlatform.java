package cn.ug.bean.type;

public enum LoginPlatform {
    APP,
    WEB;

    LoginPlatform() {

    }
}
