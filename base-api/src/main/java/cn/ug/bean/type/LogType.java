package cn.ug.bean.type;

/**
 * 登录状态
 */
public enum LogType {
    LOGIN("登录"),
    LOGOUT("注销"),
    WITHDRAW_APPLY("提现申请"),
    WITHDRAW_AUDIT("提现审核"),
    WITHDRAW_START("提现发起");

    private String result = "";

    LogType(String i){
        this.result = i;
    }

    @Override
    public String toString() {
        return result;
    }
}
