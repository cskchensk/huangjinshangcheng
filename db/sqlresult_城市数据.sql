﻿
use ugold;
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110000','110000','北京市','bjs',0,'0',116.4052850000,39.9049890000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110100','110100','市辖区','sxq',1,'110000',116.4052850000,39.9049890000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110101','110101','东城区','dcq',1,'110100',116.4371090000,39.8846160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110102','110102','西城区','xcq',1,'110100',116.3272710000,39.8854210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110103','110103','崇文区','cwq',1,'110100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110104','110104','宣武区','xwq',1,'110100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110105','110105','朝阳区','cyq',1,'110100',116.5239120000,39.9161580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110106','110106','丰台区','ftq',1,'110100',116.1616540000,39.8104730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110107','110107','石景山区','sjsq',1,'110100',116.1598210000,39.9332730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110108','110108','海淀区','hdq',1,'110100',116.3674100000,39.9958340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110109','110109','门头沟区','mtgq',1,'110100',116.1148880000,39.9074910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110111','110111','房山区','fsq',1,'110100',115.9577370000,39.7257320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110112','110112','通州区','tzq',1,'110100',116.6549110000,39.8995500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110113','110113','顺义区','syq',1,'110100',116.6536590000,40.1220670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110114','110114','昌平区','cpq',1,'110100',116.4145840000,40.0572590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110115','110115','大兴区','dxq',1,'110100',116.3393660000,39.7372250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110116','110116','怀柔区','hrq',1,'110100',116.6863610000,40.2899470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110117','110117','平谷区','pgq',1,'110100',117.1149950000,40.1415640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110200','110200','县','x',1,'110000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110228','110228','密云县','myx',2,'110200',116.8433520000,40.3773620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('110229','110229','延庆县','yqx',2,'110200',116.1659780000,40.5263850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120000','120000','天津市','tjs',0,'0',117.1901820000,39.1255960000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120100','120100','市辖区','sxq',1,'120000',117.1901820000,39.1255960000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120101','120101','和平区','hpq',1,'120100',117.2139890000,39.1173270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120102','120102','河东区','hdq',1,'120100',117.2887390000,39.0961180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120103','120103','河西区','hxq',1,'120100',117.2077370000,39.0873850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120104','120104','南开区','nkq',1,'120100',117.1703730000,39.0956790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120105','120105','河北区','hbq',1,'120100',117.2495810000,39.1667620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120106','120106','红桥区','hqq',1,'120100',117.1453580000,39.1532930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120107','120107','塘沽区','tgq',1,'120100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120108','120108','汉沽区','hgq',1,'120100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120109','120109','大港区','dgq',1,'120100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120110','120110','东丽区','dlq',1,'120100',117.3699490000,39.0793820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120111','120111','西青区','xqq',1,'120100',117.0880990000,39.1264980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120112','120112','津南区','jnq',1,'120100',117.3355020000,39.0257440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120113','120113','北辰区','bcq',1,'120100',117.2222390000,39.2068550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120114','120114','武清区','wqq',1,'120100',117.0643570000,39.3773460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120115','120115','宝坻区','bdq',1,'120100',117.3087980000,39.7194000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120200','120200','县','x',1,'120000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120221','120221','宁河县','nhx',2,'120200',117.4410240000,39.2707940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120223','120223','静海县','jhx',2,'120200',116.7724220000,38.8284870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('120225','120225','蓟县','jx',2,'120200',117.4420880000,40.1887660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130000','130000','河北省','hbs',0,'0',114.5024610000,38.0454740000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130100','130100','石家庄市','sjzs',1,'130000',114.5024610000,38.0454740000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130101','130101','市辖区','sxq',1,'130100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130102','130102','长安区','caq',1,'130100',114.5986350000,38.0729550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130103','130103','桥东区','qdq',1,'130100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130104','130104','桥西区','qxq',1,'130100',114.4376410000,38.0417680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130105','130105','新华区','xhq',1,'130100',114.4766210000,38.0933510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130107','130107','井陉矿区','jxkq',1,'130100',114.0581780000,38.0697480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130108','130108','裕华区','yhq',1,'130100',114.5320350000,38.0253480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130121','130121','井陉县','jxx',2,'130100',114.1444880000,38.0336140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130123','130123','正定县','zdx',2,'130100',114.5698870000,38.1478350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130124','130124','栾城县','lcx',2,'130100',114.6542810000,37.8869110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130125','130125','行唐县','xtx',2,'130100',114.5527340000,38.4374220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130126','130126','灵寿县','lsx',2,'130100',114.3794600000,38.3065460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130127','130127','高邑县','gyx',2,'130100',114.6106990000,37.6057140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130128','130128','深泽县','szx',2,'130100',115.2002070000,38.1845400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130129','130129','赞皇县','zhx',2,'130100',114.3877560000,37.6601990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130130','130130','无极县','wjx',2,'130100',114.9778450000,38.1763760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130131','130131','平山县','psx',2,'130100',114.1841440000,38.2593110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130132','130132','元氏县','ysx',2,'130100',114.5261800000,37.7625140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130133','130133','赵县','zx',2,'130100',114.7753620000,37.7543410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130181','130181','辛集市','xjs',2,'130100',115.2194500000,37.9179780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130182','130182','藁城市','gcs',2,'130100',114.8496470000,38.0337670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130183','130183','晋州市','jzs',2,'130100',115.0448860000,38.0274780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130184','130184','新乐市','xls',2,'130100',114.6857800000,38.3447680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130185','130185','鹿泉市','lqs',2,'130100',114.3210230000,38.0939940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130200','130200','唐山市','tss',1,'130000',118.1753930000,39.6351130000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130201','130201','市辖区','sxq',1,'130200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130202','130202','路南区','lnq',1,'130200',118.2108210000,39.6151620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130203','130203','路北区','lbq',1,'130200',118.1747360000,39.6285380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130204','130204','古冶区','gyq',1,'130200',118.4542900000,39.7157360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130205','130205','开平区','kpq',1,'130200',118.2644250000,39.6761710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130207','130207','丰南区','fnq',1,'130200',118.1107930000,39.5630300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130208','130208','丰润区','frq',1,'130200',118.1557790000,39.8313630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130223','130223','滦县','lx',2,'130200',118.6995470000,39.7448510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130224','130224','滦南县','lnx',2,'130200',118.6815520000,39.5062010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130225','130225','乐亭县','ltx',2,'130200',118.9053410000,39.4281300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130227','130227','迁西县','qxx',2,'130200',118.3051390000,40.1462380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130229','130229','玉田县','ytx',2,'130200',117.7536650000,39.8873230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130230','130230','唐海县','thx',2,'130200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130281','130281','遵化市','zhs',2,'130200',117.9658750000,40.1886160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130283','130283','迁安市','qas',2,'130200',118.7019330000,40.0121080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130300','130300','秦皇岛市','qhds',1,'130000',119.5865790000,39.9425310000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130301','130301','市辖区','sxq',1,'130300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130302','130302','海港区','hgq',1,'130300',119.6064170000,39.9320480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130303','130303','山海关区','shgq',1,'130300',119.7535910000,39.9980230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130304','130304','北戴河区','bdhq',1,'130300',119.4862860000,39.8251210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130321','130321','青龙满族自治县','qlmzzzx',2,'130300',118.9545550000,40.4060230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130322','130322','昌黎县','clx',2,'130300',119.1645410000,39.7097290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130323','130323','抚宁县','fnx',2,'130300',119.2406510000,39.8870530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130324','130324','卢龙县','llx',2,'130300',118.8818090000,39.8916390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130400','130400','邯郸市','hds',1,'130000',114.4906860000,36.6122730000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130401','130401','市辖区','sxq',1,'130400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130402','130402','邯山区','hsq',1,'130400',114.4849890000,36.6031960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130403','130403','丛台区','ctq',1,'130400',114.4947030000,36.6110820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130404','130404','复兴区','fxq',1,'130400',114.4582420000,36.6154840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130406','130406','峰峰矿区','ffkq',1,'130400',114.2099360000,36.4204870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130421','130421','邯郸县','hdx',2,'130400',114.5310830000,36.5939050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130423','130423','临漳县','lzx',2,'130400',114.6107030000,36.3376040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130424','130424','成安县','cax',2,'130400',114.6803560000,36.4438320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130425','130425','大名县','dmx',2,'130400',115.1525860000,36.2833160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130426','130426','涉县','sx',2,'130400',113.6732970000,36.5631430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130427','130427','磁县','cx',2,'130400',114.3820800000,36.3676730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130428','130428','肥乡县','fxx',2,'130400',114.8051540000,36.5557780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130429','130429','永年县','ynx',2,'130400',114.4961620000,36.7764130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130430','130430','邱县','qx',2,'130400',115.1685840000,36.8132500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130431','130431','鸡泽县','jzx',2,'130400',114.8785170000,36.9149080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130432','130432','广平县','gpx',2,'130400',114.9508590000,36.4836030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130433','130433','馆陶县','gtx',2,'130400',115.2890570000,36.5394610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130434','130434','魏县','wx',2,'130400',114.9341100000,36.3542480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130435','130435','曲周县','qzx',2,'130400',114.9575880000,36.7733980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130481','130481','武安市','was',2,'130400',114.1945810000,36.6961150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130500','130500','邢台市','xts',1,'130000',114.5088510000,37.0682000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130501','130501','市辖区','sxq',1,'130500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130502','130502','桥东区','qdq',1,'130500',114.5071310000,37.0641250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130503','130503','桥西区','qxq',1,'130500',114.4736870000,37.0680090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130521','130521','邢台县','xtx',2,'130500',114.5611320000,37.0507300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130522','130522','临城县','lcx',2,'130500',114.5068730000,37.4440090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130523','130523','内丘县','nqx',2,'130500',114.5115230000,37.2876630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130524','130524','柏乡县','bxx',2,'130500',114.6933820000,37.4835960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130525','130525','隆尧县','lyx',2,'130500',114.7763480000,37.3509250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130526','130526','任县','rx',2,'130500',114.6844690000,37.1299520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130527','130527','南和县','nhx',2,'130500',114.6913770000,37.0038120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130528','130528','宁晋县','njx',2,'130500',114.9210270000,37.6189560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130529','130529','巨鹿县','jlx',2,'130500',115.0387820000,37.2176800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130530','130530','新河县','xhx',2,'130500',115.2475370000,37.5262160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130531','130531','广宗县','gzx',2,'130500',115.1427970000,37.0755480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130532','130532','平乡县','pxx',2,'130500',115.0292180000,37.0694040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130533','130533','威县','wx',2,'130500',115.2727490000,36.9832720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130534','130534','清河县','qhx',2,'130500',115.6689990000,37.0599910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130535','130535','临西县','lxx',2,'130500',115.4986840000,36.8642000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130581','130581','南宫市','ngs',2,'130500',115.3981020000,37.3596680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130582','130582','沙河市','shs',2,'130500',114.5049020000,36.8619030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130600','130600','保定市','bds',1,'130000',115.4823310000,38.8676570000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130601','130601','市辖区','sxq',1,'130600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130602','130602','新市区','xsq',1,'130600',115.4706590000,38.8866200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130603','130603','北市区','bsq',1,'130600',115.5009340000,38.8650050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130604','130604','南市区','nsq',1,'130600',115.4986740000,38.8567020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130621','130621','满城县','mcx',2,'130600',115.3244200000,38.9513800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130622','130622','清苑县','qyx',2,'130600',115.4922210000,38.7710120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130623','130623','涞水县','lsx',2,'130600',115.7119850000,39.3931480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130624','130624','阜平县','fpx',2,'130600',114.1988010000,38.8472760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130625','130625','徐水县','xsx',2,'130600',115.6494100000,39.0203950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130626','130626','定兴县','dxx',2,'130600',115.7968950000,39.2661950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130627','130627','唐县','tx',2,'130600',114.9812410000,38.7485420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130628','130628','高阳县','gyx',2,'130600',115.7788780000,38.6900920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130629','130629','容城县','rcx',2,'130600',115.8662470000,39.0528200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130630','130630','涞源县','lyx',2,'130600',114.6925670000,39.3575500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130631','130631','望都县','wdx',2,'130600',115.1540090000,38.7074480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130632','130632','安新县','axx',2,'130600',115.9319790000,38.9299120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130633','130633','易县','yx',2,'130600',115.5011460000,39.3529700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130634','130634','曲阳县','qyx',2,'130600',114.7040550000,38.6199920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130635','130635','蠡县','lx',2,'130600',115.5836310000,38.4964290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130636','130636','顺平县','spx',2,'130600',115.1327490000,38.8451270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130637','130637','博野县','byx',2,'130600',115.4617980000,38.4582710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130638','130638','雄县','xx',2,'130600',116.1074740000,38.9908190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130681','130681','涿州市','zzs',2,'130600',115.9734090000,39.4857650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130682','130682','定州市','dzs',2,'130600',114.9913890000,38.5176020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130683','130683','安国市','ags',2,'130600',115.3314100000,38.4213670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130684','130684','高碑店市','gbds',2,'130600',115.8827040000,39.3276890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130700','130700','张家口市','zjks',1,'130000',114.8840910000,40.8119010000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130701','130701','市辖区','sxq',1,'130700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130702','130702','桥东区','qdq',1,'130700',114.8856580000,40.8138750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130703','130703','桥西区','qxq',1,'130700',114.8821270000,40.8243850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130705','130705','宣化区','xhq',1,'130700',115.0632000000,40.6093680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130706','130706','下花园区','xhyq',1,'130700',115.2824850000,40.4901080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130721','130721','宣化县','xhx',2,'130700',115.0330800000,40.5622110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130722','130722','张北县','zbx',2,'130700',114.7159510000,41.1517130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130723','130723','康保县','kbx',2,'130700',114.6158090000,41.8500460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130724','130724','沽源县','gyx',2,'130700',115.6848360000,41.6674190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130725','130725','尚义县','syx',2,'130700',113.9777130000,41.0800910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130726','130726','蔚县','yx',2,'130700',114.5826950000,39.8371810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130727','130727','阳原县','yyx',2,'130700',114.1673430000,40.1134190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130728','130728','怀安县','hax',2,'130700',114.4223640000,40.6712740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130729','130729','万全县','wqx',2,'130700',114.7361310000,40.7651360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130730','130730','怀来县','hlx',2,'130700',115.5208460000,40.4054050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130731','130731','涿鹿县','zlx',2,'130700',115.2192460000,40.3787010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130732','130732','赤城县','ccx',2,'130700',115.8327080000,40.9120810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130733','130733','崇礼县','clx',2,'130700',115.2816520000,40.9713020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130800','130800','承德市','cds',1,'130000',117.9391520000,40.9762040000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130801','130801','市辖区','sxq',1,'130800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130802','130802','双桥区','sqq',1,'130800',117.9391520000,40.9762040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130803','130803','双滦区','slq',1,'130800',117.7974850000,40.9597560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130804','130804','鹰手营子矿区','ysyzkq',1,'130800',117.6611540000,40.5469560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130821','130821','承德县','cdx',2,'130800',118.1724960000,40.7686370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130822','130822','兴隆县','xlx',2,'130800',117.5070980000,40.4185250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130823','130823','平泉县','pqx',2,'130800',118.6902380000,41.0056100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130824','130824','滦平县','lpx',2,'130800',117.3371240000,40.9366440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130825','130825','隆化县','lhx',2,'130800',117.7363430000,41.3166670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130826','130826','丰宁满族自治县','fnmzzzx',2,'130800',116.6512100000,41.2099030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130827','130827','宽城满族自治县','kcmzzzx',2,'130800',118.4886420000,40.6079810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130828','130828','围场满族蒙古族自治县','wcmzmgzzzx',2,'130800',117.7640860000,41.9494040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130900','130900','沧州市','czs',1,'130000',116.8574610000,38.3105820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130901','130901','市辖区','sxq',1,'130900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130902','130902','新华区','xhq',1,'130900',116.8730490000,38.3082730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130903','130903','运河区','yhq',1,'130900',116.8400630000,38.3074050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130921','130921','沧县','cx',2,'130900',117.0074780000,38.2198560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130922','130922','青县','qx',2,'130900',116.8383840000,38.5696460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130923','130923','东光县','dgx',2,'130900',116.5420620000,37.8865500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130924','130924','海兴县','hxx',2,'130900',117.4966060000,38.1415820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130925','130925','盐山县','ysx',2,'130900',117.2298140000,38.0561410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130926','130926','肃宁县','snx',2,'130900',115.8358560000,38.4271020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130927','130927','南皮县','npx',2,'130900',116.7091710000,38.0424390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130928','130928','吴桥县','wqx',2,'130900',116.3915120000,37.6281820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130929','130929','献县','xx',2,'130900',116.1238440000,38.1896610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130930','130930','孟村回族自治县','mchzzzx',2,'130900',117.1051040000,38.0579530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130981','130981','泊头市','bts',2,'130900',116.5701630000,38.0734790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130982','130982','任丘市','rqs',2,'130900',116.1067640000,38.7065130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130983','130983','黄骅市','hhs',2,'130900',117.3438030000,38.3692380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('130984','130984','河间市','hjs',2,'130900',116.0894520000,38.4414900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131000','131000','廊坊市','lfs',1,'130000',116.7044410000,39.5239270000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131001','131001','市辖区','sxq',1,'131000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131002','131002','安次区','acq',1,'131000',116.6945440000,39.5025690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131003','131003','广阳区','gyq',1,'131000',116.7137080000,39.5219310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131022','131022','固安县','gax',2,'131000',116.2998940000,39.4364680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131023','131023','永清县','yqx',2,'131000',116.4980890000,39.3197170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131024','131024','香河县','xhx',2,'131000',117.0071610000,39.7572120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131025','131025','大城县','dcx',2,'131000',116.6407350000,38.6992150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131026','131026','文安县','wax',2,'131000',116.4601070000,38.8668010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131028','131028','大厂回族自治县','dchzzzx',2,'131000',116.9865010000,39.8892660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131081','131081','霸州市','bzs',2,'131000',116.3920210000,39.1173310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131082','131082','三河市','shs',2,'131000',117.0770180000,39.9827780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131100','131100','衡水市','hss',1,'130000',115.6659930000,37.7350970000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131101','131101','市辖区','sxq',1,'131100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131102','131102','桃城区','tcq',1,'131100',115.6949450000,37.7322370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131121','131121','枣强县','zqx',2,'131100',115.7264990000,37.5115120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131122','131122','武邑县','wyx',2,'131100',115.8924150000,37.8037740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131123','131123','武强县','wqx',2,'131100',115.9702360000,38.0369800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131124','131124','饶阳县','ryx',2,'131100',115.7265770000,38.2326710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131125','131125','安平县','apx',2,'131100',115.5196270000,38.2335110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131126','131126','故城县','gcx',2,'131100',115.9667470000,37.3509810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131127','131127','景县','jx',2,'131100',116.2584460000,37.6866220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131128','131128','阜城县','fcx',2,'131100',116.1647270000,37.8699450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131181','131181','冀州市','jzs',2,'131100',115.5791730000,37.5427880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('131182','131182','深州市','szs',2,'131100',115.5545960000,38.0034700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140000','140000','山西省','sxs',0,'0',112.5492480000,37.8570140000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140100','140100','太原市','tys',1,'140000',112.5492480000,37.8570140000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140101','140101','市辖区','sxq',1,'140100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140105','140105','小店区','xdq',1,'140100',112.5815570000,37.8057490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140106','140106','迎泽区','yzq',1,'140100',112.5801080000,37.8646940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140107','140107','杏花岭区','xhlq',1,'140100',112.5568250000,37.8763350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140108','140108','尖草坪区','jcpq',1,'140100',112.5349820000,37.9550660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140109','140109','万柏林区','wblq',1,'140100',112.5096450000,37.8565040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140110','140110','晋源区','jyq',1,'140100',112.4810140000,37.7582660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140121','140121','清徐县','qxx',2,'140100',112.3579610000,37.6072900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140122','140122','阳曲县','yqx',2,'140100',112.6738180000,38.0587970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140123','140123','娄烦县','lfx',2,'140100',111.7937980000,38.0660350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140181','140181','古交市','gjs',2,'140100',112.1743530000,37.9085340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140200','140200','大同市','dts',1,'140000',113.2952590000,40.0903100000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140201','140201','市辖区','sxq',1,'140200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140202','140202','城区','cq',1,'140200',113.3014380000,40.0905110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140203','140203','矿区','kq',1,'140200',113.1686560000,40.0362600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140211','140211','南郊区','njq',1,'140200',113.1689200000,40.0180200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140212','140212','新荣区','xrq',1,'140200',113.1410440000,40.2582690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140221','140221','阳高县','ygx',2,'140200',113.7498710000,40.3649270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140222','140222','天镇县','tzx',2,'140200',114.0911200000,40.4213360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140223','140223','广灵县','glx',2,'140200',114.2792520000,39.7630510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140224','140224','灵丘县','lqx',2,'140200',114.2357600000,39.4388670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140225','140225','浑源县','hyx',2,'140200',113.6980910000,39.6990990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140226','140226','左云县','zyx',2,'140200',112.7064100000,40.0128730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140227','140227','大同县','dtx',2,'140200',113.6113060000,40.0393450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140300','140300','阳泉市','yqs',1,'140000',113.5832850000,37.8611880000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140301','140301','市辖区','sxq',1,'140300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140302','140302','城区','cq',1,'140300',113.5865130000,37.8609380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140303','140303','矿区','kq',1,'140300',113.5590660000,37.8700850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140311','140311','郊区','jq',1,'140300',113.5866400000,37.9409600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140321','140321','平定县','pdx',2,'140300',113.6310490000,37.8002890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140322','140322','盂县','yx',2,'140300',113.4122300000,38.0861310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140400','140400','长治市','czs',1,'140000',113.1135560000,36.1911120000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140401','140401','市辖区','sxq',1,'140400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140402','140402','城区','cq',1,'140400',113.1141070000,36.1878960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140411','140411','郊区','jq',1,'140400',113.1012110000,36.2183880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140421','140421','长治县','czx',2,'140400',113.0566790000,36.0524380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140423','140423','襄垣县','xyx',2,'140400',113.0500940000,36.5328540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140424','140424','屯留县','tlx',2,'140400',112.8927410000,36.3140720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140425','140425','平顺县','psx',2,'140400',113.4387910000,36.2002020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140426','140426','黎城县','lcx',2,'140400',113.3873660000,36.5029710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140427','140427','壶关县','hgx',2,'140400',113.2061380000,36.1109380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140428','140428','长子县','czx',2,'140400',112.8846560000,36.1194840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140429','140429','武乡县','wxx',2,'140400',112.8653000000,36.8343150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140430','140430','沁县','qx',2,'140400',112.7013800000,36.7571230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140431','140431','沁源县','qyx',2,'140400',112.3408780000,36.5007770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140481','140481','潞城市','lcs',2,'140400',113.2232450000,36.3322330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140500','140500','晋城市','jcs',1,'140000',112.8512740000,35.4975530000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140501','140501','市辖区','sxq',1,'140500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140502','140502','城区','cq',1,'140500',112.8531060000,35.4966410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140521','140521','沁水县','qsx',2,'140500',112.1872130000,35.6894720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140522','140522','阳城县','ycx',2,'140500',112.4220140000,35.4821770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140524','140524','陵川县','lcx',2,'140500',113.2788770000,35.7756140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140525','140525','泽州县','zzx',2,'140500',112.8991370000,35.6172210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140581','140581','高平市','gps',2,'140500',112.9306910000,35.7913550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140600','140600','朔州市','szs',1,'140000',112.4333870000,39.3312610000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140601','140601','市辖区','sxq',1,'140600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140602','140602','朔城区','scq',1,'140600',112.4286760000,39.3245250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140603','140603','平鲁区','plq',1,'140600',112.2952270000,39.5156030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140621','140621','山阴县','syx',2,'140600',112.8163960000,39.5267700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140622','140622','应县','yx',2,'140600',113.1875050000,39.5591870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140623','140623','右玉县','yyx',2,'140600',112.4655880000,39.9888120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140624','140624','怀仁县','hrx',2,'140600',113.1005120000,39.8207900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140700','140700','晋中市','jzs',1,'140000',112.7364650000,37.6964950000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140701','140701','市辖区','sxq',1,'140700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140702','140702','榆次区','ycq',1,'140700',112.7400560000,37.6976000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140721','140721','榆社县','ysx',2,'140700',112.9735210000,37.0690190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140722','140722','左权县','zqx',2,'140700',113.3778340000,37.0796720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140723','140723','和顺县','hsx',2,'140700',113.5729190000,37.3270270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140724','140724','昔阳县','xyx',2,'140700',113.7061660000,37.6043700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140725','140725','寿阳县','syx',2,'140700',113.1777080000,37.8911360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140726','140726','太谷县','tgx',2,'140700',112.5541030000,37.4245950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140727','140727','祁县','qx',2,'140700',112.3305320000,37.3587390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140728','140728','平遥县','pyx',2,'140700',112.1740590000,37.1954740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140729','140729','灵石县','lsx',2,'140700',111.7727590000,36.8474690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140781','140781','介休市','jxs',2,'140700',111.9138570000,37.0276160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140800','140800','运城市','ycs',1,'140000',111.0039570000,35.0227780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140801','140801','市辖区','sxq',1,'140800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140802','140802','盐湖区','yhq',1,'140800',111.0006270000,35.0256430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140821','140821','临猗县','lyx',2,'140800',110.7749300000,35.1418830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140822','140822','万荣县','wrx',2,'140800',110.8435610000,35.4170420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140823','140823','闻喜县','wxx',2,'140800',111.2203060000,35.3538390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140824','140824','稷山县','jsx',2,'140800',110.9789960000,35.6004120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140825','140825','新绛县','xjx',2,'140800',111.2252050000,35.6136970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140826','140826','绛县','jx',2,'140800',111.5761820000,35.4904500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140827','140827','垣曲县','yqx',2,'140800',111.6709900000,35.2982930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140828','140828','夏县','xx',2,'140800',111.2231740000,35.1404410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140829','140829','平陆县','plx',2,'140800',111.2123770000,34.8372560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140830','140830','芮城县','rcx',2,'140800',110.6911400000,34.6947690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140881','140881','永济市','yjs',2,'140800',110.4479840000,34.8651250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140882','140882','河津市','hjs',2,'140800',110.7102680000,35.5971500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140900','140900','忻州市','xzs',1,'140000',112.7335380000,38.4176900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140901','140901','市辖区','sxq',1,'140900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140902','140902','忻府区','xfq',1,'140900',112.7341120000,38.4177430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140921','140921','定襄县','dxx',2,'140900',112.9632310000,38.4849480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140922','140922','五台县','wtx',2,'140900',113.2590120000,38.7257110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140923','140923','代县','dx',2,'140900',112.9625190000,39.0651380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140924','140924','繁峙县','fzx',2,'140900',113.2677070000,39.1881040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140925','140925','宁武县','nwx',2,'140900',112.3079360000,39.0017180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140926','140926','静乐县','jlx',2,'140900',111.9402310000,38.3559470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140927','140927','神池县','scx',2,'140900',112.2004380000,39.0884670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140928','140928','五寨县','wzx',2,'140900',111.8410150000,38.9127610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140929','140929','岢岚县','klx',2,'140900',111.5698100000,38.7056250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140930','140930','河曲县','hqx',2,'140900',111.1466090000,39.3818950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140931','140931','保德县','bdx',2,'140900',111.0856880000,39.0225760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140932','140932','偏关县','pgx',2,'140900',111.5004770000,39.4421530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('140981','140981','原平市','yps',2,'140900',112.7131320000,38.7291860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141000','141000','临汾市','lfs',1,'140000',111.5179730000,36.0841500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141001','141001','市辖区','sxq',1,'141000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141002','141002','尧都区','ydq',1,'141000',111.5229450000,36.0803660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141021','141021','曲沃县','qwx',2,'141000',111.4755290000,35.6413870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141022','141022','翼城县','ycx',2,'141000',111.7135080000,35.7386210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141023','141023','襄汾县','xfx',2,'141000',111.4429320000,35.8761390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141024','141024','洪洞县','hdx',2,'141000',111.6736920000,36.2557420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141025','141025','古县','gx',2,'141000',111.9202070000,36.2685500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141026','141026','安泽县','azx',2,'141000',112.2513720000,36.1460320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141027','141027','浮山县','fsx',2,'141000',111.8500390000,35.9713590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141028','141028','吉县','jx',2,'141000',110.6828530000,36.0993550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141029','141029','乡宁县','xnx',2,'141000',110.8573650000,35.9754020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141030','141030','大宁县','dnx',2,'141000',110.7512830000,36.4638300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141031','141031','隰县','xx',2,'141000',110.9358090000,36.6926750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141032','141032','永和县','yhx',2,'141000',110.6312760000,36.7606140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141033','141033','蒲县','px',2,'141000',111.0973300000,36.4116820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141034','141034','汾西县','fxx',2,'141000',111.5630210000,36.6533680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141081','141081','侯马市','hms',2,'141000',111.3712720000,35.6203020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141082','141082','霍州市','hzs',2,'141000',111.7231030000,36.5720200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141100','141100','吕梁市','lls',1,'140000',111.1343350000,37.5243660000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141101','141101','市辖区','sxq',1,'141100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141102','141102','离石区','lsq',1,'141100',111.1344620000,37.5240370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141121','141121','文水县','wsx',2,'141100',112.0325950000,37.4363140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141122','141122','交城县','jcx',2,'141100',112.1591540000,37.5551550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141123','141123','兴县','xx',2,'141100',111.1248160000,38.4641360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141124','141124','临县','lx',2,'141100',110.9959630000,37.9608060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141125','141125','柳林县','llx',2,'141100',110.8961300000,37.4316640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141126','141126','石楼县','slx',2,'141100',110.8371190000,36.9994260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141127','141127','岚县','lx',2,'141100',111.6715550000,38.2786540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141128','141128','方山县','fsx',2,'141100',111.2388850000,37.8926320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141129','141129','中阳县','zyx',2,'141100',111.1933190000,37.3420540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141130','141130','交口县','jkx',2,'141100',111.1831880000,36.9830680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141181','141181','孝义市','xys',2,'141100',111.7815680000,37.1444740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('141182','141182','汾阳市','fys',2,'141100',111.7852730000,37.2677420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150000','150000','内蒙古','nmgzzq',0,'0',111.6708010000,40.8183110000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150100','150100','呼和浩特市','hhhts',1,'150000',111.6708010000,40.8183110000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150101','150101','市辖区','sxq',1,'150100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150102','150102','新城区','xcq',1,'150100',111.6736440000,40.8191580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150103','150103','回民区','hmq',1,'150100',111.6680630000,40.8150060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150104','150104','玉泉区','yqq',1,'150100',111.6601160000,40.7990970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150105','150105','赛罕区','shq',1,'150100',111.7088450000,40.8227800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150121','150121','土默特左旗','tmtzq',2,'150100',111.1336150000,40.7204160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150122','150122','托克托县','tktx',2,'150100',111.1973170000,40.2767290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150123','150123','和林格尔县','hlgex',2,'150100',111.8241430000,40.3802880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150124','150124','清水河县','qshx',2,'150100',111.6722200000,39.9124790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150125','150125','武川县','wcx',2,'150100',111.4565630000,41.0944830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150200','150200','包头市','bts',1,'150000',109.8404050000,40.6581680000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150201','150201','市辖区','sxq',1,'150200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150202','150202','东河区','dhq',1,'150200',110.0268950000,40.5870560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150203','150203','昆都仑区','kdlq',1,'150200',109.8229320000,40.6613450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150204','150204','青山区','qsq',1,'150200',109.8800490000,40.6685580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150205','150205','石拐区','sgq',1,'150200',110.2725650000,40.6720940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150206','150206','白云矿区','bykq',1,'150200',109.9701600000,41.7692460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150207','150207','九原区','jyq',1,'150200',109.9681220000,40.6005810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150221','150221','土默特右旗','tmtyq',2,'150200',110.5267660000,40.5664340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150222','150222','固阳县','gyx',2,'150200',110.0634210000,41.0300040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150223','150223','达尔罕茂明安联合旗','dehmmalhq',2,'150200',110.4384520000,41.7028360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150300','150300','乌海市','whs',1,'150000',106.8255630000,39.6737340000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150301','150301','市辖区','sxq',1,'150300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150302','150302','海勃湾区','hbwq',1,'150300',106.8177620000,39.6735270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150303','150303','海南区','hnq',1,'150300',106.8847890000,39.4415300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150304','150304','乌达区','wdq',1,'150300',106.7227110000,39.5022880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150400','150400','赤峰市','cfs',1,'150000',118.9568060000,42.2753170000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150401','150401','市辖区','sxq',1,'150400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150402','150402','红山区','hsq',1,'150400',118.9610870000,42.2697320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150403','150403','元宝山区','ybsq',1,'150400',119.2898770000,42.0411680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150404','150404','松山区','ssq',1,'150400',118.9389580000,42.2810460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150421','150421','阿鲁科尔沁旗','alkeqq',2,'150400',120.0949690000,43.8787700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150422','150422','巴林左旗','blzq',2,'150400',119.3917370000,43.9807150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150423','150423','巴林右旗','blyq',2,'150400',118.6783470000,43.5289630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150424','150424','林西县','lxx',2,'150400',118.0577500000,43.6053260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150425','150425','克什克腾旗','ksktq',2,'150400',117.5424650000,43.2562330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150426','150426','翁牛特旗','wntq',2,'150400',119.0226190000,42.9371280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150428','150428','喀喇沁旗','klqq',2,'150400',118.7085720000,41.9277800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150429','150429','宁城县','ncx',2,'150400',119.3392420000,41.5986920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150430','150430','敖汉旗','ahq',2,'150400',119.9064860000,42.2870120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150500','150500','通辽市','tls',1,'150000',122.2631190000,43.6174290000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150501','150501','市辖区','sxq',1,'150500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150502','150502','科尔沁区','keqq',1,'150500',122.2640420000,43.6174220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150521','150521','科尔沁左翼中旗','keqzyzq',2,'150500',123.3138730000,44.1271660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150522','150522','科尔沁左翼后旗','keqzyhq',2,'150500',122.3551550000,42.9545640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150523','150523','开鲁县','klx',2,'150500',121.3087970000,43.6024320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150524','150524','库伦旗','klq',2,'150500',121.7748860000,42.7346920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150525','150525','奈曼旗','nmq',2,'150500',120.6625430000,42.8468500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150526','150526','扎鲁特旗','zltq',2,'150500',120.9052750000,44.5552940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150581','150581','霍林郭勒市','hlgls',2,'150500',119.6578620000,45.5323610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150600','150600','鄂尔多斯市','eedss',1,'150000',109.9902900000,39.8171790000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150602','150602','东胜区','dsq',1,'150600',109.9894500000,39.8178800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150621','150621','达拉特旗','dltq',2,'150600',110.0402810000,40.4040760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150622','150622','准格尔旗','zgeq',2,'150600',111.2383320000,39.8652210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150623','150623','鄂托克前旗','etkqq',2,'150600',107.4817200000,38.1832570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150624','150624','鄂托克旗','etkq',2,'150600',107.9826040000,39.0957520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150625','150625','杭锦旗','hjq',2,'150600',108.7363240000,39.8317890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150626','150626','乌审旗','wsq',2,'150600',108.8424540000,38.5966110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150627','150627','伊金霍洛旗','yjhlq',2,'150600',109.7874020000,39.6043120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150700','150700','呼伦贝尔市','hlbes',1,'150000',119.7581680000,49.2153330000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150701','150701','市辖区','sxq',1,'150700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150702','150702','海拉尔区','hleq',1,'150700',119.7649230000,49.2138890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150721','150721','阿荣旗','arq',2,'150700',123.4646150000,48.1305030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150722','150722','莫力达瓦达斡尔族自治旗','mldwdwezzzq',2,'150700',124.5074010000,48.4783850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150723','150723','鄂伦春自治旗','elczzq',2,'150700',123.7256840000,50.5901770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150724','150724','鄂温克族自治旗','ewkzzzq',2,'150700',119.7540410000,49.1432930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150725','150725','陈巴尔虎旗','cbehq',2,'150700',119.4376090000,49.3284220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150726','150726','新巴尔虎左旗','xbehzq',2,'150700',118.2674540000,48.2165710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150727','150727','新巴尔虎右旗','xbehyq',2,'150700',116.8259910000,48.6691340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150781','150781','满洲里市','mzls',2,'150700',117.4555610000,49.5907880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150782','150782','牙克石市','ykss',2,'150700',120.7290050000,49.2870240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150783','150783','扎兰屯市','zlts',2,'150700',122.7444010000,48.0074120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150784','150784','额尔古纳市','eegns',2,'150700',120.1786360000,50.2439000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150785','150785','根河市','ghs',2,'150700',121.5327240000,50.7804540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150800','150800','巴彦淖尔市','bynes',1,'150000',107.4169590000,40.7574020000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150801','150801','市辖区','sxq',1,'150800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150802','150802','临河区','lhq',1,'150800',107.4170180000,40.7570920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150821','150821','五原县','wyx',2,'150800',108.2706580000,41.0976390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150822','150822','磴口县','dkx',2,'150800',107.0060560000,40.3304790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150823','150823','乌拉特前旗','wltqq',2,'150800',108.6568160000,40.7252090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150824','150824','乌拉特中旗','wltzq',2,'150800',108.5152550000,41.5725400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150825','150825','乌拉特后旗','wlthq',2,'150800',107.0749410000,41.0843070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150826','150826','杭锦后旗','hjhq',2,'150800',107.1476820000,40.8887970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150900','150900','乌兰察布市','wlcbs',1,'150000',113.1145430000,41.0341260000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150901','150901','市辖区','sxq',1,'150900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150902','150902','集宁区','jnq',1,'150900',113.1164530000,41.0341340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150921','150921','卓资县','zzx',2,'150900',112.5777020000,40.8957600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150922','150922','化德县','hdx',2,'150900',114.0100800000,41.8993350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150923','150923','商都县','sdx',2,'150900',113.5606430000,41.5601630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150924','150924','兴和县','xhx',2,'150900',113.8340090000,40.8724370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150925','150925','凉城县','lcx',2,'150900',112.5009110000,40.5316270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150926','150926','察哈尔右翼前旗','cheyyqq',2,'150900',113.2119580000,40.7868590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150927','150927','察哈尔右翼中旗','cheyyzq',2,'150900',112.6335630000,41.2742120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150928','150928','察哈尔右翼后旗','cheyyhq',2,'150900',113.1906000000,41.4472130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150929','150929','四子王旗','szwq',2,'150900',111.7012300000,41.5281140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('150981','150981','丰镇市','fzs',2,'150900',113.1634620000,40.4375340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152200','152200','兴安盟','xam',1,'150000',122.0703170000,46.0762680000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152201','152201','乌兰浩特市','wlhts',2,'152200',122.0689750000,46.0772380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152202','152202','阿尔山市','aess',2,'152200',119.9436560000,47.1770000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152221','152221','科尔沁右翼前旗','keqyyqq',2,'152200',121.9575440000,46.0764970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152222','152222','科尔沁右翼中旗','keqyyzq',2,'152200',121.4728180000,45.0596450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152223','152223','扎赉特旗','zltq',2,'152200',122.9093320000,46.7251360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152224','152224','突泉县','tqx',2,'152200',121.5648560000,45.3809860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152500','152500','锡林郭勒盟','xlglm',1,'150000',116.0909960000,43.9440180000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152501','152501','二连浩特市','elhts',2,'152500',111.9798100000,43.6528950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152502','152502','锡林浩特市','xlhts',2,'152500',116.0919030000,43.9443010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152522','152522','阿巴嘎旗','abgq',2,'152500',114.9706180000,44.0227280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152523','152523','苏尼特左旗','sntzq',2,'152500',113.6534120000,43.8541080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152524','152524','苏尼特右旗','sntyq',2,'152500',112.6553900000,42.7466620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152525','152525','东乌珠穆沁旗','dwzmqq',2,'152500',116.9800220000,45.5103070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152526','152526','西乌珠穆沁旗','xwzmqq',2,'152500',117.6152490000,44.5861470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152527','152527','太仆寺旗','tpsq',2,'152500',115.2872800000,41.8951990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152528','152528','镶黄旗','xhq',2,'152500',113.8438690000,42.2392290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152529','152529','正镶白旗','zxbq',2,'152500',115.0314230000,42.2868070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152530','152530','正蓝旗','zlq',2,'152500',116.0033110000,42.2458950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152531','152531','多伦县','dlx',2,'152500',116.4772880000,42.1979620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152900','152900','阿拉善盟','alsm',1,'150000',105.7064220000,38.8448140000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152921','152921','阿拉善左旗','alszq',2,'152900',105.7019200000,38.8472410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152922','152922','阿拉善右旗','alsyq',2,'152900',101.6719840000,39.2115900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('152923','152923','额济纳旗','ejnq',2,'152900',101.0694400000,41.9588130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210000','210000','辽宁省','lns',0,'0',123.4290960000,41.7967670000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210100','210100','沈阳市','sys',1,'210000',123.4290960000,41.7967670000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210101','210101','市辖区','sxq',1,'210100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210102','210102','和平区','hpq',1,'210100',123.4033450000,41.7910820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210103','210103','沈河区','shq',1,'210100',123.4821650000,41.7843900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210104','210104','大东区','ddq',1,'210100',123.4700310000,41.8048980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210105','210105','皇姑区','hgq',1,'210100',123.4515200000,41.8317620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210106','210106','铁西区','txq',1,'210100',123.1387600000,41.6908910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210111','210111','苏家屯区','sjtq',1,'210100',123.3416040000,41.6659040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210112','210112','东陵区','dlq',1,'210100',123.5063430000,41.6516070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210113','210113','新城子区','xczq',1,'210100',123.5223850000,42.0507100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210114','210114','于洪区','yhq',1,'210100',123.3067060000,41.7603530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210122','210122','辽中县','lzx',2,'210100',122.8941060000,41.5170030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210123','210123','康平县','kpx',2,'210100',123.3527030000,42.7415330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210124','210124','法库县','fkx',2,'210100',123.4167220000,42.5070450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210181','210181','新民市','xms',2,'210100',122.8288680000,41.9965080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210200','210200','大连市','dls',1,'210000',121.6186220000,38.9145900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210201','210201','市辖区','sxq',1,'210200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210202','210202','中山区','zsq',1,'210200',121.6740770000,38.8963090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210203','210203','西岗区','xgq',1,'210200',121.6153900000,38.9103960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210204','210204','沙河口区','shkq',1,'210200',121.5896940000,38.9140180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210211','210211','甘井子区','gjzq',1,'210200',121.5364720000,38.8899530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210212','210212','旅顺口区','lskq',1,'210200',121.1909190000,38.7947430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210213','210213','金州区','jzq',1,'210200',121.7141270000,39.1038910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210224','210224','长海县','chx',2,'210200',122.5878240000,39.2723990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210281','210281','瓦房店市','wfds',2,'210200',122.0026560000,39.6306500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210282','210282','普兰店市','plds',2,'210200',121.9705000000,39.4015550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210283','210283','庄河市','zhs',2,'210200',122.9706120000,39.6982900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210300','210300','鞍山市','ass',1,'210000',122.9956320000,41.1106260000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210301','210301','市辖区','sxq',1,'210300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210302','210302','铁东区','tdq',1,'210300',122.9944750000,41.1103440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210303','210303','铁西区','txq',1,'210300',122.9718340000,41.1106900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210304','210304','立山区','lsq',1,'210300',123.0248060000,41.1506220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210311','210311','千山区','qsq',1,'210300',122.9492980000,41.0689090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210321','210321','台安县','tax',2,'210300',122.4297360000,41.3868600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210323','210323','岫岩满族自治县','xymzzzx',2,'210300',123.2883300000,40.2815090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210381','210381','海城市','hcs',2,'210300',122.7521990000,40.8525330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210400','210400','抚顺市','fss',1,'210000',123.9211090000,41.8759560000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210401','210401','市辖区','sxq',1,'210400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210402','210402','新抚区','xfq',1,'210400',123.9028580000,41.8608200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210403','210403','东洲区','dzq',1,'210400',124.0472190000,41.8668290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210404','210404','望花区','whq',1,'210400',123.8015090000,41.8518030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210411','210411','顺城区','scq',1,'210400',123.9171650000,41.8811320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210421','210421','抚顺县','fsx',2,'210400',124.0979790000,41.9226440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210422','210422','新宾满族自治县','xbmzzzx',2,'210400',125.0375470000,41.7324560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210423','210423','清原满族自治县','qymzzzx',2,'210400',124.9271920000,42.1013500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210500','210500','本溪市','bxs',1,'210000',123.7705190000,41.2979090000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210501','210501','市辖区','sxq',1,'210500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210502','210502','平山区','psq',1,'210500',123.7612310000,41.2915810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210503','210503','溪湖区','xhq',1,'210500',123.7652260000,41.3300560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210504','210504','明山区','msq',1,'210500',123.7632880000,41.3024290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210505','210505','南芬区','nfq',1,'210500',123.7483810000,41.1040930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210521','210521','本溪满族自治县','bxmzzzx',2,'210500',124.1261560000,41.3003440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210522','210522','桓仁满族自治县','hrmzzzx',2,'210500',125.3591950000,41.2689970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210600','210600','丹东市','dds',1,'210000',124.3830440000,40.1242960000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210601','210601','市辖区','sxq',1,'210600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210602','210602','元宝区','ybq',1,'210600',124.3978140000,40.1364830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210603','210603','振兴区','zxq',1,'210600',124.3611530000,40.1028010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210604','210604','振安区','zaq',1,'210600',124.4277090000,40.1585570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210624','210624','宽甸满族自治县','kdmzzzx',2,'210600',124.7848670000,40.7304120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210681','210681','东港市','dgs',2,'210600',124.1494370000,39.8834670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210682','210682','凤城市','fcs',2,'210600',124.0710670000,40.4575670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210700','210700','锦州市','jzs',1,'210000',121.1357420000,41.1192690000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210701','210701','市辖区','sxq',1,'210700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210702','210702','古塔区','gtq',1,'210700',121.1300850000,41.1157190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210703','210703','凌河区','lhq',1,'210700',121.1513040000,41.1146620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210711','210711','太和区','thq',1,'210700',121.1072970000,41.1053780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210726','210726','黑山县','hsx',2,'210700',122.1179150000,41.6918040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210727','210727','义县','yx',2,'210700',121.2428310000,41.5372240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210781','210781','凌海市','lhs',2,'210700',121.3642360000,41.1717380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210782','210782','北宁市','bns',2,'210700',121.7959620000,41.5987640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210800','210800','营口市','yks',1,'210000',122.2351510000,40.6674320000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210801','210801','市辖区','sxq',1,'210800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210802','210802','站前区','zqq',1,'210800',122.2532350000,40.6699490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210803','210803','西市区','xsq',1,'210800',122.2100670000,40.6630860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210804','210804','鲅鱼圈区','byqq',1,'210800',122.1272420000,40.2636460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210811','210811','老边区','lbq',1,'210800',122.3825840000,40.6827230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210881','210881','盖州市','gzs',2,'210800',122.3555340000,40.4052340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210882','210882','大石桥市','dsqs',2,'210800',122.5058940000,40.6339730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210900','210900','阜新市','fxs',1,'210000',121.6489620000,42.0117960000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210901','210901','市辖区','sxq',1,'210900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210902','210902','海州区','hzq',1,'210900',121.6576390000,42.0111620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210903','210903','新邱区','xqq',1,'210900',121.7905410000,42.0866030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210904','210904','太平区','tpq',1,'210900',121.6775750000,42.0111450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210905','210905','清河门区','qhmq',1,'210900',121.4201800000,41.7804770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210911','210911','细河区','xhq',1,'210900',121.6547910000,42.0192180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210921','210921','阜新蒙古族自治县','fxmgzzzx',2,'210900',121.7431250000,42.0586070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('210922','210922','彰武县','zwx',2,'210900',122.5374440000,42.3848230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211000','211000','辽阳市','lys',1,'210000',123.1815200000,41.2694020000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211001','211001','市辖区','sxq',1,'211000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211002','211002','白塔区','btq',1,'211000',123.1726110000,41.2674500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211003','211003','文圣区','wsq',1,'211000',123.1882270000,41.2667650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211004','211004','宏伟区','hwq',1,'211000',123.2004610000,41.2057470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211005','211005','弓长岭区','gclq',1,'211000',123.4316330000,41.1578310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211011','211011','太子河区','tzhq',1,'211000',123.1853360000,41.2516820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211021','211021','辽阳县','lyx',2,'211000',123.0796740000,41.2164790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211081','211081','灯塔市','dts',2,'211000',123.3258640000,41.4278360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211100','211100','盘锦市','pjs',1,'210000',122.0695700000,41.1244840000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211101','211101','市辖区','sxq',1,'211100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211102','211102','双台子区','stzq',1,'211100',122.0557330000,41.1903650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211103','211103','兴隆台区','xltq',1,'211100',122.0716240000,41.1224230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211121','211121','大洼县','dwx',2,'211100',122.0717080000,40.9944280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211122','211122','盘山县','psx',2,'211100',121.9852800000,41.2407010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211200','211200','铁岭市','tls',1,'210000',123.8442790000,42.2905850000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211201','211201','市辖区','sxq',1,'211200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211202','211202','银州区','yzq',1,'211200',123.8448770000,42.2922780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211204','211204','清河区','qhq',1,'211200',124.1489600000,42.5429780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211221','211221','铁岭县','tlx',2,'211200',123.7256690000,42.2233160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211223','211223','西丰县','xfx',2,'211200',124.7233200000,42.7380910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211224','211224','昌图县','ctx',2,'211200',124.1101700000,42.7844410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211281','211281','调兵山市','tbss',2,'211200',123.5453660000,42.4507340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211282','211282','开原市','kys',2,'211200',124.0455510000,42.5421410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211300','211300','朝阳市','cys',1,'210000',120.4511760000,41.5767580000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211301','211301','市辖区','sxq',1,'211300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211302','211302','双塔区','stq',1,'211300',120.4487700000,41.5793890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211303','211303','龙城区','lcq',1,'211300',120.4133760000,41.5767490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211321','211321','朝阳县','cyx',2,'211300',120.4042170000,41.5263420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211322','211322','建平县','jpx',2,'211300',119.6423630000,41.4025760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211324','211324','喀喇沁左翼蒙古族自治县','klqzymgzzzx',2,'211300',119.7448830000,41.1254280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211381','211381','北票市','bps',2,'211300',120.7669510000,41.8032860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211382','211382','凌源市','lys',2,'211300',119.4047890000,41.2430860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211400','211400','葫芦岛市','hlds',1,'210000',120.8563940000,40.7555720000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211401','211401','市辖区','sxq',1,'211400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211402','211402','连山区','lsq',1,'211400',120.8593700000,40.7551430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211403','211403','龙港区','lgq',1,'211400',120.8385690000,40.7099910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211404','211404','南票区','npq',1,'211400',120.7523140000,41.0988130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211421','211421','绥中县','szx',2,'211400',120.3421120000,40.3284070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211422','211422','建昌县','jcx',2,'211400',119.8077760000,40.8128710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('211481','211481','兴城市','xcs',2,'211400',120.7293650000,40.6194130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220000','220000','吉林省','jls',0,'0',125.3245000000,43.8868410000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220100','220100','长春市','ccs',1,'220000',125.3245000000,43.8868410000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220101','220101','市辖区','sxq',1,'220100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220102','220102','南关区','ngq',1,'220100',125.3523580000,43.8645040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220103','220103','宽城区','kcq',1,'220100',125.3215440000,43.9072490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220104','220104','朝阳区','cyq',1,'220100',125.2966520000,43.8877850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220105','220105','二道区','edq',1,'220100',125.3655060000,43.8515580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220106','220106','绿园区','lyq',1,'220100',125.2716180000,43.8775460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220112','220112','双阳区','syq',1,'220100',125.6590180000,43.5251680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220122','220122','农安县','nax',2,'220100',125.1752870000,44.4312580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220181','220181','九台市','jts',2,'220100',125.8334760000,44.1500560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220182','220182','榆树市','yss',2,'220100',126.5501070000,44.8276420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220183','220183','德惠市','dhs',2,'220100',125.7033270000,44.5339090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220200','220200','吉林市','jls',1,'220000',126.5530200000,43.8435770000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220201','220201','市辖区','sxq',1,'220200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220202','220202','昌邑区','cyq',1,'220200',126.5707660000,43.8511180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220203','220203','龙潭区','ltq',1,'220200',126.5614290000,43.9097550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220204','220204','船营区','cyq',1,'220200',126.5523900000,43.8438040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220211','220211','丰满区','fmq',1,'220200',126.5607590000,43.8165940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220221','220221','永吉县','yjx',2,'220200',126.5016220000,43.6674160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220281','220281','蛟河市','jhs',2,'220200',127.3427390000,43.7205790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220282','220282','桦甸市','hds',2,'220200',126.7454450000,42.9720930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220283','220283','舒兰市','sls',2,'220200',126.9478130000,44.4109060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220284','220284','磐石市','pss',2,'220200',126.0599290000,42.9424760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220300','220300','四平市','sps',1,'220000',124.3707850000,43.1703440000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220301','220301','市辖区','sxq',1,'220300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220302','220302','铁西区','txq',1,'220300',124.3608940000,43.1762630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220303','220303','铁东区','tdq',1,'220300',124.3884640000,43.1672600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220322','220322','梨树县','lsx',2,'220300',124.3358020000,43.3083100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220323','220323','伊通满族自治县','ytmzzzx',2,'220300',125.3031240000,43.3454640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220381','220381','公主岭市','gzls',2,'220300',124.8175880000,43.5094740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220382','220382','双辽市','sls',2,'220300',123.5052830000,43.5182750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220400','220400','辽源市','lys',1,'220000',125.1453490000,42.9026920000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220401','220401','市辖区','sxq',1,'220400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220402','220402','龙山区','lsq',1,'220400',125.1451640000,42.9027020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220403','220403','西安区','xaq',1,'220400',125.1514240000,42.9204150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220421','220421','东丰县','dfx',2,'220400',125.5296230000,42.6752280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220422','220422','东辽县','dlx',2,'220400',124.9919950000,42.9277240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220500','220500','通化市','ths',1,'220000',125.9365010000,41.7211770000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220501','220501','市辖区','sxq',1,'220500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220502','220502','东昌区','dcq',1,'220500',125.9367160000,41.7212330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220503','220503','二道江区','edjq',1,'220500',126.0459870000,41.7775640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220521','220521','通化县','thx',2,'220500',125.7531210000,41.6779180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220523','220523','辉南县','hnx',2,'220500',126.0428210000,42.6834590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220524','220524','柳河县','lhx',2,'220500',125.7405360000,42.2814840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220581','220581','梅河口市','mhks',2,'220500',125.6873360000,42.5300020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220582','220582','集安市','jas',2,'220500',126.1862040000,41.1262760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220600','220600','白山市','bss',1,'220000',126.4278390000,41.9425050000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220601','220601','市辖区','sxq',1,'220600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220602','220602','八道江区','bdjq',1,'220600',126.4280350000,41.9430650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220621','220621','抚松县','fsx',2,'220600',127.2737960000,42.3326430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220622','220622','靖宇县','jyx',2,'220600',126.8083860000,42.3896890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220623','220623','长白朝鲜族自治县','cbcxzzzx',2,'220600',128.2033840000,41.4193610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220625','220625','江源县','jyx',2,'220600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220681','220681','临江市','ljs',2,'220600',126.9192960000,41.8106890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220700','220700','松原市','sys',1,'220000',124.8236080000,45.1182430000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220701','220701','市辖区','sxq',1,'220700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220702','220702','宁江区','njq',1,'220700',124.8278510000,45.1764980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220721','220721','前郭尔罗斯蒙古族自治县','qgelsmgzzzx',2,'220700',124.8268080000,45.1162880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220722','220722','长岭县','clx',2,'220700',123.9851840000,44.2765790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220723','220723','乾安县','qax',2,'220700',124.0243610000,45.0068460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220724','220724','扶余县','fyx',2,'220700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220800','220800','白城市','bcs',1,'220000',122.8411140000,45.6190260000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220801','220801','市辖区','sxq',1,'220800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220802','220802','洮北区','tbq',1,'220800',122.8424990000,45.6192530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220821','220821','镇赉县','zlx',2,'220800',123.2022460000,45.8460890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220822','220822','通榆县','tyx',2,'220800',123.0885430000,44.8091500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220881','220881','洮南市','tns',2,'220800',122.7837790000,45.3391130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('220882','220882','大安市','das',2,'220800',124.2915120000,45.5076480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222400','222400','延边朝鲜族自治州','ybcxzzzz',1,'220000',129.5132280000,42.9048230000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222401','222401','延吉市','yjs',2,'222400',129.5157900000,42.9069640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222402','222402','图们市','tms',2,'222400',129.8467010000,42.9666210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222403','222403','敦化市','dhs',2,'222400',128.2298600000,43.3669210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222404','222404','珲春市','hcs',2,'222400',130.3657870000,42.8710570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222405','222405','龙井市','ljs',2,'222400',129.4257470000,42.7710290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222406','222406','和龙市','hls',2,'222400',129.0087480000,42.5470040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222424','222424','汪清县','wqx',2,'222400',129.7661610000,43.3154260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('222426','222426','安图县','atx',2,'222400',128.9018650000,43.1109940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230000','230000','黑龙江','hljs',0,'0',126.6424640000,45.7569670000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230100','230100','哈尔滨市','hebs',1,'230000',126.6424640000,45.7569670000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230101','230101','市辖区','sxq',1,'230100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230102','230102','道里区','dlq',1,'230100',126.6142670000,45.7583110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230103','230103','南岗区','ngq',1,'230100',126.6270130000,45.7283590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230104','230104','道外区','dwq',1,'230100',126.6484580000,45.7773700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230106','230106','香坊区','xfq',1,'230100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230107','230107','动力区','dlq',1,'230100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230108','230108','平房区','pfq',1,'230100',126.6364070000,45.6018790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230109','230109','松北区','sbq',1,'230100',126.6321530000,45.8253740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230111','230111','呼兰区','hlq',1,'230100',126.6033020000,45.9842300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230123','230123','依兰县','ylx',2,'230100',129.5655940000,46.3151050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230124','230124','方正县','fzx',2,'230100',128.8361310000,45.8395360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230125','230125','宾县','bx',2,'230100',127.4859400000,45.7593690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230126','230126','巴彦县','byx',2,'230100',127.4036020000,46.0818890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230127','230127','木兰县','mlx',2,'230100',128.0426750000,45.9498260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230128','230128','通河县','thx',2,'230100',128.7477860000,45.9776180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230129','230129','延寿县','ysx',2,'230100',128.3318860000,45.4556480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230181','230181','阿城市','acs',2,'230100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230182','230182','双城市','scs',2,'230100',126.3087840000,45.3779420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230183','230183','尚志市','szs',2,'230100',127.9685390000,45.2149530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230184','230184','五常市','wcs',2,'230100',127.1575900000,44.9194180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230200','230200','齐齐哈尔市','qqhes',1,'230000',123.9579200000,47.3420810000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230201','230201','市辖区','sxq',1,'230200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230202','230202','龙沙区','lsq',1,'230200',123.9573380000,47.3417360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230203','230203','建华区','jhq',1,'230200',123.9558880000,47.3544940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230204','230204','铁锋区','tfq',1,'230200',123.9735550000,47.3394990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230205','230205','昂昂溪区','aaxq',1,'230200',123.8131810000,47.1568670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230206','230206','富拉尔基区','flejq',1,'230200',123.6388730000,47.2069700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230207','230207','碾子山区','nzsq',1,'230200',122.8879720000,47.5140100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230208','230208','梅里斯达斡尔族区','mlsdwezq',1,'230200',123.7545990000,47.3111130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230221','230221','龙江县','ljx',2,'230200',123.1872250000,47.3363880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230223','230223','依安县','yax',2,'230200',125.3075610000,47.8900980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230224','230224','泰来县','tlx',2,'230200',123.4195300000,46.3923300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230225','230225','甘南县','gnx',2,'230200',123.5060340000,47.9178380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230227','230227','富裕县','fyx',2,'230200',124.4691060000,47.7971720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230229','230229','克山县','ksx',2,'230200',125.8743550000,48.0343420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230230','230230','克东县','kdx',2,'230200',126.2490940000,48.0373200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230231','230231','拜泉县','bqx',2,'230200',126.0919110000,47.6073630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230281','230281','讷河市','nhs',2,'230200',124.8821720000,48.4811330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230300','230300','鸡西市','jxs',1,'230000',130.9759660000,45.3000460000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230301','230301','市辖区','sxq',1,'230300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230302','230302','鸡冠区','jgq',1,'230300',130.9743740000,45.3003400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230303','230303','恒山区','hsq',1,'230300',130.9106360000,45.2132420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230304','230304','滴道区','ddq',1,'230300',130.8468230000,45.3488120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230305','230305','梨树区','lsq',1,'230300',130.6977810000,45.0921950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230306','230306','城子河区','czhq',1,'230300',131.0105010000,45.3382480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230307','230307','麻山区','msq',1,'230300',130.4811260000,45.2096070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230321','230321','鸡东县','jdx',2,'230300',131.1489070000,45.2508920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230381','230381','虎林市','hls',2,'230300',132.9738810000,45.7679850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230382','230382','密山市','mss',2,'230300',131.8741370000,45.5472500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230400','230400','鹤岗市','hgs',1,'230000',130.2774870000,47.3320850000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230401','230401','市辖区','sxq',1,'230400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230402','230402','向阳区','xyq',1,'230400',130.2924780000,47.3453720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230403','230403','工农区','gnq',1,'230400',130.2766520000,47.3316780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230404','230404','南山区','nsq',1,'230400',130.2755330000,47.3132400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230405','230405','兴安区','xaq',1,'230400',130.2361690000,47.2529110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230406','230406','东山区','dsq',1,'230400',130.3171400000,47.3373850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230407','230407','兴山区','xsq',1,'230400',130.3053400000,47.3599700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230421','230421','萝北县','lbx',2,'230400',130.8290870000,47.5775770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230422','230422','绥滨县','sbx',2,'230400',131.8605260000,47.2898920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230500','230500','双鸭山市','syss',1,'230000',131.1573040000,46.6434420000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230501','230501','市辖区','sxq',1,'230500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230502','230502','尖山区','jsq',1,'230500',131.1589600000,46.6429610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230503','230503','岭东区','ldq',1,'230500',131.1636750000,46.5910760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230505','230505','四方台区','sftq',1,'230500',131.3331810000,46.5943470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230506','230506','宝山区','bsq',1,'230500',131.4042940000,46.5733660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230521','230521','集贤县','jxx',2,'230500',131.1393300000,46.7289800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230522','230522','友谊县','yyx',2,'230500',131.8106220000,46.7751590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230523','230523','宝清县','bqx',2,'230500',132.2064150000,46.3287810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230524','230524','饶河县','rhx',2,'230500',134.0211620000,46.8012880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230600','230600','大庆市','dqs',1,'230000',125.1127200000,46.5907340000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230601','230601','市辖区','sxq',1,'230600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230602','230602','萨尔图区','setq',1,'230600',125.1146430000,46.5963560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230603','230603','龙凤区','lfq',1,'230600',125.1457940000,46.5739480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230604','230604','让胡路区','rhlq',1,'230600',124.8683410000,46.6532540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230605','230605','红岗区','hgq',1,'230600',124.8895280000,46.4030490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230606','230606','大同区','dtq',1,'230600',124.8185090000,46.0343040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230621','230621','肇州县','zzx',2,'230600',125.2732540000,45.7086850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230622','230622','肇源县','zyx',2,'230600',125.0819740000,45.5188320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230623','230623','林甸县','ldx',2,'230600',124.8777420000,47.1864110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230624','230624','杜尔伯特蒙古族自治县','debtmgzzzx',2,'230600',124.4462590000,46.8659730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230700','230700','伊春市','ycs',1,'230000',128.8993960000,47.7247750000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230701','230701','市辖区','sxq',1,'230700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230702','230702','伊春区','ycq',1,'230700',128.8992840000,47.7268510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230703','230703','南岔区','ncq',1,'230700',129.2824600000,47.1373140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230704','230704','友好区','yhq',1,'230700',128.8389610000,47.8543030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230705','230705','西林区','xlq',1,'230700',129.3114410000,47.4794370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230706','230706','翠峦区','clq',1,'230700',128.6717460000,47.7262280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230707','230707','新青区','xqq',1,'230700',129.5299500000,48.2882920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230708','230708','美溪区','mxq',1,'230700',129.1334110000,47.6361020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230709','230709','金山屯区','jstq',1,'230700',129.4359440000,47.4129500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230710','230710','五营区','wyq',1,'230700',129.2450280000,48.1082040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230711','230711','乌马河区','wmhq',1,'230700',128.8029410000,47.7269610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230712','230712','汤旺河区','twhq',1,'230700',129.5722400000,48.4536510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230713','230713','带岭区','dlq',1,'230700',129.0211510000,47.0275320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230714','230714','乌伊岭区','wylq',1,'230700',129.4378470000,48.5911200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230715','230715','红星区','hxq',1,'230700',129.3887960000,48.2383680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230716','230716','上甘岭区','sglq',1,'230700',129.0250800000,47.9748590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230722','230722','嘉荫县','jyx',2,'230700',130.3976840000,48.8913780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230781','230781','铁力市','tls',2,'230700',128.0305610000,46.9857720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230800','230800','佳木斯市','jmss',1,'230000',130.3616340000,46.8096060000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230801','230801','市辖区','sxq',1,'230800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230802','230802','永红区','yhq',1,'230800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230803','230803','向阳区','xyq',1,'230800',130.3617860000,46.8096450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230804','230804','前进区','qjq',1,'230800',130.3776840000,46.8123450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230805','230805','东风区','dfq',1,'230800',130.4032970000,46.8224760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230811','230811','郊区','jq',1,'230800',130.3515880000,46.8071200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230822','230822','桦南县','hnx',2,'230800',130.5701120000,46.2401180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230826','230826','桦川县','hcx',2,'230800',130.7237130000,47.0230390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230828','230828','汤原县','tyx',2,'230800',129.9044630000,46.7300480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230833','230833','抚远县','fyx',2,'230800',134.2945010000,48.3647070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230881','230881','同江市','tjs',2,'230800',132.5101190000,47.6511310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230882','230882','富锦市','fjs',2,'230800',132.0379510000,47.2507470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230900','230900','七台河市','qths',1,'230000',131.0155840000,45.7712660000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230901','230901','市辖区','sxq',1,'230900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230902','230902','新兴区','xxq',1,'230900',130.8894820000,45.7942580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230903','230903','桃山区','tsq',1,'230900',131.0158480000,45.7712170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230904','230904','茄子河区','qzhq',1,'230900',131.0715610000,45.7765870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('230921','230921','勃利县','blx',2,'230900',130.5750250000,45.7515730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231000','231000','牡丹江市','mdjs',1,'230000',129.6186020000,44.5829620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231001','231001','市辖区','sxq',1,'231000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231002','231002','东安区','daq',1,'231000',129.6232920000,44.5823990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231003','231003','阳明区','ymq',1,'231000',129.6346450000,44.5963280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231004','231004','爱民区','amq',1,'231000',129.6012320000,44.5954430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231005','231005','西安区','xaq',1,'231000',129.6131100000,44.5810320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231024','231024','东宁县','dnx',2,'231000',131.1252960000,44.0635780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231025','231025','林口县','lkx',2,'231000',130.2684020000,45.2866450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231081','231081','绥芬河市','sfhs',2,'231000',131.1648560000,44.3968640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231083','231083','海林市','hls',2,'231000',129.3879020000,44.5741490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231084','231084','宁安市','nas',2,'231000',129.4700190000,44.3468360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231085','231085','穆棱市','mls',2,'231000',130.5270850000,44.9196700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231100','231100','黑河市','hhs',1,'230000',127.4990230000,50.2495850000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231101','231101','市辖区','sxq',1,'231100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231102','231102','爱辉区','ahq',1,'231100',127.4976390000,50.2490270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231121','231121','嫩江县','njx',2,'231100',125.2299040000,49.1774610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231123','231123','逊克县','xkx',2,'231100',128.4761520000,49.5829740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231124','231124','孙吴县','swx',2,'231100',127.3273150000,49.4239410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231181','231181','北安市','bas',2,'231100',126.5087370000,48.2454370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231182','231182','五大连池市','wdlcs',2,'231100',126.1976940000,48.5126880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231200','231200','绥化市','shs',1,'230000',126.9929300000,46.6373930000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231201','231201','市辖区','sxq',1,'231200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231202','231202','北林区','blq',1,'231200',126.9906650000,46.6349120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231221','231221','望奎县','wkx',2,'231200',126.4841910000,46.8335200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231222','231222','兰西县','lxx',2,'231200',126.2893150000,46.2590370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231223','231223','青冈县','qgx',2,'231200',126.1122680000,46.6865960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231224','231224','庆安县','qax',2,'231200',127.5100240000,46.8792030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231225','231225','明水县','msx',2,'231200',125.9075440000,47.1835270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231226','231226','绥棱县','slx',2,'231200',127.1111210000,47.2471950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231281','231281','安达市','ads',2,'231200',125.3299260000,46.4106140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231282','231282','肇东市','zds',2,'231200',125.9914020000,46.0694710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('231283','231283','海伦市','hls',2,'231200',126.9693830000,47.4604280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('232700','232700','大兴安岭地区','dxaldq',1,'230000',124.7115260000,52.3352620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('232721','232721','呼玛县','hmx',2,'232700',126.6621050000,51.7269980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('232722','232722','塔河县','thx',2,'232700',124.7105160000,52.3352290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('232723','232723','漠河县','mhx',2,'232700',122.5362560000,52.9720740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310000','310000','上海市','shs',0,'0',121.4726440000,31.2317060000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310100','310100','市辖区','sxq',1,'310000',121.4726440000,31.2317060000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310101','310101','黄浦区','hpq',1,'310100',121.4656010000,31.2121040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310103','310103','卢湾区','lwq',1,'310100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310104','310104','徐汇区','xhq',1,'310100',121.4495090000,31.1983990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310105','310105','长宁区','cnq',1,'310100',121.3978430000,31.1944310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310106','310106','静安区','jaq',1,'310100',121.4360960000,31.2210870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310107','310107','普陀区','ptq',1,'310100',121.4296300000,31.2451710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310108','310108','闸北区','zbq',1,'310100',121.4267290000,31.2889400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310109','310109','虹口区','hkq',1,'310100',121.4947920000,31.2913050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310110','310110','杨浦区','ypq',1,'310100',121.4932540000,31.3104020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310112','310112','闵行区','mxq',1,'310100',121.3825560000,31.0780290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310113','310113','宝山区','bsq',1,'310100',121.4949520000,31.3741620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310114','310114','嘉定区','jdq',1,'310100',121.3679010000,31.2523080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310115','310115','浦东新区','pdxq',1,'310100',121.5811190000,31.3453900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310116','310116','金山区','jsq',1,'310100',121.1655860000,30.8944880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310117','310117','松江区','sjq',1,'310100',121.2339530000,31.0076730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310118','310118','青浦区','qpq',1,'310100',121.1827870000,31.1947000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310119','310119','南汇区','nhq',1,'310100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310120','310120','奉贤区','fxq',1,'310100',121.3988940000,30.9026010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310200','310200','县','x',1,'310000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('310230','310230','崇明县','cmx',2,'310200',121.3974760000,31.6304730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320000','320000','江苏省','jss',0,'0',118.7674130000,32.0415440000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320100','320100','南京市','njs',1,'320000',118.7674130000,32.0415440000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320101','320101','市辖区','sxq',1,'320100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320102','320102','玄武区','xwq',1,'320100',118.8290730000,32.1001750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320103','320103','白下区','bxq',1,'320100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320104','320104','秦淮区','qhq',1,'320100',118.7744120000,32.0228560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320105','320105','建邺区','jyq',1,'320100',118.7366470000,32.0378170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320106','320106','鼓楼区','glq',1,'320100',118.7803260000,32.0462180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320107','320107','下关区','xgq',1,'320100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320111','320111','浦口区','pkq',1,'320100',118.7316360000,32.1312410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320113','320113','栖霞区','qxq',1,'320100',118.9134740000,32.0957740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320114','320114','雨花台区','yhtq',1,'320100',118.7734460000,31.9963660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320115','320115','江宁区','jnq',1,'320100',118.8760450000,31.9133180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320116','320116','六合区','lhq',1,'320100',118.7606020000,32.2140120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320124','320124','溧水县','lsx',2,'320100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320125','320125','高淳县','gcx',2,'320100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320200','320200','无锡市','wxs',1,'320000',120.3016630000,31.5747290000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320201','320201','市辖区','sxq',1,'320200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320202','320202','崇安区','caq',1,'320200',120.3194540000,31.5907670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320203','320203','南长区','ncq',1,'320200',120.2881150000,31.5711210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320204','320204','北塘区','btq',1,'320200',120.2603270000,31.6025290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320205','320205','锡山区','xsq',1,'320200',120.5543150000,31.6250440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320206','320206','惠山区','hsq',1,'320200',120.2839880000,31.6886950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320211','320211','滨湖区','bhq',1,'320200',120.2842660000,31.4721950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320281','320281','江阴市','jys',2,'320200',120.2758910000,31.9109840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320282','320282','宜兴市','yxs',2,'320200',119.8201590000,31.3478360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320300','320300','徐州市','xzs',1,'320000',117.1848110000,34.2617920000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320301','320301','市辖区','sxq',1,'320300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320302','320302','鼓楼区','glq',1,'320300',117.1929410000,34.2693970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320303','320303','云龙区','ylq',1,'320300',117.1945890000,34.2548050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320304','320304','九里区','jlq',1,'320300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320305','320305','贾汪区','jwq',1,'320300',117.4502120000,34.4416420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320311','320311','泉山区','qsq',1,'320300',117.1471260000,34.2731870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320321','320321','丰县','fx',2,'320300',116.5928880000,34.6969460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320322','320322','沛县','px',2,'320300',116.9371820000,34.7290440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320323','320323','铜山县','tsx',2,'320300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320324','320324','睢宁县','hnx',2,'320300',117.9506600000,33.8992220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320381','320381','新沂市','xys',2,'320300',118.3458280000,34.3687790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320382','320382','邳州市','pzs',2,'320300',117.9639230000,34.3147080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320400','320400','常州市','czs',1,'320000',119.9469730000,31.7727520000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320401','320401','市辖区','sxq',1,'320400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320402','320402','天宁区','tnq',1,'320400',119.9827220000,31.7947290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320404','320404','钟楼区','zlq',1,'320400',119.9489430000,31.7709260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320405','320405','戚墅堰区','qsyq',1,'320400',120.0471770000,31.7607340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320411','320411','新北区','xbq',1,'320400',119.9853020000,31.8224950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320412','320412','武进区','wjq',1,'320400',119.9444570000,31.7092050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320481','320481','溧阳市','lys',2,'320400',119.4878160000,31.4270810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320482','320482','金坛市','jts',2,'320400',119.5733950000,31.7443990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320500','320500','苏州市','szs',1,'320000',120.6195850000,31.2993790000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320501','320501','市辖区','sxq',1,'320500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320502','320502','沧浪区','clq',1,'320500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320503','320503','平江区','pjq',1,'320500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320504','320504','金阊区','jcq',1,'320500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320505','320505','虎丘区','hqq',1,'320500',120.4901860000,31.3665470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320506','320506','吴中区','wzq',1,'320500',120.7362290000,31.2723870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320507','320507','相城区','xcq',1,'320500',120.6231070000,31.3856440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320581','320581','常熟市','css',2,'320500',120.7485200000,31.6581560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320582','320582','张家港市','zjgs',2,'320500',120.5522370000,31.8767650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320583','320583','昆山市','kss',2,'320500',121.1039540000,31.2990640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320584','320584','吴江市','wjs',2,'320500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320585','320585','太仓市','tcs',2,'320500',121.1122750000,31.4525680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320600','320600','南通市','nts',1,'320000',120.8646080000,32.0162120000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320601','320601','市辖区','sxq',1,'320600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320602','320602','崇川区','ccq',1,'320600',120.8691090000,31.9990110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320611','320611','港闸区','gzq',1,'320600',120.8339000000,32.0402990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320621','320621','海安县','hax',2,'320600',120.4659950000,32.5402890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320623','320623','如东县','rdx',2,'320600',121.1860880000,32.3118320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320681','320681','启东市','qds',2,'320600',121.6597240000,31.8101580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320682','320682','如皋市','rgs',2,'320600',120.5663240000,32.3915910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320683','320683','通州市','tzs',2,'320600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320684','320684','海门市','hms',2,'320600',121.1766090000,31.8935280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320700','320700','连云港市','lygs',1,'320000',119.1788210000,34.6000180000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320701','320701','市辖区','sxq',1,'320700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320703','320703','连云区','lyq',1,'320700',119.3664870000,34.7395290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320705','320705','新浦区','xpq',1,'320700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320706','320706','海州区','hzq',1,'320700',119.1797930000,34.6015840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320721','320721','赣榆县','gyx',2,'320700',119.1287740000,34.8391540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320722','320722','东海县','dhx',2,'320700',118.7664890000,34.5228590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320723','320723','灌云县','gyx',2,'320700',119.2557410000,34.2984360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320724','320724','灌南县','gnx',2,'320700',119.3523310000,34.0925530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320800','320800','淮安市','has',1,'320000',119.0212650000,33.5975060000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320801','320801','市辖区','sxq',1,'320800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320802','320802','清河区','qhq',1,'320800',119.0194540000,33.6032340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320803','320803','楚州区','czq',1,'320800',119.1463400000,33.5074990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320804','320804','淮阴区','hyq',1,'320800',119.0208170000,33.6224520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320811','320811','清浦区','qpq',1,'320800',119.0304980000,33.5807400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320826','320826','涟水县','lsx',2,'320800',119.2660780000,33.7713080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320829','320829','洪泽县','hzx',2,'320800',118.8678750000,33.2949750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320830','320830','盱眙县','xyx',2,'320800',118.4938230000,33.0043900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320831','320831','金湖县','jhx',2,'320800',119.0169360000,33.0181620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320900','320900','盐城市','ycs',1,'320000',120.1399980000,33.3776310000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320901','320901','市辖区','sxq',1,'320900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320902','320902','亭湖区','thq',1,'320900',120.1360780000,33.3839120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320903','320903','盐都区','ydq',1,'320900',120.1397530000,33.3412880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320921','320921','响水县','xsx',2,'320900',119.5795730000,34.1999600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320922','320922','滨海县','bhx',2,'320900',119.8284340000,33.9898880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320923','320923','阜宁县','fnx',2,'320900',119.8053380000,33.7857300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320924','320924','射阳县','syx',2,'320900',120.2574440000,33.7737790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320925','320925','建湖县','jhx',2,'320900',119.7931050000,33.4726210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320981','320981','东台市','dts',2,'320900',120.3141010000,32.8531740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('320982','320982','大丰市','dfs',2,'320900',120.4703240000,33.1995310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321000','321000','扬州市','yzs',1,'320000',119.4210030000,32.3931590000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321001','321001','市辖区','sxq',1,'321000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321002','321002','广陵区','glq',1,'321000',119.4467810000,32.3977860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321003','321003','邗江区','hjq',1,'321000',119.4428470000,32.4069170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321011','321011','郊区','jq',1,'321000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321023','321023','宝应县','byx',2,'321000',119.3212840000,33.2369400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321081','321081','仪征市','yzs',2,'321000',119.1824430000,32.2719650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321084','321084','高邮市','gys',2,'321000',119.4438420000,32.7851640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321088','321088','江都市','jds',2,'321000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321100','321100','镇江市','zjs',1,'320000',119.4527530000,32.2044020000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321101','321101','市辖区','sxq',1,'321100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321102','321102','京口区','jkq',1,'321100',119.4719050000,32.2063400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321111','321111','润州区','rzq',1,'321100',119.4642360000,32.1925960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321112','321112','丹徒区','dtq',1,'321100',119.4338830000,32.1289720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321181','321181','丹阳市','dys',2,'321100',119.5819110000,31.9914590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321182','321182','扬中市','yzs',2,'321100',119.8280540000,32.2372660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321183','321183','句容市','jrs',2,'321100',119.1671350000,31.9473550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321200','321200','泰州市','tzs',1,'320000',119.9151760000,32.4848820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321201','321201','市辖区','sxq',1,'321200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321202','321202','海陵区','hlq',1,'321200',119.9201870000,32.4884060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321203','321203','高港区','ggq',1,'321200',119.8816600000,32.3157010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321281','321281','兴化市','xhs',2,'321200',119.8401620000,32.9380650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321282','321282','靖江市','jjs',2,'321200',120.2682500000,32.0181680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321283','321283','泰兴市','txs',2,'321200',120.0202280000,32.1687840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321284','321284','姜堰市','jys',2,'321200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321300','321300','宿迁市','sqs',1,'320000',118.2751620000,33.9630080000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321301','321301','市辖区','sxq',1,'321300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321302','321302','宿城区','scq',1,'321300',118.2789840000,33.9377260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321311','321311','宿豫区','syq',1,'321300',118.3300120000,33.9410710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321322','321322','沭阳县','syx',2,'321300',118.7758890000,34.1290970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321323','321323','泗阳县','syx',2,'321300',118.6812840000,33.7114330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('321324','321324','泗洪县','shx',2,'321300',118.2118240000,33.4565380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330000','330000','浙江省','zjs',0,'0',120.1535760000,30.2874590000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330100','330100','杭州市','hzs',1,'330000',120.1535760000,30.2874590000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330101','330101','市辖区','sxq',1,'330100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330102','330102','上城区','scq',1,'330100',120.1737100000,30.2264970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330103','330103','下城区','xcq',1,'330100',120.1679910000,30.3078060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330104','330104','江干区','jgq',1,'330100',120.1906850000,30.2774070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330105','330105','拱墅区','gsq',1,'330100',120.1519430000,30.3627490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330106','330106','西湖区','xhq',1,'330100',120.0861060000,30.3236100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330108','330108','滨江区','bjq',1,'330100',120.1646600000,30.1853350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330109','330109','萧山区','xsq',1,'330100',120.3330310000,30.1913160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330110','330110','余杭区','yhq',1,'330100',120.3000200000,30.4233260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330122','330122','桐庐县','tlx',2,'330100',119.6904920000,29.7967560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330127','330127','淳安县','cax',2,'330100',119.0477780000,29.6041600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330182','330182','建德市','jds',2,'330100',119.2827270000,29.4729820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330183','330183','富阳市','fys',2,'330100',119.9153460000,30.0085760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330185','330185','临安市','las',2,'330100',119.8025280000,30.2432700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330200','330200','宁波市','nbs',1,'330000',121.5497920000,29.8683880000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330201','330201','市辖区','sxq',1,'330200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330203','330203','海曙区','hsq',1,'330200',121.5399970000,29.8749120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330204','330204','江东区','jdq',1,'330200',121.5659600000,29.8597230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330205','330205','江北区','jbq',1,'330200',121.5494250000,29.9302950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330206','330206','北仑区','blq',1,'330200',121.8292450000,29.9035330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330211','330211','镇海区','zhq',1,'330200',121.7170410000,29.9545130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330212','330212','鄞州区','yzq',1,'330200',121.5083030000,29.8646890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330225','330225','象山县','xsx',2,'330200',121.8902270000,29.4699940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330226','330226','宁海县','nhx',2,'330200',121.4343620000,29.2951200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330281','330281','余姚市','yys',2,'330200',121.1519530000,30.1605170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330282','330282','慈溪市','cxs',2,'330200',121.2548320000,30.2199360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330283','330283','奉化市','fhs',2,'330200',121.4097300000,29.6586810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330300','330300','温州市','wzs',1,'330000',120.6721110000,28.0005750000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330301','330301','市辖区','sxq',1,'330300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330302','330302','鹿城区','lcq',1,'330300',120.7707210000,28.0005390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330303','330303','龙湾区','lwq',1,'330300',120.7611100000,27.9683580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330304','330304','瓯海区','ohq',1,'330300',120.6350760000,27.9934660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330322','330322','洞头县','dtx',2,'330300',121.1533060000,27.8349850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330324','330324','永嘉县','yjx',2,'330300',120.6436000000,28.0439390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330326','330326','平阳县','pyx',2,'330300',120.5631970000,27.6680370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330327','330327','苍南县','cnx',2,'330300',120.4090910000,27.3400140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330328','330328','文成县','wcx',2,'330300',120.0924500000,27.7891330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330329','330329','泰顺县','tsx',2,'330300',119.9158560000,27.4222910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330381','330381','瑞安市','ras',2,'330300',120.6137310000,27.7683040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330382','330382','乐清市','lqs',2,'330300',121.1567060000,28.4068150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330400','330400','嘉兴市','jxs',1,'330000',120.7508650000,30.7626530000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330401','330401','市辖区','sxq',1,'330400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330402','330402','秀城区','xcq',1,'330400',120.7602230000,30.7691350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330411','330411','秀洲区','xzq',1,'330400',120.7279300000,30.7671470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330421','330421','嘉善县','jsx',2,'330400',120.8935940000,30.9436040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330424','330424','海盐县','hyx',2,'330400',120.9420170000,30.5222230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330481','330481','海宁市','hns',2,'330400',120.6888210000,30.5255440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330482','330482','平湖市','phs',2,'330400',121.0146660000,30.6989210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330483','330483','桐乡市','txs',2,'330400',120.4925260000,30.7428890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330500','330500','湖州市','hzs',1,'330000',120.1023980000,30.8671980000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330501','330501','市辖区','sxq',1,'330500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330502','330502','吴兴区','wxq',1,'330500',120.1014160000,30.8672520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330503','330503','南浔区','nxq',1,'330500',120.4171950000,30.8727420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330521','330521','德清县','dqx',2,'330500',119.9676620000,30.5349270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330522','330522','长兴县','cxx',2,'330500',119.9101220000,31.0047500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330523','330523','安吉县','ajx',2,'330500',119.6878910000,30.6319740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330600','330600','绍兴市','sxs',1,'330000',120.5821120000,29.9971170000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330601','330601','市辖区','sxq',1,'330600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330602','330602','越城区','ycq',1,'330600',120.5853150000,29.9969930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330621','330621','绍兴县','sxx',2,'330600',120.4760750000,30.0780380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330624','330624','新昌县','xcx',2,'330600',120.9056650000,29.5012050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330681','330681','诸暨市','zjs',2,'330600',120.2443260000,29.7136620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330682','330682','上虞市','sys',2,'330600',120.8741850000,30.0167690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330683','330683','嵊州市','szs',2,'330600',120.8288800000,29.5866060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330700','330700','金华市','jhs',1,'330000',119.6495060000,29.0895240000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330701','330701','市辖区','sxq',1,'330700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330702','330702','婺城区','wcq',1,'330700',119.6525790000,29.0826070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330703','330703','金东区','jdq',1,'330700',119.6801380000,29.1131080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330723','330723','武义县','wyx',2,'330700',119.8191590000,28.8965630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330726','330726','浦江县','pjx',2,'330700',119.8933630000,29.4512540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330727','330727','磐安县','pax',2,'330700',120.4451300000,29.0526270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330781','330781','兰溪市','lxs',2,'330700',119.4605210000,29.2100650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330782','330782','义乌市','yws',2,'330700',120.0749110000,29.3068630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330783','330783','东阳市','dys',2,'330700',120.2362340000,29.2963930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330784','330784','永康市','yks',2,'330700',120.0330550000,28.8896990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330800','330800','衢州市','qzs',1,'330000',118.8726300000,28.9417080000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330801','330801','市辖区','sxq',1,'330800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330802','330802','柯城区','kcq',1,'330800',118.8730410000,28.9445390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330803','330803','衢江区','qjq',1,'330800',118.9576830000,28.9731950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330822','330822','常山县','csx',2,'330800',118.5216540000,28.9000390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330824','330824','开化县','khx',2,'330800',118.4144350000,29.1365030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330825','330825','龙游县','lyx',2,'330800',119.1725250000,29.0313640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330881','330881','江山市','jss',2,'330800',118.6278790000,28.7346740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330900','330900','舟山市','zss',1,'330000',122.1068630000,30.0160280000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330901','330901','市辖区','sxq',1,'330900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330902','330902','定海区','dhq',1,'330900',122.1084960000,30.0164230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330903','330903','普陀区','ptq',1,'330900',122.3019530000,29.9456140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330921','330921','岱山县','dsx',2,'330900',122.2011320000,30.2428650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('330922','330922','嵊泗县','ssx',2,'330900',122.4578090000,30.7271660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331000','331000','台州市','tzs',1,'330000',121.4285990000,28.6613780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331001','331001','市辖区','sxq',1,'331000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331002','331002','椒江区','jjq',1,'331000',121.4310490000,28.6761500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331003','331003','黄岩区','hyq',1,'331000',121.2621380000,28.6448800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331004','331004','路桥区','lqq',1,'331000',121.3729200000,28.5817990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331021','331021','玉环县','yhx',2,'331000',121.2323370000,28.1284200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331022','331022','三门县','smx',2,'331000',121.3764290000,29.1189550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331023','331023','天台县','ttx',2,'331000',121.0312270000,29.1411260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331024','331024','仙居县','xjx',2,'331000',120.7350740000,28.8492130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331081','331081','温岭市','wls',2,'331000',121.3736110000,28.3687810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331082','331082','临海市','lhs',2,'331000',121.1312290000,28.8454410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331100','331100','丽水市','lss',1,'330000',119.9217860000,28.4519930000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331101','331101','市辖区','sxq',1,'331100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331102','331102','莲都区','ldq',1,'331100',119.9316250000,28.4675410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331121','331121','青田县','qtx',2,'331100',120.2919390000,28.1352470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331122','331122','缙云县','jyx',2,'331100',120.0789650000,28.6542080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331123','331123','遂昌县','scx',2,'331100',119.2758900000,28.5924000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331124','331124','松阳县','syx',2,'331100',119.4852920000,28.4499370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331125','331125','云和县','yhx',2,'331100',119.5694580000,28.1110770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331126','331126','庆元县','qyx',2,'331100',119.0672330000,27.6182310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331127','331127','景宁畲族自治县','jnszzzx',2,'331100',119.6346690000,27.9772470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('331181','331181','龙泉市','lqs',2,'331100',119.1323190000,28.0691770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340000','340000','安徽省','ahs',0,'0',117.2830420000,31.8611900000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340100','340100','合肥市','hfs',1,'340000',117.2830420000,31.8611900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340101','340101','市辖区','sxq',1,'340100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340102','340102','瑶海区','yhq',1,'340100',117.3059030000,31.8915160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340103','340103','庐阳区','lyq',1,'340100',117.2808090000,31.8603740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340104','340104','蜀山区','ssq',1,'340100',117.2251330000,31.8469050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340111','340111','包河区','bhq',1,'340100',117.2647180000,31.8294920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340121','340121','长丰县','cfx',2,'340100',117.0726500000,32.0667290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340122','340122','肥东县','fdx',2,'340100',117.6364840000,31.9343740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340123','340123','肥西县','fxx',2,'340100',116.8771130000,31.6240800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340200','340200','芜湖市','whs',1,'340000',118.3764510000,31.3263190000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340201','340201','市辖区','sxq',1,'340200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340202','340202','镜湖区','jhq',1,'340200',118.3763430000,31.3255900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340203','340203','马塘区','mtq',1,'340200',118.3774760000,31.3133940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340204','340204','新芜区','xwq',1,'340200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340207','340207','鸠江区','jjq',1,'340200',118.4001740000,31.3627160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340221','340221','芜湖县','whx',2,'340200',118.5723010000,31.1452620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340222','340222','繁昌县','fcx',2,'340200',118.2013490000,31.0808960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340223','340223','南陵县','nlx',2,'340200',118.3371040000,30.9196380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340300','340300','蚌埠市','bbs',1,'340000',117.3632280000,32.9396670000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340301','340301','市辖区','sxq',1,'340300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340302','340302','龙子湖区','lzhq',1,'340300',117.3823120000,32.9504520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340303','340303','蚌山区','bsq',1,'340300',117.3557890000,32.9380660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340304','340304','禹会区','yhq',1,'340300',117.3525900000,32.9319330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340311','340311','淮上区','hsq',1,'340300',117.3470900000,32.9631470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340321','340321','怀远县','hyx',2,'340300',117.2001710000,32.9569340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340322','340322','五河县','whx',2,'340300',117.8888090000,33.1462020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340323','340323','固镇县','gzx',2,'340300',117.3159620000,33.3186790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340400','340400','淮南市','hns',1,'340000',117.0183290000,32.6475740000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340401','340401','市辖区','sxq',1,'340400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340402','340402','大通区','dtq',1,'340400',117.0529270000,32.6320660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340403','340403','田家庵区','tjaq',1,'340400',117.0183180000,32.6443420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340404','340404','谢家集区','xjjq',1,'340400',116.8653540000,32.5982890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340405','340405','八公山区','bgsq',1,'340400',116.8411110000,32.6282290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340406','340406','潘集区','pjq',1,'340400',116.8168790000,32.7821170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340421','340421','凤台县','ftx',2,'340400',116.7227690000,32.7053820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340500','340500','马鞍山市','mass',1,'340000',118.5079060000,31.6893620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340501','340501','市辖区','sxq',1,'340500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340502','340502','金家庄区','jjzq',1,'340500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340503','340503','花山区','hsq',1,'340500',118.5113080000,31.6990200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340504','340504','雨山区','ysq',1,'340500',118.4931040000,31.6859120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340521','340521','当涂县','dtx',2,'340500',118.4898730000,31.5561670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340600','340600','淮北市','hbs',1,'340000',116.7946640000,33.9717070000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340601','340601','市辖区','sxq',1,'340600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340602','340602','杜集区','djq',1,'340600',116.8339250000,33.9912180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340603','340603','相山区','xsq',1,'340600',116.7907750000,33.9709160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340604','340604','烈山区','lsq',1,'340600',116.8094650000,33.8895290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340621','340621','濉溪县','sxx',2,'340600',116.7674350000,33.9164070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340700','340700','铜陵市','tls',1,'340000',117.8165760000,30.9299350000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340701','340701','市辖区','sxq',1,'340700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340702','340702','铜官山区','tgsq',1,'340700',117.8184270000,30.9318200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340703','340703','狮子山区','szsq',1,'340700',117.8640940000,30.9462490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340711','340711','郊区','jq',1,'340700',117.8070700000,30.9089270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340721','340721','铜陵县','tlx',2,'340700',117.7922880000,30.9523380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340800','340800','安庆市','aqs',1,'340000',117.0435510000,30.5088300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340801','340801','市辖区','sxq',1,'340800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340802','340802','迎江区','yjq',1,'340800',117.0449650000,30.5063750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340803','340803','大观区','dgq',1,'340800',117.0345120000,30.5056320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340811','340811','郊区','jq',1,'340800',117.0700030000,30.5413230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340822','340822','怀宁县','hnx',2,'340800',116.8286640000,30.7349940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340823','340823','枞阳县','zyx',2,'340800',117.2220270000,30.7006150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340824','340824','潜山县','qsx',2,'340800',116.5736660000,30.6382220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340825','340825','太湖县','thx',2,'340800',116.3052250000,30.4518690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340826','340826','宿松县','ssx',2,'340800',116.1202040000,30.1583270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340827','340827','望江县','wjx',2,'340800',116.6909270000,30.1249100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340828','340828','岳西县','yxx',2,'340800',116.3604820000,30.8485020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('340881','340881','桐城市','tcs',2,'340800',116.9596560000,31.0505760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341000','341000','黄山市','hss',1,'340000',118.3173250000,29.7092390000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341001','341001','市辖区','sxq',1,'341000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341002','341002','屯溪区','txq',1,'341000',118.3173540000,29.7091860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341003','341003','黄山区','hsq',1,'341000',118.1366390000,30.2945170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341004','341004','徽州区','hzq',1,'341000',118.3397430000,29.8252010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341021','341021','歙县','sx',2,'341000',118.4280250000,29.8677480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341022','341022','休宁县','xnx',2,'341000',118.1885310000,29.7888780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341023','341023','黟县','yx',2,'341000',117.9429110000,29.9238120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341024','341024','祁门县','qmx',2,'341000',117.7172370000,29.8534720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341100','341100','滁州市','czs',1,'340000',118.3162640000,32.3036270000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341101','341101','市辖区','sxq',1,'341100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341102','341102','琅琊区','lyq',1,'341100',118.3164750000,32.3037970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341103','341103','南谯区','nqq',1,'341100',118.2969550000,32.3298410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341122','341122','来安县','lax',2,'341100',118.4332930000,32.4502310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341124','341124','全椒县','qjx',2,'341100',118.2685760000,32.0938500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341125','341125','定远县','dyx',2,'341100',117.6837130000,32.5271050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341126','341126','凤阳县','fyx',2,'341100',117.5624610000,32.8671460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341181','341181','天长市','tcs',2,'341100',119.0112120000,32.6815000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341182','341182','明光市','mgs',2,'341100',117.9980480000,32.7812060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341200','341200','阜阳市','fys',1,'340000',115.8197290000,32.8969690000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341201','341201','市辖区','sxq',1,'341200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341202','341202','颍州区','yzq',1,'341200',115.8139140000,32.8912380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341203','341203','颍东区','ydq',1,'341200',115.8587470000,32.9088610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341204','341204','颍泉区','yqq',1,'341200',115.8045250000,32.9247970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341221','341221','临泉县','lqx',2,'341200',115.2616880000,33.0626980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341222','341222','太和县','thx',2,'341200',115.6272430000,33.1622900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341225','341225','阜南县','fnx',2,'341200',115.5905340000,32.6381020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341226','341226','颍上县','ysx',2,'341200',116.2591220000,32.6370650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341282','341282','界首市','jss',2,'341200',115.3621170000,33.2615300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341300','341300','宿州市','szs',1,'340000',116.9840840000,33.6338910000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341301','341301','市辖区','sxq',1,'341300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341302','341302','墉桥区','yqq',1,'341300',116.9833090000,33.6338530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341321','341321','砀山县','dsx',2,'341300',116.3511130000,34.4262470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341322','341322','萧县','xx',2,'341300',116.9453990000,34.1832660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341323','341323','灵璧县','lbx',2,'341300',117.5514930000,33.5406290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341324','341324','泗县','sx',2,'341300',117.8854430000,33.4775800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341400','341400','巢湖市','chs',1,'340000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341401','341401','市辖区','sxq',1,'341400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341402','341402','居巢区','jcq',1,'341400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341421','341421','庐江县','ljx',2,'341400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341422','341422','无为县','wwx',2,'341400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341423','341423','含山县','hsx',2,'341400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341424','341424','和县','hx',2,'341400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341500','341500','六安市','las',1,'340000',116.5076760000,31.7528890000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341501','341501','市辖区','sxq',1,'341500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341502','341502','金安区','jaq',1,'341500',116.5032880000,31.7544910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341503','341503','裕安区','yaq',1,'341500',116.4945430000,31.7506920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341521','341521','寿县','sx',2,'341500',116.7853490000,32.5773040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341522','341522','霍邱县','hqx',2,'341500',116.2788750000,32.3413050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341523','341523','舒城县','scx',2,'341500',116.9440880000,31.4628480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341524','341524','金寨县','jzx',2,'341500',115.8785140000,31.6816240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341525','341525','霍山县','hsx',2,'341500',116.3330780000,31.4024560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341600','341600','亳州市','bzs',1,'340000',115.7829390000,33.8693380000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341601','341601','市辖区','sxq',1,'341600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341602','341602','谯城区','qcq',1,'341600',115.7812140000,33.8692840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341621','341621','涡阳县','wyx',2,'341600',116.2115510000,33.5028310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341622','341622','蒙城县','mcx',2,'341600',116.5603370000,33.2608140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341623','341623','利辛县','lxx',2,'341600',116.2077820000,33.1435030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341700','341700','池州市','czs',1,'340000',117.4891570000,30.6560370000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341701','341701','市辖区','sxq',1,'341700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341702','341702','贵池区','gcq',1,'341700',117.4883420000,30.6573780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341721','341721','东至县','dzx',2,'341700',117.0214760000,30.0965680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341722','341722','石台县','stx',2,'341700',117.4829070000,30.2103240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341723','341723','青阳县','qyx',2,'341700',117.8573950000,30.6381800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341800','341800','宣城市','xcs',1,'340000',118.7579950000,30.9456670000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341801','341801','市辖区','sxq',1,'341800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341802','341802','宣州区','xzq',1,'341800',118.7584120000,30.9460030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341821','341821','郎溪县','lxx',2,'341800',119.1850240000,31.1278340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341822','341822','广德县','gdx',2,'341800',119.4175210000,30.8931160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341823','341823','泾县','jx',2,'341800',118.4123970000,30.6859750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341824','341824','绩溪县','jxx',2,'341800',118.5947050000,30.0652670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341825','341825','旌德县','jdx',2,'341800',118.5430810000,30.2880570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('341881','341881','宁国市','ngs',2,'341800',118.9834070000,30.6265290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350000','350000','福建省','fjs',0,'0',119.3062390000,26.0753020000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350100','350100','福州市','fzs',1,'350000',119.3062390000,26.0753020000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350101','350101','市辖区','sxq',1,'350100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350102','350102','鼓楼区','glq',1,'350100',119.3071470000,26.1086280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350103','350103','台江区','tjq',1,'350100',119.2878650000,26.0686490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350104','350104','仓山区','csq',1,'350100',119.3341410000,26.0268400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350105','350105','马尾区','mwq',1,'350100',119.5108800000,26.0703150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350111','350111','晋安区','jaq',1,'350100',119.3166310000,26.0929140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350121','350121','闽侯县','mhx',2,'350100',119.1451170000,26.1485670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350122','350122','连江县','ljx',2,'350100',119.5383650000,26.2021090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350123','350123','罗源县','lyx',2,'350100',119.5526450000,26.4872340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350124','350124','闽清县','mqx',2,'350100',118.8684160000,26.2237930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350125','350125','永泰县','ytx',2,'350100',118.9390890000,25.8648250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350128','350128','平潭县','ptx',2,'350100',119.7911970000,25.5036720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350181','350181','福清市','fqs',2,'350100',119.3769920000,25.7204020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350182','350182','长乐市','cls',2,'350100',119.5108490000,25.9605830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350200','350200','厦门市','xms',1,'350000',118.1102200000,24.4904740000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350201','350201','市辖区','sxq',1,'350200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350203','350203','思明区','smq',1,'350200',118.1297060000,24.4860210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350205','350205','海沧区','hcq',1,'350200',118.0306290000,24.4915700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350206','350206','湖里区','hlq',1,'350200',118.1148800000,24.5091010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350211','350211','集美区','jmq',1,'350200',118.1030320000,24.5810060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350212','350212','同安区','taq',1,'350200',118.1543440000,24.7196080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350213','350213','翔安区','xaq',1,'350200',118.2528900000,24.7381890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350300','350300','莆田市','pts',1,'350000',119.0075580000,25.4310110000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350301','350301','市辖区','sxq',1,'350300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350302','350302','城厢区','cxq',1,'350300',119.0010280000,25.4337370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350303','350303','涵江区','hjq',1,'350300',119.1191020000,25.4592730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350304','350304','荔城区','lcq',1,'350300',119.0200470000,25.4300470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350305','350305','秀屿区','xyq',1,'350300',119.0926070000,25.3161410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350322','350322','仙游县','xyx',2,'350300',118.6943310000,25.3565290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350400','350400','三明市','sms',1,'350000',117.6350010000,26.2654440000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350401','350401','市辖区','sxq',1,'350400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350402','350402','梅列区','mlq',1,'350400',117.6368700000,26.2692080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350403','350403','三元区','syq',1,'350400',117.6074180000,26.2341910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350421','350421','明溪县','mxx',2,'350400',117.2018450000,26.3573750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350423','350423','清流县','qlx',2,'350400',116.8158210000,26.1776100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350424','350424','宁化县','nhx',2,'350400',116.6597250000,26.2599320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350425','350425','大田县','dtx',2,'350400',117.8493550000,25.6908030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350426','350426','尤溪县','yxx',2,'350400',118.1885770000,26.1692610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350427','350427','沙县','sx',2,'350400',117.7890950000,26.3973610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350428','350428','将乐县','jlx',2,'350400',117.4735580000,26.7286670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350429','350429','泰宁县','tnx',2,'350400',117.1775220000,26.8979950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350430','350430','建宁县','jnx',2,'350400',116.8458320000,26.8313980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350481','350481','永安市','yas',2,'350400',117.3644470000,25.9740750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350500','350500','泉州市','qzs',1,'350000',118.5894210000,24.9088530000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350501','350501','市辖区','sxq',1,'350500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350502','350502','鲤城区','lcq',1,'350500',118.5918200000,24.9145540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350503','350503','丰泽区','fzq',1,'350500',118.5848930000,24.9263320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350504','350504','洛江区','ljq',1,'350500',118.6703120000,24.9411530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350505','350505','泉港区','qgq',1,'350500',118.9122850000,25.1268590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350521','350521','惠安县','hax',2,'350500',118.7989540000,25.0287180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350524','350524','安溪县','axx',2,'350500',118.1860140000,25.0568240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350525','350525','永春县','ycx',2,'350500',118.2950300000,25.3207210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350526','350526','德化县','dhx',2,'350500',118.2429860000,25.4890040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350527','350527','金门县','jmx',2,'350500',118.3232210000,24.4364170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350581','350581','石狮市','sss',2,'350500',118.6284020000,24.7319780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350582','350582','晋江市','jjs',2,'350500',118.5773380000,24.8073220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350583','350583','南安市','nas',2,'350500',118.3870310000,24.9594940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350600','350600','漳州市','zzs',1,'350000',117.6618010000,24.5108970000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350601','350601','市辖区','sxq',1,'350600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350602','350602','芗城区','xcq',1,'350600',117.6564610000,24.5099550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350603','350603','龙文区','lwq',1,'350600',117.6713870000,24.5156560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350622','350622','云霄县','yxx',2,'350600',117.3409460000,23.9504860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350623','350623','漳浦县','zpx',2,'350600',117.6140230000,24.1179070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350624','350624','诏安县','zax',2,'350600',117.1760830000,23.7108340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350625','350625','长泰县','ctx',2,'350600',117.7559130000,24.6214750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350626','350626','东山县','dsx',2,'350600',117.4276790000,23.7028450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350627','350627','南靖县','njx',2,'350600',117.3654620000,24.5164250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350628','350628','平和县','phx',2,'350600',117.3135490000,24.3661580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350629','350629','华安县','hax',2,'350600',117.5363100000,25.0014160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350681','350681','龙海市','lhs',2,'350600',117.8172920000,24.4453410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350700','350700','南平市','nps',1,'350000',118.1784590000,26.6356270000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350701','350701','市辖区','sxq',1,'350700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350702','350702','延平区','ypq',1,'350700',118.1789180000,26.6360790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350721','350721','顺昌县','scx',2,'350700',117.8077100000,26.7928510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350722','350722','浦城县','pcx',2,'350700',118.5368220000,27.9204120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350723','350723','光泽县','gzx',2,'350700',117.3378970000,27.5428030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350724','350724','松溪县','sxx',2,'350700',118.7834910000,27.5257850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350725','350725','政和县','zhx',2,'350700',118.8586610000,27.3653980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350781','350781','邵武市','sws',2,'350700',117.4915440000,27.3379520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350782','350782','武夷山市','wyss',2,'350700',118.0327960000,27.7517330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350783','350783','建瓯市','jos',2,'350700',118.3217650000,27.0350200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350784','350784','建阳市','jys',2,'350700',118.1226700000,27.3320670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350800','350800','龙岩市','lys',1,'350000',117.0297800000,25.0916030000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350801','350801','市辖区','sxq',1,'350800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350802','350802','新罗区','xlq',1,'350800',117.0307210000,25.0918000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350821','350821','长汀县','ctx',2,'350800',116.3610070000,25.8422780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350822','350822','永定县','ydx',2,'350800',116.7326910000,24.7204420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350823','350823','上杭县','shx',2,'350800',116.4247740000,25.0500190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350824','350824','武平县','wpx',2,'350800',116.1009280000,25.0886500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350825','350825','连城县','lcx',2,'350800',116.7566870000,25.7085060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350881','350881','漳平市','zps',2,'350800',117.4207300000,25.2915970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350900','350900','宁德市','nds',1,'350000',119.5270820000,26.6592400000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350901','350901','市辖区','sxq',1,'350900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350902','350902','蕉城区','jcq',1,'350900',119.5272250000,26.6592530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350921','350921','霞浦县','xpx',2,'350900',120.0052140000,26.8820680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350922','350922','古田县','gtx',2,'350900',118.7431560000,26.5774910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350923','350923','屏南县','pnx',2,'350900',118.9875440000,26.9108260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350924','350924','寿宁县','snx',2,'350900',119.5067330000,27.4577980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350925','350925','周宁县','znx',2,'350900',119.3382390000,27.1031060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350926','350926','柘荣县','zrx',2,'350900',119.8982260000,27.2361630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350981','350981','福安市','fas',2,'350900',119.6507980000,27.0842460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('350982','350982','福鼎市','fds',2,'350900',120.2197610000,27.3188840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360000','360000','江西省','jxs',0,'0',115.8921510000,28.6764930000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360100','360100','南昌市','ncs',1,'360000',115.8921510000,28.6764930000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360101','360101','市辖区','sxq',1,'360100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360102','360102','东湖区','dhq',1,'360100',115.9157740000,28.6741730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360103','360103','西湖区','xhq',1,'360100',115.8939190000,28.6695130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360104','360104','青云谱区','qypq',1,'360100',115.9297690000,28.6209830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360105','360105','湾里区','wlq',1,'360100',115.7389980000,28.7063550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360111','360111','青山湖区','qshq',1,'360100',115.9246040000,28.6549650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360121','360121','南昌县','ncx',2,'360100',116.0056120000,28.2936540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360122','360122','新建县','xjx',2,'360100',115.7882960000,28.6727560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360123','360123','安义县','ayx',2,'360100',115.5511820000,28.8331310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360124','360124','进贤县','jxx',2,'360100',116.1726320000,28.2246320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360200','360200','景德镇市','jdzs',1,'360000',117.2146640000,29.2925600000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360201','360201','市辖区','sxq',1,'360200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360202','360202','昌江区','cjq',1,'360200',117.1950230000,29.2884650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360203','360203','珠山区','zsq',1,'360200',117.2148140000,29.2928120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360222','360222','浮梁县','flx',2,'360200',117.2176110000,29.3522510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360281','360281','乐平市','lps',2,'360200',117.1293760000,28.9673610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360300','360300','萍乡市','pxs',1,'360000',113.8521860000,27.6229460000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360301','360301','市辖区','sxq',1,'360300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360302','360302','安源区','ayq',1,'360300',113.8550440000,27.6258260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360313','360313','湘东区','xdq',1,'360300',113.7456000000,27.6393190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360321','360321','莲花县','lhx',2,'360300',113.9555820000,27.1278070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360322','360322','上栗县','slx',2,'360300',113.8005250000,27.8770410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360323','360323','芦溪县','lxx',2,'360300',114.0412060000,27.6336330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360400','360400','九江市','jjs',1,'360000',115.9928110000,29.7120340000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360401','360401','市辖区','sxq',1,'360400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360402','360402','庐山区','lsq',1,'360400',116.0166550000,29.7080350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360403','360403','浔阳区','xyq',1,'360400',115.9959470000,29.7246500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360421','360421','九江县','jjx',2,'360400',115.8929770000,29.6102640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360423','360423','武宁县','wnx',2,'360400',115.1056460000,29.2601820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360424','360424','修水县','xsx',2,'360400',114.5734280000,29.0327290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360425','360425','永修县','yxx',2,'360400',115.8090550000,29.0182120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360426','360426','德安县','dax',2,'360400',115.7626110000,29.3274740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360427','360427','星子县','xzx',2,'360400',116.0437430000,29.4561690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360428','360428','都昌县','dcx',2,'360400',116.2051140000,29.2751050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360429','360429','湖口县','hkx',2,'360400',116.2443130000,29.7263000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360430','360430','彭泽县','pzx',2,'360400',116.5558400000,29.8988650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360481','360481','瑞昌市','rcs',2,'360400',115.6714410000,29.6816000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360500','360500','新余市','xys',1,'360000',114.9308350000,27.8108340000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360501','360501','市辖区','sxq',1,'360500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360502','360502','渝水区','ysq',1,'360500',114.9239230000,27.8191710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360521','360521','分宜县','fyx',2,'360500',114.6752620000,27.8113010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360600','360600','鹰潭市','yts',1,'360000',117.0338380000,28.2386380000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360601','360601','市辖区','sxq',1,'360600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360602','360602','月湖区','yhq',1,'360600',117.0341120000,28.2390760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360622','360622','余江县','yjx',2,'360600',116.8227630000,28.2061770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360681','360681','贵溪市','gxs',2,'360600',117.2121030000,28.2836930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360700','360700','赣州市','gzs',1,'360000',114.9402780000,25.8509700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360701','360701','市辖区','sxq',1,'360700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360702','360702','章贡区','zgq',1,'360700',114.9387200000,25.8513670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360721','360721','赣县','gx',2,'360700',115.0184610000,25.8654320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360722','360722','信丰县','xfx',2,'360700',114.9308930000,25.3802300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360723','360723','大余县','dyx',2,'360700',114.3622430000,25.3959370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360724','360724','上犹县','syx',2,'360700',114.5405370000,25.7942840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360725','360725','崇义县','cyx',2,'360700',114.3073480000,25.6879110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360726','360726','安远县','ayx',2,'360700',115.3923280000,25.1345910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360727','360727','龙南县','lnx',2,'360700',114.7926570000,24.9047600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360728','360728','定南县','dnx',2,'360700',115.0326700000,24.7742770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360729','360729','全南县','qnx',2,'360700',114.5315890000,24.7426510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360730','360730','宁都县','ndx',2,'360700',116.0187820000,26.4720540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360731','360731','于都县','ydx',2,'360700',115.4111980000,25.9550330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360732','360732','兴国县','xgx',2,'360700',115.3518960000,26.3304890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360733','360733','会昌县','hcx',2,'360700',115.7911580000,25.5991250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360734','360734','寻乌县','xwx',2,'360700',115.6513990000,24.9541360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360735','360735','石城县','scx',2,'360700',116.3422490000,26.3265820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360781','360781','瑞金市','rjs',2,'360700',116.0348540000,25.8752780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360782','360782','南康市','nks',2,'360700',114.7569330000,25.6617210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360800','360800','吉安市','jas',1,'360000',114.9863730000,27.1116990000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360801','360801','市辖区','sxq',1,'360800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360802','360802','吉州区','jzq',1,'360800',114.9873310000,27.1123670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360803','360803','青原区','qyq',1,'360800',115.0163060000,27.1058790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360821','360821','吉安县','jax',2,'360800',114.9051170000,27.0400420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360822','360822','吉水县','jsx',2,'360800',115.1345690000,27.2134450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360823','360823','峡江县','xjx',2,'360800',115.3193310000,27.5808620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360824','360824','新干县','xgx',2,'360800',115.3992940000,27.7557580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360825','360825','永丰县','yfx',2,'360800',115.4355590000,27.3210870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360826','360826','泰和县','thx',2,'360800',114.9013930000,26.7901640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360827','360827','遂川县','scx',2,'360800',114.5168900000,26.3237050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360828','360828','万安县','wax',2,'360800',114.7846940000,26.4620850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360829','360829','安福县','afx',2,'360800',114.6138400000,27.3827460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360830','360830','永新县','yxx',2,'360800',114.2425340000,26.9447210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360881','360881','井冈山市','jgss',2,'360800',114.2844210000,26.7459190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360900','360900','宜春市','ycs',1,'360000',114.3911360000,27.8043000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360901','360901','市辖区','sxq',1,'360900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360902','360902','袁州区','yzq',1,'360900',114.3873790000,27.8001170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360921','360921','奉新县','fxx',2,'360900',115.3898990000,28.7006720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360922','360922','万载县','wzx',2,'360900',114.4490120000,28.1045280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360923','360923','上高县','sgx',2,'360900',114.9326530000,28.2347890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360924','360924','宜丰县','yfx',2,'360900',114.7873810000,28.3882890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360925','360925','靖安县','jax',2,'360900',115.3617440000,28.8605400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360926','360926','铜鼓县','tgx',2,'360900',114.3701400000,28.5209560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360981','360981','丰城市','fcs',2,'360900',115.7860050000,28.1915840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360982','360982','樟树市','zss',2,'360900',115.5433880000,28.0558980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('360983','360983','高安市','gas',2,'360900',115.3815270000,28.4209510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361000','361000','抚州市','fzs',1,'360000',116.3583510000,27.9838500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361001','361001','市辖区','sxq',1,'361000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361002','361002','临川区','lcq',1,'361000',116.3614040000,27.9819190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361021','361021','南城县','ncx',2,'361000',116.6394500000,27.5553100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361022','361022','黎川县','lcx',2,'361000',116.9145700000,27.2925610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361023','361023','南丰县','nfx',2,'361000',116.5329940000,27.2101320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361024','361024','崇仁县','crx',2,'361000',116.0591090000,27.7609070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361025','361025','乐安县','lax',2,'361000',115.8384320000,27.4201010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361026','361026','宜黄县','yhx',2,'361000',116.2230230000,27.5465120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361027','361027','金溪县','jxx',2,'361000',116.7787510000,27.9073870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361028','361028','资溪县','zxx',2,'361000',117.0660950000,27.7065300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361029','361029','东乡县','dxx',2,'361000',116.6053410000,28.2325000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361030','361030','广昌县','gcx',2,'361000',116.3272910000,26.8384260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361100','361100','上饶市','srs',1,'360000',117.9711850000,28.4444200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361101','361101','市辖区','sxq',1,'361100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361102','361102','信州区','xzq',1,'361100',117.9705220000,28.4453780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361121','361121','上饶县','srx',2,'361100',117.9061200000,28.4538970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361122','361122','广丰县','gfx',2,'361100',118.1898520000,28.4402850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361123','361123','玉山县','ysx',2,'361100',118.2444080000,28.6734790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361124','361124','铅山县','qsx',2,'361100',117.7119060000,28.3108920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361125','361125','横峰县','hfx',2,'361100',117.6082470000,28.4151030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361126','361126','弋阳县','yyx',2,'361100',117.4350020000,28.4023910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361127','361127','余干县','ygx',2,'361100',116.6910720000,28.6917300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361128','361128','鄱阳县','pyx',2,'361100',116.6737480000,28.9933740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361129','361129','万年县','wnx',2,'361100',117.0701500000,28.6925890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361130','361130','婺源县','wyx',2,'361100',117.8621900000,29.2540150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('361181','361181','德兴市','dxs',2,'361100',117.5787320000,28.9450340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370000','370000','山东省','sds',0,'0',117.0009230000,36.6758070000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370100','370100','济南市','jns',1,'370000',117.0009230000,36.6758070000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370101','370101','市辖区','sxq',1,'370100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370102','370102','历下区','lxq',1,'370100',117.0265000000,36.6549970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370103','370103','市中区','szq',1,'370100',116.9893940000,36.6569300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370104','370104','槐荫区','hyq',1,'370100',116.9867560000,36.6659790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370105','370105','天桥区','tqq',1,'370100',116.9637610000,36.7093150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370112','370112','历城区','lcq',1,'370100',117.0839880000,36.6923810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370113','370113','长清区','cqq',1,'370100',116.7599360000,36.5516380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370124','370124','平阴县','pyx',2,'370100',116.4550540000,36.2869230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370125','370125','济阳县','jyx',2,'370100',117.1760350000,36.9767720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370126','370126','商河县','shx',2,'370100',117.1563690000,37.3105440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370181','370181','章丘市','zqs',2,'370100',117.5406900000,36.7120900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370200','370200','青岛市','qds',1,'370000',120.3551730000,36.0829820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370201','370201','市辖区','sxq',1,'370200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370202','370202','市南区','snq',1,'370200',120.3010300000,36.0598840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370203','370203','市北区','sbq',1,'370200',120.3902480000,36.0938770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370205','370205','四方区','sfq',1,'370200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370211','370211','黄岛区','hdq',1,'370200',119.9860980000,35.8704640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370212','370212','崂山区','lsq',1,'370200',120.4589080000,36.0856360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370213','370213','李沧区','lcq',1,'370200',120.4029260000,36.1885570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370214','370214','城阳区','cyq',1,'370200',120.3906560000,36.3082990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370281','370281','胶州市','jzs',2,'370200',120.0257870000,36.2930210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370282','370282','即墨市','jms',2,'370200',120.4514770000,36.3700670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370283','370283','平度市','pds',2,'370200',119.9650410000,36.7852890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370284','370284','胶南市','jns',2,'370200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370285','370285','莱西市','lxs',2,'370200',120.5215840000,36.8641530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370300','370300','淄博市','zbs',1,'370000',118.0476480000,36.8149390000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370301','370301','市辖区','sxq',1,'370300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370302','370302','淄川区','zcq',1,'370300',117.9676960000,36.6472720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370303','370303','张店区','zdq',1,'370300',118.0535210000,36.8070490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370304','370304','博山区','bsq',1,'370300',117.8582300000,36.4975670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370305','370305','临淄区','lzq',1,'370300',118.3060180000,36.8166570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370306','370306','周村区','zcq',1,'370300',117.8510360000,36.8036990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370321','370321','桓台县','htx',2,'370300',118.1015560000,36.9597730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370322','370322','高青县','gqx',2,'370300',117.8298390000,37.1695810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370323','370323','沂源县','yyx',2,'370300',118.1661610000,36.1862820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370400','370400','枣庄市','zzs',1,'370000',117.5579640000,34.8564240000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370401','370401','市辖区','sxq',1,'370400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370402','370402','市中区','szq',1,'370400',117.5572810000,34.8566510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370403','370403','薛城区','xcq',1,'370400',117.2652930000,34.7978900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370404','370404','峄城区','ycq',1,'370400',117.5863160000,34.7677130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370405','370405','台儿庄区','tezq',1,'370400',117.7347470000,34.5648150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370406','370406','山亭区','stq',1,'370400',117.4589680000,35.0960770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370481','370481','滕州市','tzs',2,'370400',117.1620980000,35.0884980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370500','370500','东营市','dys',1,'370000',118.6647100000,37.4345640000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370501','370501','市辖区','sxq',1,'370500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370502','370502','东营区','dyq',1,'370500',118.5075430000,37.4615670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370503','370503','河口区','hkq',1,'370500',118.5296130000,37.8860150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370521','370521','垦利县','klx',2,'370500',118.5513140000,37.5886790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370522','370522','利津县','ljx',2,'370500',118.2488540000,37.4933650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370523','370523','广饶县','grx',2,'370500',118.4075220000,37.0516100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370600','370600','烟台市','yts',1,'370000',121.3913820000,37.5392970000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370601','370601','市辖区','sxq',1,'370600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370602','370602','芝罘区','zfq',1,'370600',121.3275190000,37.5386260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370611','370611','福山区','fsq',1,'370600',121.2594290000,37.4979250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370612','370612','牟平区','mpq',1,'370600',121.6015100000,37.3883560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370613','370613','莱山区','lsq',1,'370600',121.4321870000,37.4599150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370634','370634','长岛县','cdx',2,'370600',120.7383450000,37.9161940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370681','370681','龙口市','lks',2,'370600',120.5283280000,37.6484460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370682','370682','莱阳市','lys',2,'370600',120.7111510000,36.9770370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370683','370683','莱州市','lzs',2,'370600',119.9421350000,37.1827250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370684','370684','蓬莱市','pls',2,'370600',120.7626890000,37.8111680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370685','370685','招远市','zys',2,'370600',120.4031420000,37.3649190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370686','370686','栖霞市','qxs',2,'370600',120.8340970000,37.3058540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370687','370687','海阳市','hys',2,'370600',121.1683920000,36.7806570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370700','370700','潍坊市','wfs',1,'370000',119.1070780000,36.7092500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370701','370701','市辖区','sxq',1,'370700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370702','370702','潍城区','wcq',1,'370700',119.1037240000,36.6960060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370703','370703','寒亭区','htq',1,'370700',119.2119110000,36.7722450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370704','370704','坊子区','fzq',1,'370700',119.1663260000,36.6546160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370705','370705','奎文区','kwq',1,'370700',119.1483820000,36.7109760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370724','370724','临朐县','lqx',2,'370700',118.5398760000,36.5163710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370725','370725','昌乐县','clx',2,'370700',118.8399950000,36.7032530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370781','370781','青州市','qzs',2,'370700',118.4846930000,36.6978550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370782','370782','诸城市','zcs',2,'370700',119.4031820000,35.9970930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370783','370783','寿光市','sgs',2,'370700',118.7085800000,36.8842010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370784','370784','安丘市','aqs',2,'370700',119.2068860000,36.4274170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370785','370785','高密市','gms',2,'370700',119.7570330000,36.3775400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370786','370786','昌邑市','cys',2,'370700',119.3945020000,36.8549370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370800','370800','济宁市','jns',1,'370000',116.5872450000,35.4153930000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370801','370801','市辖区','sxq',1,'370800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370802','370802','市中区','szq',1,'370800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370811','370811','任城区','rcq',1,'370800',116.5952610000,35.4148280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370826','370826','微山县','wsx',2,'370800',117.1286100000,34.8095250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370827','370827','鱼台县','ytx',2,'370800',116.6500230000,34.9977060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370828','370828','金乡县','jxx',2,'370800',116.3103640000,35.0697700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370829','370829','嘉祥县','jxx',2,'370800',116.3428850000,35.3980980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370830','370830','汶上县','wsx',2,'370800',116.4871460000,35.7217460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370831','370831','泗水县','ssx',2,'370800',117.2736050000,35.6532160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370832','370832','梁山县','lsx',2,'370800',116.0896300000,35.8018430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370881','370881','曲阜市','qfs',2,'370800',116.9918850000,35.5927880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370882','370882','兖州市','yzs',2,'370800',116.8289960000,35.5564450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370883','370883','邹城市','zcs',2,'370800',116.9667300000,35.4052590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370900','370900','泰安市','tas',1,'370000',117.1290630000,36.1949680000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370901','370901','市辖区','sxq',1,'370900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370902','370902','泰山区','tsq',1,'370900',117.1299840000,36.1893130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370903','370903','岱岳区','dyq',1,'370900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370921','370921','宁阳县','nyx',2,'370900',116.7992970000,35.7675400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370923','370923','东平县','dpx',2,'370900',116.4610520000,35.9304670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370982','370982','新泰市','xts',2,'370900',117.7660920000,35.9103870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('370983','370983','肥城市','fcs',2,'370900',116.7637030000,36.1856000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371000','371000','威海市','whs',1,'370000',122.1163940000,37.5096910000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371001','371001','市辖区','sxq',1,'371000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371002','371002','环翠区','hcq',1,'371000',122.1207870000,37.3094400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371081','371081','文登市','wds',2,'371000',122.1011550000,37.2112050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371082','371082','荣成市','rcs',2,'371000',122.4238420000,37.1585520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371083','371083','乳山市','rss',2,'371000',121.6165590000,36.8122140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371100','371100','日照市','rzs',1,'370000',119.4612080000,35.4285880000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371101','371101','市辖区','sxq',1,'371100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371102','371102','东港区','dgq',1,'371100',119.4577030000,35.4261520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371103','371103','岚山区','lsq',1,'371100',119.3158440000,35.1197940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371121','371121','五莲县','wlx',2,'371100',119.2067450000,35.7519360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371122','371122','莒县','jx',2,'371100',118.8328590000,35.5881150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371200','371200','莱芜市','lws',1,'370000',117.6777360000,36.2143970000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371201','371201','市辖区','sxq',1,'371200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371202','371202','莱城区','lcq',1,'371200',117.6783510000,36.2136620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371203','371203','钢城区','gcq',1,'371200',117.8203300000,36.0580380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371300','371300','临沂市','lys',1,'370000',118.3264430000,35.0652820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371301','371301','市辖区','sxq',1,'371300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371302','371302','兰山区','lsq',1,'371300',118.3276670000,35.0616310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371311','371311','罗庄区','lzq',1,'371300',118.2847950000,34.9972040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371312','371312','河东区','hdq',1,'371300',118.3982960000,35.0850040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371321','371321','沂南县','ynx',2,'371300',118.4553950000,35.5470020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371322','371322','郯城县','tcx',2,'371300',118.3429630000,34.6147410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371323','371323','沂水县','ysx',2,'371300',118.6345430000,35.7870290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371324','371324','苍山县','csx',2,'371300',118.0499680000,34.8555730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371325','371325','费县','fx',2,'371300',117.9688690000,35.2691740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371326','371326','平邑县','pyx',2,'371300',117.6318840000,35.5115190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371327','371327','莒南县','jnx',2,'371300',118.8383220000,35.1759110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371328','371328','蒙阴县','myx',2,'371300',117.9432710000,35.7124350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371329','371329','临沭县','lsx',2,'371300',118.6483790000,34.9170620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371400','371400','德州市','dzs',1,'370000',116.3074280000,37.4539680000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371401','371401','市辖区','sxq',1,'371400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371402','371402','德城区','dcq',1,'371400',116.3070760000,37.4539230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371421','371421','陵县','lx',2,'371400',116.5749290000,37.3328480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371422','371422','宁津县','njx',2,'371400',116.7937200000,37.6496190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371423','371423','庆云县','qyx',2,'371400',117.3905070000,37.7777240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371424','371424','临邑县','lyx',2,'371400',116.8670280000,37.1920440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371425','371425','齐河县','qhx',2,'371400',116.7583940000,36.7954970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371426','371426','平原县','pyx',2,'371400',116.4339040000,37.1644650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371427','371427','夏津县','xjx',2,'371400',116.0038160000,36.9505010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371428','371428','武城县','wcx',2,'371400',116.0786270000,37.2095270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371481','371481','乐陵市','lls',2,'371400',117.2166570000,37.7291150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371482','371482','禹城市','ycs',2,'371400',116.6425540000,36.9344850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371500','371500','聊城市','lcs',1,'370000',115.9803670000,36.4560130000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371501','371501','市辖区','sxq',1,'371500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371502','371502','东昌府区','dcfq',1,'371500',115.9800230000,36.4560600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371521','371521','阳谷县','ygx',2,'371500',115.7842870000,36.1137080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371522','371522','莘县','sx',2,'371500',115.6672910000,36.2375970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371523','371523','茌平县','cpx',2,'371500',116.2533500000,36.5919340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371524','371524','东阿县','dex',2,'371500',116.2488550000,36.3360040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371525','371525','冠县','gx',2,'371500',115.4448080000,36.4837530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371526','371526','高唐县','gtx',2,'371500',116.2296620000,36.8597550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371581','371581','临清市','lqs',2,'371500',115.7134620000,36.8425980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371600','371600','滨州市','bzs',1,'370000',118.0169740000,37.3835420000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371601','371601','市辖区','sxq',1,'371600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371602','371602','滨城区','bcq',1,'371600',118.0201490000,37.3848420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371621','371621','惠民县','hmx',2,'371600',117.5089410000,37.4838760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371622','371622','阳信县','yxx',2,'371600',117.5813260000,37.6404920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371623','371623','无棣县','wdx',2,'371600',117.6163250000,37.7408480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371624','371624','沾化县','zhx',2,'371600',118.1299020000,37.6984560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371625','371625','博兴县','bxx',2,'371600',118.1230960000,37.1470020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371626','371626','邹平县','zpx',2,'371600',117.7368070000,36.8780300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371700','371700','荷泽市','hzs',1,'370000',115.4693810000,35.2465310000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371701','371701','市辖区','sxq',1,'371700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371702','371702','牡丹区','mdq',1,'371700',115.4709460000,35.2431100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371721','371721','曹县','cx',2,'371700',115.5494820000,34.8232530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371722','371722','单县','dx',2,'371700',116.0826200000,34.7908510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371723','371723','成武县','cwx',2,'371700',115.8973490000,34.9473660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371724','371724','巨野县','jyx',2,'371700',116.0893410000,35.3909990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371725','371725','郓城县','ycx',2,'371700',115.9388500000,35.5947730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371726','371726','鄄城县','jcx',2,'371700',115.5143400000,35.5602570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371727','371727','定陶县','dtx',2,'371700',115.5696010000,35.0727010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('371728','371728','东明县','dmx',2,'371700',115.0984120000,35.2896370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410000','410000','河南省','hns',0,'0',113.6654120000,34.7579750000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410100','410100','郑州市','zzs',1,'410000',113.6654120000,34.7579750000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410101','410101','市辖区','sxq',1,'410100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410102','410102','中原区','zyq',1,'410100',113.6145800000,34.7554410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410103','410103','二七区','eqq',1,'410100',113.6265070000,34.7286680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410104','410104','管城回族区','gchzq',1,'410100',113.6646700000,34.7415140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410105','410105','金水区','jsq',1,'410100',113.6516430000,34.8125830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410106','410106','上街区','sjq',1,'410100',113.2985160000,34.8056000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410108','410108','邙山区','msq',1,'410100',113.6668390000,34.8714250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410122','410122','中牟县','zmx',2,'410100',114.0225210000,34.7219760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410181','410181','巩义市','gys',2,'410100',112.9828300000,34.7521800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410182','410182','荥阳市','xys',2,'410100',113.3915230000,34.7890770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410183','410183','新密市','xms',2,'410100',113.3806160000,34.5378460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410184','410184','新郑市','xzs',2,'410100',113.7396700000,34.3942190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410185','410185','登封市','dfs',2,'410100',113.0377680000,34.4599390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410200','410200','开封市','kfs',1,'410000',114.3414470000,34.7970490000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410201','410201','市辖区','sxq',1,'410200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410202','410202','龙亭区','ltq',1,'410200',114.3533480000,34.7998330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410203','410203','顺河回族区','shhzq',1,'410200',114.3648750000,34.8004590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410204','410204','鼓楼区','glq',1,'410200',114.3485000000,34.7923830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410205','410205','南关区','ngq',1,'410200',114.3502460000,34.7797270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410211','410211','郊区','jq',1,'410200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410221','410221','杞县','qx',2,'410200',114.7704720000,34.5545850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410222','410222','通许县','txx',2,'410200',114.4677340000,34.4773020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410223','410223','尉氏县','wsx',2,'410200',114.1939270000,34.4122560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410224','410224','开封县','kfx',2,'410200',114.4376220000,34.7564760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410225','410225','兰考县','lkx',2,'410200',114.8205720000,34.8298990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410300','410300','洛阳市','lys',1,'410000',112.4344680000,34.6630410000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410301','410301','市辖区','sxq',1,'410300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410302','410302','老城区','lcq',1,'410300',112.4783040000,34.6872870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410303','410303','西工区','xgq',1,'410300',112.4432320000,34.6678470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410304','410304','廛河回族区','chhzq',1,'410300',112.4916250000,34.6847380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410305','410305','涧西区','jxq',1,'410300',112.3933220000,34.6579090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410306','410306','吉利区','jlq',1,'410300',112.5847960000,34.8990930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410307','410307','洛龙区','llq',1,'410300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410322','410322','孟津县','mjx',2,'410300',112.4438920000,34.8264850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410323','410323','新安县','xax',2,'410300',112.1414030000,34.7286790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410324','410324','栾川县','lcx',2,'410300',111.6183860000,33.7831950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410325','410325','嵩县','sx',2,'410300',112.0877650000,34.1315630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410326','410326','汝阳县','ryx',2,'410300',112.4737890000,34.1532300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410327','410327','宜阳县','yyx',2,'410300',112.1799890000,34.5164780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410328','410328','洛宁县','lnx',2,'410300',111.6553990000,34.3871790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410329','410329','伊川县','ycx',2,'410300',112.4293840000,34.4234160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410381','410381','偃师市','yss',2,'410300',112.7877390000,34.7230420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410400','410400','平顶山市','pdss',1,'410000',113.3077180000,33.7352410000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410401','410401','市辖区','sxq',1,'410400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410402','410402','新华区','xhq',1,'410400',113.2990610000,33.7375790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410403','410403','卫东区','wdq',1,'410400',113.3103270000,33.7392850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410404','410404','石龙区','slq',1,'410400',112.8898850000,33.9015380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410411','410411','湛河区','zhq',1,'410400',113.3208730000,33.7256810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410421','410421','宝丰县','bfx',2,'410400',113.0668120000,33.8663590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410422','410422','叶县','yx',2,'410400',113.3582980000,33.6212520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410423','410423','鲁山县','lsx',2,'410400',112.9067030000,33.7403250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410425','410425','郏县','jx',2,'410400',113.2204510000,33.9719930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410481','410481','舞钢市','wgs',2,'410400',113.5262500000,33.3020820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410482','410482','汝州市','rzs',2,'410400',112.8453360000,34.1674080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410500','410500','安阳市','ays',1,'410000',114.3524820000,36.1034420000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410501','410501','市辖区','sxq',1,'410500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410502','410502','文峰区','wfq',1,'410500',114.3525620000,36.0981010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410503','410503','北关区','bgq',1,'410500',114.3526460000,36.1097800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410505','410505','殷都区','ydq',1,'410500',114.3000980000,36.1089740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410506','410506','龙安区','laq',1,'410500',114.3235220000,36.0955680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410522','410522','安阳县','ayx',2,'410500',114.1302070000,36.1305850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410523','410523','汤阴县','tyx',2,'410500',114.3623570000,35.9223490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410526','410526','滑县','hx',2,'410500',114.5240000000,35.5746280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410527','410527','内黄县','nhx',2,'410500',114.9045820000,35.9537020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410581','410581','林州市','lzs',2,'410500',113.8237670000,36.0634030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410600','410600','鹤壁市','hbs',1,'410000',114.2954440000,35.7482360000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410601','410601','市辖区','sxq',1,'410600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410602','410602','鹤山区','hsq',1,'410600',114.1665510000,35.9361280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410603','410603','山城区','scq',1,'410600',114.1842020000,35.8960580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410611','410611','淇滨区','qbq',1,'410600',114.2939170000,35.7483820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410621','410621','浚县','jx',2,'410600',114.5501620000,35.6712820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410622','410622','淇县','qx',2,'410600',114.2003790000,35.6094780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410700','410700','新乡市','xxs',1,'410000',113.8839910000,35.3026160000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410701','410701','市辖区','sxq',1,'410700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410702','410702','红旗区','hqq',1,'410700',113.8781580000,35.3026840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410703','410703','卫滨区','wbq',1,'410700',113.8660650000,35.3049050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410704','410704','凤泉区','fqq',1,'410700',113.9067120000,35.3798550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410711','410711','牧野区','myq',1,'410700',113.8971600000,35.3129740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410721','410721','新乡县','xxx',2,'410700',113.8061860000,35.1900210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410724','410724','获嘉县','hjx',2,'410700',113.6572490000,35.2616850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410725','410725','原阳县','yyx',2,'410700',113.9659660000,35.0540010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410726','410726','延津县','yjx',2,'410700',114.2009820000,35.1495150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410727','410727','封丘县','fqx',2,'410700',114.4234050000,35.0405700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410728','410728','长垣县','cyx',2,'410700',114.6738070000,35.1961500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410781','410781','卫辉市','whs',2,'410700',114.0658550000,35.4042950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410782','410782','辉县市','hxs',2,'410700',113.8025180000,35.4613180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410800','410800','焦作市','jzs',1,'410000',113.2382660000,35.2390400000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410801','410801','市辖区','sxq',1,'410800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410802','410802','解放区','jfq',1,'410800',113.2261260000,35.2413530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410803','410803','中站区','zzq',1,'410800',113.1754850000,35.2361450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410804','410804','马村区','mcq',1,'410800',113.3217030000,35.2654530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410811','410811','山阳区','syq',1,'410800',113.2676600000,35.2147600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410821','410821','修武县','xwx',2,'410800',113.4474650000,35.2299230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410822','410822','博爱县','bax',2,'410800',113.0693130000,35.1703510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410823','410823','武陟县','wzx',2,'410800',113.4083340000,35.0988500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410825','410825','温县','wx',2,'410800',113.0791180000,34.9412330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410881','410881','济源市','jys',2,'410800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410882','410882','沁阳市','qys',2,'410800',112.9345380000,35.0890100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410883','410883','孟州市','mzs',2,'410800',112.7870800000,34.9096300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410900','410900','濮阳市','pys',1,'410000',115.0412990000,35.7682340000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410901','410901','市辖区','sxq',1,'410900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410902','410902','华龙区','hlq',1,'410900',115.0318400000,35.7604730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410922','410922','清丰县','qfx',2,'410900',115.1072870000,35.9024130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410923','410923','南乐县','nlx',2,'410900',115.2043360000,36.0752040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410926','410926','范县','fx',2,'410900',115.5042120000,35.8519770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410927','410927','台前县','tqx',2,'410900',115.8556810000,35.9964740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('410928','410928','濮阳县','pyx',2,'410900',115.0238440000,35.7103490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411000','411000','许昌市','xcs',1,'410000',113.8260630000,34.0229560000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411001','411001','市辖区','sxq',1,'411000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411002','411002','魏都区','wdq',1,'411000',113.8283070000,34.0271100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411023','411023','许昌县','xcx',2,'411000',113.8428980000,34.0050180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411024','411024','鄢陵县','ylx',2,'411000',114.1885070000,34.1005020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411025','411025','襄城县','xcx',2,'411000',113.4931660000,33.8559430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411081','411081','禹州市','yzs',2,'411000',113.4713160000,34.1544030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411082','411082','长葛市','cgs',2,'411000',113.7689120000,34.2192570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411100','411100','漯河市','lhs',1,'410000',114.0264050000,33.5758550000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411101','411101','市辖区','sxq',1,'411100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411102','411102','源汇区','yhq',1,'411100',114.0179480000,33.5654410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411103','411103','郾城区','ycq',1,'411100',114.0168130000,33.5888970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411104','411104','召陵区','zlq',1,'411100',114.0516860000,33.5675550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411121','411121','舞阳县','wyx',2,'411100',113.6105650000,33.4362780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411122','411122','临颍县','lyx',2,'411100',113.9388910000,33.8060900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411200','411200','三门峡市','smxs',1,'410000',111.1940990000,34.7773380000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411201','411201','市辖区','sxq',1,'411200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411202','411202','湖滨区','hbq',1,'411200',111.1948700000,34.7781200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411221','411221','渑池县','mcx',2,'411200',111.7629920000,34.7634870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411222','411222','陕县','sx',2,'411200',111.1038510000,34.7202440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411224','411224','卢氏县','lsx',2,'411200',111.0526490000,34.0539950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411281','411281','义马市','yms',2,'411200',111.8694170000,34.7468680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411282','411282','灵宝市','lbs',2,'411200',110.8857700000,34.5212640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411300','411300','南阳市','nys',1,'410000',112.5409180000,32.9990820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411301','411301','市辖区','sxq',1,'411300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411302','411302','宛城区','wcq',1,'411300',112.5445910000,32.9948570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411303','411303','卧龙区','wlq',1,'411300',112.5287890000,32.9898770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411321','411321','南召县','nzx',2,'411300',112.4355830000,33.4886170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411322','411322','方城县','fcx',2,'411300',113.0109330000,33.2551380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411323','411323','西峡县','xxx',2,'411300',111.4857720000,33.3029810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411324','411324','镇平县','zpx',2,'411300',112.2327220000,33.0366510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411325','411325','内乡县','nxx',2,'411300',111.8438010000,33.0463580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411326','411326','淅川县','xcx',2,'411300',111.4890260000,33.1361060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411327','411327','社旗县','sqx',2,'411300',112.9382790000,33.0561260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411328','411328','唐河县','thx',2,'411300',112.8384920000,32.6878920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411329','411329','新野县','xyx',2,'411300',112.3656240000,32.5240060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411330','411330','桐柏县','tbx',2,'411300',113.4060590000,32.3671530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411381','411381','邓州市','dzs',2,'411300',112.0927160000,32.6816420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411400','411400','商丘市','sqs',1,'410000',115.6504970000,34.4370540000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411401','411401','市辖区','sxq',1,'411400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411402','411402','梁园区','lyq',1,'411400',115.6545900000,34.4365530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411403','411403','睢阳区','hyq',1,'411400',115.6538130000,34.3905360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411421','411421','民权县','mqx',2,'411400',115.1481460000,34.6484550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411422','411422','睢县','hx',2,'411400',115.0701090000,34.4284330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411423','411423','宁陵县','nlx',2,'411400',115.3200550000,34.4492990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411424','411424','柘城县','zcx',2,'411400',115.3074330000,34.0752770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411425','411425','虞城县','ycx',2,'411400',115.8638110000,34.3996340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411426','411426','夏邑县','xyx',2,'411400',116.1398900000,34.2408940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411481','411481','永城市','ycs',2,'411400',116.4496720000,33.9313180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411500','411500','信阳市','xys',1,'410000',114.0750310000,32.1232740000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411501','411501','市辖区','sxq',1,'411500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411502','411502','师河区','shq',1,'411500',114.0750310000,32.1232740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411503','411503','平桥区','pqq',1,'411500',114.1260270000,32.0983950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411521','411521','罗山县','lsx',2,'411500',114.5334140000,32.2032060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411522','411522','光山县','gsx',2,'411500',114.9035770000,32.0103980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411523','411523','新县','xx',2,'411500',114.8770500000,31.6351500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411524','411524','商城县','scx',2,'411500',115.4062970000,31.7999820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411525','411525','固始县','gsx',2,'411500',115.6673280000,32.1830740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411526','411526','潢川县','hcx',2,'411500',115.0501230000,32.1340240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411527','411527','淮滨县','hbx',2,'411500',115.4154510000,32.4526390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411528','411528','息县','xx',2,'411500',114.7407130000,32.3447440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411600','411600','周口市','zks',1,'410000',114.6496530000,33.6203570000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411601','411601','市辖区','sxq',1,'411600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411602','411602','川汇区','chq',1,'411600',114.6521360000,33.6148360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411621','411621','扶沟县','fgx',2,'411600',114.3920080000,34.0540610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411622','411622','西华县','xhx',2,'411600',114.5300670000,33.7843780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411623','411623','商水县','ssx',2,'411600',114.6092700000,33.5438450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411624','411624','沈丘县','sqx',2,'411600',115.0783750000,33.3955140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411625','411625','郸城县','dcx',2,'411600',115.1890000000,33.6438520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411626','411626','淮阳县','hyx',2,'411600',114.8701660000,33.7325470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411627','411627','太康县','tkx',2,'411600',114.8538340000,34.0653120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411628','411628','鹿邑县','lyx',2,'411600',115.4863860000,33.8610670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411681','411681','项城市','xcs',2,'411600',114.8995210000,33.4430850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411700','411700','驻马店市','zmds',1,'410000',114.0247360000,32.9801690000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411701','411701','市辖区','sxq',1,'411700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411702','411702','驿城区','ycq',1,'411700',114.0291490000,32.9775590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411721','411721','西平县','xpx',2,'411700',114.0268640000,33.3823150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411722','411722','上蔡县','scx',2,'411700',114.2668920000,33.2647190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411723','411723','平舆县','pyx',2,'411700',114.6371050000,32.9556260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411724','411724','正阳县','zyx',2,'411700',114.3894800000,32.6018260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411725','411725','确山县','qsx',2,'411700',114.0266790000,32.8015380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411726','411726','泌阳县','byx',2,'411700',113.3260500000,32.7251290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411727','411727','汝南县','rnx',2,'411700',114.3594950000,33.0045350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411728','411728','遂平县','spx',2,'411700',114.0037100000,33.1469800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('411729','411729','新蔡县','xcx',2,'411700',114.9752460000,32.7499480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420000','420000','湖北省','hbs',0,'0',114.2985720000,30.5843550000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420100','420100','武汉市','whs',1,'420000',114.2985720000,30.5843550000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420101','420101','市辖区','sxq',1,'420100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420102','420102','江岸区','jaq',1,'420100',114.3699850000,30.6777090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420103','420103','江汉区','jhq',1,'420100',114.2912610000,30.5713200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420104','420104','乔口区','qkq',1,'420100',114.2647070000,30.5715270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420105','420105','汉阳区','hyq',1,'420100',114.2551010000,30.5237910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420106','420106','武昌区','wcq',1,'420100',114.3398940000,30.5946980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420107','420107','青山区','qsq',1,'420100',114.4058310000,30.6292310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420111','420111','洪山区','hsq',1,'420100',114.4838010000,30.6065790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420112','420112','东西湖区','dxhq',1,'420100',114.1415670000,30.6374020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420113','420113','汉南区','hnq',1,'420100',114.0817430000,30.3101590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420114','420114','蔡甸区','cdq',1,'420100',113.9417790000,30.4574660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420115','420115','江夏区','jxq',1,'420100',114.3166630000,30.3481650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420116','420116','黄陂区','hbq',1,'420100',114.2182840000,30.7865050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420117','420117','新洲区','xzq',1,'420100',114.8046960000,30.8428500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420200','420200','黄石市','hss',1,'420000',115.0770480000,30.2200740000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420201','420201','市辖区','sxq',1,'420200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420202','420202','黄石港区','hsgq',1,'420200',115.0901640000,30.2120860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420203','420203','西塞山区','xssq',1,'420200',115.0933540000,30.2053650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420204','420204','下陆区','xlq',1,'420200',114.9757550000,30.1778450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420205','420205','铁山区','tsq',1,'420200',114.9013660000,30.2060100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420222','420222','阳新县','yxx',2,'420200',115.2128830000,29.8415720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420281','420281','大冶市','dys',2,'420200',114.9748420000,30.0988040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420300','420300','十堰市','sys',1,'420000',110.7879160000,32.6469070000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420301','420301','市辖区','sxq',1,'420300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420302','420302','茅箭区','mjq',1,'420300',110.7862100000,32.6444630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420303','420303','张湾区','zwq',1,'420300',110.7723650000,32.6525160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420321','420321','郧县','yx',2,'420300',110.8120990000,32.8382670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420322','420322','郧西县','yxx',2,'420300',110.4264720000,32.9914570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420323','420323','竹山县','zsx',2,'420300',110.2296000000,32.2258600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420324','420324','竹溪县','zxx',2,'420300',109.7171960000,32.3153420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420325','420325','房县','fx',2,'420300',110.7419660000,32.0550020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420381','420381','丹江口市','djks',2,'420300',111.5137930000,32.5388390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420500','420500','宜昌市','ycs',1,'420000',111.2908430000,30.7026360000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420501','420501','市辖区','sxq',1,'420500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420502','420502','西陵区','xlq',1,'420500',111.2954680000,30.7024760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420503','420503','伍家岗区','wjgq',1,'420500',111.3072150000,30.6790530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420504','420504','点军区','djq',1,'420500',111.2681630000,30.6923220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420505','420505','猇亭区','xtq',1,'420500',111.4276420000,30.5307440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420506','420506','夷陵区','ylq',1,'420500',111.3267470000,30.7701990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420525','420525','远安县','yax',2,'420500',111.6433100000,31.0596260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420526','420526','兴山县','xsx',2,'420500',110.7544990000,31.3479500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420527','420527','秭归县','zgx',2,'420500',110.9767850000,30.8239080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420528','420528','长阳土家族自治县','cytjzzzx',2,'420500',111.1984750000,30.4665340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420529','420529','五峰土家族自治县','wftjzzzx',2,'420500',110.6749380000,30.1992520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420581','420581','宜都市','yds',2,'420500',111.4543670000,30.3872340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420582','420582','当阳市','dys',2,'420500',111.7934190000,30.8244920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420583','420583','枝江市','zjs',2,'420500',111.7517990000,30.4253640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420600','420600','襄樊市','xfs',1,'420000',112.1441460000,32.0424260000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420601','420601','市辖区','sxq',1,'420600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420602','420602','襄城区','xcq',1,'420600',112.1503270000,32.0150880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420606','420606','樊城区','fcq',1,'420600',112.1395700000,32.0585890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420607','420607','襄阳区','xyq',1,'420600',112.1973780000,32.0855170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420624','420624','南漳县','nzx',2,'420600',111.8444240000,31.7769200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420625','420625','谷城县','gcx',2,'420600',111.6401470000,32.2626760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420626','420626','保康县','bkx',2,'420600',111.2622350000,31.8735070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420682','420682','老河口市','lhks',2,'420600',111.6757320000,32.3854380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420683','420683','枣阳市','zys',2,'420600',112.7652680000,32.1230830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420684','420684','宜城市','ycs',2,'420600',112.2614410000,31.7092030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420700','420700','鄂州市','ezs',1,'420000',114.8905930000,30.3965360000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420701','420701','市辖区','sxq',1,'420700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420702','420702','梁子湖区','lzhq',1,'420700',114.6819670000,30.0981910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420703','420703','华容区','hrq',1,'420700',114.7414800000,30.5344680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420704','420704','鄂城区','ecq',1,'420700',114.8900120000,30.3966900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420800','420800','荆门市','jms',1,'420000',112.2042510000,31.0354200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420801','420801','市辖区','sxq',1,'420800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420802','420802','东宝区','dbq',1,'420800',112.2048040000,31.0334610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420804','420804','掇刀区','ddq',1,'420800',112.1984130000,30.9807980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420821','420821','京山县','jsx',2,'420800',113.1145950000,31.0224580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420822','420822','沙洋县','syx',2,'420800',112.5952180000,30.7035900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420881','420881','钟祥市','zxs',2,'420800',112.5872670000,31.1655730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420900','420900','孝感市','xgs',1,'420000',113.9266550000,30.9264230000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420901','420901','市辖区','sxq',1,'420900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420902','420902','孝南区','xnq',1,'420900',113.9258490000,30.9259660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420921','420921','孝昌县','xcx',2,'420900',113.9889640000,31.2516180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420922','420922','大悟县','dwx',2,'420900',114.1262490000,31.5654830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420923','420923','云梦县','ymx',2,'420900',113.7506160000,31.0216910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420981','420981','应城市','ycs',2,'420900',113.5738420000,30.9390380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420982','420982','安陆市','als',2,'420900',113.6904010000,31.2617400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('420984','420984','汉川市','hcs',2,'420900',113.8353010000,30.6521650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421000','421000','荆州市','jzs',1,'420000',112.2381300000,30.3268570000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421001','421001','市辖区','sxq',1,'421000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421002','421002','沙市区','ssq',1,'421000',112.2574330000,30.3158950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421003','421003','荆州区','jzq',1,'421000',112.1953540000,30.3506740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421022','421022','公安县','gax',2,'421000',112.2301790000,30.0590650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421023','421023','监利县','jlx',2,'421000',112.9043440000,29.8200790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421024','421024','江陵县','jlx',2,'421000',112.4173500000,30.0339190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421081','421081','石首市','sss',2,'421000',112.4088700000,29.7164370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421083','421083','洪湖市','hhs',2,'421000',113.4703040000,29.8129700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421087','421087','松滋市','szs',2,'421000',111.7781800000,30.1760370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421100','421100','黄冈市','hgs',1,'420000',114.8793650000,30.4477110000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421101','421101','市辖区','sxq',1,'421100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421102','421102','黄州区','hzq',1,'421100',114.8789340000,30.4474350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421121','421121','团风县','tfx',2,'421100',114.8720290000,30.6356900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421122','421122','红安县','hax',2,'421100',114.6150950000,31.2847770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421123','421123','罗田县','ltx',2,'421100',115.3989840000,30.7816790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421124','421124','英山县','ysx',2,'421100',115.6775300000,30.7357940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421125','421125','浠水县','xsx',2,'421100',115.2634400000,30.4548370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421126','421126','蕲春县','qcx',2,'421100',115.4339640000,30.2349270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421127','421127','黄梅县','hmx',2,'421100',115.9425480000,30.0751130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421181','421181','麻城市','mcs',2,'421100',115.0254100000,31.1779060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421182','421182','武穴市','wxs',2,'421100',115.5624200000,29.8493420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421200','421200','咸宁市','xns',1,'420000',114.3289630000,29.8327980000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421201','421201','市辖区','sxq',1,'421200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421202','421202','咸安区','xaq',1,'421200',114.3338940000,29.8247160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421221','421221','嘉鱼县','jyx',2,'421200',113.9215470000,29.9733630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421222','421222','通城县','tcx',2,'421200',113.8141310000,29.2460760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421223','421223','崇阳县','cyx',2,'421200',114.0499580000,29.5410100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421224','421224','通山县','tsx',2,'421200',114.4931630000,29.6044550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421281','421281','赤壁市','cbs',2,'421200',113.8836600000,29.7168790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421300','421300','随州市','szs',1,'420000',113.3737700000,31.7174970000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421301','421301','市辖区','sxq',1,'421300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421302','421302','曾都区','cdq',1,'421300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('421381','421381','广水市','gss',2,'421300',113.8266010000,31.6177310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422800','422800','恩施土家族苗族自治州','estjzmzzzz',1,'420000',109.4869900000,30.2831140000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422801','422801','恩施市','ess',2,'422800',109.4867610000,30.2824060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422802','422802','利川市','lcs',2,'422800',108.9434910000,30.2942470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422822','422822','建始县','jsx',2,'422800',109.7238220000,30.6016320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422823','422823','巴东县','bdx',2,'422800',110.3366650000,31.0414030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422825','422825','宣恩县','xex',2,'422800',109.4828190000,29.9886700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422826','422826','咸丰县','xfx',2,'422800',109.1504100000,29.6789670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422827','422827','来凤县','lfx',2,'422800',109.4083280000,29.5069450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('422828','422828','鹤峰县','hfx',2,'422800',110.0336990000,29.8872980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('429000','429000','省直辖行政单位','szxxzdw',1,'420000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('429004','429004','仙桃市','xts',2,'429000',113.4539740000,30.3649530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('429005','429005','潜江市','qjs',2,'429000',112.8968660000,30.4212150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('429006','429006','天门市','tms',2,'429000',113.1658620000,30.6530610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('429021','429021','神农架林区','snjlq',1,'429000',110.6715250000,31.7444490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430000','430000','湖南省','hns',0,'0',112.9822790000,28.1940900000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430100','430100','长沙市','css',1,'430000',112.9822790000,28.1940900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430101','430101','市辖区','sxq',1,'430100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430102','430102','芙蓉区','frq',1,'430100',113.0093210000,28.2073540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430103','430103','天心区','txq',1,'430100',112.9671880000,28.1878260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430104','430104','岳麓区','ylq',1,'430100',112.9333300000,28.1639110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430105','430105','开福区','kfq',1,'430100',112.9822640000,28.2275480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430111','430111','雨花区','yhq',1,'430100',112.9857100000,28.1732300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430121','430121','长沙县','csx',2,'430100',113.0800980000,28.2378880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430122','430122','望城县','wcx',2,'430100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430124','430124','宁乡县','nxx',2,'430100',112.5531820000,28.2539280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430181','430181','浏阳市','lys',2,'430100',113.6333010000,28.1411120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430200','430200','株洲市','zzs',1,'430000',113.1517370000,27.8358060000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430201','430201','市辖区','sxq',1,'430200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430202','430202','荷塘区','htq',1,'430200',113.1625480000,27.8330360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430203','430203','芦淞区','lsq',1,'430200',113.1551690000,27.8272460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430204','430204','石峰区','sfq',1,'430200',113.1129500000,27.8719450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430211','430211','天元区','tyq',1,'430200',113.1362520000,27.8269090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430221','430221','株洲县','zzx',2,'430200',113.1461760000,27.7058440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430223','430223','攸县','yx',2,'430200',113.3457740000,27.0000710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430224','430224','茶陵县','clx',2,'430200',113.5465090000,26.7895340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430225','430225','炎陵县','ylx',2,'430200',113.7768840000,26.4894590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430281','430281','醴陵市','lls',2,'430200',113.5071570000,27.6578730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430300','430300','湘潭市','xts',1,'430000',112.9440520000,27.8297300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430301','430301','市辖区','sxq',1,'430300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430302','430302','雨湖区','yhq',1,'430300',112.9074270000,27.8607700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430304','430304','岳塘区','ytq',1,'430300',112.9277070000,27.8288540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430321','430321','湘潭县','xtx',2,'430300',112.9528290000,27.7786010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430381','430381','湘乡市','xxs',2,'430300',112.5252170000,27.7349180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430382','430382','韶山市','sss',2,'430300',112.5284800000,27.9226820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430400','430400','衡阳市','hys',1,'430000',112.6076930000,26.9003580000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430401','430401','市辖区','sxq',1,'430400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430405','430405','珠晖区','zhq',1,'430400',112.6286720000,26.8916530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430406','430406','雁峰区','yfq',1,'430400',112.6100500000,26.8932420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430407','430407','石鼓区','sgq',1,'430400',112.6062940000,26.9000970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430408','430408','蒸湘区','zxq',1,'430400',112.5924270000,26.8932380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430412','430412','南岳区','nyq',1,'430400',112.7341470000,27.2405360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430421','430421','衡阳县','hyx',2,'430400',112.3796430000,26.9623880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430422','430422','衡南县','hnx',2,'430400',112.6774590000,26.7399730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430423','430423','衡山县','hsx',2,'430400',112.8697100000,27.2348080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430424','430424','衡东县','hdx',2,'430400',112.9504120000,27.0835310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430426','430426','祁东县','qdx',2,'430400',112.1111920000,26.7871090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430481','430481','耒阳市','lys',2,'430400',112.8472150000,26.4141620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430482','430482','常宁市','cns',2,'430400',112.3968210000,26.4067730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430500','430500','邵阳市','sys',1,'430000',111.4692300000,27.2378420000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430501','430501','市辖区','sxq',1,'430500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430502','430502','双清区','sqq',1,'430500',111.4797560000,27.2400010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430503','430503','大祥区','dxq',1,'430500',111.4629680000,27.2335930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430511','430511','北塔区','btq',1,'430500',111.4523150000,27.2456880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430521','430521','邵东县','sdx',2,'430500',111.7431680000,27.2572730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430522','430522','新邵县','xsx',2,'430500',111.4597620000,27.3114290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430523','430523','邵阳县','syx',2,'430500',111.2757000000,26.9897130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430524','430524','隆回县','lhx',2,'430500',111.0387850000,27.1160020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430525','430525','洞口县','dkx',2,'430500',110.5792120000,27.0622860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430527','430527','绥宁县','snx',2,'430500',110.1550750000,26.5806220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430528','430528','新宁县','xnx',2,'430500',110.8591150000,26.4389120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430529','430529','城步苗族自治县','cbmzzzx',2,'430500',110.3132260000,26.3635750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430581','430581','武冈市','wgs',2,'430500',110.6368040000,26.7320860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430600','430600','岳阳市','yys',1,'430000',113.1328550000,29.3702900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430601','430601','市辖区','sxq',1,'430600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430602','430602','岳阳楼区','yylq',1,'430600',113.1207510000,29.3667840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430603','430603','云溪区','yxq',1,'430600',113.2738700000,29.4733950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430611','430611','君山区','jsq',1,'430600',113.0040820000,29.4380620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430621','430621','岳阳县','yyx',2,'430600',113.1160730000,29.1448430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430623','430623','华容县','hrx',2,'430600',112.5593690000,29.5241070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430624','430624','湘阴县','xyx',2,'430600',112.8897480000,28.6774980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430626','430626','平江县','pjx',2,'430600',113.5937510000,28.7015230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430681','430681','汨罗市','mls',2,'430600',113.0794190000,28.8031490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430682','430682','临湘市','lxs',2,'430600',113.4508090000,29.4715940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430700','430700','常德市','cds',1,'430000',111.6913470000,29.0402250000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430701','430701','市辖区','sxq',1,'430700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430702','430702','武陵区','wlq',1,'430700',111.6907180000,29.0404770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430703','430703','鼎城区','dcq',1,'430700',111.6853270000,29.0144260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430721','430721','安乡县','axx',2,'430700',112.1722890000,29.4144830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430722','430722','汉寿县','hsx',2,'430700',111.9685060000,28.9073190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430723','430723','澧县','lx',2,'430700',111.7616820000,29.6426400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430724','430724','临澧县','llx',2,'430700',111.6456020000,29.4432170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430725','430725','桃源县','tyx',2,'430700',111.4845030000,28.9027340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430726','430726','石门县','smx',2,'430700',111.3790870000,29.5847030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430781','430781','津市市','jss',2,'430700',111.8796090000,29.6308670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430800','430800','张家界市','zjjs',1,'430000',110.4799210000,29.1274010000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430801','430801','市辖区','sxq',1,'430800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430802','430802','永定区','ydq',1,'430800',110.4845590000,29.1259610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430811','430811','武陵源区','wlyq',1,'430800',110.5475800000,29.3478270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430821','430821','慈利县','clx',2,'430800',111.1327020000,29.4238760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430822','430822','桑植县','szx',2,'430800',110.1640390000,29.3999390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430900','430900','益阳市','yys',1,'430000',112.3550420000,28.5700660000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430901','430901','市辖区','sxq',1,'430900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430902','430902','资阳区','zyq',1,'430900',112.3308400000,28.5927710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430903','430903','赫山区','hsq',1,'430900',112.3609460000,28.5683270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430921','430921','南县','nx',2,'430900',112.4103990000,29.3721810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430922','430922','桃江县','tjx',2,'430900',112.1397320000,28.5209930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430923','430923','安化县','ahx',2,'430900',111.2218240000,28.3774210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('430981','430981','沅江市','yjs',2,'430900',112.3610880000,28.8397130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431000','431000','郴州市','czs',1,'430000',113.0320670000,25.7935890000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431001','431001','市辖区','sxq',1,'431000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431002','431002','北湖区','bhq',1,'431000',113.0322080000,25.7926280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431003','431003','苏仙区','sxq',1,'431000',113.0386980000,25.7931570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431021','431021','桂阳县','gyx',2,'431000',112.7344660000,25.7374470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431022','431022','宜章县','yzx',2,'431000',112.9478840000,25.3943450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431023','431023','永兴县','yxx',2,'431000',113.1148190000,26.1293920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431024','431024','嘉禾县','jhx',2,'431000',112.3706180000,25.5873090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431025','431025','临武县','lwx',2,'431000',112.5645890000,25.2791190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431026','431026','汝城县','rcx',2,'431000',113.6856860000,25.5537590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431027','431027','桂东县','gdx',2,'431000',113.9458790000,26.0739170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431028','431028','安仁县','arx',2,'431000',113.2721700000,26.7086250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431081','431081','资兴市','zxs',2,'431000',113.2368200000,25.9741520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431100','431100','永州市','yzs',1,'430000',111.6080190000,26.4345160000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431101','431101','市辖区','sxq',1,'431100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431102','431102','芝山区','zsq',1,'431100',111.6263480000,26.2233470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431103','431103','冷水滩区','lstq',1,'431100',111.6071560000,26.4343640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431121','431121','祁阳县','qyx',2,'431100',111.8573400000,26.5859290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431122','431122','东安县','dax',2,'431100',111.3130350000,26.3972780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431123','431123','双牌县','spx',2,'431100',111.6621460000,25.9593970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431124','431124','道县','dx',2,'431100',111.5916140000,25.5184440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431125','431125','江永县','jyx',2,'431100',111.3468030000,25.2681540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431126','431126','宁远县','nyx',2,'431100',111.9445290000,25.5841120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431127','431127','蓝山县','lsx',2,'431100',112.1941950000,25.3752550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431128','431128','新田县','xtx',2,'431100',112.2203410000,25.9069270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431129','431129','江华瑶族自治县','jhyzzzx',2,'431100',111.5772760000,25.1825960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431200','431200','怀化市','hhs',1,'430000',109.9782400000,27.5500820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431201','431201','市辖区','sxq',1,'431200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431202','431202','鹤城区','hcq',1,'431200',109.9822420000,27.5484740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431221','431221','中方县','zfx',2,'431200',109.9480610000,27.4373600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431222','431222','沅陵县','ylx',2,'431200',110.3991610000,28.4555540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431223','431223','辰溪县','cxx',2,'431200',110.1969530000,28.0054740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431224','431224','溆浦县','xpx',2,'431200',110.5933730000,27.9038020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431225','431225','会同县','htx',2,'431200',109.7207850000,26.8707890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431226','431226','麻阳苗族自治县','mymzzzx',2,'431200',109.8028070000,27.8659910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431227','431227','新晃侗族自治县','xhdzzzx',2,'431200',109.1744430000,27.3598970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431228','431228','芷江侗族自治县','zjdzzzx',2,'431200',109.6877770000,27.4379960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431229','431229','靖州苗族侗族自治县','jzmzdzzzx',2,'431200',109.6911590000,26.5735110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431230','431230','通道侗族自治县','tddzzzx',2,'431200',109.7833590000,26.1583490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431281','431281','洪江市','hjs',2,'431200',109.8317650000,27.2018760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431300','431300','娄底市','lds',1,'430000',112.0084970000,27.7281360000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431301','431301','市辖区','sxq',1,'431300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431302','431302','娄星区','lxq',1,'431300',112.0084860000,27.7266430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431321','431321','双峰县','sfx',2,'431300',112.1982450000,27.4591260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431322','431322','新化县','xhx',2,'431300',111.3067470000,27.7374560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431381','431381','冷水江市','lsjs',2,'431300',111.4345730000,27.6845440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('431382','431382','涟源市','lys',2,'431300',111.6708470000,27.6923010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433100','433100','湘西土家族苗族自治州','xxtjzmzzzz',1,'430000',109.7397350000,28.3142960000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433101','433101','吉首市','jss',2,'433100',109.7382730000,28.3148270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433122','433122','泸溪县','lxx',2,'433100',110.2144280000,28.2145160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433123','433123','凤凰县','fhx',2,'433100',109.5991910000,27.9483080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433124','433124','花垣县','hyx',2,'433100',109.4790630000,28.5813520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433125','433125','保靖县','bjx',2,'433100',109.6514450000,28.7096050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433126','433126','古丈县','gzx',2,'433100',109.9495920000,28.6169730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433127','433127','永顺县','ysx',2,'433100',109.8532920000,28.9980680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('433130','433130','龙山县','lsx',2,'433100',109.4411890000,29.4534380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440000','440000','广东省','gds',0,'0',113.2806370000,23.1251780000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440100','440100','广州市','gzs',1,'440000',113.2806370000,23.1251780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440101','440101','市辖区','sxq',1,'440100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440102','440102','东山区','dsq',1,'440100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440103','440103','荔湾区','lwq',1,'440100',113.2515230000,23.1434520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440104','440104','越秀区','yxq',1,'440100',113.2581070000,23.1254560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440105','440105','海珠区','hzq',1,'440100',113.3638150000,23.0817720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440106','440106','天河区','thq',1,'440100',113.3283890000,23.1212040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440107','440107','芳村区','fcq',1,'440100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440111','440111','白云区','byq',1,'440100',113.3637610000,23.3627370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440112','440112','黄埔区','hpq',1,'440100',113.4290710000,23.0990400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440113','440113','番禺区','fyq',1,'440100',113.4530890000,22.8502410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440114','440114','花都区','hdq',1,'440100',113.2223740000,23.3610600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440183','440183','增城市','zcs',2,'440100',113.7000750000,23.2741480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440184','440184','从化市','chs',2,'440100',113.6519840000,23.6406150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440200','440200','韶关市','sgs',1,'440000',113.5915440000,24.8013220000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440201','440201','市辖区','sxq',1,'440200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440203','440203','武江区','wjq',1,'440200',113.5882890000,24.8001600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440204','440204','浈江区','zjq',1,'440200',113.5992240000,24.8039770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440205','440205','曲江区','qjq',1,'440200',113.6055820000,24.6801950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440222','440222','始兴县','sxx',2,'440200',114.0672050000,24.9483640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440224','440224','仁化县','rhx',2,'440200',113.7486270000,25.0882260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440229','440229','翁源县','wyx',2,'440200',114.1312890000,24.3538870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440232','440232','乳源瑶族自治县','ryyzzzx',2,'440200',113.2784170000,24.7761090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440233','440233','新丰县','xfx',2,'440200',114.2070340000,24.0554120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440281','440281','乐昌市','lcs',2,'440200',113.3524130000,25.1284450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440282','440282','南雄市','nxs',2,'440200',114.3112310000,25.1153280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440300','440300','深圳市','szs',1,'440000',114.0859470000,22.5470000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440301','440301','市辖区','sxq',1,'440300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440303','440303','罗湖区','lhq',1,'440300',114.1373190000,22.5785960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440304','440304','福田区','ftq',1,'440300',114.0267860000,22.5253160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440305','440305','南山区','nsq',1,'440300',113.9548270000,22.5429850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440306','440306','宝安区','baq',1,'440300',113.8622790000,22.6151920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440307','440307','龙岗区','lgq',1,'440300',114.4229780000,22.6314890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440308','440308','盐田区','ytq',1,'440300',114.2328870000,22.5537820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440400','440400','珠海市','zhs',1,'440000',113.5539860000,22.2249790000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440401','440401','市辖区','sxq',1,'440400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440402','440402','香洲区','xzq',1,'440400',113.5495890000,22.2316250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440403','440403','斗门区','dmq',1,'440400',113.2977540000,22.2064770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440404','440404','金湾区','jwq',1,'440400',113.3562630000,22.0500090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440500','440500','汕头市','sts',1,'440000',116.7084630000,23.3710200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440501','440501','市辖区','sxq',1,'440500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440507','440507','龙湖区','lhq',1,'440500',116.7320150000,23.3737540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440511','440511','金平区','jpq',1,'440500',116.7035830000,23.3670710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440512','440512','濠江区','hjq',1,'440500',116.7295280000,23.2793450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440513','440513','潮阳区','cyq',1,'440500',116.6026020000,23.2623360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440514','440514','潮南区','cnq',1,'440500',116.4236070000,23.2497980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440515','440515','澄海区','chq',1,'440500',116.7633600000,23.4684400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440523','440523','南澳县','nax',2,'440500',117.0271050000,23.4195620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440600','440600','佛山市','fss',1,'440000',113.1227170000,23.0287620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440601','440601','市辖区','sxq',1,'440600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440604','440604','禅城区','ccq',1,'440600',113.0196360000,22.9720970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440605','440605','南海区','nhq',1,'440600',113.1019130000,23.1460120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440606','440606','顺德区','sdq',1,'440600',113.2396300000,22.7646010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440607','440607','三水区','ssq',1,'440600',112.8466750000,23.0602550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440608','440608','高明区','gmq',1,'440600',112.7891320000,22.8664270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440700','440700','江门市','jms',1,'440000',113.0949420000,22.5904310000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440701','440701','市辖区','sxq',1,'440700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440703','440703','蓬江区','pjq',1,'440700',113.0785900000,22.5967700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440704','440704','江海区','jhq',1,'440700',113.1206010000,22.5722110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440705','440705','新会区','xhq',1,'440700',113.0385840000,22.5202470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440781','440781','台山市','tss',2,'440700',112.7934140000,22.2507130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440783','440783','开平市','kps',2,'440700',112.6922620000,22.3662860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440784','440784','鹤山市','hss',2,'440700',112.9617950000,22.7681040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440785','440785','恩平市','eps',2,'440700',112.3140510000,22.1829560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440800','440800','湛江市','zjs',1,'440000',110.3649770000,21.2748980000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440801','440801','市辖区','sxq',1,'440800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440802','440802','赤坎区','ckq',1,'440800',110.3616340000,21.2733650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440803','440803','霞山区','xsq',1,'440800',110.4063820000,21.1942290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440804','440804','坡头区','ptq',1,'440800',110.4556320000,21.2444100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440811','440811','麻章区','mzq',1,'440800',110.3291670000,21.2659970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440823','440823','遂溪县','sxx',2,'440800',110.2553210000,21.3769150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440825','440825','徐闻县','xwx',2,'440800',110.1757180000,20.3260830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440881','440881','廉江市','ljs',2,'440800',110.2849610000,21.6112810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440882','440882','雷州市','lzs',2,'440800',110.0882750000,20.9085230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440883','440883','吴川市','wcs',2,'440800',110.7805080000,21.4284530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440900','440900','茂名市','mms',1,'440000',110.9192290000,21.6597510000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440901','440901','市辖区','sxq',1,'440900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440902','440902','茂南区','mnq',1,'440900',110.9205420000,21.6604250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440903','440903','茂港区','mgq',1,'440900',111.0072640000,21.5072190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440923','440923','电白县','dbx',2,'440900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440981','440981','高州市','gzs',2,'440900',110.8532510000,21.9151530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440982','440982','化州市','hzs',2,'440900',110.6383900000,21.6549530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('440983','440983','信宜市','xys',2,'440900',110.9416560000,22.3526810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441200','441200','肇庆市','zqs',1,'440000',112.4725290000,23.0515460000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441201','441201','市辖区','sxq',1,'441200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441202','441202','端州区','dzq',1,'441200',112.4723290000,23.0526620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441203','441203','鼎湖区','dhq',1,'441200',112.5652490000,23.1558220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441223','441223','广宁县','gnx',2,'441200',112.4404190000,23.6314860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441224','441224','怀集县','hjx',2,'441200',112.1824660000,23.9130720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441225','441225','封开县','fkx',2,'441200',111.5029730000,23.4347310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441226','441226','德庆县','dqx',2,'441200',111.7815600000,23.1417110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441283','441283','高要市','gys',2,'441200',112.4608460000,23.0276940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441284','441284','四会市','shs',2,'441200',112.6950280000,23.3403240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441300','441300','惠州市','hzs',1,'440000',114.4125990000,23.0794040000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441301','441301','市辖区','sxq',1,'441300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441302','441302','惠城区','hcq',1,'441300',114.4179710000,23.1174140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441303','441303','惠阳区','hyq',1,'441300',114.4694440000,22.7885100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441322','441322','博罗县','blx',2,'441300',113.8639460000,23.1272400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441323','441323','惠东县','hdx',2,'441300',114.7230920000,22.9830360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441324','441324','龙门县','lmx',2,'441300',114.2599860000,23.7238940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441400','441400','梅州市','mzs',1,'440000',116.1175820000,24.2991120000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441401','441401','市辖区','sxq',1,'441400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441402','441402','梅江区','mjq',1,'441400',116.1211600000,24.3025930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441421','441421','梅县','mx',2,'441400',116.0834820000,24.2678250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441422','441422','大埔县','dbx',2,'441400',116.6955200000,24.3515870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441423','441423','丰顺县','fsx',2,'441400',116.1844190000,23.7527710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441424','441424','五华县','whx',2,'441400',115.7750040000,23.9254240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441426','441426','平远县','pyx',2,'441400',115.8917290000,24.5696510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441427','441427','蕉岭县','jlx',2,'441400',116.1705310000,24.6533130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441481','441481','兴宁市','xns',2,'441400',115.7316480000,24.1380770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441500','441500','汕尾市','sws',1,'440000',115.3642380000,22.7744850000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441501','441501','市辖区','sxq',1,'441500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441502','441502','城区','cq',1,'441500',115.3694310000,22.7691120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441521','441521','海丰县','hfx',2,'441500',115.3512620000,22.9763010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441523','441523','陆河县','lhx',2,'441500',115.6575650000,23.3026820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441581','441581','陆丰市','lfs',2,'441500',115.6408980000,22.9457010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441600','441600','河源市','hys',1,'440000',114.6978020000,23.7462660000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441601','441601','市辖区','sxq',1,'441600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441602','441602','源城区','ycq',1,'441600',114.6968280000,23.7462550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441621','441621','紫金县','zjx',2,'441600',115.1843830000,23.6337440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441622','441622','龙川县','lcx',2,'441600',115.2564150000,24.1011740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441623','441623','连平县','lpx',2,'441600',114.4959520000,24.3642270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441624','441624','和平县','hpx',2,'441600',114.9414730000,24.4431800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441625','441625','东源县','dyx',2,'441600',114.7427110000,23.7890930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441700','441700','阳江市','yjs',1,'440000',111.9751070000,21.8592220000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441701','441701','市辖区','sxq',1,'441700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441702','441702','江城区','jcq',1,'441700',111.9689090000,21.8591820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441721','441721','阳西县','yxx',2,'441700',111.6175560000,21.7536700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441723','441723','阳东县','ydx',2,'441700',112.0112670000,21.8647280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441781','441781','阳春市','ycs',2,'441700',111.7905000000,22.1695980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441800','441800','清远市','qys',1,'440000',113.0512270000,23.6850220000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441801','441801','市辖区','sxq',1,'441800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441802','441802','清城区','qcq',1,'441800',113.0486980000,23.6889760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441821','441821','佛冈县','fgx',2,'441800',113.5340940000,23.8667390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441823','441823','阳山县','ysx',2,'441800',112.6340190000,24.4702860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441825','441825','连山壮族瑶族自治县','lszzyzzzx',2,'441800',112.0865550000,24.5672710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441826','441826','连南瑶族自治县','lnyzzzx',2,'441800',112.2908080000,24.7190970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441827','441827','清新县','qxx',2,'441800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441881','441881','英德市','yds',2,'441800',113.4054040000,24.1861200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441882','441882','连州市','lzs',2,'441800',112.3792710000,24.7839660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('441900','441900','东莞市','dgs',1,'440000',113.7409780000,23.0093990000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('442000','442000','中山市','zss',1,'440000',113.3826180000,22.5429190000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445100','445100','潮州市','czs',1,'440000',116.6323010000,23.6617010000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445101','445101','市辖区','sxq',1,'445100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445102','445102','湘桥区','xqq',1,'445100',116.6336500000,23.6646750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445121','445121','潮安县','cax',2,'445100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445122','445122','饶平县','rpx',2,'445100',117.0020500000,23.6681710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445200','445200','揭阳市','jys',1,'440000',116.3557330000,23.5437780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445201','445201','市辖区','sxq',1,'445200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445202','445202','榕城区','rcq',1,'445200',116.3570450000,23.5355240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445221','445221','揭东县','jdx',2,'445200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445222','445222','揭西县','jxx',2,'445200',115.8387080000,23.4273000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445224','445224','惠来县','hlx',2,'445200',116.2958320000,23.0298340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445281','445281','普宁市','pns',2,'445200',116.1650820000,23.2978800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445300','445300','云浮市','yfs',1,'440000',112.0444390000,22.9298010000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445301','445301','市辖区','sxq',1,'445300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445302','445302','云城区','ycq',1,'445300',112.0447100000,22.9308270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445321','445321','新兴县','xxx',2,'445300',112.2308300000,22.7032040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445322','445322','郁南县','ynx',2,'445300',111.5359210000,23.2377090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445323','445323','云安县','yax',2,'445300',112.0056090000,23.0731520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('445381','445381','罗定市','lds',2,'445300',111.5782010000,22.7654150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450000','450000','广西','gxzzzzq',0,'0',108.3200040000,22.8240200000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450100','450100','南宁市','nns',1,'450000',108.3200040000,22.8240200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450101','450101','市辖区','sxq',1,'450100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450102','450102','兴宁区','xnq',1,'450100',108.3471950000,22.8370300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450103','450103','青秀区','qxq',1,'450100',108.3235520000,22.8087360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450105','450105','江南区','jnq',1,'450100',108.2747570000,22.8072670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450107','450107','西乡塘区','xxtq',1,'450100',108.3282090000,22.8476840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450108','450108','良庆区','lqq',1,'450100',108.3242990000,22.7558530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450109','450109','邕宁区','ynq',1,'450100',108.4921910000,22.7518700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450122','450122','武鸣县','wmx',2,'450100',108.2807170000,23.1571630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450123','450123','隆安县','lax',2,'450100',107.6886610000,23.1747630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450124','450124','马山县','msx',2,'450100',108.1729030000,23.7117580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450125','450125','上林县','slx',2,'450100',108.6039370000,23.4317690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450126','450126','宾阳县','byx',2,'450100',108.8167350000,23.2168840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450127','450127','横县','hx',2,'450100',109.2709870000,22.6874300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450200','450200','柳州市','lzs',1,'450000',109.4117030000,24.3146170000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450201','450201','市辖区','sxq',1,'450200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450202','450202','城中区','czq',1,'450200',109.4117490000,24.3123240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450203','450203','鱼峰区','yfq',1,'450200',109.4153640000,24.3038480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450204','450204','柳南区','lnq',1,'450200',109.3959360000,24.2870130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450205','450205','柳北区','lbq',1,'450200',109.4065770000,24.3591450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450221','450221','柳江县','ljx',2,'450200',109.3345030000,24.2575120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450222','450222','柳城县','lcx',2,'450200',109.2458120000,24.6551210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450223','450223','鹿寨县','lzx',2,'450200',109.7408050000,24.4834050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450224','450224','融安县','rax',2,'450200',109.4036210000,25.2147030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450225','450225','融水苗族自治县','rsmzzzx',2,'450200',109.2527440000,25.0688120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450226','450226','三江侗族自治县','sjdzzzx',2,'450200',109.6148460000,25.7855300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450300','450300','桂林市','gls',1,'450000',110.2991210000,25.2742150000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450301','450301','市辖区','sxq',1,'450300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450302','450302','秀峰区','xfq',1,'450300',110.2924450000,25.2785440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450303','450303','叠彩区','dcq',1,'450300',110.3007830000,25.3013340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450304','450304','象山区','xsq',1,'450300',110.2848820000,25.2619860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450305','450305','七星区','qxq',1,'450300',110.3175770000,25.2543390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450311','450311','雁山区','ysq',1,'450300',110.3056670000,25.0776460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450321','450321','阳朔县','ysx',2,'450300',110.4946990000,24.7753400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450322','450322','临桂县','lgx',2,'450300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450323','450323','灵川县','lcx',2,'450300',110.3257120000,25.4085410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450324','450324','全州县','qzx',2,'450300',111.0729890000,25.9298970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450325','450325','兴安县','xax',2,'450300',110.6707830000,25.6095540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450326','450326','永福县','yfx',2,'450300',109.9892080000,24.9866920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450327','450327','灌阳县','gyx',2,'450300',111.1602480000,25.4890980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450328','450328','龙胜各族自治县','lsgzzzx',2,'450300',110.0094230000,25.7964280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450329','450329','资源县','zyx',2,'450300',110.6425870000,26.0342000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450330','450330','平乐县','plx',2,'450300',110.6428210000,24.6322160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450331','450331','荔蒲县','lpx',2,'450300',110.4001490000,24.4977860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450332','450332','恭城瑶族自治县','gcyzzzx',2,'450300',110.8295200000,24.8336120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450400','450400','梧州市','wzs',1,'450000',111.2976040000,23.4748030000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450401','450401','市辖区','sxq',1,'450400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450403','450403','万秀区','wxq',1,'450400',111.3158170000,23.4713180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450404','450404','蝶山区','dsq',1,'450400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450405','450405','长洲区','czq',1,'450400',111.2756780000,23.4777000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450421','450421','苍梧县','cwx',2,'450400',111.5440080000,23.8450970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450422','450422','藤县','tx',2,'450400',110.9318260000,23.3739630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450423','450423','蒙山县','msx',2,'450400',110.5226000000,24.1998290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450481','450481','岑溪市','cxs',2,'450400',110.9981140000,22.9184060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450500','450500','北海市','bhs',1,'450000',109.1192540000,21.4733430000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450501','450501','市辖区','sxq',1,'450500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450502','450502','海城区','hcq',1,'450500',109.1075290000,21.4684430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450503','450503','银海区','yhq',1,'450500',109.1187070000,21.4449090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450512','450512','铁山港区','tsgq',1,'450500',109.4505730000,21.5928000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450521','450521','合浦县','hpx',2,'450500',109.2006950000,21.6635540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450600','450600','防城港市','fcgs',1,'450000',108.3454780000,21.6146310000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450601','450601','市辖区','sxq',1,'450600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450602','450602','港口区','gkq',1,'450600',108.3462810000,21.6144060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450603','450603','防城区','fcq',1,'450600',108.3584260000,21.7647580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450621','450621','上思县','ssx',2,'450600',107.9821390000,22.1514230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450681','450681','东兴市','dxs',2,'450600',107.9701700000,21.5411720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450700','450700','钦州市','qzs',1,'450000',108.6241750000,21.9671270000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450701','450701','市辖区','sxq',1,'450700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450702','450702','钦南区','qnq',1,'450700',108.6266290000,21.9668080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450703','450703','钦北区','qbq',1,'450700',108.4491100000,22.1327610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450721','450721','灵山县','lsx',2,'450700',109.2934680000,22.4180410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450722','450722','浦北县','pbx',2,'450700',109.5563410000,22.2683350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450800','450800','贵港市','ggs',1,'450000',109.6021460000,23.0936000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450801','450801','市辖区','sxq',1,'450800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450802','450802','港北区','gbq',1,'450800',109.5948100000,23.1076770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450803','450803','港南区','gnq',1,'450800',109.6046650000,23.0675160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450804','450804','覃塘区','ttq',1,'450800',109.4156970000,23.1328150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450821','450821','平南县','pnx',2,'450800',110.3974850000,23.5445460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450881','450881','桂平市','gps',2,'450800',110.0746680000,23.3824730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450900','450900','玉林市','yls',1,'450000',110.1543930000,22.6313600000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450901','450901','市辖区','sxq',1,'450900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450902','450902','玉州区','yzq',1,'450900',110.1549120000,22.6321320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450921','450921','容县','rx',2,'450900',110.5524670000,22.8564350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450922','450922','陆川县','lcx',2,'450900',110.2648420000,22.3210540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450923','450923','博白县','bbx',2,'450900',109.9800040000,22.2712850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450924','450924','兴业县','xyx',2,'450900',109.8777680000,22.7418700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('450981','450981','北流市','bls',2,'450900',110.3480520000,22.7016480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451000','451000','百色市','bss',1,'450000',106.6162850000,23.8977420000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451001','451001','市辖区','sxq',1,'451000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451002','451002','右江区','yjq',1,'451000',106.6157270000,23.8976750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451021','451021','田阳县','tyx',2,'451000',106.9043150000,23.7360790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451022','451022','田东县','tdx',2,'451000',107.1242600000,23.6004440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451023','451023','平果县','pgx',2,'451000',107.5804030000,23.3204790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451024','451024','德保县','dbx',2,'451000',106.6181640000,23.3214640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451025','451025','靖西县','jxx',2,'451000',106.4175490000,23.1347660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451026','451026','那坡县','npx',2,'451000',105.8335530000,23.4007850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451027','451027','凌云县','lyx',2,'451000',106.5648700000,24.3456430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451028','451028','乐业县','lyx',2,'451000',106.5596380000,24.7822040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451029','451029','田林县','tlx',2,'451000',106.2350470000,24.2902620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451030','451030','西林县','xlx',2,'451000',105.0950250000,24.4920410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451031','451031','隆林各族自治县','llgzzzx',2,'451000',105.3423630000,24.7743180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451100','451100','贺州市','hzs',1,'450000',111.5520560000,24.4141410000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451101','451101','市辖区','sxq',1,'451100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451102','451102','八步区','bbq',1,'451100',111.5519910000,24.4124460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451121','451121','昭平县','zpx',2,'451100',110.8108650000,24.1729580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451122','451122','钟山县','zsx',2,'451100',111.3036290000,24.5285660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451123','451123','富川瑶族自治县','fcyzzzx',2,'451100',111.2772280000,24.8189600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451200','451200','河池市','hcs',1,'450000',108.0621050000,24.6958990000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451201','451201','市辖区','sxq',1,'451200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451202','451202','金城江区','jcjq',1,'451200',108.0621310000,24.6956250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451221','451221','南丹县','ndx',2,'451200',107.5466050000,24.9831920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451222','451222','天峨县','tex',2,'451200',107.1749390000,24.9859640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451223','451223','凤山县','fsx',2,'451200',107.0445920000,24.5445610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451224','451224','东兰县','dlx',2,'451200',107.3736960000,24.5093670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451225','451225','罗城仫佬族自治县','lcmlzzzx',2,'451200',108.9024530000,24.7793270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451226','451226','环江毛南族自治县','hjmnzzzx',2,'451200',108.2586690000,24.8276280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451227','451227','巴马瑶族自治县','bmyzzzx',2,'451200',107.2531260000,24.1395380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451228','451228','都安瑶族自治县','dayzzzx',2,'451200',108.1027610000,23.9349640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451229','451229','大化瑶族自治县','dhyzzzx',2,'451200',107.9945000000,23.7395960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451281','451281','宜州市','yzs',2,'451200',108.6539650000,24.4921930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451300','451300','来宾市','lbs',1,'450000',109.2297720000,23.7337660000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451301','451301','市辖区','sxq',1,'451300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451302','451302','兴宾区','xbq',1,'451300',109.2305410000,23.7329260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451321','451321','忻城县','xcx',2,'451300',108.6673610000,24.0647790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451322','451322','象州县','xzx',2,'451300',109.6845550000,23.9598240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451323','451323','武宣县','wxx',2,'451300',109.6628700000,23.6041620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451324','451324','金秀瑶族自治县','jxyzzzx',2,'451300',110.1885560000,24.1349410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451381','451381','合山市','hss',2,'451300',108.8885800000,23.8131100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451400','451400','崇左市','czs',1,'450000',107.3539260000,22.4041080000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451401','451401','市辖区','sxq',1,'451400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451402','451402','江洲区','jzq',1,'451400',107.3544430000,22.4046900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451421','451421','扶绥县','fsx',2,'451400',107.9115330000,22.6358210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451422','451422','宁明县','nmx',2,'451400',107.0676160000,22.1313530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451423','451423','龙州县','lzx',2,'451400',106.8575020000,22.3437160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451424','451424','大新县','dxx',2,'451400',107.2008030000,22.8333690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451425','451425','天等县','tdx',2,'451400',107.1424410000,23.0824840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('451481','451481','凭祥市','pxs',2,'451400',106.7590380000,22.1088820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460000','460000','海南省','hns',0,'0',110.3311900000,20.0319710000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460100','460100','海口市','hks',1,'460000',110.3311900000,20.0319710000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460101','460101','市辖区','sxq',1,'460100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460105','460105','秀英区','xyq',1,'460100',110.2823930000,20.0081450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460106','460106','龙华区','lhq',1,'460100',110.3255790000,20.0119940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460107','460107','琼山区','qsq',1,'460100',110.3545360000,20.0072350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460108','460108','美兰区','mlq',1,'460100',110.3494700000,20.0235460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460200','460200','三亚市','sys',1,'460000',109.4611550000,18.2769780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201','460201','市辖区','sxq',1,'460200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201100','460201100','海棠湾镇','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201101','460201101','吉阳镇','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201102','460201102','凤凰镇','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201103','460201103','崖城镇','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201104','460201104','天涯镇','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201105','460201105','育才镇','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201400','460201400','国营南田农场','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201403','460201403','国营立才农场','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201404','460201404','国营南滨农场','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201451','460201451','河西区街道','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460201452','460201452','河东区街道','hks',1,'460200',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460300','460300','三沙市','hks',1,'460000',110.3311900000,20.0319710000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460321','460321','西沙群岛','hks',1,'460300',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460322','460322','南沙群岛','hks',1,'460300',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('460323','460323','中沙群岛的岛礁及其海域','hks',1,'460300',110.3311900000,20.0319710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469000','469000','省直辖县级行政单位','szxxjxzdw',1,'460000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469001','469001','五指山市','wzss',2,'469000',109.5166620000,18.7769210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469002','469002','琼海市','qhs',2,'469000',110.4667850000,19.2460110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469003','469003','儋州市','dzs',2,'469000',109.2011570000,19.7364610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469005','469005','文昌市','wcs',2,'469000',110.7539750000,19.6129860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469006','469006','万宁市','wns',2,'469000',110.3887930000,18.7962160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469007','469007','东方市','dfs',2,'469000',108.6537890000,19.1019800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469025','469025','定安县','dax',2,'469000',109.4526060000,19.2245840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469026','469026','屯昌县','tcx',2,'469000',109.0533510000,19.2609680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469027','469027','澄迈县','cmx',2,'469000',109.1754440000,18.7475800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469028','469028','临高县','lgx',2,'469000',110.0372180000,18.5050060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469030','469030','白沙黎族自治县','bslzzzx',2,'469000',109.8399960000,19.0355700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469031','469031','昌江黎族自治县','cjlzzzx',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469033','469033','乐东黎族自治县','ldlzzzx',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469034','469034','陵水黎族自治县','lslzzzx',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469035','469035','保亭黎族苗族自治县','btlzmzzzx',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469036','469036','琼中黎族苗族自治县','qzlzmzzzx',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469037','469037','西沙群岛','xsqd',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469038','469038','南沙群岛','nsqd',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('469039','469039','中沙群岛的岛礁及其海域','zsqdddjjqhy',2,'469000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500000','500000','重庆市','cqs',0,'0',106.5049620000,29.5331550000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500100','500100','市辖区','sxq',1,'500000',106.5049620000,29.5331550000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500101','500101','万州区','wzq',1,'500100',108.4826500000,30.7702880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500102','500102','涪陵区','flq',1,'500100',107.4186650000,29.6987800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500103','500103','渝中区','yzq',1,'500100',106.5361010000,29.5482490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500104','500104','大渡口区','ddkq',1,'500100',106.4931780000,29.4884710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500105','500105','江北区','jbq',1,'500100',106.5361140000,29.5814120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500106','500106','沙坪坝区','spbq',1,'500100',106.4599100000,29.5580800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500107','500107','九龙坡区','jlpq',1,'500100',106.4859020000,29.4942000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500108','500108','南岸区','naq',1,'500100',106.5565660000,29.5231820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500109','500109','北碚区','bbq',1,'500100',106.3697070000,29.7625850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500110','500110','万盛区','wsq',1,'500100',106.9288160000,28.9513380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500111','500111','双桥区','sqq',1,'500100',105.7671370000,29.4847050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500112','500112','渝北区','ybq',1,'500100',106.4794260000,29.6267360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500113','500113','巴南区','bnq',1,'500100',106.5193730000,29.3825480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500114','500114','黔江区','qjq',1,'500100',108.7739640000,29.3000360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500115','500115','长寿区','csq',1,'500100',107.0685810000,29.8073820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500200','500200','县','x',1,'500000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500222','500222','綦江县','qjx',2,'500200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500223','500223','潼南县','tnx',2,'500200',105.8418180000,30.1895540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500224','500224','铜梁县','tlx',2,'500200',106.0549480000,29.8399440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500225','500225','大足县','dzx',2,'500200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500226','500226','荣昌县','rcx',2,'500200',105.5940610000,29.4036270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500227','500227','璧山县','bsx',2,'500200',106.2311260000,29.5935810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500228','500228','梁平县','lpx',2,'500200',107.8000340000,30.6721680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500229','500229','城口县','ckx',2,'500200',108.6649000000,31.9462930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500230','500230','丰都县','fdx',2,'500200',107.7324800000,29.8664240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500231','500231','垫江县','djx',2,'500200',107.3486920000,30.3300120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500232','500232','武隆县','wlx',2,'500200',107.7565500000,29.3237600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500233','500233','忠县','zx',2,'500200',108.0375180000,30.2915370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500234','500234','开县','kx',2,'500200',108.4133170000,31.1677350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500235','500235','云阳县','yyx',2,'500200',108.6976980000,30.9305290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500236','500236','奉节县','fjx',2,'500200',109.4657740000,31.0199670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500237','500237','巫山县','wsx',2,'500200',109.8789280000,31.0748430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500238','500238','巫溪县','wxx',2,'500200',109.6289120000,31.3966000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500240','500240','石柱土家族自治县','sztjzzzx',2,'500200',108.1124480000,29.9985300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500241','500241','秀山土家族苗族自治县','xstjzmzzzx',2,'500200',108.9960430000,28.4447720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500242','500242','酉阳土家族苗族自治县','yytjzmzzzx',2,'500200',108.7672010000,28.8398280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500243','500243','彭水苗族土家族自治县','psmztjzzzx',2,'500200',108.1665510000,29.2938560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500300','500300','市','s',1,'500000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500381','500381','江津市','jjs',2,'500300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500382','500382','合川市','hcs',2,'500300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500383','500383','永川市','ycs',2,'500300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('500384','500384','南川市','ncs',2,'500300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510000','510000','四川省','scs',0,'0',104.0657350000,30.6594620000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510100','510100','成都市','cds',1,'510000',104.0657350000,30.6594620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510101','510101','市辖区','sxq',1,'510100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510104','510104','锦江区','jjq',1,'510100',104.0793990000,30.6509800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510105','510105','青羊区','qyq',1,'510100',103.9948400000,30.6628780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510106','510106','金牛区','jnq',1,'510100',104.1135190000,30.7585650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510107','510107','武侯区','whq',1,'510100',104.0896970000,30.6390070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510108','510108','成华区','chq',1,'510100',104.1146390000,30.6533960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510112','510112','龙泉驿区','lqyq',1,'510100',104.3080340000,30.6027950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510113','510113','青白江区','qbjq',1,'510100',104.2491190000,30.8865290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510114','510114','新都区','xdq',1,'510100',104.0139730000,30.8737160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510115','510115','温江区','wjq',1,'510100',103.8710160000,30.6717600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510121','510121','金堂县','jtx',2,'510100',104.7315950000,30.6268700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510122','510122','双流县','slx',2,'510100',104.1471060000,30.5294030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510124','510124','郫县','px',2,'510100',103.7485200000,30.8612310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510129','510129','大邑县','dyx',2,'510100',103.4309200000,30.5322740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510131','510131','蒲江县','pjx',2,'510100',103.5095670000,30.2997370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510132','510132','新津县','xjx',2,'510100',103.7835950000,30.4095780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510181','510181','都江堰市','djys',2,'510100',103.6078510000,30.9702560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510182','510182','彭州市','pzs',2,'510100',103.9420800000,30.9817770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510183','510183','邛崃市','qls',2,'510100',103.7047190000,30.3952270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510184','510184','崇州市','czs',2,'510100',103.5293970000,30.7861910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510300','510300','自贡市','zgs',1,'510000',104.7734470000,29.3527650000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510301','510301','市辖区','sxq',1,'510300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510302','510302','自流井区','zljq',1,'510300',104.7781880000,29.3432310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510303','510303','贡井区','gjq',1,'510300',104.7143720000,29.3456750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510304','510304','大安区','daq',1,'510300',104.7818260000,29.3602280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510311','510311','沿滩区','ytq',1,'510300',104.8764170000,29.2725210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510321','510321','荣县','rx',2,'510300',104.4239320000,29.4548510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510322','510322','富顺县','fsx',2,'510300',104.9842560000,29.1812820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510400','510400','攀枝花市','pzhs',1,'510000',101.7160070000,26.5804460000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510401','510401','市辖区','sxq',1,'510400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510402','510402','东区','dq',1,'510400',101.7151340000,26.5808870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510403','510403','西区','xq',1,'510400',101.6379690000,26.5967760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510411','510411','仁和区','rhq',1,'510400',101.7379160000,26.4971850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510421','510421','米易县','myx',2,'510400',102.1098770000,26.8874740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510422','510422','盐边县','ybx',2,'510400',101.8518480000,26.6776190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510500','510500','泸州市','lzs',1,'510000',105.4433480000,28.8891380000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510501','510501','市辖区','sxq',1,'510500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510502','510502','江阳区','jyq',1,'510500',105.4451310000,28.8828890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510503','510503','纳溪区','nxq',1,'510500',105.3772100000,28.7763100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510504','510504','龙马潭区','lmtq',1,'510500',105.4352280000,28.8975720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510521','510521','泸县','lx',2,'510500',105.3763350000,29.1512880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510522','510522','合江县','hjx',2,'510500',105.8340980000,28.8103250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510524','510524','叙永县','xyx',2,'510500',105.4377750000,28.1679190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510525','510525','古蔺县','glx',2,'510500',105.8133590000,28.0394800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510600','510600','德阳市','dys',1,'510000',104.3986510000,31.1279910000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510601','510601','市辖区','sxq',1,'510600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510603','510603','旌阳区','jyq',1,'510600',104.3832000000,31.0917330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510623','510623','中江县','zjx',2,'510600',104.6801740000,31.0437690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510626','510626','罗江县','ljx',2,'510600',104.5071260000,31.3032810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510681','510681','广汉市','ghs',2,'510600',104.2819030000,30.9771500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510682','510682','什邡市','sfs',2,'510600',104.1736530000,31.1268810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510683','510683','绵竹市','mzs',2,'510600',104.2001620000,31.3430840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510700','510700','绵阳市','mys',1,'510000',104.7417220000,31.4640200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510701','510701','市辖区','sxq',1,'510700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510703','510703','涪城区','fcq',1,'510700',104.7409710000,31.4635570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510704','510704','游仙区','yxq',1,'510700',104.7700060000,31.4847720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510722','510722','三台县','stx',2,'510700',105.0903160000,31.0909090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510723','510723','盐亭县','ytx',2,'510700',105.3919910000,31.2231800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510724','510724','安县','ax',2,'510700',104.5603410000,31.5389400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510725','510725','梓潼县','ztx',2,'510700',105.1635300000,31.6352250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510726','510726','北川羌族自治县','bcqzzzx',2,'510700',104.4680690000,31.6158630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510727','510727','平武县','pwx',2,'510700',104.5305550000,32.4075880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510781','510781','江油市','jys',2,'510700',104.7444310000,31.7763860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510800','510800','广元市','gys',1,'510000',105.8297570000,32.4336680000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510801','510801','市辖区','sxq',1,'510800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510802','510802','市中区','szq',1,'510800',105.8261940000,32.4322760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510811','510811','元坝区','ybq',1,'510800',105.9641210000,32.3227880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510812','510812','朝天区','ctq',1,'510800',105.8891700000,32.6426320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510821','510821','旺苍县','wcx',2,'510800',106.2904260000,32.2283300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510822','510822','青川县','qcx',2,'510800',105.2388470000,32.5856550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510823','510823','剑阁县','jgx',2,'510800',105.5270350000,32.2865170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510824','510824','苍溪县','cxx',2,'510800',105.9397060000,31.7322510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510900','510900','遂宁市','sns',1,'510000',105.5713310000,30.5133110000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510901','510901','市辖区','sxq',1,'510900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510903','510903','船山区','csq',1,'510900',105.5822150000,30.5026470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510904','510904','安居区','ajq',1,'510900',105.4593830000,30.3461210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510921','510921','蓬溪县','pxx',2,'510900',105.7136990000,30.7748830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510922','510922','射洪县','shx',2,'510900',105.3818490000,30.8687520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('510923','510923','大英县','dyx',2,'510900',105.2521870000,30.5815710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511000','511000','内江市','njs',1,'510000',105.0661380000,29.5870800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511001','511001','市辖区','sxq',1,'511000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511002','511002','市中区','szq',1,'511000',105.0654670000,29.5852650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511011','511011','东兴区','dxq',1,'511000',105.0672030000,29.6001070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511024','511024','威远县','wyx',2,'511000',104.6683270000,29.5268600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511025','511025','资中县','zzx',2,'511000',104.8524630000,29.7752950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511028','511028','隆昌县','lcx',2,'511000',105.2880740000,29.3381620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511100','511100','乐山市','lss',1,'510000',103.7612630000,29.5820240000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511101','511101','市辖区','sxq',1,'511100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511102','511102','市中区','szq',1,'511100',103.7494600000,29.6071760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511111','511111','沙湾区','swq',1,'511100',103.5499610000,29.4165360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511112','511112','五通桥区','wtqq',1,'511100',103.8168370000,29.4061860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511113','511113','金口河区','jkhq',1,'511100',103.0778310000,29.2460200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511123','511123','犍为县','jwx',2,'511100',103.9442660000,29.2097820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511124','511124','井研县','jyx',2,'511100',104.0688500000,29.6516450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511126','511126','夹江县','jjx',2,'511100',103.5788620000,29.7410190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511129','511129','沐川县','mcx',2,'511100',103.9021100000,28.9563380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511132','511132','峨边彝族自治县','ebyzzzx',2,'511100',103.2621480000,29.2302710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511133','511133','马边彝族自治县','mbyzzzx',2,'511100',103.5468510000,28.8389330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511181','511181','峨眉山市','emss',2,'511100',103.4924880000,29.5974780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511300','511300','南充市','ncs',1,'510000',106.0829740000,30.7952810000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511301','511301','市辖区','sxq',1,'511300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511302','511302','顺庆区','sqq',1,'511300',106.0840910000,30.7955720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511303','511303','高坪区','gpq',1,'511300',106.1040040000,30.7800010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511304','511304','嘉陵区','jlq',1,'511300',106.0670270000,30.7629760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511321','511321','南部县','nbx',2,'511300',106.0611380000,31.3494070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511322','511322','营山县','ysx',2,'511300',106.5648930000,31.0759070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511323','511323','蓬安县','pax',2,'511300',106.4134880000,31.0279780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511324','511324','仪陇县','ylx',2,'511300',106.2970830000,31.2712610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511325','511325','西充县','xcx',2,'511300',105.8930210000,30.9946160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511381','511381','阆中市','lzs',2,'511300',105.9752660000,31.5804660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511400','511400','眉山市','mss',1,'510000',103.8317880000,30.0483180000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511401','511401','市辖区','sxq',1,'511400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511402','511402','东坡区','dpq',1,'511400',103.8315530000,30.0481280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511421','511421','仁寿县','rsx',2,'511400',104.1476460000,29.9967210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511422','511422','彭山县','psx',2,'511400',103.8701000000,30.1922980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511423','511423','洪雅县','hyx',2,'511400',103.3750060000,29.9048670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511424','511424','丹棱县','dlx',2,'511400',103.5183330000,30.0127510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511425','511425','青神县','qsx',2,'511400',103.8461310000,29.8314690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511500','511500','宜宾市','ybs',1,'510000',104.6308250000,28.7601890000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511501','511501','市辖区','sxq',1,'511500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511502','511502','翠屏区','cpq',1,'511500',104.6302310000,28.7601790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511521','511521','宜宾县','ybx',2,'511500',104.5414890000,28.6956780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511522','511522','南溪县','nxx',2,'511500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511523','511523','江安县','jax',2,'511500',105.0686970000,28.7281020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511524','511524','长宁县','cnx',2,'511500',104.9211160000,28.5772710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511525','511525','高县','gx',2,'511500',104.5191870000,28.4356760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511526','511526','珙县','gx',2,'511500',104.7122680000,28.4490410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511527','511527','筠连县','ylx',2,'511500',104.5078480000,28.1620170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511528','511528','兴文县','xwx',2,'511500',105.2365490000,28.3029880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511529','511529','屏山县','psx',2,'511500',104.1626170000,28.6423700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511600','511600','广安市','gas',1,'510000',106.6333690000,30.4563980000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511601','511601','市辖区','sxq',1,'511600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511602','511602','广安区','gaq',1,'511600',106.6329070000,30.4564620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511621','511621','岳池县','ycx',2,'511600',106.4444510000,30.5335380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511622','511622','武胜县','wsx',2,'511600',106.2924730000,30.3442910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511623','511623','邻水县','lsx',2,'511600',106.9349680000,30.3343230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511681','511681','华莹市','hys',2,'511600',106.7778820000,30.3805740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511700','511700','达州市','dzs',1,'510000',107.5022620000,31.2094840000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511701','511701','市辖区','sxq',1,'511700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511702','511702','通川区','tcq',1,'511700',107.5010620000,31.2135220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511721','511721','达县','dx',2,'511700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511722','511722','宣汉县','xhx',2,'511700',107.7222540000,31.3550250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511723','511723','开江县','kjx',2,'511700',107.8641350000,31.0855370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511724','511724','大竹县','dzx',2,'511700',107.2074200000,30.7362890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511725','511725','渠县','qx',2,'511700',106.9707460000,30.8363480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511781','511781','万源市','wys',2,'511700',108.0375480000,32.0677700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511800','511800','雅安市','yas',1,'510000',103.0010330000,29.9877220000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511801','511801','市辖区','sxq',1,'511800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511802','511802','雨城区','ycq',1,'511800',103.0033980000,29.9818310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511821','511821','名山县','msx',2,'511800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511822','511822','荥经县','xjx',2,'511800',102.8446740000,29.7955290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511823','511823','汉源县','hyx',2,'511800',102.6771450000,29.3499150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511824','511824','石棉县','smx',2,'511800',102.3596200000,29.2340630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511825','511825','天全县','tqx',2,'511800',102.7634620000,30.0599550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511826','511826','芦山县','lsx',2,'511800',102.9240160000,30.1529070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511827','511827','宝兴县','bxx',2,'511800',102.8133770000,30.3690260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511900','511900','巴中市','bzs',1,'510000',106.7536690000,31.8588090000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511901','511901','市辖区','sxq',1,'511900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511902','511902','巴州区','bzq',1,'511900',106.7536710000,31.8583660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511921','511921','通江县','tjx',2,'511900',107.2476210000,31.9121200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511922','511922','南江县','njx',2,'511900',106.8434180000,32.3531640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('511923','511923','平昌县','pcx',2,'511900',107.1019370000,31.5628140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('512000','512000','资阳市','zys',1,'510000',104.6419170000,30.1222110000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('512001','512001','市辖区','sxq',1,'512000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('512002','512002','雁江区','yjq',1,'512000',104.6423380000,30.1216860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('512021','512021','安岳县','ayx',2,'512000',105.3367640000,30.0992060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('512022','512022','乐至县','lzx',2,'512000',105.0311420000,30.2756190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('512081','512081','简阳市','jys',2,'512000',104.5503390000,30.3906660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513200','513200','阿坝藏族羌族自治州','abczqzzzz',1,'510000',102.2213740000,31.8997920000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513221','513221','汶川县','wcx',2,'513200',103.5806750000,31.4746300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513222','513222','理县','lx',2,'513200',103.1654860000,31.4367640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513223','513223','茂县','mx',2,'513200',103.8506840000,31.6804070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513224','513224','松潘县','spx',2,'513200',103.5991770000,32.6383800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513225','513225','九寨沟县','jzgx',2,'513200',104.2363440000,33.2620970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513226','513226','金川县','jcx',2,'513200',102.0646470000,31.4763560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513227','513227','小金县','xjx',2,'513200',102.3631930000,30.9990160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513228','513228','黑水县','hsx',2,'513200',102.9908050000,32.0617210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513229','513229','马尔康县','mekx',2,'513200',102.2211870000,31.8997610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513230','513230','壤塘县','rtx',2,'513200',100.9791360000,32.2648870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513231','513231','阿坝县','abx',2,'513200',101.7009850000,32.9042230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513232','513232','若尔盖县','regx',2,'513200',102.9637260000,33.5759340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513233','513233','红原县','hyx',2,'513200',102.5449060000,32.7939020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513300','513300','甘孜藏族自治州','gzczzzz',1,'510000',101.9638150000,30.0506630000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513321','513321','康定县','kdx',2,'513300',101.9640570000,30.0507380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513322','513322','泸定县','ldx',2,'513300',102.2332250000,29.9124820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513323','513323','丹巴县','dbx',2,'513300',101.8861250000,30.8770830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513324','513324','九龙县','jlx',2,'513300',101.5069420000,29.0019750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513325','513325','雅江县','yjx',2,'513300',101.0157350000,30.0322500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513326','513326','道孚县','dfx',2,'513300',101.1233270000,30.9787670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513327','513327','炉霍县','lhx',2,'513300',100.6794950000,31.3926740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513328','513328','甘孜县','gzx',2,'513300',99.9917530000,31.6197500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513329','513329','新龙县','xlx',2,'513300',100.3120940000,30.9389600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513330','513330','德格县','dgx',2,'513300',98.5799900000,31.8067290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513331','513331','白玉县','byx',2,'513300',98.8243430000,31.2088050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513332','513332','石渠县','sqx',2,'513300',98.1008870000,32.9753020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513333','513333','色达县','sdx',2,'513300',100.3316570000,32.2687770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513334','513334','理塘县','ltx',2,'513300',100.2698620000,29.9918070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513335','513335','巴塘县','btx',2,'513300',99.1090370000,30.0057230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513336','513336','乡城县','xcx',2,'513300',99.7999430000,28.9308550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513337','513337','稻城县','dcx',2,'513300',100.2966890000,29.0375440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513338','513338','得荣县','drx',2,'513300',99.2880360000,28.7113400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513400','513400','凉山彝族自治州','lsyzzzz',1,'510000',102.2587460000,27.8867620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513401','513401','西昌市','xcs',2,'513400',102.2587580000,27.8857860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513422','513422','木里藏族自治县','mlczzzx',2,'513400',101.2801840000,27.9268590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513423','513423','盐源县','yyx',2,'513400',101.5089090000,27.4234150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513424','513424','德昌县','dcx',2,'513400',102.1788450000,27.4038270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513425','513425','会理县','hlx',2,'513400',102.2495480000,26.6587020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513426','513426','会东县','hdx',2,'513400',102.5789850000,26.6307130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513427','513427','宁南县','nnx',2,'513400',102.7573740000,27.0652050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513428','513428','普格县','pgx',2,'513400',102.5410820000,27.3768280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513429','513429','布拖县','btx',2,'513400',102.8088010000,27.7090620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513430','513430','金阳县','jyx',2,'513400',103.2487040000,27.6959160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513431','513431','昭觉县','zjx',2,'513400',102.8439910000,28.0105540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513432','513432','喜德县','xdx',2,'513400',102.4123420000,28.3054860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513433','513433','冕宁县','mnx',2,'513400',102.1700460000,28.5508440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513434','513434','越西县','yxx',2,'513400',102.5088750000,28.6396320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513435','513435','甘洛县','glx',2,'513400',102.7759240000,28.9770940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513436','513436','美姑县','mgx',2,'513400',103.1320070000,28.3279460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('513437','513437','雷波县','lbx',2,'513400',103.5715840000,28.2629460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520000','520000','贵州省','gzs',0,'0',106.7134780000,26.5783430000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520100','520100','贵阳市','gys',1,'520000',106.7134780000,26.5783430000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520101','520101','市辖区','sxq',1,'520100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520102','520102','南明区','nmq',1,'520100',106.7155320000,26.5747210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520103','520103','云岩区','yyq',1,'520100',106.7318500000,26.6165280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520111','520111','花溪区','hxq',1,'520100',106.6029470000,26.4586300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520112','520112','乌当区','wdq',1,'520100',106.7692240000,26.6321410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520113','520113','白云区','byq',1,'520100',106.6352700000,26.6763400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520114','520114','小河区','xhq',1,'520100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520121','520121','开阳县','kyx',2,'520100',106.8557850000,27.0517670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520122','520122','息烽县','xfx',2,'520100',106.7367140000,27.0926570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520123','520123','修文县','xwx',2,'520100',106.5992180000,26.8406720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520181','520181','清镇市','qzs',2,'520100',106.4427780000,26.5361260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520200','520200','六盘水市','lpss',1,'520000',104.8467430000,26.5846430000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520201','520201','钟山区','zsq',1,'520200',104.8462440000,26.5848050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520203','520203','六枝特区','lztq',1,'520200',105.4742350000,26.2106620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520221','520221','水城县','scx',2,'520200',104.9568500000,26.5404780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520222','520222','盘县','px',2,'520200',104.4683670000,25.7069660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520300','520300','遵义市','zys',1,'520000',106.9372650000,27.7066260000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520301','520301','市辖区','sxq',1,'520300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520302','520302','红花岗区','hhgq',1,'520300',106.9437840000,27.6943950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520303','520303','汇川区','hcq',1,'520300',106.9372650000,27.7066260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520321','520321','遵义县','zyx',2,'520300',106.8316680000,27.5352880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520322','520322','桐梓县','tzx',2,'520300',106.8265910000,28.1315590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520323','520323','绥阳县','syx',2,'520300',107.1910240000,27.9513420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520324','520324','正安县','zax',2,'520300',107.4418720000,28.5503370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520325','520325','道真仡佬族苗族自治县','dzylzmzzzx',2,'520300',107.6053420000,28.8800880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520326','520326','务川仡佬族苗族自治县','wcylzmzzzx',2,'520300',107.8878570000,28.5215670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520327','520327','凤冈县','fgx',2,'520300',107.7220210000,27.9608580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520328','520328','湄潭县','mtx',2,'520300',107.4857230000,27.7658390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520329','520329','余庆县','yqx',2,'520300',107.8925660000,27.2215520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520330','520330','习水县','xsx',2,'520300',106.2009540000,28.3278260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520381','520381','赤水市','css',2,'520300',105.6981160000,28.5870570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520382','520382','仁怀市','rhs',2,'520300',106.4124760000,27.8033770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520400','520400','安顺市','ass',1,'520000',105.9321880000,26.2455440000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520401','520401','市辖区','sxq',1,'520400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520402','520402','西秀区','xxq',1,'520400',105.9543250000,26.2521370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520421','520421','平坝县','pbx',2,'520400',106.2599420000,26.4060800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520422','520422','普定县','pdx',2,'520400',105.7456090000,26.3057940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520423','520423','镇宁布依族苗族自治县','znbyzmzzzx',2,'520400',105.7686560000,26.0560960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520424','520424','关岭布依族苗族自治县','glbyzmzzzx',2,'520400',105.6184540000,25.9442480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('520425','520425','紫云苗族布依族自治县','zymzbyzzzx',2,'520400',106.0845150000,25.7515670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522200','522200','铜仁地区','trdq',1,'520000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522201','522201','铜仁市','trs',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522222','522222','江口县','jkx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522223','522223','玉屏侗族自治县','ypdzzzx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522224','522224','石阡县','sqx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522225','522225','思南县','snx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522226','522226','印江土家族苗族自治县','yjtjzmzzzx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522227','522227','德江县','djx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522228','522228','沿河土家族自治县','yhtjzzzx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522229','522229','松桃苗族自治县','stmzzzx',2,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522230','522230','万山特区','wstq',1,'522200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522300','522300','黔西南布依族苗族自治州','qxnbyzmzzzz',1,'520000',104.8979710000,25.0881200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522301','522301','兴义市','xys',2,'522300',104.8979820000,25.0885990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522322','522322','兴仁县','xrx',2,'522300',105.1927780000,25.4313780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522323','522323','普安县','pax',2,'522300',104.9553470000,25.7864040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522324','522324','晴隆县','qlx',2,'522300',105.2187730000,25.8328810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522325','522325','贞丰县','zfx',2,'522300',105.6501330000,25.3857520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522326','522326','望谟县','wmx',2,'522300',106.0915630000,25.1666670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522327','522327','册亨县','chx',2,'522300',105.8124100000,24.9833380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522328','522328','安龙县','alx',2,'522300',105.4714980000,25.1089590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522400','522400','毕节地区','bjdq',1,'520000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522401','522401','毕节市','bjs',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522422','522422','大方县','dfx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522423','522423','黔西县','qxx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522424','522424','金沙县','jsx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522425','522425','织金县','zjx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522426','522426','纳雍县','nyx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522427','522427','威宁彝族回族苗族自治县','wnyzhzmzzzx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522428','522428','赫章县','hzx',2,'522400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522600','522600','黔东南苗族侗族自治州','qdnmzdzzzz',1,'520000',107.9774880000,26.5833520000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522601','522601','凯里市','kls',2,'522600',107.9775410000,26.5829640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522622','522622','黄平县','hpx',2,'522600',107.9013370000,26.8969730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522623','522623','施秉县','sbx',2,'522600',108.1267800000,27.0346570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522624','522624','三穗县','ssx',2,'522600',108.6811210000,26.9598840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522625','522625','镇远县','zyx',2,'522600',108.4236560000,27.0502330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522626','522626','岑巩县','cgx',2,'522600',108.8164590000,27.1732440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522627','522627','天柱县','tzx',2,'522600',109.2127980000,26.9096840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522628','522628','锦屏县','jpx',2,'522600',109.2025200000,26.6806250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522629','522629','剑河县','jhx',2,'522600',108.4404990000,26.7273490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522630','522630','台江县','tjx',2,'522600',108.3146370000,26.6691380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522631','522631','黎平县','lpx',2,'522600',109.1365040000,26.2306360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522632','522632','榕江县','rjx',2,'522600',108.5210260000,25.9310850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522633','522633','从江县','cjx',2,'522600',108.9126480000,25.7470580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522634','522634','雷山县','lsx',2,'522600',108.0796130000,26.3810270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522635','522635','麻江县','mjx',2,'522600',107.5931720000,26.4948030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522636','522636','丹寨县','dzx',2,'522600',107.7948080000,26.1994970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522700','522700','黔南布依族苗族自治州','qnbyzmzzzz',1,'520000',107.5171560000,26.2582190000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522701','522701','都匀市','dys',2,'522700',107.5170210000,26.2582050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522702','522702','福泉市','fqs',2,'522700',107.5135080000,26.7025080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522722','522722','荔波县','lbx',2,'522700',107.8838000000,25.4122390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522723','522723','贵定县','gdx',2,'522700',107.2335880000,26.5808070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522725','522725','瓮安县','wax',2,'522700',107.4784170000,27.0663390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522726','522726','独山县','dsx',2,'522700',107.5427570000,25.8262830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522727','522727','平塘县','ptx',2,'522700',107.3240500000,25.8318030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522728','522728','罗甸县','ldx',2,'522700',106.7500060000,25.4298940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522729','522729','长顺县','csx',2,'522700',106.4473760000,26.0221160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522730','522730','龙里县','llx',2,'522700',106.9777330000,26.4488090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522731','522731','惠水县','hsx',2,'522700',106.6578480000,26.1286370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('522732','522732','三都水族自治县','sdszzzx',2,'522700',107.8774700000,25.9851830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530000','530000','云南省','yns',0,'0',102.7122510000,25.0406090000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530100','530100','昆明市','kms',1,'530000',102.7122510000,25.0406090000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530101','530101','市辖区','sxq',1,'530100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530102','530102','五华区','whq',1,'530100',102.7131150000,25.0416660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530103','530103','盘龙区','plq',1,'530100',102.7262230000,25.0529560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530111','530111','官渡区','gdq',1,'530100',102.7260920000,25.0335000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530112','530112','西山区','xsq',1,'530100',102.6835090000,25.0401400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530113','530113','东川区','dcq',1,'530100',103.1820000000,26.0834900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530121','530121','呈贡县','cgx',2,'530100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530122','530122','晋宁县','jnx',2,'530100',102.7484720000,24.7056120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530124','530124','富民县','fmx',2,'530100',102.4983910000,25.2186790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530125','530125','宜良县','ylx',2,'530100',103.1452340000,24.9185300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530126','530126','石林彝族自治县','slyzzzx',2,'530100',103.2719620000,24.7545450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530127','530127','嵩明县','smx',2,'530100',103.0598870000,25.2317650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530128','530128','禄劝彝族苗族自治县','lqyzmzzzx',2,'530100',102.4690500000,25.5565330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530129','530129','寻甸回族彝族自治县','xdhzyzzzx',2,'530100',103.2575880000,25.5594740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530181','530181','安宁市','ans',2,'530100',102.4863040000,24.9228030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530300','530300','曲靖市','qjs',1,'530000',103.7978510000,25.5015570000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530301','530301','市辖区','sxq',1,'530300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530302','530302','麒麟区','qlq',1,'530300',103.7980540000,25.5012690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530321','530321','马龙县','mlx',2,'530300',103.5787550000,25.4294510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530322','530322','陆良县','llx',2,'530300',103.6552330000,25.0228780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530323','530323','师宗县','szx',2,'530300',103.9938080000,24.8256810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530324','530324','罗平县','lpx',2,'530300',104.3092630000,24.8857080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530325','530325','富源县','fyx',2,'530300',104.2569200000,25.6706400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530326','530326','会泽县','hzx',2,'530300',103.3000410000,26.4128610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530328','530328','沾益县','zyx',2,'530300',103.8192620000,25.6008780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530381','530381','宣威市','xws',2,'530300',104.0955400000,26.2277770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530400','530400','玉溪市','yxs',1,'530000',102.5439070000,24.3504610000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530401','530401','市辖区','sxq',1,'530400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530402','530402','红塔区','htq',1,'530400',102.5434680000,24.3507530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530421','530421','江川县','jcx',2,'530400',102.7498390000,24.2910060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530422','530422','澄江县','cjx',2,'530400',102.9166520000,24.6696790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530423','530423','通海县','thx',2,'530400',102.7600390000,24.1122050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530424','530424','华宁县','hnx',2,'530400',102.9289820000,24.1898070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530425','530425','易门县','ymx',2,'530400',102.1621100000,24.6695980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530426','530426','峨山彝族自治县','esyzzzx',2,'530400',102.4043580000,24.1732560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530427','530427','新平彝族傣族自治县','xpyzdzzzx',2,'530400',101.9909030000,24.0664000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530428','530428','元江哈尼族彝族傣族自治县','yjhnzyzdzzzx',2,'530400',101.9996580000,23.5976180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530500','530500','保山市','bss',1,'530000',99.1671330000,25.1118020000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530501','530501','市辖区','sxq',1,'530500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530502','530502','隆阳区','lyq',1,'530500',99.1658250000,25.1121440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530521','530521','施甸县','sdx',2,'530500',99.1837580000,24.7308470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530522','530522','腾冲县','tcx',2,'530500',98.4972920000,25.0175700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530523','530523','龙陵县','llx',2,'530500',98.6935670000,24.5919120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530524','530524','昌宁县','cnx',2,'530500',99.6123440000,24.8236620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530600','530600','昭通市','zts',1,'530000',103.7172160000,27.3369990000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530601','530601','市辖区','sxq',1,'530600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530602','530602','昭阳区','zyq',1,'530600',103.7172670000,27.3366360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530621','530621','鲁甸县','ldx',2,'530600',103.5493330000,27.1916370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530622','530622','巧家县','qjx',2,'530600',102.9292840000,26.9117000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530623','530623','盐津县','yjx',2,'530600',104.2350600000,28.1069230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530624','530624','大关县','dgx',2,'530600',103.8916080000,27.7471140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530625','530625','永善县','ysx',2,'530600',103.6373200000,28.2315260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530626','530626','绥江县','sjx',2,'530600',103.9610950000,28.5999530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530627','530627','镇雄县','zxx',2,'530600',104.8730550000,27.4362670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530628','530628','彝良县','ylx',2,'530600',104.0484920000,27.6274250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530629','530629','威信县','wxx',2,'530600',105.0486900000,27.8433810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530630','530630','水富县','sfx',2,'530600',104.4153760000,28.6296880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530700','530700','丽江市','ljs',1,'530000',100.2330260000,26.8721080000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530701','530701','市辖区','sxq',1,'530700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530702','530702','古城区','gcq',1,'530700',100.2344120000,26.8722290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530721','530721','玉龙纳西族自治县','ylnxzzzx',2,'530700',100.2383120000,26.8305930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530722','530722','永胜县','ysx',2,'530700',100.7509010000,26.6856230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530723','530723','华坪县','hpx',2,'530700',101.2677960000,26.6288340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530724','530724','宁蒗彝族自治县','nlyzzzx',2,'530700',100.8524270000,27.2811090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530800','530800','思茅市','sms',1,'530000',100.9723440000,22.7773210000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530801','530801','市辖区','sxq',1,'530800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530802','530802','翠云区','cyq',1,'530800',100.9732270000,22.7765950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530821','530821','普洱哈尼族彝族自治县','pehnzyzzzx',2,'530800',101.0452400000,23.0625070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530822','530822','墨江哈尼族自治县','mjhnzzzx',2,'530800',101.6876060000,23.4281650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530823','530823','景东彝族自治县','jdyzzzx',2,'530800',100.8400110000,24.4485230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530824','530824','景谷傣族彝族自治县','jgdzyzzzx',2,'530800',100.7014250000,23.5002780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530825','530825','镇沅彝族哈尼族拉祜族自治县','zyyzhnzlhzzzx',2,'530800',101.1085120000,24.0057120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530826','530826','江城哈尼族彝族自治县','jchnzyzzzx',2,'530800',101.8591440000,22.5833600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530827','530827','孟连傣族拉祜族佤族自治县','mldzlhzwzzzx',2,'530800',99.5854060000,22.3259240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530828','530828','澜沧拉祜族自治县','lclhzzzx',2,'530800',99.9312010000,22.5530830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530829','530829','西盟佤族自治县','xmwzzzx',2,'530800',99.5943720000,22.6444230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530900','530900','临沧市','lcs',1,'530000',100.0869700000,23.8865670000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530901','530901','市辖区','sxq',1,'530900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530902','530902','临翔区','lxq',1,'530900',100.0864860000,23.8865620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530921','530921','凤庆县','fqx',2,'530900',99.9187100000,24.5927380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530922','530922','云县','yx',2,'530900',100.1256370000,24.4390260000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530923','530923','永德县','ydx',2,'530900',99.2536790000,24.0281590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530924','530924','镇康县','zkx',2,'530900',98.8274300000,23.7614150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530925','530925','双江拉祜族佤族布朗族傣族自治县','sjlhzwzblzdzzzx',2,'530900',99.8244190000,23.4774760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530926','530926','耿马傣族佤族自治县','gmdzwzzzx',2,'530900',99.4024950000,23.5345790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('530927','530927','沧源佤族自治县','cywzzzx',2,'530900',99.2474000000,23.1468870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532300','532300','楚雄彝族自治州','cxyzzzz',1,'530000',101.5460460000,25.0419880000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532301','532301','楚雄市','cxs',2,'532300',101.5461450000,25.0409120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532322','532322','双柏县','sbx',2,'532300',101.6382400000,24.6850940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532323','532323','牟定县','mdx',2,'532300',101.5430440000,25.3121110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532324','532324','南华县','nhx',2,'532300',101.2749910000,25.1924080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532325','532325','姚安县','yax',2,'532300',101.2383990000,25.5054030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532326','532326','大姚县','dyx',2,'532300',101.3236020000,25.7223480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532327','532327','永仁县','yrx',2,'532300',101.6711750000,26.0563160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532328','532328','元谋县','ymx',2,'532300',101.8708370000,25.7033130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532329','532329','武定县','wdx',2,'532300',102.4067850000,25.5301000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532331','532331','禄丰县','lfx',2,'532300',102.0756940000,25.1432700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532500','532500','红河哈尼族彝族自治州','hhhnzyzzzz',1,'530000',103.3841820000,23.3667750000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532501','532501','个旧市','gjs',2,'532500',103.1547520000,23.3603830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532502','532502','开远市','kys',2,'532500',103.2586790000,23.7138320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532522','532522','蒙自县','mzx',2,'532500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532523','532523','屏边苗族自治县','pbmzzzx',2,'532500',103.6872290000,22.9870130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532524','532524','建水县','jsx',2,'532500',102.8204930000,23.6183870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532525','532525','石屏县','spx',2,'532500',102.4844690000,23.7125690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532526','532526','弥勒县','mlx',2,'532500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532527','532527','泸西县','lxx',2,'532500',103.7596220000,24.5323680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532528','532528','元阳县','yyx',2,'532500',102.8370560000,23.2197730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532529','532529','红河县','hhx',2,'532500',102.4212100000,23.3691910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532530','532530','金平苗族瑶族傣族自治县','jpmzyzdzzzx',2,'532500',103.2283590000,22.7799820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532531','532531','绿春县','lcx',2,'532500',102.3928600000,22.9935200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532532','532532','河口瑶族自治县','hkyzzzx',2,'532500',103.9615930000,22.5075630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532600','532600','文山壮族苗族自治州','wszzmzzzz',1,'530000',104.2440100000,23.3695100000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532621','532621','文山县','wsx',2,'532600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532622','532622','砚山县','ysx',2,'532600',104.3439890000,23.6123010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532623','532623','西畴县','xcx',2,'532600',104.6757110000,23.4374390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532624','532624','麻栗坡县','mlpx',2,'532600',104.7018990000,23.1242020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532625','532625','马关县','mgx',2,'532600',104.3986190000,23.0117230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532626','532626','丘北县','qbx',2,'532600',104.1943660000,24.0409820000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532627','532627','广南县','gnx',2,'532600',105.0566840000,24.0502720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532628','532628','富宁县','fnx',2,'532600',105.6285600000,23.6264940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532800','532800','西双版纳傣族自治州','xsbndzzzz',1,'530000',100.7979410000,22.0017240000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532801','532801','景洪市','jhs',2,'532800',100.7979470000,22.0020870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532822','532822','勐海县','mhx',2,'532800',100.4482880000,21.9558660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532823','532823','勐腊县','mlx',2,'532800',101.5670510000,21.4794490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532900','532900','大理白族自治州','dlbzzzz',1,'530000',100.2256680000,25.5894490000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532901','532901','大理市','dls',2,'532900',100.2413690000,25.5930670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532922','532922','漾濞彝族自治县','ybyzzzx',2,'532900',99.9579700000,25.6695430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532923','532923','祥云县','xyx',2,'532900',100.5540250000,25.4770720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532924','532924','宾川县','bcx',2,'532900',100.5789570000,25.8259040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532925','532925','弥渡县','mdx',2,'532900',100.4906690000,25.3425940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532926','532926','南涧彝族自治县','njyzzzx',2,'532900',100.5186830000,25.0412790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532927','532927','巍山彝族回族自治县','wsyzhzzzx',2,'532900',100.3079300000,25.2309090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532928','532928','永平县','ypx',2,'532900',99.5335360000,25.4612810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532929','532929','云龙县','ylx',2,'532900',99.3694020000,25.8849550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532930','532930','洱源县','eyx',2,'532900',99.9517080000,26.1111840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532931','532931','剑川县','jcx',2,'532900',99.9058870000,26.5300660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('532932','532932','鹤庆县','hqx',2,'532900',100.1733750000,26.5583900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533100','533100','德宏傣族景颇族自治州','dhdzjpzzzz',1,'530000',98.5783630000,24.4366940000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533102','533102','瑞丽市','rls',2,'533100',97.8558830000,24.0107340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533103','533103','潞西市','lxs',2,'533100',98.5776080000,24.4366990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533122','533122','梁河县','lhx',2,'533100',98.2981960000,24.8074200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533123','533123','盈江县','yjx',2,'533100',97.9339300000,24.7095410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533124','533124','陇川县','lcx',2,'533100',97.7944410000,24.1840650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533300','533300','怒江傈僳族自治州','njlszzzz',1,'530000',98.8543040000,25.8509490000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533321','533321','泸水县','lsx',2,'533300',98.8540630000,25.8511420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533323','533323','福贡县','fgx',2,'533300',98.8674130000,26.9027380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533324','533324','贡山独龙族怒族自治县','gsdlznzzzx',2,'533300',98.6661410000,27.7380540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533325','533325','兰坪白族普米族自治县','lpbzpmzzzx',2,'533300',99.4213780000,26.4538390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533400','533400','迪庆藏族自治州','dqczzzz',1,'530000',99.7064630000,27.8268530000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533421','533421','香格里拉县','xgllx',2,'533400',99.7086670000,27.8258040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533422','533422','德钦县','dqx',2,'533400',98.9150600000,28.4832720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('533423','533423','维西傈僳族自治县','wxlszzzx',2,'533400',99.2863550000,27.1809480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540000','540000','西藏','xczzq',0,'0',91.1322120000,29.6603610000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540100','540100','拉萨市','lss',1,'540000',91.1322120000,29.6603610000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540101','540101','市辖区','sxq',1,'540100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540102','540102','城关区','cgq',1,'540100',91.1329110000,29.6594720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540121','540121','林周县','lzx',2,'540100',91.2618420000,29.8957540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540122','540122','当雄县','dxx',2,'540100',91.1035510000,30.4748190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540123','540123','尼木县','nmx',2,'540100',90.1655450000,29.4313460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540124','540124','曲水县','qsx',2,'540100',90.7380510000,29.3498950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540125','540125','堆龙德庆县','dldqx',2,'540100',91.0028230000,29.6473470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540126','540126','达孜县','dzx',2,'540100',91.3509760000,29.6703140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('540127','540127','墨竹工卡县','mzgkx',2,'540100',91.7311580000,29.8346570000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542100','542100','昌都地区','cddq',1,'540000',97.1784520000,31.1368750000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542121','542121','昌都县','cdx',2,'542100',97.1782550000,31.1370350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542122','542122','江达县','jdx',2,'542100',98.2183510000,31.4995340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542123','542123','贡觉县','gjx',2,'542100',98.2711910000,30.8592060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542124','542124','类乌齐县','lwqx',2,'542100',96.6012590000,31.2130480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542125','542125','丁青县','dqx',2,'542100',95.5977480000,31.4106810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542126','542126','察雅县','cyx',2,'542100',97.5657010000,30.6530380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542127','542127','八宿县','bsx',2,'542100',96.9178930000,30.0534080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542128','542128','左贡县','zgx',2,'542100',97.8405320000,29.6713350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542129','542129','芒康县','mkx',2,'542100',98.5964440000,29.6866150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542132','542132','洛隆县','llx',2,'542100',95.8234180000,30.7419470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542133','542133','边坝县','bbx',2,'542100',94.7075040000,30.9338490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542200','542200','山南地区','sndq',1,'540000',91.7665290000,29.2360230000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542221','542221','乃东县','ndx',2,'542200',91.7652500000,29.2361060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542222','542222','扎囊县','znx',2,'542200',91.3380000000,29.2464760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542223','542223','贡嘎县','ggx',2,'542200',90.9852710000,29.2890780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542224','542224','桑日县','srx',2,'542200',92.0157320000,29.2597740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542225','542225','琼结县','qjx',2,'542200',91.6837530000,29.0252420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542226','542226','曲松县','qsx',2,'542200',92.2010660000,29.0636560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542227','542227','措美县','cmx',2,'542200',91.4323470000,28.4373530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542228','542228','洛扎县','lzx',2,'542200',90.8582430000,28.3857650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542229','542229','加查县','jcx',2,'542200',92.5910430000,29.1409210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542231','542231','隆子县','lzx',2,'542200',92.4633090000,28.4085480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542232','542232','错那县','cnx',2,'542200',91.9601320000,27.9917070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542233','542233','浪卡子县','lqzx',2,'542200',90.3987470000,28.9683600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542300','542300','日喀则地区','rkzdq',1,'540000',88.8851480000,29.2675190000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542301','542301','日喀则市','rkzs',2,'542300',88.8866700000,29.2670030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542322','542322','南木林县','nmlx',2,'542300',89.0994340000,29.6804590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542323','542323','江孜县','jzx',2,'542300',89.6050440000,28.9088450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542324','542324','定日县','drx',2,'542300',87.1238870000,28.6566670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542325','542325','萨迦县','sjx',2,'542300',88.0230070000,28.9010770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542326','542326','拉孜县','lzx',2,'542300',87.6374300000,29.0851360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542327','542327','昂仁县','arx',2,'542300',87.2357800000,29.2947580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542328','542328','谢通门县','xtmx',2,'542300',88.2605170000,29.4315970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542329','542329','白朗县','blx',2,'542300',89.2636180000,29.1066270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542330','542330','仁布县','rbx',2,'542300',89.8432070000,29.2302990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542331','542331','康马县','kmx',2,'542300',89.6834060000,28.5547190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542332','542332','定结县','djx',2,'542300',87.7677230000,28.3640900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542333','542333','仲巴县','zbx',2,'542300',84.0328260000,29.7683360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542334','542334','亚东县','ydx',2,'542300',88.9068060000,27.4827720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542335','542335','吉隆县','jlx',2,'542300',85.2983490000,28.8524160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542336','542336','聂拉木县','nlmx',2,'542300',85.9819530000,28.1559500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542337','542337','萨嘎县','sgx',2,'542300',85.2346220000,29.3281940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542338','542338','岗巴县','gbx',2,'542300',88.5189030000,28.2743710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542400','542400','那曲地区','nqdq',1,'540000',92.0602140000,31.4760040000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542421','542421','那曲县','nqx',2,'542400',92.0618620000,31.4757560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542422','542422','嘉黎县','jlx',2,'542400',93.2329070000,30.6408460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542423','542423','比如县','brx',2,'542400',93.6804400000,31.4799170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542424','542424','聂荣县','nrx',2,'542400',92.3036590000,32.1078550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542425','542425','安多县','adx',2,'542400',91.6818790000,32.2602990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542426','542426','申扎县','szx',2,'542400',88.7097770000,30.9290560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542427','542427','索县','sx',2,'542400',93.7849640000,31.8861730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542428','542428','班戈县','bgx',2,'542400',90.0118220000,31.3945780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542429','542429','巴青县','bqx',2,'542400',94.0540490000,31.9186910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542430','542430','尼玛县','nmx',2,'542400',87.2366460000,31.7849790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542500','542500','阿里地区','aldq',1,'540000',80.1054980000,32.5031870000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542521','542521','普兰县','plx',2,'542500',81.1775880000,30.2918960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542522','542522','札达县','zdx',2,'542500',79.8031910000,31.4785870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542523','542523','噶尔县','gex',2,'542500',80.1050050000,32.5033730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542524','542524','日土县','rtx',2,'542500',79.7319370000,33.3824540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542525','542525','革吉县','gjx',2,'542500',81.1428960000,32.3891920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542526','542526','改则县','gzx',2,'542500',84.0623840000,32.3020760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542527','542527','措勤县','cqx',2,'542500',85.1592540000,31.0167740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542600','542600','林芝地区','lzdq',1,'540000',94.3623480000,29.6546930000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542621','542621','林芝县','lzx',2,'542600',94.3609870000,29.6537320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542622','542622','工布江达县','gbjdx',2,'542600',93.2465150000,29.8844700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542623','542623','米林县','mlx',2,'542600',94.2136790000,29.2138110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542624','542624','墨脱县','mtx',2,'542600',95.3322450000,29.3257300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542625','542625','波密县','bmx',2,'542600',95.7681510000,29.8587710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542626','542626','察隅县','cyx',2,'542600',97.4650020000,28.6602440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('542627','542627','朗县','lx',2,'542600',93.0734290000,29.0446000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610000','610000','陕西省','sxs',0,'0',108.9480240000,34.2631610000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610100','610100','西安市','xas',1,'610000',108.9480240000,34.2631610000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610101','610101','市辖区','sxq',1,'610100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610102','610102','新城区','xcq',1,'610100',108.9945890000,34.2678880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610103','610103','碑林区','blq',1,'610100',108.9630900000,34.2359500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610104','610104','莲湖区','lhq',1,'610100',108.8758120000,34.2525180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610111','610111','灞桥区','bqq',1,'610100',109.0657150000,34.2867510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610112','610112','未央区','wyq',1,'610100',108.9716430000,34.2953080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610113','610113','雁塔区','ytq',1,'610100',108.9367090000,34.2039560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610114','610114','阎良区','ylq',1,'610100',109.2280200000,34.6621410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610115','610115','临潼区','ltq',1,'610100',109.1992610000,34.3823050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610116','610116','长安区','caq',1,'610100',108.9002090000,34.0670560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610122','610122','蓝田县','ltx',2,'610100',109.2262610000,34.1256980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610124','610124','周至县','zzx',2,'610100',108.2164650000,34.1615320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610125','610125','户县','hx',2,'610100',108.6073850000,34.1086680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610126','610126','高陵县','glx',2,'610100',109.0888960000,34.5350650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610200','610200','铜川市','tcs',1,'610000',108.9796080000,34.9165820000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610201','610201','市辖区','sxq',1,'610200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610202','610202','王益区','wyq',1,'610200',109.0758620000,35.0690980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610203','610203','印台区','ytq',1,'610200',109.1008140000,35.1119270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610204','610204','耀州区','yzq',1,'610200',108.9625380000,34.9102060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610222','610222','宜君县','yjx',2,'610200',109.1182780000,35.3987660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610300','610300','宝鸡市','bjs',1,'610000',107.1448700000,34.3693150000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610301','610301','市辖区','sxq',1,'610300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610302','610302','渭滨区','wbq',1,'610300',107.1444670000,34.3710080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610303','610303','金台区','jtq',1,'610300',107.1499430000,34.3751920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610304','610304','陈仓区','ccq',1,'610300',107.3836450000,34.3527470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610322','610322','凤翔县','fxx',2,'610300',107.4005770000,34.5216680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610323','610323','岐山县','qsx',2,'610300',107.6244640000,34.4429600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610324','610324','扶风县','ffx',2,'610300',107.8914190000,34.3754970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610326','610326','眉县','mx',2,'610300',107.7523710000,34.2721370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610327','610327','陇县','lx',2,'610300',106.8570660000,34.8932620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610328','610328','千阳县','qyx',2,'610300',107.1329870000,34.6425840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610329','610329','麟游县','lyx',2,'610300',107.7966080000,34.6777140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610330','610330','凤县','fx',2,'610300',106.5252120000,33.9124640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610331','610331','太白县','tbx',2,'610300',107.3165330000,34.0592150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610400','610400','咸阳市','xys',1,'610000',108.7051170000,34.3334390000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610401','610401','市辖区','sxq',1,'610400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610402','610402','秦都区','qdq',1,'610400',108.6986360000,34.3298010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610403','610403','杨凌区','ylq',1,'610400',108.0863480000,34.2713500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610404','610404','渭城区','wcq',1,'610400',108.7309570000,34.3368470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610422','610422','三原县','syx',2,'610400',108.9434810000,34.6139960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610423','610423','泾阳县','jyx',2,'610400',108.8378400000,34.5284930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610424','610424','乾县','qx',2,'610400',108.2474060000,34.5272610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610425','610425','礼泉县','lqx',2,'610400',108.4283170000,34.4825830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610426','610426','永寿县','ysx',2,'610400',108.1431290000,34.6926190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610427','610427','彬县','bx',2,'610400',108.0836740000,35.0342330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610428','610428','长武县','cwx',2,'610400',107.7958350000,35.2061220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610429','610429','旬邑县','xyx',2,'610400',108.3372310000,35.1122340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610430','610430','淳化县','chx',2,'610400',108.5811730000,34.7979700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610431','610431','武功县','wgx',2,'610400',108.2128570000,34.2597320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610481','610481','兴平市','xps',2,'610400',108.4884930000,34.2971340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610500','610500','渭南市','wns',1,'610000',109.5028820000,34.4993810000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610501','610501','市辖区','sxq',1,'610500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610502','610502','临渭区','lwq',1,'610500',109.5032990000,34.5012710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610521','610521','华县','hx',2,'610500',109.7614100000,34.5119580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610522','610522','潼关县','tgx',2,'610500',110.2472600000,34.5445150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610523','610523','大荔县','dlx',2,'610500',109.9482700000,34.7950810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610524','610524','合阳县','hyx',2,'610500',110.1479790000,35.2370980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610525','610525','澄城县','ccx',2,'610500',109.9376090000,35.1840000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610526','610526','蒲城县','pcx',2,'610500',109.5896530000,34.9560340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610527','610527','白水县','bsx',2,'610500',109.5951880000,35.1766810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610528','610528','富平县','fpx',2,'610500',109.1871740000,34.7466790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610581','610581','韩城市','hcs',2,'610500',110.4523910000,35.4752380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610582','610582','华阴市','hys',2,'610500',110.0887800000,34.5617470000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610600','610600','延安市','yas',1,'610000',109.4908100000,36.5965370000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610601','610601','市辖区','sxq',1,'610600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610602','610602','宝塔区','btq',1,'610600',109.4906900000,36.5962910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610621','610621','延长县','ycx',2,'610600',110.0129610000,36.5783060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610622','610622','延川县','ycx',2,'610600',110.1903140000,36.8820660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610623','610623','子长县','zcx',2,'610600',109.6759680000,37.1420700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610624','610624','安塞县','asx',2,'610600',109.3253410000,36.8644100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610625','610625','志丹县','zdx',2,'610600',108.7688980000,36.8230310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610626','610626','吴旗县','wqx',2,'610600',108.1769760000,36.9248520000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610627','610627','甘泉县','gqx',2,'610600',109.3496100000,36.2777290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610628','610628','富县','fx',2,'610600',109.3841360000,35.9964950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610629','610629','洛川县','lcx',2,'610600',109.4357120000,35.7621330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610630','610630','宜川县','ycx',2,'610600',110.1755370000,36.0503910000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610631','610631','黄龙县','hlx',2,'610600',109.8350200000,35.5832760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610632','610632','黄陵县','hlx',2,'610600',109.2624690000,35.5801650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610700','610700','汉中市','hzs',1,'610000',107.0286210000,33.0776680000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610701','610701','市辖区','sxq',1,'610700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610702','610702','汉台区','htq',1,'610700',107.0282330000,33.0776740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610721','610721','南郑县','nzx',2,'610700',106.9423930000,33.0033410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610722','610722','城固县','cgx',2,'610700',107.3298870000,33.1530980000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610723','610723','洋县','yx',2,'610700',107.5499620000,33.2232830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610724','610724','西乡县','xxx',2,'610700',107.7658580000,32.9879610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610725','610725','勉县','mx',2,'610700',106.6801750000,33.1556180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610726','610726','宁强县','nqx',2,'610700',106.2573900000,32.8308060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610727','610727','略阳县','lyx',2,'610700',106.1538990000,33.3296380000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610728','610728','镇巴县','zbx',2,'610700',107.8953100000,32.5358540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610729','610729','留坝县','lbx',2,'610700',106.9243770000,33.6133400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610730','610730','佛坪县','fpx',2,'610700',107.9885820000,33.5207450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610800','610800','榆林市','yls',1,'610000',109.7411930000,38.2901620000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610801','610801','市辖区','sxq',1,'610800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610802','610802','榆阳区','yyq',1,'610800',109.7479100000,38.2992670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610821','610821','神木县','smx',2,'610800',110.4970050000,38.8356410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610822','610822','府谷县','fgx',2,'610800',111.0696450000,39.0292430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610823','610823','横山县','hsx',2,'610800',109.2925960000,37.9640480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610824','610824','靖边县','jbx',2,'610800',108.8056700000,37.5960840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610825','610825','定边县','dbx',2,'610800',107.6012840000,37.5952300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610826','610826','绥德县','sdx',2,'610800',110.2653770000,37.5077010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610827','610827','米脂县','mzx',2,'610800',110.1786830000,37.7590810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610828','610828','佳县','jx',2,'610800',110.4933670000,38.0215970000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610829','610829','吴堡县','wbx',2,'610800',110.7393150000,37.4519250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610830','610830','清涧县','qjx',2,'610800',110.1214600000,37.0877020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610831','610831','子洲县','zzx',2,'610800',110.0345700000,37.6115730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610900','610900','安康市','aks',1,'610000',109.0292730000,32.6903000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610901','610901','市辖区','sxq',1,'610900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610902','610902','汉滨区','hbq',1,'610900',109.0290980000,32.6908170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610921','610921','汉阴县','hyx',2,'610900',108.5109460000,32.8911210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610922','610922','石泉县','sqx',2,'610900',108.2505120000,33.0385120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610923','610923','宁陕县','nsx',2,'610900',108.3137140000,33.3121840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610924','610924','紫阳县','zyx',2,'610900',108.5377880000,32.5201760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610925','610925','岚皋县','lgx',2,'610900',108.9006630000,32.3106900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610926','610926','平利县','plx',2,'610900',109.3618650000,32.3879330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610927','610927','镇坪县','zpx',2,'610900',109.5264370000,31.8833950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610928','610928','旬阳县','xyx',2,'610900',109.3681490000,32.8335670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('610929','610929','白河县','bhx',2,'610900',110.1141860000,32.8094840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611000','611000','商洛市','sls',1,'610000',109.9397760000,33.8683190000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611001','611001','市辖区','sxq',1,'611000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611002','611002','商州区','szq',1,'611000',109.9376850000,33.8692080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611021','611021','洛南县','lnx',2,'611000',110.1457160000,34.0885020000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611022','611022','丹凤县','dfx',2,'611000',110.3319100000,33.6947110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611023','611023','商南县','snx',2,'611000',110.8854370000,33.5263670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611024','611024','山阳县','syx',2,'611000',109.8804350000,33.5304110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611025','611025','镇安县','zax',2,'611000',109.1510750000,33.4239810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('611026','611026','柞水县','zsx',2,'611000',109.1112490000,33.6827730000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620000','620000','甘肃省','gss',0,'0',103.8235570000,36.0580390000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620100','620100','兰州市','lzs',1,'620000',103.8235570000,36.0580390000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620101','620101','市辖区','sxq',1,'620100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620102','620102','城关区','cgq',1,'620100',103.8037740000,36.0712140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620103','620103','七里河区','qlhq',1,'620100',103.7640990000,36.0526320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620104','620104','西固区','xgq',1,'620100',103.6058170000,36.1006860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620105','620105','安宁区','anq',1,'620100',103.7499900000,36.0956350000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620111','620111','红古区','hgq',1,'620100',102.8634020000,36.3439320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620121','620121','永登县','ydx',2,'620100',103.2622030000,36.7344280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620122','620122','皋兰县','glx',2,'620100',103.9484760000,36.3282130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620123','620123','榆中县','yzx',2,'620100',104.1143800000,35.8462030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620200','620200','嘉峪关市','jygs',1,'620000',98.2773040000,39.7865290000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620201','620201','市辖区','sxq',1,'620200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620300','620300','金昌市','jcs',1,'620000',102.1878880000,38.5142380000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620301','620301','市辖区','sxq',1,'620300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620302','620302','金川区','jcq',1,'620300',102.1876830000,38.5137930000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620321','620321','永昌县','ycx',2,'620300',101.9719570000,38.2473540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620400','620400','白银市','bys',1,'620000',104.1736060000,36.5456800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620401','620401','市辖区','sxq',1,'620400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620402','620402','白银区','byq',1,'620400',104.1742500000,36.5456490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620403','620403','平川区','pcq',1,'620400',104.8192070000,36.7292100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620421','620421','靖远县','jyx',2,'620400',104.6869720000,36.5614240000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620422','620422','会宁县','hnx',2,'620400',105.0543370000,35.6924860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620423','620423','景泰县','jtx',2,'620400',104.0663940000,37.1935190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620500','620500','天水市','tss',1,'620000',105.7249980000,34.5785290000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620501','620501','市辖区','sxq',1,'620500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620502','620502','秦城区','qcq',1,'620500',105.7244770000,34.5786450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620503','620503','北道区','bdq',1,'620500',105.8976310000,34.5635040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620521','620521','清水县','qsx',2,'620500',106.1398780000,34.7528700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620522','620522','秦安县','qax',2,'620500',105.6733000000,34.8623540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620523','620523','甘谷县','ggx',2,'620500',105.3323470000,34.7473270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620524','620524','武山县','wsx',2,'620500',104.8916960000,34.7219550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620525','620525','张家川回族自治县','zjchzzzx',2,'620500',106.2124160000,34.9932370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620600','620600','武威市','wws',1,'620000',102.6346970000,37.9299960000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620601','620601','市辖区','sxq',1,'620600',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620602','620602','凉州区','lzq',1,'620600',102.6344920000,37.9302500000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620621','620621','民勤县','mqx',2,'620600',103.0906540000,38.6246210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620622','620622','古浪县','glx',2,'620600',102.8980470000,37.4705710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620623','620623','天祝藏族自治县','tzczzzx',2,'620600',103.1420340000,36.9716780000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620700','620700','张掖市','zys',1,'620000',100.4554720000,38.9328970000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620701','620701','市辖区','sxq',1,'620700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620702','620702','甘州区','gzq',1,'620700',100.4548620000,38.9317740000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620721','620721','肃南裕固族自治县','snygzzzx',2,'620700',99.6170860000,38.8372690000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620722','620722','民乐县','mlx',2,'620700',100.8166230000,38.4344540000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620723','620723','临泽县','lzx',2,'620700',100.1663330000,39.1521510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620724','620724','高台县','gtx',2,'620700',99.8166500000,39.3763080000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620725','620725','山丹县','sdx',2,'620700',101.0884420000,38.7848390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620800','620800','平凉市','pls',1,'620000',106.6846910000,35.5427900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620801','620801','市辖区','sxq',1,'620800',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620802','620802','崆峒区','ktq',1,'620800',106.6842230000,35.5417300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620821','620821','泾川县','jcx',2,'620800',107.3652180000,35.3352830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620822','620822','灵台县','ltx',2,'620800',107.6205870000,35.0640090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620823','620823','崇信县','cxx',2,'620800',107.0312530000,35.3045330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620824','620824','华亭县','htx',2,'620800',106.6493080000,35.2153420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620825','620825','庄浪县','zlx',2,'620800',106.0419790000,35.2034280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620826','620826','静宁县','jnx',2,'620800',105.7334890000,35.5252430000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620900','620900','酒泉市','jqs',1,'620000',98.5107950000,39.7440230000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620901','620901','市辖区','sxq',1,'620900',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620902','620902','肃州区','szq',1,'620900',98.5111550000,39.7438580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620921','620921','金塔县','jtx',2,'620900',98.9029590000,39.9830360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620922','620922','安西县','axx',2,'620900',95.7805910000,40.5165250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620923','620923','肃北蒙古族自治县','sbmgzzzx',2,'620900',94.8772800000,39.5122400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620924','620924','阿克塞哈萨克族自治县','akshskzzzx',2,'620900',94.3376420000,39.6316420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620981','620981','玉门市','yms',2,'620900',97.0372060000,40.2868200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('620982','620982','敦煌市','dhs',2,'620900',94.6642790000,40.1411190000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621000','621000','庆阳市','qys',1,'620000',107.6383720000,35.7342180000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621001','621001','市辖区','sxq',1,'621000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621002','621002','西峰区','xfq',1,'621000',107.6388240000,35.7337130000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621021','621021','庆城县','qcx',2,'621000',107.8856640000,36.0135040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621022','621022','环县','hx',2,'621000',107.3087540000,36.5693220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621023','621023','华池县','hcx',2,'621000',107.9862880000,36.4573040000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621024','621024','合水县','hsx',2,'621000',108.0198650000,35.8190050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621025','621025','正宁县','znx',2,'621000',108.3610680000,35.4906420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621026','621026','宁县','nx',2,'621000',107.9211820000,35.5020100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621027','621027','镇原县','zyx',2,'621000',107.1957060000,35.6778060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621100','621100','定西市','dxs',1,'620000',104.6262940000,35.5795780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621101','621101','市辖区','sxq',1,'621100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621102','621102','安定区','adq',1,'621100',104.6257700000,35.5797640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621121','621121','通渭县','twx',2,'621100',105.2501020000,35.2089220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621122','621122','陇西县','lxx',2,'621100',104.6375540000,35.0034090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621123','621123','渭源县','wyx',2,'621100',104.2117420000,35.1330230000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621124','621124','临洮县','ltx',2,'621100',103.8621860000,35.3762330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621125','621125','漳县','zx',2,'621100',104.4667560000,34.8486420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621126','621126','岷县','mx',2,'621100',104.0398820000,34.4391050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621200','621200','陇南市','lns',1,'620000',104.9293790000,33.3885980000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621201','621201','市辖区','sxq',1,'621200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621202','621202','武都区','wdq',1,'621200',104.9298660000,33.3881550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621221','621221','成县','cx',2,'621200',105.7344340000,33.7398630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621222','621222','文县','wx',2,'621200',104.6824480000,32.9421710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621223','621223','宕昌县','dcx',2,'621200',104.3944750000,34.0426550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621224','621224','康县','kx',2,'621200',105.6095340000,33.3282660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621225','621225','西和县','xhx',2,'621200',105.2997370000,34.0137180000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621226','621226','礼县','lx',2,'621200',105.1816160000,34.1893870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621227','621227','徽县','hx',2,'621200',106.0856320000,33.7677850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('621228','621228','两当县','ldx',2,'621200',106.3069590000,33.9107290000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622900','622900','临夏回族自治州','lxhzzzz',1,'620000',103.2120060000,35.5994460000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622901','622901','临夏市','lxs',2,'622900',103.2116340000,35.5994100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622921','622921','临夏县','lxx',2,'622900',102.9938730000,35.4923600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622922','622922','康乐县','klx',2,'622900',103.7098520000,35.3719060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622923','622923','永靖县','yjx',2,'622900',103.3198710000,35.9389330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622924','622924','广河县','ghx',2,'622900',103.5761880000,35.4816880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622925','622925','和政县','hzx',2,'622900',103.3503570000,35.4259710000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622926','622926','东乡族自治县','dxzzzx',2,'622900',103.3895680000,35.6638300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('622927','622927','积石山保安族东乡族撒拉族自治县','jssbazdxzslzzzx',2,'622900',102.8774730000,35.7129060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623000','623000','甘南藏族自治州','gnczzzz',1,'620000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623001','623001','合作市','hzs',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623021','623021','临潭县','ltx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623022','623022','卓尼县','znx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623023','623023','舟曲县','zqx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623024','623024','迭部县','dbx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623025','623025','玛曲县','mqx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623026','623026','碌曲县','lqx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('623027','623027','夏河县','xhx',2,'623000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630000','630000','青海省','qhs',0,'0',101.7789160000,36.6231780000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630100','630100','西宁市','xns',1,'630000',101.7789160000,36.6231780000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630101','630101','市辖区','sxq',1,'630100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630102','630102','城东区','cdq',1,'630100',101.8330320000,36.5905750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630103','630103','城中区','czq',1,'630100',101.7800120000,36.6257030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630104','630104','城西区','cxq',1,'630100',101.7614800000,36.6265650000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630105','630105','城北区','cbq',1,'630100',101.6682340000,36.6623900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630121','630121','大通回族土族自治县','dthztzzzx',2,'630100',101.6851100000,36.9326370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630122','630122','湟中县','hzx',2,'630100',101.5746480000,36.4975000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('630123','630123','湟源县','hyx',2,'630100',101.2669260000,36.6850060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632100','632100','海东地区','hddq',1,'630000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632121','632121','平安县','pax',2,'632100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632122','632122','民和回族土族自治县','mhhztzzzx',2,'632100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632123','632123','乐都县','ldx',2,'632100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632126','632126','互助土族自治县','hztzzzx',2,'632100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632127','632127','化隆回族自治县','hlhzzzx',2,'632100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632128','632128','循化撒拉族自治县','xhslzzzx',2,'632100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632200','632200','海北藏族自治州','hbczzzz',1,'630000',100.9010590000,36.9594350000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632221','632221','门源回族自治县','myhzzzx',2,'632200',101.6184610000,37.3766270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632222','632222','祁连县','qlx',2,'632200',100.2497780000,38.1754090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632223','632223','海晏县','hyx',2,'632200',100.9004900000,36.9595420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632224','632224','刚察县','gcx',2,'632200',100.1384170000,37.3262630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632300','632300','黄南藏族自治州','hnczzzz',1,'630000',102.0199880000,35.5177440000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632321','632321','同仁县','trx',2,'632300',102.0176040000,35.5163370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632322','632322','尖扎县','jzx',2,'632300',102.0319530000,35.9382050000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632323','632323','泽库县','zkx',2,'632300',101.4693430000,35.0368420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632324','632324','河南蒙古族自治县','hnmgzzzx',2,'632300',101.6118770000,34.7345220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632500','632500','海南藏族自治州','hnczzzz',1,'630000',100.6195420000,36.2803530000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632521','632521','共和县','ghx',2,'632500',100.6195970000,36.2802860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632522','632522','同德县','tdx',2,'632500',100.5794650000,35.2544920000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632523','632523','贵德县','gdx',2,'632500',101.4318560000,36.0404560000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632524','632524','兴海县','xhx',2,'632500',99.9869630000,35.5890900000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632525','632525','贵南县','gnx',2,'632500',100.7479200000,35.5870850000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632600','632600','果洛藏族自治州','glczzzz',1,'630000',100.2421430000,34.4736000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632621','632621','玛沁县','mqx',2,'632600',100.2435310000,34.4733860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632622','632622','班玛县','bmx',2,'632600',100.7379550000,32.9315890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632623','632623','甘德县','gdx',2,'632600',99.9025890000,33.9669870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632624','632624','达日县','drx',2,'632600',99.6517150000,33.7532590000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632625','632625','久治县','jzx',2,'632600',101.4848840000,33.4302170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632626','632626','玛多县','mdx',2,'632600',98.2113430000,34.9152800000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632700','632700','玉树藏族自治州','ysczzzz',1,'630000',97.0085220000,33.0040490000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632721','632721','玉树县','ysx',2,'632700',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632722','632722','杂多县','zdx',2,'632700',95.2934230000,32.8918860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632723','632723','称多县','cdx',2,'632700',97.1108930000,33.3678840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632724','632724','治多县','zdx',2,'632700',95.6168430000,33.8523220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632725','632725','囊谦县','nqx',2,'632700',96.4797970000,32.2032060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632726','632726','曲麻莱县','qmlx',2,'632700',95.8006740000,34.1265400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632800','632800','海西蒙古族藏族自治州','hxmgzczzzz',1,'630000',97.3707850000,37.3746630000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632801','632801','格尔木市','gems',2,'632800',94.9057770000,36.4015410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632802','632802','德令哈市','dlhs',2,'632800',97.3701430000,37.3745550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632821','632821','乌兰县','wlx',2,'632800',98.4798520000,36.9303890000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632822','632822','都兰县','dlx',2,'632800',98.0891610000,36.2985530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('632823','632823','天峻县','tjx',2,'632800',99.0207800000,37.2990600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640000','640000','宁夏','nxhzzzq',0,'0',106.2781790000,38.4663700000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640100','640100','银川市','ycs',1,'640000',106.2781790000,38.4663700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640101','640101','市辖区','sxq',1,'640100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640104','640104','兴庆区','xqq',1,'640100',106.2853640000,38.4605700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640105','640105','西夏区','xxq',1,'640100',106.1248430000,38.4965110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640106','640106','金凤区','jfq',1,'640100',106.1868930000,38.4851960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640121','640121','永宁县','ynx',2,'640100',106.2524310000,38.2771460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640122','640122','贺兰县','hlx',2,'640100',106.3459040000,38.5545630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640181','640181','灵武市','lws',2,'640100',106.3347010000,38.0940580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640200','640200','石嘴山市','szss',1,'640000',106.3761730000,39.0133300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640201','640201','市辖区','sxq',1,'640200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640202','640202','大武口区','dwkq',1,'640200',106.3766510000,39.0141580000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640205','640205','惠农区','hnq',1,'640200',106.7755130000,39.2300940000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640221','640221','平罗县','plx',2,'640200',106.5448900000,38.9067400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640300','640300','吴忠市','wzs',1,'640000',106.1994090000,37.9861650000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640301','640301','市辖区','sxq',1,'640300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640302','640302','利通区','ltq',1,'640300',106.1994190000,37.9859670000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640323','640323','盐池县','ycx',2,'640300',107.4054100000,37.7842220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640324','640324','同心县','txx',2,'640300',105.9147640000,36.9829000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640381','640381','青铜峡市','qtxs',2,'640300',106.0753950000,38.0215090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640400','640400','固原市','gys',1,'640000',106.2852410000,36.0045610000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640401','640401','市辖区','sxq',1,'640400',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640402','640402','原州区','yzq',1,'640400',106.2847700000,36.0053370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640422','640422','西吉县','xjx',2,'640400',105.7318010000,35.9653840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640423','640423','隆德县','ldx',2,'640400',106.1234400000,35.6182340000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640424','640424','泾源县','jyx',2,'640400',106.3386740000,35.4934400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640425','640425','彭阳县','pyx',2,'640400',106.6415120000,35.8499750000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640500','640500','中卫市','zws',1,'640000',105.1895680000,37.5149510000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640501','640501','市辖区','sxq',1,'640500',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640502','640502','沙坡头区','sptq',1,'640500',105.1905360000,37.5145640000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640521','640521','中宁县','znx',2,'640500',105.6757840000,37.4897360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('640522','640522','海原县','hyx',2,'640500',105.6473230000,36.5620070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650000','650000','新疆','xjwwezzq',0,'0',87.6177330000,43.7928180000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650100','650100','乌鲁木齐市','wlmqs',1,'650000',87.6177330000,43.7928180000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650101','650101','市辖区','sxq',1,'650100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650102','650102','天山区','tsq',1,'650100',87.6257830000,43.7680000000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650103','650103','沙依巴克区','sybkq',1,'650100',87.5941320000,43.7879660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650104','650104','新市区','xsq',1,'650100',87.5085050000,43.8908680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650105','650105','水磨沟区','smgq',1,'650100',87.6187690000,43.8084200000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650106','650106','头屯河区','tthq',1,'650100',87.4170950000,43.8814010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650107','650107','达坂城区','dbcq',1,'650100',88.3099400000,43.3618100000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650108','650108','东山区','dsq',1,'650100',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650121','650121','乌鲁木齐县','wlmqx',2,'650100',87.5056030000,43.9825460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650200','650200','克拉玛依市','klmys',1,'650000',84.8739460000,45.5958860000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650201','650201','市辖区','sxq',1,'650200',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650202','650202','独山子区','dszq',1,'650200',84.8822670000,44.3272070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650203','650203','克拉玛依区','klmyq',1,'650200',84.8689180000,45.6004770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650204','650204','白碱滩区','bjtq',1,'650200',85.1298820000,45.6890210000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('650205','650205','乌尔禾区','wehq',1,'650200',85.6977670000,46.0877600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652100','652100','吐鲁番地区','tlfdq',1,'650000',89.1840780000,42.9476130000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652101','652101','吐鲁番市','tlfs',2,'652100',89.1823240000,42.9476270000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652122','652122','鄯善县','ssx',2,'652100',90.2126920000,42.8655030000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652123','652123','托克逊县','tkxx',2,'652100',88.6557710000,42.7935360000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652200','652200','哈密地区','hmdq',1,'650000',93.5131600000,42.8332480000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652201','652201','哈密市','hms',2,'652200',93.5091740000,42.8338880000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652222','652222','巴里坤哈萨克自治县','blkhskzzx',2,'652200',93.0217950000,43.5990320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652223','652223','伊吾县','ywx',2,'652200',94.6927730000,43.2520120000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652300','652300','昌吉回族自治州','cjhzzzz',1,'650000',87.3040120000,44.0145770000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652301','652301','昌吉市','cjs',2,'652300',87.3041120000,44.0131830000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652302','652302','阜康市','fks',2,'652300',87.9838400000,44.1521530000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652303','652303','米泉市','mqs',2,'652300',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652323','652323','呼图壁县','htbx',2,'652300',86.8886130000,44.1893420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652324','652324','玛纳斯县','mnsx',2,'652300',86.2176870000,44.3056250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652325','652325','奇台县','qtx',2,'652300',89.5914370000,44.0219960000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652327','652327','吉木萨尔县','jmsex',2,'652300',89.1812880000,43.9971620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652328','652328','木垒哈萨克自治县','mlhskzzx',2,'652300',90.2828330000,43.8324420000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652700','652700','博尔塔拉蒙古自治州','betlmgzzz',1,'650000',82.0747780000,44.9032580000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652701','652701','博乐市','bls',2,'652700',82.0722370000,44.9030870000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652722','652722','精河县','jhx',2,'652700',82.8929380000,44.6056450000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652723','652723','温泉县','wqx',2,'652700',81.0309900000,44.9737510000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652800','652800','巴音郭楞蒙古自治州','byglmgzzz',1,'650000',86.1509690000,41.7685520000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652801','652801','库尔勒市','kels',2,'652800',86.1459480000,41.7631220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652822','652822','轮台县','ltx',2,'652800',84.2485420000,41.7812660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652823','652823','尉犁县','ylx',2,'652800',86.2634120000,41.3374280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652824','652824','若羌县','rqx',2,'652800',88.1688070000,39.0238070000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652825','652825','且末县','qmx',2,'652800',85.5326290000,38.1385620000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652826','652826','焉耆回族自治县','yqhzzzx',2,'652800',86.5698000000,42.0643490000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652827','652827','和静县','hjx',2,'652800',86.3910670000,42.3171600000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652828','652828','和硕县','hsx',2,'652800',86.8649470000,42.2688630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652829','652829','博湖县','bhx',2,'652800',86.6315760000,41.9801660000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652900','652900','阿克苏地区','aksdq',1,'650000',80.2650680000,41.1707120000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652901','652901','阿克苏市','akss',2,'652900',80.2629000000,41.1712720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652922','652922','温宿县','wsx',2,'652900',80.2432730000,41.2729950000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652923','652923','库车县','kcx',2,'652900',82.9630400000,41.7171410000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652924','652924','沙雅县','syx',2,'652900',82.7807700000,41.2262680000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652925','652925','新和县','xhx',2,'652900',82.6108280000,41.5511760000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652926','652926','拜城县','bcx',2,'652900',81.8698810000,41.7961010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652927','652927','乌什县','wsx',2,'652900',79.2308050000,41.2158700000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652928','652928','阿瓦提县','awtx',2,'652900',80.3784260000,40.6384220000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('652929','652929','柯坪县','kpx',2,'652900',79.0478500000,40.5062400000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653000','653000','克孜勒苏柯尔克孜自治州','kzlskekzzzz',1,'650000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653001','653001','阿图什市','atss',2,'653000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653022','653022','阿克陶县','aktx',2,'653000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653023','653023','阿合奇县','ahqx',2,'653000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653024','653024','乌恰县','wqx',2,'653000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653100','653100','喀什地区','ksdq',1,'650000',75.9891380000,39.4676640000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653101','653101','喀什市','kss',2,'653100',75.9883800000,39.4678610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653121','653121','疏附县','sfx',2,'653100',75.8630750000,39.3783060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653122','653122','疏勒县','slx',2,'653100',76.0536530000,39.3994610000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653123','653123','英吉沙县','yjsx',2,'653100',76.1742920000,38.9298390000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653124','653124','泽普县','zpx',2,'653100',77.2735930000,38.1912170000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653125','653125','莎车县','scx',2,'653100',77.2488840000,38.4144990000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653126','653126','叶城县','ycx',2,'653100',77.4203530000,37.8846790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653127','653127','麦盖提县','mgtx',2,'653100',77.6515380000,38.9033840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653128','653128','岳普湖县','yphx',2,'653100',76.7724000000,39.2352480000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653129','653129','伽师县','jsx',2,'653100',76.7419820000,39.4943250000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653130','653130','巴楚县','bcx',2,'653100',78.5504100000,39.7834790000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653131','653131','塔什库尔干塔吉克自治县','tskegtjkzzx',2,'653100',75.2280680000,37.7754370000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653200','653200','和田地区','htdq',1,'650000',79.9253300000,37.1106870000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653201','653201','和田市','hts',2,'653200',79.9275420000,37.1089440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653221','653221','和田县','htx',2,'653200',79.8190700000,37.1200310000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653222','653222','墨玉县','myx',2,'653200',79.7366290000,37.2715110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653223','653223','皮山县','psx',2,'653200',78.2823010000,37.6163320000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653224','653224','洛浦县','lpx',2,'653200',80.1840380000,37.0743770000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653225','653225','策勒县','clx',2,'653200',80.8035720000,37.0016720000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653226','653226','于田县','ytx',2,'653200',81.6678450000,36.8546280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('653227','653227','民丰县','mfx',2,'653200',82.6923540000,37.0649090000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654000','654000','伊犁哈萨克自治州','ylhskzzz',1,'650000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654002','654002','伊宁市','yns',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654003','654003','奎屯市','kts',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654021','654021','伊宁县','ynx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654022','654022','察布查尔锡伯自治县','cbcexbzzx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654023','654023','霍城县','hcx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654024','654024','巩留县','glx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654025','654025','新源县','xyx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654026','654026','昭苏县','zsx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654027','654027','特克斯县','tksx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654028','654028','尼勒克县','nlkx',2,'654000',null,null,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654200','654200','塔城地区','tcdq',1,'650000',82.9857320000,46.7463010000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654201','654201','塔城市','tcs',2,'654200',82.9839880000,46.7462810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654202','654202','乌苏市','wss',2,'654200',84.6776240000,44.4301150000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654221','654221','额敏县','emx',2,'654200',83.6221180000,46.5225550000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654223','654223','沙湾县','swx',2,'654200',85.6225080000,44.3295440000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654224','654224','托里县','tlx',2,'654200',83.6046900000,45.9358630000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654225','654225','裕民县','ymx',2,'654200',82.9821570000,46.2027810000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654226','654226','和布克赛尔蒙古自治县','hbksemgzzx',2,'654200',85.7335510000,46.7930010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654300','654300','阿勒泰地区','altdq',1,'650000',88.1396300000,47.8483930000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654301','654301','阿勒泰市','alts',2,'654300',88.1387430000,47.8489110000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654321','654321','布尔津县','bejx',2,'654300',86.8618600000,47.7045300000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654322','654322','富蕴县','fyx',2,'654300',89.5249930000,46.9931060000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654323','654323','福海县','fhx',2,'654300',87.4945690000,47.1131280000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654324','654324','哈巴河县','hbhx',2,'654300',86.4189640000,48.0592840000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654325','654325','青河县','qhx',2,'654300',90.3815610000,46.6724460000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('654326','654326','吉木乃县','jmnx',2,'654300',85.8760640000,47.4346330000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('659000','659000','省直辖行政单位','szxxzdw',1,'650000',null,null,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('659001','659001','石河子市','shzs',2,'659000',86.0410750000,44.3058860000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('659002','659002','阿拉尔市','ales',2,'659000',81.2858840000,40.5419140000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('659003','659003','图木舒克市','tmsks',2,'659000',79.0779780000,39.8673160000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('659004','659004','五家渠市','wjqs',2,'659000',87.5268840000,44.1674010000,3);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710000','710000','台湾省','tws',0,'0',null,null,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710100','710100','澎湖县','澎湖县',2,'710000',119.5834400000,23.5589500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710200','710200','彰化县','彰化县',2,'710000',120.5474900000,24.0677800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710300','710300','宜兰县','宜兰县',2,'710000',121.7669700000,24.7231300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710400','710400','台东县','台东县',2,'710000',121.1500200000,22.7537000000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710500','710500','桃园县','桃园县',2,'710000',121.3013400000,24.9925200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710600','710600','屏东县','屏东县',2,'710000',120.4880000000,22.6824500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710700','710700','花莲县','花莲县',2,'710000',121.6199400000,23.9910400000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710800','710800','基隆市','基隆市',2,'710000',121.7445500000,25.1316700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('710900','710900','苗栗县','苗栗县',2,'710000',120.8207100000,24.5645300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711000','711000','云林县','云林县',2,'710000',120.5263500000,23.6989200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711100','711100','新北市','新北市',2,'710000',121.4657200000,25.0119600000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711200','711200','台北市','台北市',2,'710000',121.5639800000,25.0373200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711300','711300','南投县','南投县',2,'710000',120.6902600000,23.9025200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711400','711400','台南县','台南县',2,'710000',120.1847800000,22.9896300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711500','711500','嘉义县','嘉义县',2,'710000',120.2930200000,23.4589300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711600','711600','台中市','台中市',2,'710000',120.6785300000,24.1385200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('711700','711700','高雄市','高雄市',2,'710000',120.3121300000,22.6202300000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810000','810000','香港','xgtbxzq',0,'0',114.1733550000,22.3200480000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810100','810100','荃湾区','荃湾区',2,'810000',114.1128400000,22.3816700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810200','810200','湾仔区','湾仔区',2,'810000',114.1736600000,22.2771200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810300','810300','东区','东区',2,'810000',114.2074300000,22.2906800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810400','810400','南区','南区',2,'810000',114.1402100000,22.2495200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810500','810500','油尖旺区','油尖旺区',2,'810000',114.1671800000,22.3029900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810600','810600','深水埗区','深水埗区',2,'810000',114.1627300000,22.3304700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810700','810700','九龙城区','九龙城区',2,'810000',114.1900400000,22.3216100000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810800','810800','中西区','中西区',2,'810000',114.1596700000,22.2786100000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('810900','810900','观塘区','观塘区',2,'810000',114.2264400000,22.3120900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811000','811000','离岛区','离岛区',2,'810000',113.9050100000,22.2538900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811100','811100','屯门区','屯门区',2,'810000',113.9762800000,22.3903200000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811200','811200','元朗区','元朗区',2,'810000',114.0267500000,22.4451700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811300','811300','北区','北区',2,'810000',114.1349500000,22.4959600000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811400','811400','大埔区','大埔区',2,'810000',114.1660400000,22.4463900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811500','811500','西贡区','西贡区',2,'810000',114.2710900000,22.3822800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811600','811600','沙田区','沙田区',2,'810000',114.1876900000,22.3826500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811700','811700','葵青区','葵青区',2,'810000',114.0960500000,22.3476900000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('811800','811800','黄大仙区','黄大仙区',2,'810000',114.1945100000,22.3415800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820000','820000','澳门','amtbxzq',0,'0',113.5490900000,22.1989510000,1);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820100','820100','花地玛堂区','花地玛堂区',2,'820000',113.5530100000,22.2098700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820200','820200','圣安多尼堂区','圣安多尼堂区',2,'820000',113.5399300000,22.1990700000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820300','820300','大堂区','大堂区',2,'820000',113.5495500000,22.1897500000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820400','820400','望德堂区','望德堂区',2,'820000',113.5485600000,22.1992800000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820500','820500','风顺堂区','风顺堂区',2,'820000',113.5372100000,22.1907100000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820600','820600','嘉模堂区','嘉模堂区',2,'820000',113.5553800000,22.1581400000,2);
insert into `sys_area`(`id`,`city_code`,`name`,`en_name`,`division`,`parent_id`,`longitude`,`latitude`,`level`)
values('820700','820700','圣方济各堂区','圣方济各堂区',2,'820000',113.5697600000,22.1228700000,2);
