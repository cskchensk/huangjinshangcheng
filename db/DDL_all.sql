﻿
create database `ugold` default character set utf8mb4 collate utf8mb4_general_ci;

use ugold;

SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS  `activity_card`;
CREATE TABLE `activity_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_no` varchar(64) DEFAULT NULL COMMENT '卡号',
  `password` varchar(64) NOT NULL COMMENT '兑换码（密码）',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '是否被兑换：0-未兑换；1-已兑换',
  `gift_id` int(10) NOT NULL COMMENT '礼品id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='热门权益礼品卡券';

DROP TABLE IF EXISTS  `activity_gift`;
CREATE TABLE `activity_gift` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '礼品名称',
  `type` int(10) NOT NULL DEFAULT '1' COMMENT '礼品类型：1-优惠礼券；2-热门权益；3-精品实物；',
  `init_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '初始礼品数量',
  `balance` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '剩余礼品数量',
  `coupon_type` int(10) NOT NULL DEFAULT '0' COMMENT '优惠券类型：0-黄金红包；1-回租福利券；2-商城满减券；',
  `coupon_id` int(10) DEFAULT '0' COMMENT '优惠券ID',
  `total_price` decimal(20,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '进货总价（单位元）',
  `price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品单价（单位金豆）',
  `market_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '市场参考价',
  `single_limit` int(10) NOT NULL DEFAULT '0' COMMENT '每人最多可兑换次数',
  `introduction` text NOT NULL COMMENT '礼品介绍',
  `remark` text NOT NULL COMMENT '详细说明',
  `img_url` varchar(64) NOT NULL COMMENT '礼品图片',
  `add_time` varchar(32) NOT NULL COMMENT '创建时间',
  `creator_id` varchar(64) DEFAULT NULL COMMENT '创建人id',
  `creator_name` varchar(32) DEFAULT NULL COMMENT '创建人名称',
  `hot` int(10) NOT NULL DEFAULT '0' COMMENT '推荐： 0-取消推荐；1-推荐',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '状态:0-待审核；1-审核通过（在售）；2-审核不通过；3-下架（审核通过）',
  `auditor_id` varchar(64) DEFAULT NULL COMMENT '审核人id',
  `auditor_name` varchar(32) DEFAULT NULL COMMENT '审核人姓名',
  `reason` varchar(512) DEFAULT NULL COMMENT '下架原因',
  `shelf_time` varchar(32) DEFAULT NULL COMMENT '上架时间(审核时间)',
  `audit_remark` varchar(512) DEFAULT NULL COMMENT '审核备注',
  `unshelf_time` varchar(32) DEFAULT NULL COMMENT '下架时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='金豆兑换礼品管理';

DROP TABLE IF EXISTS  `activity_gift_record`;
CREATE TABLE `activity_gift_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(32) NOT NULL COMMENT '流水号',
  `gift_id` int(10) NOT NULL COMMENT '礼品id',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `card_id` int(10) DEFAULT '0' COMMENT '热门权益卡券id',
  `add_time` varchar(32) NOT NULL COMMENT '下单时间',
  `pay_beans` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付金豆',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_no` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8mb4 COMMENT='金豆兑换礼品记录表';

DROP TABLE IF EXISTS  `activity_info`;
CREATE TABLE `activity_info` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `name` varchar(128) NOT NULL COMMENT '活动名称',
  `start_time` datetime DEFAULT NULL COMMENT '活动开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '活动结束时间',
  `group` int(10) NOT NULL DEFAULT '1' COMMENT '适用用户群（1：全部用户；2：安卓用户；3：iOS用户）',
  `remark` varchar(1024) DEFAULT NULL COMMENT '活动描述',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='所有活动表';

DROP TABLE IF EXISTS  `activity_invitation`;
CREATE TABLE `activity_invitation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `name` varchar(256) NOT NULL COMMENT '活动名称',
  `start_date` varchar(64) NOT NULL COMMENT '开始时间',
  `end_date` varchar(64) NOT NULL COMMENT '结束时间',
  `status` int(10) DEFAULT '1' COMMENT '状态：1：开启；2：关闭',
  `add_time` varchar(64) DEFAULT NULL COMMENT '添加时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除：1：否；2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='邀友活动';

DROP TABLE IF EXISTS  `activity_invitation_strategy`;
CREATE TABLE `activity_invitation_strategy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_invitation_id` int(10) NOT NULL COMMENT '邀友活动ID',
  `mark` int(10) NOT NULL DEFAULT '1' COMMENT '符号：1：小于等于；2：大于',
  `investment_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '投资金额（单位万元）',
  `proportion` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '奖励比例（单位为%）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COMMENT='邀友奖励策略';

DROP TABLE IF EXISTS  `activity_invitation_top`;
CREATE TABLE `activity_invitation_top` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(32) NOT NULL COMMENT '手机号',
  `investment_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '投资金额（单位万元）',
  `activity_invitation_id` int(10) DEFAULT NULL COMMENT '邀友活动的ID',
  `rewards` decimal(10,2) DEFAULT '0.00' COMMENT '奖励金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COMMENT='邀友奖励榜单';

DROP TABLE IF EXISTS  `activity_sign`;
CREATE TABLE `activity_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `sign_date` varchar(25) NOT NULL COMMENT '签到时间',
  `continue_sign` int(10) DEFAULT NULL COMMENT '连续签到天数',
  `total_sign` int(10) DEFAULT '0' COMMENT '累计签到天数',
  `total_gold_bean` int(10) DEFAULT '0' COMMENT '累计获得金豆数',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=659 DEFAULT CHARSET=utf8mb4 COMMENT='签到表';

DROP TABLE IF EXISTS  `activity_sign_history`;
CREATE TABLE `activity_sign_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `sign_date` varchar(25) NOT NULL COMMENT '签到时间',
  `gold_bean` int(10) NOT NULL COMMENT '获得金豆数',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4485 DEFAULT CHARSET=utf8mb4 COMMENT='签到历史表';

DROP TABLE IF EXISTS  `activity_task`;
CREATE TABLE `activity_task` (
  `id` varchar(50) NOT NULL,
  `member_id` varchar(50) NOT NULL COMMENT '会员id',
  `task_type` int(11) NOT NULL COMMENT '任务类型 1:了解柚子金 2:认证绑卡 3:7天活动产品 4:预订稳赢金产品',
  `is_finish` int(11) NOT NULL COMMENT '是否完成 1:未完成 2:已完成',
  `is_receive` int(11) NOT NULL COMMENT '是否领取 1:未领取 2:已领取',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `bonus`;
CREATE TABLE `bonus` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT '唯一标识',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '名称',
  `type` int(10) NOT NULL DEFAULT '1' COMMENT '返利类型（1：加息券;2:红包; 3:现金；4：黄金）',
  `status` int(10) DEFAULT '1' COMMENT '状态（1：禁用；2：启用）',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `bonus_strategy`;
CREATE TABLE `bonus_strategy` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `quota` int(10) NOT NULL COMMENT '投资额度',
  `bonus_id` varchar(64) NOT NULL COMMENT '返利id',
  `coupon_id` varchar(64) DEFAULT '' COMMENT '优惠券id',
  `amount` double(20,2) DEFAULT '0.00' COMMENT '黄金克数或现金数',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `channel`;
CREATE TABLE `channel` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `name` varchar(64) NOT NULL COMMENT '渠道名称',
  `code` varchar(64) DEFAULT NULL COMMENT '渠道编号',
  `username` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `amount` int(10) NOT NULL DEFAULT '0' COMMENT '奖励金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `channel_activation`;
CREATE TABLE `channel_activation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `adid` varchar(32) DEFAULT NULL COMMENT '广告计划ID',
  `cid` varchar(32) DEFAULT NULL COMMENT '广告创意ID',
  `uuid` varchar(64) DEFAULT NULL COMMENT 'idfa/imei',
  `mac` varchar(32) DEFAULT NULL,
  `os` varchar(32) DEFAULT NULL,
  `timestamp` varchar(32) DEFAULT NULL,
  `callback_url` varchar(256) DEFAULT NULL,
  `callback` varchar(256) DEFAULT NULL,
  `status` int(10) DEFAULT '0' COMMENT '是否转化：0-未转化；1-已转化',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_uuid` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=3863 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `channel_share_member`;
CREATE TABLE `channel_share_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `channel_id` varchar(64) NOT NULL COMMENT '渠道主键',
  `member_id` varchar(64) NOT NULL COMMENT '分享人主键',
  `friend_id` varchar(64) NOT NULL COMMENT '好友（被分享人）主键',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='渠道分享好友记录';

DROP TABLE IF EXISTS  `channel_view`;
CREATE TABLE `channel_view` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel_id` varchar(64) NOT NULL COMMENT '渠道ID',
  `ip` varchar(32) NOT NULL COMMENT '访问ip',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `index_ip_channel_id` (`channel_id`,`ip`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=59835 DEFAULT CHARSET=utf8mb4 COMMENT='渠道访问';

DROP TABLE IF EXISTS  `contract`;
CREATE TABLE `contract` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `order_id` varchar(64) NOT NULL COMMENT '订单号',
  `thirdparty_url` varchar(512) DEFAULT NULL COMMENT ' 第三方文件路径',
  `original_url` varchar(128) DEFAULT NULL COMMENT '原始文件路径',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='合同表-存储合同附件信息';

DROP TABLE IF EXISTS  `coupon_channel`;
CREATE TABLE `coupon_channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_no` varchar(64) NOT NULL COMMENT '订单号',
  `mobile` varchar(16) NOT NULL COMMENT '用户手机号',
  `channel_id` varchar(64) DEFAULT NULL COMMENT '渠道ID',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '交易金额',
  `coupon_id` varchar(64) NOT NULL COMMENT '优惠券ID',
  `coupon_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠券额度',
  `add_time` varchar(32) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_no` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=611 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `coupon_donation`;
CREATE TABLE `coupon_donation` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `type` int(10) NOT NULL COMMENT '类型1：红包；2加息券',
  `time_type` int(10) NOT NULL DEFAULT '1' COMMENT '赠送时间：1:立即赠送；2：定时赠送；',
  `donation_time` varchar(32) DEFAULT NULL COMMENT '赠送时间',
  `coupon_id` varchar(64) NOT NULL COMMENT '优惠券的id',
  `target_type` int(10) NOT NULL DEFAULT '1' COMMENT '赠送对象1:全员赠送；2：批次赠送；',
  `mobiles` text COMMENT '赠送手机号',
  `quantity` int(10) NOT NULL DEFAULT '0' COMMENT '每人赠送数量',
  `total_quantity` int(10) DEFAULT NULL COMMENT '赠送人数',
  `reason` varchar(1024) DEFAULT NULL COMMENT '赠送原因',
  `status` int(10) DEFAULT '1' COMMENT '1：待审核；2：审核不通过；3：审核通过；4：已赠送',
  `create_user_id` varchar(64) DEFAULT NULL COMMENT '创建人id',
  `create_user_name` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `audit_user_id` varchar(64) DEFAULT NULL COMMENT '审核人id',
  `audit_user_name` varchar(32) DEFAULT NULL COMMENT '审核人姓名',
  `remark` varchar(1024) DEFAULT NULL COMMENT '审核失败或成功的理由',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `is_sms` int(10) NOT NULL DEFAULT '0' COMMENT '是否发送短信：0-不发送；1-发送',
  `coupon_amount` int(10) NOT NULL DEFAULT '0' COMMENT '优惠券金额或百分率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='优惠券赠送';

DROP TABLE IF EXISTS  `coupon_donation_v2`;
CREATE TABLE `coupon_donation_v2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) NOT NULL DEFAULT '0' COMMENT '类型：0-黄金红包；1-回租福利券；2-商城满减券',
  `time_type` int(10) NOT NULL DEFAULT '1' COMMENT '赠送时间：1:立即赠送；2：定时赠送',
  `donation_time` varchar(32) DEFAULT NULL COMMENT '赠送时间',
  `coupon_ids` varchar(256) NOT NULL COMMENT '优惠券ID（多个用","分割，比如：1,2,3）',
  `coupon_names` varchar(1024) NOT NULL COMMENT '优惠券名称（多个用","分割）',
  `target_type` int(11) NOT NULL DEFAULT '1' COMMENT '赠送对象1:全员赠送；2：批次赠送',
  `mobiles` text COMMENT '赠送手机号',
  `quantity` int(10) NOT NULL DEFAULT '0' COMMENT '每人赠送数量',
  `total_quantity` int(10) NOT NULL DEFAULT '0' COMMENT '赠送人数',
  `reason` varchar(256) DEFAULT NULL COMMENT '赠送原因',
  `status` int(10) DEFAULT '1' COMMENT '状态：1：待审核；2：审核不通过；3：审核通过；4：已赠送',
  `create_user_id` varchar(64) DEFAULT NULL COMMENT '创建人id',
  `create_user_name` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `audit_user_id` varchar(64) DEFAULT NULL COMMENT '审核人id',
  `audit_user_name` varchar(32) DEFAULT NULL COMMENT '审核人姓名',
  `remark` varchar(256) DEFAULT NULL COMMENT '审核失败或成功的理由',
  `add_time` varchar(32) DEFAULT NULL COMMENT '添加时间',
  `is_notice` int(10) DEFAULT '1' COMMENT '是否通知：1-不通知；2-通知',
  `notice_type` varchar(32) DEFAULT NULL COMMENT '(1:短信；2：站内信；3：推送，个多用","分割)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `coupon_member`;
CREATE TABLE `coupon_member` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `member_id` varchar(64) DEFAULT '' COMMENT '会员id',
  `amount` double(20,2) DEFAULT '0.00' COMMENT '金额',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '赠送时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '使用时间',
  `status` int(10) DEFAULT '1' COMMENT '状态（1：未使用；2：已使用;3：已转赠;4:锁定）',
  `coupon_id` varchar(64) DEFAULT '' COMMENT '优惠券id',
  `order_id` varchar(64) DEFAULT '' COMMENT '使用订单号',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `grant_status` int(10) DEFAULT '1' COMMENT '是否发放：1：未发放；2：已发放',
  `grant_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '发放时间',
  `grant_amount` decimal(10,2) DEFAULT '0.00' COMMENT '发放金额',
  `grant_gold` decimal(10,5) DEFAULT '0.00000' COMMENT '发放g重',
  PRIMARY KEY (`id`),
  KEY `index_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `coupon_member_v2`;
CREATE TABLE `coupon_member_v2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `coupon_id` int(10) NOT NULL COMMENT '优惠券ID',
  `order_no` varchar(32) DEFAULT NULL COMMENT '使用时关联订单号',
  `add_time` varchar(32) NOT NULL COMMENT '赠送时间',
  `used_time` varchar(32) DEFAULT NULL COMMENT '使用时间',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '状态（0：未使用；1：已使用;2:锁定）',
  `discount_amount` decimal(20,2) DEFAULT '0.00' COMMENT '使用时减免金额',
  PRIMARY KEY (`id`),
  KEY `index_coupon_id` (`coupon_id`) USING BTREE,
  KEY `index_member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=136492 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `coupon_repertory`;
CREATE TABLE `coupon_repertory` (
  `id` varchar(64) NOT NULL DEFAULT '' COMMENT '唯一标识',
  `name` varchar(32) DEFAULT '' COMMENT '名称',
  `type` int(10) DEFAULT '1' COMMENT '红包类型：1-固定红包；2-概率红包；3-加息劵',
  `amount` varchar(16) DEFAULT '' COMMENT '金额：固定金额（"10"),随机金额（"10-50"）, 加息率（"2"）',
  `indate_type` int(10) DEFAULT '1' COMMENT '有效期限类型：1：永久有效； 2：领取X后过期；3：某天过期；',
  `indate_days` int(10) DEFAULT '0' COMMENT '有效天数（indate_type等于2时必填）；',
  `indate_time` varchar(32) DEFAULT '' COMMENT '有效时间（indate_type等于3时必填）；',
  `finance_period` int(10) DEFAULT '0' COMMENT '理财期限',
  `donation` int(10) DEFAULT '1' COMMENT '是否可转赠（1：不可转赠；2：可转赠）',
  `trade_amount_type` int(10) DEFAULT '1' COMMENT '投资金额类型（1：单笔投资；2：累计投资）',
  `trade_amount` int(10) DEFAULT '0' COMMENT '投资金额',
  `use_condition` varchar(128) DEFAULT NULL COMMENT '使用条件',
  `increase_days` int(10) DEFAULT '0' COMMENT '加息天数',
  `status` int(10) DEFAULT '1' COMMENT '状态（1:禁用；2:启用）',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `purpose` int(10) DEFAULT '1' COMMENT '用途（1:注册；2：绑卡-邀请者奖励；3：绑卡-被邀请者奖励；4：首投定期产品-邀请者奖励；5：首投定期产品-被邀请者奖励；6:系统赠送；7：商城；8:体验金利息;9：生日;10：返利；11:认证绑卡；）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `coupon_repertory_v2`;
CREATE TABLE `coupon_repertory_v2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `name` varchar(64) NOT NULL COMMENT '优惠券名称',
  `type` int(10) NOT NULL DEFAULT '0' COMMENT '类型：0-黄金红包；1-回租福利券；2-商城满减券；',
  `category` int(10) NOT NULL DEFAULT '1' COMMENT '优惠券赠送方式：0-自动触发赠送；1-手动系统赠送；2-金豆兑换后赠送;3-领取后自动发放',
  `group_of` int(10) NOT NULL DEFAULT '0' COMMENT '赠送用户组：0-注册未绑卡；1-绑卡未交易；2-已交易用户;3-体验金购买完成；4-回租完成；5-点赞活动',
  `transaction_amount` int(10) NOT NULL DEFAULT '0' COMMENT '交易额度',
  `discount_amount` int(10) NOT NULL DEFAULT '0' COMMENT '优惠额度',
  `used_condition` varchar(256) NOT NULL COMMENT '使用条件',
  `indate_type` int(10) NOT NULL DEFAULT '0' COMMENT '有效期限类型：0：领取X后过期；1：某天过期；',
  `indate_days` int(10) NOT NULL DEFAULT '0' COMMENT '有效天数（indate_type等于0时必填）',
  `indate_time` varchar(32) DEFAULT NULL COMMENT '有效时间（indate_type等于1时必填）',
  `remark` varchar(128) DEFAULT NULL COMMENT '使用须知',
  `add_time` varchar(32) NOT NULL COMMENT '创建时间',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '状态：0-禁用；1-启用',
  `deleted` int(10) NOT NULL DEFAULT '0' COMMENT '删除：0-不删除；1-已删除',
  `is_notice` int(10) NOT NULL DEFAULT '1' COMMENT '到期是否提醒：1-不提醒；2-提醒',
  `precede_days` int(10) NOT NULL DEFAULT '0' COMMENT '提前天数',
  `leaseback_days` int(10) NOT NULL DEFAULT '0' COMMENT '回租天数',
  `year_income` double(20,2) NOT NULL DEFAULT '0.00' COMMENT '预计年化收益',
  `raise_year_income` double(20,2) NOT NULL DEFAULT '0.00' COMMENT '黄金红包-加息年化收益率',
  `product_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '适用产品类型 0:全部产品  1:实物金产品  2:安稳金产品',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `cup_ball_game`;
CREATE TABLE `cup_ball_game` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `code` varchar(8) NOT NULL,
  `game_date` varchar(32) NOT NULL COMMENT '比赛日期',
  `game_time` varchar(32) NOT NULL COMMENT '比赛时间',
  `type` int(10) NOT NULL DEFAULT '1' COMMENT '比赛类型(1:小组赛；2:1/8决赛；3:1/4决赛；4：半决赛；5:3-4名决赛；6：决赛)',
  `teama_id` varchar(64) NOT NULL COMMENT '球队A的id',
  `teama_name` varchar(64) NOT NULL COMMENT '球队A的名称',
  `teamb_id` varchar(64) NOT NULL COMMENT '球队B的id',
  `teamb_name` varchar(64) NOT NULL COMMENT '球队B的名称',
  `result` varchar(32) DEFAULT NULL COMMENT '比赛结果',
  `succee_team_id` varchar(64) DEFAULT NULL COMMENT '胜利球队id',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `status` int(10) DEFAULT '1' COMMENT '是否统计过数据：1-未统计；2-已统计'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `cup_ball_team`;
CREATE TABLE `cup_ball_team` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `name` varchar(64) NOT NULL COMMENT '球队名称',
  `icon` varchar(256) NOT NULL COMMENT '球队图片',
  `group_of` varchar(16) DEFAULT NULL COMMENT '所属小组',
  `success_num` int(10) DEFAULT '0' COMMENT '胜场数（场）',
  `fail_num` int(10) DEFAULT '0' COMMENT '负场数（场）',
  `dogfall_num` int(10) DEFAULT '0' COMMENT '平场数（场）',
  `goals` int(11) DEFAULT '0' COMMENT '进球数',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `cup_guess_user`;
CREATE TABLE `cup_guess_user` (
  `id` varchar(64) NOT NULL,
  `member_id` varchar(64) NOT NULL COMMENT '竞猜用户id',
  `game_id` varchar(64) NOT NULL COMMENT '竞猜比赛id',
  `guess_result` int(10) NOT NULL COMMENT '竞猜结果：1：A队胜；2：平局；3：B队胜',
  `final_result` int(10) DEFAULT NULL COMMENT '最终结果：1：A队胜；2：平局；3：B队胜',
  `guess_status` int(10) DEFAULT '1' COMMENT '竞猜状态(1:未公布;2:猜中了;3:猜错了)',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `cup_join_user`;
CREATE TABLE `cup_join_user` (
  `id` varchar(64) NOT NULL,
  `join_date` varchar(32) NOT NULL COMMENT '参加时间',
  `member_id` varchar(64) NOT NULL COMMENT '参与用户id',
  `balance` int(10) DEFAULT '0' COMMENT '竞猜剩余次数',
  `got_num` int(10) DEFAULT '0' COMMENT '猜中次数',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `gold_shop`;
CREATE TABLE `gold_shop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL COMMENT '门店名称',
  `gold_industry` varchar(32) DEFAULT NULL COMMENT '所属金行',
  `branch_name` varchar(32) DEFAULT NULL COMMENT '分行名称',
  `start_time` varchar(32) NOT NULL COMMENT '开始营业时间',
  `end_time` varchar(32) NOT NULL COMMENT '结束营业时间',
  `province` varchar(32) NOT NULL COMMENT '省',
  `city` varchar(32) NOT NULL COMMENT '市',
  `area` varchar(32) NOT NULL COMMENT '区',
  `address` varchar(32) NOT NULL COMMENT '详细地址',
  `longitude` decimal(25,16) NOT NULL DEFAULT '0.0000000000000000' COMMENT '经度',
  `latitude` decimal(25,16) NOT NULL DEFAULT '0.0000000000000000' COMMENT '纬度',
  `contact_name` varchar(32) NOT NULL COMMENT '联系人姓名',
  `contact_mobile` varchar(16) NOT NULL COMMENT '联系人手机号',
  `enterprise_name` varchar(32) DEFAULT NULL COMMENT '企业名称',
  `head_img` varchar(128) NOT NULL COMMENT '门店头图',
  `column_img` varchar(128) NOT NULL COMMENT '门店列图',
  `create_id` varchar(64) DEFAULT NULL COMMENT '创建人ID',
  `create_name` varchar(64) DEFAULT NULL COMMENT '创建人姓名',
  `add_time` varchar(32) DEFAULT NULL COMMENT '创建时间',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '状态：0-关闭 1-开启',
  `deleted` int(10) NOT NULL DEFAULT '0' COMMENT '删除：0-未删除；1-已删除',
  `province_code` varchar(16) NOT NULL COMMENT '省份编码',
  `city_code` varchar(16) NOT NULL COMMENT '城市编码',
  `area_code` varchar(16) NOT NULL COMMENT '区域编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `gold_shop_services`;
CREATE TABLE `gold_shop_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` int(10) NOT NULL COMMENT '门店ID',
  `title` varchar(128) NOT NULL COMMENT '服务名称',
  `content` varchar(256) NOT NULL COMMENT '服务内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 COMMENT='门店服务描述';

DROP TABLE IF EXISTS  `guess_join_member`;
CREATE TABLE `guess_join_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `activity_id` int(10) NOT NULL COMMENT '猜金价活动ID',
  `lastest_time` varchar(32) DEFAULT NULL COMMENT '最近一次参与活动时间',
  `share_num` int(10) NOT NULL DEFAULT '0' COMMENT '分享次数',
  `golds` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '获得黄金克重',
  `coupons` int(10) NOT NULL DEFAULT '0' COMMENT '获得优惠券个数',
  `join_num` int(10) NOT NULL DEFAULT '0' COMMENT '参与次数',
  `add_time` varchar(32) DEFAULT NULL COMMENT '添加时间',
  `balance` int(10) NOT NULL DEFAULT '0' COMMENT '剩余可猜次数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id_activity_id` (`member_id`,`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=630 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `guess_price_activity`;
CREATE TABLE `guess_price_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `name` varchar(32) NOT NULL COMMENT '猜金价活动名称',
  `number` varchar(32) NOT NULL COMMENT '活动期数',
  `start_date` varchar(32) NOT NULL COMMENT '开始日期',
  `end_date` varchar(32) NOT NULL COMMENT '结束日期',
  `gold_weight` int(10) NOT NULL DEFAULT '0',
  `time_point` varchar(32) NOT NULL COMMENT '竞猜金价时间点',
  `start_guess_point` varchar(32) NOT NULL COMMENT '可竞猜开始时间点',
  `end_guess_point` varchar(32) NOT NULL COMMENT '可竞猜结束时间点',
  `publish_point` varchar(32) NOT NULL COMMENT '公布结果时间',
  `shelf_state` int(10) NOT NULL DEFAULT '2' COMMENT '上架状态 1:上架 2:下架',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `coupon_ids` text COMMENT '红包ID，多个用英文","分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `guess_record`;
CREATE TABLE `guess_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `activity_id` varchar(64) NOT NULL COMMENT '猜金价活动id',
  `guess_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '竞猜时间',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `guess_price` double(20,2) NOT NULL DEFAULT '0.00' COMMENT '竞猜金价',
  `final_price` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '最终金价',
  `guess_result` int(10) NOT NULL DEFAULT '1' COMMENT '竞猜结果：1--未公布；2--已猜中；3--未猜中',
  `share_status` int(10) DEFAULT '1' COMMENT '1分享状态：1--未分享；2--已分享',
  `got_gold` double(20,3) DEFAULT '0.000' COMMENT '获得克重奖励',
  `coupon_amount` double(20,2) DEFAULT '0.00' COMMENT '获得红包金额',
  `got_status` int(10) DEFAULT '1' COMMENT '领取状态：1--未领取；2--已领取',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3346 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `guess_share_record`;
CREATE TABLE `guess_share_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `add_time` varchar(64) NOT NULL COMMENT '分享时间',
  `member_id` varchar(64) NOT NULL COMMENT '分享会员ID',
  `activity_id` int(10) NOT NULL COMMENT '参与活动',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2027 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `help_centre`;
CREATE TABLE `help_centre` (
  `id` varchar(50) NOT NULL DEFAULT '' COMMENT '唯一标识',
  `title` varchar(128) DEFAULT '' COMMENT '标题',
  `content` varchar(1024) DEFAULT NULL COMMENT '内容',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `status` int(10) DEFAULT '1' COMMENT '是否显示（1--隐藏；2--显示）',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '发布人的id',
  `create_user_name` varchar(32) DEFAULT NULL COMMENT '发布人员姓名',
  `category` varchar(128) DEFAULT NULL COMMENT '分类',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `info_advert`;
CREATE TABLE `info_advert` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `advert_position_id` varchar(50) DEFAULT NULL COMMENT '广告位ID',
  `advert_position_title` varchar(200) DEFAULT NULL COMMENT '广告位标题',
  `type` int(11) DEFAULT '1' COMMENT '广告类型 1：图片 2：flash 3：代码 4：文字',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `sub_title` varchar(255) DEFAULT NULL COMMENT '副标题',
  `target` varchar(50) DEFAULT NULL COMMENT '打开方式默认_blank',
  `link` varchar(500) DEFAULT NULL COMMENT '链接地址',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `image` varchar(255) DEFAULT NULL COMMENT '图片',
  `content` varchar(500) DEFAULT NULL COMMENT '广告内容',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '截止时间',
  `hits` int(11) DEFAULT '1' COMMENT '点击量',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[信息发布] 广告';

DROP TABLE IF EXISTS  `info_advert_position`;
CREATE TABLE `info_advert_position` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `article_class_id` varchar(50) DEFAULT NULL COMMENT '分类ID',
  `article_class_title` varchar(200) DEFAULT NULL COMMENT '分类标题',
  `code` varchar(50) DEFAULT NULL COMMENT '编号',
  `title` varchar(200) DEFAULT NULL COMMENT '名称',
  `width` int(11) DEFAULT '0' COMMENT '宽度',
  `height` int(11) DEFAULT '0' COMMENT '高度',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[信息发布] 广告位';

DROP TABLE IF EXISTS  `info_article`;
CREATE TABLE `info_article` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `article_class_id` varchar(50) DEFAULT NULL COMMENT '分类ID',
  `article_class_title` varchar(200) DEFAULT NULL COMMENT '分类标题',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `sub_title` varchar(255) DEFAULT NULL COMMENT '副标题',
  `keywords` varchar(255) DEFAULT NULL COMMENT '标签（关键字）',
  `push_time` datetime DEFAULT NULL COMMENT '发布时间',
  `source` varchar(50) DEFAULT NULL COMMENT '来源（作者）',
  `email` varchar(50) DEFAULT NULL COMMENT '作者的email',
  `description` varchar(500) DEFAULT NULL COMMENT '备注（简述）',
  `image` varchar(255) DEFAULT NULL COMMENT '图片',
  `content` longtext COMMENT '文章详情(详情)',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：显示 2：不显示',
  `hits` int(11) DEFAULT '0' COMMENT '文章点击数（浏览数）',
  `allow_review` int(11) DEFAULT '2' COMMENT '是否允许评论 1：是 2：否',
  `link` varchar(255) DEFAULT NULL COMMENT '该文章标题所引用的连接,如果该项有值将不能显示文章内容,即该表中content的值',
  `channel_ids` varchar(500) DEFAULT NULL COMMENT '渠道id 多个id用,号分隔',
  `sort` int(11) DEFAULT '1' COMMENT '排序（从小到大升序排列）',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `stop_time` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL COMMENT '生效开始时间',
  `is_stop` int(11) DEFAULT '1',
  `label1` varchar(20) DEFAULT NULL,
  `label2` varchar(20) DEFAULT NULL,
  `label3` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[信息发布] 文章';

DROP TABLE IF EXISTS  `info_article_class`;
CREATE TABLE `info_article_class` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `code` varchar(50) NOT NULL COMMENT '编号（自定义）',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `sub_title` varchar(200) DEFAULT NULL COMMENT '副标题',
  `layer` varchar(500) DEFAULT NULL COMMENT '级别001：分类 001001：分类 1001002：分类2',
  `keywords` varchar(200) DEFAULT NULL COMMENT '关键字',
  `sort` int(11) DEFAULT '1' COMMENT '排序（从小到大升序排列）',
  `show_innav` int(11) DEFAULT '1' COMMENT '是否显示在标题栏 1：是  2：否',
  `show_model` int(11) DEFAULT '1' COMMENT '显示模式 1：文字列表 2：图片文字列表 ',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[信息发布] 分类';

DROP TABLE IF EXISTS  `info_link`;
CREATE TABLE `info_link` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `title` varchar(255) DEFAULT NULL COMMENT '名称',
  `link` varchar(255) DEFAULT NULL COMMENT '链接',
  `image` varchar(255) DEFAULT NULL COMMENT 'Logo图片',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `content` text COMMENT '内容',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[信息发布] 友情链接';

DROP TABLE IF EXISTS  `lottery`;
CREATE TABLE `lottery` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `name` varchar(32) NOT NULL COMMENT '活动名称',
  `title` varchar(32) NOT NULL COMMENT '活动标题',
  `start_time` datetime NOT NULL COMMENT '活动开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `rules` text NOT NULL COMMENT '活动规则',
  `accumulate_num` int(10) NOT NULL DEFAULT '0' COMMENT '抽奖次数是否可累加：1：可累加；0：否，当日清空',
  `default_num` int(10) NOT NULL DEFAULT '0' COMMENT '首次参与活动默认抽奖次数',
  `everyday_num` int(10) NOT NULL DEFAULT '0' COMMENT '次日起每人每天获得抽奖次数',
  `everyday_max_num` int(10) NOT NULL DEFAULT '0' COMMENT '每日分享可获得最大抽奖次数',
  `granting_method` int(10) NOT NULL DEFAULT '0' COMMENT '中奖后奖品发放方式：0：立即发放；1：否',
  `share_rewards` int(10) NOT NULL DEFAULT '0' COMMENT '是否有分享激励：0：没有;1:有',
  `share_num` int(10) NOT NULL DEFAULT '0' COMMENT '分享多少次获得一次抽奖机会',
  `share_title` varchar(32) NOT NULL COMMENT '分享标题',
  `share_img` varchar(128) NOT NULL COMMENT '分享封面',
  `share_remark` text NOT NULL COMMENT '分享摘要',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '状态：0:未发布；1：已发布',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `publish_time` datetime DEFAULT NULL COMMENT '发布时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='抽奖活动表';

DROP TABLE IF EXISTS  `lottery_join_member`;
CREATE TABLE `lottery_join_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `lottery_id` int(10) NOT NULL COMMENT '抽奖活动ID',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '参与时间',
  `times` int(10) NOT NULL DEFAULT '0' COMMENT '抽奖次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COMMENT='抽奖活动参与用户';

DROP TABLE IF EXISTS  `lottery_member`;
CREATE TABLE `lottery_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `member_id` varchar(64) NOT NULL COMMENT '抽奖用户ID',
  `lottery_id` int(10) NOT NULL COMMENT '抽奖活动ID',
  `prize_id` int(10) NOT NULL COMMENT '奖品ID',
  `amount` double(20,3) NOT NULL DEFAULT '0.000' COMMENT '金额',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '抽奖时间',
  `get_time` datetime DEFAULT NULL COMMENT '领取时间',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '奖品领取状态：0：未领取；1：已领取',
  `rewards_status` int(10) NOT NULL DEFAULT '0' COMMENT '中奖状态：0：未中奖；1：已中奖',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=444 DEFAULT CHARSET=utf8mb4 COMMENT='用户抽奖记录';

DROP TABLE IF EXISTS  `lottery_prize`;
CREATE TABLE `lottery_prize` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `name` varchar(32) NOT NULL COMMENT '奖品名称',
  `lottery_id` int(10) NOT NULL COMMENT '抽奖活动ID',
  `prize_img` varchar(128) NOT NULL COMMENT '奖品图片',
  `type` int(10) NOT NULL DEFAULT '7' COMMENT '1:现金；2：黄金；3：现金红包；4：福利券；5：电子卡；6：实物礼品；7：未中奖；8：黄金红包',
  `amount` double(20,3) DEFAULT '0.000' COMMENT '金额（现金金额或黄金克重）',
  `quantity_limit` int(10) NOT NULL DEFAULT '0' COMMENT '数量限制：0：不限制；1：限制；',
  `quantity` int(10) NOT NULL DEFAULT '0' COMMENT '奖品数量',
  `original_quantity` int(10) NOT NULL DEFAULT '0' COMMENT '原始数量',
  `coupon_ids` text COMMENT '优惠券Id，多个用","分割',
  `probability` double(20,3) NOT NULL DEFAULT '0.000' COMMENT '中奖概率',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COMMENT='抽奖活动奖品表';

DROP TABLE IF EXISTS  `lottery_share_statistics`;
CREATE TABLE `lottery_share_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `lottery_id` int(10) NOT NULL COMMENT '抽奖活动ID',
  `member_id` varchar(64) NOT NULL COMMENT '分享用户ID',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='抽奖分享记录表';

DROP TABLE IF EXISTS  `mall_goods`;
CREATE TABLE `mall_goods` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `name` varchar(128) NOT NULL COMMENT '商品名称',
  `category` varchar(128) NOT NULL COMMENT '商品分类',
  `brand` varchar(128) NOT NULL COMMENT '商品品牌',
  `title` varchar(128) DEFAULT NULL COMMENT '标题',
  `label` varchar(128) DEFAULT NULL COMMENT '标签',
  `stock` int(10) DEFAULT '0' COMMENT '库存',
  `price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `introduction` text COMMENT '商品详情',
  `tips` text COMMENT '保养提示',
  `cognition` text COMMENT '购买须知',
  `code` varchar(64) DEFAULT NULL COMMENT '商品货号',
  `weight` decimal(10,5) DEFAULT '0.00000' COMMENT '克重',
  `quality` varchar(64) DEFAULT NULL COMMENT '商品品质',
  `processing_fee` decimal(10,2) DEFAULT NULL COMMENT '加工费',
  `fare` decimal(10,2) DEFAULT NULL COMMENT '运费',
  `thumbnail_url` varchar(128) NOT NULL COMMENT '主图URL地址',
  `audit_status` int(10) DEFAULT '1' COMMENT '审核状态：1：待审核；2审核通过；3：审核不通过',
  `audit_opinion` varchar(128) DEFAULT NULL COMMENT '审核描述',
  `sale_status` int(10) DEFAULT '1' COMMENT '上架状态：1：待上架；2：上架；3：下架',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城-商品表';

DROP TABLE IF EXISTS  `mall_goods_order`;
CREATE TABLE `mall_goods_order` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `order_no` varchar(64) NOT NULL COMMENT '订单号',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `actual_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '应付总金额（商品价格+运费+加工费）',
  `status` int(10) DEFAULT '1' COMMENT '订单状态（1：待支付；2：待发货（支付成功）；3：待收货（已发货）；4：已完成（已收货）；5：已取消（已关闭））',
  `cancel_reason` varchar(128) DEFAULT NULL COMMENT '关闭原因',
  `goods_id` varchar(64) NOT NULL COMMENT '商品id',
  `goods_name` varchar(128) NOT NULL COMMENT '商品名称',
  `quantity` int(10) DEFAULT '1' COMMENT '数量',
  `source` int(10) DEFAULT '1' COMMENT '订单来源 1：IOS 2：H5 3：微信小程序 4：第三方平台;5:安卓',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '商品价格',
  `processing_fee` decimal(10,2) DEFAULT '0.00' COMMENT '加工费',
  `fare` decimal(10,2) DEFAULT '0.00' COMMENT '运费',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间（下单时间）',
  `success_time` datetime DEFAULT NULL COMMENT '支付成功时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城-商品订单表';

DROP TABLE IF EXISTS  `mall_goods_picture`;
CREATE TABLE `mall_goods_picture` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `goods_id` varchar(50) NOT NULL COMMENT '商品id',
  `img_url` varchar(128) NOT NULL COMMENT '图片地址',
  `sort` int(11) DEFAULT '1' COMMENT '排序',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城-商品图片表';

DROP TABLE IF EXISTS  `mall_logistics`;
CREATE TABLE `mall_logistics` (
  `id` varchar(64) NOT NULL,
  `order_no` varchar(64) NOT NULL COMMENT '订单号',
  `serial_no` varchar(64) NOT NULL COMMENT '物流单号',
  `company` varchar(128) NOT NULL COMMENT '物流公司',
  `phone` varchar(16) DEFAULT NULL COMMENT '物流公司联系电话',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间（发货时间）',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `receive_time` datetime DEFAULT NULL COMMENT '收货时间',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '状态:1:待收货；2：已收货',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商城-物流信息';

DROP TABLE IF EXISTS  `member_address`;
CREATE TABLE `member_address` (
  `id` varchar(50) NOT NULL COMMENT '收获地址Id',
  `member_id` varchar(50) NOT NULL COMMENT '用户id',
  `name` varchar(50) NOT NULL COMMENT '收货人姓名',
  `mobile` varchar(25) NOT NULL COMMENT '手机号码或者固定电话',
  `province` varchar(20) NOT NULL COMMENT '省份',
  `city` varchar(20) NOT NULL COMMENT '城市',
  `area` varchar(20) NOT NULL COMMENT '地区',
  `address` varchar(255) NOT NULL COMMENT '详细地址',
  `is_default` int(11) DEFAULT '2' COMMENT '是否默认 1:否  2:是',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[会员] 收货地址';

DROP TABLE IF EXISTS  `member_analyse`;
CREATE TABLE `member_analyse` (
  `id` varchar(50) NOT NULL,
  `day` date NOT NULL COMMENT '日期',
  `channel_id` varchar(64) NOT NULL COMMENT '渠道id',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `type` int(11) DEFAULT NULL COMMENT '类型 1:注册 2:绑卡 3:充值 4:交易',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_type` (`member_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员数据分析';

DROP TABLE IF EXISTS  `member_group`;
CREATE TABLE `member_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) NOT NULL DEFAULT '2' COMMENT '类型：1-系统创建；2-自定义类型',
  `name` varchar(64) NOT NULL COMMENT '群组名称',
  `calculation_category` int(10) NOT NULL DEFAULT '1' COMMENT '计算方式：1-例行计算；2-单次计算',
  `label_category` int(10) NOT NULL DEFAULT '1' COMMENT '属性标签：1-注册未绑卡用户；2-绑卡未交易用户；3-已产生交易用户；',
  `add_time` varchar(64) NOT NULL COMMENT '创建时间',
  `modify_time` varchar(64) DEFAULT NULL COMMENT '最后计算时间',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '状态：1-关闭；2-开启',
  `create_id` varchar(64) DEFAULT NULL COMMENT '创建人ID',
  `create_name` varchar(32) DEFAULT NULL COMMENT '创建人姓名',
  `persons` int(10) DEFAULT '0' COMMENT '人数',
  `channel_id` varchar(64) DEFAULT NULL COMMENT '渠道ID',
  `start_time` varchar(32) DEFAULT NULL COMMENT '注册/绑卡/最近一次投资的开始时间',
  `end_time` varchar(32) DEFAULT NULL COMMENT '注册/绑卡/最近一次投资的结束时间',
  `start_leaseback_time` varchar(32) DEFAULT NULL COMMENT '最近一次回租的开始时间',
  `end_leaseback_time` varchar(32) DEFAULT NULL COMMENT '最近一次回租的结束时间',
  `investment_mark` int(10) DEFAULT '1' COMMENT '交易符号：1-等于； 2-大于；3-小于；4-区间',
  `start_investment_amount` int(10) DEFAULT '0' COMMENT '投资金额（investment_mark 等于1,2,3,4时此字段必填）',
  `end_investment_amount` int(10) DEFAULT '0' COMMENT '投资金额（investment_mark 等于4时此字段必填）',
  `leaseback_mark` int(10) DEFAULT '1' COMMENT '回租符号：1-等于； 2-大于；3-小于；4-区间',
  `start_leaseback_num` int(10) DEFAULT '0' COMMENT '回租次数（leaseback_mark 等于1,2,3,4时此字段必填）',
  `end_leaseback_num` int(10) DEFAULT '0' COMMENT '回租次数（leaseback_mark 等于4时此字段必填）',
  `deleted` int(10) NOT NULL DEFAULT '0' COMMENT '删除：1-未删除；1-已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='用户群组';

DROP TABLE IF EXISTS  `member_group_record`;
CREATE TABLE `member_group_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) NOT NULL COMMENT '群组ID',
  `mobile` varchar(16) NOT NULL COMMENT '用户手机号',
  `add_time` varchar(32) NOT NULL COMMENT '计算时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3449886 DEFAULT CHARSET=utf8mb4 COMMENT='群组用户记录';

DROP TABLE IF EXISTS  `member_group_statistics`;
CREATE TABLE `member_group_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `group_id` int(10) NOT NULL COMMENT '用户群组ID',
  `persons` int(10) NOT NULL DEFAULT '0' COMMENT '人数',
  `add_time` varchar(32) NOT NULL COMMENT '计算时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1284 DEFAULT CHARSET=utf8mb4 COMMENT='用户群组统计';

DROP TABLE IF EXISTS  `member_investment_rewards`;
CREATE TABLE `member_investment_rewards` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `friend_id` varchar(64) DEFAULT NULL COMMENT '投资者id',
  `amount` double(20,2) NOT NULL DEFAULT '0.00' COMMENT '奖励金额',
  `order_id` varchar(64) DEFAULT NULL COMMENT '投资订单号',
  `bill_id` varchar(64) DEFAULT NULL COMMENT '支付收支记录表id',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间（投资者投资时间）',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `type` int(10) DEFAULT '3' COMMENT '类型：1：邀请绑卡；2：邀请首投定期产品；3；邀请返现',
  `activity_invitation_id` int(10) DEFAULT NULL COMMENT '邀友活动ID',
  `investment_amount` decimal(10,2) DEFAULT '0.00' COMMENT '投资金额',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='邀请现金奖励明细';

DROP TABLE IF EXISTS  `member_label`;
CREATE TABLE `member_label` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `born_year` int(10) DEFAULT '0' COMMENT '出生年份',
  `gender` int(10) DEFAULT '3' COMMENT '性别 1:男 2:女 3:未知',
  `member_id` varchar(64) NOT NULL COMMENT '用户id',
  `mobile` varchar(16) NOT NULL COMMENT '手机号',
  `name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `channel_id` varchar(64) DEFAULT NULL COMMENT '渠道id',
  `channel_name` varchar(64) DEFAULT NULL COMMENT '渠道名称',
  `ip1` varchar(128) DEFAULT NULL COMMENT 'ip地址1',
  `ip2` varchar(128) DEFAULT NULL COMMENT 'ip地址2',
  `ip3` varchar(128) DEFAULT NULL COMMENT 'ip地址3',
  `common_ip` varchar(128) DEFAULT NULL COMMENT '常用ip地址',
  `native_place` varchar(128) DEFAULT NULL COMMENT '籍贯',
  `mobile_place` varchar(128) DEFAULT NULL,
  `mobile_company` varchar(64) DEFAULT NULL COMMENT '手机号码公司',
  `rvalue` int(10) DEFAULT '0' COMMENT 'R值',
  `fvalue` int(10) DEFAULT '0' COMMENT 'F值',
  `mvalue` double(20,2) DEFAULT '0.00' COMMENT 'M值',
  `rfm_category1` varchar(32) DEFAULT NULL COMMENT '最新RFM分类',
  `rfm_category2` varchar(32) DEFAULT NULL COMMENT '上次RFM分类',
  `rfm1` int(10) DEFAULT '0' COMMENT '最新RFM值',
  `rfm2` int(10) DEFAULT '0' COMMENT '上次RFM值',
  `rfm_change` int(10) DEFAULT '0' COMMENT 'RFM值变动',
  `label` varchar(32) DEFAULT NULL COMMENT '羊毛当标签',
  `score` int(10) DEFAULT NULL COMMENT '羊毛当分值',
  `recent_trade_time` varchar(32) DEFAULT NULL COMMENT '最近一次购买时间',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `member_label_baseline`;
CREATE TABLE `member_label_baseline` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `rfm_days` int(10) NOT NULL DEFAULT '0' COMMENT 'F计算天数',
  `rvalue` int(10) NOT NULL DEFAULT '0' COMMENT 'R阀值',
  `fvalue` int(10) NOT NULL DEFAULT '0' COMMENT 'F阀值',
  `mvalue` int(10) NOT NULL DEFAULT '0' COMMENT 'M阀值',
  `ravg` double(20,2) NOT NULL DEFAULT '0.00' COMMENT 'R平均值',
  `favg` double(20,2) NOT NULL DEFAULT '0.00' COMMENT 'F平均值',
  `mavg` double(20,2) NOT NULL DEFAULT '0.00' COMMENT 'M平均值',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '状态：1--已计算；2--未计算',
  `counting_date` varchar(24) DEFAULT NULL COMMENT '计算日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `member_level`;
CREATE TABLE `member_level` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `name` varchar(50) DEFAULT NULL COMMENT '等级名称',
  `minimum` int(11) unsigned DEFAULT '0' COMMENT '积分下限',
  `maximum` int(11) unsigned DEFAULT '0' COMMENT '积分上限',
  `discount` decimal(9,2) DEFAULT '100.00' COMMENT '折扣（默认100%）',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1：正常  2：特殊会员组',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[会员] 等级';

DROP TABLE IF EXISTS  `member_operation_record`;
CREATE TABLE `member_operation_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `member_id` varchar(64) NOT NULL COMMENT '会员id',
  `guide_num` int(10) DEFAULT '0' COMMENT '引导页点击数',
  `buy_gold_num` int(10) DEFAULT '0' COMMENT '买金点击数',
  `lease_back_num` int(10) DEFAULT '0' COMMENT '回租点击数',
  `coupon_num` int(10) DEFAULT '0' COMMENT '查看优惠卷点击数',
  `learn_more_num` int(10) DEFAULT '0' COMMENT '了解更多点击数',
  `skip_num` int(10) DEFAULT '0' COMMENT '跳过点击数',
  `login_num` int(10) DEFAULT '0' COMMENT '登陆次数',
  `buy_gold_leaseback_status` int(11) DEFAULT '0' COMMENT '回租到期提单引导弹框状态0 没有  1.存在',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `buy_gold_pending_status` int(11) DEFAULT '0' COMMENT '首次提单引导弹框状态0 不存在  1.存在',
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_operation_record_UK_member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1013 DEFAULT CHARSET=utf8mb4 COMMENT='用户操作记录';

DROP TABLE IF EXISTS  `member_temp`;
CREATE TABLE `member_temp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20690 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `member_third_account`;
CREATE TABLE `member_third_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openId` varchar(50) DEFAULT NULL COMMENT '第三方Id编号',
  `memberId` varchar(50) NOT NULL COMMENT '会员编号',
  `type` tinyint(4) DEFAULT '1' COMMENT '类型 1：微信',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `clientType` tinyint(2) DEFAULT '0' COMMENT '客户端类型1：其他,2：Android App 3：IOS App 4:微信（H5）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3265 DEFAULT CHARSET=utf8mb4 COMMENT='[会员]第三方账号关联表';

DROP TABLE IF EXISTS  `member_user`;
CREATE TABLE `member_user` (
  `id` varchar(50) NOT NULL COMMENT '会员ID',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `login_name` varchar(50) DEFAULT NULL COMMENT '登录名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `pay_password` varchar(50) DEFAULT NULL COMMENT '交易密码',
  `gender` int(11) DEFAULT '3' COMMENT '性别 1:男 2:女 3:未知',
  `age` int(11) DEFAULT '0' COMMENT '年龄',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号码',
  `birthday` varchar(20) DEFAULT NULL COMMENT '生日',
  `level` varchar(50) DEFAULT NULL COMMENT '用户等级',
  `head` varchar(255) DEFAULT NULL COMMENT '头像',
  `invite_code` varchar(8) DEFAULT NULL COMMENT '邀请码',
  `friend_id` varchar(64) DEFAULT NULL COMMENT '邀请人id',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：正常  2：冻结',
  `description` varchar(50) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) NOT NULL DEFAULT '1' COMMENT '删除 1：否 2：是',
  `channel_id` varchar(64) DEFAULT NULL COMMENT '渠道id',
  `source` int(10) DEFAULT '1' COMMENT '添加注册终端1：PC;2：Android;3：IOS',
  `province` varchar(128) DEFAULT NULL COMMENT '省份',
  `staff_id` varchar(64) DEFAULT '0' COMMENT '渠道成员id',
  `password_wrong_times` int(10) NOT NULL DEFAULT '0' COMMENT '登陆密码错误次数',
  `password_last_time` datetime DEFAULT NULL COMMENT '登陆密码最近一次错误时间',
  `password_status` int(10) NOT NULL DEFAULT '0' COMMENT '登陆密码锁定状态：0--未锁定；1--锁定',
  `trdpassd_wrong_times` int(10) NOT NULL DEFAULT '0' COMMENT '交易密码错误次数',
  `trdpassd_last_time` datetime DEFAULT NULL COMMENT '交易密码最近一次错误时间',
  `trdpassd_status` int(10) NOT NULL DEFAULT '0' COMMENT '交易密码锁定状态：0--未锁定；1--锁定',
  `activity_invitation_id` int(10) DEFAULT '0' COMMENT '邀友活动ID',
  `lottery_id` int(10) DEFAULT NULL COMMENT '抽奖活动id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_mobile` (`mobile`),
  KEY `index_friend_id` (`friend_id`) USING BTREE,
  KEY `index_channel_id` (`channel_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[会员] 会员信息';

DROP TABLE IF EXISTS  `msg_member_info`;
CREATE TABLE `msg_member_info` (
  `id` varchar(50) NOT NULL COMMENT '消息ID',
  `member_id` varchar(50) NOT NULL COMMENT '会员ID',
  `title` varchar(255) DEFAULT NULL COMMENT '消息标题',
  `type` int(11) NOT NULL COMMENT '消息类型--字典表定义',
  `content` varchar(500) DEFAULT NULL COMMENT '消息内容',
  `skip_type` int(11) DEFAULT '0' COMMENT '跳转类型 0:不跳转 1:账户余额 2:我的银行卡 3:订单支付页面 4:投资记录 5:黄金总资产',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1:未读 2:已读',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[消息] 会员消息';

DROP TABLE IF EXISTS  `msg_notice`;
CREATE TABLE `msg_notice` (
  `id` varchar(50) NOT NULL COMMENT '公告ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `send_type` int(11) NOT NULL COMMENT '发送类型  1:全部 2:指定会员',
  `send_time_type` int(11) NOT NULL COMMENT '发送时间类型 1:立即发送 2:指定时间',
  `send_time` datetime DEFAULT NULL COMMENT '指定发送时间',
  `send_number` int(11) DEFAULT NULL COMMENT '发送数量',
  `author` varchar(50) DEFAULT NULL COMMENT '署名',
  `create_user_id` varchar(50) NOT NULL COMMENT '创建用户Id',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建用户姓名',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[消息] 公告';

DROP TABLE IF EXISTS  `msg_notice_member_mapping`;
CREATE TABLE `msg_notice_member_mapping` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `member_id` varchar(50) NOT NULL COMMENT '会员Id',
  `notice_id` varchar(50) NOT NULL COMMENT '公告通知Id',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1:未读 2:已读',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[消息] 公告会员消息表';

DROP TABLE IF EXISTS  `msg_template`;
CREATE TABLE `msg_template` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `name` varchar(100) DEFAULT NULL COMMENT '场景名称',
  `type` int(11) DEFAULT NULL COMMENT '场景名称类型  --字典表获取',
  `described` varchar(255) DEFAULT NULL COMMENT '场景入口描述',
  `title` varchar(100) DEFAULT NULL COMMENT '站内信消息标题',
  `content` varchar(500) DEFAULT NULL COMMENT '消息内容',
  `skip_type` int(11) DEFAULT NULL COMMENT '跳转类型 0:不跳转 1:账户余额 2:我的银行卡 3:订单支付页面 4:投资记录 5:黄金总资产',
  `is_sms` int(11) DEFAULT NULL COMMENT '是否发送短信    1:是 2:不是',
  `is_inter_letter` int(11) DEFAULT NULL COMMENT '是否发送站内信  1:是 2:不是',
  `filing_number` varchar(20) DEFAULT NULL COMMENT '短信第三方备案号',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建人用户Id',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建人姓名',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[消息] 消息模版';

DROP TABLE IF EXISTS  `pay_account_finance_bill`;
CREATE TABLE `pay_account_finance_bill` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `day` date NOT NULL COMMENT '日期',
  `way` int(11) NOT NULL COMMENT '通道方式 1:易宝支付',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '账户总额',
  `recharge_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `withdraw_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `last_day_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '上一日累计金额',
  `balance_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '账户结余',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='账户资金财务对账';

DROP TABLE IF EXISTS  `pay_account_record`;
CREATE TABLE `pay_account_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `add_date` varchar(32) NOT NULL COMMENT '日期',
  `total_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '黄金总资产',
  `current_principal_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '活期利息',
  `current_interest_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '活期利息',
  `regular_principal_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '定期本金',
  `regular_interest_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '定期预期收益',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id_date` (`member_id`,`add_date`)
) ENGINE=InnoDB AUTO_INCREMENT=536740 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_bank_card`;
CREATE TABLE `pay_bank_card` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `member_id` varchar(50) NOT NULL COMMENT '会员ID',
  `pay_way` int(11) NOT NULL DEFAULT '1' COMMENT '支付方式 1:易宝支付',
  `name` varchar(25) NOT NULL COMMENT '卡号姓名',
  `mobile` varchar(11) NOT NULL COMMENT '该卡绑定手机号码',
  `id_card` varchar(18) NOT NULL COMMENT '身份证号码',
  `card_no` varchar(25) NOT NULL COMMENT '银行卡号',
  `card_top` varchar(6) NOT NULL COMMENT '卡号前6位',
  `card_last` varchar(4) NOT NULL COMMENT '卡号后4位',
  `bank_code` varchar(16) DEFAULT NULL COMMENT '银行编号  如:ICBC',
  `bank_name` varchar(50) DEFAULT NULL COMMENT '银行名称  如:工商银行',
  `request_no` varchar(64) DEFAULT NULL COMMENT '绑卡请求号 商户生成',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '绑定状态  1:待绑定 2:已绑定 3:已解除绑定 4:绑定失败',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='绑定银行卡列表';

DROP TABLE IF EXISTS  `pay_bank_info`;
CREATE TABLE `pay_bank_info` (
  `id` varchar(50) NOT NULL,
  `bank_code` varchar(10) NOT NULL COMMENT '银行编号',
  `bank_name` varchar(50) NOT NULL COMMENT '银行名称',
  `img` varchar(255) NOT NULL COMMENT '银行logo',
  `status` int(11) DEFAULT NULL COMMENT '状态 1:正常 2:隐藏 3:维护中',
  `way` int(11) DEFAULT NULL COMMENT '类型 1:易宝支付 2:连连支付',
  `amount_once` decimal(10,2) DEFAULT NULL COMMENT '单笔上线金额',
  `amount_day` decimal(10,2) DEFAULT NULL COMMENT '单日上线金额',
  `amount_month` decimal(10,2) DEFAULT NULL COMMENT '单月上线金额',
  `sort` int(11) DEFAULT NULL COMMENT '银行排序序号',
  `start_time` datetime DEFAULT NULL COMMENT '维护开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '维护结束时间',
  `deleted` int(11) NOT NULL DEFAULT '1' COMMENT '删除 1：否 2：是',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_bill`;
CREATE TABLE `pay_bill` (
  `id` varchar(50) NOT NULL COMMENT '收支Id',
  `member_id` varchar(50) NOT NULL COMMENT '会员Id',
  `order_id` varchar(32) NOT NULL COMMENT '订单号',
  `amount` decimal(10,2) NOT NULL COMMENT '金额',
  `fee` decimal(10,2) DEFAULT '0.00' COMMENT '手续费',
  `actual_amount` decimal(10,2) DEFAULT '0.00' COMMENT '实际金额(金额-手续费)',
  `gold_price` decimal(10,2) DEFAULT '0.00' COMMENT '金价',
  `type` int(11) NOT NULL COMMENT '类型 1:收入 2:支出',
  `trade_type` int(11) DEFAULT NULL COMMENT '交易类型 1:线上充值  2:卖金收入 3:邀请返现 4:账户提现 5:买金支出 6:投资返利 7:红包返现 8:加息奖励',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付收支记录表';

DROP TABLE IF EXISTS  `pay_bill_gold`;
CREATE TABLE `pay_bill_gold` (
  `id` varchar(50) NOT NULL COMMENT '收支Id',
  `member_id` varchar(50) DEFAULT NULL COMMENT '会员Id',
  `order_id` varchar(32) DEFAULT NULL COMMENT '订单号',
  `amount` decimal(10,5) DEFAULT '0.00000' COMMENT '资产',
  `fee` decimal(10,5) DEFAULT '0.00000' COMMENT '每克手续费',
  `actual_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '实际资产(资产-手续费)',
  `gold_price` decimal(10,2) DEFAULT '0.00' COMMENT '金价',
  `type` int(11) DEFAULT '1' COMMENT '类型 1:转入 2:转出',
  `trade_type` int(11) DEFAULT '1' COMMENT '交易类型 1:投资活期产品  2:定期转活期 3:存金 4:活期转定期 5:提金 6:卖金 7:投资返利',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 转入转出记录表';

DROP TABLE IF EXISTS  `pay_experience_bill`;
CREATE TABLE `pay_experience_bill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` varchar(50) NOT NULL COMMENT '会员Id',
  `order_no` varchar(32) NOT NULL COMMENT '流水号',
  `type` int(11) NOT NULL COMMENT '类型 1:收入 2:支出',
  `trade_type` int(10) NOT NULL COMMENT '操作行为 1.注册 2.活动 3.抽券 4.购买体验金产品',
  `gram` int(10) NOT NULL COMMENT '变动克重',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66106 DEFAULT CHARSET=utf8mb4 COMMENT='体验金收支记录';

DROP TABLE IF EXISTS  `pay_experience_order`;
CREATE TABLE `pay_experience_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `add_time` varchar(32) NOT NULL COMMENT '创建时间',
  `product_id` varchar(64) NOT NULL COMMENT '产品ID',
  `product_name` varchar(64) NOT NULL COMMENT '产品名称',
  `total_gram` int(10) NOT NULL COMMENT '购买体验金克重',
  `gold_price` decimal(10,2) NOT NULL COMMENT '回租金价',
  `leaseback_days` int(10) NOT NULL COMMENT '回租天数',
  `year_income` decimal(10,2) NOT NULL COMMENT '回租年化',
  `start_time` varchar(32) NOT NULL COMMENT '回租开始时间',
  `end_time` varchar(32) NOT NULL COMMENT '回租结束时间',
  `income_gram` decimal(10,5) NOT NULL COMMENT '体验金奖励（克）',
  `income_amount` decimal(10,2) NOT NULL COMMENT '体验金奖励（元）',
  `income_bean` int(10) NOT NULL COMMENT '体验金奖励（金豆）',
  `modify_time` varchar(32) DEFAULT NULL COMMENT '体验金到账时间',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '1-回租中；2-已到期',
  `income_type` int(10) NOT NULL DEFAULT '1' COMMENT '收益类型：1--现金；2--金豆',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_no` (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=4293 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_extract_gold`;
CREATE TABLE `pay_extract_gold` (
  `id` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL COMMENT '订单号',
  `member_id` varchar(50) NOT NULL COMMENT '会员id',
  `name` varchar(25) NOT NULL COMMENT '姓名',
  `mobile` varchar(11) NOT NULL COMMENT '提金手机号码',
  `id_card` varchar(18) NOT NULL COMMENT '身份证号码',
  `amount` decimal(10,0) NOT NULL COMMENT '提金数量',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提金表';

DROP TABLE IF EXISTS  `pay_funds_record`;
CREATE TABLE `pay_funds_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `add_date` varchar(32) NOT NULL COMMENT '添加日期',
  `total_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '资金总额（元）',
  `freeze_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '冻结资金（元）',
  `usable_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '可用资金（元）',
  `income_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '收入金额（元）',
  `withdraw_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '提现金额（元）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id_date` (`member_id`,`add_date`)
) ENGINE=InnoDB AUTO_INCREMENT=445221 DEFAULT CHARSET=utf8mb4 COMMENT='会员资金账户记录';

DROP TABLE IF EXISTS  `pay_gold_bean_record`;
CREATE TABLE `pay_gold_bean_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `type` int(10) NOT NULL COMMENT '类型 1:收入 2:支出',
  `remark` varchar(128) NOT NULL COMMENT '收支说明',
  `gold_bean` int(10) NOT NULL DEFAULT '0' COMMENT '金豆个数',
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `gold_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '预估黄金克重',
  `add_time` varchar(64) NOT NULL COMMENT '添加时间',
  `success_time` varchar(64) DEFAULT NULL COMMENT '到账时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47175 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_gold_extraction_offline`;
CREATE TABLE `pay_gold_extraction_offline` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `tbill_no` varchar(32) NOT NULL COMMENT '提单编号',
  `order_no` varchar(32) NOT NULL COMMENT '提金订单号',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '提金申请时间',
  `member_id` varchar(32) NOT NULL COMMENT '提单用户',
  `shop_name` varchar(64) NOT NULL COMMENT '门店名称',
  `shop_id` int(10) NOT NULL DEFAULT '1' COMMENT '门店id',
  `fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '提金手续费',
  `processing_fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '加工费',
  `actual_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '提金状态（0-交易失败,1-已预约， 2-待提货， 3-已取消， 4-已完成）',
  `fullname` varchar(32) NOT NULL COMMENT '联系人姓名',
  `cellphone` varchar(16) NOT NULL COMMENT '联系人手机号',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注信息',
  `close_remark` varchar(512) DEFAULT NULL COMMENT '关闭订单备注信息',
  `final_time` varchar(32) DEFAULT NULL COMMENT '提货完成时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `success_time` varchar(32) DEFAULT NULL COMMENT '支付成功时间',
  `close_time` varchar(32) DEFAULT NULL COMMENT '关闭时间',
  `readed` tinyint(4) DEFAULT '0' COMMENT '是否被阅读 0-未被阅读 1-已被阅读',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='门店提金表';

DROP TABLE IF EXISTS  `pay_gold_extraction_online`;
CREATE TABLE `pay_gold_extraction_online` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `tbill_no` varchar(32) NOT NULL COMMENT '提单编号',
  `order_no` varchar(32) NOT NULL COMMENT '提金订单号',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '提金申请时间',
  `member_id` varchar(32) NOT NULL COMMENT '提单用户',
  `fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '提金手续费',
  `courier_fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '快递费用',
  `processing_fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '加工费',
  `actual_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '实际支付金额',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '提金状态（0-交易失败,1-已预约， 2-待收货， 3-已取消， 4-已完成）',
  `logistics_name` varchar(32) DEFAULT NULL COMMENT '配送公司名称',
  `logistics_no` varchar(32) DEFAULT NULL COMMENT '快递单号',
  `deliver_time` varchar(32) DEFAULT NULL COMMENT '发货时间',
  `data` text COMMENT '快递json数据',
  `fullname` varchar(32) NOT NULL COMMENT '收货人姓名',
  `cellphone` varchar(16) NOT NULL COMMENT '收货人手机号',
  `address` varchar(512) NOT NULL COMMENT '收货人详细地址',
  `remark` varchar(512) DEFAULT NULL COMMENT '备注信息',
  `close_remark` varchar(512) DEFAULT NULL COMMENT '关闭订单备注信息',
  `final_time` varchar(32) DEFAULT NULL COMMENT '收货时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `success_time` varchar(32) DEFAULT NULL COMMENT '支付成功时间',
  `close_time` varchar(32) DEFAULT NULL COMMENT '关闭时间',
  `readed` tinyint(4) DEFAULT '0' COMMENT '是否被阅读 0-未被阅读 1-已被阅读',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='快递提金表（门店提金在另一张表）';

DROP TABLE IF EXISTS  `pay_gold_record`;
CREATE TABLE `pay_gold_record` (
  `id` varchar(50) NOT NULL COMMENT '会员资产账户Id',
  `member_id` varchar(50) NOT NULL COMMENT '会员Id',
  `principal_now_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T日本金',
  `principal_history_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日本金',
  `principal_history_total_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日累计本金',
  `interest_now_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T日利息',
  `interest_history_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日利息（可用）',
  `interest_history_total_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日累计利息',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 可用资产详表';

DROP TABLE IF EXISTS  `pay_gold_stock`;
CREATE TABLE `pay_gold_stock` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `calculation_date` varchar(32) NOT NULL COMMENT '计算日期',
  `total_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '黄金总资产',
  `current_principal_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '活期本金',
  `current_interest_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '活期利息',
  `regular_principal_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '定期本金',
  `regular_interest_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '定期预期收益',
  `closing_price` decimal(20,2) DEFAULT '0.00' COMMENT '当日收盘价',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_calculation_date` (`calculation_date`)
) ENGINE=InnoDB AUTO_INCREMENT=516 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_member_account`;
CREATE TABLE `pay_member_account` (
  `id` varchar(50) NOT NULL COMMENT '会员资金账户Id',
  `member_id` varchar(50) NOT NULL COMMENT '会员Id',
  `fund_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '资金总额',
  `freeze_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '冻结总额',
  `usable_amount` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '可用总额',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `coupon_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '已获得现金红包',
  `interest_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '加息券奖励金额',
  `total_bean` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总金豆数（待收金豆+可用金豆）',
  `usable_bean` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '可用金豆',
  `freeze_bean` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '冻结金豆数',
  `total_gram` int(10) unsigned DEFAULT '0' COMMENT '累计获得体验金（克）',
  `usable_gram` int(10) unsigned DEFAULT '0' COMMENT '当前可用体验金（克）',
  `total_income_gram` decimal(10,5) unsigned DEFAULT '0.00000' COMMENT '累计已获得体验金奖励（克）',
  `convert_amount` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '折算金额（元）',
  `convert_bean` int(10) unsigned DEFAULT '0' COMMENT '对应金豆（个）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员资金账户';

DROP TABLE IF EXISTS  `pay_member_balance`;
CREATE TABLE `pay_member_balance` (
  `id` varchar(50) NOT NULL,
  `day` date NOT NULL COMMENT '日期',
  `member_id` varchar(50) NOT NULL COMMENT '会员id',
  `amount` decimal(10,2) NOT NULL COMMENT '金额',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `day_index` (`day`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='每日余额统计';

DROP TABLE IF EXISTS  `pay_member_gold`;
CREATE TABLE `pay_member_gold` (
  `id` varchar(50) NOT NULL COMMENT '会员资产账户Id',
  `member_id` varchar(50) DEFAULT NULL COMMENT '会员Id',
  `total_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '总资产',
  `usable_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '可用资产',
  `principal_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '本金',
  `principal_now_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T日本金',
  `principal_history_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日本金',
  `principal_history_total_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日累计本金',
  `interest_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '活期利息',
  `interest_now_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T日利息',
  `interest_history_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日利息（可用）',
  `interest_history_total_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日累计利息',
  `freeze_gram` decimal(10,5) DEFAULT '0.00000' COMMENT '提金/存金冻结克重',
  `freeze_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '冻结资产',
  `freeze_buy_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '累计购买克重（未到期）',
  `freeze_income_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '总收益',
  `freeze_income_total_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '累计收益',
  `turn_into_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '转入总资产',
  `turn_out_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '转出总资产',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 会员资产账户';

DROP TABLE IF EXISTS  `pay_member_hold_gold`;
CREATE TABLE `pay_member_hold_gold` (
  `id` varchar(50) NOT NULL,
  `day` date NOT NULL COMMENT '日期',
  `amount` decimal(10,5) NOT NULL COMMENT '持有克重',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_day` (`day`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员每日持有量';

DROP TABLE IF EXISTS  `pay_operator_cost`;
CREATE TABLE `pay_operator_cost` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `operation_all_cost` decimal(10,2) NOT NULL COMMENT '每日线上运营成本总和',
  `experience_income_amount` decimal(10,2) DEFAULT NULL COMMENT '每日体验金奖励总成本（元)',
  `gold_coupon_all_cost` decimal(10,2) DEFAULT NULL COMMENT '每日黄金红包总成本（元)',
  `gold_bean_all_cost` decimal(10,2) DEFAULT NULL COMMENT '每日金豆抵扣总成本（元)',
  `lease_backall_cost` decimal(10,2) DEFAULT NULL COMMENT '每日回租福利券成本（元）',
  `shop_electronic_card` int(10) DEFAULT '0' COMMENT '每日商城满减券总成本（元）',
  `date` varchar(32) DEFAULT NULL COMMENT '日期',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=utf8mb4 COMMENT='线上运营成本统计';

DROP TABLE IF EXISTS  `pay_privilege_template`;
CREATE TABLE `pay_privilege_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `type` tinyint(4) NOT NULL COMMENT '模板类型 1.买金优惠模板 2.回租优惠模板',
  `scheme_type` tinyint(4) NOT NULL COMMENT '优惠方案类型 1.不设置优惠 2.按照单笔购买总克重优惠 3.按照会员等级优惠',
  `scheme_name` varchar(32) NOT NULL COMMENT '方案名称',
  `way` tinyint(4) DEFAULT NULL COMMENT '优惠方式 1.优惠金额 2.优惠比例 3.奖励金豆',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:启用 2:停用',
  `data` text COMMENT 'json',
  `add_time` varchar(32) NOT NULL COMMENT '操作时间',
  `modify_time` varchar(32) DEFAULT NULL COMMENT '状态变更时间',
  `deleted` tinyint(4) NOT NULL DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='买金,回租优惠模板';

DROP TABLE IF EXISTS  `pay_product_order`;
CREATE TABLE `pay_product_order` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `order_id` varchar(32) DEFAULT NULL COMMENT '订单号',
  `member_id` varchar(50) DEFAULT NULL COMMENT '会员Id',
  `product_id` varchar(50) DEFAULT NULL COMMENT '产品Id',
  `product_name` varchar(50) DEFAULT NULL COMMENT '产品名称',
  `type` int(11) DEFAULT '1' COMMENT '产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品',
  `invest_day` varchar(10) DEFAULT NULL COMMENT '投资期限  0:随时买卖 其他具体字典数据获取',
  `years_income` decimal(10,2) DEFAULT '0.00' COMMENT '年化收益',
  `amount` decimal(10,5) DEFAULT NULL COMMENT '购买克重',
  `gold_price` decimal(10,2) DEFAULT '0.00' COMMENT '购买金价',
  `income_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '预期收益',
  `start_time` datetime DEFAULT NULL COMMENT '计息时间',
  `end_time` datetime DEFAULT NULL COMMENT '到期时间',
  `pay_type` int(1) DEFAULT '1' COMMENT '支付类型 1:现金购买 2:金柚宝转入',
  `item_amount` decimal(10,2) DEFAULT '0.00' COMMENT '商品总价',
  `fee` decimal(10,2) DEFAULT '0.00' COMMENT '手续费',
  `actual_amount` decimal(10,2) DEFAULT '0.00' COMMENT '支付金额（商品总价+手续费）',
  `interest_history_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日利息（可用）',
  `principal_now_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T日本金',
  `principal_history_amount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日本金',
  `pay_status` int(11) DEFAULT '1' COMMENT '支付状态  1:待支付 2:已支付 3:已过期',
  `status` int(11) DEFAULT '1' COMMENT '产品状态  1:未到期 2:已到期',
  `stop_pay_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '支付截止时间',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 定期投资表';

DROP TABLE IF EXISTS  `pay_product_order_fee`;
CREATE TABLE `pay_product_order_fee` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `product_order_id` varchar(50) DEFAULT NULL COMMENT '投资记录ID',
  `order_id` varchar(50) DEFAULT NULL COMMENT '订单号',
  `coupon_red_id` varchar(50) DEFAULT NULL COMMENT '红包Id',
  `coupon_red_amount` decimal(10,2) DEFAULT '0.00' COMMENT '红包返现金额',
  `coupon_red_send_time` datetime DEFAULT NULL COMMENT '红包发放时间',
  `coupon_increase_id` varchar(50) DEFAULT NULL COMMENT '加息券Id',
  `coupon_increase_rate` decimal(10,2) DEFAULT '0.00' COMMENT '加息利率%',
  `total_rate_of_income` decimal(10,2) DEFAULT '0.00' COMMENT '总年化收益率%',
  `coupon_increase_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '加息收益',
  `coupon_increase_money` decimal(10,2) DEFAULT '0.00' COMMENT '加息收益（现金）',
  `coupon_increase_send_time` datetime DEFAULT NULL COMMENT '加息收益发放时间',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 定期投资费用表';

DROP TABLE IF EXISTS  `pay_record_interest`;
CREATE TABLE `pay_record_interest` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `member_id` varchar(50) DEFAULT NULL COMMENT '会员Id',
  `amount` decimal(10,5) DEFAULT '0.00000' COMMENT '利息',
  `overage_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '利息余额（利息-抵扣）',
  `send_time` datetime DEFAULT NULL COMMENT '发放时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：未到账 2：已到账',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 利息计算发放记录表';

DROP TABLE IF EXISTS  `pay_record_principal`;
CREATE TABLE `pay_record_principal` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `member_id` varchar(50) DEFAULT NULL COMMENT '会员Id',
  `amount` decimal(10,5) DEFAULT '0.00000' COMMENT '本金',
  `overage_amount` decimal(10,5) DEFAULT '0.00000' COMMENT '本金余额（本金-抵扣）',
  `interest_time` datetime DEFAULT NULL COMMENT '计息时间',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：未计息 2：已计息',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[财务-资产] 活期本金记录表';

DROP TABLE IF EXISTS  `pay_tbill`;
CREATE TABLE `pay_tbill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识(提单编号)',
  `member_id` varchar(64) NOT NULL COMMENT '提单用户',
  `order_no` varchar(32) NOT NULL COMMENT '提单订单号',
  `product_id` varchar(64) NOT NULL COMMENT '提单产品',
  `product_name` varchar(128) NOT NULL COMMENT '提单产品名称',
  `add_time` varchar(32) NOT NULL COMMENT '提单申请时间',
  `end_time` varchar(32) NOT NULL COMMENT '提单支付结束时间',
  `success_time` varchar(32) DEFAULT NULL COMMENT '支付成功时间',
  `gold_price` decimal(10,2) NOT NULL COMMENT '提单时金价（元/克）',
  `processing_fee` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '加工费',
  `beans` int(10) NOT NULL DEFAULT '0' COMMENT '抵扣金豆（个）',
  `bean_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '抵扣金豆（克）',
  `coupon_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '优惠券抵扣（克）',
  `coupon_id` int(10) DEFAULT NULL COMMENT '优惠券ID',
  `total_gram` int(10) NOT NULL DEFAULT '0' COMMENT '购买克重',
  `pay_gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '实际支付克重',
  `portion` int(10) NOT NULL DEFAULT '0' COMMENT '提单份数',
  `fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `total_amount` decimal(20,2) NOT NULL COMMENT '商品金额',
  `pay_amount` decimal(20,2) NOT NULL COMMENT '实际支付金额',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '提金状态：(0-待支付；1-已过期; 2-待处理（支付成功）；3-已回租；4-提金冻结中（已预约和待收货）;5-已提金;6-已出售;7:失败',
  `source` varchar(32) DEFAULT NULL COMMENT '买金/回租到期/回购',
  `frozen_time` varchar(32) DEFAULT NULL COMMENT '提单冻结时间',
  `modify_time` varchar(32) DEFAULT NULL COMMENT '最新状态变更时间',
  `lock_days` int(10) DEFAULT '0' COMMENT '回租期限最低期限',
  `leaseback_times` int(10) NOT NULL DEFAULT '0' COMMENT '回租已到期次数',
  `readed` int(10) NOT NULL DEFAULT '0' COMMENT '是否被阅读：0-未被阅读；1-已被阅读',
  `discount_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '优惠额度',
  `first_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '首单优惠',
  `discount_amount_type` int(11) DEFAULT '1' COMMENT '优惠额度类型：1-现金；2-黄金；3-金豆',
  `first_amount_type` int(11) NOT NULL DEFAULT '1' COMMENT '首单优惠类型：1-现金；2-金豆',
  `lease_id` int(10) DEFAULT '0' COMMENT '回租模板id',
  `ticket_id` int(10) DEFAULT '0' COMMENT '回租福利券ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_no` (`order_no`),
  KEY `index_status` (`status`) USING BTREE,
  KEY `index_member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1881 DEFAULT CHARSET=utf8mb4 COMMENT='提单记录表';

DROP TABLE IF EXISTS  `pay_tbill_leaseback`;
CREATE TABLE `pay_tbill_leaseback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `tbill_no` varchar(32) NOT NULL COMMENT '提单订单号',
  `order_no` varchar(32) NOT NULL COMMENT '回租订单号',
  `start_time` varchar(32) NOT NULL COMMENT '回租开始时间',
  `end_time` varchar(32) NOT NULL COMMENT '回租结束时间',
  `gold_price` decimal(10,2) NOT NULL COMMENT '回租金价',
  `leaseback_days` int(10) NOT NULL COMMENT '回租期限',
  `year_income` decimal(10,2) NOT NULL COMMENT '回租年化',
  `income_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '回租收益（元）',
  `income_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '回租收益（克）',
  `income_beans` int(10) NOT NULL COMMENT '回租收益（金豆）',
  `coupon_id` varchar(64) NOT NULL COMMENT '福利券ID',
  `coupon_award_amount` decimal(10,2) DEFAULT '0.00' COMMENT '福利券奖励金额',
  `coupon_amount` int(10) NOT NULL DEFAULT '0' COMMENT '福利券减免天数',
  `coupon_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '福利券奖励（克）',
  `coupon_beans` int(10) NOT NULL DEFAULT '0' COMMENT '福利券奖励（金豆）',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '奖励发放状态：0--未发放；1--已发放',
  `grant_time` varchar(32) DEFAULT NULL COMMENT '奖励到账时间',
  `discount_amount` decimal(20,2) DEFAULT '0.00' COMMENT '回租奖励',
  `coupon_year_income` decimal(10,2) DEFAULT '0.00' COMMENT '福利券对应年化 %',
  `actual_time` varchar(32) NOT NULL COMMENT '使用福利券后实际到期时间',
  `income_type` int(10) NOT NULL DEFAULT '1' COMMENT '奖励发放方式：1--现金；2--金豆',
  `discount_amount_type` int(10) NOT NULL DEFAULT '1' COMMENT '回租奖励类型：1-现金；3-金豆',
  `gold_give_gold_amount` double(20,2) NOT NULL DEFAULT '0.00' COMMENT '回租金生金奖励',
  `coupon_type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '福利卷类型 0:黄金红包  1:回租卷',
  PRIMARY KEY (`id`),
  KEY `idx_tbillNo` (`tbill_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1008 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_tbill_leaseback_income`;
CREATE TABLE `pay_tbill_leaseback_income` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `income_day` varchar(32) NOT NULL COMMENT '收益日期',
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `leaseback_income_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '回租未到期收益（元）',
  `leaseback_income_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '回租未到期收益（克）',
  `leaseback_income_beans` int(10) NOT NULL DEFAULT '0' COMMENT '回租未到期收益（金豆）',
  `income_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '回租已到期收益（元）',
  `income_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '回租已到期收益（克）',
  `income_beans` int(10) NOT NULL DEFAULT '0' COMMENT '回租已到期收益（金豆）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id_day` (`income_day`,`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52583 DEFAULT CHARSET=utf8mb4 COMMENT='会员每日回租收益详表';

DROP TABLE IF EXISTS  `pay_tbill_sold`;
CREATE TABLE `pay_tbill_sold` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `tbill_no` varchar(32) NOT NULL COMMENT '提单订单号',
  `order_no` varchar(32) NOT NULL COMMENT '出售订单号',
  `gold_price` decimal(10,2) NOT NULL COMMENT '出售时金价',
  `total_amount` decimal(20,2) NOT NULL COMMENT '总金额',
  `fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `coupon_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '扣减已优惠金额',
  `actual_amount` decimal(20,2) NOT NULL COMMENT '实际获得金额',
  `add_time` varchar(32) NOT NULL COMMENT '出售时间',
  `success_time` varchar(32) DEFAULT NULL COMMENT '到账时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=955 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_tbill_statistics`;
CREATE TABLE `pay_tbill_statistics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` varchar(32) NOT NULL COMMENT '会员ID',
  `total_bills` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '累计提单个数',
  `total_gram` int(10) NOT NULL DEFAULT '0' COMMENT '累计提单克重',
  `pending_bills` int(10) NOT NULL DEFAULT '0' COMMENT '待处理提单个数',
  `pending_gram` int(10) NOT NULL DEFAULT '0' COMMENT '待处理提单克重',
  `leaseback_bills` int(10) NOT NULL DEFAULT '0' COMMENT '回租中提单个数',
  `leaseback_gram` int(10) NOT NULL DEFAULT '0' COMMENT '回租中提单克重',
  `leaseback_income_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '回租未到期总收益（元）',
  `leaseback_income_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '回租未到期总收益（克）',
  `leaseback_income_beans` int(10) NOT NULL DEFAULT '0' COMMENT '回租未到期总收益（金豆）',
  `sold_bills` int(10) NOT NULL DEFAULT '0' COMMENT '已出售提单个数',
  `sold_gram` int(10) NOT NULL DEFAULT '0' COMMENT '已出售提单克重',
  `sold_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '已售提单到账总金额（元）',
  `frozen_bills` int(10) NOT NULL DEFAULT '0' COMMENT '提金冻结中提单个数',
  `frozen_gram` int(10) NOT NULL DEFAULT '0' COMMENT '提金冻结克重',
  `extracted_bills` int(10) NOT NULL DEFAULT '0' COMMENT '已提金个数',
  `extracted_gram` int(10) NOT NULL DEFAULT '0' COMMENT '已提金克重',
  `income_amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '回租已到期总收益（元）',
  `income_gram` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '回租已到期总收益（克）',
  `income_beans` int(10) NOT NULL DEFAULT '0' COMMENT '回租已到期总收益（金豆）',
  `add_time` varchar(32) NOT NULL COMMENT '首次提单时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_member_id` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8mb4 COMMENT='提单账户统计表';

DROP TABLE IF EXISTS  `pay_tbill_status`;
CREATE TABLE `pay_tbill_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `tbill_no` varchar(32) NOT NULL COMMENT '提单订单号',
  `order_no` varchar(32) NOT NULL COMMENT '操作流水号',
  `type` varchar(32) NOT NULL COMMENT '类型：购买 回租 出售 提金 回购 支付金饰 回租到期',
  `add_time` varchar(32) NOT NULL COMMENT '操作时间',
  `status` int(10) NOT NULL COMMENT '提金状态：2-待处理（支付成功）；3-已回租；4-提金冻结中（已预约和待收货）;5-已提金;6-已出售',
  `modify_time` varchar(32) DEFAULT NULL COMMENT '状态变更时间',
  `remark` varchar(32) DEFAULT NULL COMMENT '状态备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2913 DEFAULT CHARSET=utf8mb4 COMMENT='提单状态变更表';

DROP TABLE IF EXISTS  `pay_trade`;
CREATE TABLE `pay_trade` (
  `id` varchar(50) NOT NULL COMMENT '支付Id',
  `member_id` varchar(50) NOT NULL COMMENT '会员Id',
  `title` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `order_id` varchar(50) NOT NULL COMMENT '订单号',
  `third_number` varchar(64) DEFAULT NULL COMMENT '第三方支付流水号',
  `amount` decimal(10,2) NOT NULL COMMENT '金额',
  `pay_amount` decimal(10,2) DEFAULT NULL COMMENT '实际支付金额',
  `fee` decimal(10,2) DEFAULT NULL COMMENT '手续费',
  `status` int(11) NOT NULL COMMENT '交易状态 1:处理中 2:已支付 3:支付失败',
  `type` int(11) NOT NULL COMMENT '交易类型 1:充值 2:买金 3:商城',
  `pay_way` int(11) NOT NULL COMMENT '支付方式 1:易宝支付',
  `bank_card_id` varchar(50) DEFAULT NULL COMMENT '绑卡Id',
  `serial_number` varchar(50) DEFAULT NULL COMMENT '扩展订单号--首次订单号一致,其余变化',
  `description` varchar(255) DEFAULT NULL COMMENT '备注 --失败信息',
  `err_code` varchar(50) DEFAULT NULL COMMENT '失败信息-错误编号',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_id` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_transfer_accounts`;
CREATE TABLE `pay_transfer_accounts` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `member_id` varchar(50) NOT NULL COMMENT '转账会员Id',
  `transfer_numbers` varchar(50) NOT NULL COMMENT '用户转账单号',
  `transfer_name` varchar(50) NOT NULL COMMENT '转账户名',
  `open_bank` varchar(50) NOT NULL COMMENT '转账开户行',
  `bank_account` varchar(50) NOT NULL COMMENT '转账账号',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '转账金额',
  `voucher_img` varchar(2000) NOT NULL COMMENT '凭证单据图片',
  `audit_status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '审核状态 1:待审核 2:已通过 3:已拒绝',
  `reject_remark` varchar(50) DEFAULT NULL COMMENT '拒绝原因备注',
  `create_user_id` varchar(50) NOT NULL COMMENT '创建用户ID',
  `create_user` varchar(50) NOT NULL COMMENT '创建用户姓名',
  `audit_user_id` varchar(50) DEFAULT NULL COMMENT '审核用户ID',
  `audit_user` varchar(50) DEFAULT NULL COMMENT '审核用户姓名',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `apply_time` datetime NOT NULL COMMENT '申请时间',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='转账申请表';

DROP TABLE IF EXISTS  `pay_withdraw`;
CREATE TABLE `pay_withdraw` (
  `id` varchar(50) NOT NULL COMMENT '提现id',
  `order_id` varchar(50) NOT NULL COMMENT '订单号',
  `third_number` varchar(64) DEFAULT NULL COMMENT '第三方流水号',
  `bank_card_id` varchar(50) NOT NULL COMMENT '绑定银行卡ID',
  `member_id` varchar(50) NOT NULL COMMENT '会员id',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `fee` decimal(10,2) DEFAULT '0.00' COMMENT '手续费',
  `actual_amount` decimal(10,2) DEFAULT '0.00' COMMENT '实际金额(金额-手续费)',
  `type` int(11) NOT NULL DEFAULT '2' COMMENT '提现类型  1: T+0到账 2: T+1到账',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '提现状态  1:待提现 2:处理中 3:成功 4:失败',
  `audit_status` int(11) DEFAULT '1' COMMENT '审核状态  1:未审核 2:一级审核成功 3一级审核失败 4:二级审核成功 5:二级审核失败',
  `audit_description_one` varchar(200) DEFAULT NULL COMMENT '一级审核描述',
  `audit_user_id_one` varchar(50) DEFAULT NULL COMMENT '一级审核人用户Id',
  `audit_user_name_one` varchar(50) DEFAULT NULL COMMENT '一级审核人用户名称',
  `audit_time_one` datetime DEFAULT NULL COMMENT '一级审核时间',
  `audit_description_two` varchar(200) DEFAULT NULL COMMENT '二级审核描述',
  `audit_user_id_two` varchar(50) DEFAULT NULL COMMENT '二级审核人用户Id',
  `audit_user_name_two` varchar(50) DEFAULT NULL COMMENT '二级审核人用户名称',
  `audit_time_two` datetime DEFAULT NULL COMMENT '二级审核时间',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `balance` decimal(10,2) DEFAULT '0.00' COMMENT '提现钱余额',
  PRIMARY KEY (`id`),
  KEY `index_member_id` (`member_id`) USING BTREE,
  KEY `index_status` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='提现表';

SET FOREIGN_KEY_CHECKS = 1;


SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS  `pay_withdraw_setting`;
CREATE TABLE `pay_withdraw_setting` (
  `id` varchar(50) NOT NULL,
  `bank_id` varchar(50) NOT NULL COMMENT '银行id',
  `once_type` int(11) NOT NULL DEFAULT '1' COMMENT '单笔是否限额 1：不限 2：限额',
  `day_type` int(11) NOT NULL DEFAULT '1' COMMENT '当日是否限额 1：不限 2：限额',
  `month_type` int(11) NOT NULL DEFAULT '1' COMMENT '当月是否限额 1:不限 2:限额',
  `amount_once` decimal(10,2) DEFAULT NULL COMMENT '单笔上线金额',
  `amount_day` decimal(10,2) DEFAULT NULL COMMENT '单日上线金额',
  `amount_month` decimal(10,2) DEFAULT NULL COMMENT '单月上线金额',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `pay_yeepay_bill`;
CREATE TABLE `pay_yeepay_bill` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `type` int(10) NOT NULL COMMENT '类型：1--充值；2--提现',
  `pay_time` varchar(32) NOT NULL COMMENT '交易时间',
  `settle_time` varchar(32) DEFAULT NULL COMMENT '结算时间',
  `order_no` varchar(64) NOT NULL COMMENT '柚子黄金订单号',
  `serial_no` varchar(64) NOT NULL COMMENT '易宝订单号',
  `amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '交易金额',
  `fee` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '交易手续费',
  `status` int(10) NOT NULL DEFAULT '1' COMMENT '状态：1--成功；0--失败',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_order_no` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `product`;
CREATE TABLE `product` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `name` varchar(50) NOT NULL COMMENT '产品名称',
  `type` int(11) NOT NULL COMMENT '产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品 6:实物金',
  `up_time` datetime DEFAULT NULL COMMENT '上线时间',
  `effective_day` varchar(10) NOT NULL COMMENT '有效时间(也叫募集时间)  0:表示无具体限制',
  `years_income` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '年化收益',
  `invest_day` varchar(10) NOT NULL COMMENT '投资期限  0:随时买卖 其他具体字典数据获取',
  `interest_accrual_type` int(11) DEFAULT NULL COMMENT '计息方式(0:不计息 1:按天计息 2:到期计息)',
  `interest_accrual_day` int(11) DEFAULT '0' COMMENT '计息时间  默认：T+0',
  `risk_type` int(11) DEFAULT NULL COMMENT '风险类型(1:保本型 2:非保本型)',
  `settlement_type` int(11) DEFAULT NULL COMMENT '结算方式(1.按照克重结算 2:按照现金结算)',
  `setting_price_type` int(11) DEFAULT NULL COMMENT '金价类型设置(1:实时金价 2:自定义价格)',
  `setting_price` decimal(10,2) DEFAULT '0.00' COMMENT '自定义黄金价格',
  `price_min` decimal(10,2) DEFAULT '0.00' COMMENT '最低价格',
  `price_max` decimal(10,2) DEFAULT '0.00' COMMENT '最高价格',
  `gram_min` decimal(10,2) DEFAULT '0.00' COMMENT '最低克数',
  `gram_max` decimal(10,2) DEFAULT '0.00' COMMENT '最高克数',
  `raise_gram` decimal(10,0) DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '产品状态 1:正常 2:作废',
  `shelf_state` int(11) NOT NULL DEFAULT '2' COMMENT '上架状态 1:上架 2:下架',
  `audit_status` int(11) NOT NULL DEFAULT '1' COMMENT '审核状态 1:待审核 2:已通过 3:已拒绝',
  `is_open` int(10) DEFAULT '1' COMMENT '是否开启交易 1:是 2:否',
  `is_hot` int(11) NOT NULL DEFAULT '1' COMMENT '热销产品 1:否 2:是',
  `repayment_way` varchar(50) DEFAULT NULL COMMENT '还款方式 具体字典表查查询',
  `standard_way` varchar(50) DEFAULT NULL COMMENT '成标方式 具体字典表查询',
  `detail` text COMMENT '产品描述',
  `label` varchar(50) DEFAULT NULL COMMENT '产品标签(包含两个标签，用逗号隔开)',
  `income_label` varchar(50) DEFAULT NULL COMMENT '收益标签',
  `insurance` text COMMENT '安全保障',
  `protocol_id` varchar(50) DEFAULT NULL COMMENT '协议类型',
  `stop_time` datetime DEFAULT NULL COMMENT '产品结束时间',
  `create_user_id` varchar(50) DEFAULT NULL COMMENT '创建用户ID',
  `create_user` varchar(50) DEFAULT NULL COMMENT '创建用户姓名',
  `audit_user_id` varchar(50) DEFAULT NULL COMMENT '审核用户ID',
  `audit_user` varchar(50) DEFAULT NULL COMMENT '审核用户姓名',
  `audit_description` varchar(200) DEFAULT NULL COMMENT '审核描述',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `to_raise_gram` decimal(10,5) DEFAULT '0.00000',
  `is_activity` int(11) DEFAULT '2',
  `img` varchar(500) DEFAULT NULL,
  `specification` int(10) DEFAULT NULL COMMENT '产品规格',
  `market_price` decimal(10,2) DEFAULT NULL COMMENT '产品市场价',
  `can_buy_user_type` tinyint(4) DEFAULT '1' COMMENT '可购买用户 1平台全部用户，2复投用户，3首投用户',
  `user_astrict_status` tinyint(4) DEFAULT '0' COMMENT '用户限购 0不限购买次数,1限购购买数量, 2限制最高购买克重',
  `user_astrict_num` int(11) DEFAULT NULL COMMENT '用户限购数量',
  `user_astrict_gram` int(11) DEFAULT NULL COMMENT '用户限购最高克重',
  `award_toaccount_day` int(11) DEFAULT '1' COMMENT '奖励到账日期',
  `entity_item_no` varchar(50) DEFAULT NULL COMMENT '实物金货号',
  `process_id` int(10) DEFAULT NULL COMMENT '加工模板id',
  `lease_close_type` tinyint(4) DEFAULT NULL COMMENT '回租奖励结算方式 1以现金结算 2金豆结算',
  `lease_calculate_gold_type` tinyint(4) DEFAULT NULL COMMENT '回租奖励计算金价 1.回租前一日收盘价  2.回租前一日某个时间点金价',
  `lease_calculate_gold_time` varchar(50) DEFAULT NULL COMMENT '回租前一日时间点 时间精确到分 如12:00',
  `award_setting` tinyint(4) DEFAULT '0' COMMENT '首次回租奖励设置 0不设置奖励',
  `heading` varchar(50) DEFAULT NULL COMMENT '产品主标题',
  `heading_notes` varchar(50) DEFAULT NULL COMMENT '主标题注释',
  `subhead` varchar(50) DEFAULT NULL COMMENT '产品副标题,多个以逗号隔开',
  `lease_protocol_id` varchar(50) DEFAULT NULL COMMENT '实物金回租协议id',
  `stock` int(20) DEFAULT '0' COMMENT '库存',
  `buy_award_setting` tinyint(4) DEFAULT '0' COMMENT '首次购买优惠 0不设置优惠',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品列表';

DROP TABLE IF EXISTS  `product_bonus_mapping`;
CREATE TABLE `product_bonus_mapping` (
  `id` varchar(64) NOT NULL COMMENT '唯一标识',
  `product_id` varchar(64) NOT NULL COMMENT '产品id',
  `bonus_id` varchar(64) NOT NULL COMMENT '返利id',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `product_discount_settings`;
CREATE TABLE `product_discount_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `new_member_num` int(11) DEFAULT NULL COMMENT '新用户初始立减次数',
  `old_member_num` int(11) DEFAULT NULL COMMENT '老用户初始立减次数',
  `max_num` int(11) NOT NULL COMMENT '立减次数上限',
  `ratio` int(11) NOT NULL COMMENT '邀请人数和立减次数比例',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='产品-折扣金邀请好友配置';

DROP TABLE IF EXISTS  `product_gold_price_baseline`;
CREATE TABLE `product_gold_price_baseline` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `gold_price` decimal(10,2) DEFAULT '0.00' COMMENT '基准金价',
  `limit` decimal(10,2) DEFAULT '0.00' COMMENT '涨跌幅（%）',
  `max_price` decimal(10,2) DEFAULT '0.00' COMMENT '最高价',
  `min_price` decimal(10,2) DEFAULT '0.00' COMMENT '最低价',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[产品] 黄金交易价格记录';

DROP TABLE IF EXISTS  `product_gold_price_record`;
CREATE TABLE `product_gold_price_record` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `type` int(11) DEFAULT '1' COMMENT '类型 1:上海黄金交易所',
  `variety` varchar(50) DEFAULT NULL COMMENT '品种',
  `latestpri` decimal(10,2) DEFAULT '0.00' COMMENT '最新价',
  `actualpri` decimal(10,2) DEFAULT '0.00' COMMENT '实际价格',
  `openpri` decimal(10,2) DEFAULT '0.00' COMMENT '开盘价',
  `maxpri` decimal(10,2) DEFAULT '0.00' COMMENT '最高价',
  `minpri` decimal(10,2) DEFAULT '0.00' COMMENT '最低价',
  `limit` decimal(10,2) DEFAULT '0.00' COMMENT '涨跌幅（%）',
  `yespri` decimal(10,2) DEFAULT '0.00' COMMENT '昨收价',
  `totalvol` decimal(15,5) DEFAULT '0.00000' COMMENT '总成交量',
  `time` datetime DEFAULT NULL COMMENT '更新时间',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`),
  KEY `idx_addtime_latestpri` (`add_time`,`latestpri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[产品] 黄金交易价格记录';

DROP TABLE IF EXISTS  `product_img`;
CREATE TABLE `product_img` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `type` tinyint(4) NOT NULL COMMENT '图片类型，1.头图图片  2.图文详情',
  `img_url` varchar(200) NOT NULL COMMENT '图片url地址',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COMMENT='产品图片';

DROP TABLE IF EXISTS  `product_info`;
CREATE TABLE `product_info` (
  `id` varchar(50) NOT NULL COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `sort` int(11) DEFAULT '0',
  `title` varchar(10) DEFAULT NULL COMMENT '标题',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品详情';

DROP TABLE IF EXISTS  `product_lease_info`;
CREATE TABLE `product_lease_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `lease_day` int(11) NOT NULL COMMENT '回租期限',
  `years_income` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '年化收益',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8mb4 COMMENT='产品回租期限信息';

DROP TABLE IF EXISTS  `product_member_invite_record`;
CREATE TABLE `product_member_invite_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `friend_id` varchar(64) NOT NULL COMMENT '邀请人会员id ',
  `member_id` varchar(50) DEFAULT NULL COMMENT '被邀请人会员id',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='产品-折扣金邀请好友记录';

DROP TABLE IF EXISTS  `product_member_subtract`;
CREATE TABLE `product_member_subtract` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `member_id` varchar(64) NOT NULL COMMENT '会员id ',
  `num` int(11) DEFAULT NULL COMMENT '立减次数',
  `invite_num` int(11) DEFAULT NULL COMMENT '邀请人数',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_member_subtract_member_id_product_id_pk` (`member_id`,`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=670 DEFAULT CHARSET=utf8mb4 COMMENT='产品-折扣金立减次数记录表';

DROP TABLE IF EXISTS  `product_publicity`;
CREATE TABLE `product_publicity` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `content` varchar(500) DEFAULT NULL COMMENT '内容',
  `img_url` varchar(200) NOT NULL COMMENT '图片url地址',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COMMENT='产品宣传信息';

DROP TABLE IF EXISTS  `product_trade_time`;
CREATE TABLE `product_trade_time` (
  `id` varchar(50) NOT NULL,
  `day` date NOT NULL COMMENT '日期',
  `product_id` varchar(50) NOT NULL COMMENT '产品id',
  `template_id` varchar(50) NOT NULL COMMENT '模版id',
  `type` int(11) NOT NULL COMMENT '类型 1:买入 2:卖出',
  `detail` varchar(25) NOT NULL COMMENT '描述 周一,周二等等',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品交易时间段';

DROP TABLE IF EXISTS  `share_join_member`;
CREATE TABLE `share_join_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `member_id` varchar(64) NOT NULL COMMENT '会员ID',
  `order_no` varchar(32) DEFAULT NULL COMMENT '业务订单号  TB:提单, LB:提单回租 TS:提单出售 PE:购买体验金 PG:商城  BB:金料回购 EG:提金',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '参与时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71119 DEFAULT CHARSET=utf8mb4 COMMENT='抽奖活动参与用户';

DROP TABLE IF EXISTS  `share_member`;
CREATE TABLE `share_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `member_id` varchar(64) NOT NULL COMMENT '抽奖用户ID',
  `prize_id` int(10) NOT NULL COMMENT '奖品ID',
  `amount` double(20,3) DEFAULT '0.000' COMMENT '金额',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '抽奖时间',
  `modify_time` datetime DEFAULT NULL COMMENT '领取时间',
  `status` int(10) NOT NULL DEFAULT '0' COMMENT '奖品领取状态：0：未领取；1：已领取',
  `rewards_status` int(10) NOT NULL DEFAULT '0' COMMENT '中奖状态：0：未中奖；1：已中奖',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71142 DEFAULT CHARSET=utf8mb4 COMMENT='用户中奖记录';

DROP TABLE IF EXISTS  `share_prize`;
CREATE TABLE `share_prize` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `group_type` int(11) NOT NULL COMMENT '用户组类型 1.注册未绑卡用户(系统) 2.绑卡未交易用户(系统) 3.已产生交易用户(系统) 4.自定义创建(自定义)',
  `group_id` int(10) NOT NULL COMMENT '用户群Id',
  `name` varchar(32) NOT NULL COMMENT '奖品名称',
  `type` int(10) NOT NULL DEFAULT '7' COMMENT '奖励类型 1.体验金、2.金豆、3.黄金红包、4.回租福利券、5.商城满减券、6.现金奖励',
  `amount` double(20,3) DEFAULT '0.000' COMMENT '金额（现金金额或黄金克重,额度,金豆）',
  `quantity_limit` int(10) NOT NULL DEFAULT '0' COMMENT '数量限制：0：不限制；1：限制；',
  `quantity` int(10) DEFAULT '0' COMMENT '奖品数量',
  `coupon_ids` text COMMENT '优惠券Id，多个用","分割',
  `probability` double(20,3) NOT NULL DEFAULT '0.000' COMMENT '中奖概率',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='分享商品设置';

DROP TABLE IF EXISTS  `share_setting`;
CREATE TABLE `share_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `type` tinyint(4) NOT NULL COMMENT '1.分享限制 2.基础设置',
  `switch_status` tinyint(4) DEFAULT NULL COMMENT '分享限制开关(type为1时必输) 1.显示 2.关闭',
  `scene` varchar(32) DEFAULT NULL COMMENT '触发场景类型 多个以逗号进行区分 1.实物金购买成功 2.体验金购买成功 3.回租提单成功 4.出售提单成功 5.提金预约成功 6.商城购买成功 7.金料回购成功',
  `share_img_remark` varchar(200) DEFAULT NULL COMMENT '分享弹层插图说明',
  `share_img` varchar(128) DEFAULT NULL COMMENT '分享弹层插图',
  `share_title` varchar(32) DEFAULT NULL COMMENT '分享标题',
  `share_remark` text COMMENT '分享摘要',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='分享设置';

DROP TABLE IF EXISTS  `sys_account`;
CREATE TABLE `sys_account` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `login_name` varchar(50) NOT NULL COMMENT '登陆名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1：启用  2：禁用',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 系统维护账号';

DROP TABLE IF EXISTS  `sys_area`;
CREATE TABLE `sys_area` (
  `id` varchar(10) NOT NULL COMMENT '地域主键ID',
  `city_code` varchar(10) NOT NULL COMMENT '城市代码',
  `name` varchar(50) NOT NULL COMMENT '城市名称',
  `en_name` varchar(20) NOT NULL COMMENT '城市名称简拼',
  `division` tinyint(2) NOT NULL DEFAULT '0' COMMENT '城市乡村划分（0-省级，1-城市，2-乡村）',
  `parent_id` varchar(10) NOT NULL COMMENT '上级id',
  `longitude` decimal(20,10) DEFAULT NULL COMMENT '经度',
  `latitude` decimal(20,10) DEFAULT NULL COMMENT '纬度',
  `level` int(2) NOT NULL COMMENT '城市分级（1-省，2-市，3-县）',
  PRIMARY KEY (`id`),
  KEY `idx_area_code` (`city_code`) USING BTREE,
  KEY `idx_area_name` (`name`) USING BTREE,
  KEY `idx_area_pcode` (`division`) USING BTREE,
  KEY `idx_parent_id` (`parent_id`) USING BTREE,
  KEY `idx_level` (`level`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='国家行政地域编码表。描述全国省市区县的行政编码信息。';

DROP TABLE IF EXISTS  `sys_authorize`;
CREATE TABLE `sys_authorize` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `layer` varchar(500) DEFAULT NULL COMMENT '级别',
  `action_value` varchar(500) DEFAULT NULL COMMENT '用来描述某功能具体按钮动作',
  `sort` int(11) DEFAULT '1' COMMENT '同级别降序排列',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` int(11) DEFAULT '1' COMMENT '状态 1：启用 2：禁用',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 权限（动作）';

DROP TABLE IF EXISTS  `sys_banner`;
CREATE TABLE `sys_banner` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `title` varchar(255) NOT NULL COMMENT '描述',
  `img_url` varchar(255) NOT NULL COMMENT 'banner图片地址',
  `url` varchar(255) DEFAULT NULL COMMENT '连接',
  `status` int(11) NOT NULL DEFAULT '2' COMMENT '状态 1:显示 2:不显示',
  `sort` int(11) DEFAULT '1' COMMENT '顺序',
  `start_time` datetime DEFAULT NULL COMMENT '有效开始时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `sys_data_dictionary`;
CREATE TABLE `sys_data_dictionary` (
  `id` varchar(50) NOT NULL COMMENT '编号',
  `classification` varchar(50) DEFAULT NULL COMMENT '分类',
  `item_name` varchar(50) DEFAULT NULL COMMENT '项目名称',
  `item_value` varchar(50) DEFAULT NULL COMMENT '项目值',
  `sort` int(11) DEFAULT '1' COMMENT '降序排列',
  `type` int(11) DEFAULT '1' COMMENT '类型：1：系统 2：自定义',
  `status` int(11) DEFAULT '1' COMMENT '状态：1：启用 2：不启用',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 数据字典（常用数据项）';

DROP TABLE IF EXISTS  `sys_department`;
CREATE TABLE `sys_department` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `code` varchar(50) DEFAULT NULL COMMENT '编号',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `layer` varchar(100) DEFAULT NULL COMMENT '级别',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 部门管理';

DROP TABLE IF EXISTS  `sys_log`;
CREATE TABLE `sys_log` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `type` varchar(50) DEFAULT NULL COMMENT '功能 登录、注销、支付、下单等',
  `third_number` varchar(50) DEFAULT NULL COMMENT '功能流水号',
  `user_type` int(50) DEFAULT '0' COMMENT '用户类型 1：维护账号 2：用户 3：会员',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户名称',
  `user_login_name` varchar(50) DEFAULT NULL COMMENT '用户登录名',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 操作日志';

DROP TABLE IF EXISTS  `sys_personnel`;
CREATE TABLE `sys_personnel` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `code` varchar(50) NOT NULL COMMENT '编号',
  `login_name` varchar(50) NOT NULL COMMENT '登录名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(50) DEFAULT NULL COMMENT '手机',
  `id_card` varchar(50) DEFAULT NULL COMMENT '身份证',
  `gender` int(11) DEFAULT '3' COMMENT '1：男 2：女 3：未设置',
  `birthday` varchar(50) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `sort` int(11) DEFAULT '1' COMMENT '降序排列',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1：启用  2：禁用',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 人员';

DROP TABLE IF EXISTS  `sys_personnel_department_mapping`;
CREATE TABLE `sys_personnel_department_mapping` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `personnel_id` varchar(50) DEFAULT NULL COMMENT '人员ID',
  `department_id` varchar(50) DEFAULT NULL COMMENT '部门ID',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 人员与部门映射表';

DROP TABLE IF EXISTS  `sys_personnel_role_mapping`;
CREATE TABLE `sys_personnel_role_mapping` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `personnel_id` varchar(50) DEFAULT NULL COMMENT '人员ID',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 人员与角色映射表';

DROP TABLE IF EXISTS  `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态 1：启用  2：禁用',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 角色';

DROP TABLE IF EXISTS  `sys_role_authorize_mapping`;
CREATE TABLE `sys_role_authorize_mapping` (
  `id` varchar(50) NOT NULL COMMENT 'ID',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `authorize_id` varchar(50) DEFAULT NULL COMMENT '权限ID',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='[系统管理] 角色权限映射表';

DROP TABLE IF EXISTS  `sys_version`;
CREATE TABLE `sys_version` (
  `id` varchar(50) NOT NULL COMMENT '自增Id',
  `number` varchar(50) NOT NULL COMMENT '版本号',
  `url` varchar(255) DEFAULT NULL COMMENT '版本更新地址',
  `content` varchar(500) NOT NULL COMMENT '更新内容',
  `type` int(11) NOT NULL COMMENT '类型 1:android 2:IOS',
  `force_update` int(11) NOT NULL COMMENT '强制更新 1:否 2:是',
  `create_user_id` varchar(50) NOT NULL COMMENT '创建人用户Id',
  `create_user` varchar(50) NOT NULL COMMENT '创建人姓名',
  `add_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` int(11) NOT NULL DEFAULT '1' COMMENT '删除 1：否 2：是',
  `published` int(11) DEFAULT '0' COMMENT '是否发布，0：未发布，1：已发布',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `tb_address`;
CREATE TABLE `tb_address` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT COMMENT '收获地址Id',
  `userId` varchar(50) NOT NULL DEFAULT '' COMMENT '用户id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `mobile` varchar(25) NOT NULL DEFAULT '' COMMENT '手机号码或者固定电话',
  `province` varchar(20) NOT NULL DEFAULT '' COMMENT '省份',
  `city` varchar(20) NOT NULL DEFAULT '' COMMENT '城市',
  `area` varchar(20) NOT NULL DEFAULT '' COMMENT '地区',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `isDefault` int(11) NOT NULL DEFAULT '0' COMMENT '是否默认 0:否  1:是',
  `isDel` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除 -1:删除 0:有效',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COMMENT='[会员] 收货地址';

DROP TABLE IF EXISTS  `tb_brand`;
CREATE TABLE `tb_brand` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '品牌id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '品牌名',
  `remark` varchar(60) NOT NULL DEFAULT '' COMMENT '备注',
  `isDel` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否删除 0:有效，1:无效',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COMMENT='品牌';

DROP TABLE IF EXISTS  `tb_category`;
CREATE TABLE `tb_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '分类名',
  `parentId` bigint(20) NOT NULL DEFAULT '0' COMMENT '父分类id',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `remark` varchar(40) NOT NULL DEFAULT '' COMMENT '备注',
  `isDel` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否删除 0:有效，1:无效',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parentId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COMMENT='商品分类';

DROP TABLE IF EXISTS  `tb_channel`;
CREATE TABLE `tb_channel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL DEFAULT '' COMMENT '渠道名称',
  `number` varchar(20) NOT NULL DEFAULT '' COMMENT '渠道编号',
  `planType` tinyint(4) NOT NULL DEFAULT '0' COMMENT '结算方案 1:阶梯交易方案 2:交易周期方案 3:交易频数方案 暂时只有1',
  `period` tinyint(4) NOT NULL DEFAULT '0' COMMENT '结算周期 1:月 2:季 3:年 0未配置',
  `secondReward` decimal(5,2) NOT NULL DEFAULT '0.00' COMMENT '二级结算 (%)',
  `planStep` text COMMENT '结算阶梯json',
  `oneInviteReward` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '一级邀请奖励',
  `twoInviteReward` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '二级邀请奖励',
  `enableDate` datetime DEFAULT NULL COMMENT '启用日期',
  `expireDate` datetime DEFAULT NULL COMMENT '禁用日期',
  `isEnable` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否启动 0: 禁用 1:启动',
  `relationId` bigint(20) NOT NULL DEFAULT '0' COMMENT '渠道绑定用户id',
  `totalReward` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '总奖励',
  `waitReward` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '待结算奖励',
  `settleReward` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '已结算奖励',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `staffCount` int(10) NOT NULL DEFAULT '0' COMMENT '渠道成员数',
  `password` varchar(40) NOT NULL DEFAULT '07d2c316e8a66973caa53a8adf4fb5c4' COMMENT '密码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COMMENT='渠道';

DROP TABLE IF EXISTS  `tb_channel_assess`;
CREATE TABLE `tb_channel_assess` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `channelId` bigint(20) NOT NULL DEFAULT '0' COMMENT '渠道id',
  `staffId` bigint(20) NOT NULL DEFAULT '0' COMMENT '成员id',
  `firstBindCardNum` int(12) NOT NULL DEFAULT '0' COMMENT '一级绑卡数',
  `firstBindCardAmount` decimal(20,2) DEFAULT '0.00',
  `secondBindCardNum` int(12) NOT NULL DEFAULT '0' COMMENT '二级绑卡数',
  `secondBindCardAmount` decimal(20,2) DEFAULT '0.00',
  `firstTradeNum` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的交易量一级',
  `firstAnnual` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的年化一级',
  `secondTradeNum` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的交易量二级',
  `secondAnnual` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的年化二级',
  `tradeDate` date DEFAULT NULL COMMENT '统计的那天的',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COMMENT='渠道、成员业绩';

DROP TABLE IF EXISTS  `tb_channel_invite`;
CREATE TABLE `tb_channel_invite` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `channelId` bigint(20) NOT NULL DEFAULT '0' COMMENT '渠道id',
  `userId` varchar(60) NOT NULL DEFAULT '',
  `realName` varchar(40) NOT NULL DEFAULT '' COMMENT '绑卡人姓名',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '绑卡人手机号',
  `bindCardDate` date DEFAULT NULL,
  `inviteType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型 1:一级 2:二级',
  `inviteId` varchar(60) NOT NULL DEFAULT '' COMMENT '邀请人id',
  `inviteName` varchar(60) NOT NULL DEFAULT '',
  `superiorId` bigint(20) NOT NULL DEFAULT '0' COMMENT '邀请人上级',
  `superiorName` varchar(60) NOT NULL DEFAULT '' COMMENT '邀请人上级名称',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='渠道用户 被邀请用户';

DROP TABLE IF EXISTS  `tb_channel_period`;
CREATE TABLE `tb_channel_period` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `channelId` bigint(20) NOT NULL DEFAULT '0' COMMENT '渠道id',
  `firstBindCardNum` int(12) NOT NULL DEFAULT '0' COMMENT '一级绑卡数',
  `firstBindCardAmount` decimal(20,2) DEFAULT '0.00',
  `secondBindCardNum` int(12) NOT NULL DEFAULT '0' COMMENT '二级绑卡数',
  `secondBindCardAmount` decimal(20,2) DEFAULT '0.00',
  `firstTradeNum` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的交易量一级',
  `firstAnnual` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的年化一级',
  `firstTradeAmount` decimal(20,2) DEFAULT '0.00' COMMENT '一级交易奖励',
  `secondTradeNum` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的交易量二级',
  `secondAnnual` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT '每天的年化二级',
  `secondTradeAmount` decimal(20,2) DEFAULT '0.00' COMMENT '二级交易奖励',
  `rewardTotalAmount` decimal(2,2) NOT NULL DEFAULT '0.00' COMMENT '奖励总金额',
  `startDate` date DEFAULT NULL COMMENT '统计开始时间',
  `endDate` date DEFAULT NULL COMMENT '截止时间',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COMMENT='渠道、成员业绩';

DROP TABLE IF EXISTS  `tb_channel_settle`;
CREATE TABLE `tb_channel_settle` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `channelId` bigint(20) NOT NULL DEFAULT '0' COMMENT '渠道id',
  `amount` decimal(20,2) NOT NULL DEFAULT '0.00' COMMENT '结算金额',
  `settleUserId` varchar(60) NOT NULL DEFAULT '' COMMENT '结算操作人id',
  `settleUserName` varchar(60) NOT NULL DEFAULT '' COMMENT '结算操作人名',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='渠道结算记录';

DROP TABLE IF EXISTS  `tb_channel_staff`;
CREATE TABLE `tb_channel_staff` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `channelId` bigint(20) DEFAULT '0' COMMENT '渠道id',
  `number` varchar(40) NOT NULL DEFAULT '' COMMENT '成员编号',
  `password` varchar(40) NOT NULL DEFAULT '' COMMENT '密码',
  `realName` varchar(20) NOT NULL DEFAULT '' COMMENT '成员名称',
  `mobile` varchar(16) NOT NULL DEFAULT '' COMMENT '手机号',
  `inviteCode` varchar(64) NOT NULL DEFAULT '' COMMENT '邀请码',
  `isEnable` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否启动 0: 禁用 1:启动',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COMMENT='渠道成员';

DROP TABLE IF EXISTS  `tb_featured`;
CREATE TABLE `tb_featured` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(220) NOT NULL DEFAULT '' COMMENT '推荐位名称',
  `isDel` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除 -1删除   0正常',
  `imgUrl` varchar(220) NOT NULL DEFAULT '' COMMENT '图片地址',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0:否 1:h5  2:原生',
  `h5Url` varchar(220) NOT NULL DEFAULT '' COMMENT 'H5跳转地址',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '原生类型 1，2，3，4，5，6',
  `isShow` tinyint(4) NOT NULL DEFAULT '0' COMMENT ' 0:隐藏 1:显示',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序号',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `show_place` tinyint(4) NOT NULL DEFAULT '1' COMMENT '广告位显示位置 1首页 2商城',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COMMENT='推荐位';

DROP TABLE IF EXISTS  `tb_gold_put`;
CREATE TABLE `tb_gold_put` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` varchar(60) NOT NULL COMMENT '用户id',
  `realName` varchar(20) NOT NULL DEFAULT '' COMMENT '用户姓名',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `idCard` varchar(18) NOT NULL DEFAULT '' COMMENT '身份证号码',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型： 1:金条 2:金饰',
  `gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '克重',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='存金记录';

DROP TABLE IF EXISTS  `tb_gold_stat`;
CREATE TABLE `tb_gold_stat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` varchar(60) NOT NULL DEFAULT '' COMMENT '用户id',
  `totalTakeGram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '提金总克重',
  `totalInGram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '换金总克重',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='存金换金统计';

DROP TABLE IF EXISTS  `tb_goods`;
CREATE TABLE `tb_goods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '商品名',
  `title` varchar(60) NOT NULL DEFAULT '' COMMENT '副标题',
  `label` varchar(60) NOT NULL DEFAULT '' COMMENT '标签',
  `brandId` bigint(20) NOT NULL DEFAULT '0' COMMENT '品牌id',
  `processId` int(10) NOT NULL DEFAULT '0' COMMENT '加工模板id',
  `categoryId` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品分类id',
  `processCost` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '加工费用',
  `goodsCode` varchar(60) NOT NULL DEFAULT '' COMMENT '货品编号',
  `quality` varchar(20) NOT NULL DEFAULT '' COMMENT '商品品质',
  `minGram` decimal(12,5) NOT NULL DEFAULT '0.00000' COMMENT '最小克重',
  `maxGram` decimal(12,5) NOT NULL DEFAULT '0.00000' COMMENT '最小克重',
  `isQuota` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否限购 0:不限 1：限数量 2：限重量',
  `quotaNum` bigint(10) NOT NULL DEFAULT '0' COMMENT '限购数量 件',
  `quotaWeight` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '限购克重',
  `isPrompt` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否有限购时间 0:不限 1：限',
  `startPrompt` datetime DEFAULT NULL COMMENT '限购开始时间',
  `endPrompt` datetime DEFAULT NULL COMMENT '限购结束时间',
  `operatorId` varchar(40) NOT NULL DEFAULT '' COMMENT '操作人',
  `auditorId` varchar(20) NOT NULL DEFAULT '0' COMMENT '审核人',
  `auditType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '审核状态 1:待审核，2:已审核(通过) :3:审核(未通过)',
  `shelveType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '上下架状态 1: 待上架,2:上架，3:下架',
  `shelveTime` datetime DEFAULT NULL COMMENT '上架时间',
  `unShelveTime` datetime DEFAULT NULL COMMENT '下架时间',
  `sales` bigint(20) NOT NULL DEFAULT '0' COMMENT '销量 件数',
  `isDel` tinyint(2) NOT NULL DEFAULT '0' COMMENT '用户是否删除 0:有效，1:无效',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `stock` int(10) NOT NULL DEFAULT '0' COMMENT '库存',
  `imgUrl` varchar(200) NOT NULL DEFAULT '' COMMENT 'url',
  `minMarketPrice` decimal(20,5) NOT NULL DEFAULT '0.00000',
  `maxMarketPrice` decimal(20,5) NOT NULL DEFAULT '0.00000',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:金条 2:金饰',
  `downRemark` varchar(200) NOT NULL DEFAULT '',
  `minPrice` decimal(20,5) NOT NULL DEFAULT '0.00000',
  `maxPrice` decimal(20,5) NOT NULL DEFAULT '0.00000',
  `notice` text NOT NULL COMMENT '购买须知',
  `hint` text NOT NULL COMMENT '保养提示',
  `isUp` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否定时上架 0:否 1:是',
  `upTime` datetime DEFAULT NULL,
  `isRecommend` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐商品 0：否，1：是',
  PRIMARY KEY (`id`),
  KEY `idx_goods_id` (`brandId`),
  KEY `idx_category_id` (`categoryId`),
  KEY `idx_process_id` (`processId`)
) ENGINE=InnoDB AUTO_INCREMENT=224 DEFAULT CHARSET=utf8mb4 COMMENT='商品';

DROP TABLE IF EXISTS  `tb_goods_audit`;
CREATE TABLE `tb_goods_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '审核id',
  `goodsId` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品id',
  `auditType` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态 0:审核未通过， 1:审核通过',
  `auditorName` varchar(40) NOT NULL DEFAULT '',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '审核描述',
  `auditorId` varchar(25) NOT NULL DEFAULT '审核者',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_goods_id` (`goodsId`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4 COMMENT='商品审核表';

DROP TABLE IF EXISTS  `tb_goods_img`;
CREATE TABLE `tb_goods_img` (
  `id` bigint(25) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `goodsId` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品id',
  `url` varchar(128) NOT NULL DEFAULT '' COMMENT '图片地址',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `imgType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '图片类型 1:商品1比1图 2:商品3比2图;',
  `isDel` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已删除 0:否 -1:是',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_goods_id` (`goodsId`),
  KEY `idx_sort` (`sort`)
) ENGINE=InnoDB AUTO_INCREMENT=868 DEFAULT CHARSET=utf8mb4 COMMENT='商城-商品图片表';

DROP TABLE IF EXISTS  `tb_goods_label`;
CREATE TABLE `tb_goods_label` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'd',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '标签名',
  `url` varchar(250) NOT NULL DEFAULT '' COMMENT '图片',
  `isDel` int(4) NOT NULL DEFAULT '0' COMMENT '删除状态',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COMMENT='商品标签';

DROP TABLE IF EXISTS  `tb_goods_sku`;
CREATE TABLE `tb_goods_sku` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'SKU id',
  `goodsId` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品id',
  `skuCode` varchar(20) NOT NULL DEFAULT '' COMMENT '商品规格编号',
  `gram` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '重量',
  `price` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '价格',
  `marketPrice` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '市场价',
  `stock` int(20) NOT NULL DEFAULT '0' COMMENT '库存',
  `sales` bigint(20) NOT NULL DEFAULT '0' COMMENT '销量',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `isDel` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_goods_id` (`goodsId`)
) ENGINE=InnoDB AUTO_INCREMENT=556 DEFAULT CHARSET=utf8mb4 COMMENT='商品sku';

DROP TABLE IF EXISTS  `tb_order`;
CREATE TABLE `tb_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `serial` varchar(20) NOT NULL DEFAULT '' COMMENT '订单号',
  `userId` varchar(64) NOT NULL DEFAULT '' COMMENT '用户id',
  `userName` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `amount` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '商品总价格',
  `expressFee` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '快递费',
  `processCost` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '加工费',
  `money` decimal(20,5) NOT NULL DEFAULT '0.00000' COMMENT '支付金额=amount+express_fee+process_cost',
  `payChannel` tinyint(2) NOT NULL DEFAULT '1' COMMENT '支付渠道 1:余额支付 2:易宝支付',
  `totalGram` decimal(10,5) NOT NULL COMMENT '总克重',
  `orderType` tinyint(4) NOT NULL DEFAULT '1' COMMENT '订单类型 1:商城订单 2:提金订单 3:换金订单',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '订单状态 1:待支付,2:订单关闭,3:支付成功-待发货,4:已发货-待收货,5:确认收货-已完成;6:订单处理中',
  `addressId` bigint(20) NOT NULL DEFAULT '0' COMMENT '收货地址id',
  `logisticsId` bigint(20) NOT NULL DEFAULT '0' COMMENT '订单快递id',
  `cancelReason` varchar(200) NOT NULL DEFAULT '' COMMENT '订单关闭原因',
  `clientReamrk` varchar(200) NOT NULL DEFAULT '' COMMENT '客户订单备注',
  `serverRemark` varchar(200) NOT NULL DEFAULT '' COMMENT '后台订单备注',
  `source` tinyint(4) NOT NULL DEFAULT '1' COMMENT '订单来源 1:ios 2:android 3:H5 4:微信小程序 5:第三方平台 6:他、其他',
  `closeTime` datetime DEFAULT NULL COMMENT '订单关闭时间',
  `payTime` datetime DEFAULT NULL COMMENT '支付时间',
  `sendTime` datetime DEFAULT NULL COMMENT '发货时间',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `closeRemark` varchar(200) NOT NULL DEFAULT '' COMMENT '订单关闭原因',
  `interestHistoryAmount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日利息',
  `principalNowAmount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T日本金',
  `principalHistoryAmount` decimal(10,5) DEFAULT '0.00000' COMMENT 'T-1日本金',
  `freezeAmount` decimal(20,2) DEFAULT '0.00' COMMENT '冻结金额',
  `couponAmount` int(10) DEFAULT '0' COMMENT '商城满减券金额',
  `couponId` int(10) DEFAULT NULL COMMENT '满减券ID',
  `readed` int(10) DEFAULT '0' COMMENT '是否阅读：0-未阅读；1-已解读',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_serial` (`serial`),
  KEY `idx_user_id` (`userId`),
  KEY `idx_address_id` (`addressId`),
  KEY `idx_logistics_id` (`logisticsId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=413 DEFAULT CHARSET=utf8mb4 COMMENT='订单';

DROP TABLE IF EXISTS  `tb_order_item`;
CREATE TABLE `tb_order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `orderId` bigint(20) NOT NULL COMMENT '订单id',
  `goodsId` bigint(20) NOT NULL COMMENT '商品id',
  `skuId` bigint(20) NOT NULL COMMENT 'sku id',
  `num` bigint(20) NOT NULL DEFAULT '0' COMMENT '件数',
  `weight` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '克重',
  `price` decimal(20,4) NOT NULL DEFAULT '0.0000' COMMENT '价格',
  `goodsCode` varchar(64) NOT NULL DEFAULT '' COMMENT '商品货号',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_order_id` (`orderId`),
  KEY `idx_goods_id` (`goodsId`),
  KEY `idx_sku_id` (`skuId`)
) ENGINE=InnoDB AUTO_INCREMENT=494 DEFAULT CHARSET=utf8mb4 COMMENT='订单商品';

DROP TABLE IF EXISTS  `tb_order_logistics`;
CREATE TABLE `tb_order_logistics` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `logisticsName` varchar(20) DEFAULT '' COMMENT '快递名称',
  `logisticsNo` varchar(50) DEFAULT '' COMMENT '快递单号',
  `fullName` varchar(20) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `telephone` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `address` varchar(128) NOT NULL DEFAULT '' COMMENT '详细地址 省市区街道门牌号',
  `zipCode` varchar(20) DEFAULT '' COMMENT '邮政编码',
  `state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '快递单当前签收状态，包括0在途中、1已揽收、2疑难、3已签收、4退签、5同城派送中、6退回、7转单等7个状态，其中4-7需要另外开通才有效',
  `data` text COMMENT '快递json数据',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=413 DEFAULT CHARSET=utf8mb4 COMMENT='订单快递';

DROP TABLE IF EXISTS  `tb_process_template`;
CREATE TABLE `tb_process_template` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '模板id',
  `name` varchar(30) NOT NULL COMMENT '模板名称',
  `processCost` decimal(10,5) NOT NULL DEFAULT '0.00000' COMMENT '加工费 （元/克）',
  `disCount` tinyint(10) NOT NULL DEFAULT '1' COMMENT '折扣 (%)',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:金条 2:金饰',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COMMENT='加工费模板';

DROP TABLE IF EXISTS  `tb_settings`;
CREATE TABLE `tb_settings` (
  `keyName` varchar(30) NOT NULL,
  `keyValue` text NOT NULL COMMENT 'json',
  PRIMARY KEY (`keyName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='参数设置';

DROP TABLE IF EXISTS  `tb_shopping_trolley`;
CREATE TABLE `tb_shopping_trolley` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `memberId` varchar(64) NOT NULL COMMENT '会员ID',
  `skuId` bigint(10) NOT NULL COMMENT '规格ID',
  `imgUrl` varchar(128) NOT NULL COMMENT '图片地址',
  `name` varchar(64) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '单价',
  `gram` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '单件商品克重',
  `quantity` int(10) NOT NULL DEFAULT '0' COMMENT '数量',
  `addTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modifyTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted` int(10) DEFAULT '1' COMMENT '删除 1：否 2：是',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `temp`;
CREATE TABLE `temp` (
  `member_id` varchar(64) NOT NULL,
  `amount` int(10) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS  `terminal_statistics`;
CREATE TABLE `terminal_statistics` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `app_system` varchar(50) NOT NULL COMMENT 'Android/IOS',
  `phone_model` varchar(50) NOT NULL COMMENT '手机型号',
  `app_version` varchar(50) NOT NULL COMMENT 'app版本号',
  `launch_times` int(11) NOT NULL COMMENT '启动次数',
  `launch_date` varchar(32) NOT NULL COMMENT '启动日期',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='终端统计表';

DROP TABLE IF EXISTS  `trade_template`;
CREATE TABLE `trade_template` (
  `id` varchar(50) NOT NULL,
  `name` varchar(20) NOT NULL COMMENT '模版名称',
  `type` int(11) NOT NULL COMMENT '类型 1:买入 2:卖出',
  `is_default` int(11) NOT NULL COMMENT '是否默认模版 1:是 2:否',
  `deleted` int(11) NOT NULL DEFAULT '1' COMMENT '删除 1：否 2：是',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品交易模版';

DROP TABLE IF EXISTS  `trade_template_time`;
CREATE TABLE `trade_template_time` (
  `id` varchar(50) NOT NULL,
  `template_id` varchar(50) NOT NULL COMMENT '交易模版Id',
  `start_time` varchar(25) NOT NULL COMMENT '交易起始时间',
  `end_time` varchar(25) NOT NULL COMMENT '交易结束时间',
  `add_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '添加时间',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='模版交易时间段';

SET FOREIGN_KEY_CHECKS = 1;



