﻿use ugold;
insert into `tb_settings`(`keyName`,`keyValue`)
values('aboutUs','{"showTag":1}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('appBottomNavigation','{"goldInvest":1,"marketInfo":1,"appVersions":[]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('appIndexTopTitle','{"showTag":1,"appVersions":["3.5.0"]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('appVersion','{"appVersions":""}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('broughtToAccount','{"accountName":"杭州柚高网络信息有限公司 ","bankAccount":"571911687210601","openBank":"招商银行杭州天城路支行"}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('businessIntroductionArea','{"businessIntroductions":[{"link":"https://h5-api.u-gold.cn/goldenRule/index.html","title":"实物金回租介绍"},{"link":"http://h5-api.u-gold.cn/app-h5/discount/index2.html?appCode=111","title":"折扣金怎么玩？"},{"link":"http://h5-api-test.u-gold.cn/app-h5/rent/steady_introduction2.html?appCode=111","title":"安稳金产品介绍"}],"showTag":1}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('buySellGram','{"buyMaxGramStatus":1,"sellMaxGramStatus":1,"sellMaxGramPerDay":5000,"buyMaxGramPerDay":5000}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('customerServiceSchedule','{"showTag":1,"am":"09:00~12:00","pm":"13:00~17:30"}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('discountGold','{"closeType":1,"leaseCalculateGoldType":1,"leaseDayList":[{"leaseDay":60,"yearsIncome":4.2}]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('entityGold','{"leaseCalculateGoldTime":"","leaseCalculateGoldType":1,"leaseCloseType":1,"leaseDayList":[{"leaseDay":30,"yearsIncome":3},{"leaseDay":90,"yearsIncome":5},{"leaseDay":180,"yearsIncome":7},{"leaseDay":270,"yearsIncome":9},{"leaseDay":360,"yearsIncome":11},{"leaseDay":60,"yearsIncome":4.2}],"specificationList":[1,5,10,20,50,100,800,1800]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('experienceGold','{"closeType":1,"leaseDayList":[{"leaseDay":3,"yearsIncome":0.1}]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('freight','{"fee":0}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('goldBean','{"abatementDate":"01-01","abatementType":1,"exchangeOrigin":100000,"exchangeType":0,"expireType":0,"goldBeanMax":200,"goldBeanMin":50,"goldBeanNum":10000,"goldGram":1,"isSmsMessage":0}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('goldBeanProvide','{"firstBuyEntityGold":20,"firstBuyExperienceGold":1,"firstBuyGoldOrnament":20,"firstLeaseBack":30,"registerGoldBean":20,"tiedCardGoldBean":30}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('goldBeanProvideMarket','{"everyDayUpperStatus":1,"extraAwardStatus":0,"goldBeanNum":3,"inviteFriendsStatus":1,"inviteeGiveGoldBean":20,"inviterGiveGoldBean":20,"signDaysToGoldBeanList":[],"signFullGoldBeanNum":21,"signStatus":1,"signType":2,"supplementStatus":1,"upperNum":5}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('goldInvest','{"showTag":1,"appVersions":["3.5.0"]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('hotProductLabel','{"secureGold":["闲钱攒黄金",""],"physicalGold":["月月有惊喜",""]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('inGold','{"inMaxGram":1000,"inIsDiscount":0}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('InvestDynamic','{"showTag":1,"readNum":15}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('marketInfo','{"showTag":1,"appVersions":["3.5.0"]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('platformData','{"showTag":1,"inputGoldGram":100,"inputServeUser":10}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('productShowArea','{"moduleTitles":[{"moduleTitle":"长期回租,享稳固收益","productName":"安稳金","productType":8},{"moduleTitle":"短期回租,灵活高收益","productName":"实物金","productType":6},{"moduleTitle":"折扣购金,邀友立减","productName":"折扣金","productType":9}]}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('recharge','{"isFee":0,"minAmount":2,"beforeNum":0,"feeWay":0,"hasDiscount":0,"feePeriod":0}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('sellGold','{"isFee":1,"beforeAmount":0.3,"beforeNum":2,"feeWay":0,"hasDiscount":0,"afterAmount":0.5,"feePeriod":0}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('takeGold','{"takeIsDiscount":0,"takeMaxGram":1000}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('unpaidTime','{"unpaidTime":2}');
insert into `tb_settings`(`keyName`,`keyValue`)
values('withDraw','{"isFee":1,"minAmount":5,"beforeAmount":0,"beforeNum":2,"feeWay":0,"hasDiscount":0,"doneTime":"15:00","afterAmount":3,"feePeriod":1}');
