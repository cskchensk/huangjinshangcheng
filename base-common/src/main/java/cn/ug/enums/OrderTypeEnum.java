package cn.ug.enums;


import org.apache.commons.lang3.StringUtils;

public enum OrderTypeEnum {
    mall_order(1, "商城订单"),
    extraction_order(2, "提金订单"),
    replace_order(3, "换金订单");

    private int code;
    private String name;

    OrderTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code获取枚举
     *
     * @param code
     * @return OrderStatusEnum
     */
    public static OrderTypeEnum getByCode(int code) {
        for (OrderTypeEnum enumVal : OrderTypeEnum.values()) {
            if (code == enumVal.getCode()) {
                return enumVal;
            }
        }
        return null;
    }

    /**
     * 根据name获取枚举
     *
     * @param name
     * @return OrderStatusEnum
     */
    public static OrderTypeEnum getByName(String name) {
        for (OrderTypeEnum enumVal : OrderTypeEnum.values()) {
            if (StringUtils.equals(name, enumVal.getName())) {
                return enumVal;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getCode() {
        return code;
    }
}
