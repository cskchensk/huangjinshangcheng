package cn.ug.enums;

/**
 * @author zhaohg
 * @date 2018/07/19.
 */
public enum LogisticsStatusEnum {

    TRANSIT(0, "在途中"),
    DRAW(1, "已揽收"),
    PUZZLE(2, "疑难"),
    SIGN(3, "已签收"),
    ;

    LogisticsStatusEnum(int state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    private int    state;
    private String desc;

    public int state() {
        return state;
    }

    public String desc() {
        return desc;
    }

    public static LogisticsStatusEnum get(int state) {
        for (LogisticsStatusEnum item : LogisticsStatusEnum.values()) {
            if (item.state() == state) {
                return item;
            }
        }
        return null;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
