package cn.ug.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 订单状态
 * @author zhaohg
 * @date 2018/07/14.
 */
public enum OrderStatusEnum {

    WAIT_PAY(1, "待支付"),
    CLOSE(2, "关闭订单"),
    WAIT_SEND(3, "支付成功-待发货"),
    DELIVERED(4,"已发货-待收货"),
    RECEIPT(5,"确认收货-已完成"),
    CANCEL(6,"订单处理中")
;

    private int code;
    private String name;

    OrderStatusEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code获取枚举
     *
     * @param code
     * @return OrderStatusEnum
     */
    public static OrderStatusEnum getByCode(int code) {
        for (OrderStatusEnum enumVal : OrderStatusEnum.values()) {
            if (code == enumVal.getCode()) {
                return enumVal;
            }
        }
        return null;
    }

    /**
     * 根据name获取枚举
     *
     * @param name
     * @return OrderStatusEnum
     */
    public static OrderStatusEnum getByName(String name) {
        for (OrderStatusEnum enumVal : OrderStatusEnum.values()) {
            if (StringUtils.equals(name, enumVal.getName())) {
                return enumVal;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getCode() {
        return code;
    }
}
