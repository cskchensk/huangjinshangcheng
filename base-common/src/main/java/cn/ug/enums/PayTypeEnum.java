package cn.ug.enums;

public enum PayTypeEnum {
    RECHARGE(1, "充值"),
    BUY_GOLD(2, "买金"),
    MALL_OLD(3, "商城支付-老版本"),
    GOLD_EXTRACTION(4, "提金"),
    GOLD_CHANGE(5, "换金"),
    MALL_NEW(6, "商城支付-新版本"),
    TBILL(7, "提单"),
    ENTITY_GOLD_EXTRACTION(8,"快递提金"),
    EXTRACTION_OFFLINE(9,"门店提金");

    private int type;
    private String msg;

    PayTypeEnum(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public static String getPayTypeEnum(int type) {
        for (PayTypeEnum v : PayTypeEnum.values()) {
            if (v.getType()==type) {
                return v.getMsg();
            }
        }
        return null;
    }
    public int getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }
}
