package cn.ug.enums;

import org.apache.commons.lang3.StringUtils;

public enum WoolLabelEnum {
    LABEL1("信用异常", 1),
    LABEL2("垃圾账号", 10),
    LABEL3("无效手机", 100),
    LABEL4("黑名单", 1000),
    LABEL5("群控行为", 10000),
    LABEL6("机器行为", 100000),
    LABEL7("环境异常", 1000000),
    LABEL8("js上报异常", 10000000),
    LABEL9("撞库", 10000000);

    private String msg;
    private int score;

    WoolLabelEnum(String msg, int score) {
        this.msg = msg;
        this.score = score;
    }

    public static int getScoreByMsg(String msg) {
        for (WoolLabelEnum v : WoolLabelEnum.values()) {
            if (StringUtils.trim(v.getMsg()).equals(StringUtils.trim(msg))) {
                return v.getScore();
            }
        }
        return 0;
    }

    public String getMsg() {
        return msg;
    }

    public int getScore() {
        return score;
    }
}
