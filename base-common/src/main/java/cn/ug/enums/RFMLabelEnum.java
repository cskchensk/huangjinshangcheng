package cn.ug.enums;

import org.apache.commons.lang3.StringUtils;

public enum RFMLabelEnum {
    LABEL1("重要价值客户", 8),
    LABEL2("重要发展客户", 7),
    LABEL3("重要保留客户", 6),
    LABEL4("重要挽留客户", 5),
    LABEL5("一般价值客户", 4),
    LABEL6("一般发展客户", 3),
    LABEL7("一般保持客户", 2),
    LABEL8("一般挽留客户", 1),
    LABEL9("未计算", 0);

    private int score;
    private String msg;

    RFMLabelEnum(String msg, int score) {
        this.msg = msg;
        this.score = score;
    }

    public static int getScoreByMsg(String msg) {
        for (RFMLabelEnum v : RFMLabelEnum.values()) {
            if (StringUtils.trim(v.getMsg()).equals(StringUtils.trim(msg))) {
                return v.getScore();
            }
        }
        return 0;
    }

    public String getMsg() {
        return msg;
    }

    public int getScore() {
        return score;
    }
}
