package cn.ug.enums;

public enum BonusTypeEnum {
    INTEREST(1, "福利券"),
    KICKBACK(2, "红包"),
    CASH(3, "现金"),
    GOLD(4, "黄金");

    private int type;
    private String msg;

    BonusTypeEnum(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }
}
