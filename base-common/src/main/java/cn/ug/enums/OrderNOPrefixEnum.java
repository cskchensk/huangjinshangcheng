package cn.ug.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public enum OrderNOPrefixEnum {
    TB("生成提单"),
    LB("提单回租"),
    TS("提单出售"),
    WD("提现"),
    RC("充值"),
    PE("购买体验金"),
    PG("购买金饰（商城）"),
    BB("金料回购"),
    GB("金豆明细"),
    EG("提金"),
    GE("礼品兑换");

    private String remark;

    OrderNOPrefixEnum(String remark) {
        this.remark = remark;
    }

    public boolean exist(String prefix) {
        if (StringUtils.isBlank(prefix)) {
            return false;
        }
        for (OrderNOPrefixEnum prefixEnum : OrderNOPrefixEnum.values()) {
            if (StringUtils.equalsIgnoreCase(prefixEnum.name(), prefix)) {
                return true;
            }
        }
        return false;
    }

    public static Map switchString(String orderNo){
        if(StringUtils.isBlank(orderNo)){
            return null;
        }

        int type = 0;
        String[] flags = {OrderNOPrefixEnum.TB.name(),OrderNOPrefixEnum.PE.name(),OrderNOPrefixEnum.LB.name(),OrderNOPrefixEnum.TS.name(),
                OrderNOPrefixEnum.EG.name(),OrderNOPrefixEnum.PG.name(),OrderNOPrefixEnum.BB.name()};
        for (int i = 0;i<flags.length;i++){
            if (orderNo.contains(flags[i])){
                type = i;
                break;
            }
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type",type);
        params.put("orderNo",orderNo);
        return params;
    }

}
