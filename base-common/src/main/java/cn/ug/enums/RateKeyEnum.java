package cn.ug.enums;

public enum RateKeyEnum {
    // 买金
    BUY_GOLD("buyGold"),
    //卖金
    SELL_GOLD("sellGold"),
    //充值
    RECHARGE("recharge"),
    //提现
    WITHDRAW("withDraw"),
    //提金
    TAKE_GOLD("takeGold"),
    //换金
    IN_GOLD("inGold"),
    // 运费
    FREIGHT("freight"),
    //每人每天可购的买金卖金克重
    BUY_SELL_GRAM("buySellGram"),
    //待支付时间设置
    UNPAID_TIME("unpaidTime"),
    //登陆密码错误次数限制
    LOGIN_PASSWORD("loginPassword"),
    //交易密码错误次数限制
    TRADE_PASSWORD("tradePassword"),
    //金豆
    GOLD_BEAN("goldBean"),
    //实物金
    ENTITY_GOLD("entityGold"),
    //体验金
    EXPERIENCE_GOLD("experienceGold"),
    //买金优惠
    BUY_GOLD_PRIVILEGE("buyGoldPrivilege"),
    //金豆发放基础设置
    GOLD_BEAN_PROVIDE("goldBeanProvide"),
    //金豆发放营销
    GOLD_BEAN_PROVIDE_MARKET("goldBeanProvideMarket"),
    //对公账号
    BROUGHT_TO_ACCOUNT("broughtToAccount"),
    //折扣金
    DISCOUNT_GOLD("discountGold"),
    //首页顶部标题栏
    APP_INDEX_TOP_TITLE("appIndexTopTitle"),
    //APP底部导航栏
    APP_BOTTOM_NAVIGATION("appBottomNavigation"),
    //黄金投资导航栏
    GOLD_INVEST("goldInvest"),
    //行情导航栏
    MARKET_INFO("marketInfo"),
    //关于我们
    ABOUT_US("aboutUs"),
    //平台数据
    PLATFORM_DATA("platformData"),
    //客服作息时间
    CUSTOMER_SERVICE_SCHEDULE("customerServiceSchedule"),
    //产品热销标签
    HOT_PRODUCT_LABEL("hotProductLabel"),
    //产品展示区
    PRODUCT_SHOW_AREA("productShowArea"),
    //业务介绍区
    BUSINESS_INTRODUCTION_AREA("businessIntroductionArea"),
    //投资冬天
    INVEST_DYNAMIC("InvestDynamic");

    private String key;

    RateKeyEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
