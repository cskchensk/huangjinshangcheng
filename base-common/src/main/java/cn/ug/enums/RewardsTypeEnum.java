package cn.ug.enums;

public enum RewardsTypeEnum {
    bind_card(1, "认证绑卡"),
    INVESTMENT(2, "首次投资"),
    BONUS(3, "投资返现");

    RewardsTypeEnum(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    private int type;
    private String desc;

    public int getType() {
        return type;
    }

    public String desc() {
        return desc;
    }

    public static String get(int type) {
        for (RewardsTypeEnum item : RewardsTypeEnum.values()) {
            if (item.getType() == type) {
                return item.desc;
            }
        }
        return null;
    }
}
