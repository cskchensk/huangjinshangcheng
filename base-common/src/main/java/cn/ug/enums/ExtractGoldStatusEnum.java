package cn.ug.enums;

public enum  ExtractGoldStatusEnum {
    FAIL(0,"交易失败"),
    SUBSCRIBE(1, "已预约"),
    WAIT_TAKE(2, "待收货"),
    CANCEL(3, "已取消"),
    FINISH(4, "已完成");

    private int status;

    private String remark;

    ExtractGoldStatusEnum(int status, String remark) {
        this.status = status;
        this.remark = remark;
    }

    /**
     * 根据code获取枚举
     *
     * @param status
     * @return OrderStatusEnum
     */
    public static String getByStatus(int status) {
        for (ExtractGoldStatusEnum enumVal : ExtractGoldStatusEnum.values()) {
            if (status == enumVal.getStatus()) {
                return enumVal.getRemark();
            }
        }
        return null;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
