package cn.ug.enums;

public enum CouponPurposeEnum {
	REGISTER(1, "注册"),
	BINDED_CARD_INVITER(2, "绑卡-邀请者奖励"),
	BINDED_CARD_INVITEE(3, "绑卡-被邀请者奖励"),
	FIRST_INVEST_INVITER(4, "首投定期产品-邀请者奖励"),
	FIRST_INVEST_INVITEE(5, "首投定期产品-被邀请者奖励"),
	SYSTEM_GIVE(6, "系统赠送"),
	MALL(7, "商城"),
	INTEREST(8, "体验金利息"),
	BIRTHDAY(9, "生日"),
	BONUS(10, "返利"),
	BINDED_CARD(11, "认证绑卡"),
	BREAKTHROUGH_BINDED_CARD(12, "大闯关-认证绑卡"),
	BREAKTHROUGH_EXPERIENCE(13, "大闯关-体验7日活动产品"),
	BREAKTHROUGH_FINISH(14, "大闯关-预定稳赢金系列产品");
	
	private int purpose;
	private String msg;

	CouponPurposeEnum(int purpose, String msg) {
		this.purpose = purpose;
		this.msg = msg;
	}

	public int getPurpose() {
		return purpose;
	}

	public String getMsg() {
		return msg;
	}
}