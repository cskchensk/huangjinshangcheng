package cn.ug.enums;

public enum CouponTriggerEnum {
	REGISTER(0, "注册未绑卡"),
	BINDED_CARD(1, "绑卡未交易"),
	TRANSACTION(2, "已交易用户"),
	PAY_EXPERIENCE(3, "体验金购买完成"),
	LEASEBACK(4, "回租完成"),
	GIVELIKE(5, "点赞活动");
	private int trigger;
	private String msg;

	CouponTriggerEnum(int trigger, String msg) {
		this.trigger = trigger;
		this.msg = msg;
	}

	public int getTrigger() {
		return trigger;
	}

	public String getMsg() {
		return msg;
	}
}