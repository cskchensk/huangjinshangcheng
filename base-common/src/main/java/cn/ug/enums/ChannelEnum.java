package cn.ug.enums;

/**
 * 默认渠道，注意id不要变化
 */
public enum ChannelEnum {
    WEB("7yZGZ3ayRvd3gGXeY9JDGL", "官网"),
    ANDROID("ChC9Qj3NRHxqEP31xuyaf3", "安卓"),
    IOS("VvoJWya94SYyCM1m4eSKLg", "IOS"),
    WECHAT("4ceAUpTarngWeZjgb7jcDT", "微信"),
    INVITATION("G2RXn18dYz4hdwkTimNvLG", "邀请"),
    SMALL_ROUTINE("QeeLSY2dKfuFocdENL2taY", "小程序");

    private String id;
    private String name;

    ChannelEnum(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
