package cn.ug.enums;

public enum ProductTypeEnum {
	NOVICE(1, "新手专享"),
	ACTIVITY(2, "活动产品"),
	CURRENT(3, "活期产品"),
	REGULAR(4, "定期产品"),
	ENTITY(6, "实物金"),
	EXPERIENCE_GOLD(7,"体验金"),
	SMOOTH_GOLD(8,"安稳金"),
	DISCOUNT_GOLD(9,"折扣金");
	private int type;
	private String msg;

	ProductTypeEnum(int type, String msg) {
		this.type = type;
		this.msg = msg;
	}

	/**
	 * 根据code获取枚举
	 *
	 * @param code
	 * @return OrderStatusEnum
	 */
	public static String getByCode(int code) {
		for (ProductTypeEnum enumVal : ProductTypeEnum.values()) {
			if (code == enumVal.getType()) {
				return enumVal.getMsg();
			}
		}
		return null;
	}

	public int getType() {
		return type;
	}

	public String getMsg() {
		return msg;
	}
}