package cn.ug.enums;

public enum ExperienceTradeTypeEnum {
    REGISTER(1, "注册"),
    ACTIVITIES(2, "活动"),
    DRAW(3, "抽券"),
    BUY_GOLD(4, "购买体验金提单");

    private int type;
    private String msg;

    ExperienceTradeTypeEnum(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public static String getPayTypeEnum(int type) {
        for (ExperienceTradeTypeEnum v : ExperienceTradeTypeEnum.values()) {
            if (v.getType()==type) {
                return v.getMsg();
            }
        }
        return null;
    }

    public int getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }
}
