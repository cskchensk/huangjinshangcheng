package cn.ug.enums;

import java.math.BigDecimal;

public enum WxTemplateEnum {

    EXTRACT_FAILED(1, "提现失败", "1WK6tIKHp4pKa-I7HJZRqQ10sDiXCiAyNFM8jjM9X-Y", "提现提交通知", "尊敬的用户您好：您的提现申请已提交，今天15:00之前提现的，预计T+1日到账；今天15:00之后提现的，预计T+2日到账，请注意查收", "感谢您的使用，欢迎继续关注柚子黄金，如有疑问请咨询客服400-011-6595。"),
    EXTRACT_SUCCEED(2, "提现成功", "DjKw8JK8rFFGWEi-O-hPU_Sl8osYM1mpqJHZJ3l_gO4", "提现到帐通知","尊敬的用户您好：您申请的提现已到账，资金已到您的银行账户","感谢您的使用，欢迎继续关注柚子黄金，如有疑问请咨询客服400-011-6595。"),
    TIEDCARD_SUCCEED(3, "完成绑卡认证", "TkMfvVIwjx6hI2duF2xp5AmUeGypyOOyR2Wsaef8rms", "绑卡成功通知", "尊敬的用户您好：", "恭喜您已成功完成认证绑卡，如有疑问请咨询客服400-011-6595，更多精彩，请下载柚子黄金APP查看！"),
    FUND_CHANGE(4, "资金账户变动[转入转出]", "YvXC6oWzfWzdl5xCGYAM4icnWKDlTPIFnOJ6Ei_vD2M", "账户资金变动通知", "尊敬的用户您好：您的资金账户发生变动，请知悉", "感谢您的使用，欢迎继续关注柚子黄金，如有疑问请咨询客服400-011-6595。"),
    ACCOUNT_EARNINGS(5, "产品开始计息", "knLe-0lO3WqIdesa86mEk7qhOnJ9iBSSXIBKT22JumY", "账户收益通知", "账户资产收益统计", "柚子黄金，百倍放心，感谢您的使用，更多精彩，请下载柚子黄金APP查看！"),
    SUFFICIENT_NOT_RICH(6, "账户余额不足充值", "t9FE4KMLGETYKyGkQcOW6eOBPAWeEGbaYk3iolBzcIU", "充值成功提醒", "尊敬的用户您好：您已成功充值柚子黄金账户", "感谢您的使用，欢迎继续关注柚子黄金，如有疑问请咨询客服400-011-6595。"),
    PURCHASE_TIMING_PRODUCT_SUCCEED(7, "买定期产品成功", "YkPv5GeI1CsGUxR7WKxF-Z64MLnDabD34phq6ESzdNE", "买入成功通知", "尊敬的用户您好：您已成功购买【{product}】，产品期限为【{deadline}】天，到期日期为【{expireTime}】，预计收益为【{predictEarnings}】克。", "感谢您的支持与信任，如有疑问请咨询客服400-011-6595。"),
    PURCHASE_ACTIVITY_PRODUCT_SUCCEED(8, "买活期产品成功", "YkPv5GeI1CsGUxR7WKxF-Z64MLnDabD34phq6ESzdNE", "买入成功通知", "尊敬的用户您好：您已成功购买【{product}】，流动金产品随买随卖，T+1日开始计息。", "感谢您的支持与信任，如有疑问请咨询客服400-011-6595。"),
    PRODUCT_EXPIRE(9, "定期产品到期后", "URYhZuIEd4qZpx6YvXOyC8qY9H0ZFzIvWVE-G_vVgkQ", "转入成功通知", "尊敬的用户您好：您购买的【{product}】已到期，并已成功转入柚子钱包账户，当前可用黄金资产增加【{weight}】克，余额为【{balance}】克。", "柚子黄金，百倍放心，更多精彩，请下载柚子黄金APP查看！"),
    SALE_GOLD_SUCCEED(10, "卖金成功后", "WgwpjXM3YlkOSSDaF50c_fmDbWyvvkzY_zr8lLrTNN8", "卖出确认通知", "尊敬的用户您好：您已成功卖金【{weight}】克，当前资产余额为【{balance}】克，您的资金账户将收入【{money}】元，请在APP内查看。", "柚子黄金，百倍放心，更多精彩，请下载柚子黄金APP查看！"),
    LEASEBACK(11, "回租提单", "1V4idJP0YM80tONpOr1SRfAa5ZJew5HvThaQLgCOQ2k", "回租提单通知", "尊敬的用户您好：您的提单【{orderNO}】于【{monthAndDay}】开始回租，回租时长【{leasebackDays}】天，【{endMonthAndDay}】结束，{couponInfo}届时奖励将发放到您的账户中。", "柚子黄金，百倍放心，更多精彩，请下载柚子黄金APP查看！"),
    LEASEBACK_BECOME_DUE(12, "回租提单到期", "1V4idJP0YM80tONpOr1SRfAa5ZJew5HvThaQLgCOQ2k", "回租提单到期通知", "尊敬的用户您好：您的提单【{orderNO}】已于【{monthAndDay}】到期，回租奖励已发放到您的账户中，请打开APP查看", "柚子黄金，百倍放心，更多精彩，请下载柚子黄金APP查看！"),
    TBILL_PAY_SUCCESS(13, "提单支付成功", "YkPv5GeI1CsGUxR7WKxF-Z64MLnDabD34phq6ESzdNE", "提单支付成功通知", "尊敬的用户您好：您的订单【{orderNO}】已支付成功，{couponInfo}当前为待处理状态，您可以选择回租、出售或实物提金，请打开APP查看", "柚子黄金，百倍放心，更多精彩，请下载柚子黄金APP查看！"),
    EXTRACT_GOLD_SUBSCRIBE(14,"用户在前台发起提金预约后触发","DqG0pjGW3ZpzqaNhdLOY3agrU_ioj5_s0ec_LixK67s","预约提货成功通知","尊敬的用户您好：您预约的提货业务已受理，平台将在2-3个工作日内审核，审核结果会及时通知您","如有问题请撩小柚 400-011-6595。"),
    EXTRACT_GOLD_CLOSE(15,"后台取消提货预约或提货时支付失败触发","4X37skWdeo7EVK8kHVfZYu6e2tZ3w_aVx_IP8Iz7yB8","提货审核通知","尊敬的用户您好：您预约的提货业务已取消","感谢您的支持，如有问题请撩小柚 400-011-6595。");
    private int type;

    private String msg;

    private String templateId;

    private String title;

    /**
     * 第一行数据
     */
    private String firstData;
    /**
     * 备注
     */
    private String remark;

    WxTemplateEnum(int type, String msg, String templateId, String title, String firstData, String remark) {
        this.type = type;
        this.msg = msg;
        this.templateId = templateId;
        this.title = title;
        this.firstData = firstData;
        this.remark = remark;
    }

    public static WxTemplateEnum getWxTemplateByCode(Integer code) {
        for (WxTemplateEnum wxTemplateEnum : WxTemplateEnum.values()) {
            if (wxTemplateEnum.type == code) {
                return wxTemplateEnum;
            }
        }
        return null;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstData() {
        return firstData;
    }

    public void setFirstData(String firstData) {
        this.firstData = firstData;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public static void main(String[] args) {
        BigDecimal stm = new BigDecimal(12.54);
        System.out.println(stm.toString());
    }
}
