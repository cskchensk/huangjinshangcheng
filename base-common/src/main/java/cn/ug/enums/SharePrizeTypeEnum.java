package cn.ug.enums;

public enum  SharePrizeTypeEnum {

    //奖励类型 1.体验金、2.金豆、3.黄金红包、4.回租福利券、5.商城满减券、6.现金奖励 7：未中奖
    EXPERIENCE_GOLD(1, "体验金"),
    GOLD_BEAN(2, "金豆"),
    KICKBACK(3, "黄金红包"),
    INTEREST(4, "回租福利券"),
    SHOP_ELECTRONIC_CARD(5, "商城满减券"),
    CASH(6, "现金奖励"),
    UN_PRIZE(7, "未中奖");

    private int type;
    private String msg;

    SharePrizeTypeEnum(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }


}
