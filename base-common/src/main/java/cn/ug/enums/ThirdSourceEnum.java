package cn.ug.enums;

public enum ThirdSourceEnum {

    WXACCOUNT(1, "微信账号"),
    PUSH(2,"个推");

    private int type;

    private String msg;

    ThirdSourceEnum(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }
}
