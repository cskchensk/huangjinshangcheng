package cn.ug.enums;

import org.apache.commons.lang3.StringUtils;

public enum ShareUserTypeEnum {
    REGISTER_NOT_TIEDCARD(1, "注册未绑卡用户"),
    NOT_EXCHANGE(2, "绑卡未交易用户"),
    EXCHANGE(3, "已产生交易用户"),
    CUSTOM(4,"自定义创建");

    private int code;
    private String name;

    ShareUserTypeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    /**
     * 根据code获取枚举
     *
     * @param code
     * @return OrderStatusEnum
     */
    public static ShareUserTypeEnum getByCode(int code) {
        for (ShareUserTypeEnum enumVal : ShareUserTypeEnum.values()) {
            if (code == enumVal.getCode()) {
                return enumVal;
            }
        }
        return null;
    }

    /**
     * 根据name获取枚举
     *
     * @param name
     * @return OrderStatusEnum
     */
    public static ShareUserTypeEnum getByName(String name) {
        for (ShareUserTypeEnum enumVal : ShareUserTypeEnum.values()) {
            if (StringUtils.equals(name, enumVal.getName())) {
                return enumVal;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public int getCode() {
        return code;
    }
}
