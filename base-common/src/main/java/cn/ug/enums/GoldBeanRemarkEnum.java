package cn.ug.enums;

public enum GoldBeanRemarkEnum {
    LEASEBACK_REWARDS("回租奖励"),
    EXPERIENCE_REWARDS("体验金奖励"),
    SIGNIN_REWARDS("签到奖励"),
    REGISTER_REWARDS("新用户注册奖励"),
    BINDING_CARD_REWARDS("认证绑卡奖励"),
    INVITATION_REWARDS("邀请好友奖励"),
    SHARE_REWARDS("分享奖励"),
    REPARATION_REWARDS("老用户补偿"),
    PAY_EXPERIENCE_REWARDS("首次体验金购买成功"),
    LEASEBACK_SUCCESS_REWARDS("首次回租提单成功"),
    PAY_ORNAMENT_REWARDS("首次金饰购买成功"),
    PAY_GOLD_REWARDS("首次实物金购买成功"),
    PAY_ORNAMENT_DEDUCTION("购买金饰抵扣"),
    PAY_GOLD_DEDUCTION("购买实物金抵扣"),
    GOLD_BEAN_DEDUCT("金豆过期扣减"),
    FIRST_PAY_REWEARDS("首笔买金奖励"),
    EVERY_PAY_GOLD_REWARDS("单笔满额奖励"),
    LEASEBACK_ENJOY_REWARDS("回租专享优惠"),
    EXCHANGE("兑换支出"),
    INVITEE_REWARDS("邀友注册赠送");

    private String remark;

    GoldBeanRemarkEnum(String remark) {
        this.remark = remark;
    }

    public String getRemark() {
        return remark;
    }
}
