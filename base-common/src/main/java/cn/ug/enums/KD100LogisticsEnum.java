package cn.ug.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 快递100 物流枚举
 * @author zhaohg
 * @date 2018/07/19.
 */

public enum KD100LogisticsEnum {
    SHUN_FENG(1, "shunfeng","顺丰快递"),
    ;

    private int code;
    private String number;
    private String msg;

    KD100LogisticsEnum(int code, String number,String msg) {
        this.code = code;
        this.number = number;
        this.msg = msg;
    }

    public int code() {
        return code;
    }

    public String number() {
        return number;
    }

    public String msg() {
        return msg;
    }

    public static KD100LogisticsEnum get(int code) {
        for (KD100LogisticsEnum item : KD100LogisticsEnum.values()) {
            if (item.code() == code) {
                return item;
            }
        }
        return null;
    }

    /**
     * 默认顺丰快递
     * @param msg
     * @return
     */
    public static KD100LogisticsEnum getByMsgIndex(String msg) {
        if (StringUtils.isBlank(msg)){
            return SHUN_FENG;
        }
        for (KD100LogisticsEnum item : KD100LogisticsEnum.values()) {
            if (msg.contains(item.msg())) {
                return item;
            }
        }
        return SHUN_FENG;
    }

}