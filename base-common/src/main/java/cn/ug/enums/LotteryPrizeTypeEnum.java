package cn.ug.enums;

public enum LotteryPrizeTypeEnum {
    //1:现金；2：黄金；3：现金红包；4：福利券；5：电子卡；6：实物礼品；7：未中奖
    CASH(1, "现金"),
    GOLD(2, "黄金"),
    KICKBACK(3, "现金红包"),
    INTEREST(4, "福利券"),
    ELECTRONIC_CARD(5, "电子卡"),
    GOODS(6, "实物礼品"),
    UN_PRIZE(7, "未中奖"),
    GOLD_PACKET(8, "黄金红包");


    private int type;
    private String msg;

    LotteryPrizeTypeEnum(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }
}
