package cn.ug.enums;

public enum ProvinceRuleEnum {
    BEIJING("11", "北京"),
    TIANJIN("12", "天津"),
    HEBEI("13", "河北"),
    SHANXI("14", "山西"),
    NEIMENGGU("15", "内蒙古"),
    LIAONING("21", "辽宁"),
    JILIN("22", "吉林"),
    HEILUNGKIANG("23", "黑龙江"),
    SHANGHAI("31", "上海"),
    JIANGSU("32", "江苏"),
    CHEKIANG("33", "浙江"),
    ANHWEI("34", "安徽"),
    FUKIEN("35", "福建"),
    JIANGXI("36", "江西"),
    SHANDONG("37", "山东"),
    HENAN("41", "河南"),
    HUPEI("42", "湖北"),
    HUNAN("43", "湖南"),
    GUANGDONG("44", "广东"),
    GUANGXI("45", "广西"),
    KAINAN("46", "海南"),
    CHUNGKING("50", "重庆"),
    SICHUAN("51", "四川"),
    GUIZHOU("52", "贵州"),
    YUNNAN("53", "云南"),
    SITSANG("54", "西藏"),
    SHENSI("61", "陕西"),
    GANSU("62", "甘肃"),
    QINGHAI("63", "青海"),
    NINGXIA("64", "宁夏"),
    SINKIANG("65", "新疆"),
    TAIWAN("71", "台湾"),
    DIAOYU_ISLANDS("72", "钓鱼岛"),
    HONG_KONG("81", "香港"),
    MACAO("82", "澳门");

    private String code;
    private String province;

    ProvinceRuleEnum(String code, String province) {
        this.province = province;
        this.code = code;
    }

    public static String getProvinceByCode(String code) {
        for (ProvinceRuleEnum v : ProvinceRuleEnum.values()) {
            if (v.getCode().equals(code)) {
                return v.getProvince();
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public String getProvince() {
        return province;
    }
}
