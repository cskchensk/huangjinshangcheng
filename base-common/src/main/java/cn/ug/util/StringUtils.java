package cn.ug.util;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by zhangweijie on 2017/7/11.
 */
public class StringUtils {

    /**
     * 反转List组装成字符串
     *
     * @param list
     * @param divider 分隔符
     * @return
     */
    public static String reverseList(List<String> list, String divider) {
        StringBuffer stringBuffer = new StringBuffer();
        int startPos = list.size() - 1;
        for (int i = startPos; i >= 0; i--) {
            if (divider != null && i != startPos) {
                stringBuffer.append(divider);
            }
            stringBuffer.append(list.get(i));
        }
        return stringBuffer.toString();
    }

    /**
     * 是否包含汉字
     */
    public static boolean containsHanScript(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }

        return s.codePoints().anyMatch(
                codePoint ->
                        Character.UnicodeScript.of(codePoint) == Character.UnicodeScript.HAN);
    }

    /**
     * 提取汉字(只提取第一组汉字)
     */
    public static String getFirstHanGroup(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }

        String regex = "([\u4e00-\u9fa5]+)";
        Matcher matcher = Pattern.compile(regex).matcher(s);
        if (matcher.find()) {
            return matcher.group(0);
        }
        return "";
    }

    public static boolean isNull(String str) {
        return str == null || str.length() == 0 || str.equals("null") || str.equals("NULL");
    }

    /**
     * 去除重复字符串
     *
     * @param params 字符串
     * @return divider 分隔符
     */
    public static String removeSameString(String params, String divider) {
        if (org.apache.commons.lang3.StringUtils.isBlank(divider))
            divider = ",";

        Set<String> mlinkedset = new LinkedHashSet<>();
        String[] strArray = params.split(divider);
        for (String str : strArray) {
            mlinkedset.add(str);
        }
        return String.join(divider, mlinkedset);
    }

    /**
     * 将空字符转为""，保证返回字符不为空
     *
     * @param str
     * @return
     */
    public static String null2String(Object str) {
        return str == null ? "" : str.toString();
    }

    /**
     * 银行卡 敏感处理
     *
     * @param str
     * @return
     */
    public static String sensitivityBankCard(String str) {
        return str.substring(0, 4) + " **** **** " + str.substring(str.length() - 4, str.length());
    }


    /**
     * 版本号比较
     *
     * @param v1
     * @param v2
     * @return 0代表相等，1代表左边大，-1代表右边大
     * Utils.compareVersion("1.0.358_20180820090554","1.0.358_20180820090553")=1
     */
    public static int compareVersion(String v1, String v2) {
        if (v1.equals(v2)) {
            return 0;
        }
        String[] version1Array = v1.split("[._]");
        String[] version2Array = v2.split("[._]");
        int index = 0;
        int minLen = Math.min(version1Array.length, version2Array.length);
        long diff = 0;

        while (index < minLen
                && (diff = Long.parseLong(version1Array[index])
                - Long.parseLong(version2Array[index])) == 0) {
            index++;
        }
        if (diff == 0) {
            for (int i = index; i < version1Array.length; i++) {
                if (Long.parseLong(version1Array[i]) > 0) {
                    return 1;
                }
            }

            for (int i = index; i < version2Array.length; i++) {
                if (Long.parseLong(version2Array[i]) > 0) {
                    return -1;
                }
            }
            return 0;
        } else {
            return diff > 0 ? 1 : -1;
        }
    }
}
