package cn.ug.util;

public interface RedisConstantUtil {
    public static final String INVITE_CODE_KEY = "ug:invite:code:key";
    public static final String CHANNEL_CODE_KEY = "ug:channel:code:key";
    public static final String GAME_CODE_KEY = "ug:game:code:key";
    public static final String BIND_CARD_KEY = "ug:bind:card:key:";
    public static final String ESIGN_ERROR_KEY = "ug:esign:error:num:";
    public static final String WITHDRAW_STATUS_ERROR_KEY = "ug:withdraw:status:error:num:";
    public static final String DONATION_CASH_ERROR_KEY = "ug:donation:cash:error:num:";
    public static final String REWARDS_ERROR_KEY = "ug:rewards:error:num:";
    public static final String LOTTERY_SHARE_NUM_KEY = "ug:lottery:share:num:";
    public static final String LOTTERY_SHARE_DRAW_NUM_KEY = "ug:lottery:share:gotDrawNum:";
    public static final String WX_ACCESSTOKEN_KEY = "ug:wx:accesstoken:key:";
    public static final String WX_CASH_ERROR_KEY = "ug:wx:cash:error:num:";
    public static final String CHANNEL_CASH_KEY = "ug:channel:cash:key:";
    public static final String LOTTERY_DOUBLEELEVEN_DRAW_NUM_KEY = "ug:lottery:doubleEleven:gotDrawNum:";
}
