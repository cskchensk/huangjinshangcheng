package cn.ug.util;

import java.math.BigDecimal;

public class BigDecimalUtil {
	/**
	 * 汉语中数字大写
	 */
	private static final String[] CN_UPPER_NUMBER = { "零", "壹", "贰", "叁", "肆",
			"伍", "陆", "柒", "捌", "玖" };
	/**
	 * 汉语中货币单位大写，这样的设计类似于占位符
	 */
	private static final String[] CN_UPPER_MONETRAY_UNIT = { "分", "角", "元",
			"拾", "佰", "仟", "万", "拾", "佰", "仟", "亿", "拾", "佰", "仟", "兆", "拾",
			"佰", "仟" };
	/**
	 * 特殊字符：整
	 */
	private static final String CN_FULL = "整";
	/**
	 * 特殊字符：负
	 */
	private static final String CN_NEGATIVE = "负";
	/**
	 * 金额的精度，默认值为2
	 */
	private static final int MONEY_PRECISION = 2;
	/**
	 * 特殊字符：零元整
	 */
	private static final String CN_ZEOR_FULL = "零元" + CN_FULL;


	/**
	 * 返回两位小数点
	 * @param v
	 * @return
	 */
	public static BigDecimal to2Point(BigDecimal v) {
		if (v == null) {
			return new BigDecimal(0.00);
		}
		return v.setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 返回五位小数点
	 * @param v
	 * @return
	 */
	public static BigDecimal to5Point(BigDecimal v) {
		if (v == null) {
			return new BigDecimal(0.00000);
		}
		return v.setScale(5, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * 相加
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static BigDecimal add(BigDecimal v1, BigDecimal v2) {
		if(null == v1) {
			v1 = BigDecimal.ZERO;
		}
		if(null == v2) {
			v2 = BigDecimal.ZERO;
		}
		return v1.add(v2);
	}

	/**
	 * 相加
	 * @param v
	 * @return
	 */
	public static BigDecimal adds(BigDecimal... v) {
		int len = v.length;
		BigDecimal result = null;
		for (int i = 0; i < len; i++) {
			result = add(result, v[i]);
		}
		return result;
	}

	/**
	 * 减少并取绝对值
	 * @param v1	被减数
	 * @param v2	减数
	 * @return		差，并取绝对值
	 */
	public static BigDecimal subtractAndAbs(BigDecimal v1, BigDecimal v2) {
		if(null == v1) {
			v1 = BigDecimal.ZERO;
		}
		if(null == v2) {
			v2 = BigDecimal.ZERO;
		}
		return v1.subtract(v2).abs();
	}

	/**
	 * 减少
	 * @param v1	被减数
	 * @param v2	减数
	 * @return		差
	 */
	public static BigDecimal subtract(BigDecimal v1, BigDecimal v2) {
		if(null == v1) {
			v1 = BigDecimal.ZERO;
		}
		if(null == v2) {
			v2 = BigDecimal.ZERO;
		}
		return v1.subtract(v2);
	}

	/**
	 * 相乘
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static BigDecimal multiply(BigDecimal v1, BigDecimal v2) {
        if(null == v1) {
            v1 = BigDecimal.ZERO;
        }
        if(null == v2) {
            v2 = BigDecimal.ZERO;
        }
		return v1.multiply(v2);
	}

	/**
	 * 判断是否为null或者0
	 * @param v
	 * @return false:null或0，true其他值
	 */
	public static Boolean isZeroOrNull(BigDecimal v) {
		if (null == v) {
			return true;
		}
		if(BigDecimal.ZERO.compareTo(v) == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 把输入的金额转换为汉语中人民币的大写
	 *
	 * @param numberOfMoney
	 *            输入的金额
	 * @return 对应的汉语大写
	 */
	public static String number2CNMontrayUnit(BigDecimal numberOfMoney) {
		StringBuffer sb = new StringBuffer();
		// -1, 0, or 1 as the value of this BigDecimal is negative, zero, or
		// positive.
		int signum = numberOfMoney.signum();
		// 零元整的情况
		if (signum == 0) {
			return CN_ZEOR_FULL;
		}
		// 这里会进行金额的四舍五入
		long number = numberOfMoney.movePointRight(MONEY_PRECISION)
				.setScale(0, 4).abs().longValue();
		// 得到小数点后两位值
		long scale = number % 100;
		int numUnit = 0;
		int numIndex = 0;
		boolean getZero = false;
		// 判断最后两位数，一共有四中情况：00 = 0, 01 = 1, 10, 11
		if (!(scale > 0)) {
			numIndex = 2;
			number = number / 100;
			getZero = true;
		}
		if ((scale > 0) && (!(scale % 10 > 0))) {
			numIndex = 1;
			number = number / 10;
			getZero = true;
		}
		int zeroSize = 0;
		while (true) {
			if (number <= 0) {
				break;
			}
			// 每次获取到最后一个数
			numUnit = (int) (number % 10);
			if (numUnit > 0) {
				if ((numIndex == 9) && (zeroSize >= 3)) {
					sb.insert(0, CN_UPPER_MONETRAY_UNIT[6]);
				}
				if ((numIndex == 13) && (zeroSize >= 3)) {
					sb.insert(0, CN_UPPER_MONETRAY_UNIT[10]);
				}
				sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
				sb.insert(0, CN_UPPER_NUMBER[numUnit]);
				getZero = false;
				zeroSize = 0;
			} else {
				++zeroSize;
				if (!(getZero)) {
					sb.insert(0, CN_UPPER_NUMBER[numUnit]);
				}
				if (numIndex == 2) {
					if (number > 0) {
						sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
					}
				} else if (((numIndex - 2) % 4 == 0) && (number % 1000 > 0)) {
					sb.insert(0, CN_UPPER_MONETRAY_UNIT[numIndex]);
				}
				getZero = true;
			}
			// 让number每次都去掉最后一个数
			number = number / 10;
			++numIndex;
		}
		// 如果signum == -1，则说明输入的数字为负数，就在最前面追加特殊字符：负
		if (signum == -1) {
			sb.insert(0, CN_NEGATIVE);
		}
		// 输入的数字小数点后两位为"00"的情况，则要在最后追加特殊字符：整
		if (!(scale > 0)) {
			sb.append(CN_FULL);
		}
		return sb.toString();
	}

	public static void main(String[] args) {//
		// System.out.println(add(null, null));
		// System.out.println(subtract(new BigDecimal(1L), null));
		// System.out.println(subtract(null, new BigDecimal(2L)));
		// System.out.println(subtract(new BigDecimal(1L), new BigDecimal(4L)));
		//System.out.println(adds(new BigDecimal(1), new BigDecimal(2), new BigDecimal(3)));
		//System.out.println(new BigDecimal(4.1).setScale(0, BigDecimal.ROUND_HALF_UP));

		/*double money = 3000.66;
		BigDecimal numberOfMoney = new BigDecimal(money);
		String s = number2CNMontrayUnit(numberOfMoney);
		System.out.println("你输入的金额为：【" + money + "】   #--# [" + s.toString());*/
		String date = "2019-08-30 15:30:00";
		System.out.println(date.substring(0,10));
	}
}
