package cn.ug.util;


import com.alibaba.fastjson.JSON;
import com.gexin.rp.sdk.base.IPushResult;
import com.gexin.rp.sdk.base.impl.ListMessage;
import com.gexin.rp.sdk.base.impl.SingleMessage;
import com.gexin.rp.sdk.base.impl.Target;
import com.gexin.rp.sdk.base.payload.APNPayload;
import com.gexin.rp.sdk.http.IGtPush;
import com.gexin.rp.sdk.template.LinkTemplate;
import com.gexin.rp.sdk.template.NotificationTemplate;
import com.gexin.rp.sdk.template.TransmissionTemplate;
import com.gexin.rp.sdk.template.style.Style0;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class AppPushUtils {

    //定义常量, appId、appKey、masterSecret  在"个推控制台"中获得的应用配置
    // 由IGetui管理页面生成，是您的应用与SDK通信的标识之一，每个应用都对应一个唯一的AppID
    private static String appId = "";
    // 预先分配的第三方应用对应的Key，是您的应用与SDK通信的标识之一。
    private static String appKey = "";
    // 个推服务端API鉴权码，用于验证调用方合法性。在调用个推服务端API时需要提供。（请妥善保管，避免通道被盗用）
    private static String masterSecret = "";

    private static String host = "http://sdk.open.api.igexin.com/apiex.htm";


    // 构造器
    public AppPushUtils(String appId, String appKey, String masterSecret) {
        // 初始化类
        this.appId = appId;
        this.appKey = appKey;
        this.masterSecret = masterSecret;
    }

    // 设置通知消息模板
    /*
     * 1. appId
     * 2. appKey
     * 3. 要传送到客户端的 msg
     * 3.1 标题栏：key = title,
     * 3.2 通知栏内容： key = text,
     */
    private static LinkTemplate getLinkTemplate(String appId, String appKey, Map<String, String> msg) {
        // 在通知栏显示一条含图标、标题等的通知，用户点击后激活您的应用
        LinkTemplate template = new LinkTemplate();
        // 穿透消息设置为，1 强制启动应用
        // 设置appid，appkey
        template.setAppId(appId);
        template.setAppkey(appKey);
        // 穿透消息设置为，1 强制启动应用
        //template.setTransmissionType(1);
        // 设置style
        Style0 style = new Style0();
        // 设置通知栏标题和内容
        style.setTitle(msg.get("title"));
        style.setText(msg.get("content"));
        if (StringUtils.isNotBlank(msg.get("logo"))) {
            // 配置通知栏图标
            style.setLogo(msg.get("logo"));
        }
        if (StringUtils.isNotBlank(msg.get("logoUrl"))) {
            // 配置通知栏网络图标
            style.setLogoUrl(msg.get("logoUrl"));
        }

        // 设置通知，响铃、震动、可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        // 设置
        template.setStyle(style);
        // 设置打开的网址地址
        template.setUrl(msg.get("skipUrl"));
        return template;

    }

    // 设置通知消息模板
    /*
     * 1. appId
     * 2. appKey
     * 3. 要传送到客户端的 msg
     * 3.1 标题栏：key = title,
     * 3.2 通知栏内容： key = titleText,
     * 3.3 穿透内容：key = transText
     */
    private static NotificationTemplate getNotifacationTemplate(String appId, String appKey, Map<String, String> msg) {
        // 在通知栏显示一条含图标、标题等的通知，用户点击后激活您的应用
        NotificationTemplate template = new NotificationTemplate();
        // 设置appid，appkey
        template.setAppId(appId);
        template.setAppkey(appKey);
        // 穿透消息设置为，1 强制启动应用
        template.setTransmissionType(1);
        // 设置穿透内容
        template.setTransmissionContent(msg.get("title"));
        // 设置style
        Style0 style = new Style0();
        // 设置通知栏标题和内容
        style.setTitle(msg.get("title"));
        style.setText(msg.get("content"));
        if (StringUtils.isNotBlank(msg.get("logo"))) {
            // 配置通知栏图标
            style.setLogo(msg.get("logo"));
        }
        if (StringUtils.isNotBlank(msg.get("logoUrl"))) {
            // 配置通知栏网络图标
            style.setLogoUrl(msg.get("logoUrl"));
        }
        // 设置通知，响铃、震动、可清除
        style.setRing(true);
        style.setVibrate(true);
        style.setClearable(true);
        // 设置
        template.setStyle(style);
        return template;
    }

    /**
     * 透传模板
     * @param appId
     * @param appKey
     * @param msg
     * @return
     */
    private static TransmissionTemplate getTransmissionTemplate(String appId, String appKey, Map<String, String> msg){
        TransmissionTemplate template = new TransmissionTemplate();//透传方式
        template.setAppId(appId);
        template.setAppkey(appKey);
        String str = JSON.toJSONString(msg);
        template.setTransmissionContent(str);
        //收到消息是否立即启动应用，1为立即启动，2则广播等待客户端自启动
        template.setTransmissionType(2);

        APNPayload payload = new APNPayload();
        //在已有数字基础上加1显示，设置为-1时，在已有数字上减1显示，设置为数字时，显示指定数字
        payload.setAutoBadge("+1");
        payload.setContentAvailable(1);
        payload.setSound("default");
        payload.setCategory("$由客户端定义");
        //简单模式APNPayload.SimpleMsg
        payload.setAlertMsg(new APNPayload.SimpleAlertMsg(msg.get("content")));
        payload.addCustomMsg("title",msg.get("title"));
        payload.addCustomMsg("content",msg.get("content"));
        payload.addCustomMsg("skipUrl",msg.get("skipUrl"));
        payload.addCustomMsg("skipType",msg.get("skipType"));
        template.setAPNInfo(payload);
        return template;
    }
    /**
     * 对单个用户推送消息
     * 1. cid
     * 2. 要传到客户端的 msg
     * 2.1 标题栏：key = title,
     */
    public static IPushResult pushMsgToSingle(String cid, String alias, Map<String, String> msg) {
        // 代表在个推注册的一个 app，调用该类实例的方法来执行对个推的请求
        IGtPush push = new IGtPush(host, appKey, masterSecret);
        // 创建信息模板
        NotificationTemplate template = getNotifacationTemplate(appId, appKey, msg);
        //定义消息推送方式为，单推
        SingleMessage message = new SingleMessage();
        message.setOffline(true);
        // 离线有效时间，单位为毫秒，可选
        message.setOfflineExpireTime(24 * 3600 * 500);
        // 可选，1为wifi，0为不限制网络环境。根据手机处于的网络情况，决定是否下发
        message.setPushNetWorkType(0);
        // 设置推送消息的内容
        message.setData(template);
        // 设置推送目标
        Target target = new Target();
        target.setAppId(appId);
        if (!StringUtils.isBlank(cid)) {
            // 设置cid
            target.setClientId(cid);
        } else {
            target.setAlias(alias);
        }


        // 获得推送结果
        IPushResult result = push.pushMessageToSingle(message, target);
        /*
         * 1. 失败：{result=sign_error}
         * 2. 成功：{result=ok, taskId=OSS-0212_1b7578259b74972b2bba556bb12a9f9a, status=successed_online}
         * 3. 异常
         */
        return result;
    }


    /**
     * 透传请求
     * @param cid
     * @param alias
     * @param msg
     * @return
     */
    public static IPushResult pushTransmissionMsgToSingle(String cid, String alias, Map<String, String> msg) {
        // 代表在个推注册的一个 app，调用该类实例的方法来执行对个推的请求
        IGtPush push = new IGtPush(host, appKey, masterSecret);
        // 创建透传信息模板
        TransmissionTemplate template = getTransmissionTemplate(appId, appKey, msg);
        //定义消息推送方式为，单推
        SingleMessage message = new SingleMessage();
        message.setOffline(true);
        // 离线有效时间，单位为毫秒，可选
        message.setOfflineExpireTime(500);
        // 可选，1为wifi，0为不限制网络环境。根据手机处于的网络情况，决定是否下发
        message.setPushNetWorkType(0);
        // 设置推送消息的内容
        message.setData(template);
        // 设置推送目标
        Target target = new Target();
        target.setAppId(appId);
        if (!StringUtils.isBlank(cid)) {
            // 设置cid
            target.setClientId(cid);
        } else {
            target.setAlias(alias);
        }
        // 获得推送结果
        IPushResult result = push.pushMessageToSingle(message, target);
        /*
         * 1. 失败：{result=sign_error}
         * 2. 成功：{result=ok, taskId=OSS-0212_1b7578259b74972b2bba556bb12a9f9a, status=successed_online}
         * 3. 异常
         */
        return result;
    }

    /**
     * 指定列表用户推送消息
     *
     * @param cids  ClientId集合
     * @param msg   消息信息
     * @param msg
     * @return
     */
    public static IPushResult pushMsgToList(List<String> cids, Map<String, String> msg) {
        IGtPush push = new IGtPush(host, appKey, masterSecret);
        // 通知透传模板
        NotificationTemplate template = getNotifacationTemplate(appId, appKey, msg);
        ListMessage message = new ListMessage();
        message.setData(template);
        // 设置消息离线，并设置离线时间
        message.setOffline(true);
        // 离线有效时间，单位为毫秒，可选
        message.setOfflineExpireTime(24 * 1000 * 3600);
        // 配置推送目标
        List targets = new ArrayList();
        for (String cid : cids) {
            Target target = new Target();
            target.setAppId(appId);
            target.setClientId(cid);
            targets.add(target);
        }

        // taskId用于在推送时去查找对应的message
        String taskId = push.getContentId(message);
        IPushResult ret = push.pushMessageToList(taskId, targets);
        return ret;
    }


    public static void main(String[] args) {
        // 由IGetui管理页面生成，是您的应用与SDK通信的标识之一，每个应用都对应一个唯一的AppID
        String APPID_ANDROID = "jmy8iaPpAC9m7A1rVT5hY2";
        // 预先分配的第三方应用对应的Key，是您的应用与SDK通信的标识之一。
        String APPKEY_ANDROID = "buOLV8Oj228hHu0ApftEM6";
        // 个推服务端API鉴权码，用于验证调用方合法性。在调用个推服务端API时需要提供。（请妥善保管，避免通道被盗用）
        String MASTERSECRET_ANDROID = "pPILaZuC2i8NO2eWhJp529";
        AppPushUtils appPushUtils =  new AppPushUtils(APPID_ANDROID,APPKEY_ANDROID,MASTERSECRET_ANDROID);
        Map<String,String> pushMap = new HashMap();
        pushMap.put("title","习近平谈建设海洋强国3");
        pushMap.put("content","我国既是陆地大国，也是海洋大国，拥有广泛的海洋战略利益。经过多年发展");
        pushMap.put("skipUrl","http://www.baidu.com");
        pushMap.put("skipType",String.valueOf(8));
        // iso: 8061437521d71846377c100e88f20392  安卓： 27c882d1312c7447ae981585c07ea4d4
        IPushResult iPushResult = appPushUtils.pushTransmissionMsgToSingle("27c882d1312c7447ae981585c07ea4d4", null, pushMap);
        IPushResult iPushResult22 = appPushUtils.pushTransmissionMsgToSingle("8061437521d71846377c100e88f20392", null, pushMap);
        System.out.println(iPushResult22.getResponse().toString());
    }
}