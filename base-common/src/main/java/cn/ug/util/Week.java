package cn.ug.util;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 周-数据统计
 */
public class Week implements Serializable{

    /** 日期(2018-06-01~2018-06-10) **/
    private String day;
    /** 开始日期 **/
    private LocalDate startTime;
    /** 结束日期 **/
    private LocalDate endTime;
    /** 开始日期数字化 **/
    private Integer startTimeNumber;
    /** 结束日期数字化 **/
    private Integer endTimeNumber;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDate startTime) {
        this.startTime = startTime;
    }

    public LocalDate getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDate endTime) {
        this.endTime = endTime;
    }

    public Integer getStartTimeNumber() {
        return startTimeNumber;
    }

    public void setStartTimeNumber(Integer startTimeNumber) {
        this.startTimeNumber = startTimeNumber;
    }

    public Integer getEndTimeNumber() {
        return endTimeNumber;
    }

    public void setEndTimeNumber(Integer endTimeNumber) {
        this.endTimeNumber = endTimeNumber;
    }
}
