package cn.ug.util;

public interface ConstantUtil {
    public static final String BACK_SLASH = "\\";
    public static final String SHARP = "#";
    public static final String PERCENT = "%";
    public static final String DOLLAR = "$";
    public static final String SLASH = "/";
    public static final String DOT =".";
    public static final String ELLIPSIS ="...";
    public static final String COMMA =",";
    public static final String MINUS = "-";
    public static final String UNDERLINE = "_";
    public static final String BLANK = "";
    public static final String SEMICOLON = ";";
    public static final String WAVE = "～";
    public static final String COLON = ":";
    public static final String LINE_SEPARATOR = "line.separator";
    public static final String EQUAL = "=";
    public static final String AND = "&";
    public static final String BR = "<br/>";
    public static final String EMPTY = "";
    public static final String QUOTES = "\"";
    public static final String ASK = "?";
    public static final String ZERO = "0";
    public static final String UTF8 = "UTF-8";
    public static final String GBK = "gbk";
    public static final String ALGORITHM = "HmacSHA256";
    public static final String CONTENT_TYPE = "application/json";;
    public static final String NORMAL_DATE_FORMAT = "yyyy-MM-dd";
    public static final String NORMAL_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String NORMAL_MONTH_FORMAT = "yyyy-MM";
    public static final String MAX_TIME = " 23:59:59";
    public static final String MIN_TIME = " 00:00:00";
    public static final String GOLD_WEIGHT_RANGE = "募集克重上下限范围";
    public static final String NATIVE_PRI_KEY = "a5f76c56a4edd361098a9340eb7242ce";
}
