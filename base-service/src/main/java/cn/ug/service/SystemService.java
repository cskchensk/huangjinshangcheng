package cn.ug.service;

import cn.ug.bean.LogBean;
import cn.ug.bean.LoginBean;
import cn.ug.bean.type.LogType;
import cn.ug.config.CacheService;
import cn.ug.core.login.LoginHelper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static cn.ug.config.QueueName.QUEUE_SYS_LOG;

/**
 * 系统服务
 * @author kaiwotech
 */
@Service
public class SystemService extends CacheService {
    private static Log log = LogFactory.getLog(SystemService.class);

    @Resource
    private AmqpTemplate amqpTemplate;

    /**
     * 添加操作日志
     * @param type          日志类型
     */
    public void log(LogType type) {
        log(type, null, null);
    }

    /**
     * 添加操作日志
     * @param type          日志类型
     * @param description   操作描述
     */
    public void log(LogType type, String description) {
        log(type, null, description);
    }

    /**
     * 添加操作日志
     * @param type          日志类型
     * @param thirdNumber   功能流水号
     * @param description   操作描述
     */
    public void log(LogType type, String thirdNumber, String description) {
        if(null == type) {
            return;
        }
        LogBean logBean = new LogBean();
        logBean.setType(type.toString());
        logBean.setThirdNumber(thirdNumber);
        logBean.setDescription(description);
        // 操作会员资料
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(null != loginBean) {
            if(null == logBean.getUserType()) {
                logBean.setUserType(loginBean.getLoginType());
            }
            if(StringUtils.isBlank(logBean.getUserId())) {
                logBean.setUserId(loginBean.getId());
            }
            if(StringUtils.isBlank(logBean.getUserName())) {
                logBean.setUserName(loginBean.getName());
            }
            if(StringUtils.isBlank(logBean.getUserLoginName())) {
                logBean.setUserLoginName(loginBean.getLoginName());
            }
        }

        amqpTemplate.convertAndSend(QUEUE_SYS_LOG, logBean);
    }

    public void log(LogType type, String thirdNumber, String description, int userType, String userId, String userName, String loginName) {
        if(null == type) {
            return;
        }
        LogBean logBean = new LogBean();
        logBean.setType(type.toString());
        logBean.setThirdNumber(thirdNumber);
        logBean.setDescription(description);
        // 操作会员资料
        logBean.setUserType(userType);
        logBean.setUserId(userId);
        logBean.setUserName(userName);
        logBean.setUserLoginName(loginName);
        amqpTemplate.convertAndSend(QUEUE_SYS_LOG, logBean);
    }

}
