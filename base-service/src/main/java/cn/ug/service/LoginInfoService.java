package cn.ug.service;


import cn.ug.bean.LoginBean;

public interface LoginInfoService {
    LoginBean get(String key);
}
