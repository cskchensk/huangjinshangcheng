package cn.ug.service;

import cn.ug.config.CacheUtilsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class LoginCacheService extends CacheUtilsService {
    private static Log log = LogFactory.getLog(LoginCacheService.class);

    @Override
    protected final String getPrefix(String typeName) {
        return typeName;
    }
}
