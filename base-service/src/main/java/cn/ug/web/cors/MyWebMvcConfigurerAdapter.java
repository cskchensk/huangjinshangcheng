package cn.ug.web.cors;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MyWebMvcConfigurerAdapter extends WebMvcConfigurerAdapter {

//    @Autowired
//    private HtmlConverter htmlConverter;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        // 解决跨域
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .allowedHeaders("*");
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        // TODO 取消HTML过滤
        //registry.addConverter(htmlConverter);
    }
}
