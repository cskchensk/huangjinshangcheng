package cn.ug.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.web.handler.HealthCheckIndicator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 服务状态管理
 * @author kaiwotech
 */
@RestController
@RequestMapping("healthCheck")
public class HealthCheckController extends BaseController {

    @Resource
    private HealthCheckIndicator healthCheckIndicator;

    /**
     * 设置服务状态
     * @param up		    状态
     * @return			    操作结果
     */
    @RequestMapping(value = "up", method = GET)
    public SerializeObject up(Boolean up) {
        if(null == up) {
            return new SerializeObjectError("00000002");
        }
        healthCheckIndicator.setUp(up);
        return new SerializeObject<>(ResultType.NORMAL);
    }

}
