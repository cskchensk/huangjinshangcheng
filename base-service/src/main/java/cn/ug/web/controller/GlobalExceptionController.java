package cn.ug.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.exception.UnLoginException;
import cn.ug.core.exception.UnauthorizedException;
import cn.ug.core.exception.XRuntimeException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理
 * @author kaiwotech
 */
@RestControllerAdvice
public class GlobalExceptionController extends BaseController {

	@ExceptionHandler(value = Exception.class)
	public SerializeObject defaultErrorHandler(Exception e) throws Exception {
		return new SerializeObject<>(ResultType.EXCEPTION, e.getMessage());
	}

	@ExceptionHandler(value = UnauthorizedException.class)
	public SerializeObject defaultErrorHandler(UnauthorizedException e) throws Exception {
		return new SerializeObject<>(ResultType.UNAUTH, e.getMessage());
	}

	@ExceptionHandler(value = UnLoginException.class)
	public SerializeObject defaultErrorHandler(UnLoginException e) throws Exception {
		return new SerializeObject<>(ResultType.UNLOGIN, e.getMessage());
	}

	@ExceptionHandler(value = XRuntimeException.class)
	public SerializeObject xRuntimeExceptionErrorHandler(Exception e) throws Exception {
		return new SerializeObject<>(ResultType.ERROR, e.getMessage());
	}

}
