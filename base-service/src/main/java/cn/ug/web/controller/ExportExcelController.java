package cn.ug.web.controller;

import cn.ug.util.ExportExcelUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Collection;

/**
 * 导出excel基础类
 *
 * @param <T>
 * @author ywl
 */
public class ExportExcelController<T> extends ExportExcelUtil<T> {

    /**
     * <p>
     * 导出带有头部标题行的Excel <br>
     * 时间格式默认：yyyy-MM-dd hh:mm:ss <br>
     * </p>
     *
     * @param title   表格标题
     * @param headers 头部标题集合
     * @param dataset 数据集合
     * @param version 2003 或者 2007，不传时默认生成2003版本
     */
    public void exportExcel(String fileName, String title, String[] headers, String[] columns, Collection<T> dataset, HttpServletResponse response, String version) {
        try {
            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xls");
            if (StringUtils.isBlank(version) || EXCEL_FILE_2003.equals(version.trim())) {
                exportExcel2003(title, headers, columns, dataset, response.getOutputStream(), "yyyy-MM-dd hh:mm:ss", null);
            } else {
                exportExcel2007(title, headers, columns, dataset, response.getOutputStream(), "yyyy-MM-dd hh:mm:ss");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 双标题头导出   第一行为统计标题   第二行为常规标题
     * <p>
     * 导出带有双头部标题行的Excel <br>
     * 时间格式默认：yyyy-MM-dd hh:mm:ss <br>
     * </p>
     *
     * @param title      表格标题
     * @param headers    头部标题集合
     * @param dataset    数据集合
     * @param version    2003 或者 2007，不传时默认生成2003版本
     * @param firstLines 特殊的excel在头部标题集合前还有已行  统计记录
     */
    public void exportUnlikeExcel(String fileName, String title, String[] headers, String[] columns, Collection<T> dataset, HttpServletResponse response, String version, String[] firstLines) {
        try {
            response.setContentType("application/vnd.ms-excel");
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xls");
            if (StringUtils.isBlank(version) || EXCEL_FILE_2003.equals(version.trim())) {
                exportExcel2003(title, headers, columns, dataset, response.getOutputStream(), "yyyy-MM-dd hh:mm:ss", firstLines);
            } else {
                exportExcel2007(title, headers, columns, dataset, response.getOutputStream(), "yyyy-MM-dd hh:mm:ss");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 导出一个excel多个sheet
     *
     * @param workbook
     * @param fileName
     * @param title
     * @param headers
     * @param columns
     * @param dataset
     * @param response
     * @param version
     * @param firstLines
     * @param sheetNum
     * @param flag
     */
    public void exportExcelManySheet(HSSFWorkbook workbook, String fileName, String title, String[] headers, String[] columns, Collection<T> dataset, HttpServletResponse response, String version, String[] firstLines, int sheetNum, boolean flag) {
        try {
            if (sheetNum == 0) {
                response.setContentType("application/vnd.ms-excel");
                response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8") + ".xls");
            }
            if (StringUtils.isBlank(version) || EXCEL_FILE_2003.equals(version.trim())) {
                exportExcelManySheet2003(workbook, title, headers, columns, dataset, response.getOutputStream(), "yyyy-MM-dd hh:mm:ss", firstLines, sheetNum);
            } /*else {
                exportExcel2007(title, headers, columns, dataset, response.getOutputStream(), "yyyy-MM-dd hh:mm:ss");
            }*/
            try {
                if (flag)
                    workbook.write(response.getOutputStream());

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
