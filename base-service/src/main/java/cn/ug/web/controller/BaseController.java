package cn.ug.web.controller;

import cn.ug.cache.MessageCache;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

public class BaseController {
	protected static final ScheduledExecutorService EXECUTOR = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() * 2, new ThreadFactory() {
		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(r);
			t.setDaemon(true);
			return t;
		}
	});
	protected static Log log;
	private static MessageCache messageCache;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.setAutoGrowCollectionLimit(10000);
	}
		/**
         * @return the log
         */
	public final Log getLog() {
		if(null == log) {
			log = LogFactory.getLog(this.getClass());
		}
		return log;
	}

	protected String getIpAdrress(HttpServletRequest request) {
		String Xip = request.getHeader("X-Real-IP");
		String XFor = request.getHeader("X-Forwarded-For");
		if(StringUtils.isNotEmpty(XFor) && !"unKnown".equalsIgnoreCase(XFor)){
			//多次反向代理后会有多个ip值，第一个ip才是真实ip
			int index = XFor.indexOf(",");
			if(index != -1){
				return XFor.substring(0,index);
			}else{
				return XFor;
			}
		}
		XFor = Xip;
		if(StringUtils.isNotEmpty(XFor) && !"unKnown".equalsIgnoreCase(XFor)){
			return XFor;
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("HTTP_CLIENT_IP");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StringUtils.isBlank(XFor) || "unknown".equalsIgnoreCase(XFor)) {
			XFor = request.getRemoteAddr();
		}
		return XFor;
	}
	/**
	 * DEBUG级别日志
	 * @param message
	 */
	public final void debug(Object message) {
		getLog().debug(message);
	}

	/**
	 * INFO级别日志
	 * @param message
	 */
	public final void info(Object message) {
		getLog().info(message);
	}

	/**
	 * WARN级别日志
	 * @param message
	 */
	public final void warn(Object message) {
		getLog().warn(message);
	}

	/**
	 * ERROR级别日志
	 * @param message
	 */
	public final void error(Object message) {
		getLog().error(message);
	}

	/**
	 * 获取消息缓存
	 * @return
	 */
	public static MessageCache getMessageCache() {
		if(null == messageCache) {
			messageCache = MessageCache.getInstance();
		}
		return messageCache;
	}

	/**
	 * 获取缓存的消息
	 * @param code	编号
	 * @return
	 */
	public final String getMessage(String code) {
		return getMessageCache().getExceptionMessage(code);
	}

}
