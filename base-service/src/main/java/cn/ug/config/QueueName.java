package cn.ug.config;


/**
 * 队列名称
 * @author kaiwotech
 */
public class QueueName {

	/** 操作日志队列 */
	public static final String QUEUE_SYS_LOG = "sys:log";

	/** 提现状态队列 */
	public static final String QUEUE_PAY_WITHDRAW_STATUS = "pay:withdraw:status";

	/** 短信发送队列 */
	public static final String QUEUE_MSG_SMS_SEND = "msg:sms:send";

	/** 提现队列 **/
	public static final String QUEUE_PAY_WITHDRAW = "pay:withdraw";

	public static final String QUEUE_TEST = "test";

	/** 消息发送 1.短信 2.站内信 **/
	public static final String QUEUE_MSG_SEND = "msg:send";

	/** 绑卡或首次投资定期产品赠送优惠券  邀请返现奖励 产品返利**/
	public static final String QUEUE_ACTIVITY_REWARDS = "activity:rewards";

	/** 消息发送队列 DLX */
	public static final String DLX_MSG_SEND = "msg:send:dlx";

	/** 消息发送队列 延迟缓冲（按消息） */
	public static final String QUEUE_DELAY_PER_MESSAGE_TTL_MSG_SEND = "delay:per:message:msg:send";

	/** 使用红包/加息券 **/
	public static final String QUEUE_COUPON_DONATION_CASH = "activity:coupon:donation:cash";

	/** e签宝合同 **/
	public static final String QUEUE_ESIGN_CONTRACT = "activity:esign:contract";

	/** 会员分析 **/
	public static final String QUEUE_MEMBER_ANALYSE = "member:analyse";

	/** 会员标签 **/
	public static final String QUEUE_MEMBER_LABEL = "member:label";

	/** 会员密码状态 **/
	public static final String QUEUE_MEMBER_PASSWORD_STATUS = "member:password:status";
	public static final String QUEUE_MEMBER_PASSWORD_STATUS_DLX = "member:password:status:dlx";
	public static final String QUEUE_MEMBER_PASSWORD_STATUS_DELAY = "member:password:status:delay";


	/** 会员密码状态消息发送队列 延迟缓冲（推送） */
	public static final String QUEUE_DELAY_QUEUE_MEMBER_PASSWORD_STATUS = "delay:member:password:status";

	/** 微信消息推送 */
	public static final String QUEUE_MSG_WX_SEND = "msg:wx:send";

	/** 消息发送队列 DLX */
	public static final String DLX_MSG_WX_SEND = "msg:wx:send:dlx";

	/** 消息发送队列 延迟缓冲（推送） */
	public static final String QUEUE_DELAY_PER_MESSAGE_TTL_MSG_WX_SEND = "delay:per:message:msg:wx:send";
}
