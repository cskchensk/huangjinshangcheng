package cn.ug.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 缓存服务
 * @author kaiwotech
 */
@Service
public class CacheUtilsService extends CacheBaseService{
	private static Log log = LogFactory.getLog(CacheUtilsService.class);
	private static final String TYPE_NAME = CacheUtilsService.class.getTypeName();

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	/**
	 * 清除缓存
	 * @param hashKey	缓存key
	 * @return			缓存数据
	 */
	public void deleteCache(String hashKey) {
		deleteCache(TYPE_NAME, hashKey);
	}

	/**
	 * 清除缓存
	 * @param typeName	类名
	 * @param hashKey	缓存key
	 * @return			缓存数据
	 */
	public void deleteCache(String typeName, String hashKey) {
		if(null == hashKey || null == redisTemplate) {
			return;
		}

		try {
			String key = getPrefix(typeName) + ":" + hashKey;
			redisTemplate.delete(key);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


	/**
	 * 获取缓存
	 * @param hashKey	缓存key
	 * @return			缓存数据
	 */
	public Object getCache(String hashKey) {
		return getCache(TYPE_NAME, hashKey);
	}

	/**
	 * 获取缓存
	 * @param typeName	类名
	 * @param hashKey	缓存key
	 * @return			缓存数据
	 */
	public Object getCache(String typeName, String hashKey) {
		if(null == hashKey || null == redisTemplate) {
			return null;
		}

		Object obj = null;
		try {
			String key = getPrefix(typeName) + ":" + hashKey;
			obj = redisTemplate.opsForValue().get(key);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return obj;
	}

	/**
	 * 设置缓存
	 * @param hashKey	主键
	 * @param value		对象
	 */
	public void setCache(String hashKey, Object value) {
		setCache(TYPE_NAME, hashKey, value, 0, null);
	}

	/**
	 * 设置缓存
	 * @param typeName	类名
	 * @param hashKey	主键
	 * @param value		对象
	 */
	public void setCache(String typeName, String hashKey, Object value) {
		setCache(typeName, hashKey, value, 0, null);
	}

	/**
	 * 设置缓存
	 * @param typeName	类名
	 * @param hashKey	主键
	 * @param value		对象
	 * @param timeout	超时时间
	 * @param unit		时间单位
	 */
	public void setCache(String typeName, String hashKey, Object value, long timeout, TimeUnit unit) {
		try {
			if(null == hashKey || null == value || null == redisTemplate) {
				return;
			}
			String key = getPrefix(typeName) + ":" + hashKey;
			if(timeout <= 0) {
				// 永久
				redisTemplate.opsForValue().set(key, value);
			} else {
				// 临时
				redisTemplate.opsForValue().set(key, value, timeout, unit);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}
}
