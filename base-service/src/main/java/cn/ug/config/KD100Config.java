package cn.ug.config;

import java.io.Serializable;

/**
 * 快递100
 * @author zhaohg
 * @date 2018/07/19.
 */
public class KD100Config implements Serializable {

    private static final long serialVersionUID 	= 1L;

    private String callBackUrl;
    private String subscibeUrl;
    private String queryUrl;

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public String getSubscibeUrl() {
        return subscibeUrl;
    }

    public void setSubscibeUrl(String subscibeUrl) {
        this.subscibeUrl = subscibeUrl;
    }

    public String getQueryUrl() {
        return queryUrl;
    }

    public void setQueryUrl(String queryUrl) {
        this.queryUrl = queryUrl;
    }
}
