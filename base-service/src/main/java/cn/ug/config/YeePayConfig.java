package cn.ug.config;

import java.io.Serializable;

/**
 * 易宝支付配置
 * @author ywl
 */
public class YeePayConfig implements Serializable {

    private static final long serialVersionUID 	= 1L;
    /** 商户号 **/
    public String merchantAccount;
    /** 商户公钥 **/
    private String merchantPublicKey;
    /** 商户私钥 **/
    private String merchantPrivateKey;
    /** 易宝公钥 **/
    private String yeepayPublicKey;
    /** 有短验绑卡请求接口 **/
    private String bindCardRequestURL;
    /** 有短验绑卡请求确认接口 **/
    private String bindCardConfirmURL;
    /** 有短验绑卡请求重发短验接口 **/
    private String bindCardResendsmsURL;
    /** 无短验充值请求接口 **/
    private String unSendBindPayRequestURL;
    /** 银行卡信息查询接口请求地址 **/
    private String bankCardCheckURL;
    /** 提现接口请求地址 **/
    private String withdrawURL;
    /** 绑卡回调地址 **/
    private String bindCallBackUrl;
    /** 充值回调地址 **/
    private String rechargeCallBackUrl;
    /** 支付回调地址 **/
    private String payCallBackUrl;
    /** 提现回调地址 **/
    private String withdrawCallBackUrl;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getMerchantAccount() {
        return merchantAccount;
    }

    public void setMerchantAccount(String merchantAccount) {
        this.merchantAccount = merchantAccount;
    }

    public String getMerchantPublicKey() {
        return merchantPublicKey;
    }

    public void setMerchantPublicKey(String merchantPublicKey) {
        this.merchantPublicKey = merchantPublicKey;
    }

    public String getMerchantPrivateKey() {
        return merchantPrivateKey;
    }

    public void setMerchantPrivateKey(String merchantPrivateKey) {
        this.merchantPrivateKey = merchantPrivateKey;
    }

    public String getYeepayPublicKey() {
        return yeepayPublicKey;
    }

    public void setYeepayPublicKey(String yeepayPublicKey) {
        this.yeepayPublicKey = yeepayPublicKey;
    }

    public String getBindCardRequestURL() {
        return bindCardRequestURL;
    }

    public void setBindCardRequestURL(String bindCardRequestURL) {
        this.bindCardRequestURL = bindCardRequestURL;
    }

    public String getBindCardConfirmURL() {
        return bindCardConfirmURL;
    }

    public void setBindCardConfirmURL(String bindCardConfirmURL) {
        this.bindCardConfirmURL = bindCardConfirmURL;
    }

    public String getBindCardResendsmsURL() {
        return bindCardResendsmsURL;
    }

    public void setBindCardResendsmsURL(String bindCardResendsmsURL) {
        this.bindCardResendsmsURL = bindCardResendsmsURL;
    }

    public String getUnSendBindPayRequestURL() {
        return unSendBindPayRequestURL;
    }

    public void setUnSendBindPayRequestURL(String unSendBindPayRequestURL) {
        this.unSendBindPayRequestURL = unSendBindPayRequestURL;
    }

    public String getBankCardCheckURL() {
        return bankCardCheckURL;
    }

    public void setBankCardCheckURL(String bankCardCheckURL) {
        this.bankCardCheckURL = bankCardCheckURL;
    }

    public String getWithdrawURL() {
        return withdrawURL;
    }

    public void setWithdrawURL(String withdrawURL) {
        this.withdrawURL = withdrawURL;
    }

    public String getBindCallBackUrl() {
        return bindCallBackUrl;
    }

    public void setBindCallBackUrl(String bindCallBackUrl) {
        this.bindCallBackUrl = bindCallBackUrl;
    }

    public String getRechargeCallBackUrl() {
        return rechargeCallBackUrl;
    }

    public void setRechargeCallBackUrl(String rechargeCallBackUrl) {
        this.rechargeCallBackUrl = rechargeCallBackUrl;
    }

    public String getPayCallBackUrl() {
        return payCallBackUrl;
    }

    public void setPayCallBackUrl(String payCallBackUrl) {
        this.payCallBackUrl = payCallBackUrl;
    }

    public String getWithdrawCallBackUrl() {
        return withdrawCallBackUrl;
    }

    public void setWithdrawCallBackUrl(String withdrawCallBackUrl) {
        this.withdrawCallBackUrl = withdrawCallBackUrl;
    }


}
