package cn.ug.core;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.cache.MessageCache;

/**
 * 错误返回值
 * @author kaiwotech
 */
public class SerializeObjectError<T> extends SerializeObject<T> {

    public SerializeObjectError(String code, String... args) {
        this.setCode(ResultType.ERROR);
        String msg = MessageCache.getInstance().getExceptionMessage(code);
        if (args.length > 0) {
            msg = String.format(msg, args);
        }
        this.setMsg(msg);
    }
}
