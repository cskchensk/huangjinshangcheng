package cn.ug.core.ensure;

/**
 * @author kaiwotech
 */
public class EnsureParam<T> {

    protected T obj;

    public EnsureParam(T obj) {
        this.obj = obj;
    }

}
