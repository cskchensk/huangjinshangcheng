package cn.ug.aop;


import java.lang.annotation.*;

/**
 * 移除缓存
 * @author kaiwotech
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RemoveCache {

    int cleanObjectByKeyPosition() default -1;

    boolean cleanAll() default false;

    boolean cleanSearch() default false;

    Class<?>[] cleanService() default {};
}
