package cn.ug.aop;


import cn.ug.config.CacheType;

import java.lang.annotation.*;

/**
 * 保存缓存
 * @author kaiwotech
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SaveCache {

    CacheType cacheType() default CacheType.SEARCH;

    int timeout() default 60;

}
