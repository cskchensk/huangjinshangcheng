package cn.ug.aop;

/**
 * 权限验证
 * @author kaiwotech
 */
public enum Logical {
    AND, OR
}
