/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package cn.ug.feign;

import cn.ug.core.login.LoginHelper;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;




public class FeignAccesstokenInterceptor implements RequestInterceptor {
	private Log log = LogFactory.getLog(FeignAccesstokenInterceptor.class);

	@Override
	public void apply(RequestTemplate requestTemplate) {
		if (LoginHelper.getLocalAccessToken() == null) {
			log.info("localAccessToken 中的 accessToken，feign拦截器 >> 增强失败");
			return;
		}



		log.info("localAccessToken 中的 accessToken为"+LoginHelper.getLocalAccessToken());
		requestTemplate.header("accessToken",LoginHelper.getLocalAccessToken());
	}
}
