package com.ugold.common.util;

import com.alibaba.fastjson.JSONObject;
import com.ugold.compoment.exception.IntegrationException;
import com.ugold.compoment.model.Result;

import lombok.extern.slf4j.Slf4j;

/**
 * 处理Dubbo返回结果
 * 
 * @author dingjian
 * @date 2018年9月21日
 */
@Slf4j
public class DubboResultUtil {

	/**
	 * 处理Dubbo的返回结果，获取返回值，可能返回异常
	 * 
	 * @author dingjian
	 * @date 2018年9月21日
	 * @param result
	 * @return
	 */
	public static <T> T processDubboResult(Result<T> result) {
		if (result == null) {
			throw new IntegrationException("Dubbo接口返回NULL");
		}
		if (!result.isSuccess()) {
			throw new IntegrationException(result.getCode(), result.getMessage());
		}
		T data = result.getData();
		return data;
	}

	/**
	 * 处理Dubbo的返回结果，获取返回值，不返回异常，至多返回NULL
	 * 
	 * @author dingjian
	 * @date 2018年9月21日
	 * @param result
	 * @return
	 */
	public static <T> T processDubboResultWithoutException(Result<T> result) {
		if (result == null) {
			return null;
		}
		if (!result.isSuccess()) {
			log.warn("返回数据isSuccess=false，数据内容：{}", JSONObject.toJSONString(result));
		}
		T data = result.getData();
		return data;
	}
}
