package com.ugold.common.util.validate;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

/**
 * 用于封装数据校验、业务操作的结果
 * 
 * @author keby
 *
 */
@Data
public class ValidateResult {
	private boolean pass = false;

	/**
	 * 默认分隔符 ';'
	 */
	public final static String DEFAULT_SEPERATE = ";";
	// 封装消息
	private List<String> messages = new LinkedList<>();

	private ValidateResult(boolean pass) {
		this.pass = pass;
	}

	/**
	 * 获取通过的检验结果
	 * 
	 * @return
	 */
	public static ValidateResult failure() {
		return new ValidateResult(false);
	}

	/**
	 * 获取未通过的检验结果
	 * 
	 * @return
	 */
	public static ValidateResult success() {
		return new ValidateResult(true);
	}

	/**
	 * 增加错误消息
	 * 
	 * @param errorInfo
	 *            错误消息
	 */
	public void addMessage(String errorInfo) {
		if (errorInfo != null) {
			this.messages.add(errorInfo);
		}
	}

	/**
	 * 是否有消息内容
	 * 
	 * @return
	 */
	public boolean hasMessages() {
		return !this.hasNoMessages();
	}

	/**
	 * 是否有消息内容
	 * 
	 * @return
	 */
	public boolean hasNoMessages() {
		if (this.messages == null || this.messages.size() == 0) {
			return true;
		}
		return false;
	}

	/**
	 * 将错误信息内容按照指定符号拼接返回
	 * 
	 * @param separator
	 *            拼接的字符串
	 * @return
	 */
	public String getMessagesAsString(String separator) {
		if (messages.size() == 0) {
			return "";
		}
		if (separator != null) {
			return StringUtils.join(messages, separator);
		}
		return StringUtils.join(messages.toArray());
	}

	/**
	 * 将错误信息内容拼接并返回
	 * 
	 * @return
	 */
	public String getMessagesAsString() {
		if (messages == null || messages.size() == 0) {
			return "";
		}
		return StringUtils.join(messages.toArray());
	}
}
