package com.ugold.common.util.validate;

import java.util.regex.Pattern;

/**
 * 文字格式校验工具类
 * 
 * @author keby
 *
 */
public class DataFormatValidateUtil {

	/**
	 * CN手机号码校验(宽松校验,允许1开头的11位数字)
	 * 
	 * @return true(Is Mobile Number) or false(Not Is Mobile Number)
	 */
	public static boolean isMobileNumber(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^(\\+86)?1\\d{10}$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 邮箱有效性效验
	 * 
	 * @return true or false
	 */
	public static boolean isEmailAddress(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 校验金额是否符合规则(小数点前 ：Max :11位 Min:1位,小数点后：Max:6位 Min:0位)
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isAmount(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^[0-9]{1,11}+(.[0-9]{0,6})?$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 校验是否是固话(格式：区号-座机号 or 区号-座机号-分机号)
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isTelePhone(String value) {
		if (value == null) {
			return false;
		}
		// String regex = "^[0-9]{3,4}+-[0-9]{7}(|-[0-9]{1,4})$";
		String regex = "^(0[0-9]{2,3}\\-)?([1-9][0-9]{6,7})+(\\-[0-9]{1,4})?$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 校验字符串内容是否符合日期格式，<br />
	 * 支持两种校验格式 yyyy-MM-dd和yyyy/MM/dd
	 * 
	 * @param value
	 */
	public static boolean isDate(String value) {
		if (value == null) {
			return false;
		}
		String first = "^[1-9]\\d[0-9]{2}-([0-1]{1}+[0-9]{1}|[1-9])-([0-3]{1}+[0-9]{1}|[1-9])$";
		String second = "^[1-9]\\d[0-9]{2}/([0-1]{1}+[0-9]{1}|[1-9])/([0-3]{1}+[0-9]{1}|[1-9])$";
		if (!Pattern.compile(first).matcher(value).matches()) {
			return Pattern.compile(second).matcher(value).matches();
		}
		return Pattern.compile(first).matcher(value).matches();
	}

	/**
	 * 效验值是否为数字与小数点组成及小数点的精度
	 * 
	 * @param exchangeRate
	 *            汇率字符串
	 * @param accurate
	 *            精确的位数
	 */
	public static boolean isExchangeRate(String exchangeRate, Integer accurate) {
		if (exchangeRate == null) {
			return false;
		}
		String regex = "^[0-9]{1,11}+(.[0-9]{0," + accurate + "})?$";
		return Pattern.compile(regex).matcher(exchangeRate).matches();
	}

	/**
	 * 校验是否是整数，包括正整数和负整数
	 * 
	 * @param value
	 */
	public static boolean isInteger(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^[+-]?\\d+$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 校验是否为数值（正负均可，可带小数部分）
	 * 
	 * @author keby
	 * @param value
	 * @return
	 */
	public static boolean isNumber(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^[+-]?\\d+.?\\d*$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 校验是否为大于等于0的数值（可带小数部分）
	 * 
	 * @author keby
	 * @param value
	 * @return
	 */
	public static boolean isSignlessNumber(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^[+]?\\d+.?\\d*$";
		return Pattern.compile(regex).matcher(value).matches();
	}

	/**
	 * 校验是否是大于等于0的整数
	 * 
	 * @param value
	 */
	public static boolean isSignlessInteger(String value) {
		if (value == null) {
			return false;
		}
		String regex = "^[+]?\\d+$";
		return Pattern.compile(regex).matcher(value).matches();
	}
}
