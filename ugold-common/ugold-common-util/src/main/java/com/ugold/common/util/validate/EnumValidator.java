package com.ugold.common.util.validate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * 枚举值校验注解
 * 
 * @author xujuan
 * @Description:
 * @Date: create in 2019/4/18
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Constraint(validatedBy = EnumValidatorImpl.class)
public @interface EnumValidator {
	/**
	 * 用以校验的枚举类型
	 * 
	 * @author dingjian
	 * @return
	 */
	Class<?> enumTypeClass();

	/**
	 * 用以校验的属性名称
	 */
	String checkedField();

	String message() default "入参值不在正确枚举中";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
