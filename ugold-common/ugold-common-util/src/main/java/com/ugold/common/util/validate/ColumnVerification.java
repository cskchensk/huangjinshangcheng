package com.ugold.common.util.validate;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 用于校验实体类字段的业务字段校验描述，需要配合javax.persistence.Column使用，实体类需继承BaseEntity
 * 
 * @author dingjian
 * @date 2018年9月18日
 */
@Target(ElementType.FIELD)
@Retention(RUNTIME)
public @interface ColumnVerification {
	/**
	 * 字段的业务说明，默认描述值为"该项"
	 * 
	 * @return
	 */
	String description() default "该项";

	/**
	 * 用于校验的正则表达式
	 * 
	 * @author keby
	 * @return
	 */
	String regularExpression() default "";

	/**
	 * 当不符合正则表达式内容时，用于提示的内容
	 * 
	 * @author keby
	 * @return
	 */
	String regularTip() default "";

	/**
	 * 最小文本长度，默认为0
	 * 
	 * @author keby
	 * @return
	 */
	int minLength() default 0;
}
