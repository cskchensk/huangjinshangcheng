package com.ugold.common.util;

import com.ugold.compoment.exception.ErrorEnum;
import com.ugold.compoment.model.Result;

/**
 * Result结果转换为http接口响应对象
 * 
 * @author dingjian
 * @date 2018年9月21日
 */
public class HttpResponseUtil {

	/**
	 * 公共处理方法
	 * 
	 * @author dingjian
	 * @date 2018年9月21日
	 * @param result
	 * @return
	 */
	public static Result<?> executeResult(Result<?> result) {
		if (result == null) {
			return Result.buildFailureResult(ErrorEnum.ERROR_DEFAULT.getErrorCode(),
					ErrorEnum.ERROR_DEFAULT.getErrorMessage());
		}
		if (!result.isSuccess() || result.getData() == null) {
			return Result.buildFailureResult(result.getCode(), result.getMessage());
		}
		return Result.buildSuccessResult(result.getData());
	}
}
