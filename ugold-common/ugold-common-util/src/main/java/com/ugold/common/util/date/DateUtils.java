package com.ugold.common.util.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {
	// 日期格式
	public static final String DATE_SIMPLE_FORMAT_STRING = "yyyyMMdd";
	public static final String DATE_FORMAT_STRING = "yyyy-MM-dd";

	// 时间格式
	public static final String DATETIME_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";
	public static final String DATETIME_SIMPLE_FORMAT_STRING = "yyyyMMddHHmmss";
	public static final String DATETIME_SIMPLE_FORMAT = "yyyy年MM月dd HH:mm";
	public static final String DATETIME_FORMAT = "yyyy年MM月dd";

	/**
	 * 格式日期yyyy-MM-dd
	 *
	 * @param date
	 * @return
	 */
	public static String parseString(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT_STRING);
		return df.format(date);
	}

	/**
	 * 格式化简单日期yyyyMMdd
	 *
	 * @param date
	 * @return
	 */
	public static String parseSimleDate(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(DATE_SIMPLE_FORMAT_STRING);
		return df.format(date);
	}

	/**
	 * 格式化时间yyyy-MM-dd HH:mm:ss
	 *
	 * @param date
	 * @return
	 */
	public static String parseDateTime(Date date) {
		if (date == null) {
			return null;
		}

		SimpleDateFormat df = new SimpleDateFormat(DATETIME_FORMAT_STRING);
		return df.format(date);
	}

	/**
	 * 格式化时间yyyy年MM月dd
	 *
	 * @param date
	 * @return
	 */
	public static String parseSimpleDateTime(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(DATETIME_SIMPLE_FORMAT_STRING);
		return df.format(date);
	}

	/**
	 * 格式化时间yyyyMMddHHMMss
	 *
	 * @param date
	 * @return
	 */
	public static String parseSimpleDate(Date date) {
		if (date == null) {
			return null;
		}
		SimpleDateFormat df = new SimpleDateFormat(DATETIME_FORMAT);
		return df.format(date);
	}

	/**
	 * 格式化时间yyyy年MM月dd HH:mm
	 *
	 * @param date
	 * @return
	 */
	public static String parseDateTimeToStr(Date date) {
		if (date == null) {
			return null;
		}

		SimpleDateFormat df = new SimpleDateFormat(DATETIME_SIMPLE_FORMAT);
		return df.format(date);
	}

	/**
	 * 将yyyy-MM-dd字符串格式化成时间
	 *
	 * @param dt
	 * @return
	 */
	public static Date toDate(String dt) {
		if (dt == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_STRING);
		try {
			return format.parse(dt);
		} catch (Exception e) {
			throw new RuntimeException("格式化时间错误");
		}
	}

	/**
	 * 将yyyyMMddHHmmss字符串格式化成时间
	 *
	 * @param dt
	 * @return
	 */
	public static Date toSimpleDate(String dt) {
		if (dt == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT_STRING);
		try {
			return format.parse(dt);
		} catch (Exception e) {
			throw new RuntimeException("格式化时间错误");
		}
	}

	/**
	 * 将yyyy-MM-dd HH:mm:ss 字符串格式化成时间
	 *
	 * @param dt
	 * @return
	 */
	public static Date toDateTime(String dt) {
		if (dt == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT_STRING);
		try {
			return format.parse(dt);
		} catch (Exception e) {
			throw new RuntimeException("格式化时间错误");
		}
	}

	/**
	 * 将yyyyMMddHHmmss字符串格式化成时间
	 *
	 * @param dt
	 * @return
	 */
	public static Date toSimpleDateTime(String dt) {
		if (dt == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat(DATETIME_SIMPLE_FORMAT_STRING);
		try {
			return format.parse(dt);
		} catch (Exception e) {
			throw new RuntimeException("格式化时间错误");
		}
	}

	/**
	 * 获取当天的00:00:00
	 *
	 * @return
	 */
	public static Date getTodayStart() {
		return getDateStart(new Date());
	}

	/**
	 * 判断一个日期是否是今天
	 *
	 * @param date
	 * @return
	 */
	public static boolean isTodaytDay(Date date) {
		LocalDate today = LocalDate.now();
		LocalDate dateTime = dateToLocalDate(date);
		return today.equals(dateTime);
	}

	/**
	 * 02. java.util.date --> java.time.LocalDate
	 */
	public static LocalDate dateToLocalDate(Date date) {
		Instant instant = date.toInstant();
		ZoneId zone = ZoneId.systemDefault();
		LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
		return localDateTime.toLocalDate();
	}

	/**
	 * 获取当天的23:59:59
	 *
	 * @return
	 */
	public static Date getTodayEnd() {
		return getDateStart(new Date());
	}

	/**
	 * 获取指定日期的00:00:00
	 *
	 * @param date
	 * @return
	 */
	public static Date getDateStart(Date date) {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(date);

		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		return (Date) currentDate.getTime().clone();
	}

	/**
	 * 获取指定日期的23:59:59
	 *
	 * @return
	 */
	public static Date getDateEnd(Date date) {
		Calendar currentDate = new GregorianCalendar();
		currentDate.setTime(date);

		currentDate.set(Calendar.HOUR_OF_DAY, 23);
		currentDate.set(Calendar.MINUTE, 59);
		currentDate.set(Calendar.SECOND, 59);
		return (Date) currentDate.getTime().clone();

	}

	/**
	 * 日期计算增加天数
	 *
	 * @param beginDate
	 * @param days
	 * @return
	 */
	public static Date addDay(Date beginDate, int days) {
		Date date = beginDate;
		if (days != 0) {
			try {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.add(Calendar.DATE, days);
				date = calendar.getTime();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	/**
	 * 日期计算增加月数
	 *
	 * @param beginDate
	 * @param months
	 * @return
	 */
	public static Date addMonth(Date beginDate, int months) {
		Date date = beginDate;
		if (months != 0) {
			try {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.add(Calendar.MONTH, months);
				date = calendar.getTime();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	/**
	 * 日期计算增加秒数
	 *
	 * @param beginDate
	 * @param seconds
	 * @return
	 */
	public static Date addSecond(Date beginDate, int seconds) {
		Date date = beginDate;
		if (seconds != 0) {
			try {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.add(Calendar.SECOND, seconds);
				date = calendar.getTime();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return date;
	}

	public static String getCurrentMonthFirstDay() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MONTH, 0);
		c.set(Calendar.DAY_OF_MONTH, 1);// 设置为1号,当前日期既为本月第一天
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
		String firstTime = format.format(c.getTime());
		return firstTime;
	}

	public static String getCurrentMonthLastDay() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
		String lastTime = format.format(c.getTime());
		return lastTime;
	}

	public static String getCurrentYearFirstDay() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-01-01 00:00:00");
		String firstTime = format.format(new Date());
		return firstTime;
	}

	public static String getCurrentYearLastDay() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-12-31 23:59:59");
		String lastTime = format.format(new Date());
		return lastTime;
	}

	public static String getTody() {
		SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT_STRING);
		return format.format(new Date());
	}


    public static String getNowDate() {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_STRING);
        return format.format(new Date());
    }

    public static Date parseString(String source) {
        if (source == null) return null;

		SimpleDateFormat format = new SimpleDateFormat(DATETIME_FORMAT_STRING);
		try {
			return format.parse(source);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 计算两个日期之间相差的天数
	 *
	 * @param smdate 较小的时间
	 * @param bdate  较大的时间
	 * @return 相差天数
	 * @throws ParseException
	 */
	public static int daysBetween(Date smdate, Date bdate) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		long between_days = 0;
		try {
			smdate = sdf.parse(sdf.format(smdate));
			bdate = sdf.parse(sdf.format(bdate));
			Calendar cal = Calendar.getInstance();
			cal.setTime(smdate);
			long time1 = cal.getTimeInMillis();
			cal.setTime(bdate);
			long time2 = cal.getTimeInMillis();
			between_days = (time2 - time1) / (1000 * 3600 * 24);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Integer.parseInt(String.valueOf(between_days));
	}

	/**
	 * 通过时间秒毫秒数判断两个时间的间隔
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int differentDaysByMillisecond(Date date1,Date date2)
	{
		int days = (int) ((date2.getTime() - date1.getTime()) / (1000*3600*24));
		return days;
	}

	/**
	 * 获取两个时间的月份差
	 * @Param
	 * @Return
	 */
	public static Integer getDifMonth(Date startDate, Date endDate){
		Calendar start = Calendar.getInstance();
		Calendar end = Calendar.getInstance();
		start.setTime(startDate);
		end.setTime(endDate);
		int months = end.get(Calendar.MONTH) - start.get(Calendar.MONTH);
		int years = end.get(Calendar.YEAR) - start.get(Calendar.YEAR);
		return Math.abs(months + years*12);
	}

	/**
	 * 使用eaysexcel时里面的日期转换问题 解析时间 43458.234189814815 解析秒数的例子
	 */
	public static LocalTime getTime(double d) {
		// 全部转成秒
		int sec = (int) (d * 86400);
		// 时
		int s = sec / 3600;
		// 分
		int h = sec % 3600 / 60;
		// 秒
		int m = sec % 3600 % 60;

		String ss = s + "";
		String hh = h + "";
		String mm = m + "";

		if (s < 10) {
			ss = "0" + ss;
		}
		if (h < 10) {
			hh = "0" + hh;

		}
		if (m < 10) {
			mm = "0" + mm;
		}
		LocalTime time = LocalTime.parse(ss + ":" + hh + ":" + mm);
		return time;
	}
}
