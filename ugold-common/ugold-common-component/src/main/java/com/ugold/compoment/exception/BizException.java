package com.ugold.compoment.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务异常
 * 
 * @author dingjian
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BizException extends RuntimeException {
	private static final long serialVersionUID = -9126784768037895261L;
	private int errorCode = 0;
	private String errorMessage;

	public BizException() {
	}

	public BizException(String errorMessage) {
		this.errorCode = ErrorEnum.ERROR_DEFAULT.getErrorCode();
		this.errorMessage = errorMessage;
	}

	public BizException(ErrorEnum error) {
		this.errorCode = error.getErrorCode();
		this.errorMessage = error.getErrorMessage();
	}

	public BizException(ErrorEnum error, String errorMessage) {
		this.errorCode = error.getErrorCode();
		this.errorMessage = errorMessage;
	}

	public BizException(int errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}
}
