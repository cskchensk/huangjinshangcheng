package com.ugold.compoment.model;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IdReq extends BaseReq {

	private static final long serialVersionUID = 9013928774115377084L;

	@NotNull(message = "id不能为空")
	private Long id;
}
