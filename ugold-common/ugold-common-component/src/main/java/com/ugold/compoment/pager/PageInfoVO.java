package com.ugold.compoment.pager;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 分页用
 * 
 * @author dingjian
 *
 * @param <T>
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PageInfoVO<T> implements Serializable {

	private static final long serialVersionUID = 8659898739763069177L;

	/**
	 * 分页数据
	 */
	private List<T> list;

	/**
	 * 页码
	 */
	private Integer pageNo;

	/**
	 * 每页大小
	 */
	private Integer pageSize;

	/**
	 * 总页数
	 */
	private Integer totalPage;

	/**
	 * 总条数
	 */
	private Integer totalCount;

	/**
	 * 	设置分页参数
	 * @author dingjian
	 * @date 2019年7月10日上午10:25:06
	 *	@param pageNo 页数
	 *	@param pageSize 每页大小
	 *	@param totalCount 总记录数
	 */
	public void paging(Integer pageNo, Integer pageSize, Integer totalCount) {
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.totalCount = totalCount;
		if (pageSize > 0) {
			this.totalPage = totalCount % pageSize == 0 ? totalCount / pageSize : totalCount / pageSize + 1;
		}
	}

}