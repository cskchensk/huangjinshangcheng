package com.ugold.compoment.pager;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分页用(jquery.dataTables插件)
 * 
 * @author dingjian
 *
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class DataTablesResponse implements Serializable {

	private static final long serialVersionUID = 7695467581783619288L;
	private Integer draw;
	private Long recordsTotal;
	private Long recordsFiltered;
	private List<?> data = Collections.EMPTY_LIST;

	private DataTablesResponse() {

	}

	private DataTablesResponse(Integer draw, Long totalCount, List<?> data) {
		this.draw = draw;
		this.recordsTotal = this.recordsFiltered = totalCount;
		this.data = data;
	}

	/**
	 * 格式化分页数据, 使符合jQueryDataTables 1.10的要求
	 * 
	 * @param draw 绘制表格的次数
	 * @param page SpringDataJpa的分页数据对象
	 * @return
	 */
	public static DataTablesResponse format(Integer draw, Long totalCount, List<?> data) {
		return new DataTablesResponse(draw, totalCount, data);
	}
//	private Integer draw;
//	private Long iTotalDisplayRecords;
//	private Long iTotalRecords;
//	private List<?> data = Collections.EMPTY_LIST;
//
//	private DataTablesResponse() {
//
//	}
//
//	private DataTablesResponse(Integer draw, Long iTotalRecords, List<?> data) {
//		this.draw = draw;
//		this.iTotalDisplayRecords = this.iTotalRecords = iTotalRecords;
//		this.data = data;
//	}
//
//	/**
//	 * 格式化分页数据, 使符合jQueryDataTables 1.10的要求
//	 * 
//	 * @param draw
//	 *            绘制表格的次数
//	 * @param page
//	 *            SpringDataJpa的分页数据对象
//	 * @return
//	 */
//	public static DataTablesResponse format(Integer draw, Long iTotalRecords, List<?> data) {
//		return new DataTablesResponse(draw, iTotalRecords, data);
//	}
}