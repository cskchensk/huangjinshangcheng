package com.ugold.compoment.model;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 返回结果基础类型
 * 
 * @author dingjian
 *
 * @param <T>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 2102865241133691258L;
	private static final int SUCCESS_CODE = 0;
	/**
	 * 封装数据结果
	 */
	private T data;
	/**
	 * 接口返回错误码
	 */
	private int code;

	/**
	 * 接口是否异常
	 */
	private boolean success = false;

	/**
	 * 接口调用错误信息
	 */
	private String message;

	/**
	 * 是否调用成功
	 */
	public boolean isSuccess() {
		return success;
	}

	/**
	 * 成功结果
	 * 
	 * @author dingjian
	 * @return
	 */
	public static <T> Result<T> buildSuccessResult() {
		Result<T> result = new Result<>();
		result.code = SUCCESS_CODE;
		result.success = true;
		return result;
	}

	/**
	 * 成功结果
	 * 
	 * @author dingjian
	 * @param t
	 * @return
	 */
	public static <T> Result<T> buildSuccessResult(T t) {
		Result<T> result = new Result<>();
		result.code = SUCCESS_CODE;
		result.success = true;
		result.setData(t);
		return result;
	}

	/**
	 * 失败结果
	 * 
	 * @author dingjian
	 * @param code 响应编码
	 * @return
	 */
	public static <T> Result<T> buildFailureResult(int code) {
		Result<T> result = new Result<>();
		result.code = code;
		result.success = false;
		return result;
	}

	/**
	 * 失败结果
	 * 
	 * @author dingjian
	 * @param msg  响应消息
	 * @param code 响应编码
	 * @return
	 */
	public static <T> Result<T> buildFailureResult(int code, String msg) {
		Result<T> result = new Result<>();
		result.code = code;
		result.message = msg;
		result.success = false;
		return result;
	}

}
