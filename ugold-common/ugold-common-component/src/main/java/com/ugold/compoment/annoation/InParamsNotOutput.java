package com.ugold.compoment.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 请求入参切面不要输出标记
 * 
 * @author dingjian
 * @date 2019/04/24 10:22:49
 */
@Target(value = { ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface InParamsNotOutput {

}
