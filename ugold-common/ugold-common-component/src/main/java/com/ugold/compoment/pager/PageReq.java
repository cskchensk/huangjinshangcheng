package com.ugold.compoment.pager;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.ugold.compoment.model.BaseReq;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Web或Dubbo请求分页基础类
 * 
 * @author dingjian
 * @date 2019/04/22 17:39:20
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PageReq extends BaseReq {
	private static final long serialVersionUID = -4096697923043000988L;

	/**
	 * 当前页码
	 */
	@NotNull(message = "页码不能为空")
	@Min(value = 1, message = "页码最小值为1")
	private Integer pageNo = 1;

	/**
	 * 每页大小限制(默认10条)
	 */
	@NotNull(message = "分页大小不能为空")
	@Min(value = 1, message = "分页最小值为1")
	private Integer pageSize = 10;
}
