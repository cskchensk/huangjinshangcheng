package com.ugold.compoment.exception;

/**
 * 错误码<br />
 * 错误码分内外两种 对内使用最细粒度错误码，对外使用统一错误码
 * 
 * @author dingjian
 * @date 2018年9月14日
 */
public enum ErrorEnum {
	// 1开头为外不可以接收细节错误码
	ERROR_DEFAULT(10000, 10000, "系统异常"), 
	ERROR_SERVICE(10000, 10001, "业务异常"),
	ERROR_NO_SERVER(10000, 10002, "服务不可用"),
	ERROR_BUSINESS_LOGIC(10000, 10003, "业务逻辑错误"),
	
	ERROR_DB(10000, 10010, "DB操作异常"),
	ERROR_DB_DATA_NOT_EXISTS(10000, 10011, "DB数据不存在异常"),

	ERROR_PARAM(20000, 20001, "入参校验不通过"),

	ERROR_NO_AUTHORITY(30000, 30001, "无权限"),

	;

	private final int errorCode;
	private final int parentCode;
	private final String errorMessage;

	ErrorEnum(int parentCode, int errorCode, String errorMessage) {
		this.parentCode = parentCode;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public int getParentCode() {
		return parentCode;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public static ErrorEnum getErrorByCode(int code) {
		for (ErrorEnum errorEnum : values()) {
			if (errorEnum.getErrorCode() == code) {
				return errorEnum;
			}
		}
		return null;
	}
}
