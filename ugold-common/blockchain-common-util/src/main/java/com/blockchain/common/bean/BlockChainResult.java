package com.blockchain.common.bean;

import cn.bubi.access.adaptation.blockchain.bc.response.TransactionHistory;
import cn.bubi.access.utils.blockchain.BlockchainKeyPair;
import cn.bubi.sdk.core.transaction.model.TransactionCommittedResult;
import lombok.Data;

/**
 *@Author zhangweijie
 *@Date 2019/10/14 0014
 *@time 下午 13:36
 **/
@Data
public class BlockChainResult {

    private BlockchainKeyPair blockchainKeyPair;

    private TransactionCommittedResult transactionCommittedResult;

    private TransactionHistory transactionHistory;
}
