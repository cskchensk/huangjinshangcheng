package com.blockchain.common.bean;

import cn.bubi.sdk.core.transaction.model.Signature;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author Wangyz
 * @DISCRIPTION 创建链上用户参数
 * @Create 2018/12/12 0012 上午 11:19
 */
@Data
public class BuildAccount {

    private Map<String, String> metaDatas;
    private String meta;
    private String script;
    private String signer;
    /**
     * 用户类型 1-普通用户，2-合约用户 ，3-联名用户
     */
    private int type;

//    private String pubSign;

    private List<Signature> signatures;

}
