package cn.ug.product.bean.request;

import java.io.Serializable;

/**
 *@Author zhangweijie
 *@Date 2019/9/10 0010
 *@time 下午 13:35
 **/
public class ProductDiscountBean  implements Serializable{
    private static final long serialVersionUID = -1974944449823182355L;

    /**
     *   新用户初始立减次数
     * @mbg.generated
     */
    private Integer newMemberNum;

    /**
     *   老用户初始立减次数
     * @mbg.generated
     */
    private Integer oldMemberNum;

    /**
     *   立减次数上限
     * @mbg.generated
     */
    private Integer maxNum;

    /**
     *   邀请人数和立减次数比例
     * @mbg.generated
     */
    private Integer ratio;


    public Integer getNewMemberNum() {
        return newMemberNum;
    }

    public void setNewMemberNum(Integer newMemberNum) {
        this.newMemberNum = newMemberNum;
    }

    public Integer getOldMemberNum() {
        return oldMemberNum;
    }

    public void setOldMemberNum(Integer oldMemberNum) {
        this.oldMemberNum = oldMemberNum;
    }

    public Integer getMaxNum() {
        return maxNum;
    }

    public void setMaxNum(Integer maxNum) {
        this.maxNum = maxNum;
    }

    public Integer getRatio() {
        return ratio;
    }

    public void setRatio(Integer ratio) {
        this.ratio = ratio;
    }
}
