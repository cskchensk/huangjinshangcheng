package cn.ug.product.bean.response;/**
 * Create by zhangweijie on 2019/9/11 0011 下午 14:15
 */

/**
 * @Author zhangweijie
 * @Date 2019/9/11 0011
 * @time 下午 14:15
 **/
public class ProductSubtractBean {

    /**
     * 会员id
     *
     * @mbg.generated
     */
    private String memberId;

    /**
     * 立减次数
     *
     * @mbg.generated
     */
    private Integer num;

    /**
     * 邀请人数
     *
     * @mbg.generated
     */
    private Integer inviteNum;

    /**
     * 最高立减次数
     */
    private Integer maxDiscountNum;

    /**
     * 立减金额
     */
    private Integer marketPrice;


    /**
     *   邀请人数和立减次数比例
     */
    private Integer ratio;

    /**
     *   新用户初始立减次数
     */
    private Integer newMemberNum;

    /**
     *   老用户初始立减次数
     * @mbg.generated
     */
    private Integer oldMemberNum;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getInviteNum() {
        return inviteNum;
    }

    public void setInviteNum(Integer inviteNum) {
        this.inviteNum = inviteNum;
    }

    public Integer getMaxDiscountNum() {
        return maxDiscountNum;
    }

    public void setMaxDiscountNum(Integer maxDiscountNum) {
        this.maxDiscountNum = maxDiscountNum;
    }

    public Integer getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Integer marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getRatio() {
        return ratio;
    }

    public void setRatio(Integer ratio) {
        this.ratio = ratio;
    }

    public Integer getNewMemberNum() {
        return newMemberNum;
    }

    public void setNewMemberNum(Integer newMemberNum) {
        this.newMemberNum = newMemberNum;
    }

    public Integer getOldMemberNum() {
        return oldMemberNum;
    }

    public void setOldMemberNum(Integer oldMemberNum) {
        this.oldMemberNum = oldMemberNum;
    }
}
