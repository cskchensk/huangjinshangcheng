package cn.ug.product.bean.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.List;

/**
 * 热门产品列表
 * @author ywl
 * @date 2018/1/24
 */
public class ProductHotBean {

    /**  产品id  **/
    private String id;
    /** 产品名称  **/
    private String name;
    /** 产品类型 1:新手活动 2:活动产品  3:活期产品 4:定期产品 6.实物金 7.体验金 8.安稳金**/
    private Integer type;
    /** 活动类型 1:是活动 2:不是活动 **/
    private String isActivity;
    /** 年华收益 **/
    private BigDecimal yearsIncome;
    /** 金价类型设置  1:实时金价 2:自定义价格 **/
    private Integer settingPriceType;
    /** 自定义黄金价格 **/
    private BigDecimal settingPrice;
    /** 图片 **/
    private String img;
    /** 产品标签(包含两个标签，用逗号隔开) **/
    private String label;
    /** 收益标签 **/
    private String incomeLabel;
    /*投资期限*/
    private int investDay;
    /** 募集克数 **/
    private BigDecimal raiseGram;
    /** 已募集克重 **/
    private BigDecimal toRaiseGram;
    /** 募集百分比 **/
    private int percent;

    /**产品市场价**/
    private BigDecimal marketPrice;
    /**
     * 库存
     */
    private Integer stock;

    /**产品售价**/
    private BigDecimal sellingPrice;
    /**
     * 产品主标题
     */
    private String heading;

    /**
     * 产品主标题注释
     */
    private String headingNotes;
    /**
     * 副标题
     */
    private String subhead;

    /**
     * 产品规格
     */
    private Integer specification;

    /**图文详情**/
    private List<String> imgDetails;

    /**
     * 回租期限
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LeaseDay leaseDay;

    private int leaseCloseType;

    public String getId() {
        return id;
    }

    public int getInvestDay() {
        return investDay;
    }

    public int getLeaseCloseType() {
        return leaseCloseType;
    }

    public void setLeaseCloseType(int leaseCloseType) {
        this.leaseCloseType = leaseCloseType;
    }

    public void setInvestDay(int investDay) {
        this.investDay = investDay;
    }

    public BigDecimal getRaiseGram() {
        return raiseGram;
    }

    public void setRaiseGram(BigDecimal raiseGram) {
        this.raiseGram = raiseGram;
    }

    public BigDecimal getToRaiseGram() {
        return toRaiseGram;
    }

    public void setToRaiseGram(BigDecimal toRaiseGram) {
        this.toRaiseGram = toRaiseGram;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSettingPriceType() {
        return settingPriceType;
    }

    public void setSettingPriceType(Integer settingPriceType) {
        this.settingPriceType = settingPriceType;
    }

    public BigDecimal getSettingPrice() {
        return settingPrice;
    }

    public void setSettingPrice(BigDecimal settingPrice) {
        this.settingPrice = settingPrice;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIncomeLabel() {
        return incomeLabel;
    }

    public void setIncomeLabel(String incomeLabel) {
        this.incomeLabel = incomeLabel;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public String getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(String isActivity) {
        this.isActivity = isActivity;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public String getSubhead() {
        return subhead;
    }

    public void setSubhead(String subhead) {
        this.subhead = subhead;
    }

    public Integer getSpecification() {
        return specification;
    }

    public void setSpecification(Integer specification) {
        this.specification = specification;
    }

    public List<String> getImgDetails() {
        return imgDetails;
    }

    public void setImgDetails(List<String> imgDetails) {
        this.imgDetails = imgDetails;
    }

    public LeaseDay getLeaseDay() {
        return leaseDay;
    }

    public void setLeaseDay(LeaseDay leaseDay) {
        this.leaseDay = leaseDay;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getHeadingNotes() {
        return headingNotes;
    }

    public void setHeadingNotes(String headingNotes) {
        this.headingNotes = headingNotes;
    }
}
