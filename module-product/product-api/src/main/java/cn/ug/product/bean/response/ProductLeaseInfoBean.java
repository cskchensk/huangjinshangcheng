package cn.ug.product.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductLeaseInfoBean implements Serializable {

    private String productId;

    private Integer leaseDay;

    private BigDecimal yearsIncome;

    private int id;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getLeaseDay() {
        return leaseDay;
    }

    public void setLeaseDay(Integer leaseDay) {
        this.leaseDay = leaseDay;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
