package cn.ug.product.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 后台管理
 * @author ywl
 * @date 2018/1/12 0012
 */
public class ProductParamBean extends Page implements Serializable {

    /** 产品名称 **/
    private String name;
    /** 产品类型 **/
    private String type;
    /** 投资期限 **/
    private String investDay;
    /** 产品状态  1:正常 2:作废 **/
    private Integer status;
    /** 审核状态 **/
    private Integer auditStatus;
    /** 审核人id **/
    private String auditUserId;
    /** 审核人 **/
    private String auditUser;
    /** 发布用户Id **/
    private String createUserId;
    /** 创建用户**/
    private String createUser;
    /** 年华收益最小值 **/
    private BigDecimal yearsIncomeMin;
    /** 年华收益最大值 **/
    private BigDecimal yearsIncomeMax;
    /** 创建起始时间 **/
    private String startTime;
    /** 创建结束时间 **/
    private String endTime;
    /** 上线起始时间 **/
    private String upStartTime;
    /** 上线结束时间 **/
    private String upEndTime;
    /** 起始产品结束时间 **/
    private String startStopTime;
    /** 结束产品结束时间 **/
    private String endStopTime;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    //---------------------内部参数-----------------------
    private Integer queryAudit;

    private Integer queryProduct;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public BigDecimal getYearsIncomeMin() {
        return yearsIncomeMin;
    }

    public void setYearsIncomeMin(BigDecimal yearsIncomeMin) {
        this.yearsIncomeMin = yearsIncomeMin;
    }

    public BigDecimal getYearsIncomeMax() {
        return yearsIncomeMax;
    }

    public void setYearsIncomeMax(BigDecimal yearsIncomeMax) {
        this.yearsIncomeMax = yearsIncomeMax;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUpStartTime() {
        return upStartTime;
    }

    public void setUpStartTime(String upStartTime) {
        this.upStartTime = upStartTime;
    }

    public String getUpEndTime() {
        return upEndTime;
    }

    public void setUpEndTime(String upEndTime) {
        this.upEndTime = upEndTime;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getQueryAudit() {
        return queryAudit;
    }

    public void setQueryAudit(Integer queryAudit) {
        this.queryAudit = queryAudit;
    }

    public Integer getQueryProduct() {
        return queryProduct;
    }

    public void setQueryProduct(Integer queryProduct) {
        this.queryProduct = queryProduct;
    }

    public String getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(String auditUserId) {
        this.auditUserId = auditUserId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartStopTime() {
        return startStopTime;
    }

    public void setStartStopTime(String startStopTime) {
        this.startStopTime = startStopTime;
    }

    public String getEndStopTime() {
        return endStopTime;
    }

    public void setEndStopTime(String endStopTime) {
        this.endStopTime = endStopTime;
    }
}
