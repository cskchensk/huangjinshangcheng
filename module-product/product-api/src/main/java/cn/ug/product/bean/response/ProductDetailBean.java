package cn.ug.product.bean.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.List;

/**
 *  移动端产品明细
 * @author ywl
 * @date 2018/1/25
 */
public class ProductDetailBean {
    private String id;
    /** 产品名称 **/
    private String name;
    /** 产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品 6.实物金 7.体验金 8.安稳金**/
    private Integer type;
    /** 年华收益 **/
    private BigDecimal yearsIncome;
    /** 投资期限 **/
    private String investDay;
    /** 计息日**/
    private Integer interestAccrualDay;
    /** 金价类型设置  1:实时金价 2:自定义价格 **/
    private Integer settingPriceType;
    /** 自定义黄金价格 **/
    private BigDecimal settingPrice;
    /** 最低价格 **/
    private BigDecimal priceMin;
    /** 最高价格 **/
    private BigDecimal priceMax;
    /** 最低克数 **/
    private BigDecimal gramMin;
    /** 最高克数 **/
    private BigDecimal gramMax;
    /** 产品标签(包含两个标签，用逗号隔开) **/
    private String label;
    /** 收益标签 **/
    private String incomeLabel;
    /** 产品描述 **/
    private String detail;
    /** 保障 **/
    private String insurance;
    /** 协议id **/
    private String protocolId;
    /** 协议名称 **/
    private String protocolName;
    /** 协议内容 **/
    private String protocolContent;
    /** 募集克数 **/
    private BigDecimal raiseGram;
    /** 已募集克重 **/
    private BigDecimal toRaiseGram;
    /** 募集百分比 **/
    private int percent;
    /** 产品详情 老版本**/
    //private List<ProductInfo> productInfoBeanList;
    /** 产品详情 新版本**/
    private List<ProductInfoBean> productInfoBeanNewList;
    /**产品售价**/
    private BigDecimal sellingPrice;
    /**产品库存**/
    private BigDecimal repertoryGram;
    /**产品市场价**/
    private BigDecimal marketPrice;
    /**
     * 产品副标题
     */
    private String subhead;

    /** 头图图片 **/
    private String img;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String specification;

    /**图文详情**/
    private List<String> imgDetails;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 商品标签
     */
    private List<LabelBean> labelList;

    /**
     * 商店名称
     */
    private String storeName = "柚子黄金杭州旗舰店";
    /**
     * 商店描述
     */
    private String storeExplain = "互联网黄金O2O领先品牌";

    /** 加工费 **/
    private BigDecimal processingFee;

    /**
     * 产品回租期限
     */
    private List<LeaseDay>  leaseDayList;

    /**
     * 产品宣传信息
     */
    List<ProductPublicityBean> productPublicityBeans;

    /**
     * 最高立减次数
     */
    private Integer maxDiscountNum;

    public BigDecimal getRaiseGram() {
        return raiseGram;
    }

    public void setRaiseGram(BigDecimal raiseGram) {
        this.raiseGram = raiseGram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getInterestAccrualDay() {
        return interestAccrualDay;
    }

    public void setInterestAccrualDay(Integer interestAccrualDay) {
        this.interestAccrualDay = interestAccrualDay;
    }

    public Integer getSettingPriceType() {
        return settingPriceType;
    }

    public void setSettingPriceType(Integer settingPriceType) {
        this.settingPriceType = settingPriceType;
    }

    public BigDecimal getSettingPrice() {
        return settingPrice;
    }

    public void setSettingPrice(BigDecimal settingPrice) {
        this.settingPrice = settingPrice;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public BigDecimal getGramMin() {
        return gramMin;
    }

    public void setGramMin(BigDecimal gramMin) {
        this.gramMin = gramMin;
    }

    public BigDecimal getGramMax() {
        return gramMax;
    }

    public void setGramMax(BigDecimal gramMax) {
        this.gramMax = gramMax;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(String protocolId) {
        this.protocolId = protocolId;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    public String getProtocolContent() {
        return protocolContent;
    }

    public void setProtocolContent(String protocolContent) {
        this.protocolContent = protocolContent;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(BigDecimal sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public BigDecimal getRepertoryGram() {
        return repertoryGram;
    }

    public void setRepertoryGram(BigDecimal repertoryGram) {
        this.repertoryGram = repertoryGram;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getSubhead() {
        return subhead;
    }

    public void setSubhead(String subhead) {
        this.subhead = subhead;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public List<LabelBean> getLabelList() {
        return labelList;
    }

    public void setLabelList(List<LabelBean> labelList) {
        this.labelList = labelList;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreExplain() {
        return storeExplain;
    }

    public void setStoreExplain(String storeExplain) {
        this.storeExplain = storeExplain;
    }

    public BigDecimal getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(BigDecimal processingFee) {
        this.processingFee = processingFee;
    }

    public List<String> getImgDetails() {
        return imgDetails;
    }

    public void setImgDetails(List<String> imgDetails) {
        this.imgDetails = imgDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public BigDecimal getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(BigDecimal priceMin) {
        this.priceMin = priceMin;
    }

    public BigDecimal getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(BigDecimal priceMax) {
        this.priceMax = priceMax;
    }

    public String getIncomeLabel() {
        return incomeLabel;
    }

    public void setIncomeLabel(String incomeLabel) {
        this.incomeLabel = incomeLabel;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public BigDecimal getToRaiseGram() {
        return toRaiseGram;
    }

    public void setToRaiseGram(BigDecimal toRaiseGram) {
        this.toRaiseGram = toRaiseGram;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }


    public List<ProductInfoBean> getProductInfoBeanNewList() {
        return productInfoBeanNewList;
    }

    public void setProductInfoBeanNewList(List<ProductInfoBean> productInfoBeanNewList) {
        this.productInfoBeanNewList = productInfoBeanNewList;
    }

    public List<LeaseDay> getLeaseDayList() {
        return leaseDayList;
    }

    public void setLeaseDayList(List<LeaseDay> leaseDayList) {
        this.leaseDayList = leaseDayList;
    }

    public List<ProductPublicityBean> getProductPublicityBeans() {
        return productPublicityBeans;
    }

    public void setProductPublicityBeans(List<ProductPublicityBean> productPublicityBeans) {
        this.productPublicityBeans = productPublicityBeans;
    }

    public Integer getMaxDiscountNum() {
        return maxDiscountNum;
    }

    public void setMaxDiscountNum(Integer maxDiscountNum) {
        this.maxDiscountNum = maxDiscountNum;
    }
}
