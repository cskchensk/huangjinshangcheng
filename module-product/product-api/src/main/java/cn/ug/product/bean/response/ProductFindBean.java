package cn.ug.product.bean.response;

import cn.ug.bean.base.BaseBean;
import cn.ug.product.bean.request.ProductDiscountBean;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/** 产品详情(后台)
 * @author ywl
 * @date 2018/1/19
 */
public class ProductFindBean  extends BaseBean implements Serializable {

    /** 产品名称 **/
    private String name;
    /** 产品类型 1:新手活动 2:活动产品  3:活期产品 4:定期产品  6:实物金 7.体验金 8.安稳金**/
    private int type;
    /** 活动类型 1:是活动 2:不是活动 **/
    private int isActivity;
    /** 有效时间 **/
    private String effectiveDay;
    /** 协议类型 **/
    private String protocolId;
    /** 协议名称 **/
    private String protocolName;
    /** 金价类型设置  1:实时金价 2:自定义价格 **/
    private int settingPriceType;
    /** 自定义黄金价格 **/
    private BigDecimal settingPrice;
    /** 年华收益 **/
    private BigDecimal yearsIncome;
    /** 计息方式 1:按天计息 2:到期计息 **/
    private int interestAccrualType;
    /** 投资期限 **/
    private String investDay;
    /** 最低价格 **/
    private BigDecimal priceMin;
    /** 最高价格 **/
    private BigDecimal priceMax;
    /** 最低克数 **/
    private BigDecimal gramMin;
    /** 最高克数 **/
    private BigDecimal gramMax;
    /** 募集克数 **/
    private BigDecimal raiseGram;
    /** 已募集克重 **/
    private BigDecimal toRaiseGram;
    /** 计息日**/
    private int interestAccrualDay;
    /** 还款方式 **/
    private String repaymentWay;
    /** 结算方式 1.按照克重结算 2:按照现金结算 **/
    private int settlementType;
    /** 成标方式 **/
    private String standardWay;
    /** 风险类型 1:保本型 2:非保本型 **/
    private int riskType;
    /** 产品描述 **/
    private String detail;
    /** 图片 **/
    private String img;
    /** 产品标签(包含两个标签，用逗号隔开) **/
    private String label;
    /** 收益标签 **/
    private String incomeLabel;
    /** 保障  可以废弃**/
    //private String insurance;
    /** 上架状态 1:上架 2:下架 **/
    private int shelfState;
    /**  审核状态 1:待审核 2:已通过 3:已拒绝 **/
    private int auditStatus;
    /** 审核用户姓名 **/
    private String auditUser;
    /** 审核描述 **/
    private String auditDescription;
    /** 审核时间 **/
    private String auditTimeString;
    /** 上线时间 **/
    private String upTimeString;
    /** 还款方式名称  可以废弃**/
    private String repaymentWayName;
    /** 成标方式名称  可以废弃**/
    private String standardWayName;
    private String stopTimeString;
    //是否开启交易 1:是 2:否
    private Integer isOpen;
    /** 产品明细 **/
    //private List<ProductInfo> productInfoBeanList;
    /**
     * 产品详情集合
      */
    private List<ProductInfoBean> productInfoBeanNewList;


    /**#####################new#########################*******/
    /**产品市场价**/
    private BigDecimal marketPrice;
    /**
     * 可购买用户，实物金
     * 1平台全部用户，
     * 2复投用户，
     * 3首投用户
     *
     * 体验金
     * 1.不限
     * 2.已交易用户
     * 3.未交易用户
     */
    private Integer canBuyUserType;

    /**
     * 用户限购
     * 0不限购买次数，
     * 1限购购买数量
     * 2限制最高购买克重
     */
    private Integer userAstrictStatus;

    /**
     * 用户限购数量
     */
    private Integer userAstrictNum;

    /**
     * 限制用户最高购买克重
     */
    private Integer userAstrictGram;

    /**
     * 奖励到账日期
     */
    private Integer awardToaccountDay = 1;

    /**
     * 首次回租奖励设置
     * 0不设置奖励
     */
    private Integer awardSetting;

    /**
     * 产品主标题
     */
    private String heading;

    /**
     * 产品主标题注释
     */
    private String headingNotes;

    /**
     * 产品副标题
     */
    private String subhead;

    /**
     * 产品规格
     */
    private Integer specification;

    /**图文详情**/
    private List<String> imgDetails;

    /**
     * 加工模板id
     */
    private Integer processId;

    /**
     * 加工模板名称
     */
    private Integer processName;

    /**
     * 回租奖励结算方式 1以现金结算 2金豆结算
     */
    private Integer leaseCloseType;

    /**
     * 回租奖励计算金价 1.回租前一日收盘价  2.回租前一日某个时间点金价
     */
    private Integer leaseCalculateGoldType;

    /**
     * 回租前一日时间点 时间精确到分 如12:00
     */
    private String leaseCalculateGoldTime;

    /**
     * 实物金货号
     */
    private String entityItemno;

    /**
     * 实物金回租协议id
     */
    private String leaseProtocolId;

    /** 实物金回租协议名称 **/
    private String leaseProtocolName;
    /**
     * 产品回租期限
     */
    private List<LeaseDay>  leaseDayList;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 商品标签
     */
    private List<LabelBean> labelList;

    /**
     * 首次购买优惠 0无优惠
     */
    private Integer buyAwardSetting = 0;

    /**
     * 产品宣传信息
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<ProductPublicityBean> productPublicityBeans;

    /**
     * 折扣金产品
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ProductDiscountBean discountBean;

    public BigDecimal getToRaiseGram() {
        return toRaiseGram;
    }

    public String getStopTimeString() {
        return stopTimeString;
    }

    public void setStopTimeString(String stopTimeString) {
        this.stopTimeString = stopTimeString;
    }

    public int getShelfState() {
        return shelfState;
    }

    public void setShelfState(int shelfState) {
        this.shelfState = shelfState;
    }

    public void setToRaiseGram(BigDecimal toRaiseGram) {
        this.toRaiseGram = toRaiseGram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getEffectiveDay() {
        return effectiveDay;
    }

    public void setEffectiveDay(String effectiveDay) {
        this.effectiveDay = effectiveDay;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(String protocolId) {
        this.protocolId = protocolId;
    }

    public int getSettingPriceType() {
        return settingPriceType;
    }

    public void setSettingPriceType(int settingPriceType) {
        this.settingPriceType = settingPriceType;
    }

    public BigDecimal getSettingPrice() {
        return settingPrice;
    }

    public void setSettingPrice(BigDecimal settingPrice) {
        this.settingPrice = settingPrice;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public int getInterestAccrualType() {
        return interestAccrualType;
    }

    public void setInterestAccrualType(int interestAccrualType) {
        this.interestAccrualType = interestAccrualType;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public BigDecimal getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(BigDecimal priceMin) {
        this.priceMin = priceMin;
    }

    public BigDecimal getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(BigDecimal priceMax) {
        this.priceMax = priceMax;
    }

    public BigDecimal getGramMin() {
        return gramMin;
    }

    public void setGramMin(BigDecimal gramMin) {
        this.gramMin = gramMin;
    }

    public BigDecimal getGramMax() {
        return gramMax;
    }

    public void setGramMax(BigDecimal gramMax) {
        this.gramMax = gramMax;
    }

    public int getInterestAccrualDay() {
        return interestAccrualDay;
    }

    public void setInterestAccrualDay(int interestAccrualDay) {
        this.interestAccrualDay = interestAccrualDay;
    }

    public int getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(int settlementType) {
        this.settlementType = settlementType;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(int auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getAuditDescription() {
        return auditDescription;
    }

    public void setAuditDescription(String auditDescription) {
        this.auditDescription = auditDescription;
    }

    public String getAuditTimeString() {
        return auditTimeString;
    }

    public void setAuditTimeString(String auditTimeString) {
        this.auditTimeString = auditTimeString;
    }

    public String getUpTimeString() {
        return upTimeString;
    }

    public void setUpTimeString(String upTimeString) {
        this.upTimeString = upTimeString;
    }

    public String getProtocolName() {
        return protocolName;
    }

    public void setProtocolName(String protocolName) {
        this.protocolName = protocolName;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIncomeLabel() {
        return incomeLabel;
    }

    public void setIncomeLabel(String incomeLabel) {
        this.incomeLabel = incomeLabel;
    }

    public BigDecimal getRaiseGram() {
        return raiseGram;
    }

    public void setRaiseGram(BigDecimal raiseGram) {
        this.raiseGram = raiseGram;
    }

    public int getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(int isActivity) {
        this.isActivity = isActivity;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public List<ProductInfoBean> getProductInfoBeanNewList() {
        return productInfoBeanNewList;
    }

    public void setProductInfoBeanNewList(List<ProductInfoBean> productInfoBeanNewList) {
        this.productInfoBeanNewList = productInfoBeanNewList;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getCanBuyUserType() {
        return canBuyUserType;
    }

    public void setCanBuyUserType(Integer canBuyUserType) {
        this.canBuyUserType = canBuyUserType;
    }

    public Integer getUserAstrictStatus() {
        return userAstrictStatus;
    }

    public void setUserAstrictStatus(Integer userAstrictStatus) {
        this.userAstrictStatus = userAstrictStatus;
    }

    public Integer getUserAstrictNum() {
        return userAstrictNum;
    }

    public void setUserAstrictNum(Integer userAstrictNum) {
        this.userAstrictNum = userAstrictNum;
    }

    public Integer getUserAstrictGram() {
        return userAstrictGram;
    }

    public void setUserAstrictGram(Integer userAstrictGram) {
        this.userAstrictGram = userAstrictGram;
    }

    public Integer getAwardToaccountDay() {
        return awardToaccountDay;
    }

    public void setAwardToaccountDay(Integer awardToaccountDay) {
        this.awardToaccountDay = awardToaccountDay;
    }

    public Integer getAwardSetting() {
        return awardSetting;
    }

    public void setAwardSetting(Integer awardSetting) {
        this.awardSetting = awardSetting;
    }

    public String getSubhead() {
        return subhead;
    }

    public void setSubhead(String subhead) {
        this.subhead = subhead;
    }

    public Integer getSpecification() {
        return specification;
    }

    public void setSpecification(Integer specification) {
        this.specification = specification;
    }

    public List<String> getImgDetails() {
        return imgDetails;
    }

    public void setImgDetails(List<String> imgDetails) {
        this.imgDetails = imgDetails;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public Integer getLeaseCloseType() {
        return leaseCloseType;
    }

    public void setLeaseCloseType(Integer leaseCloseType) {
        this.leaseCloseType = leaseCloseType;
    }

    public Integer getLeaseCalculateGoldType() {
        return leaseCalculateGoldType;
    }

    public void setLeaseCalculateGoldType(Integer leaseCalculateGoldType) {
        this.leaseCalculateGoldType = leaseCalculateGoldType;
    }

    public String getLeaseCalculateGoldTime() {
        return leaseCalculateGoldTime;
    }

    public void setLeaseCalculateGoldTime(String leaseCalculateGoldTime) {
        this.leaseCalculateGoldTime = leaseCalculateGoldTime;
    }

    public String getEntityItemno() {
        return entityItemno;
    }

    public void setEntityItemno(String entityItemno) {
        this.entityItemno = entityItemno;
    }

    public String getLeaseProtocolId() {
        return leaseProtocolId;
    }

    public void setLeaseProtocolId(String leaseProtocolId) {
        this.leaseProtocolId = leaseProtocolId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public List<LeaseDay> getLeaseDayList() {
        return leaseDayList;
    }

    public void setLeaseDayList(List<LeaseDay> leaseDayList) {
        this.leaseDayList = leaseDayList;
    }

    public Integer getProcessName() {
        return processName;
    }

    public void setProcessName(Integer processName) {
        this.processName = processName;
    }

    public String getLeaseProtocolName() {
        return leaseProtocolName;
    }

    public void setLeaseProtocolName(String leaseProtocolName) {
        this.leaseProtocolName = leaseProtocolName;
    }

    public List<LabelBean> getLabelList() {
        return labelList;
    }

    public void setLabelList(List<LabelBean> labelList) {
        this.labelList = labelList;
    }

    public Integer getBuyAwardSetting() {
        return buyAwardSetting;
    }

    public void setBuyAwardSetting(Integer buyAwardSetting) {
        this.buyAwardSetting = buyAwardSetting;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public List<ProductPublicityBean> getProductPublicityBeans() {
        return productPublicityBeans;
    }

    public void setProductPublicityBeans(List<ProductPublicityBean> productPublicityBeans) {
        this.productPublicityBeans = productPublicityBeans;
    }

    public String getRepaymentWay() {
        return repaymentWay;
    }

    public void setRepaymentWay(String repaymentWay) {
        this.repaymentWay = repaymentWay;
    }

    public String getStandardWay() {
        return standardWay;
    }

    public void setStandardWay(String standardWay) {
        this.standardWay = standardWay;
    }

    public int getRiskType() {
        return riskType;
    }

    public void setRiskType(int riskType) {
        this.riskType = riskType;
    }

    public String getRepaymentWayName() {
        return repaymentWayName;
    }

    public void setRepaymentWayName(String repaymentWayName) {
        this.repaymentWayName = repaymentWayName;
    }

    public String getStandardWayName() {
        return standardWayName;
    }

    public void setStandardWayName(String standardWayName) {
        this.standardWayName = standardWayName;
    }

    public ProductDiscountBean getDiscountBean() {
        return discountBean;
    }

    public void setDiscountBean(ProductDiscountBean discountBean) {
        this.discountBean = discountBean;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getHeadingNotes() {
        return headingNotes;
    }

    public void setHeadingNotes(String headingNotes) {
        this.headingNotes = headingNotes;
    }
}
