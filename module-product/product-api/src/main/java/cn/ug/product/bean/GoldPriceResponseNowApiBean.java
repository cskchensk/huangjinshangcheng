package cn.ug.product.bean;

import java.util.Map;

/**
 * 金价查询返回值
 * https://www.nowapi.com
 * @author kaiwotech
 */
public class GoldPriceResponseNowApiBean implements java.io.Serializable {

	private String success;
	private String msgid;
	private String msg;
	private Map<String, String> result;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public Map<String, String> getResult() {
		return result;
	}

	public void setResult(Map<String, String> result) {
		this.result = result;
	}

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
