package cn.ug.product.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class GoldLatestPriceBean implements Serializable {
    private String time;
    private BigDecimal latestpri;
    private BigDecimal maxPrice;
    private BigDecimal minPrice;
    private BigDecimal openingPrice;
    private BigDecimal closingPrice;

    public GoldLatestPriceBean() {

    }

    public BigDecimal getOpeningPrice() {
        return openingPrice;
    }

    public void setOpeningPrice(BigDecimal openingPrice) {
        this.openingPrice = openingPrice;
    }

    public BigDecimal getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(BigDecimal closingPrice) {
        this.closingPrice = closingPrice;
    }

    public String getTime() {
        return time;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getLatestpri() {
        return latestpri;
    }

    public void setLatestpri(BigDecimal latestpri) {
        this.latestpri = latestpri;
    }
}
