package cn.ug.product.bean.request;

import java.io.Serializable;

/**
 * 产品审核
 * @author ywl
 * @date 2018/1/18
 */
public class ProductAuditParamBean implements Serializable {

    /** 产品id **/
    private String id;
    /** 审核状态 1:待审核 2:已通过 3:已拒绝 **/
    private Integer auditStatus;
    /** 审核用户Id **/
    private String auditUserId;
    /** 审核用户姓名 **/
    private String auditUser;
    /** 审核备注 **/
    private String auditDescription;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(String auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getAuditDescription() {
        return auditDescription;
    }

    public void setAuditDescription(String auditDescription) {
        this.auditDescription = auditDescription;
    }
}
