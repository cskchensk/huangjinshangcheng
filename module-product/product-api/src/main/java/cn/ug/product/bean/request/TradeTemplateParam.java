package cn.ug.product.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

/**
 * 交易时间模版
 * @author  ywl
 * @date 2018-04-30
 */
public class TradeTemplateParam extends Page implements Serializable{

    /** 是否默认 1:是 2:否 **/
    private Integer isDefault;
    /** 类型 1:买入 2:卖出 **/
    private Integer type;

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
