package cn.ug.product.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

public class LeaseDay implements Serializable {
    private int leaseDay;
    private BigDecimal yearsIncome;
    private int id;
    private BigDecimal incomeAmount;
    private int incomeType;

    public int getLeaseDay() {
        return leaseDay;
    }

    public void setLeaseDay(int leaseDay) {
        this.leaseDay = leaseDay;
    }

    public int getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(int incomeType) {
        this.incomeType = incomeType;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }
}
