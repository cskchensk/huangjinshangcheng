package cn.ug.product.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.product.bean.response.ProductBean;
import cn.ug.product.bean.response.ProductFindBean;
import cn.ug.product.bean.response.ProductSubtractBean;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * 产品对外API
 *
 * @author ywl
 */
@RequestMapping("product")
public interface ProductServiceApi {

    @RequestMapping(value = "updateStock", method = POST)
    public SerializeObject updateStock(@RequestParam("productId") String productId,
                                       @RequestParam("stock") Integer stock);

    /**
     * 根据ID查找信息(后台)
     *
     * @param id ID
     * @return 记录集
     */
    @RequestMapping(value = "queryProductById", method = GET)
    SerializeObject<ProductFindBean> queryProductById(@RequestParam("id") String id);

    /**
     * 查询是否可以买(后台)
     *
     * @param productId 产品id
     * @return 记录集
     */
    @RequestMapping(value = "findIsBuy", method = GET)
    SerializeObject findIsBuy(@RequestParam("productId") String productId);

    /**
     * 查询是否可以卖(后台)
     *
     * @return 记录集
     */
    @RequestMapping(value = "findIsSell", method = GET)
    SerializeObject findIsSell();

    /**
     * 购买产品后增加已募集克数
     *
     * @param productId
     * @param gram
     * @return
     */
    @RequestMapping(value = "addToRaiseGram", method = PUT)
    SerializeObject addToRaiseGram(@RequestParam("productId") String productId, @RequestParam("gram") BigDecimal gram);

    @RequestMapping(value = "shelf/state", method = POST)
    SerializeObject updateShelfState(@RequestParam("id") String id, @RequestParam("shelfState") int shelfState);

    /**
     * 查询活动任务产品
     *
     * @return 记录集
     */
    @RequestMapping(value = "findActivityTaskProduct", method = GET)
    SerializeObject<ProductFindBean> findActivityTaskProduct();

    /**
     * 查询在售产品-根据类型
     *
     * @param type
     * @return
     */
    @RequestMapping(value = "find/online/list", method = GET)
    SerializeObject<List<ProductBean>> queryOnlineList(@RequestParam("type")Integer type);


    /**
     * 保存折扣金在售期间邀请记录
     *
     * @param friendId
     * @param memberId
     * @return
     */
    @RequestMapping(value = "save/invite/Record", method = POST)
    SerializeObject saveProductInviteRecord(@RequestParam("friendId") String friendId, @RequestParam("memberId") String memberId);

    /**
     * 查询会员购买的产品立减次数
     *
     * @param memberId
     * @param productId
     * @return
     */
    @RequestMapping(value = "queryMemberIdSubtractNum", method = GET)
    SerializeObject<ProductSubtractBean> queryMemberIdSubtractNum(@RequestParam("memberId") String memberId, @RequestParam("productId") String productId);


    /**
     * 会员立减次数修改
     *
     * @param memberId
     * @param productId
     * @param type      操作类型： 1减  2加
     * @return
     */
    @RequestMapping(value = "member/subtract/num/update", method = POST)
    SerializeObject memberSubtractNumUpdate(@RequestParam("memberId") String memberId, @RequestParam("productId") String productId, @RequestParam("type") Integer type);

}
