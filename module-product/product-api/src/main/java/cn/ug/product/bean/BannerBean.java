package cn.ug.product.bean;

import cn.ug.bean.base.BaseBean;

/**
 * Banner
 * @author kaiwotech
 */
public class BannerBean extends BaseBean implements java.io.Serializable {

	/** 标题 */
	private String title;
	/** 副标题 */
	private String subTitle;
	/** 图片 */
	private String image;
	/** 备注 */
	private String description;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
