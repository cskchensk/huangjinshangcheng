package cn.ug.product.bean.response;

import java.io.Serializable;
import java.util.List;

public class ProductInfoBean implements Serializable {

    private String title;

    private List<ProductInfo> productIntroduceList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<ProductInfo> getProductIntroduceList() {
        return productIntroduceList;
    }

    public void setProductIntroduceList(List<ProductInfo> productIntroduceList) {
        this.productIntroduceList = productIntroduceList;
    }
}
