package cn.ug.product.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 后台管理返回
 * @author ywl
 * @date 2018/1/18
 */
public class ProductManageBean extends BaseBean implements Serializable {

    /** 产品名称 **/
    private String name;
    /** 产品类型 1:新手活动 2:活动产品  3:活期产品 4:定期产品 **/
    private Integer type;
    /** 投资期限 **/
    private String investDay;
    /** 年华收益 **/
    private BigDecimal yearsIncome;
    /** 募集克数 **/
    private BigDecimal raiseGram;
    /** 已募集克数 **/
    private BigDecimal toRaiseGram;
    /** 产品状态  1:正常 2:作废 **/
    private Integer status;
    /** 上架状态 1:上架 2:下架  **/
    private Integer shelfState;
    /**  审核状态 1:待审核 2:已通过 3:已拒绝 **/
    private Integer auditStatus;
    /** 热销产品 1:否 2:是 **/
    private Integer isHot;
    /** 是否开启交易 1:是 2:否 **/
    private Integer isOpen;
    /** 创建用户姓名 **/
    private String createUser;
    /** 审核用户姓名 **/
    private String auditUser;
    /** 审核描述 **/
    private String auditDescription;
    /** 产品结束时间 **/
    private String stopTimeString;
    /** 上线时间 **/
    private String upTimeString;
    /** 审核时间 **/
    private String auditTimeString;

    //---------------------以下是外部参数----------------------------/
    /**  剩余募集克数 **/
    private BigDecimal surplusGram;
    /** 已募集百分比 **/
    private BigDecimal raisedPer;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 实物金货号
     */
    private String entityItemno;

    /**
     * 产品规格
     */
    private Integer specification;

    private LeaseDay leaseDay;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getShelfState() {
        return shelfState;
    }

    public void setShelfState(Integer shelfState) {
        this.shelfState = shelfState;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getUpTimeString() {
        return upTimeString;
    }

    public void setUpTimeString(String upTimeString) {
        this.upTimeString = upTimeString;
    }

    public String getAuditTimeString() {
        return auditTimeString;
    }

    public void setAuditTimeString(String auditTimeString) {
        this.auditTimeString = auditTimeString;
    }

    public String getAuditDescription() {
        return auditDescription;
    }

    public void setAuditDescription(String auditDescription) {
        this.auditDescription = auditDescription;
    }

    public String getStopTimeString() {
        return stopTimeString;
    }

    public void setStopTimeString(String stopTimeString) {
        this.stopTimeString = stopTimeString;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public BigDecimal getRaiseGram() {
        return raiseGram;
    }

    public void setRaiseGram(BigDecimal raiseGram) {
        this.raiseGram = raiseGram;
    }

    public BigDecimal getToRaiseGram() {
        return toRaiseGram;
    }

    public void setToRaiseGram(BigDecimal toRaiseGram) {
        this.toRaiseGram = toRaiseGram;
    }

    public BigDecimal getSurplusGram() {
        return surplusGram;
    }

    public void setSurplusGram(BigDecimal surplusGram) {
        this.surplusGram = surplusGram;
    }

    public BigDecimal getRaisedPer() {
        return raisedPer;
    }

    public void setRaisedPer(BigDecimal raisedPer) {
        this.raisedPer = raisedPer;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getEntityItemno() {
        return entityItemno;
    }

    public void setEntityItemno(String entityItemno) {
        this.entityItemno = entityItemno;
    }

    public Integer getSpecification() {
        return specification;
    }

    public void setSpecification(Integer specification) {
        this.specification = specification;
    }

    public LeaseDay getLeaseDay() {
        return leaseDay;
    }

    public void setLeaseDay(LeaseDay leaseDay) {
        this.leaseDay = leaseDay;
    }
}
