package cn.ug.product.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.util.List;

/**
 * 后台管理返回
 * @author ywl
 * @date 2018/1/18
 */
public class TradeTemplateBean extends BaseBean implements Serializable {

    /** 模版名称 **/
    private String name;
    /** 类型 1:买入 2:卖出 **/
    private Integer type;
    /** 是否默认模版 1:是 2:否 **/
    private Integer isDefault;
    /** 时间集合 **/
    private List<TradeTemplateTimeBean> list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public List<TradeTemplateTimeBean> getList() {
        return list;
    }

    public void setList(List<TradeTemplateTimeBean> list) {
        this.list = list;
    }
}
