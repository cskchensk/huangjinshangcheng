package cn.ug.product.bean.response;

import java.io.Serializable;

/**
 * @Author zhangweijie
 * @Date 2019/8/27 0027
 * @time 下午 16:23
 **/
public class ProductPublicityBean implements Serializable {


    private static final long serialVersionUID = -5631434673000969153L;

    private int id;

    private String productId;
    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 图片地址
     */
    private String imgUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
