package cn.ug.product.bean.response;


import java.io.Serializable;

/**
 * 模版时间
 * @author ywl
 * @date 2018/1/18
 */
public class TradeTemplateTimeBean  implements Serializable {

    /** 交易起始时间 **/
    private String startTime;
    /** 交易结束时间 **/
    private String endTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
