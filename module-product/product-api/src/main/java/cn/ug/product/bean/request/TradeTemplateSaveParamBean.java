package cn.ug.product.bean.request;

/**
 * 新增、修改保存交易时间模版实体
 * @author  ywl
 * @date 2018-04-30
 */
public class TradeTemplateSaveParamBean {

    /** id **/
    private String id;
    /** 模版名称 **/
    private String name;
    /** 类型 1:买入 2:卖出 **/
    private Integer type;
    /** 是否默认模版 1:是 2:否 **/
    private Integer isDefault;
    /** 交易起始时间 **/
    private String startTime [] ;
    /** 交易结束时间 **/
    private String endTime [];

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }

    public String[] getStartTime() {
        return startTime;
    }

    public void setStartTime(String[] startTime) {
        this.startTime = startTime;
    }

    public String[] getEndTime() {
        return endTime;
    }

    public void setEndTime(String[] endTime) {
        this.endTime = endTime;
    }
}
