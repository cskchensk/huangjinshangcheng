package cn.ug.product.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * 产品交易时间
 * @author  ywl
 * @date 2018-04-30
 */
public class ProductTradeTimeBean extends BaseBean implements Serializable {

    /** 日期 **/
    private String day;
    /** 产品id **/
    private String productId;
    /** 模版id **/
    private String templateId;
    /** 描述日期 **/
    private String detail;
    /** 模版名称 **/
    private String name;
    /** 类型 **/
    private Integer type;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
