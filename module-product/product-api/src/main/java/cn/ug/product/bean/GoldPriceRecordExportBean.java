package cn.ug.product.bean;

/**
 * 金价导出
 */
public class GoldPriceRecordExportBean extends GoldPriceRecordBean {

    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
