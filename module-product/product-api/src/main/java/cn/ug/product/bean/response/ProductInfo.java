package cn.ug.product.bean.response;

import java.io.Serializable;

public class ProductInfo implements Serializable{

    /** id **/
    private String id;
    /** 产品id **/
    private String productId;
    /** key名称 **/
    private String name;
    /** value值 **/
    private String content;
    /** 排序 **/
    private Integer sort;

    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
