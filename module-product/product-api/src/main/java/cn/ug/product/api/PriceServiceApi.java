package cn.ug.product.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.product.bean.GoldLatestPriceBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 价格相关服务
 * @author kaiwotech
 */
@RequestMapping("price")
public interface PriceServiceApi {

    /**
     * 获取实时金价
     * @return  实时金价
     */
    @RequestMapping(value = "currentGoldPrice", method = GET)
    SerializeObject currentGoldPrice();

    @RequestMapping(value = "/someday", method = GET)
    SerializeObject<Double> getSomedayPrice(@RequestParam("someday")String someday);

    @RequestMapping(value = "/everyday", method = GET)
    SerializeObject<List<GoldLatestPriceBean>> listLatestPrice(@RequestParam("startDate")String startDate,
                                                               @RequestParam("endDate")String endDate);
}
