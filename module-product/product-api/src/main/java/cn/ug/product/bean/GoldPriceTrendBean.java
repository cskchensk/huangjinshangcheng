package cn.ug.product.bean;

import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 黄金交易价格走势
 * @author kaiwotech
 */
public class GoldPriceTrendBean extends BaseBean implements java.io.Serializable {

	/** 类型 1:上海黄金交易所 */
	private Integer type;
	/** 最新价 */
	private BigDecimal latestpri;

	/**数据源/实际 价格**/
	private BigDecimal actualpri;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getLatestpri() {
		return latestpri;
	}

	public void setLatestpri(BigDecimal latestpri) {
		this.latestpri = latestpri;
	}

	public BigDecimal getActualpri() {
		return actualpri;
	}

	public void setActualpri(BigDecimal actualpri) {
		this.actualpri = actualpri;
	}
}
