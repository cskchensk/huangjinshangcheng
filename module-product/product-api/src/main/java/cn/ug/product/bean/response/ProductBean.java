package cn.ug.product.bean.response;

import cn.ug.bean.base.BaseBean;
import cn.ug.product.bean.request.ProductDiscountBean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author ywl
 * @date 2018/1/18
 */
public class ProductBean  extends BaseBean implements Serializable {

    /** 产品名称 **/
    private String name;
    /** 产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品 6:实物金  7:体验金 8:安稳金 9:折扣金**/
    private Integer type;
    /** 活动类型 1:是活动 2:不是活动 **/
    private Integer isActivity;
    /** 上线时间 **/
   /* private LocalDateTime upTime;*/
    /** 有效时间 **/
    private String effectiveDay;
    /** 年华收益  可以废弃**/
    private BigDecimal yearsIncome;
    /** 投资期限  可以废弃**/
    private String investDay;
    /** 计息方式 1:按天计息 2:到期计息 **/
    private Integer interestAccrualType;
    /** 计息日**/
    private Integer interestAccrualDay;
    /** 风险类型 1:保本型 2:非保本型 **/
    //private Integer riskType;
    /** 结算方式 1.按照克重结算 2:按照现金结算 **/
    private Integer settlementType;
    /** 金价类型设置  1:实时金价 2:自定义价格 **/
    private Integer settingPriceType;
    /** 自定义黄金价格 **/
    private BigDecimal settingPrice;
    /** 最低价格 **/
    private BigDecimal priceMin;
    /** 最高价格 **/
    private BigDecimal priceMax;
    /** 最低克数 **/
    private BigDecimal gramMin;
    /** 最高克数 **/
    private BigDecimal gramMax;
    /** 募集克数 可以废弃**/
    private BigDecimal raiseGram;
    /** 产品状态  1:正常 2:作废 **/
    private Integer status;
    /** 上架状态 1:上架 2:下架  **/
    private Integer shelfState;
    /**  审核状态 1:待审核 2:已通过 3:已拒绝 **/
    private Integer auditStatus;
    /** 热销产品 1:否 2:是 **/
    private Integer isHot;
    /** 还款方式  可以废弃**/
    //private String repaymentWay;
    /** 成标方式  可以废弃**/
    //private String standardWay;
    /** 产品描述 **/
    private String detail;
    /** 图片 **/
    private String img;
    /** 产品标签(包含两个标签，用逗号隔开) **/
    private String label;
    /** 收益标签 **/
    private String incomeLabel;
    /** 保障 **/
    private String insurance;
    /** 购买协议id **/
    private String protocolId;
    /** 产品结束时间 **/
   /* private LocalDateTime stopTime;*/
    /** 创建用户Id **/
    private String createUserId;
    /** 创建用户姓名 **/
    private String createUser;
    /** 审核用户Id **/
    private String auditUserId;
    /** 审核用户姓名 **/
    private String auditUser;
    /** 审核描述 **/
    private String auditDscription;
    /** 审核时间 **/
    private String auditTimeString;
    /** 上线时间 **/
    private String upTimeString;
    /** 结束时间 **/
    private String stopTimeString;
    /** 限制交易时间段集合 **/
    private List<ProductTradeTimeBean> list;
    /** 产品详情集合 **/
    private List<ProductInfoBean> productInfoList;

    /**#####################new#########################*******/
    /**产品市场价**/
    private BigDecimal marketPrice;
    /**
     * 可购买用户
     * 1平台全部用户，
     * 2复投用户，
     * 3首投用户
     */
    private Integer canBuyUserType;

    /**
     * 用户限购
     * 0不限购买次数，
     * 1限购购买数量
     * 2限制最高购买克重
     */
    private Integer userAstrictStatus;

    /**
     * 用户限购数量
     */
    private Integer userAstrictNum;

    /**
     * 限制用户最高购买克重
     */
    private Integer userAstrictGram;

    /**
     * 奖励到账日期
     */
    private Integer awardToaccountDay = 1;

    /**
     * 首次回租奖励设置
     * 0不设置奖励
     */
    private Integer awardSetting = 0;
    /**
     * 产品主标题
     */
    private String heading;

    /**
     * 产品主标题注释
     */
    private String headingNotes;
    /**
     * 产品副标题
     */
    private String[] subhead;

    /**
     * 产品规格
     */
    private Integer specification;

    /** 头图图片 **/
    //private List<String> headImgList;

    /**图文详情**/
    private String[] imgDetails;

    /**
     * 加工模板id
     */
    private Integer processId;

    /**
     * 回租奖励结算方式 1以现金结算 2金豆结算
     */
    private Integer leaseCloseType;

    /**
     * 回租奖励计算金价 1.回租前一日收盘价  2.回租前一日某个时间点金价
     */
    private Integer leaseCalculateGoldType;

    /**
     * 回租前一日时间点 时间精确到分 如12:00
     */
    private String leaseCalculateGoldTime;

    /**
     * 实物金货号
     */
    private String entityItemno;

    /**
     * 实物金回租协议id
     */
    private String leaseProtocolId;

    /**
     * 产品回租期限
     */
    private List<LeaseDay>  leaseDayList;
    /**
     * 库存
     */
    private Integer stock;

    /**
     * 首次购买优惠
     */
    private Integer buyAwardSetting;

    /**
     * 产品宣传信息
     */
    private List<ProductPublicityBean> productPublicityBeans;

    /**
     * 折扣金产品
     */
    private ProductDiscountBean discountBean;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getEffectiveDay() {
        return effectiveDay;
    }

    public void setEffectiveDay(String effectiveDay) {
        this.effectiveDay = effectiveDay;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public Integer getInterestAccrualType() {
        return interestAccrualType;
    }

    public void setInterestAccrualType(Integer interestAccrualType) {
        this.interestAccrualType = interestAccrualType;
    }

    public Integer getInterestAccrualDay() {
        return interestAccrualDay;
    }

    public void setInterestAccrualDay(Integer interestAccrualDay) {
        this.interestAccrualDay = interestAccrualDay;
    }


    public Integer getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(Integer settlementType) {
        this.settlementType = settlementType;
    }

    public Integer getSettingPriceType() {
        return settingPriceType;
    }

    public void setSettingPriceType(Integer settingPriceType) {
        this.settingPriceType = settingPriceType;
    }

    public BigDecimal getSettingPrice() {
        return settingPrice;
    }

    public void setSettingPrice(BigDecimal settingPrice) {
        this.settingPrice = settingPrice;
    }

    public BigDecimal getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(BigDecimal priceMin) {
        this.priceMin = priceMin;
    }

    public BigDecimal getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(BigDecimal priceMax) {
        this.priceMax = priceMax;
    }

    public BigDecimal getGramMin() {
        return gramMin;
    }

    public void setGramMin(BigDecimal gramMin) {
        this.gramMin = gramMin;
    }

    public BigDecimal getGramMax() {
        return gramMax;
    }

    public void setGramMax(BigDecimal gramMax) {
        this.gramMax = gramMax;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getShelfState() {
        return shelfState;
    }

    public void setShelfState(Integer shelfState) {
        this.shelfState = shelfState;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIncomeLabel() {
        return incomeLabel;
    }

    public void setIncomeLabel(String incomeLabel) {
        this.incomeLabel = incomeLabel;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(String protocolId) {
        this.protocolId = protocolId;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(String auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getAuditDscription() {
        return auditDscription;
    }

    public void setAuditDscription(String auditDscription) {
        this.auditDscription = auditDscription;
    }

    public String getAuditTimeString() {
        return auditTimeString;
    }

    public void setAuditTimeString(String auditTimeString) {
        this.auditTimeString = auditTimeString;
    }

    public String getUpTimeString() {
        return upTimeString;
    }

    public void setUpTimeString(String upTimeString) {
        this.upTimeString = upTimeString;
    }

    public BigDecimal getRaiseGram() {
        return raiseGram;
    }

    public void setRaiseGram(BigDecimal raiseGram) {
        this.raiseGram = raiseGram;
    }

    public List<ProductTradeTimeBean> getList() {
        return list;
    }

    public void setList(List<ProductTradeTimeBean> list) {
        this.list = list;
    }

    public List<ProductInfoBean> getProductInfoList() {
        return productInfoList;
    }

    public void setProductInfoList(List<ProductInfoBean> productInfoList) {
        this.productInfoList = productInfoList;
    }

    public Integer getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(Integer isActivity) {
        this.isActivity = isActivity;
    }

    public String getStopTimeString() {
        return stopTimeString;
    }

    public void setStopTimeString(String stopTimeString) {
        this.stopTimeString = stopTimeString;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getCanBuyUserType() {
        return canBuyUserType;
    }

    public void setCanBuyUserType(Integer canBuyUserType) {
        this.canBuyUserType = canBuyUserType;
    }

    public Integer getUserAstrictStatus() {
        return userAstrictStatus;
    }

    public void setUserAstrictStatus(Integer userAstrictStatus) {
        this.userAstrictStatus = userAstrictStatus;
    }

    public Integer getUserAstrictNum() {
        return userAstrictNum;
    }

    public void setUserAstrictNum(Integer userAstrictNum) {
        this.userAstrictNum = userAstrictNum;
    }

    public Integer getUserAstrictGram() {
        return userAstrictGram;
    }

    public void setUserAstrictGram(Integer userAstrictGram) {
        this.userAstrictGram = userAstrictGram;
    }

    public Integer getAwardToaccountDay() {
        return awardToaccountDay;
    }

    public void setAwardToaccountDay(Integer awardToaccountDay) {
        this.awardToaccountDay = awardToaccountDay;
    }

    public Integer getAwardSetting() {
        return awardSetting;
    }

    public void setAwardSetting(Integer awardSetting) {
        this.awardSetting = awardSetting;
    }

    public String[] getSubhead() {
        return subhead;
    }

    public void setSubhead(String[] subhead) {
        this.subhead = subhead;
    }

    public Integer getSpecification() {
        return specification;
    }

    public void setSpecification(Integer specification) {
        this.specification = specification;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public Integer getLeaseCloseType() {
        return leaseCloseType;
    }

    public void setLeaseCloseType(Integer leaseCloseType) {
        this.leaseCloseType = leaseCloseType;
    }

    public Integer getLeaseCalculateGoldType() {
        return leaseCalculateGoldType;
    }

    public void setLeaseCalculateGoldType(Integer leaseCalculateGoldType) {
        this.leaseCalculateGoldType = leaseCalculateGoldType;
    }

    public String getLeaseCalculateGoldTime() {
        return leaseCalculateGoldTime;
    }

    public void setLeaseCalculateGoldTime(String leaseCalculateGoldTime) {
        this.leaseCalculateGoldTime = leaseCalculateGoldTime;
    }

    public String getEntityItemno() {
        return entityItemno;
    }

    public void setEntityItemno(String entityItemno) {
        this.entityItemno = entityItemno;
    }

    public String getLeaseProtocolId() {
        return leaseProtocolId;
    }

    public void setLeaseProtocolId(String leaseProtocolId) {
        this.leaseProtocolId = leaseProtocolId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }


    public Integer getBuyAwardSetting() {
        return buyAwardSetting;
    }

    public void setBuyAwardSetting(Integer buyAwardSetting) {
        this.buyAwardSetting = buyAwardSetting;
    }

    public List<LeaseDay> getLeaseDayList() {
        return leaseDayList;
    }

    public void setLeaseDayList(List<LeaseDay> leaseDayList) {
        this.leaseDayList = leaseDayList;
    }

    public String[] getImgDetails() {
        return imgDetails;
    }

    public void setImgDetails(String[] imgDetails) {
        this.imgDetails = imgDetails;
    }

    public List<ProductPublicityBean> getProductPublicityBeans() {
        return productPublicityBeans;
    }

    public void setProductPublicityBeans(List<ProductPublicityBean> productPublicityBeans) {
        this.productPublicityBeans = productPublicityBeans;
    }

    public ProductDiscountBean getDiscountBean() {
        return discountBean;
    }

    public void setDiscountBean(ProductDiscountBean discountBean) {
        this.discountBean = discountBean;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getHeadingNotes() {
        return headingNotes;
    }

    public void setHeadingNotes(String headingNotes) {
        this.headingNotes = headingNotes;
    }
}
