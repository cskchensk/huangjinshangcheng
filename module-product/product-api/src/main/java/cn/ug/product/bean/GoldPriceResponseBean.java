package cn.ug.product.bean;

import java.util.List;
import java.util.Map;

/**
 * 金价查询返回值
 * @author kaiwotech
 */
public class GoldPriceResponseBean implements java.io.Serializable {

	private String resultcode;
	private String reason;
	private List<Map<String, Map<String, String>>> result;

	public String getResultcode() {
		return resultcode;
	}

	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public List<Map<String, Map<String, String>>> getResult() {
		return result;
	}

	public void setResult(List<Map<String, Map<String, String>>> result) {
		this.result = result;
	}
}
