package cn.ug.product.bean.request;

/**
 * 生成交易时间参数
 * @author ywl
 * @date 2018-05-02
 */
public class CreateTradeTimeParamBean {

    /** 类型 1：活期 2:定期 **/
    private Integer type;
    /** 上线时间 **/
    private String upTime;
    /** 募集天数 **/
    private Integer day;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }
}
