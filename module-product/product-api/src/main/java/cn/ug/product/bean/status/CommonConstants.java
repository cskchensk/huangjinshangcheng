package cn.ug.product.bean.status;

/**
 * 会员常量
 * @author ywl
 */
public class CommonConstants {

	    //产品审核状态
		public static enum ProductAuditStatus{
			NOT_AUDIT("未审核", 1),  PASS_AUDIT("已通过", 2), REFUSE_AUDIT("已拒绝",3);

			private String name;
			private int index;

			private ProductAuditStatus(String name, int index)
			{
				this.name = name;
				this.index = index;
			}

			public String getName()
			{
				return this.name;
			}

			public int getIndex()
			{
				return this.index;
			}

			public static String getName(int index)
			{
				for (ProductAuditStatus c : ProductAuditStatus.values()) {
					if (c.getIndex() == index) {
						return c.name;
					}
				}
				return null;
			}
		}
		//产品销售状态
		public static enum ProductSaleStatus{
			WAIT_SALE("等待销售", 1),  FOR_SALE("销售中", 2), HISTORY_SALE("历史销售",3) ;

			private String name;
			private int index;

			private ProductSaleStatus(String name, int index)
			{
				this.name = name;
				this.index = index;
			}

			public String getName()
			{
				return this.name;
			}

			public int getIndex()
			{
				return this.index;
			}

			public static String getName(int index)
			{
				for (ProductSaleStatus c : ProductSaleStatus.values()) {
					if (c.getIndex() == index) {
						return c.name;
					}
				}
				return null;
			}
		}
		//产品上下架状态
		public static enum ProductShelfState{
			UP_SHELF("上架", 1),  DOWN_SHELF("下架", 2);

			private String name;
			private int index;

			private ProductShelfState(String name, int index)
			{
				this.name = name;
				this.index = index;
			}

			public String getName()
			{
				return this.name;
			}

			public int getIndex()
			{
				return this.index;
			}

			public static String getName(int index)
			{
				for (ProductShelfState c : ProductShelfState.values()) {
					if (c.getIndex() == index) {
						return c.name;
					}
				}
				return null;
			}
		}
		//产品状态
		public static enum ProductStatus{
			NORMAL("正常", 1),  CANCEL("作废", 2);

			private String name;
			private int index;

			private ProductStatus(String name, int index)
			{
				this.name = name;
				this.index = index;
			}

			public String getName()
			{
				return this.name;
			}

			public int getIndex()
			{
				return this.index;
			}

			public static String getName(int index)
			{
				for (ProductStatus c : ProductStatus.values()) {
					if (c.getIndex() == index) {
						return c.name;
					}
				}
				return null;
			}
		}
		public static final String FORMAT = "yyyy-MM-dd HH:mm:ss";

		public static final String REPAYMENT_WAY = "产品还款方式";

		public static final String  STANDARD_WAY = "产品成标方式";

}
