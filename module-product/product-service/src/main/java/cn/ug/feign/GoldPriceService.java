package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(url = "http://web.juhe.cn:8080/finance/gold/shgold", value = "juhetest")
public interface GoldPriceService {

    @RequestMapping(method = GET)
    String query(@RequestParam("key") String key);

}