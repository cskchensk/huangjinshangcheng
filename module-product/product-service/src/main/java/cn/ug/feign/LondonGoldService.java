package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@FeignClient(url = "http://api.k780.com/?app=finance.gsgold&goldid=1201&version=2&appkey=33212&sign=2e3fd7198bd9da466bf713cfbaea406b&format=json", value = "londonGoldTest")
public interface LondonGoldService {

    @RequestMapping(method = GET)
    String query();
}
