package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
//todo 查询生产金价记录

//域名
@FeignClient(url = "http://api-gold.kaiwotech.com/product/price/latest/three/record", value = "ugold")
public interface UGoldPriceService {

    @RequestMapping(method = GET)
    String query();
}
