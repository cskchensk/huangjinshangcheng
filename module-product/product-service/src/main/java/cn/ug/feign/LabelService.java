package cn.ug.feign;

import cn.ug.mall.api.LabelServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface LabelService extends LabelServiceApi{
}
