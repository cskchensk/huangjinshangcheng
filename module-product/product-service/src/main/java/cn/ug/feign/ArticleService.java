package cn.ug.feign;

import cn.ug.info.api.ArticleServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("INFO-SERVICE")
public interface ArticleService extends ArticleServiceApi {

}