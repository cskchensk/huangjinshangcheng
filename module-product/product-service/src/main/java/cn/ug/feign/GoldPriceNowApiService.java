package cn.ug.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * nowapi获取金价接口
 */
//todo  需要修改goldid appkey sign这三个值
@FeignClient(url = "http://api.k780.com/?app=finance.shgold&goldid=1051&version=2&appkey=33212&sign=2e3fd7198bd9da466bf713cfbaea406b&format=json", value = "nowapitest")
public interface GoldPriceNowApiService {

    @RequestMapping(method = GET)
    String query();

}