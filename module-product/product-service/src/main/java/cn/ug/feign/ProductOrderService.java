package cn.ug.feign;

import cn.ug.pay.api.ProductOrderServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("PAY-SERVICE")
public interface ProductOrderService extends ProductOrderServiceApi{
}
