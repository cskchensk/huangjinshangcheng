package cn.ug.feign;

import cn.ug.mall.api.RateSettingsServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="MALL-SERVICE")
public interface RateSettingsService extends RateSettingsServiceApi {
}
