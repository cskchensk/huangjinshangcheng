package cn.ug.product.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 * 交易时间
 * @author ywl
 * @date 2018-04-30
 */
public class TradeTemplateTime extends BaseEntity implements Serializable {

    /** 模版Id **/
    private String templateId;
    /** 交易起始时间 **/
    private String startTime;
    /** 交易结束时间 **/
    private String endTime;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
