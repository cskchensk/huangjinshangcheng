package cn.ug.product.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.feign.RateSettingsService;
import cn.ug.mall.bean.ModuleTitle;
import cn.ug.product.bean.response.ProductDetailBean;
import cn.ug.product.bean.response.ProductHotBean;
import cn.ug.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 产品服务(移动端)
 *
 * @author ywl
 * @date 2018/1/19
 */
@RestController
@RequestMapping("api")
public class ProductApiController {

    @Resource
    private ProductService productService;

    @Resource
    private Config config;

    @Autowired
    private RateSettingsService rateSettingsService;

    /**
     * 查询新手活动产品
     *
     * @return
     */
    @RequestMapping(value = "queryNewMemberProduct", method = GET)
    public SerializeObject<List<ProductHotBean>> queryNewMemberProduct() {
        List list = new ArrayList();
        list.add(productService.queryNewMemberProduct());
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询热门活动产品列表
     *
     * @return
     */
    @RequestMapping(value = "queryHotProductList", method = GET)
    public SerializeObject<List<ProductHotBean>> queryHotProductList(HttpServletRequest request) {
        String appVsersion = request.getHeader("app");
        List<ProductHotBean> productHotBeanList = productService.queryHotProductList(appVsersion);
        return new SerializeObject<>(ResultType.NORMAL, productHotBeanList);
    }

    /**
     * 查询产品列表(移动端)
     *
     * @param type 产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品,6:实物金 7.体验金 8.安稳金
     * @return
     */
    @RequestMapping(value = "queryProductList/{type}", method = GET)
    public SerializeObject<List<ProductHotBean>> queryProductList(@PathVariable Integer type) {
        //验证参数是否正确
        Ensure.that(type).isNull("15000001");
        List<ProductHotBean> dataTable = productService.queryProductList(type);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }


    /**
     *  @Description:获取黄金投资版块，产品展示区产品(移动端)
     *  @return
     */
    @RequestMapping(value = "queryInvestProduct", method = GET)
    public SerializeObject queryInvestProduct() {
        SerializeObject<List<ModuleTitle>> data = rateSettingsService.findProductModule();
        List list = new ArrayList();
        if (data != null && !CollectionUtils.isEmpty(data.getData())) {
            List<ModuleTitle> moduleTitles = data.getData();
            for (ModuleTitle moduleTitle : moduleTitles) {
                if (null != moduleTitle.getProductType()) {
                    List<ProductHotBean> productList = productService.queryProductList(moduleTitle.getProductType());
//                    map.put("productType",moduleTitle.getProductType());
//                    map.put("moduleTitle",moduleTitle.getModuleTitle());
//                    map.put("productInfo",CollectionUtils.isEmpty(productList) ? "" :productList.get(0));
                    if (!CollectionUtils.isEmpty(productList)) {
                        Map map = new HashMap();
                        map.put("productList", productList);
                        map.put("module",moduleTitle);
                        list.add(map);
                    }
                }
            }
         }

        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询产品详情(移动端)
     *
     * @param id 产品id
     * @return
     */
    @RequestMapping(value = "queryProductById/{id}", method = GET)
    public SerializeObject<ProductDetailBean> queryProductById(@PathVariable String id) {
        ProductDetailBean productDetailBean = productService.queryProductDetailById(id);
        if (productDetailBean == null) {
            return new SerializeObjectError("15000034");
        }
        return new SerializeObject<>(ResultType.NORMAL, productDetailBean);
    }

    /**
     * 查询产品邀请好友记录
     *
     * @return
     */
    @RequestMapping(value = "queryProductInviteRecord/{id}", method = GET)
    SerializeObject queryProductInviteRecord(@RequestHeader String accessToken, @PathVariable String id) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        return new SerializeObject<>(ResultType.NORMAL, productService.queryProductInviteRecord(loginBean.getId(), id));
    }

    /**
     * 查询当前产品邀请立减详情
     *
     * @param accessToken
     * @param id  产品id
     * @return
     */
    @RequestMapping(value = "queryProductSubtractInfo/{id}", method = GET)
    SerializeObject queryProductSubtractInfo(@RequestHeader String accessToken, @PathVariable String id) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        return new SerializeObject<>(ResultType.NORMAL, productService.queryProductSubtractInfo(loginBean.getId(),loginBean.getMobile(), id));
    }
}
