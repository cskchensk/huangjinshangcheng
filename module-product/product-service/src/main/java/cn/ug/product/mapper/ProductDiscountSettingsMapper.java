package cn.ug.product.mapper;

import cn.ug.product.mapper.entity.ProductDiscountSettings;
import org.apache.ibatis.annotations.Param;

/**
 * Create by zhangweijie on 2019/9/10 0010 上午 9:51
 * 折扣金邀请好友配置
 */
public interface ProductDiscountSettingsMapper {

    int insertSelective(ProductDiscountSettings productDiscountSettings);

    ProductDiscountSettings selectByProductId(@Param("productId") String productId);

    int updateByProductId(ProductDiscountSettings productDiscountSettings);
}
