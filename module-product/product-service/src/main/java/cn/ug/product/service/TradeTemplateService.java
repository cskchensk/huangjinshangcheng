package cn.ug.product.service;

import cn.ug.bean.base.DataTable;
import cn.ug.product.bean.request.TradeTemplateParam;
import cn.ug.product.bean.request.TradeTemplateSaveParamBean;
import cn.ug.product.bean.response.TradeTemplateBean;

import java.util.List;

public interface TradeTemplateService {

    /**
     * 搜索
     * @param tradeTemplateParam	   请求参数
     * @return				            分页数据
     */
    DataTable<TradeTemplateBean> findList(TradeTemplateParam tradeTemplateParam);

    /**
     * 查询
     * @param id
     * @return
     */
    TradeTemplateBean findById(String id);

    /**
     * 保存、修改
     * @param entity
     */
    void save(TradeTemplateSaveParamBean entity);

    /**
     * 根据类型获取
     * @param type
     * @return
     */
    List<TradeTemplateBean> findByType(Integer type);

    /**
     * 删除
     * @param id
     * @return
     */
    int deleted(String id);
}
