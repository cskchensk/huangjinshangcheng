package cn.ug.product.service;

import cn.ug.product.bean.response.ProductImgBean;

import java.util.List;

public interface ProductImgService {

    int save(String productId, Integer type, List<String> urls);

    List<ProductImgBean> findList(String productId);

    int delete(String productId);
}
