package cn.ug.product.mapper.entity;

import java.io.Serializable;

public class ProductInfo implements Serializable {

    /** id **/
    private String id;
    /** 产品id **/
    private String productId;
    /** key名称 **/
    private String name;
    /** value值 **/
    private String content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
