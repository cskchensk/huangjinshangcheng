package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.bean.response.ProductInfo;
import cn.ug.product.bean.response.ProductInfoBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ProductInfoMapper extends BaseMapper<ProductInfoMapper> {

    int insert(List<ProductInfo>  list);

    List<ProductInfo> findList(String productId);

    int deleteById(@Param("productId") String productId);
}
