package cn.ug.product.mapper;

import cn.ug.product.mapper.entity.ProductMemberSubtract;

import java.util.Map;

/**
 * 折扣金立减次数记录表
 */
public interface ProductMemberSubtractMapper {

    int insertSelective(ProductMemberSubtract productMemberSubtract);

    ProductMemberSubtract selectByProductIdAndFriendId(Map<String,Object> map);

    int updateByPrimaryKeySelective(ProductMemberSubtract productMemberSubtract);
}
