package cn.ug.product.service.impl;

import cn.ug.product.bean.response.ProductLeaseInfoBean;
import cn.ug.product.mapper.ProductLeaseInfoMapper;
import cn.ug.product.service.ProductLeaseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProductLeaseInfoServiceImpl implements ProductLeaseInfoService {

    @Resource
    private ProductLeaseInfoMapper productLeaseInfoMapper;

    @Override
    public int save(List<ProductLeaseInfoBean> list) {
        return productLeaseInfoMapper.insert(list);
    }

    @Override
    public List<ProductLeaseInfoBean> findList(String productId) {
        return productLeaseInfoMapper.findList(productId);
    }
}
