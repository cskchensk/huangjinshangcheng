package cn.ug.product.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 基准金价
 * @author kaiwotech
 */
public class GoldPriceBaseline extends BaseEntity implements java.io.Serializable {

	/** 金价 */
	private BigDecimal goldPrice;
	/** 涨跌幅（%） */
	private BigDecimal limit;
	/** 最高价 */
	private BigDecimal maxPrice;
	/** 最低价 */
	private BigDecimal minPrice;
	/** 备注 */
	private String description;

	public BigDecimal getGoldPrice() {
		return goldPrice;
	}

	public void setGoldPrice(BigDecimal goldPrice) {
		this.goldPrice = goldPrice;
	}

	public BigDecimal getLimit() {
		return limit;
	}

	public void setLimit(BigDecimal limit) {
		this.limit = limit;
	}

	public BigDecimal getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}

	public BigDecimal getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
