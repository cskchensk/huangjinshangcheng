package cn.ug.product.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 黄金交易价格记录
 * @author kaiwotech
 */
public class GoldPriceRecord extends BaseEntity implements java.io.Serializable {

	/** 类型 1:上海黄金交易所 */
	private Integer type;
	/** 品种 */
	private String variety;
	/** 最新价 */
	private BigDecimal latestpri;
	/**实际价格**/
	private BigDecimal actualpri;
	/** 开盘价 */
	private BigDecimal openpri;
	/** 最高价 */
	private BigDecimal maxpri;
	/** 最低价 */
	private BigDecimal minpri;
	/** 涨跌幅（%） */
	private BigDecimal limit;
	/** 昨收价 */
	private BigDecimal yespri;
	/** 总成交量 */
	private BigDecimal totalvol;
	/** 更新时间 */
	private LocalDateTime time;
	/** 备注 */
	private String description;

	public BigDecimal getActualpri() {
		return actualpri;
	}

	public void setActualpri(BigDecimal actualpri) {
		this.actualpri = actualpri;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public BigDecimal getLatestpri() {
		return latestpri;
	}

	public void setLatestpri(BigDecimal latestpri) {
		this.latestpri = latestpri;
	}

	public BigDecimal getOpenpri() {
		return openpri;
	}

	public void setOpenpri(BigDecimal openpri) {
		this.openpri = openpri;
	}

	public BigDecimal getMaxpri() {
		return maxpri;
	}

	public void setMaxpri(BigDecimal maxpri) {
		this.maxpri = maxpri;
	}

	public BigDecimal getMinpri() {
		return minpri;
	}

	public void setMinpri(BigDecimal minpri) {
		this.minpri = minpri;
	}

	public BigDecimal getLimit() {
		return limit;
	}

	public void setLimit(BigDecimal limit) {
		this.limit = limit;
	}

	public BigDecimal getYespri() {
		return yespri;
	}

	public void setYespri(BigDecimal yespri) {
		this.yespri = yespri;
	}

	public BigDecimal getTotalvol() {
		return totalvol;
	}

	public void setTotalvol(BigDecimal totalvol) {
		this.totalvol = totalvol;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
