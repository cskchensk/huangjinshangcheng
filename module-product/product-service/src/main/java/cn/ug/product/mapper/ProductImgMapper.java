package cn.ug.product.mapper;

import cn.ug.product.bean.response.ProductImgBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductImgMapper {

    int insert(List<ProductImgBean> productImgBeans);

    List<ProductImgBean> findList(String productId);

    int deleteById(@Param("productId") String productId);
}
