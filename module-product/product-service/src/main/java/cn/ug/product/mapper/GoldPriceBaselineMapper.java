package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.mapper.entity.GoldPriceBaseline;
import cn.ug.product.mapper.entity.GoldPriceRecord;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 基准金价
 * @author kaiwotech
 */
@Component
public interface GoldPriceBaselineMapper extends BaseMapper<GoldPriceBaseline> {

}