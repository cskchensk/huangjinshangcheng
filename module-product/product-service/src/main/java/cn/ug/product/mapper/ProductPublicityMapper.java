package cn.ug.product.mapper;

import cn.ug.product.bean.response.ProductPublicityBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 产品宣传图信息
 * @Author zhangweijie
 * @Date 2019/8/27 0027
 * @time 下午 16:28
 **/
public interface ProductPublicityMapper {

    int insert(List<ProductPublicityBean> ProductPublicityBean);

    List<ProductPublicityBean> findList(String productId);

    int deleteById(@Param("productId") String productId);
}
