package cn.ug.product.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.product.bean.request.ProductParamBean;
import cn.ug.product.bean.request.TradeTemplateParam;
import cn.ug.product.bean.request.TradeTemplateSaveParamBean;
import cn.ug.product.bean.response.TradeTemplateBean;
import cn.ug.product.bean.response.TradeTemplateTimeBean;
import cn.ug.product.mapper.TradeTemplateMapper;
import cn.ug.product.mapper.TradeTemplateTimeMapper;
import cn.ug.product.mapper.entity.TradeTemplate;
import cn.ug.product.mapper.entity.TradeTemplateTime;
import cn.ug.product.service.TradeTemplateService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TradeTemplateServiceImpl  implements TradeTemplateService{

    @Resource
    private TradeTemplateMapper tradeTemplateMapper;

    @Resource
    private TradeTemplateTimeMapper tradeTemplateTimeMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<TradeTemplateBean> findList(TradeTemplateParam tradeTemplateParam) {
        com.github.pagehelper.Page<ProductParamBean> pages = PageHelper.startPage(tradeTemplateParam.getPageNum(),tradeTemplateParam.getPageSize());
        List<TradeTemplateBean> dataList = tradeTemplateMapper.findList(tradeTemplateParam);
        return new DataTable<>(tradeTemplateParam.getPageNum(), tradeTemplateParam.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public TradeTemplateBean findById(String id) {
        Ensure.that(id).isNull("00000002");
        TradeTemplate tradeTemplate = tradeTemplateMapper.findById(id);
        TradeTemplateBean entity = dozerBeanMapper.map(tradeTemplate, TradeTemplateBean.class);
        List<TradeTemplateTimeBean> list = tradeTemplateTimeMapper.findList(tradeTemplate.getId());
        entity.setList(list);
        return entity;
    }

    @Transactional
    @Override
    public void save(TradeTemplateSaveParamBean entity) {
        Ensure.that(entity.getName()).isNull("15000101");
        Ensure.that(entity.getIsDefault()).isNull("15000102");
        Ensure.that(entity.getType()).isNull("15000103");
        Ensure.that(entity.getStartTime() == null || entity.getStartTime().length == 0).isTrue("15000104");
        Ensure.that(entity.getEndTime() == null || entity.getEndTime().length == 0).isTrue("15000105");

        //查询是否存在存在
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("type", entity.getType());
        param.put("name", entity.getName());

        //新增、修改模版
        TradeTemplate tradeTemplate = dozerBeanMapper.map(entity, TradeTemplate.class);
        if(StringUtils.isBlank(entity.getId())){
            Ensure.that(tradeTemplateMapper.findByName(param) > 0).isTrue("15000106");
            tradeTemplate.setId(UF.getRandomUUID());
            tradeTemplateMapper.insert(tradeTemplate);
        }else{
            tradeTemplateMapper.update(tradeTemplate);
        }
        //删除时间
        if(StringUtils.isNotBlank(entity.getId())) tradeTemplateTimeMapper.delete(tradeTemplate.getId());

        //新增时间
        List<TradeTemplateTime> list = new ArrayList<TradeTemplateTime>();
        for(int i=0;i<entity.getStartTime().length;i++){
            TradeTemplateTime tradeTemplateTime = new TradeTemplateTime();
            tradeTemplateTime.setId(UF.getRandomUUID());
            tradeTemplateTime.setTemplateId(tradeTemplate.getId());
            tradeTemplateTime.setStartTime(entity.getStartTime() [i]);
            tradeTemplateTime.setEndTime(entity.getEndTime() [i]);
            list.add(tradeTemplateTime);
        }
        tradeTemplateTimeMapper.insert(list);
    }

    @Override
    public List<TradeTemplateBean> findByType(Integer type) {
        Ensure.that(type).isNull("15000103");
        TradeTemplateParam entity = new TradeTemplateParam();
        entity.setType(type);
        entity.setPageSize(100); //默认出现100条数据
        List<TradeTemplateBean> dataList = tradeTemplateMapper.findList(entity);
        return dataList;
    }

    @Override
    public int deleted(String id) {
        tradeTemplateMapper.delete(id);  //删除模版
        tradeTemplateTimeMapper.delete(id);  //删除模版时间数据
        return 1;
    }

    public static void main(String[] args) {
        String str [] = new String [0];
        System.out.println(str == null);
    }
}
