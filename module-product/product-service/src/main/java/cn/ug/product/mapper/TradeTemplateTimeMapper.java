package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.bean.response.TradeTemplateTimeBean;
import cn.ug.product.mapper.entity.TradeTemplateTime;

import java.util.List;

/**
 * 交易时间
 * @author ywl
 * @date 2018-04-30
 */
public interface TradeTemplateTimeMapper extends BaseMapper<TradeTemplateTime> {

    /**
     * 获取List
     * @param templateId
     * @return
     */
    List<TradeTemplateTimeBean> findList(String templateId);

    /**
     * 批量新增
     * @param list
     * @return
     */
    int insert(List<TradeTemplateTime> list);

}
