package cn.ug.product.service;

import cn.ug.product.bean.response.ProductInfoBean;

import java.util.List;

public interface ProductInfoService {

    int save(String productId, List<ProductInfoBean> list);

    int delete(String productId);
}
