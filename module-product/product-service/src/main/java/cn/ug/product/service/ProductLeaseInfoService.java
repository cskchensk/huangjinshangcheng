package cn.ug.product.service;

import cn.ug.product.bean.response.ProductLeaseInfoBean;

import java.util.List;

public interface ProductLeaseInfoService {

    int save(List<ProductLeaseInfoBean> list);

    List<ProductLeaseInfoBean> findList(String productId);
}
