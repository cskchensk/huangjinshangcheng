package cn.ug.product.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 * 产品交易时间
 * @author ywl
 * @date 2018-04-30
 */
public class ProductTradeTime extends BaseEntity implements Serializable {

    /** 日期 **/
    private String day;
    /** 产品Id **/
    private String productId;
    /** 模版Id **/
    private String templateId;
    /** 类型 **/
    private Integer type;
    /** 描述 **/
    private String detail;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
