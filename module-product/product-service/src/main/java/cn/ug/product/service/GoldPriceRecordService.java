package cn.ug.product.service;


import cn.ug.bean.base.DataTable;
import cn.ug.product.bean.GoldLatestPriceBean;
import cn.ug.product.bean.GoldPriceRecordBean;
import cn.ug.product.bean.GoldPriceTrendBean;
import cn.ug.product.mapper.entity.GoldPriceRecord;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 黄金交易价格记录
 * @author kaiwotech
 */
public interface GoldPriceRecordService {

	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(GoldPriceRecordBean entityBean);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);

	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	GoldPriceRecordBean findById(String id);

	/**
	 * 查找最新金价对象
	 * @return		实例
	 */
	GoldPriceRecordBean findLastByTime();
	double selectSomedayPrice(String searchTime);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param type			类型 1:上海黄金交易所
	 * @param addTimeMin	最小操作时间
	 * @param addTimeMax	最大操作时间
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<GoldPriceRecordBean> query(String order, String sort, int pageNum, int pageSize, Integer type,
										LocalDateTime addTimeMin, LocalDateTime addTimeMax, String keyword);



	/**
	 * 金价走势（每天一条记录）
	 * @param type			类型 1:上海黄金交易所
	 * @param addTimeMin	最小操作时间
	 * @param addTimeMax	最大操作时间
	 * @return				分页数据
	 */
	List<GoldPriceTrendBean> queryTrend(Integer type, LocalDateTime addTimeMin, LocalDateTime addTimeMax,Integer day);

	/**
	 * 金价走势（每天一条记录）
	 * @param type			类型 1:上海黄金交易所
	 * @return				分页数据
	 */
	List<GoldPriceTrendBean> queryToday(Integer type);

	List<GoldLatestPriceBean> listLatestValue(String startDate, String endDate);
	List<GoldPriceRecordBean> queryLatestRecordThree();

	DataTable<GoldPriceRecordBean> queryPriceList(int pageSize,int pageNum,String startDate,String endDate);
}
