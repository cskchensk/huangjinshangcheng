package cn.ug.product.service;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.product.bean.request.ProductAuditParamBean;
import cn.ug.product.bean.request.ProductParamBean;
import cn.ug.product.bean.response.*;
import cn.ug.product.mapper.entity.ProductMemberInviteRecordBean;
import cn.ug.product.mapper.entity.ProductMemberInviteRecordRes;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author ywl
 * @date 2018/1/18 0012
 */
public interface ProductService {

    /**
     * 搜索
     *
     * @param productParamBean 请求参数
     * @return 分页数据
     */
    DataTable<ProductManageBean> query(ProductParamBean productParamBean);

    /**
     * 添加
     *
     * @param entityBean 实体
     * @return 0:操作成功 1：不能为空 2：数据已存在
     */
    int save(ProductBean entityBean);

    /**
     * 添加
     *
     * @param entityBean 实体
     * @return 0:操作成功 1：不能为空 2：数据已存在
     */
    int productSave(ProductBean entityBean);

    /**
     * 修改
     *
     * @param entityBean
     * @return
     */
    int productUpdate(ProductBean entityBean);

    /**
     * 查看
     *
     * @param id 用户ID
     * @return 查询
     */
    ProductFindBean queryProductById(String id);

    /**
     * 查询产品
     *
     * @param id
     * @return
     */
    ProductBean findById(String id);

    /**
     * 产品审核
     *
     * @param productAuditParamBean
     */
    SerializeObject updateAuditStatus(ProductAuditParamBean productAuditParamBean);

    /**
     * 热销产品
     *
     * @param id    产品id
     * @param isHot 状态
     */
    int updateIsHot(String[] id, int isHot);

    /**
     * 上下架
     *
     * @param id         产品id
     * @param shelfState 状态
     */
    int updateShelfState(String[] id, int shelfState);

    /**
     * 是否开启交易
     *
     * @param id
     * @param isOpen
     * @return
     */
    int updateIsOpen(String id, int isOpen);

    /**
     * 查询新手活动产品列表
     *
     * @return
     */
    ProductHotBean queryNewMemberProduct();


    /**
     * 查询热门推荐产品列表
     *
     * @return
     */
    List<ProductHotBean> queryHotProductList(String appVsersion);

    /**
     * 移动端查询产品列表
     *
     * @param type 产品类型
     * @return 分页数据
     */
    List<ProductHotBean> queryProductList(Integer type);

    /**
     * 根据id获取产品信息(移动端)
     *
     * @param id
     * @return
     */
    ProductDetailBean queryProductDetailById(String id);

    /**
     * 查询是否可以买入
     *
     * @param productId
     * @return
     */
    int findIsBuy(String productId);

    /**
     * 查询是否可以卖出
     *
     * @return
     */
    int findIsSell();

    /**
     * 购买产品后增加已募集克数
     *
     * @param productId
     * @param gram
     * @return
     */
    int addToRaiseGram(String productId, BigDecimal gram);

    List<ProductManageBean> listOverdueProduct();

    /**
     * @return
     */
    ProductFindBean findActivityTaskProduct();

    /**
     * 修改实物金库存
     *
     * @param productId
     * @param Stock
     * @return
     */
    int updateStock(String productId, Integer Stock);

    /**
     * 查询协议信息
     *
     * @param productId
     * @param type      类型 1购买协议  2回租协议
     * @return
     */
    Map<String, Object> findProtocol(String productId, Integer type);

    /**
     * 查询在售的产品列表-根据类型
     *
     * @param type
     * @return
     */
    List<ProductBean> queryOnlineList(Integer type);

    /**
     * 保存折扣金邀友记录
     *
     * @param friendId
     * @param memberId
     */
    int saveProductInviteRecord(String friendId, String memberId);

    /**
     * 查询产品的邀请好友记录
     *
     * @param memberId
     * @param productId
     * @return
     */
    ProductMemberInviteRecordRes queryProductInviteRecord(String memberId, String productId);

    /**
     * 查询产品邀友配置详情
     *
     * @param memberId
     * @param productId
     */
    ProductSubtractBean queryProductSubtractInfo(String memberId, String mobile, String productId);

    /**
     * 会员立减次数修改
     *
     * @param memberId
     * @param productId
     * @param type      操作类型： 1减  2加
     * @return
     */
    int memberSubtractNumUpdate(String memberId, String productId, Integer type);

    /**
     * 查询所有活动产品的列表
     * @return
     */
    List<ProductBean> findActivityProductList();
}
