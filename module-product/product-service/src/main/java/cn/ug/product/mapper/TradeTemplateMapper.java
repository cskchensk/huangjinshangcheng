package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.bean.request.TradeTemplateParam;
import cn.ug.product.bean.response.TradeTemplateBean;
import cn.ug.product.mapper.entity.TradeTemplate;

import java.util.List;
import java.util.Map;

/**
 * 交易时间模版
 * @author ywl
 * @date 2018-04-30
 */
public interface TradeTemplateMapper extends BaseMapper<TradeTemplate> {

    /** 查询列表 **/
    List<TradeTemplateBean> findList(TradeTemplateParam tradeTemplateParam);

    int findByName(Map<String,Object> param);

    List<TradeTemplate> findDefaultTemplate(String name);
}
