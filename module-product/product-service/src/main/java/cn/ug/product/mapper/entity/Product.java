package cn.ug.product.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author ywl
 * @date 2018/1/17
 */
public class Product extends BaseEntity implements Serializable {

    /** 产品名称 **/
    private String name;
    /** 产品类型 1:新手活动 2:活动产品  3:活期产品 4:定期产品 **/
    private Integer type;
    /** 活动类型 1:是活动 2:不是活动 **/
    private Integer isActivity;
    /** 上线时间 **/
    private LocalDateTime upTime;
    /** 有效时间 **/
    private String effectiveDay;
    /** 年华收益 **/
    private BigDecimal yearsIncome;
    /** 投资期限 **/
    private String investDay;
    /** 计息方式 1:按天计息 2:到期计息 **/
    private Integer interestAccrualType;
    /** 计息日**/
    private Integer interestAccrualDay;
    /** 风险类型 1:保本型 2:非保本型 **/
    private Integer riskType;
    /** 结算方式 1.按照克重结算 2:按照现金结算 **/
    private Integer settlementType;
    /** 金价类型设置  1:实时金价 2:自定义价格 **/
    private Integer settingPriceType;
    /** 自定义黄金价格 **/
    private BigDecimal settingPrice;
    /** 最低价格 **/
    private BigDecimal priceMin;
    /** 最高价格 **/
    private BigDecimal priceMax;
    /** 最低克数 **/
    private BigDecimal gramMin;
    /** 最高克数 **/
    private BigDecimal gramMax;
    /** 募集克数 **/
    private BigDecimal raiseGram;
    /** 已募集克数 **/
    private BigDecimal toRaiseGram;
    /** 产品状态  1:正常 2:作废 **/
    private Integer status;
    /** 上架状态 1:上架 2:下架  **/
    private Integer shelfState;
    /**  审核状态 1:待审核 2:已通过 3:已拒绝 **/
    private Integer auditStatus;
    /** 热销产品 1:否 2:是 **/
    private Integer isHot;
    /** 还款方式 **/
    private String repaymentWay;
    /** 成标方式 **/
    private String standardWay;
    /** 产品描述 **/
    private String detail;
    /** 图片 **/
    private String img;
    /** 产品标签(包含两个标签，用逗号隔开) **/
    private String label;
    /** 收益标签 **/
    private String incomeLabel;
    /** 保障 **/
    private String insurance;
    /** 协议类型 **/
    private String protocolId;
    /** 产品结束时间 **/
    private LocalDateTime stopTime;
    /** 是否开启交易 1:是 2:否 **/
    private Integer isOpen;
    /** 创建用户Id **/
    private String createUserId;
    /** 创建用户姓名 **/
    private String createUser;
    /** 审核用户Id **/
    private String auditUserId;
    /** 审核用户姓名 **/
    private String auditUser;
    /** 审核描述 **/
    private String auditDescription;
    /** 审核时间 **/
    private LocalDateTime auditTime;
    /** 审核时间 **/
    private String auditTimeString;
    /** 上线时间 **/
    private String upTimeString;

    /**#####################new#########################*******/
    /**产品市场价  加减金额  实物金为加  折扣金为减**/
    private BigDecimal marketPrice;
    /**
     * 可购买用户
     * 1平台全部用户，
     * 2复投用户，
     * 3首投用户
     */
    private Integer canBuyUserType;

    /**
     * 用户限购
     * 0不限购买次数，
     * 1限购购买数量
     * 2限制最高购买克重
     */
    private Integer userAstrictStatus;

    /**
     * 用户限购数量
     */
    private Integer userAstrictNum;

    /**
     * 限制用户最高购买克重
     */
    private Integer userAstrictGram;

    /**
     * 奖励到账日期
     */
    private Integer awardToaccountDay = 1;

    /**
     * 首次回租奖励设置
     * 0不设置奖励
     */
    private Integer awardSetting;
    /**
     * 产品主标题
     */
    private String heading;

    /**
     * 产品主标题注释
     */
    private String headingNotes;
    /**
     * 产品副标题
     */
    private String subhead;

    /**
     * 产品规格
     */
    private Integer specification;

    /** 头图图片 **/
    private List<String> headImgList;

    /**
     * 加工模板id
     */
    private Integer processId;

    /**
     * 回租奖励结算方式 1以现金结算 2金豆结算
     */
    private Integer leaseCloseType;

    /**
     * 回租奖励计算金价 1.回租前一日收盘价  2.回租前一日某个时间点金价
     */
    private Integer leaseCalculateGoldType;

    /**
     * 回租前一日时间点 时间精确到分 如12:00
     */
    private String leaseCalculateGoldTime;

    /**
     * 实物金货号
     */
    private String entityItemno;

    /**
     * 实物金回租协议id
     */
    private String leaseProtocolId;

    /**
     * 库存
     */
    private Integer stock;

    /**
     * 首次购买设置奖励 0不设置
     */
    private Integer buyAwardSetting;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public LocalDateTime getUpTime() {
        return upTime;
    }

    public void setUpTime(LocalDateTime upTime) {
        this.upTime = upTime;
    }

    public String getEffectiveDay() {
        return effectiveDay;
    }

    public void setEffectiveDay(String effectiveDay) {
        this.effectiveDay = effectiveDay;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public Integer getInterestAccrualType() {
        return interestAccrualType;
    }

    public void setInterestAccrualType(Integer interestAccrualType) {
        this.interestAccrualType = interestAccrualType;
    }

    public Integer getInterestAccrualDay() {
        return interestAccrualDay;
    }

    public void setInterestAccrualDay(Integer interestAccrualDay) {
        this.interestAccrualDay = interestAccrualDay;
    }

    public Integer getRiskType() {
        return riskType;
    }

    public void setRiskType(Integer riskType) {
        this.riskType = riskType;
    }

    public Integer getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(Integer settlementType) {
        this.settlementType = settlementType;
    }

    public Integer getSettingPriceType() {
        return settingPriceType;
    }

    public void setSettingPriceType(Integer settingPriceType) {
        this.settingPriceType = settingPriceType;
    }

    public BigDecimal getSettingPrice() {
        return settingPrice;
    }

    public void setSettingPrice(BigDecimal settingPrice) {
        this.settingPrice = settingPrice;
    }

    public BigDecimal getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(BigDecimal priceMin) {
        this.priceMin = priceMin;
    }

    public BigDecimal getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(BigDecimal priceMax) {
        this.priceMax = priceMax;
    }

    public BigDecimal getGramMin() {
        return gramMin;
    }

    public void setGramMin(BigDecimal gramMin) {
        this.gramMin = gramMin;
    }

    public BigDecimal getGramMax() {
        return gramMax;
    }

    public void setGramMax(BigDecimal gramMax) {
        this.gramMax = gramMax;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getShelfState() {
        return shelfState;
    }

    public void setShelfState(Integer shelfState) {
        this.shelfState = shelfState;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Integer getIsHot() {
        return isHot;
    }

    public void setIsHot(Integer isHot) {
        this.isHot = isHot;
    }

    public String getRepaymentWay() {
        return repaymentWay;
    }

    public void setRepaymentWay(String repaymentWay) {
        this.repaymentWay = repaymentWay;
    }

    public String getStandardWay() {
        return standardWay;
    }

    public void setStandardWay(String standardWay) {
        this.standardWay = standardWay;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIncomeLabel() {
        return incomeLabel;
    }

    public void setIncomeLabel(String incomeLabel) {
        this.incomeLabel = incomeLabel;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(String protocolId) {
        this.protocolId = protocolId;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public void setStopTime(LocalDateTime stopTime) {
        this.stopTime = stopTime;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(String auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getAuditDescription() {
        return auditDescription;
    }

    public void setAuditDescription(String auditDescription) {
        this.auditDescription = auditDescription;
    }

    public String getAuditTimeString() {
        return auditTimeString;
    }

    public void setAuditTimeString(String auditTimeString) {
        this.auditTimeString = auditTimeString;
    }

    public String getUpTimeString() {
        return upTimeString;
    }

    public void setUpTimeString(String upTimeString) {
        this.upTimeString = upTimeString;
    }

    public LocalDateTime getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(LocalDateTime auditTime) {
        this.auditTime = auditTime;
    }

    public BigDecimal getRaiseGram() {
        return raiseGram;
    }

    public void setRaiseGram(BigDecimal raiseGram) {
        this.raiseGram = raiseGram;
    }

    public Integer getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Integer isOpen) {
        this.isOpen = isOpen;
    }

    public BigDecimal getToRaiseGram() {
        return toRaiseGram;
    }

    public void setToRaiseGram(BigDecimal toRaiseGram) {
        this.toRaiseGram = toRaiseGram;
    }

    public Integer getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(Integer isActivity) {
        this.isActivity = isActivity;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(BigDecimal marketPrice) {
        this.marketPrice = marketPrice;
    }

    public Integer getCanBuyUserType() {
        return canBuyUserType;
    }

    public void setCanBuyUserType(Integer canBuyUserType) {
        this.canBuyUserType = canBuyUserType;
    }

    public Integer getUserAstrictStatus() {
        return userAstrictStatus;
    }

    public void setUserAstrictStatus(Integer userAstrictStatus) {
        this.userAstrictStatus = userAstrictStatus;
    }

    public Integer getUserAstrictNum() {
        return userAstrictNum;
    }

    public void setUserAstrictNum(Integer userAstrictNum) {
        this.userAstrictNum = userAstrictNum;
    }

    public Integer getUserAstrictGram() {
        return userAstrictGram;
    }

    public void setUserAstrictGram(Integer userAstrictGram) {
        this.userAstrictGram = userAstrictGram;
    }

    public Integer getAwardToaccountDay() {
        return awardToaccountDay;
    }

    public void setAwardToaccountDay(Integer awardToaccountDay) {
        this.awardToaccountDay = awardToaccountDay;
    }

    public Integer getAwardSetting() {
        return awardSetting;
    }

    public void setAwardSetting(Integer awardSetting) {
        this.awardSetting = awardSetting;
    }

    public String getSubhead() {
        return subhead;
    }

    public void setSubhead(String subhead) {
        this.subhead = subhead;
    }

    public Integer getSpecification() {
        return specification;
    }

    public void setSpecification(Integer specification) {
        this.specification = specification;
    }

    public List<String> getHeadImgList() {
        return headImgList;
    }

    public void setHeadImgList(List<String> headImgList) {
        this.headImgList = headImgList;
    }

    public Integer getProcessId() {
        return processId;
    }

    public void setProcessId(Integer processId) {
        this.processId = processId;
    }

    public Integer getLeaseCloseType() {
        return leaseCloseType;
    }

    public void setLeaseCloseType(Integer leaseCloseType) {
        this.leaseCloseType = leaseCloseType;
    }

    public Integer getLeaseCalculateGoldType() {
        return leaseCalculateGoldType;
    }

    public void setLeaseCalculateGoldType(Integer leaseCalculateGoldType) {
        this.leaseCalculateGoldType = leaseCalculateGoldType;
    }

    public String getLeaseCalculateGoldTime() {
        return leaseCalculateGoldTime;
    }

    public void setLeaseCalculateGoldTime(String leaseCalculateGoldTime) {
        this.leaseCalculateGoldTime = leaseCalculateGoldTime;
    }

    public String getEntityItemno() {
        return entityItemno;
    }

    public void setEntityItemno(String entityItemno) {
        this.entityItemno = entityItemno;
    }

    public String getLeaseProtocolId() {
        return leaseProtocolId;
    }

    public void setLeaseProtocolId(String leaseProtocolId) {
        this.leaseProtocolId = leaseProtocolId;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getBuyAwardSetting() {
        return buyAwardSetting;
    }

    public void setBuyAwardSetting(Integer buyAwardSetting) {
        this.buyAwardSetting = buyAwardSetting;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getHeadingNotes() {
        return headingNotes;
    }

    public void setHeadingNotes(String headingNotes) {
        this.headingNotes = headingNotes;
    }
}
