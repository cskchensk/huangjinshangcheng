package cn.ug.product.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.product.bean.BannerBean;
import cn.ug.web.controller.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Banner
 * @author kaiwotech
 */
@RestController
@RequestMapping("banner")
public class BannerController extends BaseController {

    /**
     * 查询列表
     * @param type	    1:首页Banner
     * @return			列表数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<List<BannerBean>> list(Integer type) {
        if(null == type || type <= 0) {
            type = 0;
        }
        String[] banners = {
                "http://www.caroilhui.com//file/banner/image/51ea7717b9c14c2e9b101937cd233163.png",
                "http://www.caroilhui.com//file/banner/image/51ea7717b9c14c2e9b101937cd233163.png"};
        List<BannerBean> list = new ArrayList<>();
        for (String url : banners) {
            BannerBean bannerBean = new BannerBean();
            bannerBean.setImage(url);
            list.add(bannerBean);
        }

        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
