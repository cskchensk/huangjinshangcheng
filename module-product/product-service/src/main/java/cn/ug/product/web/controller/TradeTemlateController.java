package cn.ug.product.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.product.bean.request.TradeTemplateParam;
import cn.ug.product.bean.request.TradeTemplateSaveParamBean;
import cn.ug.product.bean.response.TradeTemplateBean;
import cn.ug.product.service.TradeTemplateService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 交易模版
 * @author ywl
 * @date 2018/1/19
 */
@RestController
@RequestMapping("tradeTemplate")
public class TradeTemlateController {

    @Resource
    private TradeTemplateService tradeTemplateService;

    @Resource
    private Config config;

    /**
     * 查询列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param tradeTemplateParam       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<TradeTemplateBean>> findList(@RequestHeader String accessToken, TradeTemplateParam tradeTemplateParam) {
        if(tradeTemplateParam.getPageSize() <= 0) {
            tradeTemplateParam.setPageSize(config.getPageSize());
        }
        DataTable<TradeTemplateBean> dataTable = tradeTemplateService.findList(tradeTemplateParam);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 根据ID查找信息(后台)
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "findById", method = GET)
    public SerializeObject findById(@RequestHeader(value = "accessToken", required = false) String accessToken,String id) {
        TradeTemplateBean entity = tradeTemplateService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObject(ResultType.NORMAL, "00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增、修改模版时间(后台)
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, TradeTemplateSaveParamBean entity) {
        tradeTemplateService.save(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据类型获取List
     * @param accessToken
     * @param type
     * @return
     */
    @RequestMapping(value = "findByType" , method = GET)
    public SerializeObject<List<TradeTemplateBean>> findByType(@RequestHeader String accessToken, Integer type) {
        List<TradeTemplateBean> list = tradeTemplateService.findByType(type);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 新增、修改模版时间(后台)
     * @param accessToken		登录成功后分配的Key
     * @param id		       模版id
     * @return				    是否操作成功
     */
    @PutMapping(value = "deleted")
    public SerializeObject deleted(@RequestHeader String accessToken, String id) {
        tradeTemplateService.deleted(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

}
