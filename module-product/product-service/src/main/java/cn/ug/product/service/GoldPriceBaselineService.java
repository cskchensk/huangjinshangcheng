package cn.ug.product.service;


import cn.ug.bean.base.DataTable;
import cn.ug.product.bean.GoldPriceBaselineBean;

/**
 * 黄金交易价格记录
 * @author kaiwotech
 */
public interface GoldPriceBaselineService {

	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int saveOrUpdate(GoldPriceBaselineBean entityBean);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);

	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	GoldPriceBaselineBean findById(String id);

	/**
	 * 查找最新金价基准
	 * @return		实例
	 */
	GoldPriceBaselineBean findLastByTime();

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @return				分页数据
	 */
	DataTable<GoldPriceBaselineBean> query(String order, String sort, int pageNum, int pageSize);
}
