package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.bean.GoldLatestPriceBean;
import cn.ug.product.mapper.entity.GoldPriceRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 黄金交易价格记录
 * @author kaiwotech
 */
@Component
public interface GoldPriceRecordMapper extends BaseMapper<GoldPriceRecord> {

    /**
     * 分页查询
     * @param params			查询参数
     * @return					当前页记录数
     */
    List<GoldPriceRecord> queryTrend(Map<String, Object> params);
    List<GoldPriceRecord> queryTrendByTimeType(Map<String, Object> params);
    GoldPriceRecord queryLatestRecord();
    List<GoldPriceRecord> queryLatestRecordThree();
    List<GoldLatestPriceBean> listLatestValue(@Param("startDate")String startDate, @Param("endDate")String endDate);
    List<GoldLatestPriceBean> listOpeningAndClosingPrice(@Param("startDate")String startDate, @Param("endDate")String endDate);
    Double selectSomedayPrice(@Param("searchTime")String searchTime);
    List<GoldPriceRecord> queryRecordList(Map<String, Object> params);
    Integer countRecordList();
}