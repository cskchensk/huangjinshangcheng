package cn.ug.product.mapper.entity;/**
 * Create by zhangweijie on 2019/9/11 0011 下午 13:43
 */

import java.io.Serializable;

/**
 *@Author zhangweijie
 *@Date 2019/9/11 0011
 *@time 下午 13:43
 **/
public class ProductMemberInviteRecordBean implements Serializable {
    /**
     *   手机号
     * @mbg.generated
     */
    private String mobile;
    /**
     *   被邀请人会员id
     * @mbg.generated
     */
    private String memberId;

    /** 添加时间 */
    private String addTimeString;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAddTimeString() {
        return addTimeString;
    }

    public void setAddTimeString(String addTimeString) {
        this.addTimeString = addTimeString;
    }
}
