package cn.ug.product.service.impl;

import cn.ug.product.bean.response.ProductInfoBean;
import cn.ug.product.mapper.ProductInfoMapper;
import cn.ug.product.service.ProductInfoService;
import cn.ug.util.UF;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class ProductInfoServiceImpl implements ProductInfoService {

    @Resource
    private ProductInfoMapper productInfoMapper;

    @Override
    public int save(String productId, List<ProductInfoBean> list) {
        List<cn.ug.product.bean.response.ProductInfo> allProductInfoList = new ArrayList<>();
        if(list != null && !list.isEmpty()){
            Iterator<ProductInfoBean> iter = list.iterator();
            int i =1;
            while(iter.hasNext()){
                ProductInfoBean productInfoBean = iter.next();
                List<cn.ug.product.bean.response.ProductInfo> productInfoList = productInfoBean.getProductIntroduceList();
                for (cn.ug.product.bean.response.ProductInfo productInfo : productInfoList){
                    productInfo.setTitle(productInfoBean.getTitle());
                    productInfo.setId(UF.getRandomUUID());
                    productInfo.setProductId(productId);
                    productInfo.setSort(i);
                    i++;
                }
                allProductInfoList.addAll(productInfoList);
            }
            return productInfoMapper.insert(allProductInfoList);
        }
        return 0;
    }

    @Override
    public int delete(String productId) {
        return productInfoMapper.deleteById(productId);
    }
}
