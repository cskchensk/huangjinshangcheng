package cn.ug.product.mapper.entity;

import java.util.List;

/**
 *@Author zhangweijie
 *@Date 2019/9/23 0023
 *@time 下午 17:12
 **/
public class ProductMemberInviteRecordRes {
    private Boolean limitStatus;

    private List<ProductMemberInviteRecordBean> productMemberInviteRecordList;

    public Boolean getLimitStatus() {
        return limitStatus;
    }

    public void setLimitStatus(Boolean limitStatus) {
        this.limitStatus = limitStatus;
    }

    public List<ProductMemberInviteRecordBean> getProductMemberInviteRecordList() {
        return productMemberInviteRecordList;
    }

    public void setProductMemberInviteRecordList(List<ProductMemberInviteRecordBean> productMemberInviteRecordList) {
        this.productMemberInviteRecordList = productMemberInviteRecordList;
    }
}
