package cn.ug.product.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.product.bean.request.CreateTradeTimeParamBean;
import cn.ug.product.bean.response.ProductTradeTimeBean;
import cn.ug.product.service.ProductTradeTimeService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 产品交易时间段
 * @author  ywl
 * @date 2018-05-01
 */
@RestController
@RequestMapping("productTradeTime")
public class ProductTradeTimeController {

    @Resource
    private ProductTradeTimeService productTradeTimeService;

    @Resource
    private Config config;

    /**
     * 产品交易时间段列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param productId               请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<List<ProductTradeTimeBean>> findList(@RequestHeader String accessToken, String productId) {
        List<ProductTradeTimeBean> list = productTradeTimeService.findList(productId);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }


    /**
     * 更新某一天时间段
     * @param accessToken
     * @param id
     * @param templateId
     * @return
     */
    @PutMapping(value = "update")
    public Serializable update(@RequestHeader String accessToken, String id,String templateId){
        productTradeTimeService.update(id, templateId);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 生成交易时间
     * @param accessToken
     * @param entity
     * @return
     */
    @GetMapping(value = "createTradeTimeList")
    public SerializeObject<List<ProductTradeTimeBean>> createTradeTimeList(@RequestHeader String accessToken, CreateTradeTimeParamBean entity){
        List<ProductTradeTimeBean> list = productTradeTimeService.createTradeTimeList(entity);
        return new SerializeObject(ResultType.NORMAL, list);
    }

    /**
     * 生成下个月时间段
     * @param accessToken
     * @param productId  产品id
     * @return
     */
    @GetMapping(value = "createNextMonthTradeTimeList")
    public Serializable createNextMonthTradeTimeList(@RequestHeader String accessToken, String productId){
        productTradeTimeService.createNextMonthTradeTimeList(productId);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }


}
