package cn.ug.product.mapper;

import cn.ug.product.mapper.entity.ProductMemberInviteRecord;
import cn.ug.product.mapper.entity.ProductMemberInviteRecordBean;

import java.util.List;
import java.util.Map;

/**
 * Create by zhangweijie on 2019/9/10 0010 上午 9:54
 * 折扣金邀请好友记录
 */
public interface ProductMemberInviteRecordMapper {

    int countByProductIdAndFriendId(Map<String,Object> params);

    int insertSelective(ProductMemberInviteRecord productMemberInviteRecord);

    List<ProductMemberInviteRecordBean> queryByProductIdAndFriendId(Map<String,Object> params);
}
