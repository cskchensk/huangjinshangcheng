package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.bean.request.ProductAuditParamBean;
import cn.ug.product.bean.request.ProductParamBean;
import cn.ug.product.bean.response.ProductFindBean;
import cn.ug.product.bean.response.ProductHotBean;
import cn.ug.product.bean.response.ProductManageBean;
import cn.ug.product.mapper.entity.Product;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ywl
 * @date 2018/1/18
 */
@Component
public interface ProductMapper extends BaseMapper<Product> {

    /**
     * 查询列表(后台)
     * @param productParamBean
     * @return
     */
    List<ProductManageBean> queryProductList(ProductParamBean productParamBean);


    /**
     *  查询募集时间到期，准备下架的产品
     * @return
     */
    List<ProductManageBean> queryOverdueProductList();

    /**
     * 获取产品明细(后台)
     * @param id
     * @return
     */
    ProductFindBean queryProductById(String id);

    /**
     * 更新审核状态(后台)
     * @param productAuditParamBean
     */
    int updateAuditStatus(ProductAuditParamBean productAuditParamBean);

    /**
     * 推荐热销产品(后台)
     * @param map
     */
    int updateIsHot(Map<String,Object> map);

    /**
     * 更新上下架产品(后台)
     * @param map
     */
    int updateShelfState(Map<String,Object> map);

    /**
     * 更新是否开启交易
     * @param map
     * @return
     */
    int updateIsOpen(Map<String,Object> map);

    /**
     * 获取新会员活动--新手活动(移动端)
     * @return
     */
    ProductHotBean queryNewMemberProduct();;

    /**
     * 获取热门产品列表(移动端)
     * @return
     */
    List<ProductHotBean> queryHotProductList();

    /**
     * 获取产品列表(移动端)
     * @param  type            产品类型
     * @return
     */
    List<ProductHotBean> queryProductBaseList(Integer type);

    /** 获取活期产品 **/
    Product findCurrentProduct();

    /**
     * 新增已募集克重数量
     * @param param
     * @return
     */
    int addToRaiseGram(Map<String,Object> param);

    /**
     * 获取活动任务产品
     * @return
     */
    Product findActivityTaskProduct();

    /**
     * 修改实物金产品库存
     * @param param
     * @return
     */
    int updateStock(Map<String,Object> param);

    /**
     * 查询热销集合
     * @return
     */
    List<ProductHotBean> queryIsHotList();

    /**
     * 批量修改热销产品
     * @param list
     * @return
     */
    int updateIsHotList(List<String> list);

    /**
     * 查询是否有审核通过和上架的产品信息
     * @return
     */
    List<Product> findExperience(@Param("type") Integer type);

    /**
     * 修改图片根据id
     * @param param
     * @return
     */
    int updateImgById(Map<String,Object> param);

    /**
     * 查询用户是否交易过
     * @param param
     * @return
     */
    List<Integer> queryTransactionNum(Map<String,Object> param);

    /**
     * 查询所有的活动产品
     * @return
     */
    List<Product> findActivityProductList();
}
