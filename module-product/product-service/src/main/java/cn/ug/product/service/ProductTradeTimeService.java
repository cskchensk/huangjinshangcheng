package cn.ug.product.service;

import cn.ug.product.bean.request.CreateTradeTimeParamBean;
import cn.ug.product.bean.response.ProductTradeTimeBean;

import java.util.List;

/**
 * 产品交易时间段
 * @author  ywl
 * @date 2018-05-01
 */
public interface ProductTradeTimeService {

    /**
     * 查询所有交易时间段列表
     * @param productId
     * @return
     */
    List<ProductTradeTimeBean> findList(String productId);

    /**
     * 更新
     * @param id
     * @param templateId
     * @return
     */
    int update(String id, String templateId);

    /**
     * 生成交易时间
     * @return
     */
    List<ProductTradeTimeBean> createTradeTimeList(CreateTradeTimeParamBean entity);

    /**
     * 新增
     * @param productId 产品id
     * @param list
     * @return
     */
    int save(String productId, List<ProductTradeTimeBean> list);

    /**
     * 创建下个月交易时间集合
     * @param productId
     * @return
     */
    int createNextMonthTradeTimeList(String productId);
}
