package cn.ug.product.service.impl;

import cn.ug.product.bean.response.ProductImgBean;
import cn.ug.product.mapper.ProductImgMapper;
import cn.ug.product.service.ProductImgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductImgServiceImpl implements ProductImgService {

    @Resource
    private ProductImgMapper productImgMapper;

    @Override
    public int save(String productId, Integer type, List<String> urls) {
        if (CollectionUtils.isEmpty(urls))
            return 0;

        List<ProductImgBean> productImgBeanList = new ArrayList<>();
        for (String url : urls){
            ProductImgBean productImgBean = new ProductImgBean();
            productImgBean.setUrl(url);
            productImgBean.setProductId(productId);
            productImgBean.setType(type);
            productImgBeanList.add(productImgBean);
        }
        return productImgMapper.insert(productImgBeanList);
    }

    @Override
    public List<ProductImgBean> findList(String productId) {
        return productImgMapper.findList(productId);
    }

    @Override
    public int delete(String productId) {
        return productImgMapper.deleteById(productId);
    }
}
