package cn.ug.product.mapper;

import cn.ug.product.bean.response.ProductLeaseInfoBean;

import java.util.List;

public interface ProductLeaseInfoMapper {

    int insert(List<ProductLeaseInfoBean> list);

    List<ProductLeaseInfoBean> findList(String productId);
}
