package cn.ug.product.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 交易时间模版
 * @author ywl
 * @date 2018-04-30
 */
public class TradeTemplate  extends BaseEntity implements java.io.Serializable {

    /** 模版名称 **/
    private String name;
    /** 类型 1:买入 2:卖出 **/
    private Integer type;
    /** 是否默认模版 1:是 2:否 **/
    private Integer isDefault;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }
}
