package cn.ug.product.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.product.bean.response.ProductTradeTimeBean;
import cn.ug.product.mapper.entity.ProductTradeTime;

import java.util.List;
import java.util.Map;

/**
 * 产品交易时间模版
 * @author ywl
 * @date 2018-04-30
 */
public interface ProductTradeTimeMapper extends BaseMapper<ProductTradeTime>  {

    List<ProductTradeTimeBean> findList(String productId);

    int insert(List<ProductTradeTimeBean> list);

    ProductTradeTime findByDay(Map<String,Object> map);

    String findLastDay(String productId);
}
