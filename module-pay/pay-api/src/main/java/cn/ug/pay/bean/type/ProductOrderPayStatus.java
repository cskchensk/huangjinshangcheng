package cn.ug.pay.bean.type;

/**
 * 产品订单支付状态
 * @author kaiwotech
 */
public enum ProductOrderPayStatus {

    /** 待支付 */
    WAIT(1),
    /** 已支付 */
    COMPLETE(2),
    /** 已过期 */
    EXPIRED(3),
    /** 失败*/
    FAIL(4);

    private int result = 1;

    ProductOrderPayStatus(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

}
