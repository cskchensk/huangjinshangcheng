package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class EverydayBuyGoldBean implements Serializable {
    private int index;
    private String payDate;
    private BigDecimal payGram;
    private BigDecimal payAmount;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public BigDecimal getPayGram() {
        return payGram;
    }

    public void setPayGram(BigDecimal payGram) {
        this.payGram = payGram;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }
}
