package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.response.BankCardFindBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("bankcard")
public interface BankCardServiceApi{

    /**
     * 验证是否绑卡
     * @param memberId
     * @return
     */
    @RequestMapping(value = "validateBindBankCard" ,method = GET)
    SerializeObject validateBindBankCard(@RequestParam("memberId")String memberId);

    @RequestMapping(value = "/member/card" ,method = GET)
    SerializeObject<BankCardFindBean> getBankCard(@RequestParam("memberId")String memberId);
}
