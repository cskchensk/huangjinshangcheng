package cn.ug.pay.bean.request;


import java.io.Serializable;

/**
 * 易宝异步回调参数-移动端
 * @author ywl
 * @date 2018/2/1
 */
public class YeePayParamBean  implements Serializable{

    /** 商户编号 **/
    private String merchantno;
    /** 请求数据包 **/
    private String data;
    /** 签名密钥 **/
    private String encryptkey;

    public String getMerchantno() {
        return merchantno;
    }

    public void setMerchantno(String merchantno) {
        this.merchantno = merchantno;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getEncryptkey() {
        return encryptkey;
    }

    public void setEncryptkey(String encryptkey) {
        this.encryptkey = encryptkey;
    }
}
