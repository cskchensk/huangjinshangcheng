package cn.ug.pay.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员资金账户-后台
 * @author ywl
 * @date 2018/2/5
 */
public class MemberAccountManageBean extends BaseBean implements Serializable {

    /** 会员Id **/
    private String memberId;
    /** 会员名称 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 资金总额 **/
    private BigDecimal fundAmount;
    /** 冻结金额 **/
    private BigDecimal freezeAmount;
    /** 可用资金 **/
    private BigDecimal usableAmount;
    /**  已获得现金红包总金额 **/
    private BigDecimal couponAmount;
    /**  已获得加息券奖励总金额 **/
    private BigDecimal interestAmount;

    /** 累计获得体验金（克） **/
    private int totalGram;
    /** 当前可用体验金（克） **/
    private int usableGram;
    /** 累计已获得体验金奖励（克） **/
    private  BigDecimal totalIncomeGram;
    /** 折算金额（元） **/
    private  BigDecimal convertAmount;
    /** 对应金豆（个） **/
    private Integer convertBean;

    private int index;

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getInterestAmount() {
        return interestAmount;
    }

    public void setInterestAmount(BigDecimal interestAmount) {
        this.interestAmount = interestAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(BigDecimal fundAmount) {
        this.fundAmount = fundAmount;
    }

    public BigDecimal getFreezeAmount() {
        return freezeAmount;
    }

    public void setFreezeAmount(BigDecimal freezeAmount) {
        this.freezeAmount = freezeAmount;
    }

    public BigDecimal getUsableAmount() {
        return usableAmount;
    }

    public void setUsableAmount(BigDecimal usableAmount) {
        this.usableAmount = usableAmount;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getTotalIncomeGram() {
        return totalIncomeGram;
    }

    public void setTotalIncomeGram(BigDecimal totalIncomeGram) {
        this.totalIncomeGram = totalIncomeGram;
    }

    public BigDecimal getConvertAmount() {
        return convertAmount;
    }

    public void setConvertAmount(BigDecimal convertAmount) {
        this.convertAmount = convertAmount;
    }

    public Integer getConvertBean() {
        return convertBean;
    }

    public void setConvertBean(Integer convertBean) {
        this.convertBean = convertBean;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public int getUsableGram() {
        return usableGram;
    }

    public void setUsableGram(int usableGram) {
        this.usableGram = usableGram;
    }
}
