package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeFutureValueBean implements Serializable {
    private String time;
    private BigDecimal oneDayAmount;
    private BigDecimal sevenDayAmount;
    private BigDecimal thirtyDayAmount;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getOneDayAmount() {
        return oneDayAmount;
    }

    public void setOneDayAmount(BigDecimal oneDayAmount) {
        this.oneDayAmount = oneDayAmount;
    }

    public BigDecimal getSevenDayAmount() {
        return sevenDayAmount;
    }

    public void setSevenDayAmount(BigDecimal sevenDayAmount) {
        this.sevenDayAmount = sevenDayAmount;
    }

    public BigDecimal getThirtyDayAmount() {
        return thirtyDayAmount;
    }

    public void setThirtyDayAmount(BigDecimal thirtyDayAmount) {
        this.thirtyDayAmount = thirtyDayAmount;
    }
}
