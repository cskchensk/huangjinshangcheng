package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeTotalAmountBean implements Serializable {
    private String time;
    private BigDecimal tradeAmount;
    private BigDecimal tradeTotalAmount;
    private BigDecimal regularlyTradeAmount;
    private BigDecimal regularlyTradeTotalAmount;
    //每日持有黄金总量
    private Integer tbillGram;
    private Integer tbillTotalGram;
    //日增充值金额
    private BigDecimal rechargeAmount;
    private BigDecimal rechargeTotalAmount;

    //日提现总额
    private BigDecimal withdrawAmount;
    private BigDecimal withdrawTotalAmount;
    public TradeTotalAmountBean() {

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public BigDecimal getTradeTotalAmount() {
        return tradeTotalAmount;
    }

    public void setTradeTotalAmount(BigDecimal tradeTotalAmount) {
        this.tradeTotalAmount = tradeTotalAmount;
    }

    public BigDecimal getRegularlyTradeAmount() {
        return regularlyTradeAmount;
    }

    public void setRegularlyTradeAmount(BigDecimal regularlyTradeAmount) {
        this.regularlyTradeAmount = regularlyTradeAmount;
    }

    public BigDecimal getRegularlyTradeTotalAmount() {
        return regularlyTradeTotalAmount;
    }

    public void setRegularlyTradeTotalAmount(BigDecimal regularlyTradeTotalAmount) {
        this.regularlyTradeTotalAmount = regularlyTradeTotalAmount;
    }

    public Integer getTbillGram() {
        return tbillGram;
    }

    public void setTbillGram(Integer tbillGram) {
        this.tbillGram = tbillGram;
    }

    public Integer getTbillTotalGram() {
        return tbillTotalGram;
    }

    public void setTbillTotalGram(Integer tbillTotalGram) {
        this.tbillTotalGram = tbillTotalGram;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getRechargeTotalAmount() {
        return rechargeTotalAmount;
    }

    public void setRechargeTotalAmount(BigDecimal rechargeTotalAmount) {
        this.rechargeTotalAmount = rechargeTotalAmount;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public BigDecimal getWithdrawTotalAmount() {
        return withdrawTotalAmount;
    }

    public void setWithdrawTotalAmount(BigDecimal withdrawTotalAmount) {
        this.withdrawTotalAmount = withdrawTotalAmount;
    }
}
