package cn.ug.pay.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 活跃用户累计
 */
public class ActiveTotalBean implements Serializable {

    /** 类型 1：买黄金用户 2：卖黄金用户 **/
    private Integer type;
    /** 数量 **/
    private Integer number;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
