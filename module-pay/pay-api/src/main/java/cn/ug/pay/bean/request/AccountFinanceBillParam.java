package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountFinanceBillParam extends Page implements Serializable{

    /** 日期 **/
    private String day;
    /**  通道方式 1:易宝支付 **/
    private Integer  way;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
