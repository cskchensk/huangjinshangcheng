package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class RFMLabelBean implements Serializable {
    private String memberId;
    private BigDecimal m;
    private int r;
    private int f;
    private String recentTradeTime;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getM() {
        return m;
    }

    public void setM(BigDecimal m) {
        this.m = m;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public String getRecentTradeTime() {
        return recentTradeTime;
    }

    public void setRecentTradeTime(String recentTradeTime) {
        this.recentTradeTime = recentTradeTime;
    }
}
