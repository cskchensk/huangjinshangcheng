package cn.ug.pay.bean.response;

import java.math.BigDecimal;

//客户端提金提单详情
public class ExtractGoldDetail extends ExtractGoldBaseBean{

    /**
     * 商品克重
     */
    private BigDecimal gram;

    /**
     * 提单生成时间
     */
    private String tbillTime;

    /**
     * 提单编号
     */
    private String tbillNo;

    /**
     * 提单来源
     */
    private String source;

    /**
     * 商品名称
     */
    private String  productName;

    private String productId;

    public BigDecimal getGram() {
        return gram;
    }

    public void setGram(BigDecimal gram) {
        this.gram = gram;
    }

    public String getTbillTime() {
        return tbillTime;
    }

    public void setTbillTime(String tbillTime) {
        this.tbillTime = tbillTime;
    }

    public String getTbillNo() {
        return tbillNo;
    }

    public void setTbillNo(String tbillNo) {
        this.tbillNo = tbillNo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
