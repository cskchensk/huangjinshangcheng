package cn.ug.pay.bean.type;

/**
 * 支付类型
 */
public enum PayType {

    /** 现金购买 */
    BUY(1),
    /** 金柚宝转入 */
    TURN_INTO(2);

    private int result = 1;

    PayType(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

}
