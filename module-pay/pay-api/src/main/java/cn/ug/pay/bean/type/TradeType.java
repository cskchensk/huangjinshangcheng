package cn.ug.pay.bean.type;

/**
 * 交易类型
 */
public enum TradeType {

    /** 线上充值 */
    ONLINE_RECHARGE(1, "线上充值"),
    /** 卖金收入 */
    SELL_GOLD_INCOME(2, "卖金收入"),
    /** 邀请返现 */
    INVITATION_BACK(3, "邀请返现"),
    /** 账户提现 */
    ACCOUNT_WITHDRAWAL(4, "账户提现成功"),
    /** 买金支出 */
    BUY_GOLD(5, "买金支出"),
    /** 投资返利 */
    REBATE(6, "投资返利"),
    /** 红包返现 */
    CASH_BACK(7, "红包返现"),
    /** 加息奖励 */
    RATE_INCREASE(8, "加息奖励"),
    /** 购买商品 **/
    BUY_GOODS(9, "购买商品"),
    /** 提现失败返还 **/
    ACCOUNT_WITHDRWAL_RETURN(10, "提现失败返还"),
    /** 提金加工费**/
    GOLD_EXTRACTION_PROCESS_COST(11, "提金加工费"),
    /** 提金运费**/
    GOLD_EXTRACTION_FREIGHT(12, "提金运费"),
    /** 换金加工费**/
    CHANGE_GOLD_PROCESS_COST(13, "换金加工费"),
    /** 换金运费**/
    CHANGE_GOLD_FREIGHT(14, "换金运费"),
	CASH_REWARDS(15, "现金奖励转入"),
    /**提现处理中*/
	HANDLING_WITHDRAW(16, "账户提现处理中"),
    //卖金手续费
    SELL_GOLD_FEE(17, "卖金手续费"),
    //充值手续费
    RECHARGE_FEE(18, "充值手续费"),
    //提现手续费
    WITHDRAW_FEE(19, "提现手续费"),
    EBANK_RECHARGE(20, "网银充值"),
    LEASEBACK_REWARDS(21, "回租奖励"),
    TICKET_REWARDS(22, "回租福利券奖励"),
    TBILL_SOLD(23, "提单出售收入"),
    TBILL_SOLD_FEE(24, "提单出售手续费"),
    GOLD_EXTRACTION(25,"提金手续费"),
    GOLD_EXTRACTION_CLOSE_RETURN(26,"取消提金返还"),
    EXPERIENCE_LEASEBACK_REWARDS(27,"体验金回租奖励"),
    CHANNEL_REWARDS(30, "渠道现金奖励"),
    LEASEBACK_ENJOY_REWARDS(31, "回租专享优惠"),
    LARGE_AMOUNT_RECHARGE(32,"大额转账充值");

    private int result = 1;
    private String remark;

    TradeType(int i, String remark){
        this.result = i;
        this.remark = remark;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

    public static String getRemark(int tradeType) {
        for (TradeType v : TradeType.values()) {
            if (v.getValue() == tradeType) {
                return v.getRemark();
            }
        }
        return "";
    }

    public String getRemark() {
        return this.remark;
    }
}
