package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class DueinGoldBeanBean implements Serializable {
    private int index;
    private String orderNO;
    private String productName;
    private int leasebackDays;
    private BigDecimal yearIncome;
    private BigDecimal incomeAmount;
    private BigDecimal incomeGram;
    private int incomeBeans;
    private String endTime;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public BigDecimal getYearIncome() {
        return yearIncome;
    }

    public void setYearIncome(BigDecimal yearIncome) {
        this.yearIncome = yearIncome;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public int getIncomeBeans() {
        return incomeBeans;
    }

    public void setIncomeBeans(int incomeBeans) {
        this.incomeBeans = incomeBeans;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
