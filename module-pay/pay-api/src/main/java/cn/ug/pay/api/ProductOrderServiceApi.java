package cn.ug.pay.api;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.member.bean.MemberDetailBean;
import cn.ug.pay.bean.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 定期投资管理
 * @author kaiwotech
 */
@RequestMapping("productOrder")
public interface ProductOrderServiceApi {

    @GetMapping(value = "/trade/info")
    public SerializeObject<MemberDetailBean> getMemberTradeInfo(@RequestParam("memberId")String memberId);

    @GetMapping(value = "/rfm/list")
    SerializeObject<DataTable<RFMLabelBean>> list(@RequestParam("pageNum")int pageNum,
                                                  @RequestParam("pageSize")int pageSize,
                                                  @RequestParam("startDate")String startDate,
                                                  @RequestParam("endDate")String endDate);

    @RequestMapping(value = "/exchange/gold/amount", method = GET)
    SerializeObject<List<OpenPositionTotalBean>> listExpiredAmountStatistics(@RequestParam("timeType")int timeType,
                                                                             @RequestParam("startDate")String startDate,
                                                                             @RequestParam("endDate")String endDate,
                                                                             @RequestParam("searchType")int searchType);

    @RequestMapping(value = "/gold/record", method = GET)
    SerializeObject<List<OpenPositionTotalBean>> listGoldRecord(@RequestParam("timeType")int timeType,
                                                                @RequestParam("startDate")String startDate,
                                                                @RequestParam("endDate")String endDate);

    @RequestMapping(value = "/transaction/weight", method = GET)
    SerializeObject<List<OpenPositionTotalBean>> listAvgOpenPosition(@RequestParam("payType")int payType,
                                                                     @RequestParam("startDate")String startDate,
                                                                     @RequestParam("endDate")String endDate);

    @RequestMapping(value = "/total/open/position", method = GET)
    SerializeObject<List<OpenPositionTotalBean>> listTotalOpenPosition(@RequestParam("startDate")String startDate,
                                                                       @RequestParam("endDate")String endDate);

    @RequestMapping(value = "/total/expired", method = GET)
    SerializeObject<List<OpenPositionTotalBean>> listTotalExpired(@RequestParam("startDate")String startDate,
                                                                  @RequestParam("endDate")String endDate);

    @RequestMapping(value = "/avg/open/position", method = GET)
    SerializeObject<List<OpenPositionPastBean>> listAvgOpenPosition(@RequestParam("startDate")String startDate,
                                                                    @RequestParam("endDate")String endDate);

    @RequestMapping(value = "/k", method = GET)
    SerializeObject<List<KValueBean>> getKLine(@RequestParam("searchDate")String searchDate);

    @RequestMapping(value = "/total", method = GET)
    SerializeObject<List<TradeTotalAmountBean>> listTotal(@RequestParam("timeType")int timeType,
                                                          @RequestParam("startDate")String startDate,
                                                          @RequestParam("endDate")String endDate,
                                                          @RequestParam("channelIds")String[] channelIds);

    @RequestMapping(value = "/total/trade", method = GET)
    SerializeObject<List<TradeTotalStatisticsBean>> listTradeTotal(@RequestParam("timeType")int timeType,
                                                          @RequestParam("startDate")String startDate,
                                                          @RequestParam("endDate")String endDate,
                                                          @RequestParam("channelIds")String[] channelIds,
                                                          @RequestParam("productType")Integer[] productType);

    @RequestMapping(value = "/avg", method = GET)
    SerializeObject<List<TradeAverageValueBean>> listAvg(@RequestParam("timeType")int timeType,
                                                         @RequestParam("startDate")String startDate,
                                                         @RequestParam("endDate")String endDate,
                                                         @RequestParam("channelIds")String[] channelIds);

    @RequestMapping(value = "/future", method = GET)
    SerializeObject<List<TradeFutureValueBean>> listFuture(@RequestParam("startDate")String startDate,
                                                           @RequestParam("endDate")String endDate,
                                                           @RequestParam("channelIds")String[] channelIds);

    /**
     * 根据ID查找信息
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<ProductOrderBean> find(@PathVariable("id") String id);

    /**
     * 根据流水号, 查找对象
     * @param orderId	流水号
     * @return			记录集
     */
    @RequestMapping(value = "findByOrderId", method = GET)
    SerializeObject<ProductOrderBean> findByOrderId(@RequestParam("orderId") String orderId);

    /**
     *查询会员已购买定期产品数量
     * @param memberId
     * @return
     */
    @RequestMapping(value = "findRegularBuyCount", method = GET)
    SerializeObject<Integer> findRegularBuyCount(@RequestParam("memberId") String memberId);

    /**
     *查询会员已购买定期活动产品数量
     * @param memberId
     * @return
     */
    @RequestMapping(value = "findRegularActivityBuyCount", method = GET)
    SerializeObject<Integer> findRegularActivityBuyCount(@RequestParam("memberId") String memberId);

}
