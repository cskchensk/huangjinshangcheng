package cn.ug.pay.bean.type;

/**
 * 提现状态
 * @author kaiwotech
 */
public enum WithdrawStatus {

    /** 待提现 */
    WAIT(1),
    /** 处理中 */
    PROCESSING(2),
    /** 成功 */
    SUCCESS(3),
    /** 失败 */
    FAIL(4);

    private int result = 1;

    WithdrawStatus(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

}
