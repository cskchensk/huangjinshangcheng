package cn.ug.pay.bean;

import cn.ug.bean.base.BaseBean;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/7/22 0022
 * @time 下午 17:47
 **/
public class TransferAccountsBean extends BaseBean implements Serializable {

    private String memberId;
    /**
     * 转账单号
     */
    private String transferNumbers;

    /**
     * 转账户名
     */
    private String transferName;

    /**
     * 转账开户行
     */
    private String openBank;

    /**
     * 转账账号
     */
    private String bankAccount;

    /**
     * 转账金额
     */
    private BigDecimal amount;

    /**
     * 图片地址
     */
    private String voucherImg;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTransferNumbers() {
        return transferNumbers;
    }

    public void setTransferNumbers(String transferNumbers) {
        this.transferNumbers = transferNumbers;
    }

    public String getTransferName() {
        return transferName;
    }

    public void setTransferName(String transferName) {
        this.transferName = transferName;
    }

    public String getOpenBank() {
        return openBank;
    }

    public void setOpenBank(String openBank) {
        this.openBank = openBank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getVoucherImg() {
        return voucherImg;
    }

    public void setVoucherImg(String voucherImg) {
        this.voucherImg = voucherImg;
    }
}
