package cn.ug.pay.bean;


import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 可用资产详表
 * @author kaiwotech
 */
public class GoldRecordBean extends BaseBean implements java.io.Serializable {

	/** 会员id */
	private String memberId;
	/** T日本金 */
	private BigDecimal principalNowAmount;
	/** T-1日本金 */
	private BigDecimal principalHistoryAmount;
	/** T-1日累计本金 */
	private BigDecimal principalHistoryTotalAmount;
	/** T日利息 */
	private BigDecimal interestNowAmount;
	/** T-1日利息（可用） */
	private BigDecimal interestHistoryAmount;
	/** T-1日累计利息 */
	private BigDecimal interestHistoryTotalAmount;

	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getPrincipalNowAmount() {
		return principalNowAmount;
	}

	public void setPrincipalNowAmount(BigDecimal principalNowAmount) {
		this.principalNowAmount = principalNowAmount;
	}

	public BigDecimal getPrincipalHistoryAmount() {
		return principalHistoryAmount;
	}

	public void setPrincipalHistoryAmount(BigDecimal principalHistoryAmount) {
		this.principalHistoryAmount = principalHistoryAmount;
	}

	public BigDecimal getPrincipalHistoryTotalAmount() {
		return principalHistoryTotalAmount;
	}

	public void setPrincipalHistoryTotalAmount(BigDecimal principalHistoryTotalAmount) {
		this.principalHistoryTotalAmount = principalHistoryTotalAmount;
	}

	public BigDecimal getInterestNowAmount() {
		return interestNowAmount;
	}

	public void setInterestNowAmount(BigDecimal interestNowAmount) {
		this.interestNowAmount = interestNowAmount;
	}

	public BigDecimal getInterestHistoryAmount() {
		return interestHistoryAmount;
	}

	public void setInterestHistoryAmount(BigDecimal interestHistoryAmount) {
		this.interestHistoryAmount = interestHistoryAmount;
	}

	public BigDecimal getInterestHistoryTotalAmount() {
		return interestHistoryTotalAmount;
	}

	public void setInterestHistoryTotalAmount(BigDecimal interestHistoryTotalAmount) {
		this.interestHistoryTotalAmount = interestHistoryTotalAmount;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}
}
