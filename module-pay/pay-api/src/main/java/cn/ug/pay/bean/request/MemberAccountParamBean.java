package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员账户-后台
 * @author ywl
 * @date 2018/2/5
 */
public class MemberAccountParamBean  extends Page implements Serializable {

    /** 用户姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 最低资金总额 **/
    private BigDecimal fundAmountMin;
    /** 最高资金总额 **/
    private BigDecimal fundAmountMax;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;

    /**累计获得体验金 最小值**/
    private String totalGramMin;

    /**累计获得体验金 最大值**/
    private String totalGramMax;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    /**
     * 类型：1  体验金账户  其他账户
     */
    private Integer type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getFundAmountMin() {
        return fundAmountMin;
    }

    public void setFundAmountMin(BigDecimal fundAmountMin) {
        this.fundAmountMin = fundAmountMin;
    }

    public BigDecimal getFundAmountMax() {
        return fundAmountMax;
    }

    public void setFundAmountMax(BigDecimal fundAmountMax) {
        this.fundAmountMax = fundAmountMax;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getTotalGramMin() {
        return totalGramMin;
    }

    public void setTotalGramMin(String totalGramMin) {
        this.totalGramMin = totalGramMin;
    }

    public String getTotalGramMax() {
        return totalGramMax;
    }

    public void setTotalGramMax(String totalGramMax) {
        this.totalGramMax = totalGramMax;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
