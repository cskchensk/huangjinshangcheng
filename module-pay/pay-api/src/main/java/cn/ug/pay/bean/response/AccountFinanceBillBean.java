package cn.ug.pay.bean.response;

import cn.ug.bean.base.BaseBean;
import cn.ug.pay.bean.type.BillType;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountFinanceBillBean extends BaseBean implements Serializable {

    /** 日期 **/
    private String day;
    /** 总额 **/
    private BigDecimal amount;
    /** 当日充值总额 **/
    private BigDecimal rechargeAmount;
    /** 提现总额 **/
    private BigDecimal withdrawAmount;
    /** 上一日总额 **/
    private BigDecimal lastDayAmount;
    /** 账户结余 **/
    private BigDecimal balanceAmount;
    /**  通道方式 1:易宝支付 **/
    private Integer  way;
    private int index;
    private String wayMark;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getWayMark() {
        return wayMark;
    }

    public void setWayMark(String wayMark) {
        this.wayMark = wayMark;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRechargeAmount() {
        return rechargeAmount;
    }

    public void setRechargeAmount(BigDecimal rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }

    public BigDecimal getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(BigDecimal withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public BigDecimal getLastDayAmount() {
        return lastDayAmount;
    }

    public void setLastDayAmount(BigDecimal lastDayAmount) {
        this.lastDayAmount = lastDayAmount;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }
}
