package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

/**
 * 财务对账每日金额统计
 */
public class BillParam extends Page  implements Serializable{

  private String day;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
