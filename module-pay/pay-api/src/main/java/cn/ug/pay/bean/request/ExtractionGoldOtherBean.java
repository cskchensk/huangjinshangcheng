package cn.ug.pay.bean.request;

import java.io.Serializable;

public class ExtractionGoldOtherBean implements Serializable {

    /**
     *  提金订单号
     */
    private String orderNo;

    /**
     * 关闭原因
     */
    private String closeRemark;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 物流编号
     */
    private String logisticsNo;

    /**
     * 物流名称
     */
    private String logisticsName;

    /**
     * 类型 1快递  2门店
     */
    private Integer type;

    /**
     * 状态 1-已预约， 2-待提货， 3-已取消， 4-已完成
     */
    private Integer status;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCloseRemark() {
        return closeRemark;
    }

    public void setCloseRemark(String closeRemark) {
        this.closeRemark = closeRemark;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
