package cn.ug.pay.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *  我绑定的银行卡(移动端)
 *  author ywl
 */
public class BankCardFindBean implements Serializable{

    /** 绑卡Id **/
    private String id;
    /** 银行卡 **/
    private String cardNo;
    /** 银行名称 **/
    private String bankName;
    /** 银行编号 **/
    private String bankCode;
    /** 状态  1:正常 2:隐藏 3:维护中 **/
    private Integer status;
    /** 银行logo **/
    private String img;
    /** 单笔上线金额 **/
    private BigDecimal amountOnce;
    /** 单日上限金额 **/
    private BigDecimal amountDay;
    /** 单月上线金额 **/
    private BigDecimal amountMonth;
    /** 单笔提现上线金额 **/
    private BigDecimal withdrawAmountOnce;
    /** 单日提现上限金额 **/
    private BigDecimal withdrawAmountDay;
    //绑卡时间
    private String addTime;
    /** 单笔是否限额 1：不限 2：限额 **/
    private Integer onceType;
    /** 当日是否限额 1：不限 2：限额 **/
    private Integer dayType;
    /** 当月是否限额 1:不限 2:限额 **/
    private Integer monthType;

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public BigDecimal getWithdrawAmountOnce() {
        return withdrawAmountOnce;
    }

    public void setWithdrawAmountOnce(BigDecimal withdrawAmountOnce) {
        this.withdrawAmountOnce = withdrawAmountOnce;
    }

    public BigDecimal getWithdrawAmountDay() {
        return withdrawAmountDay;
    }

    public void setWithdrawAmountDay(BigDecimal withdrawAmountDay) {
        this.withdrawAmountDay = withdrawAmountDay;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public BigDecimal getAmountOnce() {
        return amountOnce;
    }

    public void setAmountOnce(BigDecimal amountOnce) {
        this.amountOnce = amountOnce;
    }

    public BigDecimal getAmountDay() {
        return amountDay;
    }

    public void setAmountDay(BigDecimal amountDay) {
        this.amountDay = amountDay;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getAmountMonth() {
        return amountMonth;
    }

    public void setAmountMonth(BigDecimal amountMonth) {
        this.amountMonth = amountMonth;
    }

    public Integer getOnceType() {
        return onceType;
    }

    public void setOnceType(Integer onceType) {
        this.onceType = onceType;
    }

    public Integer getDayType() {
        return dayType;
    }

    public void setDayType(Integer dayType) {
        this.dayType = dayType;
    }

    public Integer getMonthType() {
        return monthType;
    }

    public void setMonthType(Integer monthType) {
        this.monthType = monthType;
    }
}
