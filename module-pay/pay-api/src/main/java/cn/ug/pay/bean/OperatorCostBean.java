package cn.ug.pay.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author zhangweijie
 * @Date 2019/4/29 0029
 * @time 上午 10:43
 **/
public class OperatorCostBean {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    int index;
    /**
     * 主键id
     * @mbg.generated
     */
    private Integer id;

    /**
     * 每日线上运营成本总和
     * @mbg.generated
     */
    private BigDecimal operationAllCost;

    /**
     * 每日体验金奖励总成本（元)
     * @mbg.generated
     */
    private BigDecimal experienceIncomeAmount;

    /**
     * 每日黄金红包总成本（元)
     * @mbg.generated
     */
    private BigDecimal goldCouponAllCost;

    /**
     * 每日金豆抵扣总成本（元)
     * @mbg.generated
     */
    private BigDecimal goldBeanAllCost;

    /**
     * 每日回租福利券成本（元）
     * @mbg.generated
     */
    private BigDecimal leaseBackallCost;

    /**
     * 每日商城满减券总成本（元）
     * @mbg.generated
     */
    private Integer shopElectronicCard;

    /**
     * 日期
     * @mbg.generated
     */
    private String date;

    /**
     * 添加时间
     * @mbg.generated
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    /**
     * 更新时间
     * @mbg.generated
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date modifyTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getOperationAllCost() {
        return operationAllCost;
    }

    public void setOperationAllCost(BigDecimal operationAllCost) {
        this.operationAllCost = operationAllCost;
    }

    public BigDecimal getExperienceIncomeAmount() {
        return experienceIncomeAmount;
    }

    public void setExperienceIncomeAmount(BigDecimal experienceIncomeAmount) {
        this.experienceIncomeAmount = experienceIncomeAmount;
    }

    public BigDecimal getGoldCouponAllCost() {
        return goldCouponAllCost;
    }

    public void setGoldCouponAllCost(BigDecimal goldCouponAllCost) {
        this.goldCouponAllCost = goldCouponAllCost;
    }

    public BigDecimal getGoldBeanAllCost() {
        return goldBeanAllCost;
    }

    public void setGoldBeanAllCost(BigDecimal goldBeanAllCost) {
        this.goldBeanAllCost = goldBeanAllCost;
    }

    public BigDecimal getLeaseBackallCost() {
        return leaseBackallCost;
    }

    public void setLeaseBackallCost(BigDecimal leaseBackallCost) {
        this.leaseBackallCost = leaseBackallCost;
    }

    public Integer getShopElectronicCard() {
        return shopElectronicCard;
    }

    public void setShopElectronicCard(Integer shopElectronicCard) {
        this.shopElectronicCard = shopElectronicCard;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
