package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TbillCenterBean implements Serializable {
    private int id;
    private String orderNO;
    private int totalGram;
    private String addTime;
    private int readed;
    private int leasebackDays;
    private String operationNO;
    private int status;
    private String source;
    private String productId;
    private String productName;
    private Integer productType;
    /**
     * 提金类型 1快递 2门店
     */
    private Integer type;

    /**
     * 回租时金价
     */
    private BigDecimal leasebackPrice;

    /**
     * 购买时金价
     */
    private BigDecimal goldPrice;

    /**
     * 支付失败（超时，支付失败）原因
     */
    private String errMsg;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getOperationNO() {
        return operationNO;
    }

    public void setOperationNO(String operationNO) {
        this.operationNO = operationNO;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getReaded() {
        return readed;
    }

    public void setReaded(int readed) {
        this.readed = readed;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getLeasebackPrice() {
        return leasebackPrice;
    }

    public void setLeasebackPrice(BigDecimal leasebackPrice) {
        this.leasebackPrice = leasebackPrice;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }
}
