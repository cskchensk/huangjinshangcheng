package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.pay.bean.AreaTransactionBean;
import cn.ug.pay.bean.response.MemberAccountBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 会员账户
 */
@RequestMapping("memberAccount")
public interface MemberAccountServiceApi {

    @RequestMapping(value = "/area/transaction" , method = GET)
    SerializeObject<List<AreaTransactionBean>> queryTransactionList();

    /**
     * 添加账户
     * @param memberId  会员ID
     * @return			是否操作成功
     */
    @PostMapping(value = "create")
    SerializeObject create(@RequestParam("memberId") String memberId);

    /**
     * 根据会员Id获取
     * @param memberId
     * @return
     */
    @GetMapping(value = "findByMemberId")
    SerializeObject<MemberAccountBean> findByMemberId(@RequestParam("memberId") String memberId);

    /**
     * 邀请返现
     * @param memberId
     * @param amount
     * @return
     */
    @PostMapping(value = "inviteReturnCash")
    public SerializeObject inviteReturnCash(@RequestParam("memberId") String memberId, @RequestParam("amount") BigDecimal amount, @RequestParam("type") Integer type);

    /**
     * 金豆，体验金等赠送
     * @param memberId
     * @param amount
     * @return
     */
    @PostMapping(value = "awardGiveOut")
    SerializeObject awardGiveOut(@RequestParam("memberId") String memberId, @RequestParam("amount") BigDecimal amount, @RequestParam("type") Integer type);

}
