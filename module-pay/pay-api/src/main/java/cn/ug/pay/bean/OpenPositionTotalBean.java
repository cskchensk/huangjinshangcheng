package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class OpenPositionTotalBean implements Serializable{
    private String time;
    private BigDecimal value;
    private BigDecimal totalValue;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(BigDecimal totalValue) {
        this.totalValue = totalValue;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
