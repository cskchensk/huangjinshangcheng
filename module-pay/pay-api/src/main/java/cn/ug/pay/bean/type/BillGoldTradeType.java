package cn.ug.pay.bean.type;

import cn.ug.enums.ProvinceCityEnum;

/**
 * 交易类型
 */
public enum BillGoldTradeType {

    /** 投资活期产品 */
    BUY_DEMAND(1, "购买柚子钱包"),
    /** 定期转活期 */
    TERM_TO_DEMAND(2, "柚生金转柚子钱包"),
    /** 存金 */
    SAVE_GOLD(3, "存金"),
    /** 活期转定期 */
    DEMAND_TO_TERM(4, "柚子钱包转柚生金"),
    /** 提金 */
    EXTRACT_GOLD(5, "提金"),
    /** 卖金 */
    SELL_GOLD(6, "卖金"),
    /** 投资返利 */
    REBATE(7, "投资返利"),
    /**活动黄金转入*/
    ACTIVITY(8, "活动黄金转入"),
    CHANGE_GOLD(9, "换金"),
    /**黄金奖励转入*/
    LOTTERY(10, "黄金奖励转入");

    private int result = 1;
    private String remark;

    BillGoldTradeType(int i, String remark){
        this.result = i;
        this.remark = remark;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

    public static BillGoldTradeType get(int tradeType) {
        for (BillGoldTradeType v : BillGoldTradeType.values()) {
            if (v.getValue() == tradeType) {
                return v;
            }
        }
        return null;
    }

    public static String getRemark(int tradeType) {
        for (BillGoldTradeType v : BillGoldTradeType.values()) {
            if (v.getValue() == tradeType) {
                return v.getRemark();
            }
        }
        return "";
    }

    public String getRemark() {
        return this.remark;
    }
}
