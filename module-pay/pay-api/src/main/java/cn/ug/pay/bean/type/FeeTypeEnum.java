package cn.ug.pay.bean.type;

public enum FeeTypeEnum {
    RECHARGE(1, "充值"),
    WITHDRAWAL(2, "提现"),
    SELL_GOLD(3, "卖金"),
    BUY_GOLD(4, "买金"),
    MALL(5, "商城购买"),
    GOLD_EXTRACTION(6, "提金"),
    CHANGE_GOLD(7, "换金");

    private int result = 1;
    private String remark;

    FeeTypeEnum(int i, String remark){
        this.result = i;
        this.remark = remark;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

    public static String getRemark(int tradeType) {
        for (FeeTypeEnum v : FeeTypeEnum.values()) {
            if (v.getValue() == tradeType) {
                return v.getRemark();
            }
        }
        return "";
    }

    public String getRemark() {
        return this.remark;
    }
}
