package cn.ug.pay.bean.status;

import cn.ug.bean.status.BaseStatus;

/**
 * 提现审核状态
 * @author kaiwotech
 */
public class WithdrawAuditStatus extends BaseStatus {

	/** 未审核 */
	public static final int WAIT 		= 1;
	/** 一级审核成功 */
	public static final int SUCCESS_ONE	= 2;
	/** 一级审核失败 */
	public static final int FAIL_ONE	= 3;
	/** 二级审核成功 */
	public static final int SUCCESS_TWO	= 4;
	/** 二级审核失败 */
	public static final int FAIL_TWO	= 5;

}
