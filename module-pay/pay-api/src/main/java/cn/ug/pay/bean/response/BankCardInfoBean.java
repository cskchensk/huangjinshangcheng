package cn.ug.pay.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 返回银行卡详情
 * @auther ywl
 */
public class BankCardInfoBean  implements Serializable{

    /** 银行编号 **/
    private String bankCode;
    /** 银行名称 **/
    private String bankName;
    /** 卡类型 1:借记卡 2：信用卡**/
    private Integer cardType;
    /** 是否合法 1:合法 2:不合法 **/
    private Integer isvalid;
    /** 错误信息 **/
    private String errorMsg;
    /** 状态  1:正常 2:隐藏 3:维护中 **/
    private Integer status;
    /** 单笔上线金额 **/
    private BigDecimal amountOnce;
    /** 单日上限金额 **/
    private BigDecimal amountDay;


    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getCardType() {
        return cardType;
    }

    public void setCardType(Integer cardType) {
        this.cardType = cardType;
    }

    public Integer getIsvalid() {
        return isvalid;
    }

    public void setIsvalid(Integer isvalid) {
        this.isvalid = isvalid;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public BigDecimal getAmountOnce() {
        return amountOnce;
    }

    public void setAmountOnce(BigDecimal amountOnce) {
        this.amountOnce = amountOnce;
    }

    public BigDecimal getAmountDay() {
        return amountDay;
    }

    public void setAmountDay(BigDecimal amountDay) {
        this.amountDay = amountDay;
    }

    @Override
    public String toString() {
        return "BankCardInfoBean{" +
                "bankCode='" + bankCode + '\'' +
                ", bankName='" + bankName + '\'' +
                ", cardType=" + cardType +
                ", isvalid=" + isvalid +
                ", errorMsg='" + errorMsg + '\'' +
                ", status=" + status +
                ", amountOnce=" + amountOnce +
                ", amountDay=" + amountDay +
                '}';
    }
}
