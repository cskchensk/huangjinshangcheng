package cn.ug.pay.bean.type;

public enum  TbillSourceEnum {
    BUY_GOLD("买金"),
    LEASEBACK("回租到期"),
    REPO("回购");

    private String msg;

    TbillSourceEnum(String msg){
        this.msg = msg;
    }

    public String getValue() {
        return msg;
    }
}
