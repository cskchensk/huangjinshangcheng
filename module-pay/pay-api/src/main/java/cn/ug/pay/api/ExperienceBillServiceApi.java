package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.ExperienceBillBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RequestMapping("experience/record")
public interface ExperienceBillServiceApi {

    @RequestMapping(value = "add",method = POST)
    SerializeObject add(@RequestBody ExperienceBillBean experienceBillBean);
}
