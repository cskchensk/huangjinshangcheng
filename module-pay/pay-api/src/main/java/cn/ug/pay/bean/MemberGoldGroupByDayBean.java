package cn.ug.pay.bean;


import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 会员资产账户（每人每日转入转出统计）
 * @author kaiwotech
 */
public class MemberGoldGroupByDayBean extends BaseBean implements java.io.Serializable {

	/** 会员id */
	private String memberId;
	/** 转入总资产 */
	private BigDecimal turnIntoAmount;
	/** 转出总资产 */
	private BigDecimal turnOutAmount;

	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getTurnIntoAmount() {
		return turnIntoAmount;
	}

	public void setTurnIntoAmount(BigDecimal turnIntoAmount) {
		this.turnIntoAmount = turnIntoAmount;
	}

	public BigDecimal getTurnOutAmount() {
		return turnOutAmount;
	}

	public void setTurnOutAmount(BigDecimal turnOutAmount) {
		this.turnOutAmount = turnOutAmount;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}
}
