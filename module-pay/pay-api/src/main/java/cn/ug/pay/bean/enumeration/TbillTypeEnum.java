package cn.ug.pay.bean.enumeration;

public enum TbillTypeEnum {
    BUY("购买"),
    LEASEBACK("回租"),
    SOLD("出售"),
    GOLD_EXTRACTION("提金"),
    BUYBACK("回购"),
    PAY_ORNAMENT("支付金饰"),
    LEASEBACK_DUE("回租到期");

    private String type;

    TbillTypeEnum(String type) {
        this.type = type;
    }

    public String getValue() {
        return this.type;
    }
}
