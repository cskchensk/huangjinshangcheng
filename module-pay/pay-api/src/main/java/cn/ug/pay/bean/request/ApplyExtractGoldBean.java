package cn.ug.pay.bean.request;

import java.io.Serializable;
import java.math.BigDecimal;

public class ApplyExtractGoldBean implements Serializable {

    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 身份号 **/
    private String idCard;
    /** 金额 **/
    private BigDecimal amount;
    /** 交易密码 **/
    private String payPassword;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
