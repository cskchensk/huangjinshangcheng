package cn.ug.pay.bean.response;

import java.io.Serializable;

/**
 * 活跃用户统计
 */
public class ActiveMemberBean implements Serializable{

    /** 日期 **/
    private String day;
    /**  **/
    private Integer dayNumber;
    /* 买金会员数量* **/
    private Integer buyNumber;
    /** 卖金会员数量 **/
    private Integer sellerNumber;
    /** 数量 **/
    private Integer number;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getBuyNumber() {
        return buyNumber;
    }

    public void setBuyNumber(Integer buyNumber) {
        this.buyNumber = buyNumber;
    }

    public Integer getSellerNumber() {
        return sellerNumber;
    }

    public void setSellerNumber(Integer sellerNumber) {
        this.sellerNumber = sellerNumber;
    }

    public Integer getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(Integer dayNumber) {
        this.dayNumber = dayNumber;
    }
}
