package cn.ug.pay.mq;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 提现消息entity
 * @auther ywl
 */
public class WithdrawMQ  implements Serializable{

    /** 会员Id **/
    private String memberId;
    /** 订单号 **/
    private String orderId;
    /** 绑定银行卡Id **/
    private String bankCardId;
    /** 金额 **/
    private BigDecimal amount;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
