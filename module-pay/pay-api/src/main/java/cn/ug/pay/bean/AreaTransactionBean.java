package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class AreaTransactionBean implements Serializable {
    private String province;
    private int peopleNum;
    private int totalPeopleNum;
    private BigDecimal balance;
    private BigDecimal userProportion;
    private BigDecimal totalCurrentAmount;
    private BigDecimal totalRegularlyAmount;
    private BigDecimal totalMoney;
    private BigDecimal avgMoney;
    private BigDecimal avgCurrentAmount;
    private BigDecimal avgRegularlyAmount;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getPeopleNum() {
        return peopleNum;
    }

    public void setPeopleNum(int peopleNum) {
        this.peopleNum = peopleNum;
    }

    public int getTotalPeopleNum() {
        return totalPeopleNum;
    }

    public void setTotalPeopleNum(int totalPeopleNum) {
        this.totalPeopleNum = totalPeopleNum;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getUserProportion() {
        return userProportion;
    }

    public void setUserProportion(BigDecimal userProportion) {
        this.userProportion = userProportion;
    }

    public BigDecimal getTotalCurrentAmount() {
        return totalCurrentAmount;
    }

    public void setTotalCurrentAmount(BigDecimal totalCurrentAmount) {
        this.totalCurrentAmount = totalCurrentAmount;
    }

    public BigDecimal getTotalRegularlyAmount() {
        return totalRegularlyAmount;
    }

    public void setTotalRegularlyAmount(BigDecimal totalRegularlyAmount) {
        this.totalRegularlyAmount = totalRegularlyAmount;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public BigDecimal getAvgMoney() {
        return avgMoney;
    }

    public void setAvgMoney(BigDecimal avgMoney) {
        this.avgMoney = avgMoney;
    }

    public BigDecimal getAvgCurrentAmount() {
        return avgCurrentAmount;
    }

    public void setAvgCurrentAmount(BigDecimal avgCurrentAmount) {
        this.avgCurrentAmount = avgCurrentAmount;
    }

    public BigDecimal getAvgRegularlyAmount() {
        return avgRegularlyAmount;
    }

    public void setAvgRegularlyAmount(BigDecimal avgRegularlyAmount) {
        this.avgRegularlyAmount = avgRegularlyAmount;
    }
}
