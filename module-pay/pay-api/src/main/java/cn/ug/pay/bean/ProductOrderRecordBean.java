package cn.ug.pay.bean;

import java.math.BigDecimal;

public class ProductOrderRecordBean extends ProductOrderSummaryBean implements java.io.Serializable {

    /** 购买金价 */
    private BigDecimal goldPrice;

    /** 支付金额（商品总价+手续费） */
    private BigDecimal actualAmount;

    /** 手续费 */
    private BigDecimal fee;

    private int        index;

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
