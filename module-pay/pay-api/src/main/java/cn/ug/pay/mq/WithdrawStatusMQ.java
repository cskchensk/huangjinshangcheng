package cn.ug.pay.mq;


/**
 * 提现状态
 * @author kaiwotech
 */
public class WithdrawStatusMQ implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	/** ID */
	private String orderId;
	/** 提现状态  1:待提现 2:处理中 3:成功 4:失败 */
	private Integer status;
	/** 备注 */
	private String description;

	public WithdrawStatusMQ() {
	}

	public WithdrawStatusMQ(String orderId, Integer status) {
		this.orderId = orderId;
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
