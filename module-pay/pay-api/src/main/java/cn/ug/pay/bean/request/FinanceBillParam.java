package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;
import cn.ug.util.Common;

import java.io.Serializable;

public class FinanceBillParam extends Page implements Serializable {

    /** 类型 1:收入 2:支出 **/
    private Integer type;
    /** 交易类型 1:线上充值  2:卖金收入 3:邀请返现 4:账户提现 5:买金支出 6:投资返利 7:红包返现 8:加息奖励  **/
    private String tradeType;
    /** 开始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    //-------------------内部参数------------------------/
    /** 渠道字符串 **/
    private String channelId;
    /** 渠道数组 **/
    private String channelIds [];
    /** 日期类型 1：日 2：月 3:周 **/
    private Integer dayType;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getDayType() {
        return dayType;
    }

    public void setDayType(Integer dayType) {
        this.dayType = dayType;
    }

    public String[] getChannelIds() {
        return channelIds;
    }

    public void setChannelIds(String[] channelIds) {
        this.channelIds = channelIds;
    }
}
