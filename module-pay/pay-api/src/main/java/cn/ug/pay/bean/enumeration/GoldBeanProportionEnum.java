package cn.ug.pay.bean.enumeration;

public enum GoldBeanProportionEnum {
    PROPORTION(10000);

    private int proportion;

    GoldBeanProportionEnum(int proportion) {
        this.proportion = proportion;
    }

    public int getValue() {
        return  this.proportion;
    }
}
