package cn.ug.pay.bean.status;

/**
 * 交易常量
 */
public class TradeStatus {


    /**
     * 支付方式
     */
    public static final class PayWay {
        public static final int yeePay = 1;
        public static final int alipay = 2;
        public static final int wechat = 3;

    }

    /**
     * 是否可以交易
     */
    public static final class IsTrade {
        public static final int YES_TRADE = 1;  //可以交易
        public static final int NO_TRADE = 2;   //不可以交易
    }

    /**
     * 公用常量
     */
    public static final class Flag{
        public static final int YES = 1;
        public static final int NO = 2;
    }
}
