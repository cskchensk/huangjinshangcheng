package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class GoldBeanChangeBean implements Serializable {
    private int index;
    private String memberId;
    private String mobile;
    private String name;
    private int outgoBean;
    private BigDecimal outgoGram;
    private int incomeBean;
    private BigDecimal incomeGram;
    private int usableBean;
    private BigDecimal usableGram;
    private String addTime;

    public BigDecimal getOutgoGram() {
        return outgoGram;
    }

    public void setOutgoGram(BigDecimal outgoGram) {
        this.outgoGram = outgoGram;
    }

    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public BigDecimal getUsableGram() {
        return usableGram;
    }

    public void setUsableGram(BigDecimal usableGram) {
        this.usableGram = usableGram;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOutgoBean() {
        return outgoBean;
    }

    public void setOutgoBean(int outgoBean) {
        this.outgoBean = outgoBean;
    }

    public int getIncomeBean() {
        return incomeBean;
    }

    public void setIncomeBean(int incomeBean) {
        this.incomeBean = incomeBean;
    }

    public int getUsableBean() {
        return usableBean;
    }

    public void setUsableBean(int usableBean) {
        this.usableBean = usableBean;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
