package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class EverydaySellGoldBean implements Serializable {
    private int index;
    private String payDate;
    private BigDecimal payGram;
    private BigDecimal poundage;
    private BigDecimal actualAmount;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPayDate() {
        return payDate;
    }

    public void setPayDate(String payDate) {
        this.payDate = payDate;
    }

    public BigDecimal getPayGram() {
        return payGram;
    }

    public void setPayGram(BigDecimal payGram) {
        this.payGram = payGram;
    }

    public BigDecimal getPoundage() {
        return poundage;
    }

    public void setPoundage(BigDecimal poundage) {
        this.poundage = poundage;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }
}
