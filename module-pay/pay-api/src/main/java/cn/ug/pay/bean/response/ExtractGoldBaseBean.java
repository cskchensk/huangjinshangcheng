package cn.ug.pay.bean.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 提金订单基础实体类
 */
public class ExtractGoldBaseBean extends ExtractionCommonBean implements Serializable {
    /*********************快递提金属性******************************/
    /**
     * 快递费
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private BigDecimal courierFee;

    /**
     * 收货人详细地址
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String address;

    /**
     * 发货时间
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String deliverTime;

    /**
     * 快递json数据
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String data;

    /**
     * 快递名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String logisticsName;

    /**
     * 快递编号
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String logisticsNo;

    /*********************门店提金属性******************************/
    /**
     * 门店名称
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String shopName;

    /**
     * 门店id
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer shopId;

    /**
     * 门店地址
     */
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String shopAddress;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String contactMobile;

    public BigDecimal getCourierFee() {
        return courierFee;
    }

    public void setCourierFee(BigDecimal courierFee) {
        this.courierFee = courierFee;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(String deliverTime) {
        this.deliverTime = deliverTime;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName;
    }

    public String getLogisticsNo() {
        return logisticsNo;
    }

    public void setLogisticsNo(String logisticsNo) {
        this.logisticsNo = logisticsNo;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }
}
