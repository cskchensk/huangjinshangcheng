package cn.ug.pay.bean.status;

import cn.ug.bean.status.BaseStatus;

/**
 * 提现状态
 * @author kaiwotech
 */
public class WithdrawStatus extends BaseStatus {

	/** 待提现 */
	public static final int WAIT 	= 1;
	/** 成功 */
	public static final int SUCCESS	= 2;
	/** 失败 */
	public static final int FAIL	= 3;

}
