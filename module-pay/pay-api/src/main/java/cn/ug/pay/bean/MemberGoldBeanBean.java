package cn.ug.pay.bean;

import java.io.Serializable;

public class MemberGoldBeanBean implements Serializable {
    private int index;
    private String memberId;
    private String mobile;
    private String name;
    private int totalBean;
    private int usableBean;
    private int freezeBean;
    private String addTime;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalBean() {
        return totalBean;
    }

    public void setTotalBean(int totalBean) {
        this.totalBean = totalBean;
    }

    public int getUsableBean() {
        return usableBean;
    }

    public void setUsableBean(int usableBean) {
        this.usableBean = usableBean;
    }

    public int getFreezeBean() {
        return freezeBean;
    }

    public void setFreezeBean(int freezeBean) {
        this.freezeBean = freezeBean;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
