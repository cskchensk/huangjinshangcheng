package cn.ug.pay.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *  银行卡基础信息(移动端)
 *  author ywl
 */
public class BankInfoBean extends BaseBean  implements Serializable {

    /** id **/
    private String id;
    /** 银行编号 **/
    private String bankCode;
    /** 银行名称 **/
    private String bankName;
    /** 状态  1:正常 2:隐藏 3:维护中 **/
    private Integer status;
    /** 渠道 **/
    private Integer way;
    /** 银行logo **/
    private String img;
    /** 单笔上线金额 **/
    private BigDecimal amountOnce;
    /** 单日上限金额 **/
    private BigDecimal amountDay;
    /** 单月上线金额 **/
    private BigDecimal amountMonth;
    /** 银行排序序号 **/
    private Integer sort;
    /** 维护开始时间 **/
    private String startTimeString;
    /** 维护结束时间 **/
    private String endTimeString;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getAmountOnce() {
        return amountOnce;
    }

    public void setAmountOnce(BigDecimal amountOnce) {
        this.amountOnce = amountOnce;
    }

    public BigDecimal getAmountDay() {
        return amountDay;
    }

    public void setAmountDay(BigDecimal amountDay) {
        this.amountDay = amountDay;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public BigDecimal getAmountMonth() {
        return amountMonth;
    }

    public void setAmountMonth(BigDecimal amountMonth) {
        this.amountMonth = amountMonth;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.startTimeString = startTimeString;
    }

    public String getEndTimeString() {
        return endTimeString;
    }

    public void setEndTimeString(String endTimeString) {
        this.endTimeString = endTimeString;
    }
}
