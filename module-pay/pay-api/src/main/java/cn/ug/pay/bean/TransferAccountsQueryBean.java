package cn.ug.pay.bean;

import cn.ug.bean.base.Page;

/**
 * @Author zhangweijie
 * @Date 2019/7/23 0023
 * @time 上午 10:19
 **/
public class TransferAccountsQueryBean extends Page {

    private String  mobile;

    private String  name;

    private Integer status;

    private String  startTime;

    private String  endTime;

    private String  amountMin;

    private String  amountMax;

    /**
     * 1.用户对公转账审核表 财务
     * 2.用户对公转账审核记录表 运营
     */
    private Integer exportType;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAmountMin() {
        return amountMin;
    }

    public void setAmountMin(String amountMin) {
        this.amountMin = amountMin;
    }

    public String getAmountMax() {
        return amountMax;
    }

    public void setAmountMax(String amountMax) {
        this.amountMax = amountMax;
    }

    public Integer getExportType() {
        return exportType;
    }

    public void setExportType(Integer exportType) {
        this.exportType = exportType;
    }
}
