package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 提金参数
 */
public class ExtractGoldParamBean extends Page implements Serializable{

    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 起始克重 **/
    private BigDecimal startAmount;
    /** 结束克重 **/
    private BigDecimal endAmount;
    /** 起始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(BigDecimal startAmount) {
        this.startAmount = startAmount;
    }

    public BigDecimal getEndAmount() {
        return endAmount;
    }

    public void setEndAmount(BigDecimal endAmount) {
        this.endAmount = endAmount;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
