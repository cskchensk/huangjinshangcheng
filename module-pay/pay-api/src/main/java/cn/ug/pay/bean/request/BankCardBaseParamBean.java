package cn.ug.pay.bean.request;

import java.io.Serializable;

/**
 * 绑卡基础信息-移动端
 * @author ywl
 * @date 2018/2/1
 */
public class BankCardBaseParamBean  implements Serializable{

    /** 会员Id **/
    private String memberId;
    /** 持卡姓名 **/
    private String name;
    /** 身份证 **/
    private String idCard;
    /** 银行卡号 **/
    private String cardNo;
    /** 银行预留手机号码  **/
    private String mobile;
    /** 银行编号 **/
    private String bankCode;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
