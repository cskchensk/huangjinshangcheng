package cn.ug.pay.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExtractGoldBean extends BaseBean implements Serializable {

    /** 订单号 **/
    private String orderId;
    /** 会员id **/
    private String memberId;
    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 身份号 **/
    private String idCard;
    /** 金额 **/
    private BigDecimal amount;
    /** 添加时间 */
    private String addTimeString;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String getAddTimeString() {
        return addTimeString;
    }

    @Override
    public void setAddTimeString(String addTimeString) {
        this.addTimeString = addTimeString;
    }
}
