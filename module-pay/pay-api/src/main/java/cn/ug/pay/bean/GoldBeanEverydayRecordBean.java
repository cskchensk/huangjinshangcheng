package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class GoldBeanEverydayRecordBean implements Serializable {
    private int index;
    private int incomeBean;
    private int outgoBean;
    private String tradeDate;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIncomeBean() {
        return incomeBean;
    }

    public void setIncomeBean(int incomeBean) {
        this.incomeBean = incomeBean;
    }

    public int getOutgoBean() {
        return outgoBean;
    }

    public void setOutgoBean(int outgoBean) {
        this.outgoBean = outgoBean;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }
}
