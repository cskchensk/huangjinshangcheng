package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("tbill")
public interface PayTbillServiceApi {

    @RequestMapping(value = "/findPlatformGram" ,method = GET)
    SerializeObject findPlatformTotalGram();
}
