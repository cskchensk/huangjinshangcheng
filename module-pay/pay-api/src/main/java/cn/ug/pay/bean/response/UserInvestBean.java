package cn.ug.pay.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

public class UserInvestBean implements Serializable {

    //用户
    private String loginName;
    //购买金价
    private BigDecimal goldPrice;
    //支付成功时间
    private String successTime;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }
}
