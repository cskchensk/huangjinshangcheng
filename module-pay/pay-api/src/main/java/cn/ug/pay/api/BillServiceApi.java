package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.ActiveMemberBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("bill")
public interface BillServiceApi {

    /**
     * 获取活跃用户
     * @param startTime
     * @return
     */
    @RequestMapping(value = "findBuySellActiveMemberList" ,method = GET)
    SerializeObject<List<ActiveMemberBean>> findBuySellActiveMemberList(@RequestParam("startTime") String startTime,@RequestParam("endTime") String endTime, @RequestParam("channelId") String channelId);
}
