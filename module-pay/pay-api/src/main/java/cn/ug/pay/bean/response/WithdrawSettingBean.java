package cn.ug.pay.bean.response;

import java.math.BigDecimal;

/**
 * 银行提现设置
 */
public class WithdrawSettingBean {

    /** id **/
    private String id;
    /** 银行信息id **/
    private String bankId;
    /** 银行编号 **/
    private String bankCode;
    /** 银行名称 **/
    private String bankName;
    /** 状态  1:正常 2:隐藏 3:维护中 **/
    private Integer status;
    /** 渠道 **/
    private Integer way;
    /** 银行logo **/
    private String img;
    /** 单笔上线金额 **/
    private BigDecimal amountOnce;
    /** 单日上限金额 **/
    private BigDecimal amountDay;
    /** 单月上线金额 **/
    private BigDecimal amountMonth;
    /** 单笔是否限额 1：不限 2：限额 **/
    private Integer onceType;
    /** 当日是否限额 1：不限 2：限额 **/
    private Integer dayType;
    /** 当月是否限额 1:不限 2:限额 **/
    private Integer monthType;
    /** 修改时间 **/
    private String modifyTimeString;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getAmountOnce() {
        return amountOnce;
    }

    public void setAmountOnce(BigDecimal amountOnce) {
        this.amountOnce = amountOnce;
    }

    public BigDecimal getAmountDay() {
        return amountDay;
    }

    public void setAmountDay(BigDecimal amountDay) {
        this.amountDay = amountDay;
    }

    public BigDecimal getAmountMonth() {
        return amountMonth;
    }

    public void setAmountMonth(BigDecimal amountMonth) {
        this.amountMonth = amountMonth;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public Integer getOnceType() {
        return onceType;
    }

    public void setOnceType(Integer onceType) {
        this.onceType = onceType;
    }

    public Integer getDayType() {
        return dayType;
    }

    public void setDayType(Integer dayType) {
        this.dayType = dayType;
    }

    public Integer getMonthType() {
        return monthType;
    }

    public void setMonthType(Integer monthType) {
        this.monthType = monthType;
    }

    public String getModifyTimeString() {
        return modifyTimeString;
    }

    public void setModifyTimeString(String modifyTimeString) {
        this.modifyTimeString = modifyTimeString;
    }
}
