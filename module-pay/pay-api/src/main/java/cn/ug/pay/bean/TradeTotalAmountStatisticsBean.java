package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeTotalAmountStatisticsBean implements Serializable {
    private String time;
    //平台总交易金额
    private BigDecimal platformTotalAmount;
    //购买总交易金额
    private BigDecimal buyTotalAmount;
    //回租总交易金额
    private BigDecimal leasebackTotalAmount;
    //回租总奖励金额
    private BigDecimal leasebackTotalIncomeAmount;
    //出售总交易金额
    private BigDecimal soldTotalAmount;
    //出售总收益金额
    private BigDecimal soldTotalIncomeAmount;
    //营销红包总金额
    private BigDecimal marketingTotalAmount;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getPlatformTotalAmount() {
        return platformTotalAmount;
    }

    public void setPlatformTotalAmount(BigDecimal platformTotalAmount) {
        this.platformTotalAmount = platformTotalAmount;
    }

    public BigDecimal getBuyTotalAmount() {
        return buyTotalAmount;
    }

    public void setBuyTotalAmount(BigDecimal buyTotalAmount) {
        this.buyTotalAmount = buyTotalAmount;
    }

    public BigDecimal getLeasebackTotalAmount() {
        return leasebackTotalAmount;
    }

    public void setLeasebackTotalAmount(BigDecimal leasebackTotalAmount) {
        this.leasebackTotalAmount = leasebackTotalAmount;
    }

    public BigDecimal getLeasebackTotalIncomeAmount() {
        return leasebackTotalIncomeAmount;
    }

    public void setLeasebackTotalIncomeAmount(BigDecimal leasebackTotalIncomeAmount) {
        this.leasebackTotalIncomeAmount = leasebackTotalIncomeAmount;
    }

    public BigDecimal getSoldTotalAmount() {
        return soldTotalAmount;
    }

    public void setSoldTotalAmount(BigDecimal soldTotalAmount) {
        this.soldTotalAmount = soldTotalAmount;
    }

    public BigDecimal getSoldTotalIncomeAmount() {
        return soldTotalIncomeAmount;
    }

    public void setSoldTotalIncomeAmount(BigDecimal soldTotalIncomeAmount) {
        this.soldTotalIncomeAmount = soldTotalIncomeAmount;
    }

    public BigDecimal getMarketingTotalAmount() {
        return marketingTotalAmount;
    }

    public void setMarketingTotalAmount(BigDecimal marketingTotalAmount) {
        this.marketingTotalAmount = marketingTotalAmount;
    }
}
