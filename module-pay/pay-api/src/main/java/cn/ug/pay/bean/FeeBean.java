package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class FeeBean implements Serializable {
    private int index;
    private String mobile;
    private String name;
    private int type;
    private String remark;
    private BigDecimal amount;
    private BigDecimal totalAmount;
    private BigDecimal gram;
    private BigDecimal totalGram;
    private BigDecimal yeepayFee;
    private BigDecimal totalYeepayFee;
    private BigDecimal fee;
    private BigDecimal totalFee;
    private BigDecimal actualFee;
    private BigDecimal totalActualFee;
    private String payTime;

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getTotalYeepayFee() {
        return totalYeepayFee;
    }

    public void setTotalYeepayFee(BigDecimal totalYeepayFee) {
        this.totalYeepayFee = totalYeepayFee;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public BigDecimal getTotalActualFee() {
        return totalActualFee;
    }

    public void setTotalActualFee(BigDecimal totalActualFee) {
        this.totalActualFee = totalActualFee;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getGram() {
        return gram;
    }

    public void setGram(BigDecimal gram) {
        this.gram = gram;
    }

    public BigDecimal getYeepayFee() {
        return yeepayFee;
    }

    public void setYeepayFee(BigDecimal yeepayFee) {
        this.yeepayFee = yeepayFee;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getActualFee() {
        return actualFee;
    }

    public void setActualFee(BigDecimal actualFee) {
        this.actualFee = actualFee;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }
}
