package cn.ug.pay.bean.response;

import java.math.BigDecimal;

public class ProductListBean {
    private String id;

    private String producName;

    private Integer productType;
    /**投资期限**/
    private String investDay;
    /**上线时间**/
    private String upTime;
    /**结束时间**/
    private String stopTime;
    /**年化收益**/
    private BigDecimal yearsIncome;
    /**产品状态**/
    private Integer productStatus;
    /**购买总数**/
    private Integer buyCountNum;

    public String getProducName() {
        return producName;
    }

    public void setProducName(String producName) {
        this.producName = producName;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public String getUpTime() {
        return upTime;
    }

    public void setUpTime(String upTime) {
        this.upTime = upTime;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public Integer getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(Integer productStatus) {
        this.productStatus = productStatus;
    }

    public Integer getBuyCountNum() {
        return buyCountNum;
    }

    public void setBuyCountNum(Integer buyCountNum) {
        this.buyCountNum = buyCountNum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
