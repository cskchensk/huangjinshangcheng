package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.WithdrawAuditBean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 提现管理
 * @author kaiwotech
 */
@RequestMapping("withdraw")
public interface WithdrawServiceApi {

    /**
     * 根据ID查找信息
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<WithdrawAuditBean> find(@PathVariable("id") String id);

}
