package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class BuyGoldRecordBean implements Serializable {
    private int index;
    private String payTime;
    private String mobile;
    private String name;
    private String productName;
    private BigDecimal payGram;
    private BigDecimal goldPrice;
    private BigDecimal payAmount;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPayGram() {
        return payGram;
    }

    public void setPayGram(BigDecimal payGram) {
        this.payGram = payGram;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }
}
