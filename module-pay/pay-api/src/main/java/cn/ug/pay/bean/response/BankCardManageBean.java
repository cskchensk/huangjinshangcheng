package cn.ug.pay.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * 绑卡-后台
 * @author ywl
 * @date 2018/2/1
 */
public class BankCardManageBean extends BaseBean implements Serializable {

    /** 会员姓名 **/
    private String name;
    /** 会员号码 **/
    private String mobile;
    /** 身份证卡号 **/
    private String idCard;
    /** 银行卡号 **/
    private String cardNo;
    /** 银行预留手机号码 **/
    private String reservedMobile;
    /** 银行名称 **/
    private String bankName;
    /** 支付方式  1:易宝支付 **/
    private Integer payWay;

    private int index;

    private String remark;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getReservedMobile() {
        return reservedMobile;
    }

    public void setReservedMobile(String reservedMobile) {
        this.reservedMobile = reservedMobile;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
