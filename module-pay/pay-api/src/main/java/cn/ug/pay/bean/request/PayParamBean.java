package cn.ug.pay.bean.request;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 支付(买金,商城)
 * @author ywl
 */
public class PayParamBean implements Serializable{

    /** 订单号 **/
    private String  orderId;
    /** 绑定银行卡Id **/
    private String bankCardId;
    /** 交易类型 2：买金 3:商城支付 4：提金  5：换金  6：新商城支付 7: 提单支付 **/
    private Integer type;
    /** 支付方式 1：易宝支付 **/
    private Integer way;
    /** 交易密码 **/
    private String payPassword;

    //----------------以下内部参数--------------------
    /** 会员Id **/
    private String memberId;
    /** 标题-购买商品名称 **/
    private String title;
    /** 交易金额 **/
    private BigDecimal amount;
    /** 手续费 --向用户征收手续费**/
    private BigDecimal fee;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
