package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.MemberGoldBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 会员资产相关服务
 * @author kaiwotech
 */
@RequestMapping("memberGold")
public interface MemberGoldServiceApi {


    /**
     * 添加账户
     * @param memberId  会员ID
     * @return			是否操作成功
     */
    @RequestMapping(value = "create", method = POST)
    SerializeObject create(@RequestParam("memberId") String memberId);


    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken       登录成功后分配的Key
     * @param entity            参数集合(只需要传会员ID即可)
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    SerializeObject update(
            @RequestHeader("accessToken") String accessToken,
            @RequestBody MemberGoldBean entity);

    /**
     * 投资返现
     * @param memberId	            会员ID
     * @param amount	            克重
     * @return                      记录集
     */
    @RequestMapping(value = "/gold/give", method = GET)
    public SerializeObject investRebate(
            @RequestParam("memberId") String memberId,
            @RequestParam("amount") BigDecimal amount,
            @RequestParam("tradeType") int tradeType);

}
