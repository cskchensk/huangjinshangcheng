package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class BankInfoParamBean extends Page implements Serializable {

    /**
     * 银行名称
     */
    private String bankName;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
