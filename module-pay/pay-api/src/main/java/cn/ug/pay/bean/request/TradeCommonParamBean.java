package cn.ug.pay.bean.request;

import cn.ug.pay.bean.response.BankCardBean;

import java.math.BigDecimal;

public class TradeCommonParamBean {

    /** 会员Id **/
    private String memberId;
    /** 订单号 **/
    private String orderId;
    /** 金额 **/
    private BigDecimal amount;
    /** 产品名称 **/
    private String title;
    /** 银行卡信息 **/
    private BankCardBean bankCardBean;
    /**　回调地址 **/
    private String callBackUrl;

    //--------------------外部参数-----------------------
    /** 支付金额 **/
    private BigDecimal payAmount;
    /** 交易类型 2：买金 3:商城支付 **/
    private Integer type;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BankCardBean getBankCardBean() {
        return bankCardBean;
    }

    public void setBankCardBean(BankCardBean bankCardBean) {
        this.bankCardBean = bankCardBean;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
