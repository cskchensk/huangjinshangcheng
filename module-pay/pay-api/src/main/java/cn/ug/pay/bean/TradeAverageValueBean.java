package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeAverageValueBean implements Serializable {
    private String time;
    private BigDecimal singleAmount;
    private BigDecimal everyoneAmount;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getSingleAmount() {
        return singleAmount;
    }

    public void setSingleAmount(BigDecimal singleAmount) {
        this.singleAmount = singleAmount;
    }

    public BigDecimal getEveryoneAmount() {
        return everyoneAmount;
    }

    public void setEveryoneAmount(BigDecimal everyoneAmount) {
        this.everyoneAmount = everyoneAmount;
    }
}
