package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class RegularOrderExportBean implements Serializable {
    private int index;
    /** 订单号 */
    private String orderId;
    /** 会员名称 */
    private String memberName;
    /** 会员手机 */
    private String memberMobile;
    /**渠道名称*/
    private String channelName;
    /** 产品名称 */
    private String productName;
    /** 年化收益 **/
    private BigDecimal yearsIncome;
    /** 投资期限 **/
    private String investDay;
    /** 购买克重 */
    private BigDecimal amount;
    /** 购买金价 */
    private BigDecimal goldPrice;
    /** 预期收益 */
    private BigDecimal incomeAmount;
    /** 计息时间 */
    private String startTimeString;
    private String addTimeString;
    /** 到期时间 */
    private String endTimeString;
    /** 支付金额（商品总价+手续费） */
    private BigDecimal actualAmount;
    /** 产品状态  1:未到期 2:已到期 */
    private String status;
    /** 手续费 */
    private BigDecimal fee;
    /** 红包Id */
    private String couponRedId;
    /** 红包返现金额 */
    private String couponRedAmount;
    /** 加息券Id */
    private String couponIncreaseId;
    /** 加息利率% */
    private String couponIncreaseRate;
    /** 加息收益（现金） */
    private String couponIncreaseMoney;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.startTimeString = startTimeString;
    }

    public String getAddTimeString() {
        return addTimeString;
    }

    public void setAddTimeString(String addTimeString) {
        this.addTimeString = addTimeString;
    }

    public String getEndTimeString() {
        return endTimeString;
    }

    public void setEndTimeString(String endTimeString) {
        this.endTimeString = endTimeString;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public String getCouponRedId() {
        return couponRedId;
    }

    public void setCouponRedId(String couponRedId) {
        this.couponRedId = couponRedId;
    }

    public String getCouponRedAmount() {
        return couponRedAmount;
    }

    public void setCouponRedAmount(String couponRedAmount) {
        this.couponRedAmount = couponRedAmount;
    }

    public String getCouponIncreaseId() {
        return couponIncreaseId;
    }

    public void setCouponIncreaseId(String couponIncreaseId) {
        this.couponIncreaseId = couponIncreaseId;
    }

    public String getCouponIncreaseRate() {
        return couponIncreaseRate;
    }

    public void setCouponIncreaseRate(String couponIncreaseRate) {
        this.couponIncreaseRate = couponIncreaseRate;
    }

    public String getCouponIncreaseMoney() {
        return couponIncreaseMoney;
    }

    public void setCouponIncreaseMoney(String couponIncreaseMoney) {
        this.couponIncreaseMoney = couponIncreaseMoney;
    }
}
