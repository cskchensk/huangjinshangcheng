package cn.ug.pay.bean.request;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 会员账户充值交易--移动端
 * @author ywl
 *
 */
public class RechargeParamBean implements Serializable{

    /** 支付方式 1:易宝支付 **/
    private Integer payWay;
    /** 充值金额 **/
    private BigDecimal amount;
    /** 绑定银行账户ID  **/
    private String bankCardId;
    /** 交易密码 **/
    private String payPassword;

    //-------------------一以下是内部参数----------------------/
    /** 会员Id **/
    private String memberId;
    /** 产品名称 **/
    private String title;

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }
}
