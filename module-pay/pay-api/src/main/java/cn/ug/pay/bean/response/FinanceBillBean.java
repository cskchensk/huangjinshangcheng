package cn.ug.pay.bean.response;


import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @auther ywl
 */
public class FinanceBillBean  implements Serializable {

    /** 日期 **/
    private String day;
    /** 类型 1:充值 2:提现 **/
    private Integer type;
    /** 金额 **/
    private BigDecimal amount;
    /** 总数 **/
    private Integer total;
    /** 通道方式 1:易宝支付 2:连连支付  目前一个会员只能绑定一个通道-产品已定义**/
    private Integer way;
    private int index;
    private String remark;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }
}
