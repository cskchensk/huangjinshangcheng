package cn.ug.pay.bean.request;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class ProductRecordBean implements Serializable {

    private int pageNum;

    private int pageSize;
    /**开始时间**/
    @NotEmpty(message = "开始时间不能为空!")
    private String startDate;
    /**结束时间**/
    @NotEmpty(message = "结束时间不能为空!")
    private String endDate;

    /**产品类型 1.新手特权金 2.流动金  3.稳赢金**/
    @Min(value = 1, message = "产品类型不合法")
    @Max(value = 4, message = "产品类型不合法")
    private Integer productType;

    /**时间类型 1.上线时间 2.结束时间**/
    @Min(value = 1, message = "时间类型不合法")
    @Max(value = 2, message = "时间类型不合法")
    @NotNull(message = "时间类型不能为空!")
    private Integer timeType;

    /**产品状态 1.正常 2.作废**/
    @Min(value = 1, message = "产品状态不合法")
    @Max(value = 2, message = "产品状态不合法")
    private Integer productStatus;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(Integer productStatus) {
        this.productStatus = productStatus;
    }
}
