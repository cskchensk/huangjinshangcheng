package cn.ug.pay.bean.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 财务对象明细
 * @auther ywl
 */
public class FinanceBillInfoBean implements Serializable {

    /** 日期 **/
    private String day;
    /** 会员ID **/
    private String memberId;
    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 交易数量 **/
    private Integer total;
    /** 交易金额 **/
    private BigDecimal amount;
    /** 类型 1:充值； 2:提现 **/
    private Integer type;
    /** 通道方式 1:易宝支付 2:连连支付  目前一个会员只能绑定一个通道-产品已定义**/
    private Integer way;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
