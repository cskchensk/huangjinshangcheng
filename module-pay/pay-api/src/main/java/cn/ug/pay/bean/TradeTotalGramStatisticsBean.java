package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeTotalGramStatisticsBean implements Serializable {
    private String time;
    //平台总交易克重
    private BigDecimal platformTotalGram;
    //购买总交易克重
    private Integer buyTotalGram;
    //回租总交易克重
    private Integer leasebackTotalGram;
    //回租总奖励克重
    private Integer leasebackTotalIncomeGram;
    //出售总交易克重
    private Integer soldTotalGram;
    //营销红包总克重
    private BigDecimal marketingTotalGram;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getPlatformTotalGram() {
        return platformTotalGram;
    }

    public void setPlatformTotalGram(BigDecimal platformTotalGram) {
        this.platformTotalGram = platformTotalGram;
    }

    public Integer getBuyTotalGram() {
        return buyTotalGram;
    }

    public void setBuyTotalGram(Integer buyTotalGram) {
        this.buyTotalGram = buyTotalGram;
    }

    public Integer getLeasebackTotalGram() {
        return leasebackTotalGram;
    }

    public void setLeasebackTotalGram(Integer leasebackTotalGram) {
        this.leasebackTotalGram = leasebackTotalGram;
    }

    public Integer getLeasebackTotalIncomeGram() {
        return leasebackTotalIncomeGram;
    }

    public void setLeasebackTotalIncomeGram(Integer leasebackTotalIncomeGram) {
        this.leasebackTotalIncomeGram = leasebackTotalIncomeGram;
    }

    public Integer getSoldTotalGram() {
        return soldTotalGram;
    }

    public void setSoldTotalGram(Integer soldTotalGram) {
        this.soldTotalGram = soldTotalGram;
    }

    public BigDecimal getMarketingTotalGram() {
        return marketingTotalGram;
    }

    public void setMarketingTotalGram(BigDecimal marketingTotalGram) {
        this.marketingTotalGram = marketingTotalGram;
    }
}
