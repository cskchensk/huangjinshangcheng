package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExperienceBillBean implements Serializable {

    private int id;

    private String memberId;

    private String orderNo;
    /**
     * 类型 1:收入 2:支出
     */
    private Integer type;

    /**
     * 操作行为 收入：注册 活动 抽券  支出：购买体验金产品
     * 参考：ExperienceTradeTypeEnum 枚举类
     */
    private Integer tradeType;

    /**
     * 变动克重
     */
    private Integer gram;

    private String addTime;

    private String modifyTime;

    private int index;

    private String typeMark;

    private String tradeTypeMark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTradeType() {
        return tradeType;
    }

    public void setTradeType(Integer tradeType) {
        this.tradeType = tradeType;
    }

    public Integer getGram() {
        return gram;
    }

    public void setGram(Integer gram) {
        this.gram = gram;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTypeMark() {
        return typeMark;
    }

    public void setTypeMark(String typeMark) {
        this.typeMark = typeMark;
    }

    public String getTradeTypeMark() {
        return tradeTypeMark;
    }

    public void setTradeTypeMark(String tradeTypeMark) {
        this.tradeTypeMark = tradeTypeMark;
    }
}
