package cn.ug.pay.bean;


import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 提现
 * @author kaiwotech
 */
public class WithdrawBean extends BaseBean implements java.io.Serializable {

	/** 订单号 */
	private String orderId;
	/** 第三方流水号 */
	private String thirdNumber;
	/** 绑定银行卡ID */
	private String bankCardId;
	/** 会员id */
	private String memberId;
	/** 金额 */
	private BigDecimal amount;
	/** 手续费 */
	private BigDecimal fee;
	/** 实际金额(金额-手续费) */
	private BigDecimal actualAmount;
	/** 提现类型  1: T+0到账 2: T+1到账 */
	private Integer type;
	/** 提现状态  1:待提现 2:处理中 3:成功 4:失败 */
	private Integer status;
	/** 备注 */
	private String description;
	/** 会员提现前余额*/
	private BigDecimal balance;

	/** 支付通道 1:易宝支付 */
	private Integer bankPayWay;
	/** 银行卡号 */
	private String bankCardNo;
	/** 银行编号 **/
	private String bankCode;
	/** 所属银行 */
	private String bankName;
	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getThirdNumber() {
		return thirdNumber;
	}

	public void setThirdNumber(String thirdNumber) {
		this.thirdNumber = thirdNumber;
	}

	public String getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(String bankCardId) {
		this.bankCardId = bankCardId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getBankPayWay() {
		return bankPayWay;
	}

	public void setBankPayWay(Integer bankPayWay) {
		this.bankPayWay = bankPayWay;
	}

	public String getBankCardNo() {
		return bankCardNo;
	}

	public void setBankCardNo(String bankCardNo) {
		this.bankCardNo = bankCardNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}

	public BigDecimal getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
}
