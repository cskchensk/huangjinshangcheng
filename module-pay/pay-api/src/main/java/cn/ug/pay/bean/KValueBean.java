package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class KValueBean implements Serializable {
    private String time;
    private BigDecimal ma1;
    private BigDecimal ma5;
    private BigDecimal ma22;
    private BigDecimal maxAmount;
    private BigDecimal minAmount;
    private BigDecimal openingPrice;
    private BigDecimal closingPrice;

    public KValueBean() {

    }

    public BigDecimal getOpeningPrice() {
        return openingPrice;
    }

    public void setOpeningPrice(BigDecimal openingPrice) {
        this.openingPrice = openingPrice;
    }

    public BigDecimal getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(BigDecimal closingPrice) {
        this.closingPrice = closingPrice;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getMa1() {
        return ma1;
    }

    public void setMa1(BigDecimal ma1) {
        this.ma1 = ma1;
    }

    public BigDecimal getMa5() {
        return ma5;
    }

    public void setMa5(BigDecimal ma5) {
        this.ma5 = ma5;
    }

    public BigDecimal getMa22() {
        return ma22;
    }

    public void setMa22(BigDecimal ma22) {
        this.ma22 = ma22;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }
}
