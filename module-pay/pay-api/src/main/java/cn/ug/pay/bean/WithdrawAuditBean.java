package cn.ug.pay.bean;


/**
 * 提现审核
 * @author kaiwotech
 */
public class WithdrawAuditBean extends WithdrawBean implements java.io.Serializable {

	/** 审核状态  1:未审核 2:一级审核成功 3一级审核失败 4:二级审核成功 5:二级审核失败 */
	private Integer auditStatus;
	/** 一级审核时间 */
	private String auditTimeOneString;
	/** 一级审核人用户Id */
	private String auditUserIdOne;
	/** 一级审核人用户名称 */
	private String auditUserNameOne;
	/** 一级审核描述 */
	private String auditDescriptionOne;
	/** 二级审核时间 */
	private String auditTimeTwoString;
	/** 二级审核人用户Id */
	private String auditUserIdTwo;
	/** 二级审核人用户名称 */
	private String auditUserNameTwo;
	/** 二级审核描述 */
	private String auditDescriptionTwo;
	private String channelName;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Integer getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getAuditTimeOneString() {
		return auditTimeOneString;
	}

	public void setAuditTimeOneString(String auditTimeOneString) {
		this.auditTimeOneString = auditTimeOneString;
	}

	public String getAuditUserIdOne() {
		return auditUserIdOne;
	}

	public void setAuditUserIdOne(String auditUserIdOne) {
		this.auditUserIdOne = auditUserIdOne;
	}

	public String getAuditUserNameOne() {
		return auditUserNameOne;
	}

	public void setAuditUserNameOne(String auditUserNameOne) {
		this.auditUserNameOne = auditUserNameOne;
	}

	public String getAuditDescriptionOne() {
		return auditDescriptionOne;
	}

	public void setAuditDescriptionOne(String auditDescriptionOne) {
		this.auditDescriptionOne = auditDescriptionOne;
	}

	public String getAuditTimeTwoString() {
		return auditTimeTwoString;
	}

	public void setAuditTimeTwoString(String auditTimeTwoString) {
		this.auditTimeTwoString = auditTimeTwoString;
	}

	public String getAuditUserIdTwo() {
		return auditUserIdTwo;
	}

	public void setAuditUserIdTwo(String auditUserIdTwo) {
		this.auditUserIdTwo = auditUserIdTwo;
	}

	public String getAuditUserNameTwo() {
		return auditUserNameTwo;
	}

	public void setAuditUserNameTwo(String auditUserNameTwo) {
		this.auditUserNameTwo = auditUserNameTwo;
	}

	public String getAuditDescriptionTwo() {
		return auditDescriptionTwo;
	}

	public void setAuditDescriptionTwo(String auditDescriptionTwo) {
		this.auditDescriptionTwo = auditDescriptionTwo;
	}
}
