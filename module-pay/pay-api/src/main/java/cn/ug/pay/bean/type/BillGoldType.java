package cn.ug.pay.bean.type;

/**
 * 交易类型
 */
public enum BillGoldType {

    /** 转入 */
    INCOME(1),
    /** 转出 */
    OUTLAY(2);

    private int result = 1;

    BillGoldType(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

}
