package cn.ug.pay.bean.request;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 提金确认-请求实体
 */
public class ExtractionGoldPayBean implements Serializable {
    /*******************内部参数*************************/
    /**
     *  收货人姓名
     */
    private String fullname;

    /**
     *  收货人手机号
     */
    private String cellphone;

    /**
     *  收货人详细地址
     */
    private String address;

    /**
     *  加工费
     */
    private BigDecimal processingFee;

    /**
     *  快递费用
     */
    private BigDecimal courierFee;

    /**
     *  提金手续费
     */
    private BigDecimal fee;

    /**
     *  支付金额  加工费+快递费用+提金手续费(目前没有)
     */
    private BigDecimal actualAmount;

    /**
     *  产品名称
     */
    private String title;

    /**
     * 门店id
     */
    private Integer shopId;

    /*************************公共参数*************************************/
    /**
     *  支付密码
     */
    private String  payPassword;

    /** 绑定银行卡Id **/
    private String bankCardId;

    /** 支付方式 1：易宝支付 **/
    private Integer way;

    /** 交易类型 2：买金 3:商城支付 4：提金  5：换金  6：新商城支付  7.提单 8.快递提金  9.门店提金**/
    private Integer type;
    /**
     *  提单编号
     */
    private String tbillNo;

    public String getTbillNo() {
        return tbillNo;
    }

    public void setTbillNo(String tbillNo) {
        this.tbillNo = tbillNo;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(BigDecimal processingFee) {
        this.processingFee = processingFee;
    }

    public BigDecimal getCourierFee() {
        return courierFee;
    }

    public void setCourierFee(BigDecimal courierFee) {
        this.courierFee = courierFee;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public String getPayPassword() {
        return payPassword;
    }

    public void setPayPassword(String payPassword) {
        this.payPassword = payPassword;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
}
