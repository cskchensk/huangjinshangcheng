package cn.ug.pay.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/7/23 0023
 * @time 下午 16:06
 **/
public class TransferAccountsResultBean {
    /**
     * 用户账号
     */
    private String mobile;
    /**
     * 用户姓名
     */
    private String name;

    /**
     * 身份证号码
     */
    private String idCard;

    /**
     * @mbg.generated
     */
    private String id;

    /**
     *   转账会员Id
     * @mbg.generated
     */
    private String memberId;

    /**
     *   用户转账单号
     * @mbg.generated
     */
    private String transferNumbers;

    /**
     *   转账户名
     * @mbg.generated
     */
    private String transferName;

    /**
     *   转账开户行
     * @mbg.generated
     */
    private String openBank;

    /**
     *   转账账号
     * @mbg.generated
     */
    private String bankAccount;

    /**
     *   转账金额
     * @mbg.generated
     */
    private BigDecimal amount;

    /**
     *   审核状态 1:待审核 2:已通过 3:已拒绝
     * @mbg.generated
     */
    private Integer auditStatus;

    /**
     *   拒绝原因备注
     * @mbg.generated
     */
    private String rejectRemark;

    /**
     *   创建用户ID
     * @mbg.generated
     */
    private String createUserId;

    /**
     *   创建用户姓名
     * @mbg.generated
     */
    private String createUser;

    /**
     *   审核用户ID
     * @mbg.generated
     */
    private String auditUserId;

    /**
     *   审核用户姓名
     * @mbg.generated
     */
    private String auditUser;

    /**
     *   审核时间
     * @mbg.generated
     */
    private String auditTime;

    /**
     *   添加时间
     *
     * @mbg.generated
     */
    private String addTime;

    /**
     *   更新时间
     */
    private String modifyTime;

    /**
     *   凭证单据图片
     * @mbg.generated
     */
    private String voucherImg;

    /**
     * 申请时间
     */
    private String applyTime;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int index;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String statusRemark;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTransferNumbers() {
        return transferNumbers;
    }

    public void setTransferNumbers(String transferNumbers) {
        this.transferNumbers = transferNumbers;
    }

    public String getTransferName() {
        return transferName;
    }

    public void setTransferName(String transferName) {
        this.transferName = transferName;
    }

    public String getOpenBank() {
        return openBank;
    }

    public void setOpenBank(String openBank) {
        this.openBank = openBank;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getRejectRemark() {
        return rejectRemark;
    }

    public void setRejectRemark(String rejectRemark) {
        this.rejectRemark = rejectRemark;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(String auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getAuditUser() {
        return auditUser;
    }

    public void setAuditUser(String auditUser) {
        this.auditUser = auditUser;
    }

    public String getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(String auditTime) {
        this.auditTime = auditTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getVoucherImg() {
        return voucherImg;
    }

    public void setVoucherImg(String voucherImg) {
        this.voucherImg = voucherImg;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getStatusRemark() {
        return statusRemark;
    }

    public void setStatusRemark(String statusRemark) {
        this.statusRemark = statusRemark;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }
}
