package cn.ug.pay.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("bean")
public interface PayGoldBeanRecordServiceApi {

    /** 赠送金豆接口，只支持type=1即金豆收入的功能；
     * sign = DigestUtils.md5Hex(beans+memberId+remark+type+key)
     * key = cn.ug.util.ConstantUtil.NATIVE_PRI_KEY
     * */
    @PostMapping(value = "/present")
    SerializeObject presentBeans(@RequestParam("memberId")String memberId,
                                 @RequestParam("beans")int beans,
                                 @RequestParam("type")int type,
                                 @RequestParam("remark")String remark,
                                 @RequestParam("sign")String sign);
}
