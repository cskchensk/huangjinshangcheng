package cn.ug.pay.bean.type;

/**
 * 交易类型
 */
public enum BillType {

    /** 收入 */
    INCOME(1),
    /** 支出 */
    OUTLAY(2);

    private int result = 1;

    BillType(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

}
