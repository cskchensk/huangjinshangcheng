package cn.ug.pay.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class FinanceBillInfoParam extends Page implements Serializable {

    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 排序字段 */
    private String day;
    /** 排序方式 desc或asc */
    private Integer way;
    /** 类型 1：收入 2：支出 **/
    private Integer type;
    /** 交易类型 1:线上充值  2:卖金收入 3:邀请返现 4:账户提现 5:买金支出 6:投资返利 7:红包返现 8:加息奖励  **/
    private String tradeType;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }
}
