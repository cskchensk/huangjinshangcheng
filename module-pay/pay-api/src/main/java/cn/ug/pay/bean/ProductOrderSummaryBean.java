package cn.ug.pay.bean;


import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 定期投资(简介)
 * @author kaiwotech
 */
public class ProductOrderSummaryBean extends BaseBean implements java.io.Serializable {

	/** 订单号 */
	private String orderId;
	/** 会员id */
	private String memberId;
	/** 产品Id */
	private String productId;
	/** 产品名称 */
	private String productName;
	/** 购买克重 */
	private BigDecimal amount;
	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }
}
