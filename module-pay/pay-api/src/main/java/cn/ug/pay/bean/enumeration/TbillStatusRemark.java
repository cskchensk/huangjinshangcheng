package cn.ug.pay.bean.enumeration;

public enum TbillStatusRemark {
    GOLD_EXTRACTION_CANCEL("取消提金"),
    GOLD_EXTRACTION_FAIL("提金失败"),
    LEASEBACK("继续回租"),
    SOLD("出售成功"),
    GOLD_EXTRACTION_PREBOOK("提金预约"),
    GOLD_EXTRACTION_SUCCESS("提金成功");

    private String remark;

    TbillStatusRemark(String remark) {
        this.remark = remark;
    }

    public String getValue() {
        return this.remark;
    }
}

