package cn.ug.pay.bean.status;

import java.io.Serializable;

public class PayDistributePrefix implements Serializable{

    //账户
    public static final String PAY_MEMBER_ACCOUNT = "payMemberAccount:";
    //订单提交
    public static final String PAY_ORDER_SUBMIT = "payOrderSubmit:";
    //充值
    public static final String PAY_RECHARGE_SUBMIT = "payRechargeSubmit";
    //提金
    public static final String PAY_APPLY_EXTRACT_GOLD = "payApplayExtractGold";
    //实物金提金
    public static final String EXTRACT_GOLD_ORDER_SUBMIT = "ExtractGoldOrderSubmit:";
    //体验金
    public static final String EXPERIENCE_SUBMIT = "experienceSubmit:";
    //大额转账
    public static final String LARGE_MONEY_RECHARGE="largeMoneyRecharge:";
}
