package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class TradeTotalStatisticsBean implements Serializable {
    private String time;
    //购买总交易克重
    private Integer buyTotalGram;
    //购买总交易金额
    private BigDecimal buyTotalAmount;
    //回租总交易克重
    private Integer leasebackTotalGram;
    //回租总交易金额
    private BigDecimal leasebackTotalAmount;
    //回租总奖励金额
    private BigDecimal leasebackTotalIncomeAmount;
    //回租总奖励克重
    private Integer leasebackTotalIncomeGram;
    //出售总交易克重
    private Integer soldTotalGram;
    //出售总交易金额
    private BigDecimal soldTotalAmount;
    //出售总收益金额
    private BigDecimal soldTotalIncomeAmount;
    //营销红包总克重,为小数
    private BigDecimal marketingTotalGram;
    //营销红包总金额
    private BigDecimal marketingTotalAmount;


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getBuyTotalGram() {
        return buyTotalGram;
    }

    public void setBuyTotalGram(Integer buyTotalGram) {
        this.buyTotalGram = buyTotalGram;
    }

    public BigDecimal getBuyTotalAmount() {
        return buyTotalAmount;
    }

    public void setBuyTotalAmount(BigDecimal buyTotalAmount) {
        this.buyTotalAmount = buyTotalAmount;
    }

    public Integer getLeasebackTotalGram() {
        return leasebackTotalGram;
    }

    public void setLeasebackTotalGram(Integer leasebackTotalGram) {
        this.leasebackTotalGram = leasebackTotalGram;
    }

    public BigDecimal getLeasebackTotalAmount() {
        return leasebackTotalAmount;
    }

    public void setLeasebackTotalAmount(BigDecimal leasebackTotalAmount) {
        this.leasebackTotalAmount = leasebackTotalAmount;
    }

    public BigDecimal getLeasebackTotalIncomeAmount() {
        return leasebackTotalIncomeAmount;
    }

    public void setLeasebackTotalIncomeAmount(BigDecimal leasebackTotalIncomeAmount) {
        this.leasebackTotalIncomeAmount = leasebackTotalIncomeAmount;
    }

    public Integer getSoldTotalGram() {
        return soldTotalGram;
    }

    public void setSoldTotalGram(Integer soldTotalGram) {
        this.soldTotalGram = soldTotalGram;
    }

    public BigDecimal getSoldTotalAmount() {
        return soldTotalAmount;
    }

    public void setSoldTotalAmount(BigDecimal soldTotalAmount) {
        this.soldTotalAmount = soldTotalAmount;
    }

    public BigDecimal getSoldTotalIncomeAmount() {
        return soldTotalIncomeAmount;
    }

    public void setSoldTotalIncomeAmount(BigDecimal soldTotalIncomeAmount) {
        this.soldTotalIncomeAmount = soldTotalIncomeAmount;
    }

    public BigDecimal getMarketingTotalGram() {
        return marketingTotalGram;
    }

    public void setMarketingTotalGram(BigDecimal marketingTotalGram) {
        this.marketingTotalGram = marketingTotalGram;
    }

    public BigDecimal getMarketingTotalAmount() {
        return marketingTotalAmount;
    }

    public void setMarketingTotalAmount(BigDecimal marketingTotalAmount) {
        this.marketingTotalAmount = marketingTotalAmount;
    }

    public Integer getLeasebackTotalIncomeGram() {
        return leasebackTotalIncomeGram;
    }

    public void setLeasebackTotalIncomeGram(Integer leasebackTotalIncomeGram) {
        this.leasebackTotalIncomeGram = leasebackTotalIncomeGram;
    }
}
