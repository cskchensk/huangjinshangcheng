package cn.ug.pay.bean.status;

public enum TbillStatusEnum {
    WAIT_FOR_PAYING(0, "待支付"),
    PAST(1, "已过期"),
    PAY_SUCCESS(2, "待处理"),
    LEASEBACK(3, "回租中"),
    FROZEN(4, "提金冻结中"),
    GOLD_EXTRACTION(5, "已提金"),
    SOLD(6, "已出售"),
    FAIL(7, "失败");

    private int status;
    private String remark;

    TbillStatusEnum(int status, String remark) {
        this.status = status;
        this.remark = remark;
    }

    public static String getRemartByStatus(int status) {
        for (TbillStatusEnum v : TbillStatusEnum.values()) {
            if (status == v.getStatus()) {
                return v.getRemark();
            }
        }
        return "";
    }

    public int getStatus() {
        return status;
    }

    public String getRemark() {
        return remark;
    }
}
