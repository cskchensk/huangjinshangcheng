package cn.ug.pay.bean.status;

/**
 * 会员常量
 * @author ywl
 */
public class CommonConstants {

	//易宝支付会员标识
	public static final String IDENTITY_ID = "1111";
	//易宝支付会员标识类型
	public static final String USER_ID = "USER_ID";
	//易宝支付证件类型
	public static final String ID = "ID";
	//易宝支付终端标识码
	public static final String ID_CODE = "100";
	//易宝支付异步返回成功
	public static final String SUCCESS = "SUCCESS";
	//易宝支付异步返回失败
	public static final String FAIL = "fail";
	//易宝支付提现类型
	public static final String DRAWTYPE  = "NATRALDAY_URGENT";
	//易宝支付提现币种
	public static final String CURRENCY = "156";
	//易宝支付用户IP
	public static final String USER_IP = "8.8.8.8";
	//充值
	public static final String RECHARGE = "充值";
	//订单提交时间锁定时间
	public static final Long PAY_ORDER_SUBMIT_LOCK_TIME = 5000l;

	//银行卡绑定状态
	public static enum BankCardStatus{
		NOT_BIND("未绑定", 1),  YES_BIND("已绑定", 2), REMOVE_BIND("已解除绑定",3), FAIL_BIND("绑定失败",4);

		private String name;
		private int index;

		private BankCardStatus(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (BankCardStatus c : BankCardStatus.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}
	//支付方式
	public static enum PayWay{
		YEE_PAY("易宝支付", 1);

		private String name;
		private int index;

		private PayWay(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (PayWay c : PayWay.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}
	//支付状态
	public static enum PayStatus{
		NEW("新建", 0), PROCESSING("处理中", 1), YESPAY("已付款", 2), FAIL("失败", 3),  EXCEPTION("系统异常", 4);

		private String name;
		private int index;

		private PayStatus(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (PayStatus c : PayStatus.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}
	//交易类型
	public static enum PayType{
		RECHARGE("充值", 1), BUYING("买金", 2), MALL("商城", 3);

		private String name;
		private int index;

		private PayType(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (PayType c : PayType.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}
	//卡状态
	public static enum CardStatus{
		NORMAL("正常", 1), HIDE("隐藏", 2), MAINTENANCE("维护中", 3), NO_SUPPROT("不支持", 4);

		private String name;
		private int index;

		private CardStatus(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (CardStatus c : CardStatus.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}
	//模版
	public static enum Template{
		MONDAY("周一模板", 1), TUESDAY("周二模板", 2), WEDNESDAY("周三模板", 3), THURSDAY("周四模板", 4),
		FRIDAY("周五模板", 5), SATURDAY("周六模板", 6), SUNDAY("周日模板", 7);

		private String name;
		private int index;

		private Template(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (Template c : Template.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}

}
