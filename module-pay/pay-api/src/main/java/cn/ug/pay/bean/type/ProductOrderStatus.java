package cn.ug.pay.bean.type;

/**
 * 产品订单状态
 * @author kaiwotech
 */
public enum ProductOrderStatus {

    /** 未到期 */
    NOT_EXPIRED(1),
    /** 已到期 */
    EXPIRED(2);

    private int result = 1;

    ProductOrderStatus(int i){
        this.result = i;
    }

    @Override
    public String toString() {
        return Integer.toString(result);
    }

    public Integer getValue() {
        return result;
    }

}
