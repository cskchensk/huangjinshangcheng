package cn.ug.pay.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.math.BigDecimal;

public class ExtractionCommonBean extends BaseBean implements Serializable {
    /**
     * 提单编号
     */
    private String tbillNo;

    /**
     * 提金订单号
     */
    private String orderNo;

    /**
     * 会员编号
     */
    private String memberId;

    /**
     * 提金手续费
     */
    private BigDecimal fee;

    /**
     * 加工费
     */
    private BigDecimal processingFee;

    /**
     * 实际支付金额
     */
    private BigDecimal actualAmount;

    /**
     * 提金状态（1-已预约， 2-待收货， 3-已取消， 4-已完成）
     */
    private Integer status;

    /**
     * 收货人姓名
     */
    private String fullname;

    /**
     * 收货人手机号
     */
    private String cellphone;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 关闭订单备注信息
     */
    private String closeRemark;

    /**
     * 完成时间
     */
    private String finalTime;

    /**
     * 支付成功时间
     */
    private String successTime;

    /**
     * 订单取消时间
     */
    private String closeTime;

    /**
     * 提金类型  1快递  2门店
     */
    private Integer type;

    /**
     * 是否被阅读 0-未被阅读 1-已被阅读
     */
    private Integer readed;

    public String getTbillNo() {
        return tbillNo;
    }

    public void setTbillNo(String tbillNo) {
        this.tbillNo = tbillNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(BigDecimal processingFee) {
        this.processingFee = processingFee;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCloseRemark() {
        return closeRemark;
    }

    public void setCloseRemark(String closeRemark) {
        this.closeRemark = closeRemark;
    }

    public String getFinalTime() {
        return finalTime;
    }

    public void setFinalTime(String finalTime) {
        this.finalTime = finalTime;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getReaded() {
        return readed;
    }

    public void setReaded(Integer readed) {
        this.readed = readed;
    }
}
