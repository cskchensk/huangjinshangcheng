package cn.ug.pay.bean;

public class ProductOrderExportBean extends ProductOrderBean{

    private int index;

    private String productType;

    private String productStatus;

    private String productOrderYearsIncome;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getProductOrderYearsIncome() {
        return productOrderYearsIncome;
    }

    public void setProductOrderYearsIncome(String productOrderYearsIncome) {
        this.productOrderYearsIncome = productOrderYearsIncome;
    }
}
