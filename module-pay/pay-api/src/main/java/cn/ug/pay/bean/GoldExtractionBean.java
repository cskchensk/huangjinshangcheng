package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class GoldExtractionBean implements Serializable {
    private String orderNO;
    private String addTime;
    // 提金类型：1-快递提金；2-门店提金
    private int type;
    private String successTime;
    // 提金手续费
    private BigDecimal fee;
    // 加工费
    private BigDecimal processingFee;
    // 快递运费
    private BigDecimal courierFee;
    private BigDecimal actualAmount;

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(BigDecimal processingFee) {
        this.processingFee = processingFee;
    }

    public BigDecimal getCourierFee() {
        return courierFee;
    }

    public void setCourierFee(BigDecimal courierFee) {
        this.courierFee = courierFee;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }
}
