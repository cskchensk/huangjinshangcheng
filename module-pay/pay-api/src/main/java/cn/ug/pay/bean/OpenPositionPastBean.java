package cn.ug.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class OpenPositionPastBean implements Serializable {
    private String time;
    private BigDecimal ma1;
    private BigDecimal ma5;
    private BigDecimal ma22;
    private BigDecimal totalMa1;
    private BigDecimal totalMa5;
    private BigDecimal totalMa22;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getMa1() {
        return ma1;
    }

    public void setMa1(BigDecimal ma1) {
        this.ma1 = ma1;
    }

    public BigDecimal getMa5() {
        return ma5;
    }

    public void setMa5(BigDecimal ma5) {
        this.ma5 = ma5;
    }

    public BigDecimal getMa22() {
        return ma22;
    }

    public void setMa22(BigDecimal ma22) {
        this.ma22 = ma22;
    }

    public BigDecimal getTotalMa1() {
        return totalMa1;
    }

    public void setTotalMa1(BigDecimal totalMa1) {
        this.totalMa1 = totalMa1;
    }

    public BigDecimal getTotalMa5() {
        return totalMa5;
    }

    public void setTotalMa5(BigDecimal totalMa5) {
        this.totalMa5 = totalMa5;
    }

    public BigDecimal getTotalMa22() {
        return totalMa22;
    }

    public void setTotalMa22(BigDecimal totalMa22) {
        this.totalMa22 = totalMa22;
    }
}
