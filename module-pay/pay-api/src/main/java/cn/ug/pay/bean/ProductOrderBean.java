package cn.ug.pay.bean;


import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 定期投资
 * @author kaiwotech
 */
public class ProductOrderBean extends BaseBean implements java.io.Serializable {

    /** 订单号 */
    private String orderId;
    /** 会员id */
    private String memberId;
    /** 产品Id */
    private String productId;
    /** 产品名称 */
    private String productName;
    /** 产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品 **/
    private Integer type;
    /** 活动类型 1:是活动 2:不是活动 **/
    private int isActivity;
    /** 年化收益 **/
    private BigDecimal yearsIncome;
    /** 投资期限 **/
    private String investDay;
    /** 购买克重 */
    private BigDecimal amount;
    /** 购买金价 */
    private BigDecimal goldPrice;
    /** 预期收益 */
    private BigDecimal incomeAmount;
    /** 计息时间 */
    private String startTimeString;
    /** 到期时间 */
    private String endTimeString;
    /** 支付类型 1:现金购买 2:金柚宝转入 */
    private Integer payType;
    /** 商品总价 */
    private BigDecimal itemAmount;
    /** 手续费 */
    private BigDecimal fee;
    /** 支付金额（商品总价+手续费） */
    private BigDecimal actualAmount;
    /** T-1日利息 */
    private BigDecimal interestHistoryAmount;
    /** T日本金 */
    private BigDecimal principalNowAmount;
    /** T-1日本金 */
    private BigDecimal principalHistoryAmount;
    /** 支付状态  1:待支付 2:已支付（交易成功） 3:已过期 4：交易失败 */
    private Integer payStatus;
    /** 产品状态  1:未到期 2:已到期 */
    private Integer status;
    /** 支付截止时间 */
    private String stopPayTimeString;
    /** 备注 */
    private String description;
    /** 会员名称 */
    private String memberName;
    /** 会员手机 */
    private String memberMobile;
    /**渠道名称*/
    private String channelName;

    /** 购买类型 1:按克重 2:按金额 **/
    private Integer buyType;
    /** 支付截止时间 */
    private Long stopPayTimeSecond = 0L;

    /** 待支付时间 */
    private Integer unpaidTimeMinute;

    /** 红包等费用信息 */
    private ProductOrderFeeBean productOrderFeeBean;

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getYearsIncome() {
        return yearsIncome;
    }

    public void setYearsIncome(BigDecimal yearsIncome) {
        this.yearsIncome = yearsIncome;
    }

    public String getInvestDay() {
        return investDay;
    }

    public void setInvestDay(String investDay) {
        this.investDay = investDay;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.startTimeString = startTimeString;
    }

    public String getEndTimeString() {
        return endTimeString;
    }

    public void setEndTimeString(String endTimeString) {
        this.endTimeString = endTimeString;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public BigDecimal getInterestHistoryAmount() {
        return interestHistoryAmount;
    }

    public void setInterestHistoryAmount(BigDecimal interestHistoryAmount) {
        this.interestHistoryAmount = interestHistoryAmount;
    }

    public BigDecimal getPrincipalNowAmount() {
        return principalNowAmount;
    }

    public void setPrincipalNowAmount(BigDecimal principalNowAmount) {
        this.principalNowAmount = principalNowAmount;
    }

    public BigDecimal getPrincipalHistoryAmount() {
        return principalHistoryAmount;
    }

    public void setPrincipalHistoryAmount(BigDecimal principalHistoryAmount) {
        this.principalHistoryAmount = principalHistoryAmount;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStopPayTimeString() {
        return stopPayTimeString;
    }

    public void setStopPayTimeString(String stopPayTimeString) {
        this.stopPayTimeString = stopPayTimeString;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }

    public Integer getBuyType() {
        return buyType;
    }

    public void setBuyType(Integer buyType) {
        this.buyType = buyType;
    }

    public long getStopPayTimeSecond() {
        return stopPayTimeSecond;
    }

    public void setStopPayTimeSecond(long stopPayTimeSecond) {
        this.stopPayTimeSecond = stopPayTimeSecond;
    }

    public void setStopPayTimeSecond(Long stopPayTimeSecond) {
        this.stopPayTimeSecond = stopPayTimeSecond;
    }

    public ProductOrderFeeBean getProductOrderFeeBean() {
        return productOrderFeeBean;
    }

    public void setProductOrderFeeBean(ProductOrderFeeBean productOrderFeeBean) {
        this.productOrderFeeBean = productOrderFeeBean;
    }

    public int getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(int isActivity) {
        this.isActivity = isActivity;
    }

    public Integer getUnpaidTimeMinute() {
        return unpaidTimeMinute;
    }

    public void setUnpaidTimeMinute(Integer unpaidTimeMinute) {
        this.unpaidTimeMinute = unpaidTimeMinute;
    }
}
