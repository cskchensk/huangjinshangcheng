package cn.ug.pay.bean.response;

import java.io.Serializable;

public class ExtractGoldMyBean implements Serializable{

    /**
     * 提金订单号
     */
    private String orderNo;

    /**
     * 提金类型  1快递 2门店
     */
    private Integer type;

    /**
     * 提金申请时间
     */
    private String  addTime;

    private Integer status;

    /**
     * 是否被阅读 0-未被阅读 1-已被阅读
     */
    private Integer readed;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getReaded() {
        return readed;
    }

    public void setReaded(Integer readed) {
        this.readed = readed;
    }
}
