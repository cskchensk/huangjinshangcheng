package cn.ug.pay.bean.response;

import java.math.BigDecimal;

public class ExtractGoldFindBean extends ExtractGoldBaseBean {
    /**
     * 商品克重
     */
    private BigDecimal gram;

    /**
     * 商品名称
     */
    private String  productName;

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 会员账号
     */
    private String loginName;

    /**
     * 产品图片
     */
    private String productImgUrl;

    /**
     * 产品id
     */
    private String productId;

    private int index;

    private String statusMark;

    public BigDecimal getGram() {
        return gram;
    }

    public void setGram(BigDecimal gram) {
        this.gram = gram;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getProductImgUrl() {
        return productImgUrl;
    }

    public void setProductImgUrl(String productImgUrl) {
        this.productImgUrl = productImgUrl;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getStatusMark() {
        return statusMark;
    }

    public void setStatusMark(String statusMark) {
        this.statusMark = statusMark;
    }
}
