package cn.ug;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.crypto.Cipher;
import java.math.BigDecimal;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Base64;


import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;


public class MainTests {
	private static Log log = LogFactory.getLog(MainTests.class);

	public static void main(String[] args) throws Exception {
		/*String yearMonth = UF.getFormatDateTime("yyyy-MM-01", UF.getDateTime());
		LocalDateTime addTimeMin = UF.getDate(yearMonth);
		LocalDateTime addTimeMax = addTimeMin.plusMonths(1);

		System.out.println(yearMonth);
		System.out.println(UF.getFormatDateTime(addTimeMin));
		System.out.println(UF.getFormatDateTime(addTimeMax));

		System.out.println("=================================");
		System.out.println(UF.getFormatDate("yyyy-MM-dd HH:mm:ss", UF.getDate("2018-03-02")));

		System.out.println("=================================");
		System.out.println(UF.digitUppercase(99999999999.99D));*/

		/*LocalDateTime a = UF.getDateTime();
		LocalDateTime b = a.plusSeconds(22);

		System.out.println(Duration.between(a, b).getSeconds());*/

//		System.out.println(BigDecimalUtil.to5Point(new BigDecimal("5.12")).toString());
		/*LocalDateTime localDateTime = UF.getDateTime("2018-03-21");
		System.out.println(UF.getFormatDateTime(localDateTime));
		System.out.println(UF.getFormatDateTime(localDateTime.plusDays(1).minusSeconds(1)));*/

		/*String publicKeyString = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzezbnATHWVexa9O4smi0pICYZTIfjswC/M7X9wXwUwXt5u4075rpHmCrnuMQ6GHy5+9CdI9vj/aNHJb5mHiiYoulR/3Wmz8Ainl9s8ho0T6m/MSpyiIY/U7wadM1mcKkPEqvvNXCkxOAGG6WYM9nKYO4pW4fnn8FK7RSbBAQjgnI6hIatOzBn21dMi4c/Lsb9lLwzyS0Z/uDVfnSJ7raykHRJDDjARyPyLdLGaYb/nlbymFt4Qbugy1fLwlwawDrmmhnM24B6sQmHMZ/1jxa645YUt44few/uXxRvz2/w6U+z5cLUNIDUz5poO00xCQOHQ9MaTEK4dePgRdrzakWowIDAQAB";
		String privateKeyString = "MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDN7NucBMdZV7Fr07iyaLSkgJhlMh+OzAL8ztf3BfBTBe3m7jTvmukeYKue4xDoYfLn70J0j2+P9o0clvmYeKJii6VH/dabPwCKeX2zyGjRPqb8xKnKIhj9TvBp0zWZwqQ8Sq+81cKTE4AYbpZgz2cpg7ilbh+efwUrtFJsEBCOCcjqEhq07MGfbV0yLhz8uxv2UvDPJLRn+4NV+dInutrKQdEkMOMBHI/It0sZphv+eVvKYW3hBu6DLV8vCXBrAOuaaGczbgHqxCYcxn/WPFrrjlhS3jh97D+5fFG/Pb/DpT7PlwtQ0gNTPmmg7TTEJA4dD0xpMQrh14+BF2vNqRajAgMBAAECggEBAKcJz+ZR9gbh3Hhz6KlHx+h8q6HCs9iYYoABpSuqtJQUBjPB3EzUIu9SSUR8OhXWhjxfV0/Hnh5wR2z+hn6ItGgtkKfCH1gVFfI9tJqmIWhCYyA8wuHFIPIxG1EsU5ljSufhSYZq+wdk1Q58fh0PgEYrpBcFKj7eGCqofgdMTLF5hjrHRMYq6D5lIEXfhZDTJ9al5swVgKLDsQ2WVzulBC8c+OFqW9nPDlWwKxXFqPYStbXAa1YVLqamGNkmVbC4Q/rAkL+KQQMWeJ7+H2q1NRWo25ez+3kfbofF7lpwflZFAusQCdvhopN/ZWMNeBc7OHGt1Qdkz2Y1ENZRGQuaiIECgYEA/MEunxGKngFNkEl2UHcgrjFr8srsuM6dYcg3cv1CEMDBsYgA27V+gZpTC9de49D6NGuHwbXEfPgf7EjLUVLERP2Oqc9RS7f+nxRL74yPl0eM6Ktn0oS1Eax9S/TXiBdsUpBx/hATiZ0O2SGkPleb4HsljT8OqHGAfqR3R0WvrkECgYEA0JG+ulqd8QxK9sGbYo8CZfqAMT1alrnsk4M8QSLaN+w51yTI3kfLVxmDgsS+GjslRICfBf3AADvd3mLlulG1+bqrsiG8BbEjonIknRB1S3bTiHObE94g4+yCGMRXkJagsUPsBrpr1pV7xDRcQz5DNhu8XIpze//u3Js4junC0+MCgYEAmK2JIAVUmoVm+RWnJowPbank71CtFiWY5AwxkoBxe/U0C61/9zf3Jwq1x51dcHg365pY8yZ9fo/TmMOQLZu1e4c+ukzFatvGLwnvpNBfnJ5X++QuXvhS+E4dkdhgqZCcmnMuqK3FX4dEBN1RfWcmfb9lu6CvlgiZ+eYXNzJdIQECgYEAhTsUouYPzlMT79xs7eB8n7gM+2c93tqVjQVgEBu7Wc3EhPIyfgtOkSzzHS2gYhE2FTQ0YRde9sdHSy/Rn9tia0wbMYbrHJezM6nGSa6hvrsBFUED5wUV7YK++CMz7GDL9XB17TZYfJKwpPztkubK+F1vTY4Dh0pNww5EasZYdhECgYEA4hVko/htRMy27lg9DrDLsF/nc0mudWcYiv1OWkQelmeMsACUCHuDm5s2sQouU913aUmoO5E0+DCFppq325bkJQrFDw1FcGufI6YP5Db43bbpiwEoRXEH4NCkfZesSV9xQaxuhCV+G7U+uw63fJGiODZdvyF5Qwi9SF2ZgSg4wDY=";

		String data = "hello world";

		//获取公钥
		PublicKey publicKey=getPublicKey(publicKeyString);

		//获取私钥
		PrivateKey privateKey=getPrivateKey(privateKeyString);

		//公钥加密
		byte[] encryptedBytes = encrypt(data.getBytes(),  publicKey);
		System.out.println("加密后：" + new String(encryptedBytes));

		//私钥解密
		byte[] decryptedBytes = decrypt(encryptedBytes, privateKey);
		System.out.println("解密后：" + new String(decryptedBytes));*/

		List<Integer> list1 = new ArrayList<Integer>();
		list1.add(1);
		list1.add(2);
		list1.add(3);
		list1.add(4);

		List<Integer> list2 = new ArrayList<Integer>();
		list2.add(3);
		list2.add(4);
		list2.add(5);
		list2.add(6);

		// 取交集[3, 4]
		Collection<Integer> interColl = CollectionUtils.intersection(list1, list2);
		System.out.println(interColl);// 打印出[3, 4]

		// 取并集[1, 2, 3, 4, 5, 6]
		Collection<Integer> unionColl = CollectionUtils.union(list1, list2);
		System.out.println(unionColl);// 打印出[1, 2, 3, 4, 5, 6]

		// 取差集[1,2]
		Collection<Integer> disColl = CollectionUtils.disjunction(list1, interColl);
		System.out.println(disColl);// 打印出[1, 2]
	}
	//公钥加密
	/*public static byte[] encrypt(byte[] content, PublicKey publicKey) throws Exception{
		Cipher cipher=Cipher.getInstance("RSA");//java默认"RSA"="RSA/ECB/PKCS1Padding"
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return cipher.doFinal(content);
	}

	//私钥解密
	public static byte[] decrypt(byte[] content, PrivateKey privateKey) throws Exception{
		Cipher cipher=Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(content);
	}
	//将base64编码后的公钥字符串转成PublicKey实例
	public static PublicKey getPublicKey(String publicKey) throws Exception{
		byte[ ] keyBytes= Base64.getDecoder().decode(publicKey.getBytes());
		X509EncodedKeySpec keySpec=new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory=KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(keySpec);
	}

	//将base64编码后的私钥字符串转成PrivateKey实例
	public static PrivateKey getPrivateKey(String privateKey) throws Exception{
		byte[ ] keyBytes=Base64.getDecoder().decode(privateKey.getBytes());
		PKCS8EncodedKeySpec keySpec=new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory=KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}*/



}
