package cn.ug;

import cn.ug.config.RedisGlobalLock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisGlobalLockTests {

	@Resource
	private RedisGlobalLock redisGlobalLock;

	@Test
	public void contextLoads() {
		/*String key = "lock:info:add";
		// 直接获取锁，如果失败马上返回。
		boolean isOk = redisGlobalLock.lock(key, 1, TimeUnit.DAYS);
		System.out.println("isOk = " + isOk);*/

		/*String key = "lock:info:add";
		// 直接获取锁，如果失败马上返回。
		boolean isOk = redisGlobalLock.lock(key);
		// 指定业务锁有效期
		isOk = redisGlobalLock.lock(key, 1, TimeUnit.DAYS);
		// 尝试获取锁，如果失败等待20S，每30ms尝试一次，直到获取锁或超时返回
		isOk = redisGlobalLock.tryLock(key);
		// 指定超时时间
		isOk = redisGlobalLock.tryLock(key, 10, TimeUnit.SECONDS);
		// 指定超时时间和业务锁有效期
		isOk = redisGlobalLock.tryLock(key, 10, TimeUnit.SECONDS, 1, TimeUnit.DAYS);
		// 释放锁
		redisGlobalLock.unlock(key);*/
	}

}
