package cn.ug.config;

import cn.ug.mall.bean.GoldPrivilegeBean;
import cn.ug.mall.bean.PrivilegeTemplateBean;
import cn.ug.mall.bean.TemplateBean;
import cn.ug.util.BigDecimalUtil;
import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static java.math.BigDecimal.ROUND_HALF_UP;

/**
 * 资金费率配置
 * @author kaiwotech
 */
public class FundRateConfig implements Serializable{

	/**
	 * 活期利息发放方式
	 */
	public enum TnterestPaymentMethod {

		/** 资产账户 */
		GOLD_ACCOUNT(1),
		/** 资金账户 */
		FUND_ACCOUNT(2);

		private int result = 1;

		TnterestPaymentMethod(int i){
			this.result = i;
		}

		@Override
		public String toString() {
			return Integer.toString(result);
		}

		public Integer getValue() {
			return result;
		}
	}

	/**
	 * 黄金买入手续费类型
	 */
	public enum GoldBuyFeeType {

		/** 启用/有 */
		ENABLE(1),
		/** 禁用/无 */
		DISABLE(2);

		private int result = 1;

		GoldBuyFeeType(int i){
			this.result = i;
		}

		@Override
		public String toString() {
			return Integer.toString(result);
		}

		public Integer getValue() {
			return result;
		}
	}

	/**
	 * 活期金计息方式
	 */
	public enum InterestCalculateMethod {

		/** 单利 */
		SIMPLE(1),
		/** 复利 */
		COMPOUND(2);

		private int result = 1;

		InterestCalculateMethod(int i){
			this.result = i;
		}

		@Override
		public String toString() {
			return Integer.toString(result);
		}

		public Integer getValue() {
			return result;
		}
	}

	/**
	 * 提现手续费收费类型
	 */
	public enum WithdrawFeeType {

		/** 每笔按固定费用收费 */
		FIXED(1),
		/** 每笔按费率扣除 */
		RATE(2);

		private int result = 1;

		WithdrawFeeType(int i){
			this.result = i;
		}

		@Override
		public String toString() {
			return Integer.toString(result);
		}

		public Integer getValue() {
			return result;
		}
	}

	private static final long serialVersionUID 	= 1L;

	/** 买入日期 */
	private String buyDate								= "T";
	/** 计息日期 */
	private String interestRateDate 					= "T+1";
	/** 收益到账日 */
	private String interestArrivalDate 				= "T+2";
	/** 卖出日期 */
	private String sellDate 							= "T+2";
	/** 活期卖出计息截止日期 */
	private String sellInterestCutoffDate 				= "-1";
	/** 活期利息发放方式 1、活期金账户 2、资金账户余额 */
	private Integer interestPaymentMethod 				= TnterestPaymentMethod.GOLD_ACCOUNT.getValue();
	/** 黄金买入手续费类型 1：有 2：无 */
	private Integer goldBuyFeeType 						= GoldBuyFeeType.ENABLE.getValue();
	/** 黄金买入手续费 */
	private BigDecimal goldBuyFee 						= BigDecimal.ZERO;
	/** 活期金计息方式 1：单利计息 2：复利计息 */
	private Integer interestCalculateMethod 			= InterestCalculateMethod.SIMPLE.getValue();
	/** 活期金利息结算时间 */
	private String interestCalculateTime 				= "01:00";
	/** 每人每日最高购买克重(克) */
	private BigDecimal maxBuyWeightPerDay 				= new BigDecimal("2000");
	/** 每人每日最高卖出克重(克) */
	private BigDecimal maxSellWeightPerDay 			= new BigDecimal("500");
	/** 活期金年化收益(%) */
	private BigDecimal currentGoldAnnualIncome 		= new BigDecimal("3.75");
	/** 每笔最大充值 */
	private BigDecimal maxRechargeEach 				= new BigDecimal("10000000.00");

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getBuyDate() {
		return buyDate;
	}

	public void setBuyDate(String buyDate) {
		this.buyDate = buyDate;
	}

	public String getInterestRateDate() {
		return interestRateDate;
	}

	public void setInterestRateDate(String interestRateDate) {
		this.interestRateDate = interestRateDate;
	}

	public String getInterestArrivalDate() {
		return interestArrivalDate;
	}

	public void setInterestArrivalDate(String interestArrivalDate) {
		this.interestArrivalDate = interestArrivalDate;
	}

	public String getSellDate() {
		return sellDate;
	}

	public void setSellDate(String sellDate) {
		this.sellDate = sellDate;
	}

	public String getSellInterestCutoffDate() {
		return sellInterestCutoffDate;
	}

	public void setSellInterestCutoffDate(String sellInterestCutoffDate) {
		this.sellInterestCutoffDate = sellInterestCutoffDate;
	}

	public Integer getInterestPaymentMethod() {
		return interestPaymentMethod;
	}

	public void setInterestPaymentMethod(Integer interestPaymentMethod) {
		this.interestPaymentMethod = interestPaymentMethod;
	}

	public Integer getGoldBuyFeeType() {
		return goldBuyFeeType;
	}

	public void setGoldBuyFeeType(Integer goldBuyFeeType) {
		this.goldBuyFeeType = goldBuyFeeType;
	}

	public BigDecimal getGoldBuyFee() {
		return goldBuyFee;
	}

	public void setGoldBuyFee(BigDecimal goldBuyFee) {
		this.goldBuyFee = goldBuyFee;
	}

	public Integer getInterestCalculateMethod() {
		return interestCalculateMethod;
	}

	public void setInterestCalculateMethod(Integer interestCalculateMethod) {
		this.interestCalculateMethod = interestCalculateMethod;
	}

	public String getInterestCalculateTime() {
		return interestCalculateTime;
	}

	public void setInterestCalculateTime(String interestCalculateTime) {
		this.interestCalculateTime = interestCalculateTime;
	}

	public BigDecimal getMaxBuyWeightPerDay() {
		return maxBuyWeightPerDay;
	}

	public void setMaxBuyWeightPerDay(BigDecimal maxBuyWeightPerDay) {
		this.maxBuyWeightPerDay = maxBuyWeightPerDay;
	}

	public BigDecimal getMaxSellWeightPerDay() {
		return maxSellWeightPerDay;
	}

	public void setMaxSellWeightPerDay(BigDecimal maxSellWeightPerDay) {
		this.maxSellWeightPerDay = maxSellWeightPerDay;
	}

	public BigDecimal getCurrentGoldAnnualIncome() {
		return currentGoldAnnualIncome;
	}

	public void setCurrentGoldAnnualIncome(BigDecimal currentGoldAnnualIncome) {
		this.currentGoldAnnualIncome = currentGoldAnnualIncome;
	}

	public BigDecimal getMaxRechargeEach() {
		return maxRechargeEach;
	}

	public void setMaxRechargeEach(BigDecimal maxRechargeEach) {
		this.maxRechargeEach = maxRechargeEach;
	}

	public BigDecimal getFee(JSONObject json, BigDecimal payAmount, int dayNum, int monthNum) {
		BigDecimal fee = BigDecimal.ZERO;
		if (json == null) {
			return fee;
		}
		int isFee = json.getIntValue("isFee");
		if (isFee == 1) {
			int feeWay = json.getInteger("feeWay");
			int feePeriod = json.getInteger("feePeriod");
			int beforeNum = json.getInteger("beforeNum");
			int num = 0;
			if (feePeriod == 1) {
				num = monthNum;
			} else {
				num = dayNum;
			}
			if (num < beforeNum) {
				fee = json.getBigDecimal("beforeAmount");
			} else {
				fee = json.getBigDecimal("afterAmount");
			}
			BigDecimal discount = json.getBigDecimal("discount");
			int hasDiscount= json.getIntValue("hasDiscount");
			if (feeWay == 0) {
				if (hasDiscount == 1) {
					fee = BigDecimalUtil.multiply(fee, discount);
					fee = fee.divide(new BigDecimal("100"), 10, ROUND_HALF_UP);
				}
			} else {
				fee = BigDecimalUtil.multiply(payAmount, fee);
				fee = fee.divide(new BigDecimal("1000"), 10, ROUND_HALF_UP);
				if (hasDiscount == 1) {
					fee = BigDecimalUtil.multiply(fee, discount);
					fee = fee.divide(new BigDecimal("100"), 10, ROUND_HALF_UP);
				}
			}
			fee = BigDecimalUtil.to2Point(fee);
		}
		return fee;
	}

	public BigDecimal getSellGoldFee(JSONObject json, BigDecimal payAmount, BigDecimal gram, int dayNum, int monthNum) {
		BigDecimal fee = BigDecimal.ZERO;
		if (json == null) {
			return fee;
		}
		int isFee = json.getIntValue("isFee");
		if (isFee == 1) {
			int feeWay = json.getInteger("feeWay");
			int feePeriod = json.getInteger("feePeriod");
			int beforeNum = json.getInteger("beforeNum");
			int num = 0;
			if (feePeriod == 1) {
				num = monthNum;
			} else {
				num = dayNum;
			}
			if (num < beforeNum) {
				fee = json.getBigDecimal("beforeAmount");
			} else {
				fee = json.getBigDecimal("afterAmount");
			}
			BigDecimal discount = json.getBigDecimal("discount");
			int hasDiscount= json.getIntValue("hasDiscount");
			if (feeWay == 0) {
				fee = BigDecimalUtil.multiply(gram, fee);
				if (hasDiscount == 1) {
					fee = BigDecimalUtil.multiply(fee, discount);
					fee = fee.divide(new BigDecimal("100"), 10, ROUND_HALF_UP);
				}
			} else {
				fee = BigDecimalUtil.multiply(payAmount, fee);
				fee = fee.divide(new BigDecimal("1000"), 10, ROUND_HALF_UP);
				if (hasDiscount == 1) {
					fee = BigDecimalUtil.multiply(fee, discount);
					fee = fee.divide(new BigDecimal("100"), 10, ROUND_HALF_UP);
				}
			}
			fee = BigDecimalUtil.to2Point(fee);
		}
		return fee;
	}

	public BigDecimal getFirstAmount(GoldPrivilegeBean bean, int totalGram) {
		BigDecimal discountAmount = BigDecimal.ZERO;
		if (bean.getType() != null && bean.getType() == 2) {
			//现金优惠
			if (bean.getPrivilegeType() != null && bean.getPrivilegeType() == 1) {
				if (bean.getSelectType() != null && bean.getSelectType() == 1) {
					discountAmount = bean.getFirstGramPrivilegeMoney();
				} else if (bean.getSelectType() != null && bean.getSelectType() == 2) {
					BigDecimal firstGramSecPrivilegeMoney = bean.getFirstGramSecPrivilegeMoney();
					//首克优惠后续继续优惠  firstGramSecPrivilegeMoney不会为空
					//firstGramSecPrivilegeMoney = firstGramSecPrivilegeMoney == null ? 0 : firstGramSecPrivilegeMoney;
					discountAmount = firstGramSecPrivilegeMoney;
					Integer maxPrivilegeGram = bean.getMaxPrivilegeGram();
					maxPrivilegeGram = maxPrivilegeGram == null ? 0 : maxPrivilegeGram;
					BigDecimal firstGramNextPrivilegeMoney = bean.getFirstGramNextPrivilegeMoney();
					firstGramNextPrivilegeMoney = firstGramNextPrivilegeMoney == null ? BigDecimal.ZERO : firstGramNextPrivilegeMoney;
					if (maxPrivilegeGram >= totalGram) {
						discountAmount = discountAmount.add(new BigDecimal(totalGram-1).multiply(firstGramNextPrivilegeMoney));
					} else {
						discountAmount = discountAmount.add(new BigDecimal(maxPrivilegeGram-1).multiply(firstGramNextPrivilegeMoney));
					}
				}
			}

			// 奖励金豆
			if (bean.getPrivilegeType() != null && bean.getPrivilegeType() == 2) {
				if (bean.getSelectType() != null && bean.getSelectType() == 1) {
					Integer firstGramPrivilegeBean = bean.getFirstGramPrivilegeBean();
					firstGramPrivilegeBean = firstGramPrivilegeBean == null ? 0 : firstGramPrivilegeBean;
					discountAmount = new BigDecimal(firstGramPrivilegeBean);
				} else if (bean.getSelectType() != null && bean.getSelectType() == 2) {
					Integer firstGramSecPrivilegeBean = bean.getFirstGramSecPrivilegeBean();
					firstGramSecPrivilegeBean = firstGramSecPrivilegeBean == null ? 0 : firstGramSecPrivilegeBean;
					discountAmount = new BigDecimal(firstGramSecPrivilegeBean);
					Integer maxPrivilegeGram = bean.getMaxPrivilegeGram();
					maxPrivilegeGram = maxPrivilegeGram == null ? 0 : maxPrivilegeGram;
					Integer firstGramNextPrivilegeBean = bean.getFirstGramNextPrivilegeBean();
					firstGramNextPrivilegeBean = firstGramNextPrivilegeBean == null ? 0 : firstGramNextPrivilegeBean;
					if (maxPrivilegeGram >= totalGram) {
						discountAmount = discountAmount.add(new BigDecimal(totalGram-1).multiply(new BigDecimal(firstGramNextPrivilegeBean)));
					} else {
						discountAmount = discountAmount.add(new BigDecimal(maxPrivilegeGram-1).multiply(new BigDecimal(firstGramNextPrivilegeBean)));
					}
				}
				discountAmount = new BigDecimal(discountAmount.intValue());
			}
		}
		return BigDecimalUtil.to2Point(discountAmount);
	}

	public BigDecimal getDiscountAmount(PrivilegeTemplateBean bean, BigDecimal itemAmount, int mark) {
		BigDecimal discountAmount = BigDecimal.ZERO;
		if (bean.getSchemeType() != null && bean.getSchemeType() == 2) {
			List<TemplateBean> templateBeans = bean.getDataList();
			if (templateBeans != null && templateBeans.size() > 0) {
				if (mark == 0) {
					Collections.sort(templateBeans,new Comparator<TemplateBean>(){
						@Override
						public int compare(TemplateBean o1, TemplateBean o2) {
							if (o1.getLimit() != null && o2.getLimit() != null && o1.getLimit() > o2.getLimit()) {
								return 1;
							} else if (o1.getLimit() != null && o2.getLimit() != null && o1.getLimit() == o2.getLimit()) {
								return 0;
							}
							return -1;
						}
					});
					int index = 0;
					for (TemplateBean templateBean : templateBeans) {
						if (templateBean.getLimit() != null && templateBean.getPrivilegeLimit() != null && itemAmount.doubleValue() >= templateBean.getLimit()) {
							index++;
						} else {
							break;
						}
					}
					if (index > 0) {
						TemplateBean templateBean = templateBeans.get(index - 1);
						Integer way = bean.getWay();
						//1.优惠金额 2.优惠比例
						if (way != null && way == 1) {
							discountAmount = discountAmount.add(templateBean.getPrivilegeLimit());
						} else if (way != null && way == 2) {
							discountAmount = discountAmount.add(BigDecimalUtil.to2Point(itemAmount.multiply(templateBean.getPrivilegeLimit()).divide(new BigDecimal(100))));
						} else if (way != null && way == 3) {
							discountAmount = discountAmount.add(templateBean.getPrivilegeLimit());
						}
					}
				} else if (mark == 1) {
					for (TemplateBean templateBean : templateBeans) {
						if (templateBean.getLeaseDay() != null && templateBean.getPrivilegeLimit() != null && itemAmount.doubleValue() == templateBean.getLeaseDay()) {
							Integer way = bean.getWay();
							//1.优惠金额 2.优惠比例
							if (way != null && way == 1) {
								discountAmount = discountAmount.add(templateBean.getPrivilegeLimit());
							} else if (way != null && way == 2) {
								discountAmount = discountAmount.add(BigDecimalUtil.to2Point(itemAmount.multiply(templateBean.getPrivilegeLimit()).divide(new BigDecimal(100))));
							} else if (way != null && way == 3) {
								discountAmount = discountAmount.add(templateBean.getPrivilegeLimit());
							}
						}
					}
				}
			}
		}
		return BigDecimalUtil.to2Point(discountAmount);
	}
}