package cn.ug;

import cn.ug.config.Config;
import cn.ug.config.FundRateConfig;
import cn.ug.config.KD100Config;
import cn.ug.config.YeePayConfig;
import org.dozer.DozerBeanMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@MapperScan("cn.ug.pay.mapper")
public class PayServiceApplication {

	@Bean
	@ConfigurationProperties(prefix = "config")
	public Config config(){
		return new Config();
	}

	@Bean
	@ConfigurationProperties(prefix = "yeePayConfig")
	public YeePayConfig yeePayConfig(){
		return new YeePayConfig();
	}
	@Bean
	public FundRateConfig fundRateConfig(){
		return new FundRateConfig();
	}

	@Bean
	public DozerBeanMapper dozerBeanMapper(){
		return new DozerBeanMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(PayServiceApplication.class, args);
	}
}
