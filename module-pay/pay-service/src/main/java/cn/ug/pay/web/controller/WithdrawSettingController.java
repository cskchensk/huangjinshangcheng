package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.BankInfoBean;
import cn.ug.pay.bean.response.WithdrawSettingBean;
import cn.ug.pay.mapper.entity.WithdrawSetting;
import cn.ug.pay.service.WithdrawService;
import cn.ug.pay.service.WithdrawSettingService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("withdrawSetting")
public class WithdrawSettingController {

    @Resource
    private WithdrawSettingService withdrawSettingService;
    @Resource
    private Config config;


    /**
     * 查询提现限额列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param bankInfoParamBean       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<WithdrawSettingBean>> findList(@RequestHeader String accessToken, BankInfoParamBean bankInfoParamBean) {
        if(bankInfoParamBean.getPageSize() <= 0) {
            bankInfoParamBean.setPageSize(config.getPageSize());
        }
        DataTable<WithdrawSettingBean> dataTable = withdrawSettingService.findList(bankInfoParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询提现限额(后台)
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "findById/{id}" , method = GET)
    public SerializeObject<WithdrawSettingBean> findById(@RequestHeader String accessToken, @PathVariable String id) {
        WithdrawSettingBean withdrawSettingBean = withdrawSettingService.findById(id);
        return new SerializeObject<>(ResultType.NORMAL, withdrawSettingBean);
    }

    @RequestMapping(value = "/bank" , method = GET)
    public SerializeObject<WithdrawSettingBean> getBank(String bankCode) {
        WithdrawSettingBean withdrawSettingBean = withdrawSettingService.findByBankCode(bankCode);
        return new SerializeObject<>(ResultType.NORMAL, withdrawSettingBean);
    }

    /**
     * 新增提现限额信息(后台)
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, WithdrawSettingBean entity) {
        withdrawSettingService.save(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }


}
