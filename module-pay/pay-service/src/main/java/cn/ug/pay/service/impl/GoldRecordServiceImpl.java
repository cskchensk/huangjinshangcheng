package cn.ug.pay.service.impl;

import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.core.ensure.Ensure;
import cn.ug.pay.bean.GoldRecordBean;
import cn.ug.pay.mapper.GoldRecordMapper;
import cn.ug.pay.mapper.entity.GoldRecord;
import cn.ug.pay.service.GoldRecordService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.SerialNumberWorker;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class GoldRecordServiceImpl extends BaseServiceImpl implements GoldRecordService {
	
	@Resource
	private GoldRecordMapper goldRecordMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	@Transactional(rollbackFor = Exception.class)
	public int save(GoldRecordBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getMemberId())) {
			Ensure.that(true).isTrue("00000002");
		}
		GoldRecord entity = dozerBeanMapper.map(entityBean, GoldRecord.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		int rows = goldRecordMapper.insert(entity);
		Ensure.that(rows).isLt(1,"00000005");
        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return goldRecordMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return goldRecordMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return goldRecordMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public GoldRecordBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		GoldRecord entity = goldRecordMapper.findById(id);
		if(null == entity) {
			return null;
		}

		GoldRecordBean entityBean = dozerBeanMapper.map(entity, GoldRecordBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<GoldRecordBean> query(String order, String sort, int pageNum, int pageSize, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
									 String memberId, String memberName, String memberMobile, String keyword) {
		Page<GoldRecordBean> page = PageHelper.startPage(pageNum, pageSize);
		List<GoldRecordBean> list = query(order, sort, addTimeMin, addTimeMax, memberId, memberName, memberMobile, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public BigDecimal yesterdayIncomeAmount(String memberId) {
		LocalDateTime addTimeMax = UF.getDate(UF.getFormatDate(UF.getDateTime())).minusDays(1);
		LocalDateTime addTimeMin = addTimeMax.minusDays(1);
		List<GoldRecordBean> dataList = query(null, null, addTimeMin, addTimeMax, memberId, null, null, null);
		if(null == dataList ||  dataList.isEmpty()) {
			return BigDecimal.ZERO;
		}

		// T日利息
		return dataList.get(0).getInterestNowAmount();
	}

	/**
	 * 获取数据列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param addTimeMin	最小创建时间
	 * @param addTimeMax	最大创建时间
	 * @param memberId		会员ID
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param keyword		关键字
	 * @return				列表
	 */
	private List<GoldRecordBean> query(String order, String sort, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
									   String memberId, String memberName, String memberMobile, String keyword){
		if(null != addTimeMax) {
			// 加一天减一秒
			addTimeMax = addTimeMax.plusDays(1).minusSeconds(1);
		}
		List<GoldRecordBean> dataList = new ArrayList<>();
		List<GoldRecord> list = goldRecordMapper.query(
				getParams(keyword, order, sort)
						.put("addTimeMin", addTimeMin)
						.put("addTimeMax", addTimeMax)
						.put("memberId", memberId)
						.put("memberName", UF.escapeSql(memberName))
						.put("memberMobile", UF.escapeSql(memberMobile))
						.toMap());
		for (GoldRecord o : list) {
			GoldRecordBean objBean = dozerBeanMapper.map(o, GoldRecordBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}

}

