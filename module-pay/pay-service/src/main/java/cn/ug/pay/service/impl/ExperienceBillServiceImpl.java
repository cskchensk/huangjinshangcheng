package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.pay.bean.ExperienceBillBean;
import cn.ug.pay.bean.GoldRecordBean;
import cn.ug.pay.mapper.ExperienceBillMapper;
import cn.ug.pay.mapper.entity.ExperienceBillEntity;
import cn.ug.pay.service.ExperienceBillService;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ExperienceBillServiceImpl implements ExperienceBillService {

    @Autowired
    private ExperienceBillMapper experienceBillMapper;

    @Override
    public SerializeObject add(ExperienceBillBean experienceBillBean) {
        ExperienceBillEntity experienceBillEntity = new ExperienceBillEntity();
        BeanUtils.copyProperties(experienceBillEntity, experienceBillEntity);
        int rows = experienceBillMapper.insert(experienceBillEntity);
        Ensure.that(rows).isLt(1, "00000005");
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }

    @Override
    public DataTable<ExperienceBillBean> queryList(String memberId,Integer type,int pageNum, int pageSize,Order order) {
        Page<ExperienceBillBean> page = PageHelper.startPage(pageNum, pageSize);
        Map requestMap = new HashMap();
        requestMap.put("memberId",memberId);
        requestMap.put("type",type);
        if (order!=null){
            requestMap.put("order", order.getOrder());
            requestMap.put("sort", order.getSort());
        }
        List<ExperienceBillEntity> experienceBillEntityList = experienceBillMapper.queryList(requestMap);
        List<ExperienceBillBean> experienceBillBeanList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(experienceBillEntityList)) {
            for (ExperienceBillEntity experienceBillEntity : experienceBillEntityList) {
                ExperienceBillBean experienceBillBean = new ExperienceBillBean();
                BeanUtils.copyProperties(experienceBillEntity, experienceBillBean);
                experienceBillBean.setAddTime(UF.getFormatDateTime(experienceBillEntity.getAddTime()));
                experienceBillBean.setModifyTime(UF.getFormatDateTime(experienceBillEntity.getModifyTime()));
                experienceBillBeanList.add(experienceBillBean);
            }
        }
        return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), experienceBillBeanList);
    }
}
