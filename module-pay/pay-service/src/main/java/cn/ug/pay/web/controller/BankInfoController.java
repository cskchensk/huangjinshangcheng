package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.bean.response.BankInfoBean;
import cn.ug.pay.service.BankInfoService;
import cn.ug.product.bean.response.ProductBean;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 银行卡基础服务
 * @author ywl
 * @date 2018/2/2
 */
@RestController
@RequestMapping("bankInfo")
public class BankInfoController {

    @Resource
    private BankInfoService bankInfoService;

    @Resource
    private Config config;

    /**
     * 查询银行基础列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param bankInfoParamBean       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<BankInfoBean>> findList(@RequestHeader String accessToken, BankInfoParamBean bankInfoParamBean) {
        if(bankInfoParamBean.getPageSize() <= 0) {
            bankInfoParamBean.setPageSize(config.getPageSize());
        }
        DataTable<BankInfoBean> dataTable = bankInfoService.findList(bankInfoParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询银行基础信息列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "findById/{id}" , method = GET)
    public SerializeObject<BankInfoBean> findById(@RequestHeader String accessToken, @PathVariable String id) {
        BankInfoBean bankInfoBean = bankInfoService.findById(id);
        return new SerializeObject<>(ResultType.NORMAL, bankInfoBean);
    }

    /**
     * 新增银行基础信息(后台)
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, BankInfoBean entity) {
        bankInfoService.save(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除银行基础信息(后台)
     * @param accessToken		登录成功后分配的Key
     * @return				    是否操作成功
     */
    @RequestMapping(value = "deleted" ,method = POST)
    public SerializeObject deleted(@RequestHeader String accessToken, String id []) {
        bankInfoService.deleted(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询银行基础信息列表(移动端)
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "queryBankInfoList" , method = GET)
    public SerializeObject<List<BankInfoBean>> queryBankInfoList(@RequestHeader String accessToken) {
        List<BankInfoBean> bankInfoBeanList = bankInfoService.queryBankInfoList();
        return new SerializeObject<>(ResultType.NORMAL, bankInfoBeanList);
    }
}
