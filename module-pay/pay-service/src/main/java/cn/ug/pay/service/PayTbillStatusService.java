package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayTbillStatus;

import java.util.List;

public interface PayTbillStatusService {
    List<PayTbillStatus> query(String tbillNO, String type, int status, String order, String sort, int offset, int size);
    int count(String tbillNO, String type, int status);
}
