package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * This class was generated by MyBatis Generator.
 * This class corresponds to the database table pay_operator_cost
 *
 * @mbg.generated do_not_delete_during_merge
 */
public class PayOperatorCost implements Serializable {
    /**
     * Database Column Remarks:
     * 主键id
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     * Database Column Remarks:
     * 每日线上运营成本总和
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.operation_all_cost
     *
     * @mbg.generated
     */
    private BigDecimal operationAllCost;

    /**
     * Database Column Remarks:
     * 每日体验金奖励总成本（元)
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.experience_income_amount
     *
     * @mbg.generated
     */
    private BigDecimal experienceIncomeAmount;

    /**
     * Database Column Remarks:
     * 每日黄金红包总成本（元)
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.gold_coupon_all_cost
     *
     * @mbg.generated
     */
    private BigDecimal goldCouponAllCost;

    /**
     * Database Column Remarks:
     * 每日金豆抵扣总成本（元)
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.gold_bean_all_cost
     *
     * @mbg.generated
     */
    private BigDecimal goldBeanAllCost;

    /**
     * Database Column Remarks:
     * 每日回租福利券成本（元）
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.lease_backall_cost
     *
     * @mbg.generated
     */
    private BigDecimal leaseBackallCost;

    /**
     * Database Column Remarks:
     * 每日商城满减券总成本（元）
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.shop_electronic_card
     *
     * @mbg.generated
     */
    private Integer shopElectronicCard;

    /**
     * Database Column Remarks:
     * 日期
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.date
     *
     * @mbg.generated
     */
    private String date;

    /**
     * Database Column Remarks:
     * 添加时间
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.add_time
     *
     * @mbg.generated
     */
    private Date addTime;

    /**
     * Database Column Remarks:
     * 更新时间
     * <p>
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column pay_operator_cost.modify_time
     *
     * @mbg.generated
     */
    private Date modifyTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table pay_operator_cost
     *
     * @mbg.generated
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.id
     *
     * @return the value of pay_operator_cost.id
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.id
     *
     * @param id the value for pay_operator_cost.id
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.operation_all_cost
     *
     * @return the value of pay_operator_cost.operation_all_cost
     * @mbg.generated
     */
    public BigDecimal getOperationAllCost() {
        return operationAllCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.operation_all_cost
     *
     * @param operationAllCost the value for pay_operator_cost.operation_all_cost
     * @mbg.generated
     */
    public void setOperationAllCost(BigDecimal operationAllCost) {
        this.operationAllCost = operationAllCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.experience_income_amount
     *
     * @return the value of pay_operator_cost.experience_income_amount
     * @mbg.generated
     */
    public BigDecimal getExperienceIncomeAmount() {
        return experienceIncomeAmount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.experience_income_amount
     *
     * @param experienceIncomeAmount the value for pay_operator_cost.experience_income_amount
     * @mbg.generated
     */
    public void setExperienceIncomeAmount(BigDecimal experienceIncomeAmount) {
        this.experienceIncomeAmount = experienceIncomeAmount;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.gold_coupon_all_cost
     *
     * @return the value of pay_operator_cost.gold_coupon_all_cost
     * @mbg.generated
     */
    public BigDecimal getGoldCouponAllCost() {
        return goldCouponAllCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.gold_coupon_all_cost
     *
     * @param goldCouponAllCost the value for pay_operator_cost.gold_coupon_all_cost
     * @mbg.generated
     */
    public void setGoldCouponAllCost(BigDecimal goldCouponAllCost) {
        this.goldCouponAllCost = goldCouponAllCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.gold_bean_all_cost
     *
     * @return the value of pay_operator_cost.gold_bean_all_cost
     * @mbg.generated
     */
    public BigDecimal getGoldBeanAllCost() {
        return goldBeanAllCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.gold_bean_all_cost
     *
     * @param goldBeanAllCost the value for pay_operator_cost.gold_bean_all_cost
     * @mbg.generated
     */
    public void setGoldBeanAllCost(BigDecimal goldBeanAllCost) {
        this.goldBeanAllCost = goldBeanAllCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.lease_backall_cost
     *
     * @return the value of pay_operator_cost.lease_backall_cost
     * @mbg.generated
     */
    public BigDecimal getLeaseBackallCost() {
        return leaseBackallCost;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.lease_backall_cost
     *
     * @param leaseBackallCost the value for pay_operator_cost.lease_backall_cost
     * @mbg.generated
     */
    public void setLeaseBackallCost(BigDecimal leaseBackallCost) {
        this.leaseBackallCost = leaseBackallCost;
    }

    public Integer getShopElectronicCard() {
        return shopElectronicCard;
    }

    public void setShopElectronicCard(Integer shopElectronicCard) {
        this.shopElectronicCard = shopElectronicCard;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.date
     *
     * @return the value of pay_operator_cost.date
     * @mbg.generated
     */
    public String getDate() {
        return date;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.date
     *
     * @param date the value for pay_operator_cost.date
     * @mbg.generated
     */
    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.add_time
     *
     * @return the value of pay_operator_cost.add_time
     * @mbg.generated
     */
    public Date getAddTime() {
        return addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.add_time
     *
     * @param addTime the value for pay_operator_cost.add_time
     * @mbg.generated
     */
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column pay_operator_cost.modify_time
     *
     * @return the value of pay_operator_cost.modify_time
     * @mbg.generated
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column pay_operator_cost.modify_time
     *
     * @param modifyTime the value for pay_operator_cost.modify_time
     * @mbg.generated
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}