package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.BankInfoBean;

import java.util.List;

public interface BankInfoService  {

    /**
     * 后台
     * 获取银行列表
     * @return
     */
    DataTable<BankInfoBean> findList(BankInfoParamBean bankInfoParamBean);

    /**
     * 根据id获取-后台
     * @param id
     * @return
     */
    BankInfoBean findById(String id);

    /**
     * 移动端
     *获取银行基础信息列表
     * @return
     */
    List<BankInfoBean> queryBankInfoList();

    /**
     * 新增或修改基础信息
     * @param bankInfoBean
     */
    void save(BankInfoBean bankInfoBean);

    /**
     * 删除银行信息
     * @param id
     */
    void deleted(String id []);

    /**
     * 银行设置维护时间定时任务
     */
    void bankInfoJob();
}
