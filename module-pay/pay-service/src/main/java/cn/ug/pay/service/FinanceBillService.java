package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.BankInfoBean;
import cn.ug.pay.bean.response.FinanceBillBean;

import java.util.List;

/**
 * 财务对账
 * @auther ywl
 * @date 2018-04-13 11:50
 */
public interface FinanceBillService {

    /**
     * 后台
     * 获取财务账单列表
     * @return
     */
    DataTable<FinanceBillBean> findList(FinanceBillParam financeBillParam);

    List<FinanceBillBean> queryAllList(FinanceBillParam financeBillParam);

}
