package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.FinanceBillInfoParam;
import cn.ug.pay.bean.response.FinanceBillInfoBean;
import cn.ug.pay.service.FinanceBillInfoService;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("financeInfoBalance")
public class FinanceBillInfoController {

    @Resource
    private FinanceBillInfoService financeBillInfoService;

    @Resource
    private Config config;

    /**
     * 查询财务对账列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param financeBillInfoParam       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<FinanceBillInfoBean>> findList(@RequestHeader String accessToken, FinanceBillInfoParam financeBillInfoParam) {
        if(financeBillInfoParam.getPageSize() <= 0) {
            financeBillInfoParam.setPageSize(config.getPageSize());
        }
        DataTable<FinanceBillInfoBean> dataTable = financeBillInfoService.findList(financeBillInfoParam);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }


}
