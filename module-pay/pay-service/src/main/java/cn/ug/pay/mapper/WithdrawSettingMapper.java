package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.WithdrawSettingBean;
import cn.ug.pay.mapper.entity.WithdrawSetting;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface WithdrawSettingMapper extends BaseMapper<WithdrawSetting> {

    WithdrawSetting findById(String id);

    List<WithdrawSettingBean> findList(BankInfoParamBean bankInfoParamBean);

    WithdrawSettingBean findByBankCode(String bankCode);



}
