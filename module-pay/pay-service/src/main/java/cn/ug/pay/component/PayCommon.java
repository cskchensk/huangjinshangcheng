package cn.ug.pay.component;

import cn.ug.msg.mq.Msg;
import cn.ug.util.UF;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static cn.ug.config.QueueName.QUEUE_MSG_SEND;

/**
 * 支付公共类
 * @auther ywl
 */
@Component
public class PayCommon implements Serializable{

    @Resource
    private AmqpTemplate amqpTemplate;

    /**
     * 添加资金账户收入变动消息队列
     */
    public void accountChangeMsgQueue(String memberId, Integer type, String changeAmount, String amount){
        Msg msg = new Msg();
        msg.setMemberId(memberId);
        msg.setType(type);

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("date", UF.getFormatDate("MM月dd日", UF.getDateTime()));
        paramMap.put("changeAmount", changeAmount);
        paramMap.put("amount",  amount);
        msg.setParamMap(paramMap);

        amqpTemplate.convertAndSend(QUEUE_MSG_SEND, msg);
    }
}
