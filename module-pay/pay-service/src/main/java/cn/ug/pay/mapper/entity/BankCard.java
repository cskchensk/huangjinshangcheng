package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 * @author ywl
 * @date 2018/1/30
 */
public class BankCard  extends BaseEntity implements Serializable {

    /** 会员ID **/
    private String memberId;
    /** 支付方式  1:易宝支付 **/
    private Integer payWay;
    /** 卡号姓名 **/
    private String name;
    /** 预留号码 **/
    private String mobile;
    /** 身份证卡号 **/
    private String idCard;
    /** 银行卡号 **/
    private String cardNo;
    /** 卡号前6位 **/
    private String cardTop;
    /** 卡号后4位 **/
    private String cardLast;
    /** 银行编号  如:ICBC **/
    private String bankCode;
    /** 银行名称 **/
    private String bankName;
    /** 绑卡请求号 商户生成 **/
    private String requestNo;
    /** 绑定状态  1:待绑定 2:已绑定 3:已解除绑定 4:绑定失败 **/
    private Integer status;
    /** 描述 **/
    private String description;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }


    public String getCardTop() {
        return cardTop;
    }

    public void setCardTop(String cardTop) {
        this.cardTop = cardTop;
    }

    public String getCardLast() {
        return cardLast;
    }

    public void setCardLast(String cardLast) {
        this.cardLast = cardLast;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
