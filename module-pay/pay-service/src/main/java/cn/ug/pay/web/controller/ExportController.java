package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.ExtractGoldParamBean;
import cn.ug.pay.bean.response.ExtractGoldBean;
import cn.ug.pay.service.ExtractGoldService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("export")
public class ExportController {

    @Resource
    private ExtractGoldService extractGoldService;

    /**
     * 提现报表
     * @param extractGoldParamBean
     * @return
     */
    @GetMapping(value = "exportExtractGold")
    public void exportWithdraw(HttpServletResponse response, ExtractGoldParamBean extractGoldParamBean) {
        extractGoldParamBean.setPageSize(100000);
        List<ExtractGoldBean> list = extractGoldService.findExportList(extractGoldParamBean);
        if(list != null && !list.isEmpty()){
            String[] columnNames = { "订单号", "姓名","手机号码", "身份证号码", "提金克数", "提金时间"};
            String [] columns = {"orderId",  "name", "mobile", "idCard", "amount", "addTimeString"};
            String fileName = "提金报表";
            ExportExcelController<ExtractGoldBean> export = new ExportExcelController<ExtractGoldBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

}
