package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/9/23 0023
 * @time 上午 10:34
 **/
public class PayTbillSoldStatistic implements Serializable {

    //合计人数
    private int countPersonNum;

    //合计笔数
    private int countNum;

    //出售提单克重
    private int totalGram;

    //出售提单金额
    private BigDecimal totalAmount = BigDecimal.ZERO;

    //出售手续费
    private BigDecimal totalFee = BigDecimal.ZERO;

    //出售扣减已优惠金额
    private BigDecimal totalCouponAmount = BigDecimal.ZERO;

    //出售到账金额
    private BigDecimal totalActualAmount = BigDecimal.ZERO;


    public int getCountPersonNum() {
        return countPersonNum;
    }

    public void setCountPersonNum(int countPersonNum) {
        this.countPersonNum = countPersonNum;
    }

    public int getCountNum() {
        return countNum;
    }

    public void setCountNum(int countNum) {
        this.countNum = countNum;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public BigDecimal getTotalCouponAmount() {
        return totalCouponAmount;
    }

    public void setTotalCouponAmount(BigDecimal totalCouponAmount) {
        this.totalCouponAmount = totalCouponAmount;
    }

    public BigDecimal getTotalActualAmount() {
        return totalActualAmount;
    }

    public void setTotalActualAmount(BigDecimal totalActualAmount) {
        this.totalActualAmount = totalActualAmount;
    }
}
