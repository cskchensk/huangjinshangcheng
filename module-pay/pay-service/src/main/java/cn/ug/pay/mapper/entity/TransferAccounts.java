package cn.ug.pay.mapper.entity;

/**
 * @Author zhangweijie
 * @Date 2019/7/22 0022
 * @time 下午 17:23
 **/
public class TransferAccounts extends PayTransferAccounts{

    private String mobile;

    private String name;

    private String idCard;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
}
