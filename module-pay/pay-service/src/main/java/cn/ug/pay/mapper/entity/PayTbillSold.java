package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayTbillSold implements Serializable {
    private int id;
    private String tbillNO;
    private String orderNO;
    private BigDecimal goldPrice;
    private BigDecimal totalAmount;
    private BigDecimal fee;
    private BigDecimal couponAmount;
    private BigDecimal actualAmount;
    private String addTime;
    private String successTime;

    private int index;
    private int totalGram;
    private String couponId;
    private String tbillTime;
    private String source;

    private String productId;
    private Integer productType;
    private String productTypeMark;

    private String mobile;
    private String name;
    private String memberId;

    private BigDecimal buyGoldPrice;//购买时金价
    private BigDecimal buyTotalAmount;//购买时提单金额
    private BigDecimal incomeAmount;//出售赚钱差价

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTbillTime() {
        return tbillTime;
    }

    public void setTbillTime(String tbillTime) {
        this.tbillTime = tbillTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTbillNO() {
        return tbillNO;
    }

    public void setTbillNO(String tbillNO) {
        this.tbillNO = tbillNO;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(BigDecimal actualAmount) {
        this.actualAmount = actualAmount;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getProductTypeMark() {
        return productTypeMark;
    }

    public void setProductTypeMark(String productTypeMark) {
        this.productTypeMark = productTypeMark;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getBuyGoldPrice() {
        return buyGoldPrice;
    }

    public void setBuyGoldPrice(BigDecimal buyGoldPrice) {
        this.buyGoldPrice = buyGoldPrice;
    }

    public BigDecimal getBuyTotalAmount() {
        return buyTotalAmount;
    }

    public void setBuyTotalAmount(BigDecimal buyTotalAmount) {
        this.buyTotalAmount = buyTotalAmount;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }
}
