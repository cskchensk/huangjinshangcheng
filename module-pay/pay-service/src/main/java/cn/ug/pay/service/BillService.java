package cn.ug.pay.service;


import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.BillBean;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.ActiveMemberBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface BillService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(BillBean entityBean);

	int getTransactionNumForThisMonth(String memberId, int tradeType);

	int getTransactionNumForThisDay(String memberId, int tradeType);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	BillBean findById(String id);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param type			类型 1:收入 2:支出
	 * @param tradeType		交易类型 1:线上充值  2:卖金收入 3:邀请返现 4:账户提现 5:买金支出
	 * @param amountMin		最小收支记录金额
	 * @param amountMax		最大收支记录金额
	 * @param addTimeMin	最小操作时间
	 * @param addTimeMax	最大操作时间
	 * @param memberId		会员ID
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<BillBean> query(String order, String sort, int pageNum, int pageSize, Integer type, Integer tradeType,
                                             BigDecimal amountMin, BigDecimal amountMax, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
                                             String memberId, String memberName, String memberMobile, String keyword);

	List<BillBean> query(Integer tradeType, LocalDateTime addTimeMin, LocalDateTime addTimeMax);

	/**
	 * 查询活跃用户
	 * @param financeBillParam
	 * @return
	 */
	List<ActiveMemberBean> findBuySellActiveMemberList(FinanceBillParam financeBillParam);

	BigDecimal sumAmount(int tradeType, String startDate,String endDate, String memberId);
	int sumMembers(int tradeType, String startDate,String endDate, String memberId);

	/**
	 * 查询当月的充值总金额
	 * @param memberId
	 * @return
	 */
	BigDecimal getTransactionAmountForThisMonth(String memberId);
}
