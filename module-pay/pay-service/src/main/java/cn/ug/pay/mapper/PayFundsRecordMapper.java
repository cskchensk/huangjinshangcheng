package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayFundsRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayFundsRecordMapper {
    int insertInBatch(@Param("records")List<PayFundsRecord> records);

    List<PayFundsRecord> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<PayFundsRecord> queryActiveAccountForList(Map<String, Object> params);
}
