package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

public class FinanceBillInfo extends BaseEntity implements java.io.Serializable{

    /** 日期 **/
    private Date day;
    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 交易数量 **/
    private Integer total;
    /** 交易金额 **/
    private BigDecimal amount;
    /** 类型 1:收入 2:支出 **/
    private Integer type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }
}
