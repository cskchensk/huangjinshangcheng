package cn.ug.pay.service;

import cn.ug.pay.bean.FeeBean;
import cn.ug.pay.bean.YeepayBill;
import cn.ug.pay.mapper.entity.PayYeepayBill;

import java.util.List;

public interface PayYeepayBillService {
    boolean save(List<PayYeepayBill> bills);

    List<YeepayBill> query(String memberId, int offset, int size);
    int count(String memberId);

    List<FeeBean> queryFee(int type, String mobile, String name, String startDate, String endDate, int offset, int size);
    int countFee(int type, String mobile, String name, String startDate, String endDate);

    List<FeeBean> queryYeepayFee(int type, String mobile, String name, String startDate, String endDate, int offset, int size);
    int countYeepayFee(int type, String mobile, String name, String startDate, String endDate);
}
