package cn.ug.pay.service.impl;

import cn.ug.pay.mapper.PayAccountRecordMapper;
import cn.ug.pay.mapper.entity.PayAccountRecord;
import cn.ug.pay.service.PayAccountRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayAccountRecordServiceImpl implements PayAccountRecordService {
    @Autowired
    private PayAccountRecordMapper payAccountRecordMapper;

    @Override
    public boolean save(List<PayAccountRecord> records) {
        if (records != null && records.size() > 0) {
            return payAccountRecordMapper.insertInBatch(records) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<PayAccountRecord> query(String memberId, String startDate, String endDate, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                params.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                params.put("endDate", endDate);
            }
            params.put("offset", offset);
            params.put("size", size);
            return payAccountRecordMapper.query(params);
        }
        return null;
    }

    @Override
    public int count(String memberId, String startDate, String endDate) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                params.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                params.put("endDate", endDate);
            }
            return payAccountRecordMapper.count(params);
        }
        return 0;
    }
}
