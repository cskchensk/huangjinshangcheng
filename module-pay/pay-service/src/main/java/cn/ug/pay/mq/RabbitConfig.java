package cn.ug.pay.mq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static cn.ug.config.QueueName.QUEUE_PAY_WITHDRAW;
import static cn.ug.config.QueueName.QUEUE_PAY_WITHDRAW_STATUS;

/**
 * 消息队列配置
 * @author kaiwotech
 */
@Configuration
public class RabbitConfig {

    /**
     * 提现状态队列
     * @return
     */
    @Bean
    public Queue withdrawStatusQueue() {
        return new Queue(QUEUE_PAY_WITHDRAW_STATUS, true);
    }

    @Bean
    public Queue withdraw(){
        return new Queue(QUEUE_PAY_WITHDRAW, true);
    }

}
