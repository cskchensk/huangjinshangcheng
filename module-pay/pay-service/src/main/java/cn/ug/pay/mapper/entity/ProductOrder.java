package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 定期投资
 * @author kaiwotech
 */
public class ProductOrder extends BaseEntity implements java.io.Serializable {

	/** 订单号 */
	private String orderId;
	/** 会员id */
	private String memberId;
	/** 产品Id */
	private String productId;
	/** 产品名称 */
	private String productName;
	/** 产品类型 1:新手专享 2:活动产品  3:活期产品 4:定期产品 **/
	private Integer type;
	/** 年化收益 **/
	private BigDecimal yearsIncome;
	/** 投资期限 **/
	private String investDay;
	/** 购买克重 */
	private BigDecimal amount;
	/** 购买金价 */
	private BigDecimal goldPrice;
	/** 预期收益 */
	private BigDecimal incomeAmount;
	/** 计息时间 */
	private LocalDateTime startTime;
	/** 到期时间 */
	private LocalDateTime endTime;
	/** 支付类型 1:现金购买 2:金柚宝转入 */
	private Integer payType;
	/** 商品总价 */
	private BigDecimal itemAmount;
	/** 手续费 */
	private BigDecimal fee;
	/** 支付金额（商品总价+手续费） */
	private BigDecimal actualAmount;
	/** T-1日利息 */
	private BigDecimal interestHistoryAmount;
	/** T日本金 */
	private BigDecimal principalNowAmount;
	/** T-1日本金 */
	private BigDecimal principalHistoryAmount;
	/** 支付状态  1:待支付 2:已支付 3:已过期 */
	private Integer payStatus;
	/** 产品状态  1:未到期 2:已到期 */
	private Integer status;
	/** 支付截止时间 */
	private LocalDateTime stopPayTime;
	/** 备注 */
	private String description;
	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;
	private String channelName;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getYearsIncome() {
		return yearsIncome;
	}

	public void setYearsIncome(BigDecimal yearsIncome) {
		this.yearsIncome = yearsIncome;
	}

	public String getInvestDay() {
		return investDay;
	}

	public void setInvestDay(String investDay) {
		this.investDay = investDay;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getGoldPrice() {
		return goldPrice;
	}

	public void setGoldPrice(BigDecimal goldPrice) {
		this.goldPrice = goldPrice;
	}

	public BigDecimal getIncomeAmount() {
		return incomeAmount;
	}

	public void setIncomeAmount(BigDecimal incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}

	public LocalDateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public BigDecimal getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(BigDecimal itemAmount) {
		this.itemAmount = itemAmount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}

	public BigDecimal getInterestHistoryAmount() {
		return interestHistoryAmount;
	}

	public void setInterestHistoryAmount(BigDecimal interestHistoryAmount) {
		this.interestHistoryAmount = interestHistoryAmount;
	}

	public BigDecimal getPrincipalNowAmount() {
		return principalNowAmount;
	}

	public void setPrincipalNowAmount(BigDecimal principalNowAmount) {
		this.principalNowAmount = principalNowAmount;
	}

	public BigDecimal getPrincipalHistoryAmount() {
		return principalHistoryAmount;
	}

	public void setPrincipalHistoryAmount(BigDecimal principalHistoryAmount) {
		this.principalHistoryAmount = principalHistoryAmount;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getStopPayTime() {
		return stopPayTime;
	}

	public void setStopPayTime(LocalDateTime stopPayTime) {
		this.stopPayTime = stopPayTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}
}
