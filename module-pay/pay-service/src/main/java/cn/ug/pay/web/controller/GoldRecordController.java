package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.pay.bean.GoldRecordBean;
import cn.ug.pay.service.GoldRecordService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 可用资产详表
 * @author kaiwotech
 */
@RestController
@RequestMapping("goldRecord")
public class GoldRecordController extends BaseController {

    @Resource
    private GoldRecordService goldRecordService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        GoldRecordBean entity = goldRecordService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 根据当前登录用户查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param addTimeMinString	最小创建时间(yyyy-MM-dd)
     * @param addTimeMaxString  最大创建时间(yyyy-MM-dd)
     * @return			        分页数据
     */
    @RequestMapping(value = "queryByMemberId", method = GET)
    public SerializeObject<DataTable<GoldRecordBean>> queryByMemberId(@RequestHeader String accessToken, Order order, Page page, String addTimeMinString, String addTimeMaxString) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        LocalDateTime addTimeMin = null;
        LocalDateTime addTimeMax = null;
        if(StringUtils.isNotBlank(addTimeMinString)) {
            addTimeMin = UF.getDate(addTimeMinString);
        }
        if(StringUtils.isNotBlank(addTimeMaxString)) {
            addTimeMax = UF.getDate(addTimeMaxString);
        }
        String memberId = LoginHelper.getLoginId();
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObjectError<>("00000102");
        }

        DataTable<GoldRecordBean> dataTable = goldRecordService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(),
                addTimeMin, addTimeMax, memberId, null, null, null);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询数据
     * @param accessToken	        登录成功后分配的Key
     * @param order		            排序
     * @param page		            分页
     * @param addTimeMinString	    最小创建时间
     * @param addTimeMaxString	    最大创建时间
     * @param memberId		        会员ID
     * @param memberName	        会员名称
     * @param memberMobile	        会员手机
     * @param keyword		        关键字
     * @return			            分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<GoldRecordBean>> query(@RequestHeader String accessToken, Order order, Page page,
                                                          String addTimeMinString, String addTimeMaxString,
                                                          String memberId, String memberName, String memberMobile, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        LocalDateTime addTimeMin = null;
        LocalDateTime addTimeMax = null;
        if(StringUtils.isNotBlank(addTimeMinString)) {
            addTimeMin = UF.getDate(addTimeMinString);
        }
        if(StringUtils.isNotBlank(addTimeMaxString)) {
            addTimeMax = UF.getDate(addTimeMaxString);
        }
        keyword = UF.toString(keyword);

        DataTable<GoldRecordBean> dataTable = goldRecordService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(),
                addTimeMin, addTimeMax, memberId, memberName, memberMobile, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }
}
