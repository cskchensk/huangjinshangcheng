package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.MemberGoldBean;
import cn.ug.pay.bean.MemberGoldBeanBean;
import cn.ug.pay.mapper.entity.MemberGold;
import cn.ug.pay.mapper.entity.PayGoldStock;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 会员资产账户
 * @author kaiwotech
 */
@Component
public interface MemberGoldMapper extends BaseMapper<MemberGold> {

    /**
     * 根据会员Id获取每日转入转出记录
     * @param memberId  会员ID
     * @return          每日转入转出记录
     */
    List<MemberGold> queryGroupByDay(@Param(value = "memberId") String memberId);

    MemberGold findByMemberId(@Param("memberId") String memberId);
    int update(MemberGold gold);
    /**
     * 查询有效账户
     * @return			当前页记录数
     */
    List<MemberGold> queryActiveAccount();

    PayGoldStock selectEverydayTotalGramRecord();

    List<MemberGold> queryActiveAccountForList(Map<String, Object> params);
    List<MemberGold> queryActiveAccountForCurrentList(Map<String, Object> params);

    List<MemberGoldBeanBean> queryForBean(Map<String, Object> params);
    int countForBean(Map<String, Object> params);
}