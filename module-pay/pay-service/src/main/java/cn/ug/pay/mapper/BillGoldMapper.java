package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.EverydaySellGoldBean;
import cn.ug.pay.bean.SellGoldRecordBean;
import cn.ug.pay.mapper.entity.BillGold;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 资产收支记录
 * @author kaiwotech
 */
@Component
public interface BillGoldMapper extends BaseMapper<BillGold> {
    List<EverydaySellGoldBean> queryEverydaySellGoldRecords(@Param("payDate") String payDate);

    List<SellGoldRecordBean> querySellGoldRecord(Map<String, Object> params);
    int countSellGoldRecord(Map<String, Object> params);
}