package cn.ug.pay.service;


import cn.ug.bean.base.DataTable;
import cn.ug.member.bean.MemberDetailBean;
import cn.ug.pay.bean.*;
import cn.ug.pay.bean.type.ProductOrderPayStatus;
import cn.ug.pay.bean.type.ProductOrderStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 定期投资
 * @author kaiwotech
 */
public interface ProductOrderService {

	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	String save(ProductOrderBean entityBean);

	/**
	 *  根据订单编号修改订单状态
	 * @param payStatus 订单状态
	 * @param orderId   订单编号
	 * @return
	 */
	boolean updatePayStatus(int payStatus, String orderId);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);

	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	ProductOrderBean findById(String id);

	/**
	 * 根据流水号, 查找对象
	 * @param orderId	流水号
	 * @return		实例
	 */
	ProductOrderBean findByOrderId(String orderId);

	/**
	 * 产品投资记录（包含定期、活期）
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param typeList		产品类型集合
	 * @param payStatus		支付状态  1:待支付 2:已支付 3:已过期
	 * @param status		产品状态  1:未到期 2:已到期
	 * @param overEndTime 	是否收益到期订单
	 * @param amountMin		最小购买克重
	 * @param amountMax		最大购买克重
	 * @param addTimeMin	最小投资时间
	 * @param addTimeMax	最大投资时间
	 * @param memberId		会员ID
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param productId		产品ID
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<ProductOrderBean> query(String order, String sort, int pageNum, int pageSize,
									  List<Integer> typeList, Integer payStatus, Integer status, Boolean overEndTime,
									  BigDecimal amountMin, BigDecimal amountMax, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
									  String memberId, String memberName, String memberMobile, String productId, String keyword, String channelId);

	List<ProductOrderBean> query(String order, String sort, List<Integer> typeList, Integer payStatus, Integer status, Boolean overEndTime,
									  BigDecimal amountMin, BigDecimal amountMax, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
									  String memberId, String memberName, String memberMobile, String productId, String keyword, String channelId);


	List<ProductOrderBean> queryBecomeDueOrders();

	/**
	 * 根据产品ID查找投资记录
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param productId		产品ID
	 * @return				分页数据
	 */
	DataTable<ProductOrderSummaryBean> queryByProductId(String order, String sort, int pageNum, int pageSize, String productId);

	List<ProductOrderSummaryBean> queryByProductId(int offset, int size, String productId, int productType);
	int countByProductId(String productId, int productType);
	/**
	 * 更新支付状态
	 * @param id			定期投资ID
	 * @param payStatus		支付状态  1:待支付 2:已支付 3:已过期
	 * @return				操作影响的记录数
	 */
	int updatePayStatus(String[] id, ProductOrderPayStatus payStatus);

	/**
	 * 更新状态
	 * @param id			定期投资ID
	 * @param status		状态  1:未到期 2:已到期
	 * @return				操作影响的记录数
	 */
	int updateStatus(String[] id, ProductOrderStatus status);

	/**
	 * 处理过期的定期投资
	 * @return	操作影响的行数
	 */
	int expiredProductOrder();

	/**
	 * 收益计算公式 = 购买克重 * 投资期限 * 年化收益 / 100 / 365
	 * @param weight		购买克重
	 * @param investDay		投资期限
	 * @param yearsIncome	年化收益
	 * @return				收益
	 */
	BigDecimal calculateIncomeAmount(BigDecimal weight, Integer investDay, BigDecimal yearsIncome);

	/**
	 * 定期产品转活期计算
	 * @param id	投资记录ID
	 */
	void completeProductOrder(String id);

	/**
	 * 支付成功
	 * @param orderId	投资记录流水号
	 */
	void completePay(String orderId);

	/**
	 * 发放红包
	 * @param orderId	投资记录流水号
	 */
	void sendCouponRed(String orderId);

	/**
	 * 发放加息券
	 * @param orderId	投资记录流水号
	 */
	void sendCouponIncrease(String orderId);

	/**
	 * 选择红包、加息券。 （用在添加投资记录之后，支付之前）
	 * @param memberId				会员ID
	 * @param orderId               投资记录流水号
	 * @param couponRedId           红包ID
	 * @param couponIncreaseId      加息券ID
	 */
	void chooseCoupon(String memberId, String orderId, String couponRedId, String couponIncreaseId);

	/**
	 * 根据产品ID统计募集克重
	 * @param productId	产品ID
	 * @return		    募集克重
	 */
	BigDecimal sumOfAmount(String productId);

	List<TradeTotalAmountBean> listTradeAmountStatistics(int timeType, String startDate, String endDate, List<String> channelIds);
	List<TradeAverageValueBean> listAvgValueStatistics(int timeType, String startDate, String endDate, List<String> channelIds);
    List<TradeFutureValueBean> listTradeFutureValueStatistics(String startDate, String endDate, List<String> channelIds);
    List<OpenPositionTotalBean> listOpenPositionTotalStatistics(String startDate, String endDate);
    List<OpenPositionTotalBean> listExpiredStatistics(String startDate, String endDate);
	List<OpenPositionTotalBean> listExpiredAmountStatistics(int timeType, String startDate, String endDate, int searchType);
	List<OpenPositionTotalBean> listTransactionWeightStatistics(int payType, String startDate, String endDate);
    List<OpenPositionTotalBean> listGoldRecordStatistics(int timeType, String startDate, String endDate);
	List<OpenPositionPastBean> listOpenPositionAvgStatistics(String startDate, String endDate);
    List<KValueBean> listKValue(String searchDate);
	List<RFMLabelBean> queryRFMLabelList(String startDate, String endDate, int offset, int size);
	int queryRFMLabelCount();
	List<EverydayBuyGoldBean> queryEverydayBuyGoldRecords(String payDate);

	List<BuyGoldRecordBean> queryBuyGoldRecord(String payDate, int offset, int size);
	int countBuyGoldRecord(String payDate);

	/**
	 * 平台交易总计报表
	 * @param timeType
	 * @param startDate
	 * @param endDate
	 * @param channelIds
	 * @param productType
	 * @return
	 */
	List<TradeTotalStatisticsBean> listTradeTotalStatistics(int timeType, String startDate, String endDate, List<String> channelIds,List<Integer> productType);
	/**
	 * 查询会员已购买定期产品数量
	 * @param memberId
	 * @return
	 */
	int findRegularBuyCount(String memberId);

	/**
	 * 查询会员已购买定期活动产品
	 * @param memberId
	 * @return
	 */
	int findRegularActivityBuyCount(String memberId);
	int  countActivityOrders(String memberId, String productId);

	MemberDetailBean getMemberTradeInfo(String memberId);

	/**
	 * 根据产品ID查找投资记录
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param productId		产品ID
	 * @return				分页数据
	 */
	DataTable<ProductOrderRecordBean> queryRecordByProductId(int pageNum, int pageSize, String productId);

	/**
	 * 根据当前登陆信息查询黄金交易信息
	 * @param pageNum
	 * @param pageSize
	 * @param addTimeMinString
	 * @param addTimeMaxString
	 * @param endTimeMinString
	 * @param endTimeMaxString
	 * @param memberId
	 * @return
	 */
	DataTable<ProductOrderBean> queryInfoByMemberId(int pageNum, int pageSize,String addTimeMinString, String addTimeMaxString,
													String endTimeMinString, String endTimeMaxString,String memberId);
}
