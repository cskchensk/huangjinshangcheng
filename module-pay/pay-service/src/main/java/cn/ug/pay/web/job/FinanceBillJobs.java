package cn.ug.pay.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.pay.service.AccountFinanceBillService;
import cn.ug.util.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 财务对账
 * @auther ywl
 * @date 2018-04-16 10:19
 */
@Component
public class FinanceBillJobs {

    /** 业务有效期 */
    private static final long EXPIRE_DAY = 1024;

    @Resource
    private AccountFinanceBillService accountFinanceBillService;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    private static Logger logger = LoggerFactory.getLogger(FinanceBillJobs.class);

    /**
     * 财务账户统计
     * 每天凌晨1点开始执行
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void accountfinanceBillJob(){
        String key = "FinanceBillJobs:expiredAccountfinanceBillJobJob:" + UF.getFormatDateNow();
        if(redisGlobalLock.lock(key, EXPIRE_DAY, TimeUnit.DAYS)) {
           try{
               logger.info("开始账户统计账单定时任务");
               logger.info(UF.getFormatDateTime(UF.getDateTime()));
               //执行任务
               accountFinanceBillService.accountfinanceBillJob();
           }catch (Exception e){
               redisGlobalLock.unlock(key);
               throw  e;
           }/*finally {
                redisGlobalLock.unlock(key);
           }*/
        }else{
            logger.info("-------没有获取到锁-------");
            return ;
        }

    }
}
