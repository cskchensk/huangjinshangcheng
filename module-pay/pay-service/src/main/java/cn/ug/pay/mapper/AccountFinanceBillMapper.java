package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.AccountFinanceBillParam;
import cn.ug.pay.mapper.entity.AccountFinanceBill;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AccountFinanceBillMapper extends BaseMapper<AccountFinanceBill> {

    List<AccountFinanceBill> findList(AccountFinanceBillParam accountFinanceBillParam);

    AccountFinanceBill findByDay(String day);
}
