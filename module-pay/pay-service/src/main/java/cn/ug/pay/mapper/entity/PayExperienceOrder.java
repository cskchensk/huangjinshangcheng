package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayExperienceOrder implements Serializable {
    private int id;
    private String memberId;
    private String orderNO;
    private String addTime;
    private String productId;
    private String productName;
    private int totalGram;
    private BigDecimal goldPrice;
    private int leasebackDays;
    private BigDecimal yearIncome;
    private String startTime;
    private String endTime;
    private BigDecimal incomeGram;
    private BigDecimal incomeAmount;
    private int incomeBean;
    private String modifyTime;
    private int status;
    private int incomeType;

    private String name;
    private String mobile;
    private int index;
    private String statusMark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(int incomeType) {
        this.incomeType = incomeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getStatusMark() {
        return statusMark;
    }

    public void setStatusMark(String statusMark) {
        this.statusMark = statusMark;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public BigDecimal getYearIncome() {
        return yearIncome;
    }

    public void setYearIncome(BigDecimal yearIncome) {
        this.yearIncome = yearIncome;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public int getIncomeBean() {
        return incomeBean;
    }

    public void setIncomeBean(int incomeBean) {
        this.incomeBean = incomeBean;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
