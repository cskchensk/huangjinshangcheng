package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.pay.bean.request.FinanceBillInfoParam;
import cn.ug.pay.bean.response.FinanceBillInfoBean;
import cn.ug.pay.mapper.BillMapper;
import cn.ug.pay.service.FinanceBillInfoService;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class FinanceBillInfoServiceImpl implements FinanceBillInfoService {

    @Resource
    private BillMapper billMapper;

    @Resource
    private Config config;

    @Override
    public DataTable<FinanceBillInfoBean> findList(FinanceBillInfoParam financeBillInfoParam) {
        com.github.pagehelper.Page<FinanceBillInfoParam> pages = PageHelper.startPage(financeBillInfoParam.getPageNum(),financeBillInfoParam.getPageSize());
        //必传参数验证
        Ensure.that(financeBillInfoParam.getDay()).isNull("17000903");
        Ensure.that(financeBillInfoParam.getType()).isNull("17000901");
        Ensure.that(financeBillInfoParam.getType() == 1 || financeBillInfoParam.getType() == 2).isFalse("17000902");
        List<FinanceBillInfoBean> dataList = billMapper.findFinanceBillInfoList(financeBillInfoParam);
        return new DataTable<>(financeBillInfoParam.getPageNum(), financeBillInfoParam.getPageSize(), pages.getTotal(), dataList);
    }
}
