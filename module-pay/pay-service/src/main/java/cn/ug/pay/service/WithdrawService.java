package cn.ug.pay.service;


import cn.ug.bean.base.DataOtherTable;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.WithdrawAuditBean;
import cn.ug.pay.bean.WithdrawBean;
import com.alibaba.fastjson.JSONObject;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 提现管理
 * @author kaiwotech
 */
public interface WithdrawService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(WithdrawBean entityBean);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	WithdrawAuditBean findById(String id);

	/**
	 * 根据流水号, 查找对象
	 * @param orderId	流水号
	 * @return			实例
	 */
	WithdrawAuditBean findByOrderId(String orderId);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	WithdrawAuditBean findDetailById(String id);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param type			提现类型  0:全部 1: T+0到账 2: T+1到账
	 * @param status		提现状态  0:全部 1:待提现 2:处理中 3:成功 4:失败
	 * @param auditStatus	审核状态  0:全部 1:未审核 2:一级审核成功 3一级审核失败 3:二级审核成功 4:二级审核失败
	 * @param amountMin		最小提现金额
	 * @param amountMax		最大提现金额
	 * @param addTimeMin	最小操作时间
	 * @param addTimeMax	最大操作时间
	 * @param modifyTimeMin	最小反馈时间
	 * @param modifyTimeMax	最大反馈时间
	 * @param memberId		会员ID
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataOtherTable<WithdrawAuditBean> queryDetail(String order, String sort, int pageNum, int pageSize, Integer type, Integer status, Integer auditStatus,
												  BigDecimal amountMin, BigDecimal amountMax, LocalDateTime addTimeMin, LocalDateTime addTimeMax, LocalDateTime modifyTimeMin, LocalDateTime modifyTimeMax,
												  String memberId, String memberName, String memberMobile, String keyword, List<Integer> auditStatusList, String channelId);

	/**
	 * 更新状态
	 * @param orderId		提现流水号
	 * @param status		提现状态 提现状态  1:待提现 2:处理中 3:成功 4:失败
	 * @param description	备注
	 * @return				操作影响的记录数
	 */
	int updateStatus(String orderId, int status, String description);

	/**
	 * 更新状态
	 * @param id			提现ID
	 * @param status		提现状态 提现状态  1:待提现 2:处理中 3:成功 4:失败
	 * @param description	备注
	 * @return				操作影响的记录数
	 */
	int updateStatus(String[] id, int status, String description);

	/**
	 * 更新审核状态（第二次审核通过后，自动发起提现操作）
	 * @param id			提现ID
	 * @param auditLevel	审核级别（1：一级审核 2：二级审核）
	 * @param auditStatus	审核意见（1：通过 2：拒绝）
	 * @param description	审核意见详情
	 * @param userId		审核人ID
	 * @param userName		审核人名称
	 * @return				操作影响的记录数
	 */
	int audit(String[] id, int auditLevel, int auditStatus, String description, String userId, String userName);

	/**
	 * 查询当前用户提现以及费率信息
	 * @param memberId
	 * @return
	 */
	JSONObject queryWithdrawInfo(String memberId, BigDecimal amount);

}
