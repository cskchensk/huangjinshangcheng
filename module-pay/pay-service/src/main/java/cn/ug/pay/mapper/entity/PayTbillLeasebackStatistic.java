package cn.ug.pay.mapper.entity;


import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/9/20 0020
 * @time 下午 17:22
 **/
public class PayTbillLeasebackStatistic implements Serializable {


    //合计人数
    private int countPersonNum;

    //合计笔数
    private int countNum;

    //合计回租中提单克重
    private int totalGram;

    //合计回租中提单金额
    private BigDecimal totalAmount = BigDecimal.ZERO;

    //回租金生金奖励
    private BigDecimal totalGoldGiveGoldAmount = BigDecimal.ZERO;

    //金豆
    private int totalBean;

    //黄金红包额度
    private int totalCouponAmount;

    //黄金红包奖励
    private BigDecimal totalCouponAwardAmount = BigDecimal.ZERO;

    //回租专享优惠（元）
    private BigDecimal totalDiscountAmount = BigDecimal.ZERO;

    //回租总奖励（元）
    private BigDecimal totalIncomeAmount = BigDecimal.ZERO;

    public int getCountPersonNum() {
        return countPersonNum;
    }

    public void setCountPersonNum(int countPersonNum) {
        this.countPersonNum = countPersonNum;
    }

    public int getCountNum() {
        return countNum;
    }

    public void setCountNum(int countNum) {
        this.countNum = countNum;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalGoldGiveGoldAmount() {
        return totalGoldGiveGoldAmount;
    }

    public void setTotalGoldGiveGoldAmount(BigDecimal totalGoldGiveGoldAmount) {
        this.totalGoldGiveGoldAmount = totalGoldGiveGoldAmount;
    }

    public int getTotalBean() {
        return totalBean;
    }

    public void setTotalBean(int totalBean) {
        this.totalBean = totalBean;
    }

    public int getTotalCouponAmount() {
        return totalCouponAmount;
    }

    public void setTotalCouponAmount(int totalCouponAmount) {
        this.totalCouponAmount = totalCouponAmount;
    }

    public BigDecimal getTotalCouponAwardAmount() {
        return totalCouponAwardAmount;
    }

    public void setTotalCouponAwardAmount(BigDecimal totalCouponAwardAmount) {
        this.totalCouponAwardAmount = totalCouponAwardAmount;
    }

    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    public BigDecimal getTotalIncomeAmount() {
        return totalIncomeAmount;
    }

    public void setTotalIncomeAmount(BigDecimal totalIncomeAmount) {
        this.totalIncomeAmount = totalIncomeAmount;
    }
}
