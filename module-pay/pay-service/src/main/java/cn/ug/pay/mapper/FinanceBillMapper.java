package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.FinanceBillBean;
import cn.ug.pay.mapper.entity.FinanceBill;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface FinanceBillMapper extends BaseMapper<FinanceBill> {

    List<FinanceBillBean> findList(FinanceBillParam financeBillParam);
}
