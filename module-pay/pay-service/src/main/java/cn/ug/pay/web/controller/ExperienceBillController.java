package cn.ug.pay.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.enums.ExperienceTradeTypeEnum;
import cn.ug.pay.bean.ExperienceBillBean;
import cn.ug.pay.service.ExperienceBillService;
import cn.ug.pay.service.PayTbillService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("experience/record")
public class ExperienceBillController extends BaseController {
    @Autowired
    private ExperienceBillService experienceBillService;
    @Autowired
    private PayTbillService payTbillService;

    /**
     * 体验金详情
     * @param memberId
     * @return
     */
    @RequestMapping(value = "queryList",method = GET)
    public SerializeObject<List<ExperienceBillBean>> queryList(@RequestHeader String accessToken,String memberId,Integer type,Order order, Page page){
        return new SerializeObject(ResultType.NORMAL,experienceBillService.queryList(memberId,type,page.getPageNum(), page.getPageSize(),order));
    }

    /**
     * 体验金添加
     * @param experienceBillBean
     * @return
     */
    @RequestMapping(value = "add",method = POST)
    public SerializeObject add(@RequestHeader String accessToken,ExperienceBillBean experienceBillBean){
        return experienceBillService.add(experienceBillBean);
    }

    /**
     * 导出
     * @param memberId
     * @param order
     * @param page
     * @return
     */
    @RequestMapping(value = "export",method = GET)
    public SerializeObject export(String memberId,Integer type,Order order, Page page,HttpServletResponse response){
        DataTable<ExperienceBillBean> experienceBillBeanDataTable = experienceBillService.queryList(memberId,type,0, Integer.MAX_VALUE,order);

        String fileName = "体验金账户详表";
        String[] columnNames = { "序号", "流水号","获得体验金", "操作行为", "体验金变动克重（克）", "操作时间"};
        String[] columns = {"index",  "orderNo", "typeMark", "tradeTypeMark", "gram", "addTime"};
        int index = 1;
        for (ExperienceBillBean bean : experienceBillBeanDataTable.getDataList()) {
            bean.setIndex(index);
            index++;
            bean.setTypeMark(bean.getType()==1?"收入":"支出");
            bean.setTradeTypeMark(ExperienceTradeTypeEnum.getPayTypeEnum(bean.getTradeType()));
        }
        ExportExcelController<ExperienceBillBean> export = new ExportExcelController<>();
        export.exportExcel(fileName, fileName, columnNames, columns, experienceBillBeanDataTable.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
        return new SerializeObject(ResultType.NORMAL,experienceBillBeanDataTable);
    }

    /*************App接口****************/
    /**
     * app-体验金详情
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "details",method = GET)
    public SerializeObject<List<ExperienceBillBean>> queryList(@RequestHeader String accessToken,Page page){
        String memberId = LoginHelper.getLoginId();
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000102");
        }

        return new SerializeObject(ResultType.NORMAL,experienceBillService.queryList(memberId,null,page.getPageNum(), page.getPageSize(),null));
    }

    @RequestMapping(value = "/present",method = POST)
    public SerializeObject presentExperienceGram(String memberId, int tradeType, int totalGram){
        if (StringUtils.isBlank(memberId) || totalGram < 1 || tradeType < 1) {
            return new SerializeObjectError("00000002");
        }
        if (payTbillService.saveExperienceGram(memberId, tradeType, totalGram)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObjectError("00000005");
        }
    }
}
