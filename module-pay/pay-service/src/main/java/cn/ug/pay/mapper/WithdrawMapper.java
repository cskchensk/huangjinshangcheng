package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.mapper.entity.Withdraw;
import cn.ug.pay.mapper.entity.WithdrawDetail;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 提现
 * @author kaiwotech
 */
@Component
public interface WithdrawMapper extends BaseMapper<Withdraw> {

    /**
     * 分页查询
     * @param params			查询参数
     * @return					当前页记录数
     */
    List<WithdrawDetail> queryDetail(Map<String, Object> params);

    /**
     * 获取当日、当月累计提现金额
     * @param params
     * @return
     */
    BigDecimal findWithdrawTotal(Map<String, Object> params);

    /**
     *  查询上一日提现总额
     * @return
     */
    BigDecimal findLastDayWithdrawTotal();

    /**
     * 查询提现总额脚本
     * @return
     */
    BigDecimal findDayWithdrawTotalScript(@Param("day") String day);

    int getWithdrawNumForThisMonth(@Param("memberId")String memberId);
    int getWithdrawNumForThisDay(@Param("memberId")String memberId);

    Map countQueryDetail(Map map);
}