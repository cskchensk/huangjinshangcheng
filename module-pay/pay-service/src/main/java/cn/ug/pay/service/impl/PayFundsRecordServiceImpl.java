package cn.ug.pay.service.impl;

import cn.ug.pay.mapper.PayFundsRecordMapper;
import cn.ug.pay.mapper.entity.PayFundsRecord;
import cn.ug.pay.service.PayFundsRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayFundsRecordServiceImpl implements PayFundsRecordService {
    @Autowired
    private PayFundsRecordMapper payFundsRecordMapper;

    @Override
    public boolean save(List<PayFundsRecord> records) {
        if (records != null && records.size() > 0) {
            return payFundsRecordMapper.insertInBatch(records) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<PayFundsRecord> query(String memberId, String startDate, String endDate, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                params.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                params.put("endDate", endDate);
            }
            params.put("offset", offset);
            params.put("size", size);
            return payFundsRecordMapper.query(params);
        }
        return null;
    }

    @Override
    public int count(String memberId, String startDate, String endDate) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                params.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                params.put("endDate", endDate);
            }
            return payFundsRecordMapper.count(params);
        }
        return 0;
    }

    @Override
    public List<PayFundsRecord> queryActiveAccountForList(int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("offset", offset);
        params.put("size", size);
        return payFundsRecordMapper.queryActiveAccountForList(params);
    }
}
