package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayGoldStock implements Serializable {
    private int id;
    private String calculationDate;
    private BigDecimal totalGram;
    private BigDecimal currentPrincipalGram;
    private BigDecimal currentInterestGram;
    private BigDecimal regularPrincipalGram;
    private BigDecimal regularInterestGram;
    private BigDecimal closingPrice;
    private int index;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(String calculationDate) {
        this.calculationDate = calculationDate;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getCurrentPrincipalGram() {
        return currentPrincipalGram;
    }

    public void setCurrentPrincipalGram(BigDecimal currentPrincipalGram) {
        this.currentPrincipalGram = currentPrincipalGram;
    }

    public BigDecimal getCurrentInterestGram() {
        return currentInterestGram;
    }

    public void setCurrentInterestGram(BigDecimal currentInterestGram) {
        this.currentInterestGram = currentInterestGram;
    }

    public BigDecimal getRegularPrincipalGram() {
        return regularPrincipalGram;
    }

    public void setRegularPrincipalGram(BigDecimal regularPrincipalGram) {
        this.regularPrincipalGram = regularPrincipalGram;
    }

    public BigDecimal getRegularInterestGram() {
        return regularInterestGram;
    }

    public void setRegularInterestGram(BigDecimal regularInterestGram) {
        this.regularInterestGram = regularInterestGram;
    }

    public BigDecimal getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(BigDecimal closingPrice) {
        this.closingPrice = closingPrice;
    }
}
