package cn.ug.pay.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.mall.bean.FeeBean;
import cn.ug.mall.bean.GoodsSkuBean;
import cn.ug.pay.bean.request.PayParamBean;
import cn.ug.pay.bean.request.RechargeParamBean;
import cn.ug.pay.bean.request.YeePayParamBean;
import cn.ug.pay.mapper.entity.Trade;
import cn.ug.pay.mq.WithdrawMQ;

import java.math.BigDecimal;
import java.util.Map;

public interface TradeService {

    /**
     * 会员充值
     * @param rechargeParamBean
     * @return   返回订单号
     */
    String recharge(RechargeParamBean rechargeParamBean);

    /**
     * 易宝支付交易(充值)异步回调接口
     * @param yeePayParamBean
     * @return
     */
    String yeePayRechargeAsyNotify(YeePayParamBean yeePayParamBean);

    /**
     * 易宝支付交易(充值)异步回调接口
     * @param yeePayParamBean
     * @return
     */
    String yeePayAsyNotify(YeePayParamBean yeePayParamBean);

    int save(Trade trade);

    boolean notifyForSuccess(String orderId, String thirdNumber);

    /**
     *
     * @param payParamBean
     * @return
     */
    Map<String,Object> commonPayRequest(PayParamBean payParamBean);

    /**
     * 会员提现
     * @param withdrawMQ
     * @return
     */
    String yeePayWithdraw(WithdrawMQ withdrawMQ);

    /**
     * 易宝支付提现异步回调接口
     * @param yeePayParamBean
     * @return
     */
    String yeePayWithdrawAsyNotify(YeePayParamBean yeePayParamBean);

    /**
     * 会员提现0元-扣除手续费后提现金额为0情况
     * @param memberId
     * @param orderId
     * @param amount
     * @return
     */
    String withdrawZeroMoney(String memberId, String orderId, BigDecimal amount);

    /**
     * 查询订单状态
     * @param orderId
     * @return
     */
    Map<String,String> queryOrderStatus(String orderId);

    void test();

    // 提金/换金
    SerializeObject pay(String memberId, GoodsSkuBean goodsSkuBean, int quantity, int type, long addressId, FeeBean feeBean, String cellphone, int source);
}
