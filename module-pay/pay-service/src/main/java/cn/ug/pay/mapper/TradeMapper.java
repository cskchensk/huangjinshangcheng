package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.mapper.entity.Trade;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 交易
 * @author ywl
 */
@Component
public interface TradeMapper extends BaseMapper<Trade> {

    /**
     * 更新支付状态
     * @param trade
     * @return
     */
    int updatePayStatus(Trade trade);

    /**
     * 根据订单号查询
     * @param orderId
     * @return
     */
    Trade queryTradeByOrderId(String orderId);

    /**
     * 根据扩展订单号查询
     * @param serialNumber
     * @return
     */
    Trade queryTradeBySerialNumber(String serialNumber);

}
