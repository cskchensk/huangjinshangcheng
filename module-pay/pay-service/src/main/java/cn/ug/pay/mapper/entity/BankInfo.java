package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *  银行基础信息
 *  author ywl
 */
public class BankInfo implements Serializable{

    /** id **/
    private String id;
    /** 银行编号 **/
    private String bankCode;
    /** 银行名称 **/
    private String bankName;
    /** 状态  1:正常 2:隐藏 3:维护中 **/
    private Integer status;
    /** 渠道 **/
    private Integer way;
    /** 银行logo **/
    private String img;
    /** 单笔上线金额 **/
    private BigDecimal amountOnce;
    /** 单日上限金额 **/
    private BigDecimal amountDay;
    /** 单月上线金额 **/
    private BigDecimal amountMonth;
    /** 银行排序序号 **/
    private Integer sort;
    /** 银行维护开始时间 **/
    private LocalDateTime startTime;
    /** 银行维护结束时间 **/
    private LocalDateTime endTime;
    /**银行维护开始时间字符串 **/
    private String startTimeString;
    /** 银行维护结束时间字符串 **/
    private String endTimeString;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public BigDecimal getAmountOnce() {
        return amountOnce;
    }

    public void setAmountOnce(BigDecimal amountOnce) {
        this.amountOnce = amountOnce;
    }

    public BigDecimal getAmountDay() {
        return amountDay;
    }

    public void setAmountDay(BigDecimal amountDay) {
        this.amountDay = amountDay;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }

    public BigDecimal getAmountMonth() {
        return amountMonth;
    }

    public void setAmountMonth(BigDecimal amountMonth) {
        this.amountMonth = amountMonth;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.startTimeString = startTimeString;
    }

    public String getEndTimeString() {
        return endTimeString;
    }

    public void setEndTimeString(String endTimeString) {
        this.endTimeString = endTimeString;
    }
}
