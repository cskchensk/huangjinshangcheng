package cn.ug.pay.service;


import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.GoldRecordBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 可用资产详表
 * @author kaiwotech
 */
public interface GoldRecordService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(GoldRecordBean entityBean);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	GoldRecordBean findById(String id);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param addTimeMin	最小创建时间
	 * @param addTimeMax	最大创建时间
	 * @param memberId		会员ID
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<GoldRecordBean> query(String order, String sort, int pageNum, int pageSize, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
                              String memberId, String memberName, String memberMobile, String keyword);

	/**
	 * 昨日收益
	 * @param memberId	会员ID
	 * @return			收益
	 */
	BigDecimal yesterdayIncomeAmount(String memberId);
}
