package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.pay.bean.TbillRecordBean;
import cn.ug.pay.bean.status.TbillStatusEnum;
import cn.ug.pay.mapper.entity.PayTbillLeasebackIncome;
import cn.ug.pay.mapper.entity.PayTbillStatistics;
import cn.ug.pay.service.PayTbillLeasebackIncomeService;
import cn.ug.pay.service.PayTbillStatisticsService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("tbill")
public class PayTbillStatisticsController extends BaseController {
    @Autowired
    private PayTbillStatisticsService payTbillStatisticsService;
    @Autowired
    private PayTbillLeasebackIncomeService payTbillLeasebackIncomeService;

    @GetMapping(value = "/statistics/list")
    public SerializeObject<DataTable<PayTbillStatistics>> queryStatisticsForList(@RequestHeader String accessToken, Integer menu, Page page, Order order, String mobile, String name, Integer startGram, Integer endGram) {
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        startGram = startGram == null ? -1 : startGram;
        endGram = endGram == null ? -1 : endGram;
        menu = menu == null ? 1 : menu;
        int total = payTbillStatisticsService.count(menu, mobile, name, startGram, endGram);
        page.setTotal(total);
        if (total > 0) {
            List<PayTbillStatistics> list = payTbillStatisticsService.query(menu, mobile, name, startGram, endGram, order.getOrder(), order.getSort(),
                    PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<PayTbillStatistics>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<PayTbillStatistics>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<PayTbillStatistics>()));

    }

    @GetMapping(value = "/statistics/export")
    public void queryStatisticsForExport(HttpServletResponse response, Integer menu, Order order, String mobile, String name, Integer startGram, Integer endGram) {
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        startGram = startGram == null ? -1 : startGram;
        endGram = endGram == null ? -1 : endGram;
        menu = menu == null ? 1 : menu;
        List<PayTbillStatistics> list = payTbillStatisticsService.query(menu, mobile, name, startGram, endGram, order.getOrder(), order.getSort(),0, 0);
        if (list == null) {
            list = new ArrayList<PayTbillStatistics>();
        }
        String[] columnNames = null;
        String [] columns = null;
        String fileName = null;
        if (menu == 1) {
            fileName = "累计提单统计总表";
            String[] tpColumnNames = { "序号", "手机号","用户姓名", "累计提单总克重（克）", "累计提单个数（个）", "当前提单总克重（克）", "当前提单个数（个）",
                "回租中提单总克重（克）", "回租中提单个数（个）", "待处理提单总克重（克）", "待处理提单个数（个）", "冻结中提单总克重（克）", "冻结中提单个数（个）",
                "已售提单总克重（克）", "已售提单个数（个）", "已提金提单个数（克）", "已提金提单个数（个）"};
            String[] tpColumns = {"index",  "mobile", "name", "totalGram", "totalBills", "currentGram", "currentBills", "leasebackGram", "leasebackBills",
                "pendingGram", "pendingBills", "frozenGram", "frozenBills", "soldGram", "soldBills", "extractedGram", "extractedBills"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 2) {
            fileName = "待处理提单统计总表";
            String[] tpColumnNames = { "序号", "手机号","用户姓名", "待处理提单克重（克）", "待处理提单个数（个）"};
            String[] tpColumns = {"index",  "mobile", "name", "pendingGram", "pendingBills"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 3) {
            fileName = "冻结中提单统计总表";
            String[] tpColumnNames = { "序号", "手机号","用户姓名", "冻结中提单总克重（克）", "冻结中提单个数（个）"};
            String[] tpColumns = {"index",  "mobile", "name", "frozenGram", "frozenBills"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 4) {
            fileName = "回租中提单统计总表";
            String[] tpColumnNames = { "序号", "手机号","用户姓名", "回租中提单总克重", "回租中提单个数", "回租未到期总收益（克）", "折算金额（元）",
                "对应金豆数（个）", "回租已到期总收益（克）", "折算金额（元）", "对应金豆数（个）"};
            String[] tpColumns = {"index",  "mobile", "name", "leasebackGram", "leasebackBills", "leasebackIncomeGram", "leasebackIncomeAmount",
                "leasebackIncomeBeans", "incomeGram", "incomeAmount", "incomeBeans"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 5) {
            fileName = "已售出提单统计总表";
            String[] tpColumnNames = { "序号", "手机号","用户姓名", "已售提单总克重（克）", "已售提单个数（个）", "已售提单到账总金额（元）"};
            String[] tpColumns = {"index",  "mobile", "name", "soldGram", "soldBills", "soldAmount"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        }
        int index = 1;
        for (PayTbillStatistics bean : list) {
            bean.setIndex(index);
            index++;
        }
        ExportExcelController<PayTbillStatistics> export = new ExportExcelController<PayTbillStatistics>();
        export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
    }

    @GetMapping(value = "/record/list")
    public SerializeObject<DataTable<TbillRecordBean>> queryRecordForList(@RequestHeader String accessToken, String memberId, Integer menu, Page page, Order order, String startDate, String endDate, Integer status) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        status = status == null ? 0 : status;
        menu = menu == null ? 1 : menu;
        int total = payTbillStatisticsService.countForRecords(memberId, menu, startDate, endDate, status);
        page.setTotal(total);
        if (total > 0) {
            List<TbillRecordBean> list = payTbillStatisticsService.queryForRecords(memberId, menu, startDate, endDate, status, order.getOrder(), order.getSort(),
                    PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TbillRecordBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<TbillRecordBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<TbillRecordBean>()));
    }

    @GetMapping(value = "/record/export")
    public void queryRecordForExport(HttpServletResponse response, String memberId, Integer menu, Order order, String startDate, String endDate, Integer status) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        status = status == null ? 0 : status;
        menu = menu == null ? 1 : menu;
        List<TbillRecordBean> list = payTbillStatisticsService.queryForRecords(memberId, menu, startDate, endDate, status, order.getOrder(), order.getSort(), 0, 0);
        if (list == null) {
            list = new ArrayList<TbillRecordBean>();
        }
        String[] columnNames = null;
        String [] columns = null;
        String fileName = null;
        if (menu == 1) {
            fileName = "累计提单统计详表";
            String[] tpColumnNames = { "序号", "提单编号","提单变更状态", "提单克重", "提单生成时间"};
            String[] tpColumns = {"index",  "orderNO", "statusRemark", "totalGram", "addTime"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 2) {
            fileName = "待处理提单统计详表";
            String[] tpColumnNames = { "序号", "提单编号","提单克重（克）", "提单生成时间", "提单来源"};
            String[] tpColumns = {"index",  "orderNO", "totalGram", "addTime", "source"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 3) {
            fileName = "冻结中提单统计详表";
            String[] tpColumnNames = { "序号", "提单编号","提单克重（克）", "提单冻结时间", "提单来源"};
            String[] tpColumns = {"index",  "orderNO", "totalGram", "frozenTime", "source"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        } else if (menu == 5) {
            fileName = "已售出提单统计详表";
            String[] tpColumnNames = { "序号", "提单编号","提单克重（克）", "出售到账金额（元）", "出售时间"};
            String[] tpColumns = {"index",  "orderNO", "totalGram", "soldAmount", "soldTime"};
            columnNames = tpColumnNames;
            columns = tpColumns;
        }
        int index = 1;
        for (TbillRecordBean bean : list) {
            bean.setIndex(index);
            bean.setStatusRemark(TbillStatusEnum.getRemartByStatus(bean.getStatus()));
            index++;
        }
        ExportExcelController<TbillRecordBean> export = new ExportExcelController<TbillRecordBean>();
        export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
    }

    @GetMapping(value = "/leaseback/record/list")
    public SerializeObject<DataTable<PayTbillLeasebackIncome>> queryRecordForList(@RequestHeader String accessToken, String memberId, Page page, Order order, String startDate, String endDate) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        int total = payTbillLeasebackIncomeService.count(memberId, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<PayTbillLeasebackIncome> list = payTbillLeasebackIncomeService.query(memberId, startDate, endDate, order.getOrder(), order.getSort(),
                    PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<PayTbillLeasebackIncome>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<PayTbillLeasebackIncome>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<PayTbillLeasebackIncome>()));
    }

    @GetMapping(value = "/leaseback/record/export")
    public void queryRecordForExport(HttpServletResponse response, String memberId, Order order, String startDate, String endDate) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        List<PayTbillLeasebackIncome> list = payTbillLeasebackIncomeService.query(memberId, startDate, endDate, order.getOrder(), order.getSort(), 0, 0);
        if (list == null) {
            list = new ArrayList<PayTbillLeasebackIncome>();
        }
        String fileName = "回租提单详表";
        String[] columnNames = { "序号", "日期","回租未到期收益（克）", "折算金额（元）", "对应金豆（个）", "回租已到期收益（克）", "折算金额（元)", "对应金豆（个）"};
        String[] columns = {"index",  "incomeDay", "leasebackIncomeGram", "leasebackIncomeAmount", "leasebackIncomeBeans", "incomeGram", "incomeAmount", "incomeBeans"};

        int index = 1;
        for (PayTbillLeasebackIncome bean : list) {
            bean.setIndex(index);
            index++;
        }
        ExportExcelController<PayTbillLeasebackIncome> export = new ExportExcelController<PayTbillLeasebackIncome>();
        export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
    }
}
