package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayTbill implements Serializable {
    private int id;
    private String memberId;
    private String orderNO;
    private String productId;
    //产品类型 6.实物金 8.安稳金 9.折扣金
    private Integer productType;
    //导出使用
    private String productTypeMark;
    private String productName;
    private String addTime;
    private String endTime;
    private String successTime;
    private BigDecimal goldPrice;
    private BigDecimal processingFee;
    private int beans;
    private BigDecimal beanGram;
    private BigDecimal couponGram;
    private String couponId;
    private int totalGram;
    private BigDecimal fee;
    private BigDecimal payGram;
    private BigDecimal totalAmount;
    private BigDecimal payAmount;
    private int portion;
    private int status;
    private String source;
    private String frozenTime;
    private String modifyTime;
    private int lockDays;
    private int leasebackTimes;
    private int readed;
    private BigDecimal discountAmount;
    private BigDecimal firstAmount;
    private int firstAmountType;
    private int discountAmountType;
    private int leaseId;
    private String ticketId;

    private int index;
    private String name;
    private String mobile;
    private String statusMark;
    private String couponMark;
    private String lockMark;
    private int stopPayTimeSecond;
    private int unpaidTimeMinute;
    private int incomeType;

    private int leasebackDays;
    private String leasebackStartTime;
    private String leasebackEndTime;
    private String leasebackActualTime;
    private int leasebackDeductibleDays;
    //回租奖励（元）
    private BigDecimal leasebackIncomeAmount;
    private int leasebackIncomeBeans;
    private int leasebackIncomeType;
    private BigDecimal leasebackDiscountAmount;
    private int leasebackDiscountAmountType;
    //回租黄金红包奖励金额
    private BigDecimal leasebackCouponAwardAmount;
    //回租金生金奖励
    private BigDecimal leasebackGoldGiveGoldAmount;
    /**
     * 回租金价
     */
    private double leasebackPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getLeasebackDiscountAmount() {
        return leasebackDiscountAmount;
    }

    public void setLeasebackDiscountAmount(BigDecimal leasebackDiscountAmount) {
        this.leasebackDiscountAmount = leasebackDiscountAmount;
    }

    public int getLeasebackDiscountAmountType() {
        return leasebackDiscountAmountType;
    }

    public void setLeasebackDiscountAmountType(int leasebackDiscountAmountType) {
        this.leasebackDiscountAmountType = leasebackDiscountAmountType;
    }

    public int getLeasebackIncomeBeans() {
        return leasebackIncomeBeans;
    }

    public void setLeasebackIncomeBeans(int leasebackIncomeBeans) {
        this.leasebackIncomeBeans = leasebackIncomeBeans;
    }

    public int getLeasebackIncomeType() {
        return leasebackIncomeType;
    }

    public void setLeasebackIncomeType(int leasebackIncomeType) {
        this.leasebackIncomeType = leasebackIncomeType;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public String getLeasebackStartTime() {
        return leasebackStartTime;
    }

    public void setLeasebackStartTime(String leasebackStartTime) {
        this.leasebackStartTime = leasebackStartTime;
    }

    public String getLeasebackEndTime() {
        return leasebackEndTime;
    }

    public void setLeasebackEndTime(String leasebackEndTime) {
        this.leasebackEndTime = leasebackEndTime;
    }

    public String getLeasebackActualTime() {
        return leasebackActualTime;
    }

    public void setLeasebackActualTime(String leasebackActualTime) {
        this.leasebackActualTime = leasebackActualTime;
    }

    public int getLeasebackDeductibleDays() {
        return leasebackDeductibleDays;
    }

    public void setLeasebackDeductibleDays(int leasebackDeductibleDays) {
        this.leasebackDeductibleDays = leasebackDeductibleDays;
    }

    public BigDecimal getLeasebackIncomeAmount() {
        return leasebackIncomeAmount;
    }

    public void setLeasebackIncomeAmount(BigDecimal leasebackIncomeAmount) {
        this.leasebackIncomeAmount = leasebackIncomeAmount;
    }

    public int getLeaseId() {
        return leaseId;
    }

    public void setLeaseId(int leaseId) {
        this.leaseId = leaseId;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public int getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(int incomeType) {
        this.incomeType = incomeType;
    }

    public int getFirstAmountType() {
        return firstAmountType;
    }

    public void setFirstAmountType(int firstAmountType) {
        this.firstAmountType = firstAmountType;
    }

    public int getDiscountAmountType() {
        return discountAmountType;
    }

    public void setDiscountAmountType(int discountAmountType) {
        this.discountAmountType = discountAmountType;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getFirstAmount() {
        return firstAmount;
    }

    public void setFirstAmount(BigDecimal firstAmount) {
        this.firstAmount = firstAmount;
    }

    public int getStopPayTimeSecond() {
        return stopPayTimeSecond;
    }

    public void setStopPayTimeSecond(int stopPayTimeSecond) {
        this.stopPayTimeSecond = stopPayTimeSecond;
    }

    public int getUnpaidTimeMinute() {
        return unpaidTimeMinute;
    }

    public void setUnpaidTimeMinute(int unpaidTimeMinute) {
        this.unpaidTimeMinute = unpaidTimeMinute;
    }

    public int getPortion() {
        return portion;
    }

    public void setPortion(int portion) {
        this.portion = portion;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public int getReaded() {
        return readed;
    }

    public void setReaded(int readed) {
        this.readed = readed;
    }

    public int getLeasebackTimes() {
        return leasebackTimes;
    }

    public void setLeasebackTimes(int leasebackTimes) {
        this.leasebackTimes = leasebackTimes;
    }

    public String getCouponMark() {
        return couponMark;
    }

    public void setCouponMark(String couponMark) {
        this.couponMark = couponMark;
    }

    public String getLockMark() {
        return lockMark;
    }

    public void setLockMark(String lockMark) {
        this.lockMark = lockMark;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getStatusMark() {
        return statusMark;
    }

    public void setStatusMark(String statusMark) {
        this.statusMark = statusMark;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public BigDecimal getProcessingFee() {
        return processingFee;
    }

    public void setProcessingFee(BigDecimal processingFee) {
        this.processingFee = processingFee;
    }

    public int getBeans() {
        return beans;
    }

    public void setBeans(int beans) {
        this.beans = beans;
    }

    public BigDecimal getBeanGram() {
        return beanGram;
    }

    public void setBeanGram(BigDecimal beanGram) {
        this.beanGram = beanGram;
    }

    public BigDecimal getCouponGram() {
        return couponGram;
    }

    public void setCouponGram(BigDecimal couponGram) {
        this.couponGram = couponGram;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getPayGram() {
        return payGram;
    }

    public void setPayGram(BigDecimal payGram) {
        this.payGram = payGram;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFrozenTime() {
        return frozenTime;
    }

    public void setFrozenTime(String frozenTime) {
        this.frozenTime = frozenTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getLockDays() {
        return lockDays;
    }

    public void setLockDays(int lockDays) {
        this.lockDays = lockDays;
    }

    public BigDecimal getLeasebackCouponAwardAmount() {
        return leasebackCouponAwardAmount;
    }

    public void setLeasebackCouponAwardAmount(BigDecimal leasebackCouponAwardAmount) {
        this.leasebackCouponAwardAmount = leasebackCouponAwardAmount;
    }

    public BigDecimal getLeasebackGoldGiveGoldAmount() {
        return leasebackGoldGiveGoldAmount;
    }

    public void setLeasebackGoldGiveGoldAmount(BigDecimal leasebackGoldGiveGoldAmount) {
        this.leasebackGoldGiveGoldAmount = leasebackGoldGiveGoldAmount;
    }

    public double getLeasebackPrice() {
        return leasebackPrice;
    }

    public void setLeasebackPrice(double leasebackPrice) {
        this.leasebackPrice = leasebackPrice;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getProductTypeMark() {
        return productTypeMark;
    }

    public void setProductTypeMark(String productTypeMark) {
        this.productTypeMark = productTypeMark;
    }
}
