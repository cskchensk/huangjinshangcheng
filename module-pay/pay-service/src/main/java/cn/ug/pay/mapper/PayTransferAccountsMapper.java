package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayTransferAccounts;
import cn.ug.pay.mapper.entity.TransferAccounts;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Author zhangweijie
 * @Date 2019/7/22 0022
 * @time 下午 16:35
 **/
public interface PayTransferAccountsMapper {

    int insertSelective(PayTransferAccounts payTransferAccounts);

    int updateByPrimaryKeySelective(PayTransferAccounts payTransferAccounts);

    PayTransferAccounts selectByPrimaryKey(@Param("id") String id);

    PayTransferAccounts selectByAccountAndNumbers(Map<String, Object> params);

    //列表查询
    List<TransferAccounts> queryList(Map<String, Object> params);
}
