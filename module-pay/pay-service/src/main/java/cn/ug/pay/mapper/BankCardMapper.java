package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.response.BankCardFindBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.mapper.entity.BankCard;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ywl
 * @date 2018/2/1
 */
@Component
public interface BankCardMapper extends BaseMapper<BankCard> {

    List<BankCardManageBean> queryBankCardList(BankCardParamBean bankCardParamBean);

    /**
     * 获取会员已绑定银行
     * @param memberId
     * @return
     */
    BankCardFindBean queryMemberBankCard(String memberId);

    BankCard findByMemberId(String memberId);

    /**
     * 根据银行卡卡号获取银行卡信息
     * @param cardNo
     * @return
     */
    BankCard findByCardNo(String cardNo);

    /**
     * 根据会员ID获取默认绑定银行卡信息
     * @param memberId
     * @return
     */
    BankCard findDefaultByMemberId(String memberId);
}

