package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayAccountRecord implements Serializable {
    private int id;
    private String memberId;
    private String addDate;
    private BigDecimal totalGram;
    private BigDecimal currentPrincipalGram;
    private BigDecimal currentInterestGram;
    private BigDecimal regularPrincipalGram;
    private BigDecimal regularInterestGram;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAddDate() {
        return addDate;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public BigDecimal getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(BigDecimal totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getCurrentPrincipalGram() {
        return currentPrincipalGram;
    }

    public void setCurrentPrincipalGram(BigDecimal currentPrincipalGram) {
        this.currentPrincipalGram = currentPrincipalGram;
    }

    public BigDecimal getCurrentInterestGram() {
        return currentInterestGram;
    }

    public void setCurrentInterestGram(BigDecimal currentInterestGram) {
        this.currentInterestGram = currentInterestGram;
    }

    public BigDecimal getRegularPrincipalGram() {
        return regularPrincipalGram;
    }

    public void setRegularPrincipalGram(BigDecimal regularPrincipalGram) {
        this.regularPrincipalGram = regularPrincipalGram;
    }

    public BigDecimal getRegularInterestGram() {
        return regularInterestGram;
    }

    public void setRegularInterestGram(BigDecimal regularInterestGram) {
        this.regularInterestGram = regularInterestGram;
    }
}
