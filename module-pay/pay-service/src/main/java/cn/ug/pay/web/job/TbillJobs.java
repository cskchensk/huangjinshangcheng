package cn.ug.pay.web.job;

import cn.ug.bean.base.Page;
import cn.ug.config.RedisGlobalLock;
import cn.ug.pay.mapper.PayTbillLeasebackIncomeMapper;
import cn.ug.pay.mapper.PayTbillLeasebackMapper;
import cn.ug.pay.mapper.entity.PayExperienceOrder;
import cn.ug.pay.mapper.entity.PayTbillLeaseback;
import cn.ug.pay.mapper.entity.PayTbillLeasebackIncome;
import cn.ug.pay.mapper.entity.PayTbillStatistics;
import cn.ug.pay.service.PayTbillLeasebackService;
import cn.ug.pay.service.PayTbillService;
import cn.ug.pay.service.PayTbillStatisticsService;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class TbillJobs implements Serializable {
    private Log log = LogFactory.getLog(TbillJobs.class);
    @Autowired
    private PayTbillStatisticsService payTbillStatisticsService;
    @Autowired
    private PayTbillLeasebackIncomeMapper payTbillLeasebackIncomeMapper;
    @Autowired
    private PayTbillService payTbillService;
    @Autowired
    private RedisGlobalLock redisGlobalLock;
    @Autowired
    private PayTbillLeasebackMapper payTbillLeasebackMapper;
    @Autowired
    private PayTbillLeasebackService payTbillLeasebackService;

    /**
     * 每天凌晨2点3点4点执行
     */
    @Scheduled(cron = "0 0 1,2,3 * * ?")
    //@Scheduled(cron = "0 14 14 * * ?")
    public void tbillIncomeJob() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
        String key = "TbillJobs:tbillIncomeJob:" + format.format(Calendar.getInstance().getTime());
        if (!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
            return;
        }
        log.info("回租奖励发放 开始");
        log.info(UF.getFormatDateTime(UF.getDateTime()));
        do {
            log.info("实物金回租奖励发放 开始");
            List<PayTbillLeaseback> list = payTbillLeasebackMapper.queryBecomeDueBills();
            if (null == list || list.size() == 0) {
                log.info("实物金回租奖励发放 结束");
                break;
            }

            for (PayTbillLeaseback o : list) {
                payTbillLeasebackService.completeBill(o.getOrderNO());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } while (true);

        do {
            log.info("体验金回租奖励发放 开始");
            List<PayExperienceOrder> list = payTbillService.queryBecomeDueOrders();
            if (null == list || list.size() == 0) {
                log.info("体验金回租奖励发放 结束");
                return;
            }

            for (PayExperienceOrder o : list) {
                payTbillService.completeOrder(o.getOrderNO());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } while (true);
    }

    @Scheduled(cron = "0/10 * * * * ?")
    public void expiredTbillJob() {
        payTbillService.expired();
    }

    @Scheduled(cron = "0 50 23 * * ?")
    //@Scheduled(cron = "0 57 9 * * ?")
    public void getEveryIncomeJob() {
        String key = "TbillJobs:getEveryIncomeJob:" + UF.getFormatDateTime("yyyy-MM-dd", UF.getDateTime());
        if (!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
            return;
        }
        log.info("统计会员提单回租利息 开始");

        Page page = new Page();
        page.setPageSize(500);
        page.setPageNum(1);
        do {
            List<PayTbillStatistics> data = payTbillStatisticsService.queryIncomeForList(PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));

            if (null == data || data.size() == 0) {
                log.info("统计会员提单回租利息 结束");
                return;
            }
            List<PayTbillLeasebackIncome> records = new ArrayList<PayTbillLeasebackIncome>();
            try {
                for (PayTbillStatistics o : data) {
                    PayTbillLeasebackIncome record = new PayTbillLeasebackIncome();
                    record.setIncomeDay(UF.getFormatDateNow());
                    record.setIncomeAmount(o.getIncomeAmount());
                    record.setMemberId(o.getMemberId());
                    record.setIncomeBeans(o.getIncomeBeans());
                    record.setIncomeGram(o.getIncomeGram());
                    record.setLeasebackIncomeAmount(o.getLeasebackIncomeAmount());
                    record.setLeasebackIncomeBeans(o.getLeasebackIncomeBeans());
                    record.setLeasebackIncomeGram(o.getLeasebackIncomeGram());
                    records.add(record);
                }
            } catch (Exception e) {
                continue;
            }
            payTbillLeasebackIncomeMapper.insertInBatch(records);
            page.setPageNum(page.getPageNum() + 1);
        } while (true);
    }
}


