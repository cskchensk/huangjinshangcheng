package cn.ug.pay.service.impl;

import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.bean.response.BankInfoBean;
import cn.ug.pay.bean.status.CommonConstants;
import cn.ug.pay.mapper.BankInfoMapper;
import cn.ug.pay.mapper.entity.BankInfo;
import cn.ug.pay.service.BankInfoService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.ug.config.CacheType.SEARCH;

/**
 * @author ywl
 * @date 2018/2/1
 */
@Service
public class BankInfoServiceImpl implements BankInfoService {

    @Resource
    private BankInfoMapper bankInfoMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<BankInfoBean> findList(BankInfoParamBean bankInfoParamBean) {
        com.github.pagehelper.Page<BankCardManageBean> pages = PageHelper.startPage(bankInfoParamBean.getPageNum(),bankInfoParamBean.getPageSize());
        List<BankInfoBean> dataList = bankInfoMapper.findList(bankInfoParamBean);
        return new DataTable<>(bankInfoParamBean.getPageNum(), bankInfoParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public BankInfoBean findById(String id) {
        BankInfo bankInfo = bankInfoMapper.findById(id);
        BankInfoBean bankInfoBean = dozerBeanMapper.map(bankInfo, BankInfoBean.class);
        bankInfoBean.setStartTimeString(UF.getFormatDateTime(bankInfo.getStartTime()));
        bankInfoBean.setEndTimeString(UF.getFormatDateTime(bankInfo.getEndTime()));
        return bankInfoBean;
    }

    @Override
    @SaveCache(cacheType = SEARCH)
    public List<BankInfoBean> queryBankInfoList() {
        return bankInfoMapper.queryBankList();
    }

    @Override
    public void save(BankInfoBean bankInfoBean) {
        //校验
        Ensure.that(bankInfoBean.getBankName()).isNull("17000801");
        Ensure.that(bankInfoBean.getBankCode()).isNull("17000802");
        Ensure.that(bankInfoBean.getStatus()).isNull("17000803");
        Ensure.that(bankInfoBean.getWay()).isNull("17000804");
        Ensure.that(bankInfoBean.getImg()).isNull("17000805");

        BankInfo bankInfo = dozerBeanMapper.map(bankInfoBean, BankInfo.class);

        if(StringUtils.isNotBlank(bankInfoBean.getStartTimeString())){
            bankInfo.setStartTime(UF.getDate(bankInfoBean.getStartTimeString()));
        }
        if(StringUtils.isNotBlank(bankInfoBean.getEndTimeString())){
            bankInfo.setEndTime(UF.getDate(bankInfoBean.getEndTimeString()));
        }
        //新增
        if(StringUtils.isBlank(bankInfo.getId())){
            bankInfo.setId(UF.getRandomUUID());
            bankInfoMapper.insert(bankInfo);
        }else{
            bankInfoMapper.update(bankInfo);
        }
    }

    @Override
    public void deleted(String id []) {
        bankInfoMapper.deleteByIds(id);
    }

    @Override
    public void bankInfoJob() {
        //获取维护时间
        List<BankInfo> bankInfoList = bankInfoMapper.findBankInfoJob();
        if(bankInfoList != null &&  !bankInfoList.isEmpty()){
            for(BankInfo bankInfo:bankInfoList){
                LocalDateTime startTime = bankInfo.getStartTime();
                LocalDateTime endTime = bankInfo.getEndTime();
                LocalDateTime currentTime = LocalDateTime.now();
                //验证
               Map<String,Object> param = new HashMap<String,Object>();
                //当前时间大于结束时间,那么设置银行状态为正常
                if(endTime != null && currentTime.compareTo(endTime) >= 0){
                    param.put("id", bankInfo.getId());
                    param.put("status", CommonConstants.CardStatus.NORMAL.getIndex());
                    param.put("startTime", null);
                    param.put("endTime", null);
                    bankInfoMapper.updateBankInfoJob(param);
                }else{
                    //当前时间大于维护开始时间,那么设置为维护中
                    if(currentTime.compareTo(startTime) >= 0 && bankInfo.getStatus() == CommonConstants.CardStatus.NORMAL.getIndex()){
                        param.put("id", bankInfo.getId());
                        param.put("status", CommonConstants.CardStatus.MAINTENANCE.getIndex());
                        param.put("startTime", bankInfo.getStartTime());
                        param.put("endTime", bankInfo.getEndTime());
                        bankInfoMapper.updateBankInfoJob(param);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        LocalDateTime dt1 = UF.getDate("2018-04-25 15:00:00");
        LocalDateTime dt2 = LocalDateTime.now();
        System.out.println(dt1.compareTo(dt2));
    }
}
