package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.WithdrawSettingBean;

import java.util.List;

/**
 * 提现设置
 */
public interface WithdrawSettingService {

    /**
     * 后台
     * 获取提现限额列表
     * @return
     */
    DataTable<WithdrawSettingBean> findList(BankInfoParamBean bankInfoParamBean);

    /**
     * 根据id获取-后台
     * @param id
     * @return
     */
    WithdrawSettingBean findById(String id);

    WithdrawSettingBean findByBankCode(String bankCode);
    /**
     * 新增或修改基础信息
     * @param withdrawSettingBean
     */
    void save(WithdrawSettingBean withdrawSettingBean);

}
