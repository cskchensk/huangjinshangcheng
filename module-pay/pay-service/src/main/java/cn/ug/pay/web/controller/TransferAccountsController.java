package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.pay.bean.TransferAccountsBean;
import cn.ug.pay.bean.TransferAccountsQueryBean;
import cn.ug.pay.bean.TransferAccountsResultBean;
import cn.ug.pay.service.TransferAccountsService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * @Author zhangweijie
 * @Date 2019/7/22 0022
 * @time 下午 17:33
 **/
@RestController
@RequestMapping("transfer")
public class TransferAccountsController {

    @Autowired
    private TransferAccountsService transferAccountsService;

    /**
     * 转账申请
     *
     * @return 记录集
     */
    @RequestMapping(value = "save", method = POST)
    public SerializeObject save(@RequestHeader String accessToken, TransferAccountsBean transferAccountsBean) {
        //参数校验
        Object object = checkParams(transferAccountsBean);

        if (object instanceof SerializeObjectError) {
            return (SerializeObjectError) object;
        }

        return transferAccountsService.save(transferAccountsBean);
    }


    /**
     * 列表查询
     *
     * @return 记录集
     */
    @RequestMapping(value = "list", method = GET)
    public SerializeObject queryList(@RequestHeader String accessToken, TransferAccountsQueryBean transferAccountsQueryBean) {
        return new SerializeObject(ResultType.NORMAL, transferAccountsService.queryList(transferAccountsQueryBean));
    }


    /**
     * 转账审核
     *
     * @param accessToken
     * @param id          转账申请id
     * @param status      状态 2:已通过 3:已拒绝
     * @param remark      备注。status等于3的时必输
     * @return
     */
    @RequestMapping(value = "updateAuditStatus", method = PUT)
    public SerializeObject updateAuditStatus(@RequestHeader String accessToken, String id, Integer status, String remark) {
        if (StringUtils.isBlank(id)) {
            return new SerializeObjectError("170020485");
        }

        if (status == null) {
            return new SerializeObjectError("170020486");
        }


        if (status == 1) {
            return new SerializeObjectError("170020491");
        }

        if (status == 3 && StringUtils.isBlank(remark)) {
            return new SerializeObjectError("170020487");
        }
        return transferAccountsService.updateAuditStatus(id, status, remark);
    }

    /**
     * 运营导出列表
     *
     * @param transferAccountsQueryBean
     * @param response
     */
    @RequestMapping(value = "list/export", method = GET)
    public void listExport(TransferAccountsQueryBean transferAccountsQueryBean, HttpServletResponse response) {
        transferAccountsQueryBean.setPageNum(1);
        transferAccountsQueryBean.setPageSize(Integer.MAX_VALUE);
        DataTable<TransferAccountsResultBean> dataTable = transferAccountsService.queryList(transferAccountsQueryBean);
        if (transferAccountsQueryBean.getExportType() == 1) {
            String fileName = "用户对公转账审核表";
            String[] columnNames = {"序号", "用户姓名", "用户账号", "身份证号码", "用户转账单号", "用户转账户名", "用户转账账号", "用户转账开户行", "转账金额（元）", "审核状态", "申请人", "申请时间"};
            String[] columns = {"index", "mobile", "name", "idCard", "transferNumbers", "transferName", "bankAccount", "openBank", "amount", "statusRemark", "createUser", "addTime"};
            int index = 1;
            for (TransferAccountsResultBean bean : dataTable.getDataList()) {
                bean.setIndex(index);
                bean.setStatusRemark(bean.getAuditStatus() == 1 ? "未审核" : bean.getAuditStatus() == 2 ? "已通过" : "已拒绝");
                index++;
            }
            ExportExcelController<TransferAccountsResultBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, dataTable.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
        } else {
            String fileName = "用户对公转账审核记录表";
            String[] columnNames = {"序号", "用户姓名", "用户账号", "身份证号码", "用户转账单号", "用户转账户名", "用户转账账号", "用户转账开户行", "转账金额（元）", "申请人", "申请时间", "审核状态", "审核人", "审核时间"};
            String[] columns = {"index", "mobile", "name", "idCard", "transferNumbers", "transferName", "bankAccount", "openBank", "amount", "createUser", "addTime", "statusRemark", "auditUser", "auditTime"};
            int index = 1;
            for (TransferAccountsResultBean bean : dataTable.getDataList()) {
                bean.setIndex(index);
                bean.setStatusRemark(bean.getAuditStatus() == 1 ? "未审核" : bean.getAuditStatus() == 2 ? "已通过" : "已拒绝");
                index++;
            }
            ExportExcelController<TransferAccountsResultBean> export = new ExportExcelController<>();
            export.exportExcel(fileName, fileName, columnNames, columns, dataTable.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }


    //参数校验
    private Object checkParams(TransferAccountsBean transferAccountsBean) {
        if (StringUtils.isBlank(transferAccountsBean.getMemberId())) {
            return new SerializeObjectError("170020490");
        }


        //转账单号
        if (StringUtils.isBlank(transferAccountsBean.getTransferNumbers())) {
            return new SerializeObjectError("170020475");
        }

        if (transferAccountsBean.getTransferNumbers().length() > 30) {
            return new SerializeObjectError("170020476");
        }

        //转账户名
        if (StringUtils.isBlank(transferAccountsBean.getTransferName())) {
            return new SerializeObjectError("170020477");
        }

        if (transferAccountsBean.getTransferName().length() > 50) {
            return new SerializeObjectError("170020478");
        }

        //转账开户行
        if (StringUtils.isBlank(transferAccountsBean.getOpenBank())) {
            return new SerializeObjectError("170020479");
        }

        if (transferAccountsBean.getOpenBank().length() > 30) {
            return new SerializeObjectError("170020480");
        }

        //转账账号
        if (StringUtils.isBlank(transferAccountsBean.getBankAccount())) {
            return new SerializeObjectError("170020481");
        }

        if (transferAccountsBean.getBankAccount().length() > 30) {
            return new SerializeObjectError("170020482");
        }

        //转账金额
        if (transferAccountsBean.getAmount() == null) {
            return new SerializeObjectError("170020483");
        }

        if (transferAccountsBean.getAmount().compareTo(new BigDecimal(10000000)) > 0) {
            return new SerializeObjectError("170020484");
        }

        if (StringUtils.isBlank(transferAccountsBean.getVoucherImg())) {
            return new SerializeObjectError("170020488");
        }
        return new SerializeObject();
    }
}
