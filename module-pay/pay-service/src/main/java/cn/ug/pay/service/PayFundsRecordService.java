package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayFundsRecord;

import java.util.List;

public interface PayFundsRecordService {
    boolean save(List<PayFundsRecord> records);

    List<PayFundsRecord> query(String memberId, String startDate, String endDate, int offset, int size);
    int count(String memberId, String startDate, String endDate);

    List<PayFundsRecord> queryActiveAccountForList(int offset, int size);
}
