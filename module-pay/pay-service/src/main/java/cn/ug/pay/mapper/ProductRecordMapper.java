package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.ProductListDo;
import cn.ug.pay.mapper.entity.ProductOrder;

import java.util.List;
import java.util.Map;

public interface ProductRecordMapper {

    List<ProductListDo> queryProductList(Map<String, Object> params);

    Integer queryProductorderCount(String productId);

    Integer queryCount(String productId);

    Integer queryCountExperience(String productId);
}
