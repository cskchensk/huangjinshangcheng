package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.FinanceBillBean;
import cn.ug.pay.mapper.BillMapper;
import cn.ug.pay.service.FinanceBillService;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class FinanceBillServiceImpl implements FinanceBillService {

    @Resource
    private BillMapper billMapper;

    @Override
    public DataTable<FinanceBillBean> findList(FinanceBillParam financeBillParam) {
        com.github.pagehelper.Page<FinanceBillParam> pages = PageHelper.startPage(financeBillParam.getPageNum(),financeBillParam.getPageSize());
        //必传参数验证
        Ensure.that(financeBillParam.getType()).isNull("17000901");
        Ensure.that(financeBillParam.getType() == 1 || financeBillParam.getType() == 2).isFalse("17000902");
        List<FinanceBillBean> dataList = billMapper.findFinanceBillList(financeBillParam);
        return new DataTable<>(financeBillParam.getPageNum(), financeBillParam.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public List<FinanceBillBean> queryAllList(FinanceBillParam financeBillParam) {
        return billMapper.findFinanceBillList(financeBillParam);
    }
}
