package cn.ug.pay.mapper;

import cn.ug.pay.bean.DueinGoldBeanBean;
import cn.ug.pay.bean.GoldBeanChangeBean;
import cn.ug.pay.bean.GoldBeanEverydayRecordBean;
import cn.ug.pay.mapper.entity.PayGoldBeanRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayGoldBeanRecordMapper {
    List<PayGoldBeanRecord> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<GoldBeanChangeBean> queryForBean(Map<String, Object> params);
    int countForBean(Map<String, Object> params);

    PayGoldBeanRecord findById(@Param("id")int id);
    int insert(PayGoldBeanRecord record);

    List<GoldBeanEverydayRecordBean> queryForEveryRecord(Map<String, Object> params);
    int countForEveryRecord(Map<String, Object> params);

    List<DueinGoldBeanBean> queryForDueinBean(Map<String, Object> params);
    int countForDueinBean(Map<String, Object> params);

    GoldBeanChangeBean queryForBeanByMemberId(@Param("memberId")String memberId);

    List<PayGoldBeanRecord> queryForTask(@Param("memberId")String memberId, @Param("remark")List<String> remark);
}
