package cn.ug.pay.mapper.entity;

import java.math.BigDecimal;

/**
 *@Author zhangweijie
 *@Date 2019/9/20 0020
 *@time 下午 16:46
 **/

public class PayTbillStatistic {
    //合计人数
    private int countPersonNum;

    //合计笔数
    private int countNum;

    //合计购买克重
    private int totalGram;

    //合计够买提单金额
    private BigDecimal totalAmount = BigDecimal.ZERO;

    //合计金豆抵扣
    private BigDecimal totalBeanGram = BigDecimal.ZERO;

    //合计使用抵扣卷
    private BigDecimal totalCouponGram = BigDecimal.ZERO;

    //合计实际支付克重
    private BigDecimal totalPayGram = BigDecimal.ZERO;

    //合计实际支付金额
    private BigDecimal totalPayAmount = BigDecimal.ZERO;

    public int getCountPersonNum() {
        return countPersonNum;
    }

    public void setCountPersonNum(int countPersonNum) {
        this.countPersonNum = countPersonNum;
    }

    public int getCountNum() {
        return countNum;
    }

    public void setCountNum(int countNum) {
        this.countNum = countNum;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalBeanGram() {
        return totalBeanGram;
    }

    public void setTotalBeanGram(BigDecimal totalBeanGram) {
        this.totalBeanGram = totalBeanGram;
    }

    public BigDecimal getTotalCouponGram() {
        return totalCouponGram;
    }

    public void setTotalCouponGram(BigDecimal totalCouponGram) {
        this.totalCouponGram = totalCouponGram;
    }

    public BigDecimal getTotalPayGram() {
        return totalPayGram;
    }

    public void setTotalPayGram(BigDecimal totalPayGram) {
        this.totalPayGram = totalPayGram;
    }

    public BigDecimal getTotalPayAmount() {
        return totalPayAmount;
    }

    public void setTotalPayAmount(BigDecimal totalPayAmount) {
        this.totalPayAmount = totalPayAmount;
    }
}
