package cn.ug.pay.service.impl;

import cn.ug.activity.mq.RewardsMQ;
import cn.ug.analyse.bean.request.MemberAnalyseMsgParamBean;
import cn.ug.aop.SaveCache;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.config.YeePayConfig;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.enums.*;
import cn.ug.feign.MemberUserService;
import cn.ug.feign.RateSettingsService;
import cn.ug.mall.bean.GoldBeanProvide;
import cn.ug.member.bean.request.MemberUserBaseParmBean;
import cn.ug.member.mq.MemberLabelMQ;
import cn.ug.msg.mq.Msg;
import cn.ug.msg.mq.WxMessageParamBean;
import cn.ug.msg.mq.WxNotifyData;
import cn.ug.pay.bean.request.BankCardBaseParamBean;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.response.*;
import cn.ug.pay.bean.status.CommonConstants;
import cn.ug.pay.mapper.BankCardMapper;
import cn.ug.pay.mapper.BankInfoMapper;
import cn.ug.pay.mapper.WithdrawSettingMapper;
import cn.ug.pay.mapper.entity.BankCard;
import cn.ug.pay.mapper.entity.PayGoldBeanRecord;
import cn.ug.pay.pay.yeepay.TZTService;
import cn.ug.pay.service.BankCardService;
import cn.ug.pay.service.PayGoldBeanRecordService;
import cn.ug.pay.utils.RequestUtil;
import cn.ug.util.Common;
import cn.ug.util.IdcardValidator;
import cn.ug.util.SerialNumberWorker;
import cn.ug.util.UF;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import static cn.ug.config.CacheType.SEARCH;
import static cn.ug.config.QueueName.*;
import static cn.ug.util.RedisConstantUtil.BIND_CARD_KEY;

@Service
public class BankCardServiceImpl implements BankCardService {
    @Resource
    private BankCardMapper bankCardMapper;
    @Autowired
    private WithdrawSettingMapper withdrawSettingMapper;
    @Resource
    private BankInfoMapper bankInfoMapper;
    @Resource
    private MemberUserService memberUserService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private AmqpTemplate amqpTemplate;
    @Resource
    private DozerBeanMapper dozerBeanMapper;
    @Resource
    private YeePayConfig yeePayConfig;
    @Resource
    private RequestUtil RequestUtil;
    @Autowired
    private RateSettingsService rateSettingsService;
    @Resource
    private TZTService tZTService;
    @Autowired
    private PayGoldBeanRecordService payGoldBeanRecordService;
    private static Logger logger = LoggerFactory.getLogger(BankCardServiceImpl.class);

    @Override
    public DataTable<BankCardManageBean> queryBankCardList(BankCardParamBean bankCardParamBean) {
        com.github.pagehelper.Page<BankCardManageBean> pages = PageHelper.startPage(bankCardParamBean.getPageNum(),bankCardParamBean.getPageSize());
        List<BankCardManageBean> dataList = bankCardMapper.queryBankCardList(bankCardParamBean);
        for(BankCardManageBean entity:dataList){
            String idCard = entity.getIdCard();
            entity.setIdCard(idCard.substring(0,4) + " **** **** " +idCard.substring(idCard.length()-4, idCard.length()));
        }
        return new DataTable<>(bankCardParamBean.getPageNum(), bankCardParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public BankCardFindBean queryMemberBankCard(String memberId) {
        BankCardFindBean bankCardFindBean = bankCardMapper.queryMemberBankCard(memberId);
        //银行卡号进行过滤
        if(bankCardFindBean != null ) {
            String cardNo = bankCardFindBean.getCardNo();
            String encryptCardNo = cardNo.substring(0,4) + " **** **** " +cardNo.substring(cardNo.length()-4,cardNo.length());
            bankCardFindBean.setCardNo(encryptCardNo);
        }
        WithdrawSettingBean withdrawSettingBean = withdrawSettingMapper.findByBankCode(bankCardFindBean.getBankCode());
        if (withdrawSettingBean != null) {
            bankCardFindBean.setWithdrawAmountOnce(withdrawSettingBean.getAmountOnce());
            bankCardFindBean.setWithdrawAmountDay(withdrawSettingBean.getAmountDay());
            bankCardFindBean.setDayType(withdrawSettingBean.getDayType());
            bankCardFindBean.setOnceType(withdrawSettingBean.getOnceType());
            bankCardFindBean.setMonthType(withdrawSettingBean.getMonthType());
        }
        return bankCardFindBean;
    }

    /**
     * 绑定银行卡-首次
     * @param paramBean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void bindBandCard(BankCardBaseParamBean paramBean) {
        //验证参数
        Ensure.that(paramBean.getName()).isNull("17000301");
        Ensure.that(paramBean.getIdCard()).isNull("17000302");
        Ensure.that(paramBean.getCardNo()).isNull("17000303");
        Ensure.that(paramBean.getMobile()).isNull("17000304");
        Ensure.that(paramBean.getBankCode()).isNull("17000312");

        //验证姓名是否是汉字
        Ensure.that(new Common().checkNameChinese(paramBean.getName())).isFalse("17000314");
        //验证卡号是否正确
        Ensure.that(Common.isNumeric(paramBean.getCardNo())).isFalse("17000305");
        //验证身份证是否正确
        Ensure.that(new IdcardValidator().isValidatedAllIdcard(paramBean.getIdCard())).isFalse("17000306");
        //验证手机号码是否正确
        //Ensure.that(Common.validateMobile(paramBean.getMobile())).isFalse("17000307");
        //获取卡号信息
        BankCardInfoBean bankCardInfoBean = queryBankCardInfo(paramBean.getCardNo());
        logger.info("---获取银行卡信息是否有-----"+bankCardInfoBean);
        //验证银行卡号和银行编号是否一致
        logger.info("----bankInfo--bankCode="+paramBean.getBankCode());
        //验证银行是否支持支付
        BankInfoBean bankInfo = bankInfoMapper.queryBankInfo(paramBean.getBankCode());
        Ensure.that(bankInfo == null || bankInfo.getStatus() == 2).isTrue("17000315");

        logger.info("--------bankInfo-----"+bankInfo);
        if(bankCardInfoBean != null){
            Ensure.that(bankInfo.getBankCode().equals(bankCardInfoBean.getBankCode())).isFalse("17000313");
        }
        //验证该卡号是否已经绑定过
        BankCard bc = bankCardMapper.findByCardNo(paramBean.getCardNo());
        if(bc != null){
            Ensure.that(bc.getStatus() == CommonConstants.BankCardStatus.YES_BIND.getIndex()).isTrue("17000412");
        }
        String key = BIND_CARD_KEY + paramBean.getMemberId();
        String bindCardInfo = redisTemplate.opsForValue().get(key);
        if (StringUtils.isNotBlank(bindCardInfo)) {
            Ensure.that(true).isTrue("17002012");
        }
        //验证银行是否维护中
        /*if(bankInfo != null){
            Ensure.that(CommonConstants.CardStatus.MAINTENANCE.getIndex() == bankInfo.getStatus()).isTrue("17000423");
        }*/

        //生成商户流水号
        String orderId = SerialNumberWorker.getInstance().nextId();
        //组装
        TreeMap<String, String> dataMap =  getRequestParam(paramBean,orderId);
        logger.info("-----绑定请求参数----"+dataMap.toString());
        //发起请求
        Map<String,String> map = RequestUtil.sendYeePayHttp(yeePayConfig.getBindCardRequestURL(),dataMap);
        logger.info("-----绑定返回参数----"+map);
        if(map != null && !map.isEmpty()){
            if(map.containsKey("status")){
                String status = String.valueOf(map.get("status"));
                //待短验
                if("TO_VALIDATE".equals(status)){
                    //保存记录
                    BankCard bankCard = dozerBeanMapper.map(paramBean, BankCard.class);
                    bankCard.setId(UF.getRandomUUID());
                    bankCard.setRequestNo(orderId);
                    bankCard.setStatus(CommonConstants.BankCardStatus.NOT_BIND.getIndex());  //待绑定
                    bankCard.setPayWay(CommonConstants.PayWay.YEE_PAY.getIndex());
                    String cardNo = paramBean.getCardNo();
                    bankCard.setCardTop(cardNo.substring(0,6));
                    bankCard.setCardLast(cardNo.substring(cardNo.length()-4,cardNo.length()));
                    bankCardMapper.insert(bankCard);
                    redisTemplate.opsForValue().set(key, key, 15, TimeUnit.SECONDS);
                    //绑卡失败
                }else{
                    logger.info("-----绑卡失败-----status="+status+"-----errormsg"+map.get("errormsg"));
                    //提示用户绑卡失败
                    Ensure.that(true).isTrue(map.get("errormsg"));
                }
            }else{
                //提示错误原因
                Ensure.that(true).isTrue(map.get("customError"));
            }
        }
    }

    /**
     * 绑定银行卡确认
     * @param memberId 会员Id
     * @param code 判断验证码
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public String bindSmsConfirm(String memberId,String code) {
        String result = null;
        //验证
        Ensure.that(code).isNull("17000308");
        //判断该会员是否是待绑定状态
        BankCard bankCard = bankCardMapper.findByMemberId(memberId);
        if(bankCard != null){
            //未绑定
            if(!bankCard.getStatus().equals(CommonConstants.BankCardStatus.YES_BIND.getIndex())){
                //组装数据
                TreeMap<String, String> dataMap = getBindSmsConfirmParam(bankCard.getRequestNo(),code);
                //发起请求
                Map<String,String> map = RequestUtil.sendYeePayHttp(yeePayConfig.getBindCardConfirmURL(),dataMap);
                System.out.println("-----绑卡短信验证返回参数----"+map);
                if(map != null && !map.isEmpty()){
                    if(map.containsKey("status")) {
                        String status = String.valueOf(map.get("status"));
                        String errorcode = String.valueOf(map.get("errorcode"));
                        Map<String,Object> param = new HashMap<String,Object>();
                        param.put("id",bankCard.getId());
                        if("BIND_SUCCESS".equals(status)){
                            //更新实名认证信息
                            updateMemberBaseInfo(bankCard);
                            param.put("status",    CommonConstants.BankCardStatus.YES_BIND.getIndex());         //状态
                            param.put("bankCode",  map.get("bankcode"));                                       //银行编号
                            param.put("bankName",  getBankName(map.get("bankcode")));                          //银行名称

                            if (StringUtils.isBlank(errorcode)) {
                                SerializeObject rateBean  = rateSettingsService.get(RateKeyEnum.GOLD_BEAN_PROVIDE.getKey());
                                if (rateBean != null && rateBean.getData() != null) {
                                    GoldBeanProvide goldBeanProvide = JSON.parseObject(JSONObject.toJSONString(rateBean.getData()), GoldBeanProvide.class);
                                    if (goldBeanProvide != null && goldBeanProvide.getTiedCardGoldBean() != null && goldBeanProvide.getTiedCardGoldBean() > 0) {
                                        int beans = goldBeanProvide.getTiedCardGoldBean();
                                        String remark = GoldBeanRemarkEnum.BINDING_CARD_REWARDS.getRemark();
                                        int type = 1;
                                        PayGoldBeanRecord beanRecord = new PayGoldBeanRecord();
                                        beanRecord.setOrderNO(OrderNOPrefixEnum.GB.name() + SerialNumberWorker.getInstance().nextId());
                                        beanRecord.setType(type);
                                        beanRecord.setRemark(remark);
                                        beanRecord.setGoldBean(beans);
                                        beanRecord.setGoldGram(new BigDecimal(beans/10000.0));
                                        beanRecord.setMemberId(memberId);
                                        beanRecord.setAddTime(UF.getFormatDateTime(LocalDateTime.now()));
                                        beanRecord.setSuccessTime(UF.getFormatDateTime(LocalDateTime.now()));
                                        payGoldBeanRecordService.save(beanRecord);
                                    }
                                }
                            }

                            //绑卡消息站内信--消息队列
                            Msg msg = new Msg();
                            msg.setMemberId(memberId);
                            msg.setType(cn.ug.msg.bean.status.CommonConstants.MsgType.BIND_BANK_CARD.getIndex());

                            Map<String, String> paramMap = new HashMap<>();
                            paramMap.put("cardNo", bankCard.getCardLast()+"");
                            msg.setParamMap(paramMap);

                            amqpTemplate.convertAndSend(QUEUE_MSG_SEND, msg);

                            //会员统计和发红包--消息队列
                            RewardsMQ rewardsMQ = new RewardsMQ();
                            rewardsMQ.setMemberId(memberId);
                            rewardsMQ.setScene(1);
                            amqpTemplate.convertAndSend(QUEUE_ACTIVITY_REWARDS, rewardsMQ);
                            logger.info("-----绑卡成功-----银行卡卡号="+bankCard.getCardNo());

                            //统计分析成功
                            MemberAnalyseMsgParamBean analyse = new MemberAnalyseMsgParamBean();
                            analyse.setMemberId(memberId);
                            analyse.setType(cn.ug.analyse.bean.status.CommonConstants.MemberAnalyseType.BINDING_BANKCARD.getIndex());
                            amqpTemplate.convertAndSend(QUEUE_MEMBER_ANALYSE, analyse);
                            logger.info("-----充值统计分析-----");

                            // 用户标签
                            MemberLabelMQ mq = new MemberLabelMQ();
                            mq.setType(3);
                            mq.setMemberId(memberId);
                            mq.setMobile(bankCard.getMobile());
                            mq.setIdcard(bankCard.getIdCard());
                            mq.setName(bankCard.getName());
                            logger.info("绑卡成功统计用户标签：" + JSONObject.toJSONString(mq));
                            amqpTemplate.convertAndSend(QUEUE_MEMBER_LABEL, mq);

                            /**
                             * 微信服务号消息推送  绑卡成功触发
                             */
                            WxMessageParamBean wxMessageParamBean = new WxMessageParamBean();
                            wxMessageParamBean.setMemberId(memberId);
                            wxMessageParamBean.setType(WxTemplateEnum.TIEDCARD_SUCCEED.getType());
                            WxTemplateEnum wxTemplateEnum = WxTemplateEnum.getWxTemplateByCode(WxTemplateEnum.TIEDCARD_SUCCEED.getType());
                            WxNotifyData wxNotifyData = new WxNotifyData();

                            Map<String,WxNotifyData.TemplateDataAttr> wxParamMap = new HashMap();
                            WxNotifyData.TemplateDataAttr first = new WxNotifyData.TemplateDataAttr();
                            first.setDataValue(wxTemplateEnum.getFirstData());
                            wxParamMap.put("first",first);

                            WxNotifyData.TemplateDataAttr keyword1 = new WxNotifyData.TemplateDataAttr();
                            keyword1.setDataValue(bankCard.getCardNo());
                            wxParamMap.put("keyword1",keyword1);

                            WxNotifyData.TemplateDataAttr keyword2 = new WxNotifyData.TemplateDataAttr();
                            keyword2.setDataValue(CommonConstants.BankCardStatus.YES_BIND.getName());
                            wxParamMap.put("keyword2",keyword2);

                            WxNotifyData.TemplateDataAttr remark = new WxNotifyData.TemplateDataAttr();
                            remark.setDataValue(wxTemplateEnum.getRemark());
                            wxParamMap.put("remark",remark);

                            wxNotifyData.setData(wxParamMap);
                            wxMessageParamBean.setTemplateData(wxNotifyData);
                            amqpTemplate.convertAndSend(QUEUE_MSG_WX_SEND, wxMessageParamBean);
                        }else{
                            param.put("status",CommonConstants.BankCardStatus.FAIL_BIND.getIndex());
                            param.put("description","订单状态="+status+",错误码="+map.get("errorcode")+",错误原因="+map.get("errormsg"));
                            logger.info("-----绑卡失败-----status="+status+"-----errormsg"+map.get("errormsg"));
                            result = map.get("errormsg");  //前端返回错误原因
                        }
                        //更新绑定记录
                        bankCardMapper.updateByPrimaryKeySelective(param);
                    }else{
                        //提示错误原因
                        Ensure.that(true).isTrue(map.get("customError"));
                    }
                }
            }else if(bankCard.getStatus().equals(CommonConstants.BankCardStatus.YES_BIND.getIndex())){
                //提示原因
                Ensure.that(true).isTrue("17000310");
            }
        }else{
            //提示错误原因
            Ensure.that(true).isTrue("00000005");
        }
        return result;
    }

    @Override
    public int bindBankCardReSendSms() {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean == null) Ensure.that(true).isTrue("");
        //判断该会员是否是待绑定状态
        BankCard bankCard = bankCardMapper.findByMemberId(loginBean.getId());
        if(bankCard == null){
            //未绑定
            if(bankCard.getStatus().equals(CommonConstants.BankCardStatus.NOT_BIND.getIndex())){
                //组装参数
                TreeMap<String, String> dataMap =  getBindBankCardReSendSms(bankCard.getRequestNo());
                Map<String,String> map = RequestUtil.sendYeePayHttp(yeePayConfig.getBindCardResendsmsURL(),dataMap);
                if(map != null &&  !map.isEmpty()){
                    if(map.containsKey("status")) {
                        String status = String.valueOf(map.get("status"));
                        if("TO_VALIDATE".equals(status)){
                            logger.info("----短信发送成功--");
                        }else{
                            //提示错误原因
                            logger.info("----短信发送失败--"+map.get("errormsg"));
                            Ensure.that(true).isTrue(map.get("errormsg"));
                        }
                    }else{
                        //提示错误原因
                        Ensure.that(true).isTrue(map.get("customError"));
                    }
                }
            }else if(bankCard.getStatus().equals(CommonConstants.BankCardStatus.YES_BIND.getIndex())){
                //提示原因
                Ensure.that(true).isTrue("17000310");
            }
        }else{
            //提示错误原因
            Ensure.that(true).isTrue("00000005");
        }
        return 0;
    }

    @Override
    public BankCardInfoBean queryBankCardInfo(String cardNo) {
        Ensure.that(cardNo).isNull("17000303");
        Ensure.that(Common.isNumeric(cardNo)).isFalse("17000305");

        TreeMap<String, String> dataMap	= new TreeMap<String, String>();
        dataMap.put("merchantno",   yeePayConfig.getMerchantAccount());
        dataMap.put("cardno",       cardNo);
        dataMap.put("sign",          tZTService.getSign(dataMap));

        BankCardInfoBean  bean = new BankCardInfoBean();
        Map<String,String> map = RequestUtil.sendYeePayHttp(yeePayConfig.getBankCardCheckURL(),dataMap);
        logger.info("-----查询银行卡返回信息------"+map.toString());
        if(map != null &&  !map.isEmpty()) {

            bean.setBankCode(map.get("bankcode"));
            bean.setBankName(map.get("bankname"));
            bean.setErrorMsg(map.get("errormsg") != null ? map.get("errormsg") :null);
            if(map.containsKey("cardtype")){
                if("DEBIT".equals(map.get("cardtype"))){
                    bean.setCardType(1);

                    //根据银行编号获取每笔、每日限制额度以及银行状态
                    BankInfoBean bankInfoBean = bankInfoMapper.queryBankInfo(bean.getBankCode());
                    if(bankInfoBean != null){
                        bean.setAmountOnce(bankInfoBean.getAmountOnce());
                        bean.setAmountDay(bankInfoBean.getAmountDay());
                        bean.setStatus(bankInfoBean.getStatus());
                    }else{
                        bean.setStatus(CommonConstants.CardStatus.NO_SUPPROT.getIndex());
                    }
                }else{
                    Ensure.that(true).isTrue("17000411");
                }
            }
            if(map.containsKey("isvalid")){
                if("VALID".equals(map.get("isvalid"))){
                    bean.setIsvalid(1);
                }else{
                   Ensure.that(true).isTrue("17000411");
                }
            }
        }
        return bean;
    }

    @Override
    public int validateBindBankCard(String memberId) {
        Ensure.that(memberId).isNull("17000311");
        BankCard bankCard = bankCardMapper.findDefaultByMemberId(memberId);
        if(bankCard != null) return 1;
        return 0;
    }


    @Override
    public void test() {
        /**
         * 微信服务号消息推送  绑卡成功触发
         */
        WxMessageParamBean wxMessageParamBean = new WxMessageParamBean();
        wxMessageParamBean.setMemberId("6zRutqTT77QRdGKLcEXgwr333");
        wxMessageParamBean.setType(WxTemplateEnum.TIEDCARD_SUCCEED.getType());
        WxTemplateEnum wxTemplateEnum = WxTemplateEnum.getWxTemplateByCode(WxTemplateEnum.TIEDCARD_SUCCEED.getType());
        WxNotifyData wxNotifyData = new WxNotifyData();

        Map<String,WxNotifyData.TemplateDataAttr> wxParamMap = new HashMap();
        WxNotifyData.TemplateDataAttr templateDataAttr = new WxNotifyData.TemplateDataAttr();
        templateDataAttr.setDataValue(wxTemplateEnum.getFirstData());
        wxParamMap.put("first",templateDataAttr);

        WxNotifyData.TemplateDataAttr keyword1 = new WxNotifyData.TemplateDataAttr();
        keyword1.setDataValue("62284898547564152");
        wxParamMap.put("keyword1",keyword1);

        WxNotifyData.TemplateDataAttr keyword2 = new WxNotifyData.TemplateDataAttr();
        keyword2.setDataValue(CommonConstants.BankCardStatus.NOT_BIND.getName());
        wxParamMap.put("keyword2",keyword2);

        WxNotifyData.TemplateDataAttr remark = new WxNotifyData.TemplateDataAttr();
        remark.setDataValue(wxTemplateEnum.getRemark());
        wxParamMap.put("remark",remark);

        wxNotifyData.setData(wxParamMap);
        wxMessageParamBean.setTemplateData(wxNotifyData);
        amqpTemplate.convertAndSend(QUEUE_MSG_WX_SEND, wxMessageParamBean);
    }

    /**
     * 请求数据组装
     * @param paramBean
     * @param orderId 商户订单Id
     * @return
     */
    public  TreeMap<String, String> getRequestParam(BankCardBaseParamBean paramBean,String orderId){
        TreeMap<String, String> dataMap	= new TreeMap<String, String>();
        dataMap.put("merchantno",   yeePayConfig.getMerchantAccount());
        dataMap.put("requestno",    orderId);
        dataMap.put("identityid", 	  orderId);
        dataMap.put("identitytype", CommonConstants.USER_ID);
        dataMap.put("cardno",        paramBean.getCardNo());
        dataMap.put("idcardno", 	  paramBean.getIdCard());
        dataMap.put("idcardtype", 	  CommonConstants.ID);
        dataMap.put("phone", 		  paramBean.getMobile());
        dataMap.put("username", 	  paramBean.getName());
        dataMap.put("callbackurl",  yeePayConfig.getBindCallBackUrl());
        dataMap.put("requesttime",  UF.getFormatDateTime(LocalDateTime.now()));
        dataMap.put("sign",          tZTService.getSign(dataMap));
        return dataMap;
    }

    /**
     * 绑卡确认数据组装
     * @param orderId  商户订单号
     * @param code  验证码
     * @return
     */
    public  TreeMap<String,String> getBindSmsConfirmParam(String orderId,String code){
        TreeMap<String, String> dataMap	= new TreeMap<String, String>();
        dataMap.put("merchantno",   yeePayConfig.getMerchantAccount());
        dataMap.put("requestno",    orderId);
        dataMap.put("validatecode", code);
        dataMap.put("sign",          tZTService.getSign(dataMap));
        return dataMap;
    }

    /**
     * 绑卡短信重发
     * @param orderId  商户订单号
     * @return
     */
    public  TreeMap<String,String> getBindBankCardReSendSms(String orderId){
        TreeMap<String, String> dataMap	= new TreeMap<String, String>();
        dataMap.put("merchantno",   yeePayConfig.getMerchantAccount());
        dataMap.put("requestno",    orderId);
        dataMap.put("sign",          tZTService.getSign(dataMap));
        return dataMap;
    }
    /**
     * 根据银行名称
     * @param bankCode
     * @return
     */
    @SaveCache(cacheType = SEARCH)
    public String getBankName(String bankCode){
        BankInfoBean bankInfoBean = bankInfoMapper.queryBankInfo(bankCode);
        if(bankInfoBean != null){
            return bankInfoBean.getBankName();
        }
        return  null;
    }

    /**
     * 绑卡成功后更新会员基础信息--实名认证信息
     * @param bankCard
     */
    public void updateMemberBaseInfo(BankCard bankCard){
        MemberUserBaseParmBean muParamBean = new MemberUserBaseParmBean();
        muParamBean.setId(bankCard.getMemberId());
        muParamBean.setIdCard(bankCard.getIdCard());
        if (StringUtils.isNotBlank(bankCard.getIdCard())) {
            String code = StringUtils.substring(bankCard.getIdCard(), 0,2);
            muParamBean.setProvince(ProvinceRuleEnum.getProvinceByCode(code));
        }
        muParamBean.setName(bankCard.getName());
        muParamBean.setAge(UF.getAge(bankCard.getIdCard()));
        LocalDate LocalDate = UF.getBirthday(bankCard.getIdCard());
        muParamBean.setBirthday(LocalDate.toString());
        muParamBean.setGender(UF.getGender(bankCard.getIdCard()));
        memberUserService.updateBaseInfo(muParamBean);
    }
}
