package cn.ug.pay.mapper;

import cn.ug.pay.bean.response.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ExtractionOfflineMapper {
    int insert(ExtractGoldBaseBean extractGoldBaseBean);

    List<ExtractGoldFindBean> queryList(Map<String, Object> params);

    /**
     * 修改
     * @param params
     * @return
     */
    int update(Map<String, Object> params);


    /**
     * 根据id进行查询
     */
    ExtractGoldBaseBean findById(String id);

    /**
     * 根据提单编号查询
     * @param tbillNo
     * @return
     */
    ExtractGoldBaseBean findByTbillNo(String tbillNo);

    /**
     * 根据提金订单号查询
     * @param orderNo
     * @return
     */
    ExtractGoldBaseBean findByOrderNo(String orderNo);

    /**
     * 根据会员id查询
     * @param memberId
     * @return
     */
    List<ExtractGoldFindBean> findByMemberId(String memberId);

    /**
     * 查询订单详情
     * @param orderNo
     * @return
     */
    ExtractGoldFindBean findOrder(String orderNo);

    /**
     * 查询用户提金统计
     * @param params
     * @return
     */
    List<ExtractGoldUserStatisticsBean> findUserStatistics(Map<String, Object> params);

    /**
     * 统计未读数量
     * @return
     */
    int countUnread();

    /**
     * 首页  我的提金订单列表查询
     * @param memberId
     * @return
     */
    List<ExtractGoldMyBean> findList(@Param("memberId")String memberId, @Param("status") Integer status);

    /**
     * app-查询提金提单详情
     * @param orderNo
     * @return
     */
    ExtractGoldDetail findDetail(String orderNo);

    /**
     * 查询1-已预约， 2-待收货， 3-已取消， 4-已完成是否有未读的
     * @param memberId
     * @return
     */
    List<Map<String,Object>> findNotRead(@Param("memberId")String memberId);

}
