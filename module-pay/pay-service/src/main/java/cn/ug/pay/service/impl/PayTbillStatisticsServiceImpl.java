package cn.ug.pay.service.impl;

import cn.ug.pay.bean.TbillRecordBean;
import cn.ug.pay.mapper.PayTbillStatisticsMapper;
import cn.ug.pay.mapper.entity.PayTbillStatistics;
import cn.ug.pay.service.PayTbillStatisticsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayTbillStatisticsServiceImpl implements PayTbillStatisticsService {
    @Autowired
    private PayTbillStatisticsMapper payTbillStatisticsMapper;

    @Override
    public List<PayTbillStatistics> query(int menu, String mobile, String name, int startGram, int endGram, String order, String sort, int offset, int size) {
        List<String> orders = Arrays.asList("total_bills", "total_gram", "pending_bills", "pending_gram", "leaseback_bills", "leaseback_gram",
                "leaseback_income_amount", "leaseback_income_gram", "leaseback_income_beans", "income_amount", "income_gram", "income_beans",
                "sold_bills", "sold_gram", "sold_amount", "frozen_bills", "frozen_gram", "extracted_bills", "extracted_gram", "current_bills", "current_gram");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("menu", menu);
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (startGram >= 0) {
            params.put("startGram", startGram);
        }
        if (endGram >= 0) {
            params.put("endGram", endGram);
        }
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return payTbillStatisticsMapper.query(params);
    }

    @Override
    public int count(int menu, String mobile, String name, int startGram, int endGram) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("menu", menu);
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (startGram >= 0) {
            params.put("startGram", startGram);
        }
        if (endGram >= 0) {
            params.put("endGram", endGram);
        }
        return payTbillStatisticsMapper.count(params);
    }

    @Override
    public List<TbillRecordBean> queryForRecords(String memberId, int menu, String startDate, String endDate, int status, String order, String sort, int offset, int size) {
        if (StringUtils.isBlank(memberId)) {
            return null;
        }
        List<String> orders = Arrays.asList("totalGram", "addTime", "frozenTime", "soldAmount", "soldTime");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("menu", menu);
        params.put("memberId", memberId);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("status", status);
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return payTbillStatisticsMapper.queryForRecords(params);
    }

    @Override
    public int countForRecords(String memberId, int menu, String startDate, String endDate, int status) {
        if (StringUtils.isBlank(memberId)) {
            return 0;
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("menu", menu);
        params.put("memberId", memberId);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("status", status);
        return payTbillStatisticsMapper.countForRecords(params);
    }

    @Override
    public List<PayTbillStatistics> queryIncomeForList(int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("offset", offset);
        params.put("size", size);
        return payTbillStatisticsMapper.queryIncomeForList(params);
    }

    @Override
    public PayTbillStatistics selectByMemberId(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return payTbillStatisticsMapper.selectByMemberId(memberId);
        }
        return null;
    }
}
