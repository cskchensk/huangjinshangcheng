package cn.ug.pay.utils;

import cn.ug.pay.bean.BillBean;
import cn.ug.pay.bean.request.TradeCommonParamBean;
import cn.ug.pay.bean.status.CommonConstants;
import cn.ug.pay.mapper.entity.Trade;
import cn.ug.pay.pay.yeepay.TZTService;
import cn.ug.util.UF;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 *  发起请求
 *  @auther ywl
 *  @date 2018-01-31 11:31:20
 */
public class Common {

    /**
     * 获取配置信息值
     * @param fileName  文件名称
     * @param key   键
     * @return  value  值
     */
    public static String getValue(String fileName,String key){
        InputStream in=Common.class.getClassLoader().getResourceAsStream(fileName);
        Properties pro=new Properties();
        String value=null;
        try {
            pro.load(in);
            value=pro.getProperty(key);
            return value;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取支付宝配置参数
     * @param key
     * @return
     */
    public static String getYeePayValue(String key){
        return getValue("yeepay.properties",key);
    }



    /**
     * 交易收支明细
     * @param trade
     * @return
     */
    public static BillBean getBillBean(Trade trade){
        BillBean billBean = new BillBean();
        billBean.setOrderId(trade.getOrderId());
        billBean.setMemberId(trade.getMemberId());
        billBean.setAmount(trade.getAmount());
        billBean.setActualAmount(trade.getAmount().subtract(trade.getFee()));
        billBean.setFee(trade.getFee());
        return billBean;
    }

    /**
     * 将对象转化为Map
     * @param object
     * @return
     */
    public static Map<String, String> object2Map(Object object){
        JSONObject jsonObject = (JSONObject) JSON.toJSON(object);
        Set<Map.Entry<String,Object>> entrySet = jsonObject.entrySet();
        Map<String, String> map=new HashMap<String,String>();
        for (Map.Entry<String, Object> entry : entrySet) {
            map.put(entry.getKey(), entry.getValue()+"");
        }
        return map;
    }

    /**
     * 计算日期{@code startDate}与{@code endDate}的间隔天数
     *
     * @param startDate
     * @param endDate
     * @return 间隔天数
     */
    public static long until(LocalDate startDate, LocalDate endDate){
        return startDate.until(endDate, ChronoUnit.DAYS);
    }
    /**
     * 计算日期{@code startDate}与{@code endDate}的间隔天数
     *
     * @param startDate
     * @param endDate
     * @return 间隔天数
     */
    public static long untilMonth(LocalDate startDate, LocalDate endDate){
        return startDate.until(endDate, ChronoUnit.MONTHS);
    }

}
