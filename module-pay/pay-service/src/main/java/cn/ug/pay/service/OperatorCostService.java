package cn.ug.pay.service;

import cn.ug.bean.base.DataOtherTable;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.pay.bean.OperatorCostBean;

public interface OperatorCostService {

    boolean statistics();

    /**
     * 查询
     *
     * @param order
     * @param startTime
     * @param endTime
     * @return
     */
    DataOtherTable<OperatorCostBean> list(Order order, int pageNum, int pageSize, String startTime, String endTime);

    /**
     * 查询运营详情
     * @param order
     * @param pageNum
     * @param pageSize
     * @param date
     * @param name
     * @param mobile
     * @param type 1：体验金 2：红包 3：金豆 4：回租 5：商城满减卷
     * @return
     */
    DataTable queryInfo(Order order, int pageNum, int pageSize, String date, String name, String mobile,Integer type,String orderNo);
}
