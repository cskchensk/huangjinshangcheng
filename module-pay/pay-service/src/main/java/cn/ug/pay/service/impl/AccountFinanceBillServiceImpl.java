package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.AccountFinanceBillParam;
import cn.ug.pay.bean.request.BillParam;
import cn.ug.pay.bean.type.BillType;
import cn.ug.pay.mapper.AccountFinanceBillMapper;
import cn.ug.pay.mapper.BillMapper;
import cn.ug.pay.mapper.WithdrawMapper;
import cn.ug.pay.mapper.entity.AccountFinanceBill;
import cn.ug.pay.mapper.entity.Bill;
import cn.ug.pay.service.AccountFinanceBillService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class AccountFinanceBillServiceImpl implements AccountFinanceBillService {

    @Resource
    private AccountFinanceBillMapper accountFinanceBillMapper;

    @Resource
    private BillMapper billMapper;

    @Resource
    private WithdrawMapper withdrawMapper;

    @Resource
    private Config config;

    private static Logger logger = LoggerFactory.getLogger(AccountFinanceBillServiceImpl.class);

    @Override
    public DataTable<AccountFinanceBill> findList(AccountFinanceBillParam accountFinanceBillParam) {
        com.github.pagehelper.Page<AccountFinanceBillParam> pages = PageHelper.startPage(accountFinanceBillParam.getPageNum(),accountFinanceBillParam.getPageSize());
        List<AccountFinanceBill> dataList = accountFinanceBillMapper.findList(accountFinanceBillParam);
        return new DataTable<>(accountFinanceBillParam.getPageNum(), accountFinanceBillParam.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public List<AccountFinanceBill> queryForList(AccountFinanceBillParam accountFinanceBillParam) {
        return accountFinanceBillMapper.findList(accountFinanceBillParam);
    }

    @Override
    public void accountfinanceBillJob() {

        LocalDateTime localDateTime = LocalDateTime.now();
        localDateTime  = localDateTime.plusDays(-1);
        String lastDay = UF.getFormatDate(localDateTime);
        localDateTime = localDateTime.plusDays(-1);
        String beforeYesDay = UF.getFormatDate(localDateTime);

        BigDecimal rechargeAmount = billMapper.findRechargeTotal();
        BigDecimal withdrawAmount = withdrawMapper.findLastDayWithdrawTotal();
        AccountFinanceBill entity = new AccountFinanceBill();
        entity.setId(UF.getRandomUUID());
        entity.setDay(lastDay);
        entity.setRechargeAmount(rechargeAmount);
        entity.setWithdrawAmount(withdrawAmount);
        entity.setWay(1);  //支付通道--易宝支付
        entity.setAmount(rechargeAmount.subtract(withdrawAmount));

        //设置当日余额、上一日金额
        AccountFinanceBill accountFinanceBill = accountFinanceBillMapper.findByDay(beforeYesDay);
        if(accountFinanceBill != null){
            entity.setBalanceAmount(accountFinanceBill.getBalanceAmount().add(entity.getAmount()));
            entity.setLastDayAmount(accountFinanceBill.getBalanceAmount());
        }else{
            entity.setBalanceAmount(entity.getAmount());
            entity.setLastDayAmount(BigDecimal.valueOf(0l));
        }

        accountFinanceBillMapper.insert(entity);
    }
    public void accountJob(LocalDate startTime, LocalDate endTime){


        //获取
        do{

            if(startTime.compareTo(endTime) >0){
                break;  //跳出循环
            }
            BigDecimal rechargeAmount = billMapper.findRechargeScript(startTime.toString());
            BigDecimal withdrawAmount = withdrawMapper.findDayWithdrawTotalScript(startTime.toString());
            AccountFinanceBill entity = new AccountFinanceBill();
            entity.setId(UF.getRandomUUID());
            entity.setDay(startTime.toString());
            entity.setRechargeAmount(rechargeAmount);
            entity.setWithdrawAmount(withdrawAmount);
            entity.setWay(1);  //支付通道--易宝支付
            entity.setAmount(rechargeAmount.subtract(withdrawAmount));

            LocalDate lastDay = startTime.plusDays(-1);

            //设置当日余额、上一日金额
            AccountFinanceBill accountFinanceBill = accountFinanceBillMapper.findByDay(lastDay.toString());
            if(accountFinanceBill != null){
                entity.setBalanceAmount(accountFinanceBill.getBalanceAmount().add(entity.getAmount()));
                entity.setLastDayAmount(accountFinanceBill.getBalanceAmount());
            }else{
                entity.setBalanceAmount(entity.getAmount());
                entity.setLastDayAmount(BigDecimal.valueOf(0l));
            }

            accountFinanceBillMapper.insert(entity);
            startTime = startTime.plusDays(1);
        }while (true);
    }
}
