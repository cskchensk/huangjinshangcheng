package cn.ug.pay.web.job;

import cn.ug.bean.base.SerializeObject;
import cn.ug.config.RedisGlobalLock;
import cn.ug.enums.RateKeyEnum;
import cn.ug.feign.RateSettingsService;
import cn.ug.mall.bean.GoldBean;
import cn.ug.mall.bean.GoldBeanProvideMarket;
import cn.ug.pay.service.MemberAccountService;
import cn.ug.util.DateUtils;
import cn.ug.util.UF;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;


@Component
public class GoldBeanJobs implements Serializable {
    private Log log = LogFactory.getLog(GoldBeanJobs.class);

    @Resource
    private RateSettingsService rateSettingsService;

    @Autowired
    private MemberAccountService memberAccountService;

    @Autowired
    private RedisGlobalLock redisGlobalLock;

    /***************每天凌晨执行一次************************/
    @Scheduled(cron = "0 0 0 * * ?")
    public void goldBeanJob() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
        String key = "GoldBeanJobs:GoldBeanDeductJob:" + format.format(Calendar.getInstance().getTime());
        if (!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
            return;
        }
        log.info("定时扣减用户金豆数量 开始");
        log.info(UF.getFormatDateTime(UF.getDateTime()));

        String nowDateStr = DateUtils.getCurrentDateStr2();
        SerializeObject serializeObject = rateSettingsService.get(RateKeyEnum.GOLD_BEAN.getKey());
        if (serializeObject != null && serializeObject.getData() != null) {
            GoldBean goldBean = JSON.parseObject(JSONObject.toJSONString(serializeObject.getData()), GoldBean.class);
            if (goldBean.getExpireType() == 1) {
                String monthDay = nowDateStr.substring(5, nowDateStr.length());
                if (monthDay.equals(goldBean.getAbatementDate())) {
                    memberAccountService.batchUpdate(goldBean.getAbatementType());
                    log.info("定时扣减用户金豆数量 结束");
                }
            }
            return;
        }
        return;
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void expireGoldBeanSmsSend() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH");
        String key = "GoldBeanJobs:expireGoldBeanSmsSendJob:" + format.format(Calendar.getInstance().getTime());
        if (!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
            return;
        }
        log.info("金豆到期清除短信提醒 开始");
        log.info(UF.getFormatDateTime(UF.getDateTime()));

        String nowDateStr = DateUtils.getCurrentDateStr2();
        SerializeObject serializeObject = rateSettingsService.get(RateKeyEnum.GOLD_BEAN.getKey());
        if (serializeObject != null && serializeObject.getData() != null) {
            GoldBean goldBean = JSON.parseObject(JSONObject.toJSONString(serializeObject.getData()), GoldBean.class);
            if (goldBean.getExpireType() == 1 && goldBean.getIsSmsMessage() == 1) {
                String abatementDateStr = nowDateStr.substring(0, 5) + goldBean.getAbatementDate();
                if (dateCalculate(nowDateStr, abatementDateStr) == 3) {
                    memberAccountService.expireSmsSend(goldBean.getAbatementType());
                    log.info("金豆到期清除短信提醒 结束");
                }
            }
            return;
        }
        return;
    }

    /**
     * 时间计算
     *
     * @param currentStr
     * @param abatementStr
     * @return
     */
    private static int dateCalculate(String currentStr, String abatementStr) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date abatementDate = df.parse(abatementStr);
            //当前时间DATE
            Date currentDate = df.parse(currentStr);
            int differentDay = DateUtils.differentDaysByMillisecond(currentDate, abatementDate);
            return differentDay;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
