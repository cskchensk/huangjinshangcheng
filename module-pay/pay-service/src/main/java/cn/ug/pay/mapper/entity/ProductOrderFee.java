package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 定期投资费用
 * @author kaiwotech
 */
public class ProductOrderFee extends BaseEntity implements java.io.Serializable {

	/** 订单号 */
	private String orderId;
	/** 投资记录id */
	private String productOrderId;
	/** 红包Id */
	private String couponRedId;
	/** 红包返现金额 */
	private BigDecimal couponRedAmount;
	/** 红包发放时间 */
	private LocalDateTime couponRedSendTime;
	/** 加息券Id */
	private String couponIncreaseId;
	/** 加息利率% */
	private BigDecimal couponIncreaseRate;
	/** 总年化收益率% */
	private BigDecimal totalRateOfIncome;
	/** 加息收益 */
	private BigDecimal couponIncreaseAmount;
	/** 加息收益（现金） */
	private BigDecimal couponIncreaseMoney;
	/** 加息收益发放时间 */
	private LocalDateTime couponIncreaseSendTime;
	/** 备注 */
	private String description;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductOrderId() {
		return productOrderId;
	}

	public void setProductOrderId(String productOrderId) {
		this.productOrderId = productOrderId;
	}

	public String getCouponRedId() {
		return couponRedId;
	}

	public void setCouponRedId(String couponRedId) {
		this.couponRedId = couponRedId;
	}

	public BigDecimal getCouponRedAmount() {
		return couponRedAmount;
	}

	public void setCouponRedAmount(BigDecimal couponRedAmount) {
		this.couponRedAmount = couponRedAmount;
	}

	public LocalDateTime getCouponRedSendTime() {
		return couponRedSendTime;
	}

	public void setCouponRedSendTime(LocalDateTime couponRedSendTime) {
		this.couponRedSendTime = couponRedSendTime;
	}

	public String getCouponIncreaseId() {
		return couponIncreaseId;
	}

	public void setCouponIncreaseId(String couponIncreaseId) {
		this.couponIncreaseId = couponIncreaseId;
	}

	public BigDecimal getCouponIncreaseRate() {
		return couponIncreaseRate;
	}

	public void setCouponIncreaseRate(BigDecimal couponIncreaseRate) {
		this.couponIncreaseRate = couponIncreaseRate;
	}

	public BigDecimal getTotalRateOfIncome() {
		return totalRateOfIncome;
	}

	public void setTotalRateOfIncome(BigDecimal totalRateOfIncome) {
		this.totalRateOfIncome = totalRateOfIncome;
	}

	public BigDecimal getCouponIncreaseAmount() {
		return couponIncreaseAmount;
	}

	public void setCouponIncreaseAmount(BigDecimal couponIncreaseAmount) {
		this.couponIncreaseAmount = couponIncreaseAmount;
	}

	public BigDecimal getCouponIncreaseMoney() {
		return couponIncreaseMoney;
	}

	public void setCouponIncreaseMoney(BigDecimal couponIncreaseMoney) {
		this.couponIncreaseMoney = couponIncreaseMoney;
	}

	public LocalDateTime getCouponIncreaseSendTime() {
		return couponIncreaseSendTime;
	}

	public void setCouponIncreaseSendTime(LocalDateTime couponIncreaseSendTime) {
		this.couponIncreaseSendTime = couponIncreaseSendTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
