package cn.ug.pay.service.impl;

import cn.ug.pay.bean.FeeBean;
import cn.ug.pay.bean.YeepayBill;
import cn.ug.pay.bean.type.FeeTypeEnum;
import cn.ug.pay.mapper.PayYeepayBillMapper;
import cn.ug.pay.mapper.entity.PayYeepayBill;
import cn.ug.pay.service.PayYeepayBillService;
import cn.ug.util.BigDecimalUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayYeepayBillServiceImpl implements PayYeepayBillService {
    @Autowired
    private PayYeepayBillMapper payYeepayBillMapper;

    @Override
    public boolean save(List<PayYeepayBill> bills) {
        if (bills != null && bills.size() > 0) {
            return payYeepayBillMapper.insertInBatch(bills) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<YeepayBill> query(String memberId, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("offset", offset);
            params.put("size", size);
            return payYeepayBillMapper.query(params);
        }
        return null;
    }

    @Override
    public int count(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            return payYeepayBillMapper.count(params);
        }
        return 0;
    }

    @Override
    public List<FeeBean> queryFee(int type, String mobile, String name, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalGram = BigDecimal.ZERO;
        BigDecimal totalYeepayFee = BigDecimal.ZERO;
        BigDecimal totalFee = BigDecimal.ZERO;
        BigDecimal totalActualFee = BigDecimal.ZERO;
        params.put("offset", 0);
        params.put("size", 0);
        List<FeeBean> allFeeBeans = payYeepayBillMapper.queryFee(params);
        if (allFeeBeans != null && allFeeBeans.size() > 0) {
            for (FeeBean bean : allFeeBeans) {
                if (bean.getFee() != null && bean.getFee().doubleValue() > 0 && bean.getType() == FeeTypeEnum.WITHDRAWAL.getValue()) {
                    bean.setActualFee(bean.getFee());
                    bean.setFee(bean.getActualFee().subtract(bean.getYeepayFee()));
                    //totalActualFee = totalActualFee.add(bean.getFee().add(bean.getYeepayFee()));
                }
                totalActualFee = totalActualFee.add(bean.getActualFee());
                totalAmount = totalAmount.add(bean.getAmount());
                totalGram = totalGram.add(bean.getGram());
                totalYeepayFee = totalYeepayFee.add(bean.getYeepayFee());
                totalFee = totalFee.add(bean.getFee());
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        List<FeeBean> feeBeans = payYeepayBillMapper.queryFee(params);
        if (feeBeans != null && feeBeans.size() > 0) {
            for (FeeBean bean : feeBeans) {
                if (bean.getFee() != null && bean.getFee().doubleValue() > 0 && bean.getType() == FeeTypeEnum.WITHDRAWAL.getValue()) {
                    bean.setActualFee(bean.getFee());
                    bean.setFee(bean.getActualFee().subtract(bean.getYeepayFee()));
                    //totalActualFee = totalActualFee.add(bean.getFee().add(bean.getYeepayFee()));
                }

                bean.setFee(BigDecimalUtil.to2Point(bean.getFee()));
                bean.setActualFee(BigDecimalUtil.to2Point(bean.getActualFee()));
                bean.setAmount(BigDecimalUtil.to2Point(bean.getAmount()));
                bean.setYeepayFee(BigDecimalUtil.to2Point(bean.getYeepayFee()));
                bean.setRemark(FeeTypeEnum.getRemark(bean.getType()));

                bean.setTotalActualFee(BigDecimalUtil.to2Point(totalActualFee));
                bean.setTotalAmount(BigDecimalUtil.to2Point(totalAmount));
                bean.setTotalFee(BigDecimalUtil.to2Point(totalFee));
                bean.setTotalGram(BigDecimalUtil.to5Point(totalGram));
                bean.setTotalYeepayFee(BigDecimalUtil.to2Point(totalYeepayFee));
            }
        }
        return feeBeans;
    }

    @Override
    public int countFee(int type, String mobile, String name, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return payYeepayBillMapper.countFee(params);
    }

    @Override
    public List<FeeBean> queryYeepayFee(int type, String mobile, String name, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", 0);
        params.put("size", 0);
        BigDecimal totalAmount = BigDecimal.ZERO;
        BigDecimal totalGram = BigDecimal.ZERO;
        BigDecimal totalYeepayFee = BigDecimal.ZERO;
        BigDecimal totalFee = BigDecimal.ZERO;
        BigDecimal totalActualFee = BigDecimal.ZERO;
        List<FeeBean> allFeeBeans = payYeepayBillMapper.queryYeepayFee(params);
        if (allFeeBeans != null && allFeeBeans.size() > 0) {
            for (FeeBean bean : allFeeBeans) {
                if (bean.getFee() != null && bean.getFee().doubleValue() > 0 && bean.getType() == FeeTypeEnum.WITHDRAWAL.getValue()) {
                    bean.setActualFee(bean.getFee());
                    bean.setFee(bean.getActualFee().subtract(bean.getYeepayFee()));
                    //totalActualFee = totalActualFee.add(bean.getFee().add(bean.getYeepayFee()));
                }
                totalActualFee = totalActualFee.add(bean.getActualFee());
                totalAmount = totalAmount.add(bean.getAmount());
                totalGram = totalGram.add(bean.getGram());
                totalYeepayFee = totalYeepayFee.add(bean.getYeepayFee());
                totalFee = totalFee.add(bean.getFee());
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        List<FeeBean> feeBeans = payYeepayBillMapper.queryYeepayFee(params);
        if (feeBeans != null && feeBeans.size() > 0) {
            for (FeeBean bean : feeBeans) {
                if (bean.getFee() != null && bean.getFee().doubleValue() > 0 && bean.getType() == FeeTypeEnum.WITHDRAWAL.getValue()) {
                    bean.setActualFee(bean.getFee());
                    bean.setFee(bean.getActualFee().subtract(bean.getYeepayFee()));
                    //totalActualFee = totalActualFee.add(bean.getFee().add(bean.getYeepayFee()));
                }
                bean.setFee(BigDecimalUtil.to2Point(bean.getFee()));
                bean.setActualFee(BigDecimalUtil.to2Point(bean.getActualFee()));
                bean.setAmount(BigDecimalUtil.to2Point(bean.getAmount()));
                bean.setYeepayFee(BigDecimalUtil.to2Point(bean.getYeepayFee()));
                bean.setRemark(FeeTypeEnum.getRemark(bean.getType()));

                bean.setTotalActualFee(BigDecimalUtil.to2Point(totalActualFee));
                bean.setTotalAmount(BigDecimalUtil.to2Point(totalAmount));
                bean.setTotalFee(BigDecimalUtil.to2Point(totalFee));
                bean.setTotalGram(BigDecimalUtil.to5Point(totalGram));
                bean.setTotalYeepayFee(BigDecimalUtil.to2Point(totalYeepayFee));
            }
        }
        return feeBeans;
    }

    @Override
    public int countYeepayFee(int type, String mobile, String name, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return payYeepayBillMapper.countYeepayFee(params);
    }
}
