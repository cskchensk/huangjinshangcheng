package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayTbillLeasebackIncome;

import java.util.List;

public interface PayTbillLeasebackIncomeService {
    List<PayTbillLeasebackIncome> query(String memberId, String startDate, String endDate, String order, String sort, int offset, int size);
    int count(String memberId, String startDate, String endDate);
}
