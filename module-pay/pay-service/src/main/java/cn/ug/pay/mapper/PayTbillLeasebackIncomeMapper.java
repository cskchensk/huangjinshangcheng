package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayTbillLeasebackIncome;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayTbillLeasebackIncomeMapper {
    List<PayTbillLeasebackIncome> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    int insertInBatch(@Param("beans") List<PayTbillLeasebackIncome> beans);
}
