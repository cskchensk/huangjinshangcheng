package cn.ug.pay.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.FrozenTbillBean;
import cn.ug.pay.bean.TbillCenterBean;
import cn.ug.pay.bean.TbillRecord;
import cn.ug.pay.bean.WaitingPayBean;
import cn.ug.pay.mapper.entity.PayExperienceOrder;
import cn.ug.pay.mapper.entity.PayTbill;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface PayTbillService {
    List<PayTbill> query(String mobile, String name, int startGram, int endGram, String startDate, String endDate, int status, String source, String order, String sort, int offset, int size,Integer[] productType,
                         String tbillNo,String tbillMoneyMin,String tbillMoneyMax);
    int count(String mobile, String name, int startGram, int endGram, String startDate, String endDate,  int status, String source,Integer[] productType,String tbillNo,String tbillMoneyMin,String tbillMoneyMax);

    List<FrozenTbillBean> queryForFrozenTbills(String mobile, String name, int startGram, int endGram, String startDate, String endDate, String order, String sort, int offset, int size);
    int countForFrozenTbills(String mobile, String name, int startGram, int endGram, String startDate, String endDate);

    Map save(String memberId, String productId, int num, int beans, String couponId, int leaseId, String ticketId);
    String saveExperienceOrder(String memberId, String productId, int totalGram, BigDecimal goldPrice, String hashKey);

    boolean saveExperienceGram(String memberId, int tradeType, int totalGram);
    boolean expired();
    PayTbill selectByOrderNO(String orderNO);
    void completePay(String orderNO);

    void completeOrder(String orderNO);

    List<TbillCenterBean> queryForTbillCenter(String memberId, int status, int offset, int size);
    int countForTbillCenter(String memberId, int status);

    boolean read(String orderNO);
    boolean readUndeterminedBills(String memberId);
    int countUnreadNum(String memberId, int status);

    List<WaitingPayBean> queryForWaitingTbills(String memberId, int offset, int size);
    int countForWaitingTbills(String memberId);

    List<TbillRecord> queryForRecords(String productId, int offset, int size);
    int countForRecords(String productId);

    List<PayExperienceOrder> query(String mobile, String name, String memberId, int status, String startAddTime, String endAddTime, String startOverTime, String endOverTime, String order, String sort, int offset, int size);
    int count(String mobile, String name, String memberId, int status, String startAddTime, String endAddTime, String startOverTime, String endOverTime);
    PayExperienceOrder findByOrderNO(String orderNO);

    List<PayExperienceOrder> queryBecomeDueOrders();
    int countSuccessfulNum(String memberId);
    int countExperienceSuccessfulNum(String memberId);

    List<Map<String,Object>> queryForTbillList(String orderNo);

    int queryCountForRecordsList(String memberId);

    //获取投资动态集合
    SerializeObject findInvestList(Map map);
}
