package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.response.ProductListBean;

public interface ProductRecordSercice {

    /**
     * 查询产品列表
     * @param productRecordBean
     * @return
     */
    DataTable<ProductListBean>  queryProductList(int pageNum,int pageSize,String startDate,String endDate,Integer productType,Integer timeType,Integer productStatus);
}
