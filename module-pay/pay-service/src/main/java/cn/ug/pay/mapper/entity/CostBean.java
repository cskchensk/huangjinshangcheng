package cn.ug.pay.mapper.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;

/**
 * @Author zhangweijie
 * @Date 2019/4/29 0029
 * @time 下午 14:54
 **/
public class CostBean {

    private String orderNo;

    private String memberId;

    private String mobile;

    private String name;

    /**
     * 1：体验金  2：红包 3：金豆 4：回租 5：商城满减卷
     */
    private Integer type;

    /**
     * 总成本
     */
    private BigDecimal cost;

    private String addTime;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private int index;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String remark;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
