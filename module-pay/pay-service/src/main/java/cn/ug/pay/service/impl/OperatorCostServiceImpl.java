package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataOtherTable;
import cn.ug.bean.base.Order;
import cn.ug.pay.bean.OperatorCostBean;
import cn.ug.pay.mapper.PayOperatorCostMapper;
import cn.ug.pay.mapper.entity.CostBean;
import cn.ug.pay.mapper.entity.PayOperatorCost;
import cn.ug.pay.service.OperatorCostService;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.DateUtils;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * @Author zhangweijie
 * @Date 2019/4/28 0028
 * @time 下午 14:56
 **/
@Service
public class OperatorCostServiceImpl extends BaseServiceImpl implements OperatorCostService {

    @Autowired
    private PayOperatorCostMapper payOperatorCostMapper;

    @Override
    public DataOtherTable<OperatorCostBean> list(Order order, int pageNum, int pageSize, String startTime, String endTime) {
        com.github.pagehelper.Page<OperatorCostBean> resultPage = PageHelper.startPage(pageNum, pageSize);
        List<PayOperatorCost> resultList = payOperatorCostMapper.list(getParams(null, order.getOrder(), order.getSort())
                .put("startTime", startTime)
                .put("endTime", endTime)
                .toMap());
        List<OperatorCostBean> operatorCostBeanList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(resultList)) {
            for (PayOperatorCost payOperatorCost : resultList) {
                OperatorCostBean operatorCostBean = new OperatorCostBean();
                BeanUtils.copyProperties(payOperatorCost, operatorCostBean);
                operatorCostBeanList.add(operatorCostBean);
            }
        }
        Map resultMap = payOperatorCostMapper.selectStatisticsAll(getParams(null, order.getOrder(), order.getSort())
                .put("startTime", startTime)
                .put("endTime", endTime).toMap());
        return new DataOtherTable<>(resultPage.getPageNum(), resultPage.getPageSize(), resultPage.getTotal(), operatorCostBeanList, resultMap);
    }

    @Override
    public DataOtherTable queryInfo(Order order, int pageNum, int pageSize, String date, String name, String mobile,Integer type,String orderNo) {
        com.github.pagehelper.Page<OperatorCostBean> resultPage = PageHelper.startPage(pageNum, pageSize);
        List<String> orderNos = null;
        if (org.apache.commons.lang3.StringUtils.isNotBlank(orderNo)){
            orderNos = Arrays.asList(orderNo.split(","));
        }

        List<CostBean> costBeanList = payOperatorCostMapper.queryInfoByDate(getParams(null, order.getOrder(), order.getSort())
                .put("date", date)
                .put("name", name)
                .put("mobile", mobile)
                .put("type",type)
                .put("orderNoList",org.apache.commons.lang3.StringUtils.isNotBlank(orderNo)?orderNos:null)
                .toMap());
        Map resultMap = new HashMap();
        resultMap.put("count",costBeanList.size());
        resultMap.put("allCost",CollectionUtils.isEmpty(costBeanList)?0:costBeanList.stream().map(CostBean::getCost).reduce(BigDecimal.ZERO,BigDecimal::add));
        return new DataOtherTable<>(resultPage.getPageNum(), resultPage.getPageSize(), resultPage.getTotal(), costBeanList,resultMap);
    }

    @Override
    public boolean statistics() {
        String nowDateStr = DateUtils.getCurrentDateStr2();
        /*//1.计算每日体验金奖励总成本
        BigDecimal experienceValue = payOperatorCostMapper.selectExperienceByNowDate(nowDateStr);
        if (experienceValue==null)
            experienceValue = new BigDecimal(0);*/

        //todo 2.计算每日黄金红包总成本,计算每日金豆抵扣总成本
        List<Map> result = payOperatorCostMapper.selectCouponByNowDate(nowDateStr);
       /* //3.计算每日回租福利券总成本
        BigDecimal leaseBackallValue = payOperatorCostMapper.selectLeaseBackallByNowDate(nowDateStr);
        if (leaseBackallValue == null)
            leaseBackallValue = new BigDecimal(0);*/
        //todo 4.计算每日商城满减券总成本
        Integer shopElectronicCardValue = payOperatorCostMapper.selectShopElectronicCardByNowDate(nowDateStr);
        //统计数据落地
        PayOperatorCost payOperatorCost = new PayOperatorCost();
        payOperatorCost.setExperienceIncomeAmount(new BigDecimal(0));
        payOperatorCost.setGoldBeanAllCost(new BigDecimal(result.get(0).get("goldBeanAllCost").toString()));
        payOperatorCost.setGoldCouponAllCost(new BigDecimal(result.get(1).get("goldCouponAllCost").toString()));
        payOperatorCost.setLeaseBackallCost(new BigDecimal(0));
        payOperatorCost.setShopElectronicCard(shopElectronicCardValue);
        payOperatorCost.setOperationAllCost(
                payOperatorCost.getGoldBeanAllCost().
                add(payOperatorCost.getGoldCouponAllCost()).
                add(new BigDecimal(payOperatorCost.getShopElectronicCard())));
        payOperatorCost.setDate(nowDateStr);
        int rows = payOperatorCostMapper.insertSelective(payOperatorCost);

        //计算每日体验金奖励  往前推5天
        /*List<PayOperatorCost> operatorCostList = payOperatorCostMapper.list(new HashMap());
        int index = 0;
        if (!CollectionUtils.isEmpty(operatorCostList) && index < 6){
            for (PayOperatorCost operatorCost : operatorCostList){
                ++index;
                BigDecimal resultValue = payOperatorCostMapper.selectExperienceByNowDate(operatorCost.getDate());
                if (resultValue==null)
                    resultValue = new BigDecimal(0);
                operatorCost.setExperienceIncomeAmount(resultValue);
                operatorCost.setOperationAllCost(operatorCost.getOperationAllCost().subtract(operatorCost.getExperienceIncomeAmount()).add(resultValue));
                payOperatorCostMapper.updateByPrimaryKeySelective(operatorCost);
            }
        }*/
        return rows == 1 ? true : false;
    }
}
