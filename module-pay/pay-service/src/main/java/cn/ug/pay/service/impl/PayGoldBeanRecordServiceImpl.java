package cn.ug.pay.service.impl;

import cn.ug.core.ensure.Ensure;
import cn.ug.pay.bean.DueinGoldBeanBean;
import cn.ug.pay.bean.GoldBeanChangeBean;
import cn.ug.pay.bean.GoldBeanEverydayRecordBean;
import cn.ug.pay.mapper.MemberAccountMapper;
import cn.ug.pay.mapper.PayGoldBeanRecordMapper;
import cn.ug.pay.mapper.entity.MemberAccount;
import cn.ug.pay.mapper.entity.PayGoldBeanRecord;
import cn.ug.pay.service.PayGoldBeanRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayGoldBeanRecordServiceImpl implements PayGoldBeanRecordService {
    @Autowired
    private PayGoldBeanRecordMapper payGoldBeanRecordMapper;
    @Autowired
    private MemberAccountMapper memberAccountMapper;

    @Override
    public List<PayGoldBeanRecord> query(String memberId, int type, String remark, String order, String sort, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("type", type);
            List<String> orders = Arrays.asList("gold_bean", "add_time", "success_time");
            if (orders.contains(order)) {
                params.put("order", order);
                params.put("sort", "desc");
                if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                    params.put("sort", sort);
                }
            }
            if (StringUtils.isNotBlank(remark)) {
                params.put("remark", remark);
            }
            params.put("offset", offset);
            params.put("size", size);
            return payGoldBeanRecordMapper.query(params);
        }
        return null;
    }

    @Override
    public int count(String memberId, int type, String remark) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(remark)) {
                params.put("remark", remark);
            }
            params.put("type", type);
            return payGoldBeanRecordMapper.count(params);
        }
        return 0;
    }

    @Override
    public List<GoldBeanChangeBean> queryForBean(int type, String name, String mobile, int beanFrom, int beanTo, String startDate, String endDate, String order, String sort, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        params.put("beanFrom", beanFrom);
        params.put("beanTo", beanTo);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        List<String> orders = Arrays.asList("outgoBean", "usableBean", "incomeBean");
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return payGoldBeanRecordMapper.queryForBean(params);
    }

    @Override
    public int countForBean(int type, String name, String mobile, int beanFrom, int beanTo, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("type", type);
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        params.put("beanFrom", beanFrom);
        params.put("beanTo", beanTo);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }

        return payGoldBeanRecordMapper.countForBean(params);
    }

    @Override
    public List<GoldBeanEverydayRecordBean> queryForEveryRecord(String memberId, String order, String sort, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            List<String> orders = Arrays.asList("incomeBean", "outgoBean");
            if (orders.contains(order)) {
                params.put("order", order);
                params.put("sort", "desc");
                if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                    params.put("sort", sort);
                }
            }
            params.put("offset", offset);
            params.put("size", size);
            return payGoldBeanRecordMapper.queryForEveryRecord(params);
        }
        return null;
    }

    @Override
    public int countForEveryRecord(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            return payGoldBeanRecordMapper.countForEveryRecord(params);
        }
        return 0;
    }

    @Override
    public List<DueinGoldBeanBean> queryForDueinBean(String memberId, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("offset", offset);
            params.put("size", size);
            return payGoldBeanRecordMapper.queryForDueinBean(params);
        }
        return null;
    }

    @Override
    public int countForDueinBean(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            return payGoldBeanRecordMapper.countForDueinBean(params);
        }
        return 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(PayGoldBeanRecord record) {
        //金豆收入
        if (record != null && StringUtils.isNotBlank(record.getMemberId()) && record.getType() == 1) {
            MemberAccount memberAccount = memberAccountMapper.findByMemberId(record.getMemberId());
            if (memberAccount != null) {
                Map<String,Object> accountMap = new HashMap<String,Object>();
                accountMap.put("id",memberAccount.getId());
                accountMap.put("totalBean", memberAccount.getTotalBean()+record.getGoldBean());
                accountMap.put("usableBean", memberAccount.getUsableBean()+record.getGoldBean());
                int rows = memberAccountMapper.updateByPrimaryKeySelective(accountMap);
                Ensure.that(rows).isLt(1, "00000005");
                rows = payGoldBeanRecordMapper.insert(record);
                Ensure.that(rows).isLt(1, "00000005");
                return true;
            }
        }
        //金豆支出
        if (record != null && StringUtils.isNotBlank(record.getMemberId()) && record.getType() == 2) {
            MemberAccount memberAccount = memberAccountMapper.findByMemberId(record.getMemberId());
            if (memberAccount != null) {
                Map<String,Object> accountMap = new HashMap<String,Object>();
                accountMap.put("id",memberAccount.getId());
                accountMap.put("totalBean", memberAccount.getTotalBean()-record.getGoldBean());
                accountMap.put("usableBean", memberAccount.getUsableBean()-record.getGoldBean());
                int rows = memberAccountMapper.updateByPrimaryKeySelective(accountMap);
                Ensure.that(rows).isLt(1, "00000005");
                rows = payGoldBeanRecordMapper.insert(record);
                Ensure.that(rows).isLt(1, "00000005");
                return true;
            }
        }
        return false;
    }

    @Override
    public GoldBeanChangeBean queryForBeanByMemberId(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return payGoldBeanRecordMapper.queryForBeanByMemberId(memberId);
        }
        return null;
    }

    @Override
    public List<PayGoldBeanRecord> queryForTask(String memberId, List<String> remark) {
        if (StringUtils.isNotBlank(memberId) && remark != null && remark.size() > 0) {
            return payGoldBeanRecordMapper.queryForTask(memberId, remark);
        }
        return null;
    }

}