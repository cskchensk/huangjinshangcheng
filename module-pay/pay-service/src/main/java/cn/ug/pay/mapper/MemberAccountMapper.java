package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.AreaBalanceBean;
import cn.ug.pay.bean.AreaTransactionBean;
import cn.ug.pay.bean.request.MemberAccountParamBean;
import cn.ug.pay.bean.response.MemberAccountManageBean;
import cn.ug.pay.mapper.entity.MemberAccount;
import org.springframework.stereotype.Component;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Component
public interface MemberAccountMapper extends BaseMapper<MemberAccount>{

    List<MemberAccountManageBean> queryMemberAccountList(MemberAccountParamBean memberAccountParamBean);

    /**
     * 更新支付会员账户资金
     */
    void updatePayMemberAccount(Map<String,String> map);

    /**
     * 根据会员Id获取账户信息
     * @param memberId
     * @return
     */
    MemberAccount findByMemberId(String memberId);

    List<AreaBalanceBean> selectAreaBalance(Map<String, Object> params);
    List<AreaTransactionBean> selectAreaTransaction();

    List<MemberAccount> queryIds();

    int updateGoldBeanHalf(@Param(value = "list")List<String> list);

    int updateGoldBeanAll(@Param(value = "list")List<String> list);
}
