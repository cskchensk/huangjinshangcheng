package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.ProductOrderRecordBean;
import cn.ug.pay.bean.response.ProductListBean;
import cn.ug.pay.service.ProductOrderService;
import cn.ug.pay.service.ProductRecordSercice;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 产品记录
 */
@RestController
@RequestMapping("productRecord")
public class ProductRecordController extends BaseController {
    @Resource
    private Config config;
    @Autowired
    private ProductRecordSercice productRecordSercice;

    @Autowired
    private ProductOrderService productOrderService;

    @RequestMapping(value = "/list", method = GET)
    public SerializeObject<DataTable<ProductListBean>> list(int pageNum,int pageSize,String startDate,String endDate,Integer productType,Integer timeType,Integer productStatus) {
        if (pageSize <= 0) {
            pageSize = config.getPageSize();
        }
        return new SerializeObject<>(ResultType.NORMAL, productRecordSercice.queryProductList(pageNum,pageSize,startDate,endDate,productType,timeType,productStatus));
    }

    @RequestMapping(value = "/queryByProductId", method = GET)
    public SerializeObject<DataTable<ProductOrderRecordBean>> queryByProductId(int pageNum, int pageSize, String productId) {
        if (pageSize <= 0) {
            pageSize = config.getPageSize();
        }
        return new SerializeObject<>(ResultType.NORMAL, productOrderService.queryRecordByProductId(pageNum, pageSize, productId));
    }


    /**
     * 导出产品购买记录
     *
     * @return
     */
    @RequestMapping(value = "/export", method = GET)
    public void export(HttpServletResponse response,String productId) {
        String[] columnNames = {"序号", "流水号", "购买时间", "用户账户", "用户姓名", "购买克重", "当时金价","支付金额", "手续费"};
        String[] columns = {"index", "orderId", "addTimeString", "memberMobile", "memberName", "amount", "goldPrice","actualAmount", "fee"};
        String fileName = "购买记录统计详表";
        ExportExcelController<ProductOrderRecordBean> export = new ExportExcelController<>();
        /*if (pageSize <= 0) {
            pageSize = Integer.MAX_VALUE;
        }*/
        export.exportExcel(fileName, fileName, columnNames, columns,
                wrapPutGoldBeanData(productOrderService.queryRecordByProductId(1, Integer.MAX_VALUE, productId).getDataList()), response, ExportExcelUtil.EXCEL_FILE_2003);
    }

    private List<ProductOrderRecordBean> wrapPutGoldBeanData(List<ProductOrderRecordBean> list) {
        int index = 1;
        if (!CollectionUtils.isEmpty(list)){
            for (ProductOrderRecordBean bean : list) {
                bean.setIndex(index++);
            }
        }
        return list;
    }
}
