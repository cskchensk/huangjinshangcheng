package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayTbillLeaseback implements Serializable {
    private int id;
    private String tbillNO;
    private String orderNO;
    private String startTime;
    private String endTime;
    private BigDecimal goldPrice;
    private int leasebackDays;
    private BigDecimal yearIncome;
    private BigDecimal incomeAmount;
    private BigDecimal incomeGram;
    private int incomeBeans;
    private String couponId;
    //黄金红包额度
    private int couponAmount;
    private BigDecimal couponYearIncome;
    private String actualTime;
    private int status;
    private String grantTime;
    private BigDecimal discountAmount;
    private int incomeType;
    private int discountAmountType;

    private int index;
    private String mobile;
    private String name;
    private String memberId;
    private int totalGram;
    private BigDecimal totalMoney;
    private int leasebackTimes;
    private String amountMark;
    private String beansMark;
    private String tbillTime;
    private String source;

    //金生金奖励
    private BigDecimal goldGiveGoldAmount;
    //福利卷类型 0:黄金红包  1:回租卷
    private int couponType;
    //黄金红包奖励
    private BigDecimal couponAwardAmount;
    //优惠卷名称
    private String couponName;
    //黄金红包额度
    private Integer couponDiscountAmount;
    //加息年化
    private BigDecimal raiseYearIncome;

    //产品类型  6.实物金  7.体验金 8.安稳金
    private Integer productType;
    //导出使用
    private String productTypeMark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiscountAmountType() {
        return discountAmountType;
    }

    public void setDiscountAmountType(int discountAmountType) {
        this.discountAmountType = discountAmountType;
    }

    public int getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(int incomeType) {
        this.incomeType = incomeType;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public int getIndex() {
        return index;
    }

    public String getTbillTime() {
        return tbillTime;
    }

    public void setTbillTime(String tbillTime) {
        this.tbillTime = tbillTime;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getAmountMark() {
        return amountMark;
    }

    public void setAmountMark(String amountMark) {
        this.amountMark = amountMark;
    }

    public String getBeansMark() {
        return beansMark;
    }

    public void setBeansMark(String beansMark) {
        this.beansMark = beansMark;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public int getLeasebackTimes() {
        return leasebackTimes;
    }

    public void setLeasebackTimes(int leasebackTimes) {
        this.leasebackTimes = leasebackTimes;
    }

    public String getTbillNO() {
        return tbillNO;
    }

    public void setTbillNO(String tbillNO) {
        this.tbillNO = tbillNO;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getGoldPrice() {
        return goldPrice;
    }

    public void setGoldPrice(BigDecimal goldPrice) {
        this.goldPrice = goldPrice;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public BigDecimal getYearIncome() {
        return yearIncome;
    }

    public void setYearIncome(BigDecimal yearIncome) {
        this.yearIncome = yearIncome;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public int getIncomeBeans() {
        return incomeBeans;
    }

    public void setIncomeBeans(int incomeBeans) {
        this.incomeBeans = incomeBeans;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public int getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(int couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getCouponYearIncome() {
        return couponYearIncome;
    }

    public void setCouponYearIncome(BigDecimal couponYearIncome) {
        this.couponYearIncome = couponYearIncome;
    }

    public String getActualTime() {
        return actualTime;
    }

    public void setActualTime(String actualTime) {
        this.actualTime = actualTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getGrantTime() {
        return grantTime;
    }

    public void setGrantTime(String grantTime) {
        this.grantTime = grantTime;
    }

    public BigDecimal getGoldGiveGoldAmount() {
        return goldGiveGoldAmount;
    }

    public void setGoldGiveGoldAmount(BigDecimal goldGiveGoldAmount) {
        this.goldGiveGoldAmount = goldGiveGoldAmount;
    }

    public int getCouponType() {
        return couponType;
    }

    public void setCouponType(int couponType) {
        this.couponType = couponType;
    }

    public BigDecimal getCouponAwardAmount() {
        return couponAwardAmount;
    }

    public void setCouponAwardAmount(BigDecimal couponAwardAmount) {
        this.couponAwardAmount = couponAwardAmount;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Integer getCouponDiscountAmount() {
        return couponDiscountAmount;
    }

    public void setCouponDiscountAmount(Integer couponDiscountAmount) {
        this.couponDiscountAmount = couponDiscountAmount;
    }

    public BigDecimal getRaiseYearIncome() {
        return raiseYearIncome;
    }

    public void setRaiseYearIncome(BigDecimal raiseYearIncome) {
        this.raiseYearIncome = raiseYearIncome;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public String getProductTypeMark() {
        return productTypeMark;
    }

    public void setProductTypeMark(String productTypeMark) {
        this.productTypeMark = productTypeMark;
    }

    public BigDecimal getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(BigDecimal totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
