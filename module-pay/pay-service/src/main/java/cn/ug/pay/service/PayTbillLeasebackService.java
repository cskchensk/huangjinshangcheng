package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayTbillAgainLeaseback;
import cn.ug.pay.mapper.entity.PayTbillLeaseback;

import java.util.List;

public interface PayTbillLeasebackService {
    List<PayTbillLeaseback> query(String mobile, String name, int startGram, int endGram, String startOfStartDate, String endOfStartDate,
                                  String startOfEndDate, String endOfEndDate, int status, String tbillNO, Integer[] leasebackDays, String order, String sort, int offset, int size,Integer[] productType
            ,String tbillMoneyMin,String tbillMoneyMax);
    int count(String mobile, String name, int startGram, int endGram, String startOfStartDate, String endOfStartDate,
              String startOfEndDate, String endOfEndDate, int status, String tbillNO, Integer[] leasebackDays,Integer[] productType
            ,String tbillMoneyMin,String tbillMoneyMax);

    boolean save(PayTbillLeaseback payTbillLeaseback);

    PayTbillLeaseback selectByOrderNO(String orderNO);
    PayTbillLeaseback selectLatestByTbillNO(String tbillNO);
    void completeBill(String orderNO);

    int countSuccessfulNum(String memberId);

    /**
     * 提单复投记录列表
     * @param mobile
     * @param name
     * @param tbillNo
     * @param startGram
     * @param endGram
     * @param startAmount
     * @param endAmount
     * @param s_startTime
     * @param e_startTime
     * @param s_endTime
     * @param e_endTime
     * @param leasebackDays
     * @param productType
     * @param order
     * @param sort
     * @return
     */
    List<PayTbillAgainLeaseback> queryAgainLeaseback(String mobile, String name, String tbillNo, int startGram, int endGram, String startAmount, String endAmount,
                                                     String s_startTime, String e_startTime, String s_endTime, String e_endTime, Integer[] leasebackDays,
                                                     Integer[] productType, String order, String sort);


    /**
     * 提单复投记录列表（针对用户的复投）
     * @param mobile
     * @param name
     * @param tbillNo
     * @param startGram
     * @param endGram
     * @param startAmount
     * @param endAmount
     * @param s_startTime
     * @param e_startTime
     * @param s_endTime
     * @param e_endTime
     * @param leasebackDays
     * @param productType
     * @param order
     * @param sort
     * @return
     */
    List<PayTbillAgainLeaseback> queryUserAgainLeasebackList(String mobile, String name, String tbillNo, int startGram, int endGram, String startAmount, String endAmount,
                                                     String s_startTime, String e_startTime, String s_endTime, String e_endTime, Integer[] leasebackDays,
                                                             Integer[] productType, String order, String sort);

    /**
     * 平台累计回租总克重
     * @return
     */
    int findPlatformLeasebackTotalGram();
}
