package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.AccountFinanceBillParam;
import cn.ug.pay.mapper.entity.AccountFinanceBill;

import java.time.LocalDate;
import java.util.List;

/**
 * 财务对账
 * @auther ywl
 * @date 2018-04-13 14:50
 */
public interface AccountFinanceBillService {


    /**
     * 后台
     * 获取财务账单列表
     * @return
     */
    DataTable<AccountFinanceBill> findList(AccountFinanceBillParam accountFinanceBillParam);

    List<AccountFinanceBill> queryForList(AccountFinanceBillParam accountFinanceBillParam);

    /**
     * 账户总额
     */
    void accountfinanceBillJob();

    void accountJob(LocalDate startTime, LocalDate endTime);
}
