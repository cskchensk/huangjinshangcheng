package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.BankCardBaseParamBean;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.response.BankCardFindBean;
import cn.ug.pay.bean.response.BankCardInfoBean;
import cn.ug.pay.bean.response.BankCardManageBean;

public interface BankCardService {

    /**
     * 绑卡列表--后台
     * @param bankCardParamBean	    请求参数
     * @return				            分页数据
     */
    DataTable<BankCardManageBean> queryBankCardList(BankCardParamBean bankCardParamBean);

    /**
     * 会员绑定银行卡--移动端
     * @param memberId
     * @return
     */
    BankCardFindBean queryMemberBankCard(String memberId);

    /**
     * 绑卡(第一步) --移动端
     * @param bankCardBaseParamBean
     */
    void bindBandCard(BankCardBaseParamBean bankCardBaseParamBean);

    /**
     * 绑卡(第二步)短信确认 --移动端
     * @param memberId  会员Id
     * @param code
     * @return
     */
    String bindSmsConfirm(String memberId,String code);

    /**
     *  绑卡短信重发--移动端
     * @return
     */
    int bindBankCardReSendSms();

    /**
     * 查询卡号基本信息
     * @param cardNo
     * @return
     */
    BankCardInfoBean queryBankCardInfo(String cardNo);

    /**
     * 查询会员是否绑定银行卡
     * @param memberId
     * @return
     */
    int validateBindBankCard(String memberId);


    void test();
}
