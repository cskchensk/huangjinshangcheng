package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayGoldStock;

import java.math.BigDecimal;
import java.util.List;

public interface PayGoldStockService {
    boolean save(PayGoldStock payGoldStock);
    boolean modify(String calculationDate, BigDecimal closingPrice);
    List<PayGoldStock> query(String calculationDate, int offset, int size);
    int count(String calculationDate);
}
