package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.OpenPositionTotalBean;
import cn.ug.pay.mapper.entity.Bill;
import cn.ug.pay.mapper.entity.GoldRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 可用资产详表
 * @author kaiwotech
 */
@Component
public interface GoldRecordMapper extends BaseMapper<GoldRecord> {
	List<OpenPositionTotalBean> listGoldRecordStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate, @Param("endDate")String endDate);
}