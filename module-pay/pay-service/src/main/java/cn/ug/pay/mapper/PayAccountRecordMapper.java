package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayAccountRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayAccountRecordMapper {
    int insertInBatch(@Param("records")List<PayAccountRecord> records);

    List<PayAccountRecord> query(Map<String, Object> params);
    int count(Map<String, Object> params);
}
