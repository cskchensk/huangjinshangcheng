package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.enums.ProductTypeEnum;
import cn.ug.pay.bean.ProductOrderBean;
import cn.ug.pay.bean.request.ProductRecordBean;
import cn.ug.pay.bean.response.ProductListBean;
import cn.ug.pay.mapper.ProductRecordMapper;
import cn.ug.pay.mapper.entity.ProductListDo;
import cn.ug.pay.service.ProductRecordSercice;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductRecordSerciceImpl implements ProductRecordSercice {

    @Resource
    private ProductRecordMapper productRecordMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<ProductListBean> queryProductList(int pageNum, int pageSize, String startDate, String endDate, Integer productType, Integer timeType, Integer productStatus) {
        PageHelper.startPage(pageNum, pageSize);
        ProductRecordBean productRecordBean = new ProductRecordBean();
        productRecordBean.setPageSize(pageSize);
        productRecordBean.setPageNum(pageNum);
        productRecordBean.setEndDate(endDate);
        productRecordBean.setProductType(productType);
        productRecordBean.setStartDate(startDate);
        productRecordBean.setTimeType(timeType);
        productRecordBean.setProductStatus(productStatus);
        List<ProductListDo> productListDoList = productRecordMapper.queryProductList(beanToMap(productRecordBean));
        PageInfo<ProductListDo> pageInfo = new PageInfo<>(productListDoList);
        List<ProductListBean> dataList = new ArrayList<>();
        for (ProductListDo productListDo : pageInfo.getList()) {
            ProductListBean objBean = dozerBeanMapper.map(productListDo, ProductListBean.class);
            int count = 0;
            if (productListDo.getProductType() == ProductTypeEnum.ENTITY.getType() ||
                    productListDo.getProductType() == ProductTypeEnum.SMOOTH_GOLD.getType() ||
                    productListDo.getProductType() == ProductTypeEnum.DISCOUNT_GOLD.getType()) {
                count = productRecordMapper.queryCount(objBean.getId());
            } else if (productListDo.getProductType() == ProductTypeEnum.EXPERIENCE_GOLD.getType()) {
                count = productRecordMapper.queryCountExperience(objBean.getId());
            } else {
                count = productRecordMapper.queryProductorderCount(objBean.getId());
            }
            if (productListDo.getAuditStatus() == 2 && productListDo.getShelfState() == 2) {
                objBean.setProductStatus(2);
            } else {
                objBean.setProductStatus(1);
            }
            objBean.setBuyCountNum(count);
            dataList.add(objBean);
        }
        return new DataTable<>(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getTotal(), dataList);
    }

    public static Map<String, Object> beanToMap(Object obj) {
        Map<String, Object> params = new HashMap<String, Object>(0);
        try {
            PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
            PropertyDescriptor[] descriptors = propertyUtilsBean.getPropertyDescriptors(obj);
            for (int i = 0; i < descriptors.length; i++) {
                String name = descriptors[i].getName();
                if (!"class".equals(name)) {
                    params.put(name, propertyUtilsBean.getNestedProperty(obj, name));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }

}
