package cn.ug.pay.mapper;

import cn.ug.pay.bean.FeeBean;
import cn.ug.pay.bean.YeepayBill;
import cn.ug.pay.mapper.entity.PayYeepayBill;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayYeepayBillMapper {
    int insertInBatch(@Param("bills")List<PayYeepayBill> bills);

    List<YeepayBill> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<FeeBean> queryYeepayFee(Map<String, Object> params);
    int countYeepayFee(Map<String, Object> params);

    List<FeeBean> queryFee(Map<String, Object> params);
    int countFee(Map<String, Object> params);
}
