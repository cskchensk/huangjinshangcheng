package cn.ug.pay.service.impl;

import cn.ug.pay.mapper.PayGoldStockMapper;
import cn.ug.pay.mapper.entity.PayGoldStock;
import cn.ug.pay.service.PayGoldStockService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayGoldStockServiceImpl implements PayGoldStockService {
    @Autowired
    private PayGoldStockMapper payGoldStockMapper;

    @Override
    public boolean save(PayGoldStock payGoldStock) {
        if (payGoldStock != null) {
            return payGoldStockMapper.insert(payGoldStock) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean modify(String calculationDate, BigDecimal closingPrice) {
        if (StringUtils.isNotBlank(calculationDate) && closingPrice != null) {
            PayGoldStock payGoldStock = new PayGoldStock();
            payGoldStock.setCalculationDate(calculationDate);
            payGoldStock.setClosingPrice(closingPrice);
            return payGoldStockMapper.update(payGoldStock) > 0 ? true : false;

        }
        return false;
    }

    @Override
    public List<PayGoldStock> query(String calculationDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(calculationDate)) {
            params.put("calculationDate", calculationDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        return payGoldStockMapper.query(params);
    }

    @Override
    public int count(String calculationDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(calculationDate)) {
            params.put("calculationDate", calculationDate);
        }
        return payGoldStockMapper.count(params);
    }
}
