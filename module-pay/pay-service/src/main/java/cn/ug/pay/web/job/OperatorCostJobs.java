package cn.ug.pay.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.pay.service.OperatorCostService;
import cn.ug.util.BigDecimalUtil;
import cn.ug.util.UF;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

/**
 * @Author zhangweijie
 * @Date 2019/4/28 0028
 * @time 下午 14:53
 **/
@Component
public class OperatorCostJobs {
    private Log log = LogFactory.getLog(OperatorCostJobs.class);
    @Autowired
    private RedisGlobalLock redisGlobalLock;

    @Autowired
    private OperatorCostService operatorCostService;

    private static final long EXPIRE_DAY = 1;

    /**
     * 每天23点50
     */
    @Scheduled(cron = "0 59 23 * * ?")
    public void statistics() {
        String key = "Pay:OperatorCostJobs:statistics:" + UF.getFormatDateNow();
        if (!redisGlobalLock.lock(key, EXPIRE_DAY, TimeUnit.DAYS)) {
            return;
        }
        operatorCostService.statistics();
    }

    public static void main(String[] args) {
        System.out.println(BigDecimalUtil.to2Point(new BigDecimal(15).multiply(new BigDecimal(30*1.0/(30 - 29) - 1))));
    }
}
