package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayGoldStock;

import java.util.List;
import java.util.Map;

public interface PayGoldStockMapper {
    List<PayGoldStock> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    int insert(PayGoldStock payGoldStock);
    int update(PayGoldStock payGoldStock);
}
