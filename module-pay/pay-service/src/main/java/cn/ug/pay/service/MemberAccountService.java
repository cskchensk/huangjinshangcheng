package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.AreaBalanceBean;
import cn.ug.pay.bean.AreaTransactionBean;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.request.MemberAccountParamBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.bean.response.MemberAccountBean;
import cn.ug.pay.bean.response.MemberAccountManageBean;

import java.math.BigDecimal;
import java.util.List;

public interface MemberAccountService {

    /**
     * 会员资金账户列表--后台
     * @param memberAccountParamBean 请求参数
     * @return				             分页数据
     */
    DataTable<MemberAccountManageBean> queryMemberAccountList(MemberAccountParamBean memberAccountParamBean);

    /**
     * 查询会员可用余额
     * @return
     */
    BigDecimal queryUsableAmount();

    /**
     * 会员提现
     * @param amount
     * @return
     */
    int withdraw(String memberId,BigDecimal amount);

    /**
     * 会员卖金
     * @param memberId  会员ID
     * @param amount
     * @return
     */
    boolean sellGold(String memberId, BigDecimal amount, BigDecimal fee, BigDecimal goldPrice,BigDecimal weight);

    /**
     * 创建资金账户
     * @param memberId
     * @return
     */
    boolean create(String memberId);

    /**
     * 邀请返现
     * @param memberId
     * @param amount
     * @return
     */
    String inviteReturnCash(String memberId,BigDecimal amount, Integer type);

    /**
     * 根据会员Id获取资金账户
     *
     * @param memberId
     * @return
     */
    MemberAccountBean findByMemberId(String memberId);

    /**
     * 订单过期解冻账户--如果账户使用余额和银行卡支付
     * @param orderId
     */
    void orderExpireUnfreezeAmount(String orderId);

    /**
     * 提现失败
     * @param memberId
     * @param amount
     */
    void withdrawFail(String memberId, BigDecimal freezeAmount, BigDecimal amount);

    /**
     * 提现成功
     * @param memberId
     * @param amount
     */
    void withdrawSuccess(String memberId,BigDecimal amount);

    List<AreaBalanceBean> listAreaBalance(String order, String sort);
    List<AreaTransactionBean> listAreaTransaction();

    /**
     * 奖励发放
     * @param memberId
     * @param amount
     * @param type
     * @return
     */
    String awardGiveOut(String memberId,BigDecimal amount, Integer type);

    /**
     * 批量修改
     * @param type 扣减数量的类型 1.一半 2.全部
     * @return
     */
    void batchUpdate(Integer type);

    /**
     * 金豆到期短信通知
     * @param type
     */
    void expireSmsSend(Integer type);


    /**
     * 大额充值
     * @param memberId
     * @param amount
     * @return
     */
    String largeRecharge(String memberId,BigDecimal amount, Integer type,BigDecimal fee);
}
