package cn.ug.pay.service;

import cn.ug.pay.bean.DueinGoldBeanBean;
import cn.ug.pay.bean.GoldBeanChangeBean;
import cn.ug.pay.bean.GoldBeanEverydayRecordBean;
import cn.ug.pay.mapper.entity.PayGoldBeanRecord;

import java.util.List;

public interface PayGoldBeanRecordService {
    List<PayGoldBeanRecord> query(String memberId, int type, String remark, String order, String sort, int offset, int size);
    int count(String memberId, int type, String remark);

    List<GoldBeanChangeBean> queryForBean(int type, String name, String mobile, int beanFrom, int beanTo, String startDate, String endDate, String order, String sort, int offset, int size);
    int countForBean(int type, String name, String mobile, int beanFrom, int beanTo, String startDate, String endDate);

    List<GoldBeanEverydayRecordBean> queryForEveryRecord(String memberId, String order, String sort, int offset, int size);
    int countForEveryRecord(String memberId);

    List<DueinGoldBeanBean> queryForDueinBean(String memberId, int offset, int size);
    int countForDueinBean(String memberId);

    boolean save(PayGoldBeanRecord record);

    GoldBeanChangeBean queryForBeanByMemberId(String memberId);

    List<PayGoldBeanRecord> queryForTask(String memberId, List<String> remark);
}
