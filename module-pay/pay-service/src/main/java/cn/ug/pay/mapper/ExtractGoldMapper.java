package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.ExtractGoldParamBean;
import cn.ug.pay.bean.response.ExtractGoldBean;
import cn.ug.pay.mapper.entity.ExtractGold;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ExtractGoldMapper extends BaseMapper<ExtractGold>{

    List<ExtractGoldBean> findList(ExtractGoldParamBean extractGoldParamBean);
}
