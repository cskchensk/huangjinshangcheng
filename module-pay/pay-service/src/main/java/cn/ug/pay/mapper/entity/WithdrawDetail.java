package cn.ug.pay.mapper.entity;

import java.math.BigDecimal;

/**
 * 提现详情
 * @author kaiwotech
 */
public class WithdrawDetail extends Withdraw implements java.io.Serializable {

	/** 支付通道 1:易宝支付 */
	private Integer bankPayWay;
	/** 银行卡号 */
	private String bankCardNo;
	/** 所属银行 */
	private String bankName;
	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;
	/** 会员提现前余额*/
	private BigDecimal balance;
	private String channelName;

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public String getBankCardNo() {
		return bankCardNo;
	}

	public void setBankCardNo(String bankCardNo) {
		this.bankCardNo = bankCardNo;
	}

	public String getBankName() {
		return bankName;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Integer getBankPayWay() {
		return bankPayWay;
	}

	public void setBankPayWay(Integer bankPayWay) {
		this.bankPayWay = bankPayWay;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}
}
