package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 提现
 * @author kaiwotech
 */
public class Withdraw extends BaseEntity implements java.io.Serializable {

	/** 订单号 */
	private String orderId;
	/** 第三方流水号 */
	private String thirdNumber;
	/** 绑定银行卡ID */
	private String bankCardId;
	/** 会员id */
	private String memberId;
	/** 金额 */
	private BigDecimal amount;
	/** 手续费 */
	private BigDecimal fee;
	/** 实际金额(金额-手续费) */
	private BigDecimal actualAmount;
	/** 提现类型  1: T+0到账 2: T+1到账 */
	private Integer type;
	/** 提现状态  1:待提现 2:处理中 3:成功 4:失败 */
	private Integer status;
	/** 审核状态  1:未审核 2:一级审核成功 3一级审核失败 4:二级审核成功 5:二级审核失败 */
	private Integer auditStatus;
	/** 一级审核时间 */
	private LocalDateTime auditTimeOne;
	/** 一级审核人用户Id */
	private String auditUserIdOne;
	/** 一级审核人用户名称 */
	private String auditUserNameOne;
	/** 一级审核描述 */
	private String auditDescriptionOne;
	/** 二级审核时间 */
	private LocalDateTime auditTimeTwo;
	/** 二级审核人用户Id */
	private String auditUserIdTwo;
	/** 二级审核人用户名称 */
	private String auditUserNameTwo;
	/** 二级审核描述 */
	private String auditDescriptionTwo;
	/** 备注 */
	private String description;
	private BigDecimal balance;

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getThirdNumber() {
		return thirdNumber;
	}

	public void setThirdNumber(String thirdNumber) {
		this.thirdNumber = thirdNumber;
	}

	public String getBankCardId() {
		return bankCardId;
	}

	public void setBankCardId(String bankCardId) {
		this.bankCardId = bankCardId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}

	public LocalDateTime getAuditTimeOne() {
		return auditTimeOne;
	}

	public void setAuditTimeOne(LocalDateTime auditTimeOne) {
		this.auditTimeOne = auditTimeOne;
	}

	public String getAuditUserIdOne() {
		return auditUserIdOne;
	}

	public void setAuditUserIdOne(String auditUserIdOne) {
		this.auditUserIdOne = auditUserIdOne;
	}

	public String getAuditUserNameOne() {
		return auditUserNameOne;
	}

	public void setAuditUserNameOne(String auditUserNameOne) {
		this.auditUserNameOne = auditUserNameOne;
	}

	public String getAuditDescriptionOne() {
		return auditDescriptionOne;
	}

	public void setAuditDescriptionOne(String auditDescriptionOne) {
		this.auditDescriptionOne = auditDescriptionOne;
	}

	public LocalDateTime getAuditTimeTwo() {
		return auditTimeTwo;
	}

	public void setAuditTimeTwo(LocalDateTime auditTimeTwo) {
		this.auditTimeTwo = auditTimeTwo;
	}

	public String getAuditUserIdTwo() {
		return auditUserIdTwo;
	}

	public void setAuditUserIdTwo(String auditUserIdTwo) {
		this.auditUserIdTwo = auditUserIdTwo;
	}

	public String getAuditUserNameTwo() {
		return auditUserNameTwo;
	}

	public void setAuditUserNameTwo(String auditUserNameTwo) {
		this.auditUserNameTwo = auditUserNameTwo;
	}

	public String getAuditDescriptionTwo() {
		return auditDescriptionTwo;
	}

	public void setAuditDescriptionTwo(String auditDescriptionTwo) {
		this.auditDescriptionTwo = auditDescriptionTwo;
	}

	public BigDecimal getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
