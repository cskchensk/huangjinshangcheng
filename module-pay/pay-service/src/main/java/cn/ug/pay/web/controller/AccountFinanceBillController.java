package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.AccountFinanceBillParam;
import cn.ug.pay.mapper.entity.AccountFinanceBill;
import cn.ug.pay.service.AccountFinanceBillService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("accountFinanceBalance")
public class AccountFinanceBillController {

    @Resource
    private AccountFinanceBillService accountFinanceBillService;

    @Resource
    private Config config;

    /**
     * 查询财务对账列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param accountFinanceBillParam       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<AccountFinanceBill>> findList(@RequestHeader String accessToken, AccountFinanceBillParam accountFinanceBillParam) {
        if(accountFinanceBillParam.getPageSize() <= 0) {
            accountFinanceBillParam.setPageSize(config.getPageSize());
        }
        DataTable<AccountFinanceBill> dataTable = accountFinanceBillService.findList(accountFinanceBillParam);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }


    @GetMapping(value = "/export")
    public void exportData(HttpServletResponse response, String day) {
        day = UF.toString(day);
        AccountFinanceBillParam accountFinanceBillParam = new AccountFinanceBillParam();
        accountFinanceBillParam.setPageNum(0);
        accountFinanceBillParam.setPageSize(5000000);
        accountFinanceBillParam.setDay(day);
        List<AccountFinanceBill> list = accountFinanceBillService.queryForList(accountFinanceBillParam);
        if(list != null && list.size() > 0){
            String[] columnNames = { "序号","日期", "支付通道", "充值总额(元)", "提现总额(元)", "当日账户盈亏（元)",  "前日累计账户余额(元)",
                    "当日账户累计余额（元)"};
            String [] columns = {"index", "day", "wayMark", "rechargeAmount", "withdrawAmount", "amount", "lastDayAmount", "balanceAmount"};
            String fileName = "资金对账总表";
            int index = 1;
            for (AccountFinanceBill bill : list) {
                bill.setIndex(index);
                index++;
                bill.setWayMark("易宝支付");
            }
            ExportExcelController<AccountFinanceBill> export = new ExportExcelController<AccountFinanceBill>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    @RequestMapping(value = "accountJobTest" , method = GET)
    public SerializeObject accountJobTest(String startTimeString,String endTimeString) {
        LocalDate startTime = LocalDate.parse(startTimeString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate endTime = LocalDate.parse(endTimeString, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        accountFinanceBillService.accountJob(startTime,endTime);
        return new SerializeObject<>(ResultType.NORMAL, "0000001");
    }


}
