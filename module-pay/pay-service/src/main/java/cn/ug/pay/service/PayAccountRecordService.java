package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayAccountRecord;

import java.util.List;

public interface PayAccountRecordService {
    boolean save(List<PayAccountRecord> records);

    List<PayAccountRecord> query(String memberId, String startDate, String endDate, int offset, int size);
    int count(String memberId, String startDate, String endDate);
}
