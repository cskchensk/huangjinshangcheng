package cn.ug.pay.pay.yeepay;

import cn.ug.pay.utils.Common;

/**
 *  发起请求
 *  @auther ywl
 *  @date 2018-01-31 15:31:20
 */
public class YeePayConfig {

    //商户号
    public static String merchantAccount ;
    //商户公钥
    public static String merchantPublicKey;
    //商户私钥
    public static String merchantPrivateKey;
    //易宝公钥
    public static String yeepayPublicKey;
    //有短信验证绑定接口
    public static String bindCardRequestURL;
    //有短验绑卡请求确认接口
    public static String bindCardConfirmURL;
    //有短验绑卡请求重发短验接口
    public static String bindCardResendsmsURL;
    //银行卡查询接口
    public static String bankCardCheckURL;
    //无短验充值请求接口
    public static String unSendBindPayRequestURL;
    //提现接口请求地址
    public static String withdrawURL;
    //绑卡接口回调地址
    public static String bindCallBackUrl;
    //交易回调地址
    public static String payCallBackUrl;
    //充值回调地址
    public static String rechargeCallBackUrl;
    //提现回调地址
    public static String withdrawCallBackUrl;

    static{
        merchantAccount = Common.getYeePayValue("merchantAccount");
        merchantPublicKey = Common.getYeePayValue("merchantPublicKey");
        merchantPrivateKey = Common.getYeePayValue("merchantPrivateKey");
        yeepayPublicKey = Common.getYeePayValue("yeepayPublicKey");
        bindCardRequestURL = Common.getYeePayValue("bindCardRequestURL");
        bindCardConfirmURL = Common.getYeePayValue("bindCardConfirmURL");
        bindCardResendsmsURL = Common.getYeePayValue("bindCardResendsmsURL");
        bankCardCheckURL = Common.getYeePayValue("bankCardCheckURL");
        unSendBindPayRequestURL = Common.getYeePayValue("unSendBindPayRequestURL");
        withdrawURL = Common.getYeePayValue("withdrawURL");
        bindCallBackUrl = Common.getYeePayValue("bindCallBackUrl");
        rechargeCallBackUrl = Common.getYeePayValue("rechargeCallBackUrl");
        payCallBackUrl = Common.getYeePayValue("payCallBackUrl");
        withdrawCallBackUrl = Common.getYeePayValue("withdrawCallBackUrl");
    }

    /**
     * 取得商户AESKey
     */
    public static String getMerchantAESKey() {
        return (RandomUtil.getRandom(16));
    }
}
