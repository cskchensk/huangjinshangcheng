package cn.ug.pay.service.impl;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.bean.response.BankInfoBean;
import cn.ug.pay.bean.response.WithdrawSettingBean;
import cn.ug.pay.mapper.WithdrawSettingMapper;
import cn.ug.pay.mapper.entity.WithdrawSetting;
import cn.ug.pay.service.WithdrawSettingService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WithdrawSettingServiceImpl implements WithdrawSettingService {

    @Resource
    private WithdrawSettingMapper withdrawSettingMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<WithdrawSettingBean> findList(BankInfoParamBean bankInfoParamBean) {
        com.github.pagehelper.Page<BankInfoParamBean> pages = PageHelper.startPage(bankInfoParamBean.getPageNum(),bankInfoParamBean.getPageSize());
        List<WithdrawSettingBean> dataList = withdrawSettingMapper.findList(bankInfoParamBean);
        return new DataTable<>(bankInfoParamBean.getPageNum(), bankInfoParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public WithdrawSettingBean findById(String id) {
        WithdrawSetting withdrawSetting = withdrawSettingMapper.findById(id);
        WithdrawSettingBean withdrawSettingBean = dozerBeanMapper.map(withdrawSetting, WithdrawSettingBean.class);
        return withdrawSettingBean;
    }

    @Override
    public WithdrawSettingBean findByBankCode(String bankCode) {
        if (StringUtils.isNotBlank(bankCode)) {
            return withdrawSettingMapper.findByBankCode(bankCode);
        }
        return null;
    }

    @Override
    public void save(WithdrawSettingBean withdrawSettingBean) {
        WithdrawSetting entity = dozerBeanMapper.map(withdrawSettingBean, WithdrawSetting.class);
        if(StringUtils.isBlank(withdrawSettingBean.getId())){
            entity.setId(UF.getRandomUUID());
            withdrawSettingMapper.insert(entity);
        }else{
            withdrawSettingMapper.update(entity);
        }
    }
}
