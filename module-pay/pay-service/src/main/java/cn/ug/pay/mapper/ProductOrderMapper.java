package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.member.bean.MemberDetailBean;
import cn.ug.pay.bean.*;
import cn.ug.pay.bean.response.ActiveMemberBean;
import cn.ug.pay.mapper.entity.ProductOrder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 定期投资
 * @author kaiwotech
 */
@Component
public interface ProductOrderMapper extends BaseMapper<ProductOrder> {

    /**
     * 根据产品ID统计募集克重
     * @param productId	产品ID
     * @return		    募集克重
     */
    BigDecimal sumOfAmount(@Param(value = "productId") String productId);

    BigDecimal sumToadySuccessAmount(@Param(value = "memberId")String memberId);

    int updatePayStatus(@Param("payStatus")int payStatus, @Param("orderId")String orderId);
    List<ProductOrder> queryProductOrders(Map<String, Object> params);
    List<ProductOrder> queryBecomeDueOrders();
	int countProductOrders(@Param("productId")String productId, @Param("productType")int productType);
    int countTodayNoviceOrders();
    List<TradeTotalAmountBean> listTradeAmountStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                         @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<TradeTotalAmountBean> listLeasebackAmountStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                         @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<TradeAverageValueBean> listTradeAvgValueStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                            @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<TradeFutureValueBean> listTradeFutureValueStatistics(@Param("startDate")String startDate, @Param("endDate")String endDate,
                                                              @Param("channelIds")List<String> channelIds);
    List<TradeTotalAmountBean> listTotalGramStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                       @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);

    List<TradeTotalAmountBean> listRechargeAmountStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                            @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);

    List<TradeTotalAmountBean> listWithdrawAmountStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                            @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);

    List<TradeTotalStatisticsBean> listBuyTotalStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                              @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds,
                                                              @Param("productType")List<Integer> productType);

    List<TradeTotalStatisticsBean> listLeasebackTotalStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                              @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds,
                                                              @Param("productType")List<Integer> productType);

    List<TradeTotalStatisticsBean> listSoldTotalStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                              @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds,
                                                              @Param("productType")List<Integer> productType);

    List<TradeTotalStatisticsBean> listBuyMarketingStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                              @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds,
                                                              @Param("productType")List<Integer> productType);

    List<TradeTotalStatisticsBean> listLeasebackMarketingStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                                    @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds,
                                                                    @Param("productType")List<Integer> productType);

    List<KValueBean> listMaxValue(@Param("startDate")String startDate, @Param("endDate")String endDate);
    List<KValueBean> listMinValue(@Param("startDate")String startDate, @Param("endDate")String endDate);
    List<KValueBean> listAvgValue(@Param("startDate")String startDate, @Param("endDate")String endDate);

    List<OpenPositionTotalBean> listOpenPositionTotalStatistics(@Param("startDate")String startDate, @Param("endDate")String endDate);
    List<OpenPositionTotalBean> listExpiredStatistics(@Param("startDate")String startDate, @Param("endDate")String endDate);
    List<OpenPositionTotalBean> listExpiredAmountStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate, @Param("endDate")String endDate);
    List<OpenPositionPastBean> listOpenPositionAvgStatistics(@Param("startDate")String startDate, @Param("endDate")String endDate);
    List<OpenPositionTotalBean> listTransactionWeightStatistics(@Param("payType")int payType, @Param("startDate")String startDate, @Param("endDate")String endDate);
    List<RFMLabelBean> queryRFMLabelList(Map<String, Object> params);
    int queryRFMLabelCount();
    /**
     * 查询会员已购买定期产品
     * @param memberId
     * @return
     */
    int  findRegularBuyCount(@Param("memberId") String memberId);

    /**
     * 查询会员已购买定期活动产品
     * @param memberId
     * @return
     */
    int  findRegularActivityBuyCount(@Param("memberId") String memberId);
    int  countActivityOrders(@Param("memberId") String memberId, @Param("productId") String productId);
    List<EverydayBuyGoldBean> queryEverydayBuyGoldRecords(@Param("payDate") String payDate);

    List<BuyGoldRecordBean> queryBuyGoldRecord(Map<String, Object> params);
    int countBuyGoldRecord(Map<String, Object> params);

    MemberDetailBean selectMemberTradeInfo(@Param("memberId") String memberId);
    int countTradeNum(@Param("memberId") String memberId);

    List<ProductOrder> queryInfoByMemberId(Map<String, Object> params);

    int getTransactionNumForThisMonth(@Param("memberId") String memberId);
    int getTransactionNumForThisDay(@Param("memberId") String memberId);

    List<ActiveMemberBean> findBuyActiveMemberList(@Param("startTime")String startTime,
                                                       @Param("endTime")String endTime,
                                                       @Param("channelIds")List<String> channelIds);
}