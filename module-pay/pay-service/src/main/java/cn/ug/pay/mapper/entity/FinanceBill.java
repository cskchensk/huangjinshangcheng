package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 财务对账
 * @auther ywl
 */
public class FinanceBill extends BaseEntity implements java.io.Serializable{

    /** 日期 **/
    private Date day;
    /** 类型 1:收入 2:支出 **/
    private Integer type;
    /** 金额 **/
    private BigDecimal amount;
    /** 总数 **/
    private Integer total;
    /** 通道方式 1:易宝支付 2:连连支付  目前一个会员只能绑定一个通道-产品已定义**/
    private Integer way;

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getWay() {
        return way;
    }

    public void setWay(Integer way) {
        this.way = way;
    }
}
