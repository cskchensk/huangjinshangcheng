package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 会员资产账户
 * @author kaiwotech
 */
public class MemberGold extends BaseEntity implements java.io.Serializable {

	/** 会员id */
	private String memberId;
	/** 总资产 */
	private BigDecimal totalAmount;
	/** 可用资产 */
	private BigDecimal usableAmount;
	/** 本金 */
	private BigDecimal principalAmount;
	/** T日本金 */
	private BigDecimal principalNowAmount;
	/** T-1日本金 */
	private BigDecimal principalHistoryAmount;
	/** T-1日累计本金 */
	private BigDecimal principalHistoryTotalAmount;
	/** 活期利息 */
	private BigDecimal interestAmount;
	/** T日利息 */
	private BigDecimal interestNowAmount;
	/** T-1日利息（可用） */
	private BigDecimal interestHistoryAmount;
	/** T-1日累计利息 */
	private BigDecimal interestHistoryTotalAmount;
	/** 冻结资产 */
	private BigDecimal freezeAmount;
	/**提金/存金冻结克重*/
	private BigDecimal freezeGram;
	/** 累计购买克重（未到期） */
	private BigDecimal freezeBuyAmount;
	/** 总收益 */
	private BigDecimal freezeIncomeAmount;
	/** 累计收益 */
	private BigDecimal freezeIncomeTotalAmount;
	/** 转入总资产 */
	private BigDecimal turnIntoAmount;
	/** 转出总资产 */
	private BigDecimal turnOutAmount;

	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;

	public String getMemberId() {
		return memberId;
	}

	public BigDecimal getFreezeGram() {
		return freezeGram;
	}

	public void setFreezeGram(BigDecimal freezeGram) {
		this.freezeGram = freezeGram;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getUsableAmount() {
		return usableAmount;
	}

	public void setUsableAmount(BigDecimal usableAmount) {
		this.usableAmount = usableAmount;
	}

	public BigDecimal getPrincipalAmount() {
		return principalAmount;
	}

	public void setPrincipalAmount(BigDecimal principalAmount) {
		this.principalAmount = principalAmount;
	}

	public BigDecimal getPrincipalNowAmount() {
		return principalNowAmount;
	}

	public void setPrincipalNowAmount(BigDecimal principalNowAmount) {
		this.principalNowAmount = principalNowAmount;
	}

	public BigDecimal getPrincipalHistoryAmount() {
		return principalHistoryAmount;
	}

	public void setPrincipalHistoryAmount(BigDecimal principalHistoryAmount) {
		this.principalHistoryAmount = principalHistoryAmount;
	}

	public BigDecimal getPrincipalHistoryTotalAmount() {
		return principalHistoryTotalAmount;
	}

	public void setPrincipalHistoryTotalAmount(BigDecimal principalHistoryTotalAmount) {
		this.principalHistoryTotalAmount = principalHistoryTotalAmount;
	}

	public BigDecimal getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
	}

	public BigDecimal getInterestNowAmount() {
		return interestNowAmount;
	}

	public void setInterestNowAmount(BigDecimal interestNowAmount) {
		this.interestNowAmount = interestNowAmount;
	}

	public BigDecimal getInterestHistoryAmount() {
		return interestHistoryAmount;
	}

	public void setInterestHistoryAmount(BigDecimal interestHistoryAmount) {
		this.interestHistoryAmount = interestHistoryAmount;
	}

	public BigDecimal getInterestHistoryTotalAmount() {
		return interestHistoryTotalAmount;
	}

	public void setInterestHistoryTotalAmount(BigDecimal interestHistoryTotalAmount) {
		this.interestHistoryTotalAmount = interestHistoryTotalAmount;
	}

	public BigDecimal getFreezeAmount() {
		return freezeAmount;
	}

	public void setFreezeAmount(BigDecimal freezeAmount) {
		this.freezeAmount = freezeAmount;
	}

	public BigDecimal getFreezeBuyAmount() {
		return freezeBuyAmount;
	}

	public void setFreezeBuyAmount(BigDecimal freezeBuyAmount) {
		this.freezeBuyAmount = freezeBuyAmount;
	}

	public BigDecimal getFreezeIncomeAmount() {
		return freezeIncomeAmount;
	}

	public void setFreezeIncomeAmount(BigDecimal freezeIncomeAmount) {
		this.freezeIncomeAmount = freezeIncomeAmount;
	}

	public BigDecimal getTurnIntoAmount() {
		return turnIntoAmount;
	}

	public void setTurnIntoAmount(BigDecimal turnIntoAmount) {
		this.turnIntoAmount = turnIntoAmount;
	}

	public BigDecimal getTurnOutAmount() {
		return turnOutAmount;
	}

	public void setTurnOutAmount(BigDecimal turnOutAmount) {
		this.turnOutAmount = turnOutAmount;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}

	public BigDecimal getFreezeIncomeTotalAmount() {
		return freezeIncomeTotalAmount;
	}

	public void setFreezeIncomeTotalAmount(BigDecimal freezeIncomeTotalAmount) {
		this.freezeIncomeTotalAmount = freezeIncomeTotalAmount;
	}
}
