package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.BillParam;
import cn.ug.pay.bean.request.FinanceBillInfoParam;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.ActiveMemberBean;
import cn.ug.pay.bean.response.FinanceBillBean;
import cn.ug.pay.bean.response.FinanceBillInfoBean;
import cn.ug.pay.mapper.entity.Bill;
import cn.ug.pay.mapper.entity.FinanceBillInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 收支记录
 * @author kaiwotech
 */
@Component
public interface BillMapper extends BaseMapper<Bill> {

    /**
     * 财务对账收支日列表
     * @param financeBillParam
     * @return
     */
    List<FinanceBillBean> findFinanceBillList(FinanceBillParam financeBillParam);
    /**
     * 财务对账日收支明细列表
     * @param financeBillInfoParam
     * @return
     */
    List<FinanceBillInfoBean> findFinanceBillInfoList(FinanceBillInfoParam financeBillInfoParam);

    int updateTradeTypeByOrderId(@Param("orderId")String orderId, @Param("tradeType")int tradeType,
                                 @Param("type")int type);
    Bill findByOrderId(@Param("orderId")String orderId);
    /**
     * 查询每日账务明细列表
     * @param billParam
     * @return
     */
    List<Bill> findAccountFinanceBillList(BillParam billParam);

    /**
     * 查询活跃用户
     * @return
     */
    List<ActiveMemberBean> findBuySellActiveMemberList(@Param("tradeType")String tradeType,
                                                       @Param("startTime")String startTime,
                                                       @Param("endTime")String endTime,
                                                       @Param("channelIds")List<String> channelIds);

    BigDecimal sumAmount(Map<String, Object> params);
    int sumMembers(Map<String, Object> params);

    /**
     * 查询上一日充值总额
     * @return
     */
    BigDecimal findRechargeTotal();

    /**
     * 查询充值总额脚本
     * @return
     */
    BigDecimal findRechargeScript(@Param("day") String day);

    int getTransactionNumForThisMonth(@Param("memberId")String memberId,
                                      @Param("tradeType")int tradeType);

    int getTransactionNumForThisDay(@Param("memberId")String memberId,
                                      @Param("tradeType")int tradeType);

    /**
     * 查询当月的充值总金额
     * @param memberId
     * @return
     */
    BigDecimal getTransactionAmountForThisMonth(@Param("memberId")String memberId);
}