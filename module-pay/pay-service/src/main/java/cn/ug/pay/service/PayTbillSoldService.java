package cn.ug.pay.service;

import cn.ug.pay.mapper.entity.PayTbillSold;

import java.math.BigDecimal;
import java.util.List;

public interface PayTbillSoldService {
    List<PayTbillSold> query(String mobile, String name, int startGram, int endGram, String startDate, String endDate, String order, String sort, int offset, int size,Integer[] productType,
                             String tbillNO,String totalAmountMin,String totalAmountMax,String actualAmountMin,String actualAmountMax);
    int count(String mobile, String name, int startGram, int endGram, String startDate, String endDate,Integer[] productType,
              String tbillNO,String totalAmountMin,String totalAmountMax,String actualAmountMin,String actualAmountMax);
    String sale(String orderNO, String goldPriceKey);
    BigDecimal getFee(String memberId, BigDecimal totalGram, BigDecimal goldPrice);

    PayTbillSold selectByOrderNO(String orderNO);
    PayTbillSold selectByTbillNO(String tbillNO);
    //定时任务需要的方法，防止新开事物导致数据异常
    String saleJob(String orderNO, BigDecimal goldPriceKey);

    /**
     * 提单涨幅收益列表
     * @param mobile
     * @param name
     * @param startGram
     * @param endGram
     * @param startDate
     * @param endDate
     * @param order
     * @param sort
     * @param productType
     * @param tbillNo
     * @param buyTotalAmountMin
     * @param buyTotalAmountMax
     * @param soldTotalAmountMin
     * @param soldTotalAmountMax
     * @return
     */
    List<PayTbillSold> queryIncome(String mobile, String name, int startGram, int endGram, String startDate, String endDate, String order, String sort,Integer[] productType,
                             String tbillNo,String buyTotalAmountMin,String buyTotalAmountMax,String soldTotalAmountMin,String soldTotalAmountMax);

}
