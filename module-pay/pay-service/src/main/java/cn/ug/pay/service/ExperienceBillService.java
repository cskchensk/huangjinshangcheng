package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.ExperienceBillBean;

public interface ExperienceBillService {

    SerializeObject add(ExperienceBillBean experienceBillBean);

    DataTable<ExperienceBillBean> queryList(String memberId,Integer type, int pageNum, int pageSize,Order order);
}
