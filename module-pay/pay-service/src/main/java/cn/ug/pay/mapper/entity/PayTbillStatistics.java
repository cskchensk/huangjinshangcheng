package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayTbillStatistics implements Serializable {
    private int id;
    private String memberId;
    //累计提单个数
    private int totalBills;
    //累计提单克重
    private int totalGram;
    //待处理提单个数
    private int pendingBills;
    //待处理提单克重
    private int pendingGram;
    //回租中提单个数
    private int leasebackBills;
    //回租中提单克重
    private int leasebackGram;
    //回租未到期总收益（元）
    private BigDecimal leasebackIncomeAmount;
    //回租未到期总收益（克）
    private BigDecimal leasebackIncomeGram;
    //回租未到期总收益（金豆）
    private int leasebackIncomeBeans;
    //已出售提单个数
    private int soldBills;
    //已出售提单克重
    private int soldGram;
    //已售提单到账总金额（元）
    private BigDecimal soldAmount;
    //提金冻结中提单个数
    private int frozenBills;
    //提金冻结克重
    private int frozenGram;
    //已提金个数
    private int extractedBills;
    //已提金克重
    private int extractedGram;
    //回租已到期总收益（元）
    private BigDecimal incomeAmount;
    //回租已到期总收益（克）
    private BigDecimal incomeGram;
    //回租已到期总收益（金豆）
    private int incomeBeans;
    //首次提单时间
    private String addTime;

    //扩展字段
    private int index;
    private String name;
    private String mobile;
    //当前提单个数
    private int currentBills;
    //当前提单总克重
    private int currentGram;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getLeasebackIncomeAmount() {
        return leasebackIncomeAmount;
    }

    public void setLeasebackIncomeAmount(BigDecimal leasebackIncomeAmount) {
        this.leasebackIncomeAmount = leasebackIncomeAmount;
    }

    public BigDecimal getLeasebackIncomeGram() {
        return leasebackIncomeGram;
    }

    public void setLeasebackIncomeGram(BigDecimal leasebackIncomeGram) {
        this.leasebackIncomeGram = leasebackIncomeGram;
    }

    public int getLeasebackIncomeBeans() {
        return leasebackIncomeBeans;
    }

    public void setLeasebackIncomeBeans(int leasebackIncomeBeans) {
        this.leasebackIncomeBeans = leasebackIncomeBeans;
    }

    public BigDecimal getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(BigDecimal soldAmount) {
        this.soldAmount = soldAmount;
    }

    public int getCurrentBills() {
        return currentBills;
    }

    public void setCurrentBills(int currentBills) {
        this.currentBills = currentBills;
    }

    public int getCurrentGram() {
        return currentGram;
    }

    public void setCurrentGram(int currentGram) {
        this.currentGram = currentGram;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getTotalBills() {
        return totalBills;
    }

    public void setTotalBills(int totalBills) {
        this.totalBills = totalBills;
    }

    public int getTotalGram() {
        return totalGram;
    }

    public void setTotalGram(int totalGram) {
        this.totalGram = totalGram;
    }

    public int getPendingBills() {
        return pendingBills;
    }

    public void setPendingBills(int pendingBills) {
        this.pendingBills = pendingBills;
    }

    public int getPendingGram() {
        return pendingGram;
    }

    public void setPendingGram(int pendingGram) {
        this.pendingGram = pendingGram;
    }

    public int getLeasebackBills() {
        return leasebackBills;
    }

    public void setLeasebackBills(int leasebackBills) {
        this.leasebackBills = leasebackBills;
    }

    public int getLeasebackGram() {
        return leasebackGram;
    }

    public void setLeasebackGram(int leasebackGram) {
        this.leasebackGram = leasebackGram;
    }

    public int getSoldBills() {
        return soldBills;
    }

    public void setSoldBills(int soldBills) {
        this.soldBills = soldBills;
    }

    public int getSoldGram() {
        return soldGram;
    }

    public void setSoldGram(int soldGram) {
        this.soldGram = soldGram;
    }

    public int getFrozenBills() {
        return frozenBills;
    }

    public void setFrozenBills(int frozenBills) {
        this.frozenBills = frozenBills;
    }

    public int getFrozenGram() {
        return frozenGram;
    }

    public void setFrozenGram(int frozenGram) {
        this.frozenGram = frozenGram;
    }

    public int getExtractedBills() {
        return extractedBills;
    }

    public void setExtractedBills(int extractedBills) {
        this.extractedBills = extractedBills;
    }

    public int getExtractedGram() {
        return extractedGram;
    }

    public void setExtractedGram(int extractedGram) {
        this.extractedGram = extractedGram;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public int getIncomeBeans() {
        return incomeBeans;
    }

    public void setIncomeBeans(int incomeBeans) {
        this.incomeBeans = incomeBeans;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
