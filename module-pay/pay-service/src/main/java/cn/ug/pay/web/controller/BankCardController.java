package cn.ug.pay.web.controller;


import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.pay.bean.request.BankCardBaseParamBean;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.response.BankCardFindBean;
import cn.ug.pay.bean.response.BankCardInfoBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.bean.response.MemberAccountManageBean;
import cn.ug.pay.mapper.entity.PayFundsRecord;
import cn.ug.pay.service.BankCardService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import java.io.Serializable;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 绑卡服务
 *
 * @author ywl
 * @date 2018/2/1
 */
@RestController
@RequestMapping("bankcard")
public class BankCardController extends BaseController {

    @Resource
    private BankCardService bankCardService;

    @Resource
    private Config config;

    /**
     * 查询绑卡列表(后台)
     *
     * @param accessToken       登录成功后分配的Key
     * @param bankCardParamBean 请求参数
     * @return 分页数据
     */
    @RequestMapping(value = "queryBankCardList", method = GET)
    public SerializeObject<DataTable<BankCardManageBean>> queryBankCardList(@RequestHeader String accessToken, BankCardParamBean bankCardParamBean) {
        if (bankCardParamBean.getPageSize() <= 0) {
            bankCardParamBean.setPageSize(config.getPageSize());
        }
        DataTable<BankCardManageBean> dataTable = bankCardService.queryBankCardList(bankCardParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询绑定银行卡(移动端)
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "queryMemberBankCard", method = GET)
    public SerializeObject<BankCardFindBean> queryMemberBankCard(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null) Ensure.that(true).isTrue("00000102");
        BankCardFindBean entity = bankCardService.queryMemberBankCard(loginBean.getId());
        if (null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObject(ResultType.NORMAL, "00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 绑定银行卡(移动端)
     *
     * @param accessToken 登录成功后分配的Key
     * @param entity      记录集
     * @return 是否操作成功
     */
    @RequestMapping(value = "bindBankCard", method = POST)
    public SerializeObject bindBankCard(@RequestHeader String accessToken, BankCardBaseParamBean entity) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null) Ensure.that(true).isTrue("00000102");
        entity.setMemberId(loginBean.getId());
        bankCardService.bindBandCard(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 绑定银行卡短信确认(移动端)
     *
     * @param accessToken 登录成功后分配的Key
     * @param code        短信验证码
     * @return 是否操作成功
     */
    @RequestMapping(value = "bindBankCardConfirm", method = POST)
    public SerializeObject bindBankCardConfirm(@RequestHeader String accessToken, String code) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null) Ensure.that(true).isTrue("00000102");
        String result = bankCardService.bindSmsConfirm(loginBean.getId(), code);
        if (StringUtils.isBlank(result)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, result);
        }

    }

    @RequestMapping(value = "test", method = POST)
    public SerializeObject bindBankCardConfirm() {
        bankCardService.test();
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 绑定银行卡短信重发(移动端)
     *
     * @param accessToken 登录成功后分配的Key
     * @return 是否操作成功
     */
    @RequestMapping(value = "bindBankCardReSendSms", method = POST)
    public SerializeObject bindBankCardReSendSms(@RequestHeader String accessToken) {
        bankCardService.bindBankCardReSendSms();
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 获取银行卡号基本信息(移动端)
     *
     * @param accessToken 登录成功后分配的Key
     * @return 是否操作成功
     */
    @RequestMapping(value = "queryBankCardInfo", method = GET)
    public SerializeObject queryBankCardInfo(@RequestHeader String accessToken, String cardNo) {
        BankCardInfoBean entity = bankCardService.queryBankCardInfo(cardNo);
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 验证是否绑卡
     *
     * @param memberId 会员ID
     * @return 是否操作成功
     */
    @RequestMapping(value = "validateBindBankCard", method = GET)
    public SerializeObject validateBindBankCard(String memberId) {
        int num = bankCardService.validateBindBankCard(memberId);
        if (num == 0) {
            return new SerializeObject<>(ResultType.ERROR, "00000003");
        } else {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
    }

    @RequestMapping(value = "/member/card", method = GET)
    public SerializeObject<BankCardFindBean> getBankCard(String memberId) {
        BankCardFindBean bean = bankCardService.queryMemberBankCard(memberId);
        if (bean != null) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001", bean);
        } else {
            return new SerializeObject<>(ResultType.ERROR, "00000003");
        }
    }


    /**
     * 银行账号管理导出
     *
     * @param bankCardParamBean
     * @return
     */
    @RequestMapping(value = "queryBankCardList/export", method = GET)
    public void queryBankCardListExport(BankCardParamBean bankCardParamBean, HttpServletResponse response) {
        bankCardParamBean.setPageSize(Integer.MAX_VALUE);
        bankCardParamBean.setPageNum(1);
        DataTable<BankCardManageBean> dataTable = bankCardService.queryBankCardList(bankCardParamBean);
        if (!CollectionUtils.isEmpty(dataTable.getDataList())) {
            int index = 1;
            for (BankCardManageBean bean : dataTable.getDataList()) {
                bean.setIndex(index);
                bean.setRemark("易宝支付");
                index++;
            }
            String[] columnNames = {"序号", "手机号", "用户姓名", "身份证号", "银行卡号", "银行预留手机号码", "所属银行", "支付通道", "创建时间"};
            String[] columns = {"index", "mobile", "name", "idCard", "cardNo", "reservedMobile", "bankName","remark","addTimeString"};
            String fileName = "银行账号管理";
            ExportExcelController<BankCardManageBean> export = new ExportExcelController<BankCardManageBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, dataTable.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }
}
