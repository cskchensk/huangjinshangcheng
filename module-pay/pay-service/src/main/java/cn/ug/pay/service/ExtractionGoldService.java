package cn.ug.pay.service;

import cn.ug.bean.base.DataOtherTable;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.GoldExtractionBean;
import cn.ug.pay.bean.request.ExtractionGoldOtherBean;
import cn.ug.pay.bean.request.ExtractionGoldPayBean;
import cn.ug.pay.bean.response.ExtractGoldDetail;
import cn.ug.pay.bean.response.ExtractGoldFindBean;
import cn.ug.pay.bean.response.ExtractGoldMyBean;
import cn.ug.pay.bean.response.ExtractGoldUserStatisticsBean;

import java.time.LocalDateTime;

public interface ExtractionGoldService {

    /**
     * 查询费率
     * @return
     */
    SerializeObject findFee(String tbillNo);

    /**
     * 提金提交
     * @param requestParams
     */
    SerializeObject pay(ExtractionGoldPayBean requestParams, String memberId);

    DataTable<ExtractGoldFindBean> query(String order, String sort, int pageNum, int pageSize,
                                         Integer type, String orderNo,
                                         String  goodsName, String memberMobile,
                                         Integer minGram, Integer maxGram,
                                         LocalDateTime addTimeMinString, LocalDateTime addTimeMaxString,Integer status,String shopName);

    /**
     * 关闭提金订单
     */
    SerializeObject closeOrder(ExtractionGoldOtherBean submit);

    /**
     * 备注订单
     * @param submit
     */
    SerializeObject remarkOrder(ExtractionGoldOtherBean submit);

    /**
     * 查看备注
     * @param orderNo
     * @return
     */
    SerializeObject findRemark(String orderNo,Integer type);

    /**
     * 订单发货
     * @param submit
     * @return
     */
    SerializeObject  orderDelivery(ExtractionGoldOtherBean submit);

    /**
     * 订单跟踪
     * @param orderNo
     * @return
     */
    SerializeObject  orderTrack(String orderNo);

    /**
     * 查询订单详情
     * @param orderNo
     * @return
     */
    SerializeObject findOrder(String orderNo,Integer type);

    /**
     * 查询用户提金统计
     * @param memberName
     * @param mobile
     * @param minGram
     * @param maxGram
     * @return
     */
    DataTable<ExtractGoldUserStatisticsBean> findUserStatistics (int pageNum, int pageSize,String memberName, String mobile, Integer minGram, Integer maxGram);

    /**
     * 查询用户提金明细
     * @param pageNum
     * @param pageSize
     * @param memberId
     * @return
     */
    DataTable<ExtractGoldFindBean> queryDetail(int pageNum, int pageSize,String memberId);

    /**
     * 订单支付完成
     * @param orderNo  订单号
     * @param type     类型 1:快递提金 2:门店提金
     */
    void succeed(String orderNo,Integer type);

    /**
     * 统计未读订单信息
     * @return
     */
    int countUnread();

    /**
     * 信息已读
     * @param orderNo
     * @return
     */
    void read(String orderNo,Integer type);
    /**
     * 我的提金订单列表
     * @return
     */
    DataOtherTable<ExtractGoldMyBean> findList(String memberId, int pageNum, int pageSize, Integer status);

    /**
     * 查询订单详情
     * @param orderNo
     * @param type  1快递 2门店
     * @return
     */
    SerializeObject<ExtractGoldDetail> findDetail(String orderNo,Integer type);

    /**
     * 支付完成后进行微信提金预约通知
     * @param memberId
     * @param productName  商品名称
     * @param address      收货地址
     * @param fullname     收货人
     * @param cellphone    收货电话
     */
    void wxSubscribeSend(String memberId,String productName,String address,String fullname,String cellphone);

    /**
     * 提金取消或者提金支付失败微信推送通知
     * @param memberId
     * @param statusMsg
     */
    void wxCloseSend(String memberId,String statusMsg);

    /**
     * 门店提金完成
     * @param orderNo
     * @param type
     */
    SerializeObject extractionSucceed(String orderNo,Integer type,Integer status);

    GoldExtractionBean selectByTbillNO(String tbillNO);
}
