package cn.ug.pay.service.impl;

import cn.ug.pay.mapper.PayTbillLeasebackIncomeMapper;
import cn.ug.pay.mapper.entity.PayTbillLeasebackIncome;
import cn.ug.pay.service.PayTbillLeasebackIncomeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayTbillLeasebackIncomeServiceImpl implements PayTbillLeasebackIncomeService {
    @Autowired
    private PayTbillLeasebackIncomeMapper payTbillLeasebackIncomeMapper;

    @Override
    public List<PayTbillLeasebackIncome> query(String memberId, String startDate, String endDate, String order, String sort, int offset, int size) {
        if (StringUtils.isBlank(memberId)) {
            return null;
        }
        List<String> orders = Arrays.asList("leaseback_income_amount", "leaseback_income_gram", "leaseback_income_beans", "income_amount", "income_gram", "income_beans");
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("memberId", memberId);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return payTbillLeasebackIncomeMapper.query(params);
    }

    @Override
    public int count(String memberId, String startDate, String endDate) {
        if (StringUtils.isBlank(memberId)) {
            return 0;
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("memberId", memberId);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return payTbillLeasebackIncomeMapper.count(params);
    }
}
