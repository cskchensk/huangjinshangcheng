package cn.ug.pay.service;


import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.MemberGoldBean;
import cn.ug.pay.bean.MemberGoldBeanBean;
import cn.ug.pay.bean.MemberGoldGroupByDayBean;
import cn.ug.pay.bean.SellGoldBean;
import cn.ug.pay.bean.type.BillGoldTradeType;
import cn.ug.pay.mapper.entity.PayGoldStock;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 会员资产账户管理
 * @author kaiwotech
 */
public interface MemberGoldService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(MemberGoldBean entityBean);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	MemberGoldBean findById(String id);

	/**
	 * 根据会员ID, 查找对象
	 * @param memberId	会员ID
	 * @return		实例
	 */
	MemberGoldBean findByMemberId(String memberId);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param amountMin		最小黄金总资产
	 * @param amountMax		最大黄金总资产
	 * @param usableAmountMin	最小可用资产
	 * @param usableAmountMax	最大可用资产
	 * @param freezeAmountMin	最小冻结资产
	 * @param freezeAmountMax	最大冻结资产
	 * @param turnIntoAmountMin	最小转入资产
	 * @param turnIntoAmountMax	最大转入资产
	 * @param turnOutAmountMin	最小转出资产
	 * @param turnOutAmountMax	最大转出资产
	 * @param addTimeMin	最小创建时间
	 * @param addTimeMax	最大创建时间
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<MemberGoldBean> query(String order, String sort, int pageNum, int pageSize,
									BigDecimal amountMin, BigDecimal amountMax,
									BigDecimal usableAmountMin, BigDecimal usableAmountMax,
									BigDecimal freezeAmountMin, BigDecimal freezeAmountMax,
									BigDecimal turnIntoAmountMin, BigDecimal turnIntoAmountMax,
									BigDecimal turnOutAmountMin, BigDecimal turnOutAmountMax,
									LocalDateTime addTimeMin, LocalDateTime addTimeMax,
									String memberName, String memberMobile, String keyword);

	/**
	 * 查询有效账户
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @return				分页数据
	 */
	DataTable<MemberGoldBean> queryActiveAccount(int pageNum, int pageSize);
	List<MemberGoldBean> queryActiveAccountForList(int offset, int size);
	List<MemberGoldBean> queryActiveAccountForCurrentList(int offset, int size);

	/**
	 * 每日利息计算
	 * @param memberId	会员ID
	 */
	void interestCalculate(String memberId);

	/**
	 * 每日可用资产详表记录
	 * @param memberId	会员ID
	 */
	void recordGoldDetailPerDay(String memberId);

	/**
	 * 增加T日本金（活期投资/其他收益发放）
	 * @param memberId		会员ID
	 * @param amount		购买克重
	 * @param tradeType		转入类型
	 * @param goldPrice		金价
	 */
	void addPrincipalNowAmount(String memberId, BigDecimal amount, BillGoldTradeType tradeType, BigDecimal goldPrice);

	/**
	 * 冻结资产（固定投资）
	 * @param memberId		会员ID
	 * @param amount		购买克重
	 * @param incomeAmount	预期收益
	 */
	void freezeAmount(String memberId, BigDecimal amount, BigDecimal incomeAmount);

	/**
	 * 解冻资产（定期投资到期解除冻结，连本带息转入T日本金）
	 * @param memberId		会员ID
	 * @param amount		购买克重
	 * @param incomeAmount	预期收益
	 * @param tradeType		转入类型
	 * @param goldPrice		金价
	 */
	void unfreezeAmount(String memberId, BigDecimal amount, BigDecimal incomeAmount, BillGoldTradeType tradeType, BigDecimal goldPrice);

	/**
	 * 可用资产支付。支付优先级
	 * 1、T-1日利息
	 * 2、T日本金
	 * 3、T-1日本金
	 *
	 * @param memberId		会员ID
	 * @param amount		支付金额
	 * @param tradeType		转出类型
	 * @param goldPrice		金价
	 * @return				足额支付：{支付总金额、T-1日利息、T日本金、T-1日本金} 支付失败：null
	 */
	BigDecimal[] payAmount(String memberId, BigDecimal amount, BillGoldTradeType tradeType, BigDecimal goldPrice, BigDecimal feeFactor);

	/**
	 * 尝试可用资产支付。支付优先级
	 * 1、T-1日利息
	 * 2、T日本金
	 * 3、T-1日本金
	 *
	 * @param memberId		会员ID
	 * @param amount		支付金额
	 * @return				是否足够支付
	 */
	boolean tryPayAmount(String memberId, BigDecimal amount);

	/**
	 * 查询可用于支付和提现的余额 = T-1日利息 + T日本金 + T-1日本金
	 * @param memberId		会员ID
	 * @return				可用于支付和提现的余额
	 */
	BigDecimal viewUsableAmount(String memberId);

	/**
	 * 卖金计算接口（锁定金价）
	 * @param memberId		会员ID
	 * @param weight		出售克重
	 * @return
	 */
	SellGoldBean sellGoldCalculate(String memberId, BigDecimal weight);

	/**
	 * 卖金
	 * @param memberId		会员ID
	 * @param weight		出售克重
	 * @return				是否成功
	 */
	boolean sellGold(String memberId, BigDecimal weight);

	/**
	 * 每日转入转出记录
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param memberId		会员ID
	 * @return				分页数据
	 */
	DataTable<MemberGoldGroupByDayBean> queryGroupByDay(int pageNum, int pageSize, String memberId);

	PayGoldStock selectEverydayTotalGramRecord();

	List<MemberGoldBeanBean> queryForBean(String name, String mobile, int beanFrom, int beanTo, String startDate, String endDate, String order, String sort, int offset, int size);
	int countForBean(String name, String mobile, int beanFrom, int beanTo, String startDate, String endDate);
}
