package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.BankCardParamBean;
import cn.ug.pay.bean.request.BankInfoParamBean;
import cn.ug.pay.bean.response.BankCardFindBean;
import cn.ug.pay.bean.response.BankCardManageBean;
import cn.ug.pay.bean.response.BankInfoBean;
import cn.ug.pay.mapper.entity.BankCard;
import cn.ug.pay.mapper.entity.BankInfo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author ywl
 * @date 2018/2/1
 */
@Component
public interface BankInfoMapper extends BaseMapper<BankInfo> {

    /**
     * 根据银行编号查询
     * @param bankCode
     * @return
     */
    BankInfoBean queryBankInfo(String bankCode);

    /**
     * 获取所有银行基础信息
     * @return
     */
    List<BankInfoBean> queryBankList();

    /**
     * 获取银行信息--后台
     * @param bankInfoParamBean
     * @return
     */
    List<BankInfoBean> findList(BankInfoParamBean bankInfoParamBean);

    /**
     * 查询所有设置银行维护时间
     * @return
     */
    List<BankInfo> findBankInfoJob();

    /**
     * 更新银行维护
     * @param param
     */
    void updateBankInfoJob(Map<String,Object> param);

}

