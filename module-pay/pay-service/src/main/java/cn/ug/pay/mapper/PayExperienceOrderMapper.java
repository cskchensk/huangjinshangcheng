package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayExperienceOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayExperienceOrderMapper {
    List<PayExperienceOrder> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    PayExperienceOrder findByOrderNO(@Param("orderNO")String orderNO);
    int insert(PayExperienceOrder order);
    int update(Map<String, Object> params);
    List<PayExperienceOrder> queryBecomeDueOrders();
    List<PayExperienceOrder> queryBuyRecordList(Map<String, Object> params);

    int countSuccessfulNum(@Param("memberId")String memberId);
}
