package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 提金
 */
public class ExtractGold   extends BaseEntity implements java.io.Serializable{

    /** 订单号 **/
    private String orderId;
    /** 会员id **/
    private String memberId;
    /** 姓名 **/
    private String name;
    /** 手机号码 **/
    private String mobile;
    /** 身份号 **/
    private String idCard;
    /** 金额 **/
    private BigDecimal amount;
    /** 创建时间　**/
    private LocalDateTime addTime;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public LocalDateTime getAddTime() {
        return addTime;
    }

    @Override
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }
}
