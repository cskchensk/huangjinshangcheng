package cn.ug.pay.service;


import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.BillGoldBean;
import cn.ug.pay.bean.EverydaySellGoldBean;
import cn.ug.pay.bean.SellGoldRecordBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface BillGoldService {

	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(BillGoldBean entityBean);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);

	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	BillGoldBean findById(String id);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param type			类型 0:全部 1:转入 2:转出
	 * @param tradeType		交易类型 0:全部 1:投资活期产品  2:定期转活期 3:存金 4:活期转定期 5:提金 6:卖金
	 * @param amountMin		最小资产收支记录金额
	 * @param amountMax		最大资产收支记录金额
	 * @param addTimeMin	最小操作时间
	 * @param addTimeMax	最大操作时间
	 * @param memberId		会员ID
	 * @param memberName	会员名称
	 * @param memberMobile	会员手机
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<BillGoldBean> query(String order, String sort, int pageNum, int pageSize, Integer type, Integer tradeType,
                              BigDecimal amountMin, BigDecimal amountMax, LocalDateTime addTimeMin, LocalDateTime addTimeMax,
                              String memberId, String memberName, String memberMobile, String keyword);

	/**
	 * 统计交易总额
	 * @param memberId		会员ID
	 * @param tradeType		交易类型 0:全部 1:投资活期产品  2:定期转活期 3:存金 4:活期转定期 5:提金 6:卖金
	 * @param date			日期（yyyy-MM-dd）
	 * @return				交易总金额
	 */
	BigDecimal totalAmountByTradeType(String memberId, Integer tradeType, String date);

	/**
	 * 统计当月交易数量
	 * @param memberId		会员ID
	 * @param tradeType		交易类型 0:全部 1:投资活期产品  2:定期转活期 3:存金 4:活期转定期 5:提金 6:卖金
	 * @return				交易总金额
	 */
	Integer totalNumByTradeTypeTheMonth(String memberId, Integer tradeType);

	List<EverydaySellGoldBean> queryEverydaySellGoldRecords(String payDate);

	List<SellGoldRecordBean> querySellGoldRecord(String payDate, int offset, int size);
	int countSellGoldRecord(String payDate);
}
