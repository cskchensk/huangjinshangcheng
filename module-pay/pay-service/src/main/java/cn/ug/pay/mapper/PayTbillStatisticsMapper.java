package cn.ug.pay.mapper;

import cn.ug.pay.bean.TbillRecordBean;
import cn.ug.pay.mapper.entity.PayTbillStatistics;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayTbillStatisticsMapper {
    List<PayTbillStatistics> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<TbillRecordBean> queryForRecords(Map<String, Object> params);
    int countForRecords(Map<String, Object> params);

    List<PayTbillStatistics> queryIncomeForList(Map<String, Object> params);

    int insert(PayTbillStatistics payTbillStatistics);
    int update(Map<String, Object> params);
    PayTbillStatistics selectByMemberId(@Param("memberId")String memberId);
}
