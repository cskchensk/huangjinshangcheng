package cn.ug.pay.service.impl;

import cn.ug.pay.mapper.PayTbillStatusMapper;
import cn.ug.pay.mapper.entity.PayTbillStatus;
import cn.ug.pay.service.PayTbillStatusService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PayTbillStatusServiceImpl implements PayTbillStatusService {
    @Autowired
    private PayTbillStatusMapper payTbillStatusMapper;


    @Override
    public List<PayTbillStatus> query(String tbillNO, String type, int status, String order, String sort, int offset, int size) {
        if (StringUtils.isNotBlank(tbillNO)) {
            List<String> orders = Arrays.asList("modify_time", "add_time");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("tbillNO", tbillNO);
            if (StringUtils.isNotBlank(type)) {
                params.put("type", type);
            }
            params.put("status", status);
            if (orders.contains(order)) {
                params.put("order", order);
                params.put("sort", "desc");
                if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                    params.put("sort", sort);
                }
            }
            params.put("offset", offset);
            params.put("size", size);
            return payTbillStatusMapper.query(params);
        }
        return null;
    }

    @Override
    public int count(String tbillNO, String type, int status) {
        if (StringUtils.isNotBlank(tbillNO)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("tbillNO", tbillNO);
            if (StringUtils.isNotBlank(type)) {
                params.put("type", type);
            }
            params.put("status", status);
            return payTbillStatusMapper.count(params);
        }
        return 0;
    }
}
