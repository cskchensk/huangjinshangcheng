package cn.ug.pay.mapper;

import cn.ug.member.bean.MemberDetailBean;
import cn.ug.pay.bean.*;
import cn.ug.pay.bean.response.UserInvestBean;
import cn.ug.pay.mapper.entity.PayTbill;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayTbillMapper {
    List<PayTbill> query(Map<String, Object> params);
    List<PayTbill> queryForExpiredList();
    int count(Map<String, Object> params);

    int insert(PayTbill payTbill);
    int update(Map<String, Object> params);

    int getTransactionNumForThisMonth(@Param("memberId")String memberId);
    int getTransactionNumForThisDay(@Param("memberId")String memberId);
    int countSuccessfulNum(@Param("memberId")String memberId);
    int sumSuccessfulGram(@Param("memberId")String memberId, @Param("productId")String productId);

    int expired(@Param("ids")List<Integer> ids);

    PayTbill selectByOrderNO(@Param("orderNO")String orderNO);

    int sumToadySuccessGram(@Param("memberId")String memberId);

    List<TbillCenterBean> queryForTbillCenter(Map<String, Object> params);
    int countForTbillCenter(Map<String, Object> params);

    int read(@Param("orderNO")String orderNO);
    int readUndeterminedBills(@Param("memberId")String memberId);
    int countUnreadNum(@Param("memberId")String memberId, @Param("status")int status);

    List<FrozenTbillBean> queryForFrozenTbills(Map<String, Object> params);
    int countForFrozenTbills(Map<String, Object> params);

    List<WaitingPayBean> queryForWaitingTbills(Map<String, Object> params);
    int countForWaitingTbills(Map<String, Object> params);

    List<TbillRecord> queryForRecords(Map<String, Object> params);
    int countForRecords(Map<String, Object> params);

    List<TbillBean> queryBuyRecords(@Param("productId")String productId);

    List<TbillCenterBean> queryForTbillList(@Param("memberId")String memberId);

    String queryForType(Map<String, Object> params);

    int queryCountForRecordsList(@Param("memberId")String memberId);

    MemberDetailBean selectMemberTradeInfo(@Param("memberId") String memberId);

    //获取投资动态list
    List<UserInvestBean> findInvestList(Map<String, Object> params);
}
