package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayTbillStatus;

import java.util.List;
import java.util.Map;

public interface PayTbillStatusMapper {
    List<PayTbillStatus> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    int insert(PayTbillStatus payTbillStatus);
}
