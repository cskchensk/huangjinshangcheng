package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.ExperienceBillEntity;

import java.util.List;
import java.util.Map;

public interface ExperienceBillMapper {

    int insert(ExperienceBillEntity experienceBillEntity);

    List<ExperienceBillEntity> queryList(Map map);

}
