package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.ApplyExtractGoldBean;
import cn.ug.pay.bean.request.ExtractGoldParamBean;
import cn.ug.pay.bean.response.ExtractGoldBean;

import java.util.List;

public interface ExtractGoldService {

    /**
     * 提金列表--后台
     * @param extractGoldParamBean	    请求参数
     * @return				            分页数据
     */
    DataTable<ExtractGoldBean> findList(ExtractGoldParamBean extractGoldParamBean);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    ExtractGoldBean findById(String id);

    /**
     * 申请提金
     * @param applyExtractGoldBean
     */
    void applyExtractGold(ApplyExtractGoldBean applyExtractGoldBean);

    /**
     * 导出提金
     * @param extractGoldParamBean
     * @return
     */
    List<ExtractGoldBean> findExportList(ExtractGoldParamBean extractGoldParamBean);
}
