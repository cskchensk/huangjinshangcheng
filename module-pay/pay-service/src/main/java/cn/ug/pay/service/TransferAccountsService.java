package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.pay.bean.TransferAccountsBean;
import cn.ug.pay.bean.TransferAccountsQueryBean;
import cn.ug.pay.bean.TransferAccountsResultBean;
import cn.ug.pay.mapper.entity.TransferAccounts;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author zhangweijie
 * @Date 2019/7/22 0022
 * @time 下午 17:34
 **/
public interface TransferAccountsService {

    /**
     * 保存/修改转账申请
     * @param transferAccountsBean
     * @return
     */
    SerializeObject save(TransferAccountsBean transferAccountsBean);

    /**
     *
     * @param transferAccountsQueryBean
     * @return
     */
    DataTable<TransferAccountsResultBean> queryList(TransferAccountsQueryBean transferAccountsQueryBean);

    /**
     * 审核操作
     * @param id      转账申请id
     * @param status  审核状态  2:已通过 3:已拒绝
     * @param remark  审核拒绝备注
     * @return
     */
    SerializeObject updateAuditStatus(String id, Integer status, String remark);
}
