package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.bean.request.FinanceBillInfoParam;
import cn.ug.pay.bean.response.FinanceBillInfoBean;
import cn.ug.pay.mapper.entity.FinanceBillInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface FinanceBillInfoMapper extends BaseMapper<FinanceBillInfo> {

    /**
     *
     * @param financeBillInfoParam
     * @return
     */
    List<FinanceBillInfoBean> findList(FinanceBillInfoParam financeBillInfoParam);
}
