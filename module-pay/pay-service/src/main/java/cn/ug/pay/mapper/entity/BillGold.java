package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 资产收支记录
 * @author kaiwotech
 */
public class BillGold extends BaseEntity implements java.io.Serializable {

	/** 订单号 */
	private String orderId;
	/** 会员id */
	private String memberId;
	/** 资产 */
	private BigDecimal amount;
	/** 手续费 */
	private BigDecimal fee;
	/** 实际资产(资产-手续费) */
	private BigDecimal actualAmount;
	/** 金价 */
	private BigDecimal goldPrice;
	/** 类型 1:转入 2:转出 */
	private Integer type;
	/** 交易类型 1:投资活期产品  2:定期转活期 3:存金 4:活期转定期 5:提金 6:卖金 7:投资返利 */
	private Integer tradeType;

	/** 会员名称 */
	private String memberName;
	/** 会员手机 */
	private String memberMobile;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(BigDecimal actualAmount) {
		this.actualAmount = actualAmount;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getTradeType() {
		return tradeType;
	}

	public void setTradeType(Integer tradeType) {
		this.tradeType = tradeType;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberMobile() {
		return memberMobile;
	}

	public void setMemberMobile(String memberMobile) {
		this.memberMobile = memberMobile;
	}

	public BigDecimal getGoldPrice() {
		return goldPrice;
	}

	public void setGoldPrice(BigDecimal goldPrice) {
		this.goldPrice = goldPrice;
	}
}
