package cn.ug.pay.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;
import cn.ug.pay.bean.request.ExtractionGoldPayBean;
import cn.ug.pay.bean.request.PayParamBean;
import cn.ug.pay.bean.status.CommonConstants;
import cn.ug.util.UF;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author ywl
 * @date 2018/1/30
 */
public class Trade extends BaseEntity implements Serializable {

    /** 交易ID **/
    private String id;
    /** 会员Id **/
    private String memberId;
    /** 产品名称 **/
    private String title;
    /** 订单号 **/
    private String orderId;
    /** 第三方支付流水号 **/
    private String thirdNumber;
    /** 金额 **/
    private BigDecimal amount;
    /** 实际支付金额**/
    private BigDecimal payAmount;
    /** 手续费 **/
    private BigDecimal fee;
    /** 交易状态 1:待支付 2:已支付 3:支付失败 **/
    private Integer status;
    /** 交易类型 1:充值 2:买金 **/
    private Integer type;
    /** 支付方式 支付方式 1:易宝支付 **/
    private Integer payWay;
    /** 绑卡Id **/
    private String bankCardId;
    /** 扩展订单号--首次订单号一致,其余变化 **/
    private String serialNumber;
    /** 备注 --失败信息 **/
    private String description;
    /** 错误码 **/
    private String errCode;
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getThirdNumber() {
        return thirdNumber;
    }

    public void setThirdNumber(String thirdNumber) {
        this.thirdNumber = thirdNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public String getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(String bankCardId) {
        this.bankCardId = bankCardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public static  Trade convert(PayParamBean payParamBean){
        Trade  trade = new Trade();
        trade.setId(UF.getRandomUUID());
        trade.setMemberId(payParamBean.getMemberId());
        trade.setOrderId(payParamBean.getOrderId());
        trade.setSerialNumber(payParamBean.getOrderId());
        trade.setSerialNumber(payParamBean.getOrderId());
        trade.setStatus(CommonConstants.PayStatus.PROCESSING.getIndex());
        trade.setTitle(payParamBean.getTitle());
        trade.setAmount(payParamBean.getAmount());
        trade.setPayAmount(payParamBean.getAmount());
        trade.setBankCardId(payParamBean.getBankCardId());
        trade.setType(payParamBean.getType());
        trade.setPayWay(payParamBean.getWay());
        trade.setFee(BigDecimal.ZERO);
        return trade;
    }

    public static  Trade convertBean(ExtractionGoldPayBean payParamBean,String memberId,String orderNo){
        Trade  trade = new Trade();
        trade.setId(UF.getRandomUUID());
        trade.setMemberId(memberId);
        trade.setOrderId(orderNo);
        trade.setSerialNumber(orderNo);
        trade.setSerialNumber(orderNo);
        trade.setStatus(CommonConstants.PayStatus.PROCESSING.getIndex());
        trade.setTitle(payParamBean.getTitle());
        trade.setAmount(payParamBean.getActualAmount());
        trade.setPayAmount(payParamBean.getActualAmount());
        trade.setBankCardId(payParamBean.getBankCardId());
        trade.setType(payParamBean.getType());
        trade.setPayWay(payParamBean.getWay());
        trade.setFee(BigDecimal.ZERO);
        return trade;
    }
}
