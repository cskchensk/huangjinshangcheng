package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.FinanceBillParam;
import cn.ug.pay.bean.response.FinanceBillBean;
import cn.ug.pay.service.FinanceBillService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("financeBalance")
public class FinanceBillController {

    @Resource
    private FinanceBillService financeBillService;

    @Resource
    private Config config;

    /**
     * 查询财务对账列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param financeBillParam       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<FinanceBillBean>> findList(@RequestHeader String accessToken, FinanceBillParam financeBillParam) {
        if(financeBillParam.getPageSize() <= 0) {
            financeBillParam.setPageSize(config.getPageSize());
        }
        DataTable<FinanceBillBean> dataTable = financeBillService.findList(financeBillParam);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    @RequestMapping(value = "/export" , method = GET)
    public void exportData(HttpServletResponse response, FinanceBillParam financeBillParam) {
        List<FinanceBillBean> list = financeBillService.queryAllList(financeBillParam);
        if(list != null && list.size() > 0) {
            if (StringUtils.equals(String.valueOf(financeBillParam.getType()), String.valueOf(1))) {
                String[] columnNames = {"序号", "日期", "充值总额(元)", "每日充值人数(人)", "支付通道"};
                String[] columns = {"index", "day", "amount", "total", "remark"};
                String fileName = "每日充值总表";
                int index = 1;
                for (FinanceBillBean bean : list) {
                    bean.setIndex(index);
                    bean.setRemark("易宝支付");
                    index++;
                }
                ExportExcelController<FinanceBillBean> export = new ExportExcelController<FinanceBillBean>();
                export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
            } else {
                String[] columnNames = {"序号", "日期", "提现总额(元)", "每日提现人数(人)", "支付通道"};
                String[] columns = {"index", "day", "amount", "total", "remark"};
                String fileName = "每日提现总表";
                int index = 1;
                for (FinanceBillBean bean : list) {
                    bean.setIndex(index);
                    bean.setRemark("易宝支付");
                    index++;
                }
                ExportExcelController<FinanceBillBean> export = new ExportExcelController<FinanceBillBean>();
                export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
            }
        }
    }
}
