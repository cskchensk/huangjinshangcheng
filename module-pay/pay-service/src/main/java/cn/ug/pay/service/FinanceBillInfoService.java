package cn.ug.pay.service;

import cn.ug.bean.base.DataTable;
import cn.ug.pay.bean.request.FinanceBillInfoParam;
import cn.ug.pay.bean.response.FinanceBillInfoBean;

/**
 * 财务对账
 * @auther ywl
 * @date 2018-04-13 13:50
 */
public interface FinanceBillInfoService {

    /**
     * 后台
     * 获取财务账单列表
     * @return
     */
    DataTable<FinanceBillInfoBean> findList(FinanceBillInfoParam financeBillInfoParam);
}
