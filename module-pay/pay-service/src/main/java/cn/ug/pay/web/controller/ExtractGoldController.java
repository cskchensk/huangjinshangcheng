package cn.ug.pay.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.pay.bean.request.ApplyExtractGoldBean;
import cn.ug.pay.bean.request.ExtractGoldParamBean;
import cn.ug.pay.bean.response.ExtractGoldBean;
import cn.ug.pay.service.ExtractGoldService;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("extractGold")
public class ExtractGoldController {

    @Resource
    private ExtractGoldService extractGoldService;

    @Resource
    private Config config;

    /**
     * 查询提金列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param extractGoldParamBean       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<ExtractGoldBean>> findList(@RequestHeader String accessToken, ExtractGoldParamBean extractGoldParamBean) {
        if(extractGoldParamBean.getPageSize() <= 0) {
            extractGoldParamBean.setPageSize(config.getPageSize());
        }
        DataTable<ExtractGoldBean> dataTable = extractGoldService.findList(extractGoldParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 申请提金(后台)
     * @param accessToken		                登录成功后分配的Key
     * @param applyExtractGoldBean		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, ApplyExtractGoldBean applyExtractGoldBean) {
        extractGoldService.applyExtractGold(applyExtractGoldBean);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

}
