package cn.ug.pay.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.pay.mapper.entity.ProductOrderFee;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 定期投资费用
 * @author kaiwotech
 */
@Component
public interface ProductOrderFeeMapper extends BaseMapper<ProductOrderFee> {
    ProductOrderFee findByOrderId(@Param("orderId")String orderId);
}