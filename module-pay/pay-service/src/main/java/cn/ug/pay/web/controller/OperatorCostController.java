package cn.ug.pay.web.controller;

import cn.ug.bean.base.*;
import cn.ug.bean.type.ResultType;
import cn.ug.pay.bean.OperatorCostBean;
import cn.ug.pay.mapper.entity.CostBean;
import cn.ug.pay.service.OperatorCostService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author zhangweijie
 * @Date 2019/4/29 0029
 * @time 上午 10:40
 **/
@RestController
@RequestMapping("operator/cost")
public class OperatorCostController {

    @Autowired
    private OperatorCostService operatorCostService;

    @GetMapping(value = "/list")
    public SerializeObject list(@RequestHeader String accessToken, Order order, Page page, String startTime, String endTime) {
        return new SerializeObject<>(ResultType.NORMAL, operatorCostService.list(order, page.getPageNum(), page.getPageSize(), startTime, endTime));
    }


    @GetMapping(value = "/export")
    public void export(Order order, String startTime, String endTime, HttpServletResponse response) {
        DataOtherTable<OperatorCostBean> resultList = operatorCostService.list(order, 1, Integer.MAX_VALUE, startTime, endTime);

        List<OperatorCostBean> operatorCostBeanList = resultList.getDataList();
        if (!CollectionUtils.isEmpty(operatorCostBeanList)) {
            int index = 1;
            for (OperatorCostBean operatorCostBean : operatorCostBeanList) {
                operatorCostBean.setIndex(index);
                index++;
            }
        }
        String[] columnNames = {"序号", "日期", "每日线上运营成本总和（元）", "每日黄金红包总成本（元）", "每日金豆抵扣总成本（元）", "每日商城满减券总成本（元）"};
        String[] columns = {"index", "date", "operationAllCost", "goldCouponAllCost", "goldBeanAllCost", "shopElectronicCard"};
        String fileName = "每日线上运营成本总表";
        ExportExcelController<OperatorCostBean> export = new ExportExcelController<OperatorCostBean>();
        Map<String, Object> parmasMap = resultList.getResultMap();
        String[] firstLines = {"合计", "", parmasMap.get("allCost") + "元", parmasMap.get("allCouponAll") + "元", parmasMap.get("allBean") + "元", parmasMap.get("allShopElectronic") + "元"};
        export.exportUnlikeExcel(fileName, fileName, columnNames, columns, operatorCostBeanList, response, ExportExcelUtil.EXCEL_FILE_2003, firstLines);
    }

    /**
     * 查询每日线上运营成本详表
     *
     * @param accessToken
     * @param order
     * @param page
     * @param date
     * @param name
     * @param mobile
     * @param type 1：体验金 2：红包 3：金豆 4：回租 5：商城满减卷
     * @return
     */
    @GetMapping(value = "/query/info")
    public SerializeObject queryInfo(@RequestHeader String accessToken, Order order, Page page, String date, String name, String mobile,Integer type) {
        if (StringUtils.isBlank(date)) {
            return new SerializeObject<>(ResultType.ERROR, "170020474");
        }
        return new SerializeObject<>(ResultType.NORMAL, operatorCostService.queryInfo(order, page.getPageNum(), page.getPageSize(), date, name, mobile,type,null));
    }

    /**
     * 导出
     *
     * @param order
     * @param date
     * @param name
     * @param mobile
     * @param orderNo
     * @param response
     */
    @GetMapping(value = "/query/info/export")
    public void queryInfoExport(Order order, String date, String name, String mobile,Integer type,String orderNo,HttpServletResponse response) {
        DataTable resultList = operatorCostService.queryInfo(order, 1, Integer.MAX_VALUE, date, name, mobile,type,orderNo);
        List<CostBean> operatorCostBeanList = resultList.getDataList();
        if (!CollectionUtils.isEmpty(operatorCostBeanList)) {
            int index = 1;
            for (CostBean costBean : operatorCostBeanList) {
                costBean.setIndex(index);
                String remark = null;
                switch (costBean.getType()) {
                    case 1:
                        remark = "体验金奖励";
                        break;
                    case 2:
                        remark = "黄金红包成本";
                        break;
                    case 3:
                        remark = "金豆抵扣成本";
                        break;
                    case 4:
                        remark = "回租福利券成本";
                        break;
                    case 5:
                        remark = "商城满减券成本";
                        break;
                }
                costBean.setRemark(remark);
                index++;
            }
        }
        String[] columnNames = {"序号", "操作流水号", "手机号码", "用户姓名", "类型", "该类型运营成本（元）", "操作时间"};
        String[] columns = {"index", "orderNo", "mobile", "name", "remark", "cost", "addTime"};
        String fileName = "每日线上运营成本详表";
        ExportExcelController<CostBean> export = new ExportExcelController<CostBean>();
        String[] firstLines = {"", "", "合计", operatorCostBeanList.size() + "人","",operatorCostBeanList.stream().map(CostBean::getCost).reduce(BigDecimal.ZERO,BigDecimal::add)+"元",""};
        export.exportUnlikeExcel(fileName, fileName, columnNames, columns, operatorCostBeanList, response, ExportExcelUtil.EXCEL_FILE_2003,firstLines);
    }

    /**
     * 在运营成本总表页面导出祥表
     * @param dates  多个日期 yyyy-mm-dd 多个以逗号隔开
     * @param response
     */
    @GetMapping(value = "/query/info/exports")
    public void queryInfoExport(String dates,HttpServletResponse response) {
        String[] dateArr = dates.split(",");
        HSSFWorkbook workbook = new HSSFWorkbook();
        for (int i = 0;i<dateArr.length;i++){
            DataTable resultList = operatorCostService.queryInfo(new Order(), 1, Integer.MAX_VALUE, dateArr[i], null, null,null,null);
            List<CostBean> operatorCostBeanList = resultList.getDataList();
            if (!CollectionUtils.isEmpty(operatorCostBeanList)) {
                int index = 1;
                for (CostBean costBean : operatorCostBeanList) {
                    costBean.setIndex(index);
                    String remark = null;
                    switch (costBean.getType()) {
                        case 1:
                            remark = "体验金奖励";
                            break;
                        case 2:
                            remark = "黄金红包成本";
                            break;
                        case 3:
                            remark = "金豆抵扣成本";
                            break;
                        case 4:
                            remark = "回租福利券成本";
                            break;
                        case 5:
                            remark = "商城满减券成本";
                            break;
                    }
                    costBean.setRemark(remark);
                    index++;
                }
            }
            String[] columnNames = {"序号", "操作流水号", "手机号码", "用户姓名", "类型", "该类型运营成本（元）", "操作时间"};
            String[] columns = {"index", "orderNo", "mobile", "name", "remark", "cost", "addTime"};
            String fileName = "线上运营成本详表";
            String title = dateArr[i] + "线上运营成本表";
            ExportExcelController<CostBean> export = new ExportExcelController<CostBean>();
            String[] firstLines = {"", "", "合计", operatorCostBeanList.size() + "人","",operatorCostBeanList.stream().map(CostBean::getCost).reduce(BigDecimal.ZERO,BigDecimal::add)+"元",""};
            export.exportExcelManySheet(workbook,fileName, title, columnNames, columns, operatorCostBeanList, response, ExportExcelUtil.EXCEL_FILE_2003,firstLines,i,i+1 == dateArr.length?true:false);
        }

    }
}
