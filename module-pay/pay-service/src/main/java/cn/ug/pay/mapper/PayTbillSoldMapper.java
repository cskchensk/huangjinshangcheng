package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayTbillSold;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayTbillSoldMapper {
    List<PayTbillSold> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    int insert(PayTbillSold payTbillSold);

    int sumSomedaySoldGram(@Param("memberId")String memberId, @Param("currentDate")String currentDate);

    PayTbillSold selectByOrderNO(@Param("orderNO")String orderNO);
    PayTbillSold selectByTbillNO(@Param("tbillNO")String tbillNO);

    List<PayTbillSold> queryIncome(Map<String, Object> params);

}
