package cn.ug.pay.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayTbillLeasebackIncome implements Serializable {
    private int id;
    private String memberId;
    private String incomeDay;
    //回租未到期总收益（元）
    private BigDecimal leasebackIncomeAmount;
    //回租未到期总收益（克）
    private BigDecimal leasebackIncomeGram;
    //回租未到期总收益（金豆）
    private int leasebackIncomeBeans;
    //回租已到期总收益（元）
    private BigDecimal incomeAmount;
    //回租已到期总收益（克）
    private BigDecimal incomeGram;
    //回租已到期总收益（金豆）
    private int incomeBeans;

    private int index;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getIncomeDay() {
        return incomeDay;
    }

    public void setIncomeDay(String incomeDay) {
        this.incomeDay = incomeDay;
    }

    public BigDecimal getLeasebackIncomeAmount() {
        return leasebackIncomeAmount;
    }

    public void setLeasebackIncomeAmount(BigDecimal leasebackIncomeAmount) {
        this.leasebackIncomeAmount = leasebackIncomeAmount;
    }

    public BigDecimal getLeasebackIncomeGram() {
        return leasebackIncomeGram;
    }

    public void setLeasebackIncomeGram(BigDecimal leasebackIncomeGram) {
        this.leasebackIncomeGram = leasebackIncomeGram;
    }

    public int getLeasebackIncomeBeans() {
        return leasebackIncomeBeans;
    }

    public void setLeasebackIncomeBeans(int leasebackIncomeBeans) {
        this.leasebackIncomeBeans = leasebackIncomeBeans;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public int getIncomeBeans() {
        return incomeBeans;
    }

    public void setIncomeBeans(int incomeBeans) {
        this.incomeBeans = incomeBeans;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
