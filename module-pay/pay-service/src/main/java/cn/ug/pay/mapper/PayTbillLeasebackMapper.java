package cn.ug.pay.mapper;

import cn.ug.pay.mapper.entity.PayTbillAgainLeaseback;
import cn.ug.pay.mapper.entity.PayTbillLeaseback;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface PayTbillLeasebackMapper {
    List<PayTbillLeaseback> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    int insert(PayTbillLeaseback payTbillLeaseback);
    int update(Map<String, Object> params);
    PayTbillLeaseback selectByOrderNO(@Param("orderNO")String orderNO);
    PayTbillLeaseback selectLatestByTbillNO(@Param("tbillNO")String tbillNO);

    List<PayTbillLeaseback> queryBecomeDueBills();

    int countSuccessfulNum(@Param("memberId")String memberId);

    List<PayTbillAgainLeaseback> queryAgainLeaseback(Map<String,Object> map);

    List<PayTbillAgainLeaseback> queryUserAgainLeasebackList(Map<String,Object> map);

    //平台累计回租总克重
    int findPlatformLeasebackTotalGram();
}
