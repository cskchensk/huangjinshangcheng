package cn.ug.pay.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.pay.service.ProductOrderService;
import cn.ug.util.UF;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 产品投资记录定时任务
 * @author kaiwotech
 */
@Component
public class ProductOrderJobs {
    private Log log = LogFactory.getLog(MemberGoldJobs.class);

    @Resource
    private ProductOrderService productOrderService;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    /**
     * 固定等待时间（方法体执行完成后等待指定时间）
     */
//    @Scheduled(fixedDelay = 5000)
    public void fixedDelayJob() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        log.info("fixedDelayJob :" + UF.getFormatDateTime(UF.getDateTime()));
    }

    /**
     * 固定间隔时间（不管方法体有没有执行完。每隔指定时间就会触发方法）
     */
//    @Scheduled(fixedRate = 2000)
    public void fixedRateJob() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        log.info("fixedRateJob:" + UF.getFormatDateTime(UF.getDateTime()));
    }

    /**
     * 过期产品订单处理
     */
    @Scheduled(cron = "0/5 * * * * ?")
    public void expiredProductOrderJob() {
        // 分布式锁，锁到分钟（每分钟只有一个线程执行）
        String key = "ProductOrderJobs:expiredProductOrderJob:" + UF.getFormatDateTime("yyyy-MM-dd HH:mm", UF.getDateTime());
        // 锁定5秒的倍数
        key = key + (UF.getDateTime().getSecond() / 5);
        if(!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
            // 如果没有获取到锁
            return;
        }
        int rows = productOrderService.expiredProductOrder();
        if(rows <= 0) {
            return;
        }
        log.info("开始处理过期产品订单");
        log.info(UF.getFormatDateTime(UF.getDateTime()));
        log.info("过期产品订单处理结束[rows = " + rows + "]");
    }

}
