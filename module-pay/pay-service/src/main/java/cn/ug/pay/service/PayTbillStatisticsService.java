package cn.ug.pay.service;

import cn.ug.pay.bean.TbillRecordBean;
import cn.ug.pay.mapper.entity.PayTbillStatistics;

import java.util.List;

public interface PayTbillStatisticsService {
    List<PayTbillStatistics> query(int menu, String mobile, String name, int startGram, int endGram, String order, String sort, int offset, int size);
    int count(int menu, String mobile, String name, int startGram, int endGram);

    List<TbillRecordBean> queryForRecords(String memberId, int menu, String startDate, String endDate, int status, String order, String sort, int offset, int size);
    int countForRecords(String memberId, int menu, String startDate, String endDate, int status);

    List<PayTbillStatistics> queryIncomeForList(int offset, int size);

    PayTbillStatistics selectByMemberId(String memberId);
}
