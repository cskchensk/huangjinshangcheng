package cn.ug.feign;

import cn.ug.account.api.DataDictionaryServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACCOUNT-SERVICE")
public interface DataDictionaryService extends DataDictionaryServiceApi {
}
