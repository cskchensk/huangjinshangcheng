package cn.ug.feign;

import cn.ug.product.api.PriceServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("PRODUCT-SERVICE")
public interface PriceService extends PriceServiceApi {
}
