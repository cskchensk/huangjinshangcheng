package cn.ug.feign;

import cn.ug.mall.api.KdServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface KdService extends KdServiceApi{
}
