package cn.ug.feign;

import cn.ug.mall.api.OrderServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface OrderService extends OrderServiceApi {
}
