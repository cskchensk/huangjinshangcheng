package cn.ug.feign;

import cn.ug.mall.api.ProcessTemplateApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface ProcessTemplateService extends ProcessTemplateApi {
}
