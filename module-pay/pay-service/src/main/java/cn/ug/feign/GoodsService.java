package cn.ug.feign;

import cn.ug.mall.api.GoodsServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface GoodsService extends GoodsServiceApi {
}
