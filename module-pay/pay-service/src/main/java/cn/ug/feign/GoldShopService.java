package cn.ug.feign;

import cn.ug.activity.api.GoldShopServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("ACTIVITY-SERVICE")
public interface GoldShopService extends GoldShopServiceApi {
}
