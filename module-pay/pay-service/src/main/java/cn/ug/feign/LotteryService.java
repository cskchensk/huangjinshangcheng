package cn.ug.feign;

import cn.ug.operation.api.LotteryServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("OPERATION-SERVICE")
public interface LotteryService extends LotteryServiceApi {
}
