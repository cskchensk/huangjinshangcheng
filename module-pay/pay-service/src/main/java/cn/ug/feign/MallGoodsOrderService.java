package cn.ug.feign;

import cn.ug.mall.api.MallGoodsOrderServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface MallGoodsOrderService extends MallGoodsOrderServiceApi {
}
