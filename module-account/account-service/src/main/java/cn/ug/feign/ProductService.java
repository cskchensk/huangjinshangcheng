package cn.ug.feign;

import cn.ug.product.api.ProductServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @Author zhangweijie
 * @Date 2019/9/10 0010
 * @time 下午 18:21
 **/
@FeignClient(name="PRODUCT-SERVICE")
public interface ProductService extends ProductServiceApi {
}
