package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Area;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 行政地域编码表
 * @author kaiwotech
 */
@Component
public interface AreaMapper extends BaseMapper<Area> {

}