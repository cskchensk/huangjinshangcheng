package cn.ug.account.service.impl;

import cn.ug.account.bean.DataDictionaryBean;
import cn.ug.account.mapper.DataDictionaryMapper;
import cn.ug.account.mapper.entity.DataDictionary;
import cn.ug.account.service.DataDictionaryService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.Common;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * 字典
 * @author ywl
 * @date 2018/1/10
 */
@Service
public class DataDictionaryServiceImpl  extends BaseServiceImpl implements DataDictionaryService{

    @Resource
    private DataDictionaryMapper dataDictionaryMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    @RemoveCache(cleanSearch = true)
    public int save(DataDictionaryBean entityBean) {
        if(null == entityBean || StringUtils.isBlank(entityBean.getClassification())) {
            return 1;
        }else if (StringUtils.isBlank(entityBean.getItemName())){
            return 2;
        }else if (StringUtils.isBlank(entityBean.getItemValue())){
            return 3;
        }else if(!Common.isNumeric(entityBean.getType() + "")){
            return 4;
        }else if (!Common.isNumeric(entityBean.getStatus()+"")){
            return 5;
        }
        DataDictionary dataDictionary = dozerBeanMapper.map(entityBean, DataDictionary.class);
        dataDictionary.setId(UF.getRandomUUID());
        dataDictionaryMapper.insert(dataDictionary);
        return 0;
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public int update(String id, DataDictionaryBean entityBean) {
        DataDictionary entity = dataDictionaryMapper.findById(entityBean.getId());
        dozerBeanMapper.map(entityBean, entity);
        dataDictionaryMapper.update(entity);
        return 0;
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public int delete(String id) {
        if(StringUtils.isBlank(id)) {
            return 0;
        }

        return dataDictionaryMapper.delete(id);
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public int deleteByIds(String[] id){
        if(id == null || id.length<=0){
            return 0;
        }

        return dataDictionaryMapper.deleteByIds(id);
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public int removeByIds(String[] id) {
        if(id == null || id.length<=0){
            return 0;
        }
        return dataDictionaryMapper.updateByPrimaryKeySelective(
                getParams()
                        .put("id", id)
                        .put("deleted", DeleteStatus.YES)
                        .toMap()
        );
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public int updateStatus(String[] id, Integer status) {
        if(id == null || id.length<=0){
            return 0;
        }

        return dataDictionaryMapper.updateByPrimaryKeySelective(
                getParams()
                        .put("id", id)
                        .put("status", status)
                        .toMap()
        );
    }

    @Override
    @SaveCache(cacheType = OBJECT)
    public DataDictionaryBean findById(String id) {
        if(StringUtils.isBlank(id)) {
            return null;
        }

        DataDictionary entity = dataDictionaryMapper.findById(id);
        if(null == entity) {
            return null;
        }

        DataDictionaryBean entityBean = dozerBeanMapper.map(entity, DataDictionaryBean.class);
        entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
        entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
        return entityBean;
    }

    @Override
    @SaveCache(cacheType = SEARCH)
    public DataDictionaryBean queryDataDictionary(String classification, String itemValue) {
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("classification",classification);
        param.put("itemValue",itemValue);
        DataDictionary dataDictionary = dataDictionaryMapper.queryDataDictionary(param);
        DataDictionaryBean dataDictionaryBean = dozerBeanMapper.map(dataDictionary, DataDictionaryBean.class);
        return dataDictionaryBean;
    }

    @Override
    @SaveCache(cacheType = SEARCH)
    public DataTable<DataDictionaryBean> query(String order, String sort, int pageNum, int pageSize, String classification, String itemName, Integer type, Integer status, String keyword) {
        Page<DataDictionaryBean> page = PageHelper.startPage(pageNum,pageSize);
        List<DataDictionaryBean> dataList = query(order, sort, classification, itemName, type, status, keyword);
        return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), dataList);
    }

    /**
     * 获取数据列表
     * @param order			    排序字段
     * @param sort			    排序方式 desc或asc
     * @param classification	分类
     * @param itemName			选项名
     * @param type		        类型	 0：全部 1：系统 2：自定义
     * @param status		    状态	 0：全部 1：正常 2：禁用
     * @param keyword		    关键字
     * @return				    列表
     */
    private List<DataDictionaryBean> query(String order, String sort, String classification, String itemName, Integer type, Integer status, String keyword){
        List<DataDictionary> list = dataDictionaryMapper.query(
                getParams(keyword, order, sort)
                        .put("classification", classification)
                        .put("itemName", itemName)
                        .put("type", type)
                        .put("status", status)
                        .toMap());
        return toBean(list);
    }

    private List<DataDictionaryBean> toBean(List<DataDictionary> list) {
        List<DataDictionaryBean> dataList = new ArrayList<>();
        if(null == list || list.isEmpty()) {
            return dataList;
        }
        for (DataDictionary o : list) {
            DataDictionaryBean objBean = dozerBeanMapper.map(o, DataDictionaryBean.class);
            objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
            objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
            dataList.add(objBean);
        }
        return dataList;
    }
}
