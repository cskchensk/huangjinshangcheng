package cn.ug.account.mapper;

import cn.ug.account.bean.VersionBean;
import cn.ug.account.bean.VersionParamBean;
import cn.ug.account.mapper.entity.Version;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface VersionMapper extends BaseMapper<Version> {

    /**
     * 查询列表
     * @param versionParamBean
     * @return
     */
    List<VersionBean> findList(VersionParamBean versionParamBean);

    /**
     *
     * @return
     */
    Version find(Integer type);

    /**
     * 获取所有未删除的版本号
     * @return
     */
    List<String> findAllVersions();

    int updatePublished(Version version);
}
