package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.PersonnelRoleMapping;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 人员和角色映射表
 * @author kaiwotech
 */
@Component
public interface PersonnelRoleMappingMapper extends BaseMapper<PersonnelRoleMapping> {

    /**
     * 批量添加数据
     * @param list	    映射关系数组
     * @return			记录数
     */
    int insertBatch(@Param(value = "list") List<PersonnelRoleMapping> list);

    /**
     * 根据人员ID删除
     * @param personnelId	人员ID
     * @return
     */
    int deleteByPersonnelId(@Param(value = "personnelId") String personnelId);

    /**
     * 根据角色ID删除
     * @param roleId	部门ID
     * @return
     */
    int deleteByRoleId(@Param(value = "roleId") String roleId);

}