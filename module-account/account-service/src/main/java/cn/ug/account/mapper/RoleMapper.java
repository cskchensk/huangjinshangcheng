package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Role;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 权限
 * @author kaiwotech
 */
@Component
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 获取人员的角色
     * @param personnelId	人员ID
     * @return
     */
    List<Role> findListByPersonnel(@Param(value = "personnelId") String personnelId);

}