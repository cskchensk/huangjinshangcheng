package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 人员
 * @author kaiwotech
 */
public class Personnel extends BaseEntity implements java.io.Serializable {

	/** 编号 */
	private String code;
	/** 登录名 */
	private String loginName;
	/** 密码 */
	private String password;
	/** 姓名 */
	private String name;
	/** '邮箱' */
	private String email;
	/** 手机 */
	private String mobile;
	/** 身份证 */
	private String idCard;
	/** 性别1：男 2：女 3：未设置 */
	private Integer gender;
	/** 生日 */
	private String birthday;
	/** '地址' */
	private String address;
	/** '降序排列' */
	private Integer sort;
	/** 状态	 1：正常 2：被锁定 */
	private Integer status;
	/** 备注 */
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
