package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Personnel;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 人员管理
 * @author kaiwotech
 */
@Component
public interface PersonnelMapper extends BaseMapper<Personnel> {
	
}