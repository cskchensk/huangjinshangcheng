package cn.ug.account.service.impl;

import cn.ug.account.bean.VersionBean;
import cn.ug.account.bean.VersionParamBean;
import cn.ug.account.mapper.VersionMapper;
import cn.ug.account.mapper.entity.Version;
import cn.ug.account.service.VersionService;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VersionServiceImpl implements VersionService {

    @Resource
    private VersionMapper versionMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<VersionBean> findList(VersionParamBean versionParamBean) {
        com.github.pagehelper.Page<VersionParamBean> pages = PageHelper.startPage(versionParamBean.getPageNum(),versionParamBean.getPageSize());
        List<VersionBean> dataList = versionMapper.findList(versionParamBean);
        return new DataTable<>(versionParamBean.getPageNum(), versionParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public void save(VersionBean entityBean) {
        //验证参数
        Ensure.that(entityBean.getNumber()).isNull("11000601");
        Ensure.that(entityBean.getUrl()).isNull("11000602");
        Ensure.that(entityBean.getContent()).isNull("11000603");
        Ensure.that(entityBean.getType()).isNull("11000604");
        Ensure.that(entityBean.getForceUpdate()).isNull("11000605");

        LoginBean loginBean = LoginHelper.getLoginBean();
        entityBean.setId(UF.getRandomUUID());
        entityBean.setCreateUserId(loginBean.getId());  //设置创建用户id
        entityBean.setCreateUser(loginBean.getName());  //设置创建用户姓名
        Version version = dozerBeanMapper.map(entityBean, Version.class);
        version.setId(UF.getRandomUUID());
        versionMapper.insert(version);
    }

    @Override
    public void delete(String id) {
        versionMapper.delete(id);
    }

    @Override
    public VersionBean find(Integer type) {
        Ensure.that(type).isNull("11000604");
        Version version = versionMapper.find(type);
        if(version != null){
            VersionBean versionBean = dozerBeanMapper.map(version, VersionBean.class);
            return versionBean;
        }
        return null;
    }

    /**
     * 获取所有未删除的版本号
     *
     * @return
     */
    @Override
    public List<String> findAllVersions() {
        return versionMapper.findAllVersions();
    }

    /**
     * 设置是否发布
     *
     * @param version
     */
    @Override
    public SerializeObject updatePublished(Version version) {
        Ensure.that(version == null).isTrue("00000002");
        Ensure.that(version.getId()).isNull("11000606");
        Ensure.that(version.getPublished()).isNull("11000607");
        Ensure.that(version.getPublished() != 1 && version.getPublished() != 0).isTrue("11000608");
        int num = versionMapper.updatePublished(version);
        Ensure.that(num).isLt(1, "00000005");
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }
}
