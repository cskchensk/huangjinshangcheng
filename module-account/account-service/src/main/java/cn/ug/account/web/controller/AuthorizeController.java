package cn.ug.account.web.controller;

import cn.ug.account.bean.AuthorizeBean;
import cn.ug.account.bean.status.AccountStatus;
import cn.ug.account.service.AuthorizeService;
import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import com.netflix.ribbon.proxy.annotation.Http;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 权限相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("authorize")
public class AuthorizeController extends BaseController {

    @Resource
    private AuthorizeService authorizeService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@RequestHeader String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        AuthorizeBean entity = authorizeService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @param supLayer		    父节点
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:authorize:update")
    @RequestMapping(method = POST)
    public SerializeObject update(@RequestHeader String accessToken, AuthorizeBean entity, String supLayer) {
        if(null == entity || StringUtils.isBlank(entity.getName())) {
            return new SerializeObjectError("11000101");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = authorizeService.save(entity, supLayer);
        } else {
            val = authorizeService.update(entity.getId(), entity);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:authorize:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = authorizeService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:authorize:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = authorizeService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 启用权限信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:authorize:enable")
    @RequestMapping(value = "enable", method = PUT)
    public SerializeObject enable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        int rows = authorizeService.updateStatus(id, AccountStatus.UNLOCK);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 禁用权限信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:authorize:disable")
    @RequestMapping(value = "disable", method = PUT)
    public SerializeObject disable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        int rows = authorizeService.updateStatus(id, AccountStatus.LOCK);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequiresPermissions("sys:authorize")
    @RequestMapping(value = "list" , method = GET)
    public SerializeObject<List<AuthorizeBean>> list(@RequestHeader String accessToken, Order order, Integer status, String keyword) {
        keyword = UF.toString(keyword);
        List<AuthorizeBean> list = authorizeService.findList(order.getOrder(), order.getSort(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequiresPermissions("sys:authorize")
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<AuthorizeBean>> query(@RequestHeader String accessToken, Order order, Page page, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        keyword = UF.toString(keyword);

        DataTable<AuthorizeBean> dataTable = authorizeService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequiresPermissions("sys:authorize:verify")
    @RequestMapping(value = "verify",method = GET)
    public SerializeObject verify(@RequestHeader String accessToken, AuthorizeBean entity) {
        if(authorizeService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据层级获取权限
     * @param accessToken	    登录成功后分配的Key
     * @param layer		        层级
     * @return			        分页数据
     */
    @RequestMapping(value = "findListByLayer" , method = GET)
    public SerializeObject<List<AuthorizeBean>> findListByLayer(@RequestHeader String accessToken, String layer) {
        List<AuthorizeBean> list = authorizeService.findListByLayer(layer);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 根据角色ID获取权限信息
     * @param roleId	        角色ID
     * @return			        分页数据
     */
    @RequestMapping(value = "findListByRole" , method = GET)
    public SerializeObject<List<AuthorizeBean>> findListByRole(String roleId) {
        List<AuthorizeBean> list = authorizeService.findListByRole(roleId);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 根据用户ID获取权限信息
     * @param personnelId	    用户ID
     * @return			        列表数据
     */
    @RequestMapping(value = "findListByPersonnel" , method = GET)
    public SerializeObject<List<AuthorizeBean>> findListByPersonnel(String personnelId) {
        List<AuthorizeBean> list = authorizeService.findListByPersonnel(personnelId);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 获取所有权限（生成PID）
     * @return			        列表数据
     */
    @RequestMapping(value = "findAllListWithPid" , method = GET)
    public SerializeObject<List<AuthorizeBean>> findAllListWithPid() {
        List<AuthorizeBean> list = new ArrayList<>();
        DataTable<AuthorizeBean> dataTable = authorizeService.query(null, null, 0, Integer.MAX_VALUE, 1, null);
        if(null == dataTable || dataTable.getDataList().isEmpty()) {
            return new SerializeObject<>(ResultType.NORMAL, list);
        }
        list = dataTable.getDataList();
        Map<String, String> idLayerMap = new HashMap<>();
        for (AuthorizeBean o : list) {
            idLayerMap.put(o.getLayer(), o.getId());
        }
        for (AuthorizeBean o : list) {
            if(StringUtils.isBlank(o.getLayer())) {
                continue;
            }
            if(o.getLayer().length() == 3) {
                o.setLayer("0");
                continue;
            }
            String pLayer = o.getLayer().substring(0, o.getLayer().length() - 3);
            o.setLayer(idLayerMap.get(pLayer));
        }

        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
