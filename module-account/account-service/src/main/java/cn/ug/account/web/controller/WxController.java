package cn.ug.account.web.controller;

import cn.ug.account.utils.WxService;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.util.RedisConstantUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 微信类
 * @author  ywl
 * @date 2018-05-10
 */
@RestController
@RequestMapping("wx")
public class WxController {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    /**
     * 分享
     * @return
     */
    @RequestMapping(value = "share", method = GET)
    public SerializeObject<Map<String,Object>> share(String url) {
       /* String key = RedisConstantUtil.WX_ACCESSTOKEN_KEY;
        String accesstoken = redisTemplate.opsForValue().get(key);
        *//**accesstoken缓存查询**//*
        if (StringUtils.isBlank(accesstoken)) {
            accesstoken = WxService.getAccessToken();
            redisTemplate.opsForValue().set(key, accesstoken.replace(" ", ""), 7200, TimeUnit.SECONDS);
        }*/

        Map<String,Object> resultMap = WxService.getShareSign(url);
        return new SerializeObject(ResultType.NORMAL, resultMap);
    }
}
