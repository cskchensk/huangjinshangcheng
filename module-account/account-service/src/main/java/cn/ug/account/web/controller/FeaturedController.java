package cn.ug.account.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.account.service.FeaturedService;
import cn.ug.account.web.submit.FeaturedSubmit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author zhaohg
 * @date 2018/08/01.
 */
@RestController
@RequestMapping("/featured")
public class FeaturedController {

    @Autowired
    private FeaturedService featuredService;

    /**
     * 添加推荐位
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/add", method = POST)
    public SerializeObject insert(@RequestHeader String accessToken, FeaturedSubmit submit) {
        return featuredService.insert(submit);
    }

    @RequestMapping(value = "/delete/{id}", method = GET)
    public SerializeObject deleteLabel(@RequestHeader String accessToken, @PathVariable("id") Long id) {
        if (id > 0) {
            return featuredService.deleteById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 查找推荐位
     *
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "/find/{id}", method = GET)
    public SerializeObject find(@RequestHeader String accessToken, @PathVariable("id") Long id) {
        if (id > 0) {
            return featuredService.findById(id);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 更新推荐位
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/update", method = POST)
    public SerializeObject update(@RequestHeader String accessToken, FeaturedSubmit submit) {
        if (submit.getId() > 0) {
            return featuredService.update(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 更新推荐位
     *
     * @param accessToken
     * @param submit
     * @return
     */
    @RequestMapping(value = "/isShow", method = POST)
    public SerializeObject isShow(@RequestHeader String accessToken, FeaturedSubmit submit) {
        if (submit.getId() > 0 && (submit.getIsShow() == 0 || submit.getIsShow() == 1)) {
            return featuredService.isShow(submit);
        }
        return new SerializeObjectError("00000005");
    }

    /**
     * 推荐位 分页
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/list", method = GET)
    public SerializeObject list(@RequestHeader String accessToken, FeaturedSubmit submit) {
        return featuredService.findList(submit);
    }

    /**
     * 推荐位 不分页 移动
     *1首页 2商城
     * @return
     */
    @RequestMapping(value = "/search", method = GET)
    public SerializeObject search(Integer type) {
        return featuredService.search(type);
    }

}
