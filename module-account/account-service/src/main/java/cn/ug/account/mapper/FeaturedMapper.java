package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.FeaturedEntity;
import cn.ug.account.mapper.entity.QuerySubmit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaohg
 * @date 2018/08/01.
 */
@Mapper
public interface FeaturedMapper {

    /**
     * 添加推荐位
     * @param entity
     * @return
     */
    int insert(FeaturedEntity entity);

    /**
     * 更新推荐位
     * @param entity
     * @return
     */
    int update(FeaturedEntity entity);

    /**
     * 删除id
     * @param id
     * @return
     */
    int deleteById(long id);

    /**
     * 查找推荐位
     * @param id
     * @return
     */
    FeaturedEntity findById(Long id);

    /**
     * 推荐位行数
     * @param submit
     * @return
     */
    int count(QuerySubmit submit);

    /**
     * 推荐位list
     * @return
     */
    List<FeaturedEntity> findList(QuerySubmit submit);

    /**
     * 推荐位list
     * @return
     */

    List<FeaturedEntity> searchList(@Param("type")Integer type);

    /**
     * 显示隐藏推荐位
     * @param id
     * @param isShow
     * @return
     */
    int isShow(@Param("id") Long id, @Param("isShow") Integer isShow);

    /**
     * 根据显示位置统计显示广告位的数量
     * @param showPlace
     * @return
     */
    int countByShowPlace(@Param("showPlace") Integer showPlace);
}
