package cn.ug.account.service;


import cn.ug.account.bean.RoleBean;
import cn.ug.bean.base.DataTable;

import java.util.List;
import java.util.Set;

/**
 * 角色管理
 * @author kaiwotech
 */
public interface RoleService {
	
	/**
	 * 添加
	 * @param entityBean	实体
	 * @param authorizeIds	权限ID集合
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(RoleBean entityBean, String[] authorizeIds);
	
	/**
	 * 编辑
	 * @param id			ID
	 * @param entityBean	实体
	 * @param authorizeIds	权限ID集合
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int update(String id, RoleBean entityBean, String[] authorizeIds);
	
	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
     * 判断数据是否存在
     * @param entityBean	数据对象
     * @param id			例外ID
     * @return				True 是 | False 否
     */
    boolean exists(RoleBean entityBean, String id);
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	RoleBean findById(String id);

	/**
	 * 获取列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param status		状态	 0：全部 1：正常 2：被锁定
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	List<RoleBean> findList(String order, String sort, Integer status,String keyword);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param status		状态	 0：全部 1：正常 2：被锁定
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<RoleBean> query(String order, String sort, int pageNum, int pageSize, Integer status,String keyword);

	/**
	 * 更新状态
	 * @param id			用户ID
	 * @param status		用户状态
	 * @return				操作影响的记录数
	 */
	int updateStatus(String[] id, Integer status);

	/**
	 * 获取人员的角色
	 * @param personnelId	人员ID
	 * @return
	 */
	List<RoleBean> findListByPersonnel(String personnelId);
}
