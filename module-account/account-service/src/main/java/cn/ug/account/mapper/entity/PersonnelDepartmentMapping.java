package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 人员与部门映射表
 * @author kaiwotech
 */
public class PersonnelDepartmentMapping extends BaseEntity implements java.io.Serializable {

	/** 人员ID */
	private String personnelId;
	/** 部门ID */
	private String departmentId;

    public String getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(String personnelId) {
        this.personnelId = personnelId;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }
}
