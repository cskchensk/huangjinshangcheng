package cn.ug.account.service.impl;

import cn.ug.account.bean.PersonnelBean;
import cn.ug.account.mapper.PersonnelDepartmentMappingMapper;
import cn.ug.account.mapper.PersonnelMapper;
import cn.ug.account.mapper.PersonnelRoleMappingMapper;
import cn.ug.account.mapper.entity.Personnel;
import cn.ug.account.mapper.entity.PersonnelDepartmentMapping;
import cn.ug.account.mapper.entity.PersonnelRoleMapping;
import cn.ug.account.service.PersonnelService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class PersonnelServiceImpl extends BaseServiceImpl implements PersonnelService {
	
	@Resource
	private PersonnelMapper personnelMapper;
	@Resource
	private PersonnelRoleMappingMapper personnelRoleMappingMapper;
	@Resource
	private PersonnelDepartmentMappingMapper personnelDepartmentMappingMapper;

	@Resource
	private Config config;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true, cleanService = {RoleServiceImpl.class, DepartmentServiceImpl.class})
	@Transactional
	public int save(PersonnelBean entityBean, String[] departmentIds, String[] roleIds) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getLoginName())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }

		Personnel entity = dozerBeanMapper.map(entityBean, Personnel.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		if(StringUtils.isBlank(entity.getPassword())) {
			entity.setPassword(DigestUtils.md5Hex(config.getDefaultPassword()));
		}
		int rows = personnelMapper.insert(entity);
		Ensure.that(rows).isLt(1, "00000005");

		// 添加关联信息
		if(null != departmentIds && departmentIds.length > 0) {
			List<PersonnelDepartmentMapping> personnelDepartmentMappingList = new ArrayList<>();
			for (String departmentId : departmentIds) {
				PersonnelDepartmentMapping o = new PersonnelDepartmentMapping();
				o.setId(UF.getRandomUUID());
				o.setPersonnelId(entity.getId());
				o.setDepartmentId(departmentId);

				personnelDepartmentMappingList.add(o);
			}
			personnelDepartmentMappingMapper.insertBatch(personnelDepartmentMappingList);
		}
		if(null != roleIds && roleIds.length > 0) {
			List<PersonnelRoleMapping> personnelRoleMappingList = new ArrayList<>();
			for (String roleId : roleIds) {
				PersonnelRoleMapping o = new PersonnelRoleMapping();
				o.setId(UF.getRandomUUID());
				o.setPersonnelId(entity.getId());
				o.setRoleId(roleId);

				personnelRoleMappingList.add(o);
			}
			rows = personnelRoleMappingMapper.insertBatch(personnelRoleMappingList);
			Ensure.that(rows).isLt(1, "00000005");
		}

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0, cleanService = {RoleServiceImpl.class, DepartmentServiceImpl.class})
	@Transactional
	public int update(String id, PersonnelBean entityBean, String[] departmentIds, String[] roleIds) {
        // 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getId()) || StringUtils.isBlank(entityBean.getLoginName())) {
			Ensure.that(true).isTrue("11000401");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }

		Personnel entity = personnelMapper.findById(entityBean.getId());
		dozerBeanMapper.map(entityBean, entity);
		personnelMapper.update(entity);

		// 移除关联信息
		personnelDepartmentMappingMapper.deleteByPersonnelId(entityBean.getId());
		personnelRoleMappingMapper.deleteByPersonnelId(entityBean.getId());

		// 添加关联信息
		if(null != departmentIds && departmentIds.length > 0) {
			List<PersonnelDepartmentMapping> personnelDepartmentMappingList = new ArrayList<>();
			for (String departmentId : departmentIds) {
				PersonnelDepartmentMapping o = new PersonnelDepartmentMapping();
				o.setId(UF.getRandomUUID());
				o.setPersonnelId(entity.getId());
				o.setDepartmentId(departmentId);

				personnelDepartmentMappingList.add(o);
			}
			personnelDepartmentMappingMapper.insertBatch(personnelDepartmentMappingList);
		}
		if(null != roleIds && roleIds.length > 0) {
			List<PersonnelRoleMapping> personnelRoleMappingList = new ArrayList<>();
			for (String roleId : roleIds) {
				PersonnelRoleMapping o = new PersonnelRoleMapping();
				o.setId(UF.getRandomUUID());
				o.setPersonnelId(entity.getId());
				o.setRoleId(roleId);

				personnelRoleMappingList.add(o);
			}
			personnelRoleMappingMapper.insertBatch(personnelRoleMappingList);
		}
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return personnelMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return personnelMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return personnelMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public boolean exists(PersonnelBean entityBean, String id) {
		if(null == entityBean || StringUtils.isBlank(entityBean.getLoginName())) {
			return false;
		}

		int rows = personnelMapper.exists(
				getParams()
						.put("loginName", entityBean.getLoginName())
						.put("id", id)
						.toMap()
		);
		return rows > 0;
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public PersonnelBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		Personnel entity = personnelMapper.findById(id);
		if(null == entity) {
			return null;
		}

		PersonnelBean entityBean = dozerBeanMapper.map(entity, PersonnelBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<PersonnelBean> findList(String order, String sort, Integer status,String keyword) {
		return query(order, sort, null, null, status, keyword);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<PersonnelBean> query(String order, String sort, int pageNum, int pageSize, Integer status,String keyword){
		Page<PersonnelBean> page = PageHelper.startPage(pageNum, pageSize);
		List<PersonnelBean> list = query(order, sort, null, null, status, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public PersonnelBean findByLoginName(String loginName) {
		if(StringUtils.isBlank(loginName)) {
			return null;
		}

		List<PersonnelBean> list = query(null, null, loginName, null, 0, null);
		if(null == list || list.isEmpty()) {
			return null;
		}

		return list.get(0);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int resetPassword(String[] id, String newPassword) {
		if(StringUtils.isBlank(newPassword)) {
			newPassword = config.getDefaultPassword();
		}

		return personnelMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("password", DigestUtils.md5Hex(newPassword))
						.toMap()
		);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int updateStatus(String[] id, Integer status) {
		if(id == null || id.length<=0){
			return 0;
		}

		return personnelMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("status", status)
						.toMap()
		);
	}

	/**
	 * 获取数据列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param loginName		登录名
	 * @param mobile		手机
	 * @param status		状态	 0：全部 1：正常 2：被锁定
	 * @param keyword		关键字
	 * @return				列表
	 */
	private List<PersonnelBean> query(String order, String sort, String loginName, String mobile, Integer status,String keyword){
		List<PersonnelBean> dataList = new ArrayList<>();
		List<Personnel> list = personnelMapper.query(
				getParams(keyword, order, sort)
						.put("loginName", loginName)
						.put("mobile", mobile)
						.put("status", status)
						.toMap());
		for (Personnel o : list) {
			PersonnelBean objBean = dozerBeanMapper.map(o, PersonnelBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}
}

