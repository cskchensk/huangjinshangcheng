package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.DataDictionary;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 字典
 * @author ywl
 * @date 2018/1/10
 */
@Component
public interface DataDictionaryMapper extends BaseMapper<DataDictionary> {

        DataDictionary queryDataDictionary(Map<String,Object> map);
}
