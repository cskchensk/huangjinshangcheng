package cn.ug.account.mapper;

import cn.ug.account.bean.BannerBean;
import cn.ug.account.bean.BannerParamBean;
import cn.ug.account.mapper.entity.Banner;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 首页banner
 * @auther ywl
 * 2018-04-10
 */
@Component
public interface BannerMapper extends BaseMapper<Banner> {

    /**
     * 获取列表--后台
     * @param bannerParamBean
     * @return
     */
    List<BannerBean> findList(BannerParamBean bannerParamBean);

    /**
     * 获取列表--移动端
     * @return
     */
    List<BannerBean> queryBannerList();

    /**
     * 定时任务
     * @return
     */
    List<Banner> findBannerJob();

    /**
     * 更新定时任务
     * @param param
     */
    void updateBannerJob(Map<String,Object> param);

}
