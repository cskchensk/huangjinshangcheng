package cn.ug.account.service.impl;

import cn.ug.account.bean.BannerBean;
import cn.ug.account.bean.BannerParamBean;
import cn.ug.account.mapper.BannerMapper;
import cn.ug.account.mapper.entity.Banner;
import cn.ug.account.service.BannerService;
import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BannerServiceImpl implements BannerService {

    @Resource
    private BannerMapper bannerMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<BannerBean> findList(BannerParamBean bannerParamBean) {
        com.github.pagehelper.Page<BannerParamBean> pages = PageHelper.startPage(bannerParamBean.getPageNum(),bannerParamBean.getPageSize());
        List<BannerBean> dataList = bannerMapper.findList(bannerParamBean);
        return new DataTable<>(bannerParamBean.getPageNum(), bannerParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public BannerBean findById(String id) {
        Ensure.that(id).isNull("00000002");
        Banner banner = bannerMapper.findById(id);
        if(banner != null){
            BannerBean bannerBean = dozerBeanMapper.map(banner, BannerBean.class);
            bannerBean.setStartTimeString(UF.getFormatDateTime(banner.getStartTime()));
            return bannerBean;
        }
        return null;
    }

    @Override
    public void save(BannerBean bannerBean) {
        Ensure.that(bannerBean.getTitle()).isNull("11000701");
        Ensure.that(bannerBean.getImgUrl()).isNull("11000702");
        Ensure.that(bannerBean.getStatus()).isNull("11000703");

        Banner banner = dozerBeanMapper.map(bannerBean, Banner.class);
        //当不限制时间
        if(StringUtils.isNotBlank(bannerBean.getStartTimeString())){
            banner.setStartTime(UF.getDate(bannerBean.getStartTimeString()));
        }

        if(StringUtils.isBlank(bannerBean.getId())){
           banner.setId(UF.getRandomUUID());
           bannerMapper.insert(banner);
        }else{
            bannerMapper.update(banner);
        }
    }

    @Override
    public void deleted(String id []) {
        bannerMapper.deleteByIds(id);
    }

    @Override
    public void updateStatus(String id, Integer status) {
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        param.put("status",status);
        bannerMapper.updateByPrimaryKeySelective(param);
    }

    @Override
    public void updateSort(String id, Integer sort) {
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("id",id);
        param.put("sort",sort);
        bannerMapper.updateByPrimaryKeySelective(param);
    }

    @Override
    public List<BannerBean> queryBannerList() {
        List<BannerBean> bannerBeanList = bannerMapper.queryBannerList();
        return bannerBeanList;
    }

    @Override
    public void bannerJob() {
        //获取设置时间
        List<Banner> bannerList = bannerMapper.findBannerJob();
        if(bannerList != null && !bannerList.isEmpty()){
            for(Banner banner:bannerList){
                LocalDateTime startTime = banner.getStartTime();
                LocalDateTime currentTime = LocalDateTime.now();
                //设置可显示
                if(currentTime.compareTo(startTime) >=0){
                    //更新状态
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("id", banner.getId());
                    map.put("status", "1");
                    map.put("startTime", null);
                    bannerMapper.updateBannerJob(map);
                }
            }
        }
    }
}
