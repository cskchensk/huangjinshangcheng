package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Log;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 操作日志
 * @author kaiwotech
 */
@Component
public interface LogMapper extends BaseMapper<Log> {

    /**
     * 批量保存
     * @param entityList	实体列表
     * @return			0：操作失败 1：操作成功
     */
    int insertBatch(List<Log> entityList);
	
}