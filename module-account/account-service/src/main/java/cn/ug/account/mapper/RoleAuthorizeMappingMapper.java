package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.RoleAuthorizeMapping;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 角色与权限映射表
 * @author kaiwotech
 */
@Component
public interface RoleAuthorizeMappingMapper extends BaseMapper<RoleAuthorizeMapping> {

    /**
     * 批量添加数据
     * @param list	    映射关系数组
     * @return			记录数
     */
    int insertBatch(@Param(value = "list") List<RoleAuthorizeMapping> list);

    /**
     * 根据角色ID删除
     * @param roleId	角色ID
     * @return
     */
    int deleteByRoleId(@Param(value = "roleId") String roleId);

    /**
     * 根据权限ID删除
     * @param authorizeId	权限ID
     * @return
     */
    int deleteByAuthorizeId(@Param(value = "authorizeId") String authorizeId);

}