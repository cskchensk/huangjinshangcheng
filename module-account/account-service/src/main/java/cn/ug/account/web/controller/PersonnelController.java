package cn.ug.account.web.controller;

import cn.ug.account.bean.PersonnelBean;
import cn.ug.account.bean.status.AccountStatus;
import cn.ug.account.service.PersonnelService;
import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 系统账号相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("personnel")
public class PersonnelController extends BaseController {

    @Resource
    private PersonnelService personnelService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@RequestHeader String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        PersonnelBean entity = personnelService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @param departmentIds		部门ID数组
     * @param roleIds		    角色ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:personnel:update")
    @RequestMapping(method = POST)
    public SerializeObject update(@RequestHeader String accessToken, PersonnelBean entity, String[] departmentIds, String[] roleIds) {
        if(null == entity || StringUtils.isBlank(entity.getLoginName())) {
            return new SerializeObjectError("11000401");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = personnelService.save(entity, departmentIds, roleIds);
        } else {
            val = personnelService.update(entity.getId(), entity, departmentIds, roleIds);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:personnel:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = personnelService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:personnel:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = personnelService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 重置密码
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:personnel:passwordReset")
    @RequestMapping(value = "passwordReset", method = PUT)
    public SerializeObject passwordReset(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = personnelService.resetPassword(id, config.getDefaultPassword());
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 修改当前登录账号密码
     * @param accessToken		登录成功后分配的Key
     * @param password		    原密码
     * @param newPassword       新密码
     * @return				    是否操作成功
     */
    @RequestMapping(value = "updatePassword", method = PUT)
    public SerializeObject updatePassword(@RequestHeader String accessToken, String password, String newPassword) {
        String id = LoginHelper.getLoginId();
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000102");
        }
        if(StringUtils.isBlank(password)) {
            return new SerializeObjectError("11000402");
        }
        if(StringUtils.isBlank(newPassword)) {
            return new SerializeObjectError("11000403");
        }

        PersonnelBean accountBean = personnelService.findById(id);
        password = DigestUtils.md5Hex(password);
        if(!StringUtils.equalsIgnoreCase(password, accountBean.getPassword())) {
            return new SerializeObjectError("11000404");
        }

        String[] ids = {id};
        personnelService.resetPassword(ids, newPassword);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 启用账号信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:personnel:enable")
    @RequestMapping(value = "enable", method = PUT)
    public SerializeObject enable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        int rows = personnelService.updateStatus(id, AccountStatus.UNLOCK);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 禁用账号信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:personnel:disable")
    @RequestMapping(value = "disable", method = PUT)
    public SerializeObject disable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        int rows = personnelService.updateStatus(id, AccountStatus.LOCK);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据登录名查找信息
     * @param loginName	        登录名
     * @return			        记录集
     */
    @RequestMapping(value = "loginName", method = GET)
    public SerializeObject<PersonnelBean> loginName(String loginName) {
        if(StringUtils.isBlank(loginName)) {
            return new SerializeObjectError("00000002");
        }
        PersonnelBean entity = personnelService.findByLoginName(loginName);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list" , method = GET)
    public SerializeObject<List<PersonnelBean>> list(@RequestHeader String accessToken, Order order, Integer status, String keyword) {
        keyword = UF.toString(keyword);
        List<PersonnelBean> list = personnelService.findList(order.getOrder(), order.getSort(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<PersonnelBean>> query(@RequestHeader String accessToken, Order order, Page page, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        keyword = UF.toString(keyword);

        DataTable<PersonnelBean> dataTable = personnelService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequiresPermissions("sys:personnel:verify")
    @RequestMapping(value = "verify",method = GET)
    public SerializeObject verify(@RequestHeader String accessToken, PersonnelBean entity) {
        if(personnelService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }
}
