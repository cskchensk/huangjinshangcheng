package cn.ug.account.service;

import cn.ug.account.bean.VersionBean;
import cn.ug.account.bean.VersionParamBean;
import cn.ug.account.mapper.entity.Version;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;

import java.util.List;

/**
 * 版本管理
 * @author  ywl
 */
public interface VersionService {

    /**
     * 搜索
     * @param versionParamBean	   请求参数
     * @return				            分页数据
     */
    DataTable<VersionBean> findList(VersionParamBean versionParamBean);

    /**
     * 保存
     * @param versionBean
     */
    void save(VersionBean versionBean);

    /**
     * 删除
     * @param id
     */
    void delete(String id);

    /**
     * 保存
     * @return
     */
    VersionBean find(Integer type);

    /**
     * 获取所有未删除的版本号
     * @return
     */
    List<String> findAllVersions();

    /**
     * 设置是否发布
     * @param version
     */
    SerializeObject updatePublished(Version version);

}
