package cn.ug.account.web.controller.job;

import cn.ug.account.service.BannerService;
import cn.ug.config.RedisGlobalLock;
import cn.ug.util.UF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

/**
 * 银行状态设置
 * @author  ywl
 */
@Component
public class BannerJobs implements Serializable{

    /** 业务有效期 */
    private static final long MINUTES_DAY = 4;

    @Resource
    private BannerService bannerService;

    @Resource
    private RedisGlobalLock redisGlobalLock;

    private static Logger logger = LoggerFactory.getLogger(BannerJobs.class);

    /**
     * 银行维护状态处理
     * 每五分钟执行一次
     */
    @Scheduled(cron = "0 */5 * * * ?")
    public void accountfinanceBillJob(){
        String key = "BannerJobs:expiredBannerJob:" + UF.getFormatDateNow();
        if(redisGlobalLock.lock(key, MINUTES_DAY, TimeUnit.MINUTES)) {
            try{
                logger.info("开始执行Banner定时任务");
                logger.info(UF.getFormatDateTime(UF.getDateTime()));
                //执行任务
                bannerService.bannerJob();
            }catch (Exception e){
                throw  e;
            }finally {
                redisGlobalLock.unlock(key);
            }
        }else{
            logger.info("-------没有获取到锁-------");
            return ;
        }

    }

}
