package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Authorize;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 权限
 * @author kaiwotech
 */
@Component
public interface AuthorizeMapper extends BaseMapper<Authorize> {

    /**
     * 根据层级获取权限
     * @param layer		null:返回顶级数据  layer___：获取layer子节点
     * @return
     */
    List<Authorize> findListByLayer(@Param(value = "layer") String layer);

    /**
     * 获取角色下的权限
     * @param roleId	角色ID
     * @return
     */
    List<Authorize> findListByRole(@Param(value = "roleId") String roleId);

    /**
     * 获取人员的权限
     * @param personnelId	人员ID
     * @return
     */
    List<Authorize> findListByPersonnel(@Param(value = "personnelId") String personnelId);

}