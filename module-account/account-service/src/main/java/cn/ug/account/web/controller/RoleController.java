package cn.ug.account.web.controller;

import cn.ug.account.bean.RoleBean;
import cn.ug.account.bean.status.AccountStatus;
import cn.ug.account.service.RoleService;
import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 角色相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("role")
public class RoleController extends BaseController {

    @Resource
    private RoleService roleService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequiresPermissions("sys:role")
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@RequestHeader String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        RoleBean entity = roleService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @param authorizeIds		权限ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:role:update")
    @RequestMapping(method = POST)
    public SerializeObject update(@RequestHeader String accessToken, RoleBean entity, String[] authorizeIds) {
        if(null == entity || StringUtils.isBlank(entity.getName())) {
            return new SerializeObjectError("11000201");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = roleService.save(entity, authorizeIds);
        } else {
            val = roleService.update(entity.getId(), entity, authorizeIds);
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:role:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = roleService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:role:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = roleService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 启用角色信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:role:enable")
    @RequestMapping(value = "enable", method = PUT)
    public SerializeObject enable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        int rows = roleService.updateStatus(id, AccountStatus.UNLOCK);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 禁用角色信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:role:disable")
    @RequestMapping(value = "disable", method = PUT)
    public SerializeObject disable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        int rows = roleService.updateStatus(id, AccountStatus.LOCK);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequiresPermissions("sys:role")
    @RequestMapping(value = "list" , method = GET)
    public SerializeObject<List<RoleBean>> list(@RequestHeader String accessToken, Order order, Integer status, String keyword) {
        keyword = UF.toString(keyword);
        List<RoleBean> list = roleService.findList(order.getOrder(), order.getSort(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequiresPermissions("sys:role")
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<RoleBean>> query(@RequestHeader String accessToken, Order order, Page page, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        keyword = UF.toString(keyword);

        DataTable<RoleBean> dataTable = roleService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequiresPermissions("sys:role")
    @RequestMapping(value = "verify",method = GET)
    public SerializeObject verify(@RequestHeader String accessToken, RoleBean entity) {
        if(roleService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据用户ID获取角色信息
     * @param accessToken	    登录成功后分配的Key
     * @param personnelId	    用户ID
     * @return			        分页数据
     */
    @RequiresPermissions("sys:role")
    @RequestMapping(value = "findListByPersonnel" , method = GET)
    public SerializeObject<List<RoleBean>> findListByPersonnel(@RequestHeader String accessToken, String personnelId) {
        List<RoleBean> list = roleService.findListByPersonnel(personnelId);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
