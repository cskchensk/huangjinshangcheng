package cn.ug.account.web.controller;

import cn.ug.account.bean.DepartmentBean;
import cn.ug.account.service.DepartmentService;
import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 部门相关服务
 * @author kaiwotech
 */
@RestController
@RequestMapping("department")
public class DepartmentController extends BaseController {

    @Resource
    private DepartmentService departmentService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@RequestHeader String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        DepartmentBean entity = departmentService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @param supLayer		    父节点
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:department:update")
    @RequestMapping(method = POST)
    public SerializeObject update(@RequestHeader String accessToken, DepartmentBean entity, String supLayer) {
        if(null == entity || StringUtils.isBlank(entity.getName())) {
            return new SerializeObject<>(ResultType.ERROR, "部门名称不能为空");
        }
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = departmentService.save(entity, supLayer);
        } else {
            val = departmentService.update(entity.getId(), entity);
        }

        if(val == 1) {
            return new SerializeObject<>(ResultType.ERROR, "部门名称不能为空");
        } else if(val == 2) {
            return new SerializeObject<>(ResultType.ERROR, "提交的数据已存在");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:department:delete")
    @RequestMapping(method = DELETE)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = departmentService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequiresPermissions("sys:department:remove")
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }

        int rows = departmentService.removeByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list" , method = GET)
    public SerializeObject<List<DepartmentBean>> list(@RequestHeader String accessToken, Order order, Integer status, String keyword) {
        keyword = UF.toString(keyword);
        List<DepartmentBean> list = departmentService.findList(order.getOrder(), order.getSort(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询数据
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param page		        分页
     * @param status	        状态	 0：全部 1：正常 2：被锁定
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<DepartmentBean>> query(@RequestHeader String accessToken, Order order, Page page, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        keyword = UF.toString(keyword);

        DataTable<DepartmentBean> dataTable = departmentService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 验证是否存在
     * @param accessToken	    登录成功后分配的Key
     * @param entity	        验证参数
     * @return			        是否验证通过
     */
    @RequiresPermissions("sys:department:verify")
    @RequestMapping(value = "verify",method = GET)
    public SerializeObject verify(@RequestHeader String accessToken, DepartmentBean entity) {
        if(departmentService.exists(entity, entity.getId())) {
            return new SerializeObjectError("00000004");
        }
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 根据层级获取部门
     * @param accessToken	    登录成功后分配的Key
     * @param layer		        层级
     * @return			        分页数据
     */
    @RequestMapping(value = "findListByLayer" , method = GET)
    public SerializeObject<List<DepartmentBean>> findListByLayer(@RequestHeader String accessToken, String layer) {
        List<DepartmentBean> list = departmentService.findListByLayer(layer);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 根据用户ID获取部门信息
     * @param accessToken	    登录成功后分配的Key
     * @param personnelId	    用户ID
     * @return			        分页数据
     */
    @RequestMapping(value = "findListByPersonnel" , method = GET)
    public SerializeObject<List<DepartmentBean>> findListByPersonnel(@RequestHeader String accessToken, String personnelId) {
        List<DepartmentBean> list = departmentService.findListByPersonnel(personnelId);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
