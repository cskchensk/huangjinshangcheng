package cn.ug.account.mq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static cn.ug.config.QueueName.QUEUE_SYS_LOG;

/**
 * 消息队列配置
 * @author kaiwotech
 */
@Configuration
public class RabbitConfig {

    /**
     * 操作日志队列
     * @return
     */
    @Bean
    public Queue sysLogQueue() {
        return new Queue(QUEUE_SYS_LOG, true);
    }

}
