package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 角色
 * @author kaiwotech
 */
public class Role extends BaseEntity implements java.io.Serializable {

	/** 名称 */
	private String name;
	/** 状态	 1：启用 2：禁用 */
	private int status;
	/** 备注 */
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
