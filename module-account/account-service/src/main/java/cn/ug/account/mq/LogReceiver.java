package cn.ug.account.mq;

import cn.ug.account.service.LogService;
import cn.ug.bean.LogBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static cn.ug.config.QueueName.QUEUE_SYS_LOG;

/**
 * 操作日志接收
 * @author kaiwotech
 */
@Component
@RabbitListener(queues = QUEUE_SYS_LOG)
public class LogReceiver {
    private Log log = LogFactory.getLog(LogReceiver.class);
    private ObjectMapper mapper = new ObjectMapper();

    @Resource
    private LogService logService;

    @RabbitHandler
    public void process(LogBean o) {
        try {
            log.info("LogReceiver.process");
            log.info(mapper.writeValueAsString(o));
            logService.save(o);
        } catch (Exception e) {
            log.error(e);
        }
    }

}
