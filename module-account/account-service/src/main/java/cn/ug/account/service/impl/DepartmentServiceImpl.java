package cn.ug.account.service.impl;

import cn.ug.account.bean.DepartmentBean;
import cn.ug.account.mapper.DepartmentMapper;
import cn.ug.account.mapper.entity.Department;
import cn.ug.account.service.DepartmentService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.core.ensure.Ensure;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class DepartmentServiceImpl extends BaseServiceImpl implements DepartmentService {
	
	@Resource
	private DepartmentMapper departmentMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(DepartmentBean entityBean, String supLayer) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getCode()) || StringUtils.isBlank(entityBean.getName())) {
			Ensure.that(true).isTrue("11000301");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }
		Department entity = dozerBeanMapper.map(entityBean, Department.class);

		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		// 设置级别
		entity.setLayer(findSelfLayer(supLayer));
		departmentMapper.insert(entity);

        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int update(String id, DepartmentBean entityBean) {
        // 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getId()) || StringUtils.isBlank(entityBean.getCode()) || StringUtils.isBlank(entityBean.getName())) {
			Ensure.that(true).isTrue("11000301");
		}
		if(exists(entityBean, entityBean.getId())){
            // 该数据已存在
        	Ensure.that(true).isTrue("00000004");
        }
		Department entity = departmentMapper.findById(entityBean.getId());
		dozerBeanMapper.map(entityBean, entity);

		departmentMapper.update(entity);
		return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return departmentMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return departmentMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return departmentMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public boolean exists(DepartmentBean entityBean, String id) {
		if(null == entityBean || StringUtils.isBlank(entityBean.getCode())) {
			return false;
		}

		int rows = departmentMapper.exists(
				getParams()
						.put("code", entityBean.getCode())
						.put("id", id)
						.toMap()
		);
		return rows > 0;
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public DepartmentBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		Department entity = departmentMapper.findById(id);
		if(null == entity) {
			return null;
		}

		DepartmentBean entityBean = dozerBeanMapper.map(entity, DepartmentBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<DepartmentBean> findList(String order, String sort, Integer status,String keyword) {
		return query(order, sort, status, keyword);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<DepartmentBean> query(String order, String sort, int pageNum, int pageSize, Integer status,String keyword){
		Page<DepartmentBean> page = PageHelper.startPage(pageNum, pageSize);
		List<DepartmentBean> list = query(order, sort, status, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int updateStatus(String[] id, Integer status) {
		if(id == null || id.length<=0){
			return 0;
		}

		return departmentMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("status", status)
						.toMap()
		);
	}

	/**
	 * 获取数据列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param status		状态	 0：全部 1：正常 2：禁用
	 * @param keyword		关键字
	 * @return				列表
	 */
	private List<DepartmentBean> query(String order, String sort, Integer status,String keyword){
		List<Department> list = departmentMapper.query(
				getParams(keyword, order, sort)
						.put("status", status)
						.toMap());
		return toBean(list);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<DepartmentBean> findListByLayer(String layer) {
		List<Department> list = departmentMapper.findListByLayer(layer);
		return toBean(list);
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public List<DepartmentBean> findListByPersonnel(String personnelId) {
		List<Department> list = departmentMapper.findListByPersonnel(personnelId);
		return toBean(list);
	}

	private List<DepartmentBean> toBean(List<Department> list) {
		List<DepartmentBean> dataList = new ArrayList<>();
		if(null == list || list.isEmpty()) {
			return dataList;
		}
		for (Department o : list) {
			DepartmentBean objBean = dozerBeanMapper.map(o, DepartmentBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}

	/**
	 * 计算新增节点级别
	 * @param supLayer	上级级别
	 * @return			新增节点级别
	 */
	private String findSelfLayer(String supLayer) {
		if(StringUtils.isBlank(supLayer)) {
			supLayer = "";
		}
		List<Department> aList = departmentMapper.findListByLayer(supLayer);
		if(null == aList || aList.isEmpty()) {
			return supLayer + "100";
		}
		// 获取最大的一个节点
		String selfLayer = "";
		for(Department o : aList){
			if(StringUtils.isBlank(selfLayer) || selfLayer.compareTo(o.getLayer()) < 0){
				selfLayer = o.getLayer();
			}
		}
		if(StringUtils.isBlank(selfLayer ) || selfLayer.length() < 3) {
			return supLayer + "100";
		}

		String leftLayer = selfLayer.substring(0, selfLayer.length()-3);
		String maxNodeLayer = selfLayer.substring(selfLayer.length()-3);
		selfLayer = leftLayer + (UF.toInt(maxNodeLayer) + 1);
		return selfLayer;
	}
}

