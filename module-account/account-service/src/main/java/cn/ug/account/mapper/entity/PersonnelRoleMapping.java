package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 人员与角色映射表
 * @author kaiwotech
 */
public class PersonnelRoleMapping extends BaseEntity implements java.io.Serializable {

	/** 人员ID */
	private String personnelId;
	/** 角色ID */
	private String roleId;

    public String getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(String personnelId) {
        this.personnelId = personnelId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
}
