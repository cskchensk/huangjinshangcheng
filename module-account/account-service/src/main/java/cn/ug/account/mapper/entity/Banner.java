package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * 首页banner
 * @author ywl
 * @date 2018/4/10
 */
public class Banner  extends BaseEntity implements java.io.Serializable  {

    /** 标题 **/
    private String title;
    /** banner地址 **/
    private String imgUrl;
    /** 跳转地址 **/
    private String url;
    /** 状态 1:显示 2:不显示 **/
    private Integer status;
    /** 排序 **/
    private Integer sort;
    /** 有效开始时间 **/
    private LocalDateTime startTime;
    /** 有效截至时间 **/
    private LocalDateTime endTime;
    /** 有效开始时间字符串 **/
    private String startTimeString;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.startTimeString = startTimeString;
    }

}
