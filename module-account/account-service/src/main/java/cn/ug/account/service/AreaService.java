package cn.ug.account.service;


import cn.ug.account.bean.AreaBean;

import java.util.List;
import java.util.Map;

/**
 * 行政地域编码表
 * @author kaiwotech
 */
public interface AreaService {
	
	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	AreaBean findById(String id);

	/**
	 * 获取列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param cityCode		城市编码
	 * @param parentId		上级id
	 * @return				分页数据
	 */
	List<AreaBean> findList(String order, String sort, String cityCode, String parentId);

}
