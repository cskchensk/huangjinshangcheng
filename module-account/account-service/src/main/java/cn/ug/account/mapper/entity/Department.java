package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 部门
 * @author kaiwotech
 */
public class Department extends BaseEntity implements java.io.Serializable {

	/** 编号 */
	private String code;
	/** 名称 */
	private String name;
	/** 层级(001：  001001：001002) */
	private String layer;
	/** 备注 */
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLayer() {
		return layer;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
