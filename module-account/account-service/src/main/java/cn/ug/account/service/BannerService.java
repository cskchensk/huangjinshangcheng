package cn.ug.account.service;

import cn.ug.account.bean.BannerBean;
import cn.ug.account.bean.BannerParamBean;
import cn.ug.bean.base.DataTable;

import java.util.List;

public interface BannerService {

    /**
     * 获取列表--后台
     * @param bannerParamBean
     * @return
     */
    DataTable<BannerBean> findList(BannerParamBean bannerParamBean);

    /**
     * 根据id查询--后台
     * @param id
     * @return
     */
    BannerBean findById(String id);

    /**
     * 保存/修改--后台
     * @param bannerBean
     */
    void save(BannerBean bannerBean);

    /**
     * 删除--后台
     * @param id
     */
    void deleted(String id []);

    /**
     *上下架--后台
     * @param id
     * @param status
     */
    void updateStatus(String id, Integer status);

    /**
     * 排序--后台
     * @param id
     * @param sort
     */
    void updateSort(String id, Integer sort);

    /**
     * 查询列表--移动端
     * @return
     */
    List<BannerBean> queryBannerList();

    /**
     * banner定时上线
     */
    void bannerJob();

}
