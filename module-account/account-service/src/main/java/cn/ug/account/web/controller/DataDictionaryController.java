package cn.ug.account.web.controller;

import cn.ug.account.bean.DataDictionaryBean;
import cn.ug.account.bean.status.AccountStatus;
import cn.ug.account.service.DataDictionaryService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.SerializeObjectError;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 字典服务
 * @author ywl
 * @date 2018/1/10 0010
 */
@RestController
@RequestMapping("dataDictionary")
public class DataDictionaryController {

    @Resource
    private DataDictionaryService dataDictionaryService;

    @Resource
    private Config config;

    /**
     * 根据ID查找信息
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        DataDictionaryBean entity = dataDictionaryService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject update(@RequestHeader String accessToken, DataDictionaryBean entity) {
        int val;
        if(StringUtils.isBlank(entity.getId())) {
            val = dataDictionaryService.save(entity);
        } else {
            val = dataDictionaryService.update(entity.getId(), entity);
        }

        if(val == 1) {
            return new SerializeObject<>(ResultType.ERROR, "分类不能为空");
        }else if(val == 2) {
            return new SerializeObject<>(ResultType.ERROR, "项目名称不能为空");
        }else if(val == 3){
            return new SerializeObject<>(ResultType.ERROR, "项目值不能为空");
        }else if(val == 4){
            return new SerializeObject<>(ResultType.ERROR, "类型不能为空");
        }else if(val == 5){
            return new SerializeObject<>(ResultType.ERROR, "状态不能为空");
        }
        return new SerializeObject<>(ResultType.NORMAL, "操作成功");
    }

    /**
     * 删除信息(逻辑删除)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "remove", method = PUT)
    public SerializeObject remove(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObject<>(ResultType.ERROR, "请求参数有误");
        }

        int rows = dataDictionaryService.removeByIds(id);
        return new SerializeObject<>(ResultType.NORMAL, "共删除 " + rows + " 条记录");
    }

    /**
     * 启用字典信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "enable", method = PUT)
    public SerializeObject enable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObject<>(ResultType.ERROR, "请求参数有误");
        }
        int rows = dataDictionaryService.updateStatus(id, AccountStatus.UNLOCK);
        return new SerializeObject<>(ResultType.NORMAL, "共启用 " + rows + " 条记录");
    }

    /**
     * 禁用字典信息
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "disable", method = PUT)
    public SerializeObject disable(@RequestHeader String accessToken, String[] id) {
        if(null == id || id.length <= 0) {
            return new SerializeObject<>(ResultType.ERROR, "请求参数有误");
        }
        int rows = dataDictionaryService.updateStatus(id, AccountStatus.LOCK);
        return new SerializeObject<>(ResultType.NORMAL, "共禁用 " + rows + " 条记录");
    }

    /**
     * 查询列表
     * @param classification	分类
     * @param itemName			选项名
     * @param status		    状态	 0：全部 1：正常 2：禁用
     * @return			        实体
     */
    @RequestMapping(value = "findListByItemName" , method = GET)
    public SerializeObject<DataDictionaryBean> findListByClassification(String classification, String itemName, Integer status) {
        if(null == status || status < 0) {
            status = 0;
        }
        if(StringUtils.isBlank(classification) || StringUtils.isBlank(itemName)) {
            return new SerializeObjectError<>("00000002");
        }
        DataTable<DataDictionaryBean> dataTable = dataDictionaryService.query("sort", "desc", 0, Integer.MAX_VALUE, classification, itemName, null, status, null);
        if(null == dataTable || dataTable.getDataList().isEmpty()) {
            return new SerializeObjectError<>("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, dataTable.getDataList().get(0));
    }

    /**
     * 查询列表
     * @param classification	分类
     * @param status		    状态	 0：全部 1：正常 2：禁用
     * @return			        分页数据
     */
    @RequestMapping(value = "findListByClassification" , method = GET)
    public SerializeObject<List<DataDictionaryBean>> findListByClassification(String classification, Integer status) {
        if(null == status || status < 0) {
            status = 0;
        }
        DataTable<DataDictionaryBean> dataTable = dataDictionaryService.query("sort", "asc", 0, Integer.MAX_VALUE, classification, null, null, status, null);
        return new SerializeObject<>(ResultType.NORMAL, dataTable.getDataList());
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param classification	分类
     * @param itemName			选项名
     * @param type		        类型	 0：全部 1：系统 2：自定义
     * @param status		    状态	 0：全部 1：正常 2：禁用
     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(value = "list" , method = GET)
    public SerializeObject<DataTable<DataDictionaryBean>> list(@RequestHeader String accessToken, Page page, Order order, String classification, String itemName, Integer type, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        if ("产品投资期限".equals(keyword)){
            order.setOrder("sort");
            order.setSort("asc");
        }
        DataTable<DataDictionaryBean> dataTable = dataDictionaryService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), classification, itemName, type, status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询列表
     * @param accessToken	    登录成功后分配的Key
     * @param order		        排序
     * @param classification	分类
     * @param itemName			选项名
     * @param type		        类型	 0：全部 1：系统 2：自定义

     * @param keyword	        关键字
     * @return			        分页数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<DataTable<DataDictionaryBean>> query(@RequestHeader String accessToken, Page page, Order order, String classification, String itemName, Integer type, Integer status, String keyword) {
        if(page.getPageSize() <= 0) {
            page.setPageSize(config.getPageSize());
        }
        if(null == status || status < 0) {
            status = 0;
        }
        DataTable<DataDictionaryBean> dataTable = dataDictionaryService.query(order.getOrder(), order.getSort(), page.getPageNum(), page.getPageSize(), classification, itemName, type, status, keyword);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 获取单个字典数据
     * @param classification
     * @param itemValue
     * @return
     */
    @RequestMapping(value = "queryDataDictionary" , method = GET)
    public SerializeObject<DataDictionaryBean> queryDataDictionary(String classification, String itemValue) {
        DataDictionaryBean dataDictionaryBean = dataDictionaryService.queryDataDictionary(classification,itemValue);
        return new SerializeObject<>(ResultType.NORMAL, dataDictionaryBean);
    }

}
