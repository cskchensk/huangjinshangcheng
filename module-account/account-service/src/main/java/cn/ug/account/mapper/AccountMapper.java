package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Account;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 帐号管理
 * @author kaiwotech
 */
@Component
public interface AccountMapper extends BaseMapper<Account> {
	
}