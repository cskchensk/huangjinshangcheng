package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * 版本管理
 * @author  ywl
 */
public class Version extends BaseEntity implements java.io.Serializable {

    /** 版本号 **/
    private String number;
    /** 版本更新地址  **/
    private String url;
    /** 版本更新内容 **/
    private String content;
    /** 类型 1:android 2:IOS **/
    private Integer type;
    /** 强制更新 1:否 2:是 **/
    private Integer forceUpdate;
    /** 创建用户Id **/
    private String createUserId;
    /** 创建用户 **/
    private String createUser;
    /** 添加时间 **/
    private LocalDateTime addTime;
    /** 更新时间 **/
    private LocalDateTime modifyTime;
    /**是否发布，0：未发布，1：已发布**/
    private Integer published;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Integer forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    @Override
    public LocalDateTime getAddTime() {
        return addTime;
    }

    @Override
    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    @Override
    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    @Override
    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getPublished() {
        return published;
    }

    public void setPublished(Integer published) {
        this.published = published;
    }
}
