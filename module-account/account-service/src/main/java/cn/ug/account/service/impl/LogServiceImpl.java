package cn.ug.account.service.impl;

import cn.ug.account.mapper.LogMapper;
import cn.ug.account.mapper.entity.Log;
import cn.ug.account.service.LogService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.bean.LogBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.status.DeleteStatus;
import cn.ug.core.ensure.Ensure;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

import static cn.ug.config.CacheType.OBJECT;
import static cn.ug.config.CacheType.SEARCH;

/**
 * @author kaiwotech
 */
@Service
public class LogServiceImpl extends BaseServiceImpl implements LogService {
	
	@Resource
	private LogMapper logMapper;

	@Resource
	private DozerBeanMapper dozerBeanMapper;

	@Override
	public int saveBatch(List<LogBean> entityBeanList) {
		if(null == entityBeanList || entityBeanList.isEmpty()) {
			return 0;
		}
		List<Log> entityList = new ArrayList<>();
		for (LogBean o : entityBeanList) {
			entityList.add(dozerBeanMapper.map(o, Log.class));
		}
		return logMapper.insertBatch(entityList);
	}

	@Override
	@RemoveCache(cleanSearch = true)
	public int save(LogBean entityBean) {
		// 数据完整性校验
		if(null == entityBean || StringUtils.isBlank(entityBean.getType())) {
			Ensure.that(true).isTrue("00000002");
		}

		if(entityBean.getDescription().length() > 500) {
			entityBean.setDescription(entityBean.getDescription().substring(0, 500));
		}

		Log entity = dozerBeanMapper.map(entityBean, Log.class);
		if(StringUtils.isBlank(entity.getId())) {
			entity.setId(UF.getRandomUUID());
		}
		int rows = logMapper.insert(entity);
        return 0;
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int delete(String id) {
		if(StringUtils.isBlank(id)) {
			return 0;
		}

		return logMapper.delete(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int deleteByIds(String[] id){
		if(id == null || id.length<=0){
			return 0;
		}

		return logMapper.deleteByIds(id);
	}

	@Override
	@RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
	public int removeByIds(String[] id) {
		if(id == null || id.length<=0){
			return 0;
		}

		return logMapper.updateByPrimaryKeySelective(
				getParams()
						.put("id", id)
						.put("deleted", DeleteStatus.YES)
						.toMap()
		);
	}

	@Override
	@SaveCache(cacheType = OBJECT)
	public LogBean findById(String id) {
		if(StringUtils.isBlank(id)) {
			return null;
		}

		Log entity = logMapper.findById(id);
		if(null == entity) {
			return null;
		}

		LogBean entityBean = dozerBeanMapper.map(entity, LogBean.class);
		entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
		entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
		return entityBean;
	}

	@Override
	@SaveCache(cacheType = SEARCH)
	public DataTable<LogBean> query(String order, String sort, int pageNum, int pageSize, String type, String thirdNumber, String userId, String userType, String userLoginName, String keyword){
		Page<LogBean> page = PageHelper.startPage(pageNum, pageSize);
		List<LogBean> list = query(order, sort, type, thirdNumber, userId, userType, userLoginName, keyword);
		return new DataTable<>(page.getPageNum(), page.getPageSize(), page.getTotal(), list);
	}

	/**
	 * 获取数据列表
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param type			功能
	 * @param thirdNumber	功能流水号
	 * @param userId		用户id
	 * @param userType		用户类型 1：维护账号 2：用户 3：会员
	 * @param userLoginName	用户登录名
	 * @param keyword		关键字
	 * @return				列表
	 */
	private List<LogBean> query(String order, String sort, String type, String thirdNumber, String userId, String userType, String userLoginName, String keyword){
		List<LogBean> dataList = new ArrayList<>();
		List<Log> list = logMapper.query(
				getParams(keyword, order, sort)
						.put("type", type)
						.put("thirdNumber", thirdNumber)
						.put("userId", userId)
						.put("userType", userType)
						.put("userLoginName", userLoginName)
						.toMap());
		for (Log o : list) {
			LogBean objBean = dozerBeanMapper.map(o, LogBean.class);
			objBean.setAddTimeString(UF.getFormatDateTime(o.getAddTime()));
			objBean.setModifyTimeString(UF.getFormatDateTime(o.getModifyTime()));
			dataList.add(objBean);
		}
		return dataList;
	}
}

