package cn.ug.account.service;


import cn.ug.bean.LogBean;
import cn.ug.bean.base.DataTable;

import java.util.List;

/**
 * 操作日志
 * @author kaiwotech
 */
public interface LogService {
	
	/**
	 * 批量添加
	 * @param entityBeanList	实体列表
	 * @return 					操作影响的行数
	 */
	int saveBatch(List<LogBean> entityBeanList);

	/**
	 * 添加
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：不能为空 2：数据已存在
	 */
	int save(LogBean entityBean);

	/**
	 * 根据ID删除
	 * @param id	ID
	 * @return		操作影响的记录数
	 */
	int delete(String id);
	
	/**
	 * 删除对象
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int deleteByIds(String[] id);

	/**
	 * 删除对象(逻辑删除)
	 * @param id	ID数组
	 * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
	 */
	int removeByIds(String[] id);

	/**
	 * 根据ID, 查找对象
	 * @param id	ID
	 * @return		实例
	 */
	LogBean findById(String id);

	/**
	 * 搜索
	 * @param order			排序字段
	 * @param sort			排序方式 desc或asc
	 * @param pageNum		当前页1..N
	 * @param pageSize		每页记录数
	 * @param type			功能
	 * @param thirdNumber	功能流水号
	 * @param userId		用户id
	 * @param userType		用户类型 1：维护账号 2：用户 3：会员
	 * @param userLoginName	用户登录名
	 * @param keyword		关键字
	 * @return				分页数据
	 */
	DataTable<LogBean> query(String order, String sort, int pageNum, int pageSize, String type, String thirdNumber, String userId, String userType, String userLoginName, String keyword);

}
