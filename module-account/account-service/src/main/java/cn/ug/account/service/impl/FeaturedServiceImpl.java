package cn.ug.account.service.impl;


import cn.ug.account.bean.FeaturedBean;
import cn.ug.account.mapper.FeaturedMapper;
import cn.ug.account.mapper.entity.FeaturedEntity;
import cn.ug.account.mapper.entity.QuerySubmit;
import cn.ug.account.service.FeaturedService;
import cn.ug.account.web.submit.FeaturedSubmit;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.enums.ProductTypeEnum;
import cn.ug.feign.ProductService;
import cn.ug.product.bean.response.ProductBean;
import cn.ug.service.impl.BaseServiceImpl;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
@Service
public class FeaturedServiceImpl extends BaseServiceImpl implements FeaturedService {

    @Resource
    private FeaturedMapper featuredMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    private ProductService productService;

    @Override
    public SerializeObject insert(FeaturedSubmit submit) {
        submit.checkAndReduce();

        FeaturedEntity entity = dozerBeanMapper.map(submit, FeaturedEntity.class);
        int success = featuredMapper.insert(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject findById(Long id) {
        FeaturedEntity entity = featuredMapper.findById(id);
        if (entity != null) {
            FeaturedBean bean = dozerBeanMapper.map(entity, FeaturedBean.class);
            return new SerializeObject<>(ResultType.NORMAL, "00000001", bean);
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject update(FeaturedSubmit submit) {
        submit.checkAndReduce();

        FeaturedEntity entity = dozerBeanMapper.map(submit, FeaturedEntity.class);
        int success = featuredMapper.update(entity);
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject isShow(FeaturedSubmit submit) {
        //操作-显示/隐藏 超过3个需要提示“只允许显示3个首页/商城广告位”逻辑判断
        if (submit.getIsShow() == 1) {
            FeaturedEntity featuredEntity = featuredMapper.findById(submit.getId());
            if (featuredEntity == null) {
                return new SerializeObjectError("00000003");
            }

            int rows = featuredMapper.countByShowPlace(featuredEntity.getShowPlace());
            if (rows >= 3) {
                return new SerializeObjectError("11000707");
            }
        }
        int success = featuredMapper.isShow(submit.getId(), submit.getIsShow());
        if (success > 0) {
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject deleteById(long id) {
        FeaturedEntity entity = featuredMapper.findById(id);
        if (entity != null) {
            featuredMapper.deleteById(id);
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject findList(FeaturedSubmit submit) {
        QuerySubmit querySubmit = new QuerySubmit();
        submit.setParams(querySubmit);

        int count = featuredMapper.count(querySubmit);
        Page page = new Page(submit.getPageNum(), submit.getPageSize(), count);
        List<FeaturedBean> list = new ArrayList<>(count);
        if (count > 0) {
            List<FeaturedEntity> featuredList = featuredMapper.findList(querySubmit);
            for (FeaturedEntity entity : featuredList) {
                FeaturedBean bean = dozerBeanMapper.map(entity, FeaturedBean.class);
                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", new DataTable<>(page, list));
    }

    @Override
    public SerializeObject search(Integer type) {
        List<FeaturedBean> list = new ArrayList<>();
        List<FeaturedEntity> featuredList = featuredMapper.searchList(type);
        boolean flag = false;
        String bizId = null;
        SerializeObject<List<ProductBean>> serializeObject = productService.queryOnlineList(ProductTypeEnum.DISCOUNT_GOLD.getType());
        if (null != serializeObject && serializeObject.getCode() == ResultType.NORMAL) {
            if (serializeObject.getData() != null) {
                List<ProductBean> productBeanList = serializeObject.getData();
                if (!CollectionUtils.isEmpty(productBeanList)) {
                    ProductBean productBean = productBeanList.get(0);
                    bizId = productBean.getId();
                    flag = true;
                }
            }
        }

        if (featuredList != null) {
            for (FeaturedEntity entity : featuredList) {
                //不存在在售折扣金  并且有折扣金推荐位
                if (!flag && entity.getStatus() == 2 && entity.getType() == 6) {
                    continue;
                }
                FeaturedBean bean = dozerBeanMapper.map(entity, FeaturedBean.class);
                //存在在售折扣金  并且有折扣金推荐位
                if (entity.getStatus() == 2 && entity.getType() == 6 && flag) {
                    bean.setBizId(bizId);
                }

                list.add(bean);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, "00000001", list);
    }


}
