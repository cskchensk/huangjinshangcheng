package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

/**
 * 角色与权限映射表
 * @author kaiwotech
 */
public class RoleAuthorizeMapping extends BaseEntity implements java.io.Serializable {

	/** 角色ID */
	private String roleId;
	/** 权限ID */
	private String authorizeId;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getAuthorizeId() {
		return authorizeId;
	}

	public void setAuthorizeId(String authorizeId) {
		this.authorizeId = authorizeId;
	}
}
