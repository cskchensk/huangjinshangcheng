package cn.ug.account.web.controller;

import cn.ug.account.bean.VersionBean;
import cn.ug.account.bean.VersionParamBean;
import cn.ug.account.mapper.entity.Version;
import cn.ug.account.service.VersionService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 版本管理
 */
@RestController
@RequestMapping("version")
public class VersionController {

    @Resource
    private VersionService versionService;

    @Resource
    private Config config;

    /**
     * 查询版本列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param versionParamBean       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<VersionBean>> findList(@RequestHeader String accessToken, VersionParamBean versionParamBean) {
        if(versionParamBean.getPageSize() <= 0) {
            versionParamBean.setPageSize(config.getPageSize());
        }
        DataTable<VersionBean> dataTable = versionService.findList(versionParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询未删除的所有版本号
     * @return
     */
    @RequestMapping(value = "findAllVersions", method = GET)
    public SerializeObject<List<String>> findAllVersions () {
        List<String> list = versionService.findAllVersions();
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 新增信息（id为null、''）或修改信息（id不为空）
     * @param accessToken		登录成功后分配的Key
     * @param versionBean		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, VersionBean versionBean) {
        versionService.save(versionBean);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除(后台)
     * @param accessToken		登录成功后分配的Key
     * @return				    是否操作成功
     */
    @RequestMapping(value = "delete/{id}", method = PUT)
    public SerializeObject delete(@RequestHeader String accessToken, @PathVariable String id) {
        versionService.delete(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    @RequestMapping(value = "updatePublished", method = POST)
    public SerializeObject updatePublished(@RequestHeader String accessToken,  Version version) {
        if (version == null || StringUtils.isBlank(version.getId()) || null ==version.getPublished()) {
            return new SerializeObject(ResultType.ERROR, "00000002");
        }
        versionService.updatePublished(version);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查找最新发布的版本-app端
     * @return			    记录集
     */
    @RequestMapping(value = "find", method = GET)
    public SerializeObject find(Integer type) {

        VersionBean entity = versionService.find(type);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObject(ResultType.NORMAL, "00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }
}
