package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.PersonnelDepartmentMapping;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 人员和部门映射表
 * @author kaiwotech
 */
@Component
public interface PersonnelDepartmentMappingMapper extends BaseMapper<PersonnelDepartmentMapping> {

    /**
     * 批量添加数据
     * @param list	    映射关系数组
     * @return			记录数
     */
    int insertBatch(@Param(value = "list") List<PersonnelDepartmentMapping> list);

    /**
     * 根据人员ID删除
     * @param personnelId	人员ID
     * @return
     */
    int deleteByPersonnelId(@Param(value = "personnelId") String personnelId);

    /**
     * 根据部门ID删除
     * @param departmentId	部门ID
     * @return
     */
    int deleteByDepartmentId(@Param(value = "departmentId") String departmentId);

}