package cn.ug.account.web.submit;

import cn.ug.core.ensure.Ensure;
import cn.ug.account.mapper.entity.QuerySubmit;
import cn.ug.util.UF;
import org.apache.commons.lang.StringUtils;

/**
 * @author zhaohg
 * @date 2018/08/01.
 */
public class FeaturedSubmit {

    /**
     * 推荐位id
     */
    private Long id;
    /**
     * 推荐位名称
     */
    private String  name;
    /**
     * 推荐位图片地址
     */
    private String  imgUrl;
    /**
     * h5跳转地址
     */
    private String  h5Url;
    /**
     * 跳转状态 0:不跳转 1:h5跳转 2:原生跳转
     */
    private Integer status;
    /**
     * 原生跳转类型 0, 不跳转 1，2，3
     *
     * 1.认证绑卡  2.登陆注册 3.柚生金30天
     */
    private Integer type;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 0 不显示 1：显示
     */
    private Integer isShow;

    /**
     * 广告位显示位置 1首页 2商城
     */
    private Integer showPlace;

    private Integer pageSize = 10;
    private Integer pageNum = 1;
    private String startTime;
    private String endTime;

    public void checkAndReduce() {
        if (this.status == 0) {
            this.h5Url = "";
            this.type = 0;
        }
        if (this.status == 1) {
            this.type = 0;
        }
        if (status == 2) {
            this.h5Url = "";
        }

        Ensure.that(StringUtils.isEmpty(name)).isTrue("11000705");
        Ensure.that(StringUtils.isEmpty(imgUrl)).isTrue("11000706");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getH5Url() {
        return h5Url;
    }

    public void setH5Url(String h5Url) {
        this.h5Url = h5Url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getShowPlace() {
        return showPlace;
    }

    public void setShowPlace(Integer showPlace) {
        this.showPlace = showPlace;
    }

    public void setParams(QuerySubmit querySubmit) {
        if (this.pageNum < 1) {
            this.pageNum = 1;
        }
        if (this.pageSize < 10) {
            this.pageSize = 10;
        }

        querySubmit.setLimit(this.pageNum, this.pageSize);
        querySubmit.put("name", name);

        if (org.apache.commons.lang3.StringUtils.isNotEmpty(startTime)) {
            querySubmit.put("startTime", UF.getDate(startTime));
        }
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(endTime)) {
            querySubmit.put("endTime", UF.getDate(endTime));
        }
    }
}
