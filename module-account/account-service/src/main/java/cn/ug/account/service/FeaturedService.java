package cn.ug.account.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.account.web.submit.FeaturedSubmit;

/**
 * 推荐位
 * @author zhaohg
 * @date 2018/08/01.
 */
public interface FeaturedService {

    /**
     * 添加推荐位
     *
     * @param submit
     * @return
     */
    SerializeObject insert(FeaturedSubmit submit);

    /**
     * 更新推荐位
     *
     * @param submit
     * @return
     */
    SerializeObject update(FeaturedSubmit submit);

    /**
     * 更新推荐位
     *
     * @param submit
     * @return
     */
    SerializeObject isShow(FeaturedSubmit submit);


    /**
     * 删除推荐位
     * @param id
     * @return
     */
    SerializeObject deleteById(long id);

    /**
     * 查找推荐位
     * @param id
     * @return
     */
    SerializeObject findById(Long id);

    /**
     * 推荐位列表
     *
     * @return
     */
    SerializeObject findList(FeaturedSubmit submit);

    /**
     * 不分页
     * @return
     */
    SerializeObject search(Integer type);
}
