package cn.ug.account.service;

import cn.ug.account.bean.DataDictionaryBean;
import cn.ug.bean.base.DataTable;

/**
 * 字典
 * @author ywl
 * @date 2018/1/10 0010
 */
public interface DataDictionaryService {

    /**
     * 添加
     * @param entityBean	实体
     * @return 				0:操作成功 1：不能为空 2：数据已存在
     */
    int save(DataDictionaryBean entityBean);

    /**
     * 添加
     * @param id			ID
     * @param entityBean	实体
     * @return 				0:操作成功 1：不能为空 2：数据已存在
     */
    int update(String id, DataDictionaryBean entityBean);

    /**
     * 根据ID删除
     * @param id	ID
     * @return		操作影响的记录数
     */
    int delete(String id);

    /**
     * 删除对象
     * @param id	ID数组
     * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
     */
    int deleteByIds(String[] id);

    /**
     * 删除对象(逻辑删除)
     * @param id	ID数组
     * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
     */
    int removeByIds(String[] id);

    /**
     * 更新状态
     * @param id			用户ID
     * @param status		用户状态
     * @return				操作影响的记录数
     */
    int updateStatus(String[] id, Integer status);

    /**
     * 搜索
     * @param order		        排序字段
     * @param sort			    排序方式 desc或asc
     * @param pageNum		    当前页1..N
     * @param pageSize	        每页记录数
     * @param classification	分类
     * @param itemName			选项名
     * @param type		        类型	 0：全部 1：系统 2：自定义
     * @param status		    状态	 0：全部 1：正常 2：禁用
     * @param keyword		    关键字
     * @return				    分页数据
     */
    DataTable<DataDictionaryBean> query(String order, String sort, int pageNum, int pageSize, String classification, String itemName, Integer type, Integer status, String keyword);

    /**
     * 根据ID, 查找对象
     * @param id	ID
     * @return		实例
     */
    DataDictionaryBean findById(String id);

    /**
     * 根据
     * @param classification
     * @param itemValue
     * @return
     */
    DataDictionaryBean queryDataDictionary( String classification, String itemValue);
}
