package cn.ug.account.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 * 数据字典
 * @author ywl
 * @date 2018/1/10
 */
public class DataDictionary  extends BaseEntity implements Serializable{

     /** 编号 **/
     private String id;
     /** 分类 **/
     private String classification;
     /** 项目名称 **/
     private String itemName;
     /** 项目值 **/
     private String itemValue;
     /** 降序排列 **/
     private Integer sort;
     /** 类型：1：系统 2：自定义 **/
     private Integer type;
     /** 状态：1：启用 2：不启用 **/
     private Integer status;
     /** 备注 **/
     private String description;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
