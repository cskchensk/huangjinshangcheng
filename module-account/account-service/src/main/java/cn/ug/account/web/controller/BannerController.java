package cn.ug.account.web.controller;

import cn.ug.account.bean.BannerBean;
import cn.ug.account.bean.BannerParamBean;
import cn.ug.account.service.BannerService;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.io.Serializable;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("banner")
public class BannerController {

    @Resource
    private BannerService bannerService;

    @Resource
    private Config config;

    /**
     * 查询Banner列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param bannerParamBean       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<DataTable<BannerBean>> findList(@RequestHeader String accessToken, BannerParamBean bannerParamBean) {
        if(bannerParamBean.getPageSize() <= 0) {
            bannerParamBean.setPageSize(config.getPageSize());
        }
        DataTable<BannerBean> dataTable = bannerService.findList(bannerParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询详情
     * @param accessToken
     * @param id
     * @return
     */
    @GetMapping(value = "findById/{id}")
    public SerializeObject<BannerBean> findById(@RequestHeader String accessToken, @PathVariable String id){
        BannerBean bannerBean = bannerService.findById(id);
        return new SerializeObject<>(ResultType.NORMAL, bannerBean);
    }

    /**
     * 保存和修改--后台
     * @param accessToken
     * @param bannerBean
     * @return
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken,BannerBean bannerBean){
        bannerService.save(bannerBean);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除--后台
     * @param accessToken
     * @param id
     * @return
     */
    @RequestMapping(value = "deleted", method = PUT)
    public SerializeObject deleted(@RequestHeader String accessToken,String id []){
        bannerService.deleted(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 上架/下架(后台)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @param status         上架状态   1:是 2:否
     * @return				    是否操作成功
     */
    @RequestMapping(value = "updateStatus", method = PUT)
    public SerializeObject updateStatus(@RequestHeader String accessToken, String id, Integer status) {
        bannerService.updateStatus(id, status);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 设置排序(后台)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @param sort         上架状态   1:是 2:否
     * @return				    是否操作成功
     */
    @RequestMapping(value = "updateSort", method = PUT)
    public SerializeObject updateSort(@RequestHeader String accessToken, String id, Integer sort) {
        bannerService.updateSort(id, sort);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询Banner列表(移动端)
     * @return			                分页数据
     */
    @RequestMapping(value = "queryBannerList" , method = GET)
    public SerializeObject<List<BannerBean>> queryBannerList() {
        List<BannerBean> list = bannerService.queryBannerList();
        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
