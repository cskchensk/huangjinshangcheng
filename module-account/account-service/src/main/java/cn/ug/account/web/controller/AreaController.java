package cn.ug.account.web.controller;

import cn.ug.account.bean.AreaBean;
import cn.ug.account.service.AreaService;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 行政地域编码表
 * @author kaiwotech
 */
@RestController
@RequestMapping("area")
public class AreaController extends BaseController {

    @Resource
    private AreaService areaService;

    /**
     * 根据ID查找信息
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    public SerializeObject find(@PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        AreaBean entity = areaService.findById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 查询列表
     * @param order			排序
     * @param cityCode		城市编码
     * @param parentId		上级id
     * @return			    列表数据
     */
    @RequestMapping(method = GET)
    public SerializeObject<List<AreaBean>> list(Order order, String cityCode, String parentId) {
        List<AreaBean> list = areaService.findList(order.getOrder(), order.getSort(), cityCode, parentId);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

}
