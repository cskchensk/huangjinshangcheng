package cn.ug.account.mapper;

import cn.ug.account.mapper.entity.Department;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 部门
 * @author kaiwotech
 */
@Component
public interface DepartmentMapper extends BaseMapper<Department> {

    /**
     * 根据层级获取权限
     * @param layer		null:返回顶级数据  layer___：获取layer子节点
     * @return
     */
    List<Department> findListByLayer(@Param(value = "layer") String layer);

    /**
     * 获取人员的权限
     * @param personnelId	人员ID
     * @return
     */
    List<Department> findListByPersonnel(@Param(value = "personnelId") String personnelId);

}