package cn.ug.account.mapper.entity;

import cn.ug.util.UF;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zhaohg
 * @date 2018/07/09.
 */
public class QuerySubmit extends HashMap<String, Object> {

    private static final long   serialVersionUID = 1L;
    private static final String LIMIT_START      = "start";
    private static final String LIMIT_END        = "end";

    public QuerySubmit(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public QuerySubmit(int initialCapacity) {
        super(initialCapacity);
    }

    public QuerySubmit() {
        super(5);
        super.put(LIMIT_START, 0);
        super.put(LIMIT_END, 10);
    }

    public QuerySubmit(Map<? extends String, ?> map) {
        super(map);
    }


    public int getLimitStart() {
        return UF.toInteger(super.get(LIMIT_START), 0);
    }

    public void setLimitStart(int start) {
        super.put(LIMIT_START, start);
    }

    public int getLimitEnd() {
        return UF.toInteger(super.get(LIMIT_END), 10);
    }

    public void setLimitEnd(int end) {
        super.put(LIMIT_END, end);
    }

    /**
     * Default value is {pageNum = 1,  pageSize = 10}
     *
     * @param pageNum
     * @param pageSize
     */
    public void setLimit(final Integer pageNum, final Integer pageSize) {
        int _pageNum = 1;
        int _pageSize = 20;

        if (pageNum != null && pageNum > 0) {
            _pageNum = pageNum;
        }

        if (pageSize != null && pageSize > 0) {
            _pageSize = pageSize;
        }

        super.put(LIMIT_START, (_pageNum - 1) * _pageSize);
        super.put(LIMIT_END, _pageSize);
    }


}