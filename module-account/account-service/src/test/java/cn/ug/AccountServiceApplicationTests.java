package cn.ug;

import cn.ug.account.bean.AuthorizeBean;
import cn.ug.account.service.AuthorizeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceApplicationTests {


	@Resource
	private AuthorizeService authorizeService;

	@Test
	public void contextLoads() {

		List<AuthorizeBean> list = authorizeService.findListByRole("GU7UFn3gtrnZEN7PW825BD");
		System.out.println(list.size());
		for (AuthorizeBean o : list) {
			System.out.println(o.getName());
		}

	}

}
