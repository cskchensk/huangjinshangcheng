package cn.ug;

import cn.ug.account.service.impl.AuthorizeServiceImpl;
import cn.ug.account.service.impl.RoleServiceImpl;
import cn.ug.bean.LogBean;
import cn.ug.config.CacheService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static cn.ug.config.QueueName.QUEUE_SYS_LOG;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogServiceApplicationTests {

	@Resource
	private AmqpTemplate amqpTemplate;

	@Autowired
	private CacheService cacheService;

	@Test
	public void contextLoads() {
		/*LogBean logBean = new LogBean();
		logBean.setType("setType");
		logBean.setDescription("setDescription");
		amqpTemplate.convertAndSend(QUEUE_SYS_LOG, logBean);*/

		Class<?>[] cleanService = {RoleServiceImpl.class, AuthorizeServiceImpl.class};

		// 清除其他关联类
		cacheService.cleanCache(cleanService);




	}

}
