package cn.ug.account.bean;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class BannerParamBean  extends Page implements Serializable {

    /** 标题 **/
    private String title;
    /**  起始时间**/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
