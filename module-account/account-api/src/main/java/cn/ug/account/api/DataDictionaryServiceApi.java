package cn.ug.account.api;

import cn.ug.account.bean.DataDictionaryBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * 系统字典相关服务
 * @author ywl
 */
@RequestMapping("dataDictionary")
public interface DataDictionaryServiceApi {

    /**
     * 根据ID查找信息
     * @param classification		 产品类型
     * @param itemValue            产品值
     * @return
     */
    @RequestMapping(value = "queryDataDictionary", method = GET)
    SerializeObject<DataDictionaryBean> queryDataDictionary(@RequestParam("classification") String classification, @RequestParam("itemValue") String itemValue);

    /**
     * 查询列表
     * @param classification	分类
     * @param status		    状态	 0：全部 1：正常 2：禁用
     * @return
     */
    @RequestMapping(value = "findListByClassification" , method = GET)
    SerializeObject<List<DataDictionaryBean>> findListByClassification(@RequestParam("classification")String classification, @RequestParam("status")Integer status);
}
