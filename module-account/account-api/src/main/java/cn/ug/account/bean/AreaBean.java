package cn.ug.account.bean;


import cn.ug.bean.base.BaseBean;

import java.math.BigDecimal;

/**
 * 行政地域编码表
 * @author kaiwotech
 */
public class AreaBean extends BaseBean implements java.io.Serializable {

	/** 城市代码 */
	private String cityCode;
	/** 城市名称 */
	private String name;
	/** 城市名称简拼 */
	private String enName;
	/** 城市乡村划分（0-省级，1-城市，2-乡村） */
	private Integer division;
	/** 上级id */
	private String parentId;
	/** 经度 */
	private BigDecimal longitude;
	/** 纬度 */
	private BigDecimal latitude;
	/** 城市分级（1-省，2-市，3-县） */
	private int level;

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public Integer getDivision() {
		return division;
	}

	public void setDivision(Integer division) {
		this.division = division;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
