package cn.ug.account.bean;

import cn.ug.bean.base.BaseBean;

/**
 * 字典
 * @author ywl
 * @date 2018/1/10 0010
 */
public class DataDictionaryBean extends BaseBean implements java.io.Serializable {

    /** 分类 **/
    private String classification;
    /** 项目名称 **/
    private String itemName;
    /** 项目值 **/
    private String itemValue;
    /** 降序排列 **/
    private Integer sort;
    /** 类型：1：系统 2：自定义 **/
    private Integer type;
    /** 状态：1：启用 2：不启用 **/
    private Integer status;
    /** 备注 **/
    private String description;

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemValue() {
        return itemValue;
    }

    public void setItemValue(String itemValue) {
        this.itemValue = itemValue;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
