package cn.ug.account.bean;

import cn.ug.bean.base.Page;

import java.io.Serializable;


public class VersionParamBean extends Page implements Serializable {

    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
