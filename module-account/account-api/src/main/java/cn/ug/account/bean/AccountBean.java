package cn.ug.account.bean;


import cn.ug.bean.base.BaseBean;

/**
 * 系统账号相关服务
 * @author kaiwotech
 */
public class AccountBean extends BaseBean implements java.io.Serializable {

	/** 登录名 */
	private String loginName;
	/** 密码 */
	private String password;
	/** 姓名 */
	private String name;
	/** 手机 */
	private String mobile;
	/** 状态	 1：正常 2：被锁定 */
	private Integer status;
	/** 备注 */
	private String description;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
