package cn.ug.account.api;

import cn.ug.account.bean.AreaBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 行政地域编码表
 * @author kaiwotech
 */
@RequestMapping("area")
public interface AreaServiceApi {

    /**
     * 根据ID查找信息
     * @param id		    ID数组
     * @return			    记录集
     */
    @RequestMapping(value = "{id}", method = GET)
    SerializeObject<AreaBean> find(@PathVariable("id") String id);

    /**
     * 查询数据
     * @param order		    排序字段
     * @param sort		    排序方式 desc或asc
     * @param cityCode		城市编码
     * @param parentId		上级id
     * @return			    分页数据
     */
    @RequestMapping(method = GET)
    SerializeObject<DataTable<AreaBean>> query(
            @RequestParam("order") String order,
            @RequestParam("sort") String sort,
            @RequestParam("cityCode") String cityCode,
            @RequestParam("parentId") String parentId);


}
