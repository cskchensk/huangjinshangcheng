package cn.ug.account.bean;

import cn.ug.bean.base.BaseBean;

public class BannerBean extends BaseBean implements java.io.Serializable  {

    /** 标题 **/
    private String title;
    /** banner地址 **/
    private String imgUrl;
    /** 跳转地址 **/
    private String url;
    /** 状态 1:显示 2:不显示 **/
    private Integer status;
    /** 排序 **/
    private Integer sort;
    /** 有效开始时间字符串 **/
    private String startTimeString;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getStartTimeString() {
        return startTimeString;
    }

    public void setStartTimeString(String startTimeString) {
        this.startTimeString = startTimeString;
    }


}
