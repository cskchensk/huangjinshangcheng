package cn.ug.account.bean;


import cn.ug.bean.base.BaseBean;

/**
 * 权限
 * @author kaiwotech
 */
public class AuthorizeBean extends BaseBean implements java.io.Serializable {

	/** 名称 */
	private String name;
	/** 层级(001：  001001：001002) */
	private String layer;
	/** 动作值 */
	private String actionValue;
	/** 排序 */
	private int sort;
	/** 状态	 1：启用 2：禁用 */
	private int status;
	/** 备注 */
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLayer() {
		return layer;
	}

	public void setLayer(String layer) {
		this.layer = layer;
	}

	public String getActionValue() {
		return actionValue;
	}

	public void setActionValue(String actionValue) {
		this.actionValue = actionValue;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
