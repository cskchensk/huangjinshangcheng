package cn.ug.account.api;

import cn.ug.account.bean.AuthorizeBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 权限相关服务
 * @author kaiwotech
 */
@RequestMapping("authorize")
public interface AuthorizeServiceApi {

    /**
     * 根据用户ID获取权限信息
     * @param personnelId	    用户ID
     * @return			        列表数据
     */
    @RequestMapping(value = "findListByPersonnel" , method = GET)
    SerializeObject<List<AuthorizeBean>> findListByPersonnel(
            @RequestParam("personnelId") String personnelId);

    /**
     * 获取所有权限（生成PID）
     * @return			        列表数据
     */
    @RequestMapping(value = "findAllListWithPid" , method = GET)
    SerializeObject<List<AuthorizeBean>> findAllListWithPid();

}
