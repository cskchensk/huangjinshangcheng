package cn.ug.account.bean.status;

import cn.ug.bean.status.BaseStatus;

/**
 * 状态类型
 * @author kaiwotech
 */
public class AccountStatus extends BaseStatus {
    /** 正常(未锁定) */
    public static final int UNLOCK 	= 1;
    /** 锁定 */
    public static final int LOCK	= 2;
}
