package cn.ug.account.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author zhaohg
 * @date 2018/08/01.
 */
public class FeaturedBean {

    /**
     * 推荐位id
     */
    private Long    id;
    /**
     * 跳转状态 0:不跳转 1:h5跳转 2:原生跳转
     */
    private Integer status;
    /**
     * 推荐位名称
     */
    private String  name;
    /**
     * 推荐位图片地址
     */
    private String  imgUrl;
    /**
     * h5跳转地址
     */
    private String  h5Url;
    /**
     * 原生跳转类型 1，2，3，4，5，6，7
     */
    private Integer  type;

    private Integer isShow;
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date    addTime;

    /**
     * 广告位显示位置 1首页 2商城
     */
    private Integer showPlace;

    //业务Id type为7的时候传值产品id
    private String bizId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getH5Url() {
        return h5Url;
    }

    public void setH5Url(String h5Url) {
        this.h5Url = h5Url;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getShowPlace() {
        return showPlace;
    }

    public void setShowPlace(Integer showPlace) {
        this.showPlace = showPlace;
    }

    public String getBizId() {
        return bizId;
    }

    public void setBizId(String bizId) {
        this.bizId = bizId;
    }
}
