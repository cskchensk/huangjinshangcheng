package cn.ug.account.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 系统版本相关服务
 * @author kaiwotech
 */
@RequestMapping("version")
public interface VersionServiceApi {

    /**
     * 获取所有未删除的版本号
     * @return
     */
    @RequestMapping(value = "findAllVersions", method = GET)
    SerializeObject<List<String>> findAllVersions();

}
