package cn.ug.operation.bean;

import java.io.Serializable;

public class GuessMemberStatisticsRemarkBean implements Serializable {
    private int index;
    private String memberId;
    private String mobile;
    private String name;
    private String bindedCard;
    private String gotGold;
    private String couponNum;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBindedCard() {
        return bindedCard;
    }

    public void setBindedCard(String bindedCard) {
        this.bindedCard = bindedCard;
    }

    public String getGotGold() {
        return gotGold;
    }

    public void setGotGold(String gotGold) {
        this.gotGold = gotGold;
    }

    public String getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(String couponNum) {
        this.couponNum = couponNum;
    }
}
