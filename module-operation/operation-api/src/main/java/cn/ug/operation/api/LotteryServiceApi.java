package cn.ug.operation.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.operation.bean.LotteryBean;
import cn.ug.operation.bean.LotteryJoin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/lottery")
public interface LotteryServiceApi {

    /**
     * 根据用户id，查询用户参加的活动
     * @param memberId
     * @return
     */
    @GetMapping("/findJoinLottery")
    public SerializeObject<LotteryJoin> findJoinLotteryByMember(@RequestParam("memberId")String memberId);

    @GetMapping("/findLottery")
    public SerializeObject<LotteryBean> findLottery(@RequestParam("lotteryId")int lotteryId);

    @GetMapping("/countLotteryNumToday")
    public SerializeObject countLotteryNumToday(@RequestParam("memberId")String memberId, @RequestParam("lotteryId")int lotteryId);

    @PostMapping("/addLotteryTimes")
    public SerializeObject addLotteryTimes (@RequestParam("memberId")String memberId, @RequestParam("lotteryId")int lotteryId);
}
