package cn.ug.operation.bean;

import java.io.Serializable;

public class GuessMemberRemarkBean implements Serializable {
    private int index;
    private String mobile;
    private String name;
    private String guessTime;
    private double guessPrice;
    private double finalPrice;
    private String guessResult;
    private String shareStatus;
    private String gotGold;
   private String couponAmount;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGuessTime() {
        return guessTime;
    }

    public void setGuessTime(String guessTime) {
        this.guessTime = guessTime;
    }

    public double getGuessPrice() {
        return guessPrice;
    }

    public void setGuessPrice(double guessPrice) {
        this.guessPrice = guessPrice;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getGuessResult() {
        return guessResult;
    }

    public void setGuessResult(String guessResult) {
        this.guessResult = guessResult;
    }

    public String getShareStatus() {
        return shareStatus;
    }

    public void setShareStatus(String shareStatus) {
        this.shareStatus = shareStatus;
    }

    public String getGotGold() {
        return gotGold;
    }

    public void setGotGold(String gotGold) {
        this.gotGold = gotGold;
    }

    public String getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(String couponAmount) {
        this.couponAmount = couponAmount;
    }
}
