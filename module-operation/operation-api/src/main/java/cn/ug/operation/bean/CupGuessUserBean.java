package cn.ug.operation.bean;

import java.io.Serializable;

public class CupGuessUserBean implements Serializable {
    private String id;
    private String mobile;
    private String username;
    private String code;
    private int type;
    private int guessResult;
    private int finalResult;
    private int guessStatus;
    private String guessDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGuessResult() {
        return guessResult;
    }

    public void setGuessResult(int guessResult) {
        this.guessResult = guessResult;
    }

    public int getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(int finalResult) {
        this.finalResult = finalResult;
    }

    public int getGuessStatus() {
        return guessStatus;
    }

    public void setGuessStatus(int guessStatus) {
        this.guessStatus = guessStatus;
    }

    public String getGuessDate() {
        return guessDate;
    }

    public void setGuessDate(String guessDate) {
        this.guessDate = guessDate;
    }
}
