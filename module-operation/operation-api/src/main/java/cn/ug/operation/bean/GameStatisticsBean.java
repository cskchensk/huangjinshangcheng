package cn.ug.operation.bean;

import java.io.Serializable;

public class GameStatisticsBean implements Serializable {
    private int totalGoals;
    private int totalGuessNum;
    private int groupGoals;
    private int groupGuessNum;

    public int getTotalGoals() {
        return totalGoals;
    }

    public void setTotalGoals(int totalGoals) {
        this.totalGoals = totalGoals;
    }

    public int getTotalGuessNum() {
        return totalGuessNum;
    }

    public void setTotalGuessNum(int totalGuessNum) {
        this.totalGuessNum = totalGuessNum;
    }

    public int getGroupGoals() {
        return groupGoals;
    }

    public void setGroupGoals(int groupGoals) {
        this.groupGoals = groupGoals;
    }

    public int getGroupGuessNum() {
        return groupGuessNum;
    }

    public void setGroupGuessNum(int groupGuessNum) {
        this.groupGuessNum = groupGuessNum;
    }
}
