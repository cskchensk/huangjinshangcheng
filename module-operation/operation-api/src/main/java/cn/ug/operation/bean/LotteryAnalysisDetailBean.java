package cn.ug.operation.bean;

import java.io.Serializable;

public class LotteryAnalysisDetailBean implements Serializable {
    private String drawDate;
    private int joinNum;
    private int drawNum;
    private int prizeNum;
    private int receiveNum;
    private int index;
    private int newAddNum;
    public String getDrawDate() {
        return drawDate;
    }

    public void setDrawDate(String drawDate) {
        this.drawDate = drawDate;
    }

    public int getJoinNum() {
        return joinNum;
    }

    public void setJoinNum(int joinNum) {
        this.joinNum = joinNum;
    }

    public int getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(int drawNum) {
        this.drawNum = drawNum;
    }

    public int getPrizeNum() {
        return prizeNum;
    }

    public void setPrizeNum(int prizeNum) {
        this.prizeNum = prizeNum;
    }

    public int getReceiveNum() {
        return receiveNum;
    }

    public void setReceiveNum(int receiveNum) {
        this.receiveNum = receiveNum;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getNewAddNum() {
        return newAddNum;
    }

    public void setNewAddNum(int newAddNum) {
        this.newAddNum = newAddNum;
    }
}
