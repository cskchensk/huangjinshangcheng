package cn.ug.operation.bean;

import java.io.Serializable;
import java.util.Date;

public class GameBean implements Serializable {
    private String id;
    private String gameDate;
    private String gameTime;
    private String teamaId;
    private String teamaName;
    private String teamaIcon;
    private String teambId;
    private String teambName;
    private String teambIcon;
    private String successTeamId;
    private int type;
    private String datetime;
    private String gameResult;
    /**0：未参与竞猜；1：A队胜；2：平局；3：B队胜**/
    private int guessResult;
    /**0：未参与竞猜；1：竞猜成功，等待结果；2：恭喜您，猜中；3：猜错了**/
    private int guessStatus;
    /** 0:未开始；1：进行中；2：已结束**/
    private int gameStatus;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGameDate() {
        return gameDate;
    }

    public void setGameDate(String gameDate) {
        this.gameDate = gameDate;
    }

    public String getGameTime() {
        return gameTime;
    }

    public void setGameTime(String gameTime) {
        this.gameTime = gameTime;
    }

    public String getTeamaId() {
        return teamaId;
    }

    public void setTeamaId(String teamaId) {
        this.teamaId = teamaId;
    }

    public String getTeamaName() {
        return teamaName;
    }

    public void setTeamaName(String teamaName) {
        this.teamaName = teamaName;
    }

    public String getTeamaIcon() {
        return teamaIcon;
    }

    public void setTeamaIcon(String teamaIcon) {
        this.teamaIcon = teamaIcon;
    }

    public String getTeambId() {
        return teambId;
    }

    public void setTeambId(String teambId) {
        this.teambId = teambId;
    }

    public String getTeambName() {
        return teambName;
    }

    public void setTeambName(String teambName) {
        this.teambName = teambName;
    }

    public String getTeambIcon() {
        return teambIcon;
    }

    public void setTeambIcon(String teambIcon) {
        this.teambIcon = teambIcon;
    }

    public int getGuessResult() {
        return guessResult;
    }

    public void setGuessResult(int guessResult) {
        this.guessResult = guessResult;
    }

    public String getSuccessTeamId() {
        return successTeamId;
    }

    public void setSuccessTeamId(String successTeamId) {
        this.successTeamId = successTeamId;
    }

    public String getGameResult() {
        return gameResult;
    }

    public void setGameResult(String gameResult) {
        this.gameResult = gameResult;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGuessStatus() {
        return guessStatus;
    }

    public void setGuessStatus(int guessStatus) {
        this.guessStatus = guessStatus;
    }

    public int getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(int gameStatus) {
        this.gameStatus = gameStatus;
    }
}
