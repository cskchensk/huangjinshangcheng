package cn.ug.operation.bean;

import java.io.Serializable;

public class EverydayPrizeBean implements Serializable {
    private int id;
    private String prizeName;
    private int consumeNum;
    private int totalNum;
    private int getPrizeNum;
    private int receiveNum;
    private String remark;
    private int index;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public int getConsumeNum() {
        return consumeNum;
    }

    public void setConsumeNum(int consumeNum) {
        this.consumeNum = consumeNum;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getGetPrizeNum() {
        return getPrizeNum;
    }

    public void setGetPrizeNum(int getPrizeNum) {
        this.getPrizeNum = getPrizeNum;
    }

    public int getReceiveNum() {
        return receiveNum;
    }

    public void setReceiveNum(int receiveNum) {
        this.receiveNum = receiveNum;
    }
}
