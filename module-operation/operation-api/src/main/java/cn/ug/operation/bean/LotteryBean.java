package cn.ug.operation.bean;

import java.io.Serializable;

public class LotteryBean implements Serializable {
    private int id;
    private String name;
    private String title;
    private String startTime;
    private String endTime;
    private String rules;
    private Integer accumulateNum;
    private Integer defaultNum;
    private Integer everydayNum;
    private Integer everydayMaxNum;
    private Integer grantingMethod;
    private Integer shareRewards;
    private int shareNum;
    private String shareTitle;
    private String shareImg;
    private String shareRemark;
    private int status;
    private String addTime;
    private String publishTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Integer getAccumulateNum() {
        return accumulateNum;
    }

    public void setAccumulateNum(Integer accumulateNum) {
        this.accumulateNum = accumulateNum;
    }

    public Integer getDefaultNum() {
        return defaultNum;
    }

    public void setDefaultNum(Integer defaultNum) {
        this.defaultNum = defaultNum;
    }

    public Integer getEverydayNum() {
        return everydayNum;
    }

    public void setEverydayNum(Integer everydayNum) {
        this.everydayNum = everydayNum;
    }

    public Integer getEverydayMaxNum() {
        return everydayMaxNum;
    }

    public void setEverydayMaxNum(Integer everydayMaxNum) {
        this.everydayMaxNum = everydayMaxNum;
    }

    public Integer getGrantingMethod() {
        return grantingMethod;
    }

    public void setGrantingMethod(Integer grantingMethod) {
        this.grantingMethod = grantingMethod;
    }

    public Integer getShareRewards() {
        return shareRewards;
    }

    public void setShareRewards(Integer shareRewards) {
        this.shareRewards = shareRewards;
    }

    public int getShareNum() {
        return shareNum;
    }

    public void setShareNum(int shareNum) {
        this.shareNum = shareNum;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareImg() {
        return shareImg;
    }

    public void setShareImg(String shareImg) {
        this.shareImg = shareImg;
    }

    public String getShareRemark() {
        return shareRemark;
    }

    public void setShareRemark(String shareRemark) {
        this.shareRemark = shareRemark;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }
}
