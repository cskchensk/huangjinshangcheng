package cn.ug.operation.bean;

import java.io.Serializable;

public class MemberPrizeBean implements Serializable {
    private int id;
    private String lotteryName;
    private String addTime;
    private String prizeName;
    private int status;
    private int rewardsStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRewardsStatus() {
        return rewardsStatus;
    }

    public void setRewardsStatus(int rewardsStatus) {
        this.rewardsStatus = rewardsStatus;
    }
}
