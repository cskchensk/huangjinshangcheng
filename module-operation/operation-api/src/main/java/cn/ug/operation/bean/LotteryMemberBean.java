package cn.ug.operation.bean;

import java.io.Serializable;

public class LotteryMemberBean implements Serializable {
    private int id;
    private String lotteryName;
    private int lotteryStatus;
    private String cellphone;
    private String memeberName;
    private String joinTime;
    private String prizeName;
    private String getPrizeTime;
    private int prizeStatus;
    private String receivePrizeTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName;
    }

    public int getLotteryStatus() {
        return lotteryStatus;
    }

    public void setLotteryStatus(int lotteryStatus) {
        this.lotteryStatus = lotteryStatus;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getMemeberName() {
        return memeberName;
    }

    public void setMemeberName(String memeberName) {
        this.memeberName = memeberName;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getGetPrizeTime() {
        return getPrizeTime;
    }

    public void setGetPrizeTime(String getPrizeTime) {
        this.getPrizeTime = getPrizeTime;
    }

    public int getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(int prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public String getReceivePrizeTime() {
        return receivePrizeTime;
    }

    public void setReceivePrizeTime(String receivePrizeTime) {
        this.receivePrizeTime = receivePrizeTime;
    }
}
