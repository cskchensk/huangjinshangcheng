package cn.ug.operation.bean;

import java.util.List;

public class SharePrizeSettingBean  extends ShareUserTypeSetting{

    private List<SharePrizeBean>  sharePrizeBeans;

    public List<SharePrizeBean> getSharePrizeBeans() {
        return sharePrizeBeans;
    }

    public void setSharePrizeBeans(List<SharePrizeBean> sharePrizeBeans) {
        this.sharePrizeBeans = sharePrizeBeans;
    }
}
