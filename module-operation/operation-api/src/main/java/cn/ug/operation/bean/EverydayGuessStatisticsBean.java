package cn.ug.operation.bean;

import java.io.Serializable;

public class EverydayGuessStatisticsBean implements Serializable {
    private String guessDate;
    private int joinNum;
    private int gotNum;
    private double gotGold;
    private int failedNum;
    private int couponNum;
    private int shareNum;

    public int getShareNum() {
        return shareNum;
    }

    public void setShareNum(int shareNum) {
        this.shareNum = shareNum;
    }

    public String getGuessDate() {
        return guessDate;
    }

    public void setGuessDate(String guessDate) {
        this.guessDate = guessDate;
    }

    public int getJoinNum() {
        return joinNum;
    }

    public void setJoinNum(int joinNum) {
        this.joinNum = joinNum;
    }

    public int getGotNum() {
        return gotNum;
    }

    public void setGotNum(int gotNum) {
        this.gotNum = gotNum;
    }

    public double getGotGold() {
        return gotGold;
    }

    public void setGotGold(double gotGold) {
        this.gotGold = gotGold;
    }

    public int getFailedNum() {
        return failedNum;
    }

    public void setFailedNum(int failedNum) {
        this.failedNum = failedNum;
    }

    public int getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(int couponNum) {
        this.couponNum = couponNum;
    }
}
