package cn.ug.operation.bean;

import java.io.Serializable;

public class CupJoinUserBean implements Serializable {
    private String id;
    private String joinDate;
    private String mobile;
    private String username;
    private int inviteNum;
    private int balance;
    private int gotNum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getInviteNum() {
        return inviteNum;
    }

    public void setInviteNum(int inviteNum) {
        this.inviteNum = inviteNum;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getGotNum() {
        return gotNum;
    }

    public void setGotNum(int gotNum) {
        this.gotNum = gotNum;
    }
}
