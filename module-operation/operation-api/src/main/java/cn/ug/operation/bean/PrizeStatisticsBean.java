package cn.ug.operation.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class PrizeStatisticsBean implements Serializable{
    private int prizeId;
    private String prizeName;
    private int type;
    private String prizeImg;
    private int consumeNum;
    private int totalNum;
    private int gotNum;
    private int onRoadNum;
    private int packNum;
    private BigDecimal probability;

    public int getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(int prizeId) {
        this.prizeId = prizeId;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPrizeImg() {
        return prizeImg;
    }

    public void setPrizeImg(String prizeImg) {
        this.prizeImg = prizeImg;
    }

    public int getConsumeNum() {
        return consumeNum;
    }

    public void setConsumeNum(int consumeNum) {
        this.consumeNum = consumeNum;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getGotNum() {
        return gotNum;
    }

    public void setGotNum(int gotNum) {
        this.gotNum = gotNum;
    }

    public int getOnRoadNum() {
        return onRoadNum;
    }

    public void setOnRoadNum(int onRoadNum) {
        this.onRoadNum = onRoadNum;
    }

    public int getPackNum() {
        return packNum;
    }

    public void setPackNum(int packNum) {
        this.packNum = packNum;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }
}
