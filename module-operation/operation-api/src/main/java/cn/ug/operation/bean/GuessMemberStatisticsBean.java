package cn.ug.operation.bean;

public class GuessMemberStatisticsBean {
    private String memberId;
    private String mobile;
    private String name;
    private int bindedCard;
    private String bindedCardTime;
    private double gotGold;
    private int couponNum;
    private int joinNum;
    private int shareNum;
    private String lastestTime;

    public String getBindedCardTime() {
        return bindedCardTime;
    }

    public void setBindedCardTime(String bindedCardTime) {
        this.bindedCardTime = bindedCardTime;
    }

    public int getJoinNum() {
        return joinNum;
    }

    public void setJoinNum(int joinNum) {
        this.joinNum = joinNum;
    }

    public int getShareNum() {
        return shareNum;
    }

    public void setShareNum(int shareNum) {
        this.shareNum = shareNum;
    }

    public String getLastestTime() {
        return lastestTime;
    }

    public void setLastestTime(String lastestTime) {
        this.lastestTime = lastestTime;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBindedCard() {
        return bindedCard;
    }

    public void setBindedCard(int bindedCard) {
        this.bindedCard = bindedCard;
    }

    public double getGotGold() {
        return gotGold;
    }

    public void setGotGold(double gotGold) {
        this.gotGold = gotGold;
    }

    public int getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(int couponNum) {
        this.couponNum = couponNum;
    }
}
