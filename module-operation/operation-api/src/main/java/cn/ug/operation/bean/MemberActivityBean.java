package cn.ug.operation.bean;

import java.io.Serializable;

public class MemberActivityBean implements Serializable {
    private String startDate;
    private String endDate;
    private String activityName;
    private int getPrizeNum;
    private String joinTime;
    private int joinedNum;
    private int shareNum;

    private int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public int getGetPrizeNum() {
        return getPrizeNum;
    }

    public void setGetPrizeNum(int getPrizeNum) {
        this.getPrizeNum = getPrizeNum;
    }

    public int getJoinedNum() {
        return joinedNum;
    }

    public void setJoinedNum(int joinedNum) {
        this.joinedNum = joinedNum;
    }

    public int getShareNum() {
        return shareNum;
    }

    public void setShareNum(int shareNum) {
        this.shareNum = shareNum;
    }
}
