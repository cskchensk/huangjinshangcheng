package cn.ug.operation.bean;

import java.io.Serializable;

public class LotteryStatisticsBean implements Serializable {
    private int lotteryId;
    private String name;
    private int prizeNum;
    private int gotNum;
    private int onRoadNum;
    private int packNum;

    public int getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(int lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrizeNum() {
        return prizeNum;
    }

    public void setPrizeNum(int prizeNum) {
        this.prizeNum = prizeNum;
    }

    public int getGotNum() {
        return gotNum;
    }

    public void setGotNum(int gotNum) {
        this.gotNum = gotNum;
    }

    public int getOnRoadNum() {
        return onRoadNum;
    }

    public void setOnRoadNum(int onRoadNum) {
        this.onRoadNum = onRoadNum;
    }

    public int getPackNum() {
        return packNum;
    }

    public void setPackNum(int packNum) {
        this.packNum = packNum;
    }
}
