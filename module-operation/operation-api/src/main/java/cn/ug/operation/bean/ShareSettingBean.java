package cn.ug.operation.bean;

import java.io.Serializable;

public class ShareSettingBean implements Serializable {

    private Integer id;

    /**
     * 1.分享限制 2.基础设置
     */
    private Integer type;

    /**
     * 分享限制开关(type为1时必输) 1.显示 2.关闭
     */
    private Integer switchStatus;

    /**
     * 触发场景类型 多个以逗号进行区分 1.实物金购买成功 2.体验金购买成功 3.回租提单成功 4.出售提单成功 5.提金预约成功 6.商城购买成功 7.金料回购成功
     */
    private String scene;

    /**
     * 分享弹层插图说明
     */
    private String shareImgRemark;

    /**
     * 分享弹层插图
     */
    private String shareImg;

    /**
     * 分享标题
     */
    private String shareTitle;

    /**
     * 分享摘要
     */
    private String shareRemark;

    private String addTime;

    private String modifyTime;

    /**
     * 删除 1：否 2：是
     */
    private Integer deleted;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSwitchStatus() {
        return switchStatus;
    }

    public void setSwitchStatus(Integer switchStatus) {
        this.switchStatus = switchStatus;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getShareImgRemark() {
        return shareImgRemark;
    }

    public void setShareImgRemark(String shareImgRemark) {
        this.shareImgRemark = shareImgRemark;
    }

    public String getShareImg() {
        return shareImg;
    }

    public void setShareImg(String shareImg) {
        this.shareImg = shareImg;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareRemark() {
        return shareRemark;
    }

    public void setShareRemark(String shareRemark) {
        this.shareRemark = shareRemark;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

}
