package cn.ug.operation.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("user/join")
public interface CupJoinUserServiceApi {

    @PostMapping("/balance")
    public SerializeObject modifyBalance(@RequestParam("memberId")String memberId,
                                         @RequestParam("num")int num);
}
