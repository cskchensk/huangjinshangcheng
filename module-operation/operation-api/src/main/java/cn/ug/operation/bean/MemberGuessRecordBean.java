package cn.ug.operation.bean;

import java.io.Serializable;

public class MemberGuessRecordBean implements Serializable {
    private int recordId;
    private String guessDate;
    private String mobile;
    private double guessPrice;
    private double finalPrice;
    private int guessResult;
    private int shareStatus;
    private double gotGold;
    private double couponAmount;
    private int gotStatus;

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public int getGotStatus() {
        return gotStatus;
    }

    public void setGotStatus(int gotStatus) {
        this.gotStatus = gotStatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getShareStatus() {
        return shareStatus;
    }

    public void setShareStatus(int shareStatus) {
        this.shareStatus = shareStatus;
    }

    public String getGuessDate() {
        return guessDate;
    }

    public void setGuessDate(String guessDate) {
        this.guessDate = guessDate;
    }

    public double getGuessPrice() {
        return guessPrice;
    }

    public void setGuessPrice(double guessPrice) {
        this.guessPrice = guessPrice;
    }

    public int getGuessResult() {
        return guessResult;
    }

    public void setGuessResult(int guessResult) {
        this.guessResult = guessResult;
    }

    public double getGotGold() {
        return gotGold;
    }

    public void setGotGold(double gotGold) {
        this.gotGold = gotGold;
    }

    public double getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(double couponAmount) {
        this.couponAmount = couponAmount;
    }
}
