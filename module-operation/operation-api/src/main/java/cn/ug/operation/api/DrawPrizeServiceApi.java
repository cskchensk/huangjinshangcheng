package cn.ug.operation.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("draw")
public interface DrawPrizeServiceApi {

    @PostMapping("/award")
    SerializeObject award(@RequestParam("memberId")String memberId);
}
