package cn.ug.operation.bean;

import java.io.Serializable;

public class LotteryMemberMarkBean implements Serializable {
    private int index;
    private int id;
    private String lotteryName;
    private String lotteryStatus;
    private String cellphone;
    private String memeberName;
    private String joinTime;
    private String prizeName;
    private String getPrizeTime;
    private String prizeStatus;
    private String receivePrizeTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getLotteryName() {
        return lotteryName;
    }

    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName;
    }

    public String getLotteryStatus() {
        return lotteryStatus;
    }

    public void setLotteryStatus(String lotteryStatus) {
        this.lotteryStatus = lotteryStatus;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getMemeberName() {
        return memeberName;
    }

    public void setMemeberName(String memeberName) {
        this.memeberName = memeberName;
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getGetPrizeTime() {
        return getPrizeTime;
    }

    public void setGetPrizeTime(String getPrizeTime) {
        this.getPrizeTime = getPrizeTime;
    }

    public String getPrizeStatus() {
        return prizeStatus;
    }

    public void setPrizeStatus(String prizeStatus) {
        this.prizeStatus = prizeStatus;
    }

    public String getReceivePrizeTime() {
        return receivePrizeTime;
    }

    public void setReceivePrizeTime(String receivePrizeTime) {
        this.receivePrizeTime = receivePrizeTime;
    }
}
