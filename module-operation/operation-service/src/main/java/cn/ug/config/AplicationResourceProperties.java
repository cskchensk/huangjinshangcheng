package cn.ug.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix=AplicationResourceProperties.RESOURCE_PREFIX)
public class AplicationResourceProperties {
    public static final String RESOURCE_PREFIX = "system.resource";
    private int startDate;
    private int endDate;
    private List<String> couponIds;

    public int getStartDate() {
        return startDate;
    }

    public void setStartDate(int startDate) {
        this.startDate = startDate;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public List<String> getCouponIds() {
        return couponIds;
    }

    public void setCouponIds(List<String> couponIds) {
        this.couponIds = couponIds;
    }
}