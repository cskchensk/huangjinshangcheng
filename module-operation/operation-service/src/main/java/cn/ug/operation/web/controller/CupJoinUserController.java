package cn.ug.operation.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.feign.MemberUserService;
import cn.ug.member.bean.response.MemberUserBean;
import cn.ug.operation.bean.CupJoinUserBean;
import cn.ug.operation.mapper.entity.CupJoinUser;
import cn.ug.operation.service.CupJoinUserService;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("user/join")
public class CupJoinUserController extends BaseController {
    @Autowired
    private CupJoinUserService cupJoinUserService;
    @Autowired
    private MemberUserService memberUserService;

    @PostMapping
    public SerializeObject save(@RequestHeader String accessToken, String memberId) {
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000002");
        }
        SerializeObject<MemberUserBean> userBean = memberUserService.findById(memberId);
        if (userBean == null || userBean.getData() == null) {
            return new SerializeObjectError("00000003");
        }
        CupJoinUser entity = new CupJoinUser();
        entity.setMemberId(memberId);
        if (cupJoinUserService.save(entity)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PostMapping("/balance")
    public SerializeObject modifyBalance(String memberId, int num) {
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000002");
        }
        if (cupJoinUserService.modifyBalance(memberId, num)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PostMapping(value = "/check")
    public SerializeObject check(@RequestHeader String accessToken, String memberId) {
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000002");
        }

        CupJoinUser joinUser = cupJoinUserService.getJoinUser(memberId);
        if (joinUser != null) {
            return new SerializeObject(ResultType.NORMAL, "00000001", 1);
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005", 0);
        }
    }

    @GetMapping(value = "/{memberId}")
    public SerializeObject get(@PathVariable String memberId) {
        if(StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000002");
        }
        CupJoinUser joinUser = cupJoinUserService.getJoinUser(memberId);
        if(null == joinUser || StringUtils.isBlank(joinUser.getId())) {
            return new SerializeObject<>(ResultType.NORMAL, new CupJoinUser());
        }
        return new SerializeObject<>(ResultType.NORMAL, joinUser);
    }

    @GetMapping(value = "/list")
    public SerializeObject<DataTable<CupJoinUserBean>> list(@RequestHeader String accessToken, Page page,
        String mobile, String name, String startDate, String endDate) {
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        int total = cupJoinUserService.count(mobile, name, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<CupJoinUserBean> list = cupJoinUserService.list(mobile, name, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            if (list != null && list.size() > 0) {
                for (CupJoinUserBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getMobile())) {
                        bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
                    }
                }
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<CupJoinUserBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<CupJoinUserBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<CupJoinUserBean>()));
    }

    @GetMapping(value = "/top50")
    public SerializeObject<List<CupJoinUserBean>> top50() {
        List<CupJoinUserBean> list = cupJoinUserService.top50();
        if (list != null && list.size() > 0) {
            for (CupJoinUserBean bean : list) {
                if (StringUtils.isNotBlank(bean.getMobile())) {
                    bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
                }
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, list);
    }
}
