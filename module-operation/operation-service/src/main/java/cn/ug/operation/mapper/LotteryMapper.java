package cn.ug.operation.mapper;

import cn.ug.operation.bean.*;
import cn.ug.operation.mapper.entity.Lottery;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LotteryMapper {
    int insert(LotteryBean lottery);
    int update(LotteryBean lottery);

    List<LotteryActivityBean> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    Lottery findById(@Param("id")int id);
    int deleteByIds(@Param(value = "id") int[] id);
    int publish(@Param(value = "id") int[] id);
    int toFinish(@Param(value = "id") int[] id);

    List<LotteryStatisticsBean> queryForStatistics(Map<String, Object> params);
    int countForStatistics(Map<String, Object> params);

    List<LotteryAnalysisBean> queryForAnalysis(Map<String, Object> params);
    LotteryAnalysisBean selectByLotteryId(@Param("lotteryId")int lotteryId);
    int countForAnalysis(Map<String, Object> params);

    List<Lottery> queryUnderwayLottery();

    List<MemberActivityBean> queryActivity(Map<String, Object> params);
    int countActivity(Map<String, Object> params);
}
