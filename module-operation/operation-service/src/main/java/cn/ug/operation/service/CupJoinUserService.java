package cn.ug.operation.service;

import cn.ug.operation.bean.CupJoinUserBean;
import cn.ug.operation.mapper.entity.CupJoinUser;

import java.util.List;

public interface CupJoinUserService {
    CupJoinUser getJoinUser(String memberId);
    List<CupJoinUserBean> list(String mobile, String name, String startDate, String endDate, int offset, int size);
    List<CupJoinUserBean> top50();
    int count(String mobile, String name, String startDate, String endDate);
    boolean save(CupJoinUser joinUser);
    boolean increasingBalance();
    boolean modifyBalance(String memberId, int num);
    boolean increasingGotNum(String gameId, int result);
}
