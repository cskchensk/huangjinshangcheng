package cn.ug.operation.mapper;

import cn.ug.operation.bean.EverydayPrizeBean;
import cn.ug.operation.bean.LotteryAnalysisDetailBean;
import cn.ug.operation.bean.LotteryJoinMemberBean;
import cn.ug.operation.mapper.entity.LotteryJoinMember;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LotteryJoinMemberMapper {
    List<LotteryJoinMemberBean> queryForStatistics(Map<String, Object> params);
    int countForStatistics(Map<String, Object> params);

    List<LotteryAnalysisDetailBean> queryForAnalysisDetail(Map<String, Object> params);
    int countForAnalysisDetail(Map<String, Object> params);

    List<EverydayPrizeBean> queryForEverydayPrizes(Map<String, Object> params);
    int countForEverydayPrizes(Map<String, Object> params);

    int isJoined(@Param("memberId")String memberId, @Param("lotteryId")int lotteryId);
    int autoIncreasingTimes(@Param("memberId")String memberId, @Param("lotteryId")int lotteryId);
    int decreaseTimes(@Param("memberId")String memberId, @Param("lotteryId")int lotteryId);
    int insert(LotteryJoinMember lotteryJoinMember);
    LotteryJoinMember findByMemberId(@Param("memberId")String memberId, @Param("lotteryId")int lotteryId);
    int overlayTimes(@Param("lotteryId")int lotteryId, @Param("times")int times);
    int accumulateTimes(@Param("lotteryId")int lotteryId, @Param("times")int times);
    LotteryJoinMember findJoinLottery(@Param("memberId")String memberId);
 }
