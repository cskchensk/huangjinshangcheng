package cn.ug.operation.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class CupBallGame extends BaseEntity implements Serializable {
    private String code;
    private String gameDate;
    private String gameTime;
    private int type;
    private String teamaId;
    private String teamaName;
    private String teambId;
    private String teambName;
    private String result;
    private String succeeTeamId;
    private int status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGameDate() {
        return gameDate;
    }

    public void setGameDate(String gameDate) {
        this.gameDate = gameDate;
    }

    public String getGameTime() {
        return gameTime;
    }

    public void setGameTime(String gameTime) {
        this.gameTime = gameTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTeamaId() {
        return teamaId;
    }

    public void setTeamaId(String teamaId) {
        this.teamaId = teamaId;
    }

    public String getTeamaName() {
        return teamaName;
    }

    public void setTeamaName(String teamaName) {
        this.teamaName = teamaName;
    }

    public String getTeambId() {
        return teambId;
    }

    public void setTeambId(String teambId) {
        this.teambId = teambId;
    }

    public String getTeambName() {
        return teambName;
    }

    public void setTeambName(String teambName) {
        this.teambName = teambName;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSucceeTeamId() {
        return succeeTeamId;
    }

    public void setSucceeTeamId(String succeeTeamId) {
        this.succeeTeamId = succeeTeamId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
