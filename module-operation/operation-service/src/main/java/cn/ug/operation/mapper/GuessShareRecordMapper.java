package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.GuessShareRecord;

public interface GuessShareRecordMapper {
    int insert(GuessShareRecord record);
}
