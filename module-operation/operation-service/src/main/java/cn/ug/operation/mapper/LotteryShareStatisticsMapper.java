package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.LotteryShareStatistics;

public interface LotteryShareStatisticsMapper {
    int insert(LotteryShareStatistics lotteryShareStatistics);
}
