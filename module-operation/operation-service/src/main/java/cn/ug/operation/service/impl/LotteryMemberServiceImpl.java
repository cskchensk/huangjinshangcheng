package cn.ug.operation.service.impl;

import cn.ug.operation.bean.LotteryDrawRecord;
import cn.ug.operation.bean.LotteryMemberBean;
import cn.ug.operation.bean.MemberPrizeBean;
import cn.ug.operation.bean.MyLotteryPrizeBean;
import cn.ug.operation.mapper.LotteryMemberMapper;
import cn.ug.operation.service.LotteryMemberService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LotteryMemberServiceImpl implements LotteryMemberService {
    @Autowired
    private LotteryMemberMapper lotteryMemberMapper;

    @Override
    public List<MemberPrizeBean> queryForList(int lotteryId, String memberId, int status, String lotteryName, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("lotteryId", lotteryId);
            params.put("status", status);
            if (StringUtils.isNotBlank(lotteryName)) {
                params.put("lotteryName", lotteryName);
            }
            params.put("offset", offset);
            params.put("size", size);
            return lotteryMemberMapper.queryForList(params);
        }
        return null;
    }

    @Override
    public int queryForCount(int lotteryId, String memberId, int status, String lotteryName) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("lotteryId", lotteryId);
            params.put("status", status);
            if (StringUtils.isNotBlank(lotteryName)) {
                params.put("lotteryName", lotteryName);
            }
            return lotteryMemberMapper.queryForCount(params);
        }
        return 0;
    }

    @Override
    public List<LotteryMemberBean> queryForMemberList(int lotteryStatus, int prizeStatus, String lotteryName, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("lotteryStatus", lotteryStatus);
        params.put("prizeStatus", prizeStatus);
        if (StringUtils.isNotBlank(lotteryName)) {
            params.put("lotteryName", lotteryName);
        }
        params.put("offset", offset);
        params.put("size", size);
        return lotteryMemberMapper.queryForMemberList(params);
    }

    @Override
    public int queryForMemberCount(int lotteryStatus, int prizeStatus, String lotteryName) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("lotteryStatus", lotteryStatus);
        params.put("prizeStatus", prizeStatus);
        if (StringUtils.isNotBlank(lotteryName)) {
            params.put("lotteryName", lotteryName);
        }
        return lotteryMemberMapper.queryForMemberCount(params);
    }

    @Override
    public List<MyLotteryPrizeBean> queryForListByMemberId(String memberId, int lotteryId, int offset, int size) {
        if (StringUtils.isNotBlank(memberId) && lotteryId > 0) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("lotteryId", lotteryId);
            params.put("offset", offset);
            params.put("size", size);
            return lotteryMemberMapper.queryForListByMemberId(params);
        }
        return null;
    }

    @Override
    public int countForListByMemberId(String memberId, int lotteryId) {
        if (StringUtils.isNotBlank(memberId) && lotteryId > 0) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            params.put("lotteryId", lotteryId);
            return lotteryMemberMapper.countForListByMemberId(params);
        }
        return 0;
    }

    @Override
    public List<LotteryDrawRecord> recentList() {
        return lotteryMemberMapper.recentList();
    }

    /**
     * 当日抽奖次数
     *
     * @param map
     * @return
     */
    @Override
    public int countLotteryNumToday(String memberId, int lotteryId) {
        Map params = new HashMap();
        params.put("memberId",memberId);
        params.put("lotteryId",lotteryId);
        return lotteryMemberMapper.countLotteryNumToday(params);
    }
}
