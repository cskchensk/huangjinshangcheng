package cn.ug.operation.service.impl;

import cn.ug.operation.bean.PriceActivityStatistics;
import cn.ug.operation.mapper.GuessPriceActivityMapper;
import cn.ug.operation.mapper.entity.GuessJoinMember;
import cn.ug.operation.mapper.entity.GuessPriceActivity;
import cn.ug.operation.service.GuessPriceActivityService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GuessPriceActivityServiceImpl implements GuessPriceActivityService {
    @Autowired
    private GuessPriceActivityMapper guessPriceActivityMapper;

    @Override
    public GuessPriceActivity getActivity(int id) {
        if (id > 0) {
            return guessPriceActivityMapper.findById(id);
        }
        return null;
    }

    @Override
    public GuessPriceActivity getUsableActivity() {
        return guessPriceActivityMapper.findUsableActivity();
    }

    @Override
    public List<PriceActivityStatistics> queryStatisticsList(String number, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(number)) {
            params.put("number", number);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        return guessPriceActivityMapper.queryStatisticsList(params);
    }

    @Override
    public PriceActivityStatistics findStatisticsById(int activityId) {
        if (activityId > 0) {
            return guessPriceActivityMapper.findStatisticsById(activityId);
        }
        return null;
    }



    @Override
    public List<GuessPriceActivity> query(String number, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(number)) {
            params.put("number", number);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        return guessPriceActivityMapper.query(params);
    }

    @Override
    public int count(String number, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(number)) {
            params.put("number", number);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return guessPriceActivityMapper.count(params);
    }

    @Override
    public boolean save(GuessPriceActivity activity) {
        if (activity != null) {
            if (activity.getId() > 0) {
                activity.setModifyTime(LocalDateTime.now());
                return guessPriceActivityMapper.update(activity) > 0 ? true : false;
            } else {
                activity.setAddTime(LocalDateTime.now());
                return guessPriceActivityMapper.insert(activity) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public boolean updateShelfState(Integer[] id, int shelfState) {
        if (id != null && id.length > 0) {
            if (shelfState == 1 || shelfState == 2) {
                return guessPriceActivityMapper.updateShelfState(id, shelfState) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public boolean removeInBatch(Integer[] id) {
        if (id != null && id.length > 0) {
            return guessPriceActivityMapper.deleteByIds(id) > 0 ? true : false;
        }
        return false;
    }
}
