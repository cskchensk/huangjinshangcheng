package cn.ug.operation.mapper.entity;

import java.io.Serializable;

public class LotteryShareStatistics implements Serializable {
    private int lotteryId;
    private String memberId;

    public int getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(int lotteryId) {
        this.lotteryId = lotteryId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
