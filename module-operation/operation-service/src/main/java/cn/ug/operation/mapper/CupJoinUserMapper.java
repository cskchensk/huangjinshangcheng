package cn.ug.operation.mapper;

import cn.ug.operation.bean.CupJoinUserBean;
import cn.ug.operation.mapper.entity.CupJoinUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CupJoinUserMapper {
    CupJoinUser findByMemberId(@Param("memberId")String memberId);
    List<CupJoinUserBean> query(Map<String, Object> params);
    List<CupJoinUserBean> top50();
    int count(Map<String, Object> params);
    int insert(CupJoinUser joinUser);
    int update(CupJoinUser joinUser);
    int increasingBalance();
    int updateBalanceByMemberId(@Param("memberId")String memberId, @Param("num")int num);
    int increasingGotNum(@Param("gameId")String gameId, @Param("result")int result);
}