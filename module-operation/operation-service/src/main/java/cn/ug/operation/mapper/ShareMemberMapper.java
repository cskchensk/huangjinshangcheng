package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.ShareMemberEntity;

import java.util.Map;

public interface ShareMemberMapper {

    int insert(ShareMemberEntity shareMemberEntity);

    int update(Map map);

    ShareMemberEntity queryByOrderNo(Map map);
}
