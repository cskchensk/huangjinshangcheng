package cn.ug.operation.web.controller;

import cn.ug.activity.bean.InvitationRewardsBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.operation.bean.MemberActivityBean;
import cn.ug.operation.service.LotteryService;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("activity")
public class ActivityController extends BaseController {
    @Autowired
    private LotteryService lotteryService;

    @GetMapping(value = "/list")
    public SerializeObject<DataTable<InvitationRewardsBean>> listInvitationRecordsRewards(@RequestHeader String accessToken, Page page, String memberId, String startDate, String endDate) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        int total = lotteryService.countActivity(memberId, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<MemberActivityBean> list = lotteryService.queryActivity(memberId, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject(ResultType.NORMAL, new DataTable<MemberActivityBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<MemberActivityBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<MemberActivityBean>()));
    }

    @GetMapping(value = "/export")
    public void exportData(HttpServletResponse response, String memberId, String startDate, String endDate) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        List<MemberActivityBean> list = lotteryService.queryActivity(memberId, startDate, endDate, 0, 0);
        if(list != null && list.size() > 0){
            String[] columnNames = { "序号", "活动名称","活动开始日期", "活动结束日期", "首次参与活动时间", "获得奖励数量", "参与活动次数", "分享活动次数"};
            String [] columns = {"index",  "activityName", "startDate", "endDate", "joinTime", "getPrizeNum", "joinedNum", "shareNum"};
            String fileName = "参与活动记录";
            int index = 1;
            for (MemberActivityBean bean : list) {
                bean.setIndex(index);
                index++;
            }
            ExportExcelController<MemberActivityBean> export = new ExportExcelController<MemberActivityBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }
}
