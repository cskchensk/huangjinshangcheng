package cn.ug.operation.service.impl;

import cn.ug.core.ensure.Ensure;
import cn.ug.operation.bean.*;
import cn.ug.operation.mapper.LotteryMapper;
import cn.ug.operation.mapper.LotteryPrizeMapper;
import cn.ug.operation.mapper.entity.Lottery;
import cn.ug.operation.mapper.entity.LotteryPrize;
import cn.ug.operation.service.LotteryService;
import cn.ug.util.UF;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LotteryServiceImpl implements LotteryService {
    @Autowired
    private LotteryMapper lotteryMapper;
    @Autowired
    private LotteryPrizeMapper lotteryPrizeMapper;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(LotteryBean lottery, List<LotteryPrize> prizes) {
        if (lottery != null && prizes != null && prizes.size() > 0) {
            if (lottery.getId() > 0) {
                Lottery lo = lotteryMapper.findById(lottery.getId());
                if (lo == null) {
                    return false;
                }
                int rows = lotteryMapper.update(lottery);
                if (rows > 0) {
                    if (lo.getStatus() < 2) {
                        rows = lotteryPrizeMapper.deleteByLotteryId(lottery.getId());
                        Ensure.that(rows).isLt(1,"00000005");
                        for (LotteryPrize prize : prizes) {
                            prize.setLotteryId(lottery.getId());
                            rows = lotteryPrizeMapper.insert(prize);
                            Ensure.that(rows).isLt(1,"00000005");
                        }
                    } else {
                        for (LotteryPrize prize : prizes) {
                            rows = lotteryPrizeMapper.updateProbability(prize.getId(), prize.getProbability());
                            Ensure.that(rows).isLt(1,"00000005");
                        }
                    }
                    /*rows = lotteryPrizeMapper.deleteByLotteryId(lottery.getId());
                    Ensure.that(rows).isLt(1,"00000005");
                    for (LotteryPrize prize : prizes) {
                        prize.setLotteryId(lottery.getId());
                        rows = lotteryPrizeMapper.insert(prize);
                        Ensure.that(rows).isLt(1,"00000005");
                    }*/
                    return true;
                }
            } else {
                int rows = lotteryMapper.insert(lottery);
                if (rows > 0) {
                    for (LotteryPrize prize : prizes) {
                        prize.setLotteryId(lottery.getId());
                        rows = lotteryPrizeMapper.insert(prize);
                        Ensure.that(rows).isLt(1,"00000005");
                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<LotteryActivityBean> query(int status, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        return lotteryMapper.query(params);
    }

    @Override
    public int count(int status, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return lotteryMapper.count(params);
    }

    @Override
    public LotteryBean getLottery(int id) {
        if (id > 0) {
            Lottery lottery = lotteryMapper.findById(id);
            if (lottery != null) {
                LotteryBean bean = dozerBeanMapper.map(lottery, LotteryBean.class);
                if (lottery.getAddTime() != null) {
                    bean.setAddTime(UF.getFormatDateTime(lottery.getAddTime()));
                }
                if (lottery.getStartTime() != null) {
                    bean.setStartTime(UF.getFormatDateTime(lottery.getStartTime()));
                }
                if (lottery.getEndTime() != null) {
                    bean.setEndTime(UF.getFormatDateTime(lottery.getEndTime()));
                }
                if (lottery.getPublishTime() != null) {
                    bean.setPublishTime(UF.getFormatDateTime(lottery.getPublishTime()));
                }
                return bean;
            }
        }
        return null;
    }

    @Override
    public List<LotteryAnalysisBean> queryForAnalysis(int status, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        return lotteryMapper.queryForAnalysis(params);
    }

    @Override
    public int countForAnalysis(int status, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("status", status);
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return lotteryMapper.countForAnalysis(params);
    }

    @Override
    public LotteryAnalysisBean getLotteryAnalysis(int lotteryId) {
        if (lotteryId > 0) {
            return lotteryMapper.selectByLotteryId(lotteryId);
        }
        return null;
    }

    @Override
    public List<LotteryPrize> selectByLotteryId(int lotteryId) {
        if (lotteryId > 0) {
            return lotteryPrizeMapper.selectByLotteryId(lotteryId);
        }
        return null;
    }

    @Override
    public boolean deleteByIds(int[] id) {
        if (id != null && id.length > 0) {
            return lotteryMapper.deleteByIds(id) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean publish(int[] id) {
        if (id != null && id.length > 0) {
            return lotteryMapper.publish(id) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean toFinish(int[] id) {
        if (id != null && id.length > 0) {
            return lotteryMapper.toFinish(id) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<LotteryStatisticsBean> queryForStatistics(String lotteryName, String prizeName, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(lotteryName)) {
            params.put("lotteryName", lotteryName);
        }
        if (StringUtils.isNotBlank(prizeName)) {
            params.put("prizeName", prizeName);
        }
        params.put("offset", offset);
        params.put("size", size);
        return lotteryMapper.queryForStatistics(params);
    }

    @Override
    public int countForStatistics(String lotteryName, String prizeName) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(lotteryName)) {
            params.put("lotteryName", lotteryName);
        }
        if (StringUtils.isNotBlank(prizeName)) {
            params.put("prizeName", prizeName);
        }
        return lotteryMapper.countForStatistics(params);
    }

    @Override
    public List<PrizeStatisticsBean> queryForStatistics(int lotteryId, String prizeName) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(prizeName)) {
            params.put("prizeName", prizeName);
        }
        params.put("lotteryId", lotteryId);
        return lotteryPrizeMapper.queryForStatistics(params);
    }

    @Override
    public List<Lottery> queryUnderwayLottery() {
        return lotteryMapper.queryUnderwayLottery();
    }

    @Override
    public List<MemberActivityBean> queryActivity(String memberId, String startDate, String endDate, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                params.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                params.put("endDate", endDate);
            }
            params.put("offset", offset);
            params.put("size", size);
            return lotteryMapper.queryActivity(params);
        }
        return null;
    }

    @Override
    public int countActivity(String memberId, String startDate, String endDate) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                params.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                params.put("endDate", endDate);
            }
            return lotteryMapper.countActivity(params);
        }
        return 0;
    }
}
