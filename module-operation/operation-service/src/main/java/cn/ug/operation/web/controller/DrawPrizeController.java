package cn.ug.operation.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.operation.service.DrawPrizeService;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/draw")
public class DrawPrizeController extends BaseController {

    @Autowired
    private DrawPrizeService drawPrizeService;
    private static Logger logger = LoggerFactory.getLogger(DrawPrizeController.class);

    @PostMapping(value = "/prize")
    public SerializeObject prize(@RequestHeader String accessToken, String  orderNo) {
        String memberId = LoginHelper.getLoginId();
        if (StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000102");
        }

        if (StringUtils.isBlank(orderNo)){
            return new SerializeObjectError("25000080");
        }
        logger.info("会员Id:"+memberId+" accessToken:"+accessToken);
        return drawPrizeService.draw(memberId,orderNo);
    }


    @GetMapping(value = "/record")
    public SerializeObject record(@RequestHeader String accessToken, String  orderNo) {
        String memberId = LoginHelper.getLoginId();
        if (StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000102");
        }
        return drawPrizeService.drawRecord(memberId,orderNo);
    }

    @PostMapping(value = "/award")
    public SerializeObject award(String  memberId) {
        if (StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000102");
        }
        return drawPrizeService.draw(memberId,null);
    }
}


