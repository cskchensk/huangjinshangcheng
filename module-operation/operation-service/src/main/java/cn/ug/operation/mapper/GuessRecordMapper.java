package cn.ug.operation.mapper;

import cn.ug.operation.bean.EverydayGuessStatisticsBean;
import cn.ug.operation.bean.GuessMemberBean;
import cn.ug.operation.bean.GuessMemberStatisticsBean;
import cn.ug.operation.bean.MemberGuessRecordBean;
import cn.ug.operation.mapper.entity.GuessRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GuessRecordMapper {
    List<EverydayGuessStatisticsBean> queryForRecord(Map<String, Object> params);
    int countForRecord(Map<String, Object> params);

    List<GuessMemberBean> queryForGuessMembers(Map<String, Object> params);
    int countForGuessMembers(Map<String, Object> params);

    List<GuessMemberStatisticsBean> queryForGuessMemberStatistics(Map<String, Object> params);
    int countForGuessMemberStatistics(Map<String, Object> params);

    List<MemberGuessRecordBean> queryForListByMemberId(Map<String, Object> params);
    int queryForCountByMemberId(Map<String, Object> params);
    List<MemberGuessRecordBean> queryForListRecent30();
    Double selectYestodayFinalPrice();
    Double selectTodayFinalPrice();
    List<MemberGuessRecordBean> selectTodayRecord(@Param("activityId")int activityId, @Param("memberId")String memberId);
    int selectGotNum(@Param("activityId")int activityId, @Param("someday")String someday);
    int insert(GuessRecord record);
    int update(GuessRecord record);
    GuessRecord findById(@Param("id")int id);
    int updateFinalPrice(@Param("activityId")int activityId, @Param("finalPrice")double finalPrice);
    int updateGotRecord(@Param("activityId")int activityId);
    int updateFailedRecord(@Param("activityId")int activityId);

    double sumSomedayGolds(@Param("activityId")int activityId, @Param("memberId")String memberId,
                           @Param("someday")String someday);

    int updateGotStatus(@Param("activityId")int activityId,
                        @Param("memberId")String memberId,
                        @Param("someday")String someday,
                        @Param("gotStatus")int gotStatus);
}
