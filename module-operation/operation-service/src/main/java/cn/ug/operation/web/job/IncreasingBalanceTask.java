package cn.ug.operation.web.job;

import static cn.ug.util.ConstantUtil.DATE_FORMAT;

import cn.ug.config.AplicationResourceProperties;
import cn.ug.config.RedisGlobalLock;
import cn.ug.operation.service.CupJoinUserService;
import cn.ug.util.UF;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

@Component
public class IncreasingBalanceTask {
    private Log log = LogFactory.getLog(IncreasingBalanceTask.class);
    @Autowired
    private CupJoinUserService cupJoinUserService;
    @Resource
    private RedisGlobalLock redisGlobalLock;
    @Autowired
    private AplicationResourceProperties properties;

    //@Scheduled(cron = "0 0 1 * * ?")
    public void increasingBalance() {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        int currentDate = Integer.parseInt(format.format(Calendar.getInstance().getTime()));
        if (currentDate >= properties.getStartDate() && currentDate <= properties.getEndDate()) {
            String key = "IncreasingBalanceTask:increasingBalance:" + UF.getFormatDateTime("yyyy-MM-dd", UF.getDateTime());
            if(!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
                return;
            }
            log.info("赠送参与活动人员的可竞猜次数...");
            cupJoinUserService.increasingBalance();
        }
    }
}
