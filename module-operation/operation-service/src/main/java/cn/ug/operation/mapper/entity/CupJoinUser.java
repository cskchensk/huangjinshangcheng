package cn.ug.operation.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class CupJoinUser extends BaseEntity implements Serializable {
    private String joinDate;
    private String memberId;
    private int balance;
    private int gotNum;

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getGotNum() {
        return gotNum;
    }

    public void setGotNum(int gotNum) {
        this.gotNum = gotNum;
    }
}
