package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.ShareJoinMemberEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShareJoinMemberMapper {

    int insert(ShareJoinMemberEntity shareJoinMemberEntity);

    ShareJoinMemberEntity query(@Param("memberId")String memberId, @Param("orderNo")String orderNo );
}
