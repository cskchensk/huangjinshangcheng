package cn.ug.operation.mapper.entity;

import java.io.Serializable;

public class ShareUserTypeSetting implements Serializable {

    /**
     *用户类型 1.注册未绑卡用户(系统) 2.绑卡未交易用户(系统) 3.已产生交易用户(系统) 4.自定义创建(自定义)
     */
    private Integer groupType;

    /**
     *用户群Id
     */
    private Integer groupId;


    public Integer getGroupType() {
        return groupType;
    }

    public void setGroupType(Integer groupType) {
        this.groupType = groupType;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

}
