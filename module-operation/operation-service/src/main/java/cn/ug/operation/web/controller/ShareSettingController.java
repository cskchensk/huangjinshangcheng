package cn.ug.operation.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.operation.bean.ShareSettingBean;
import cn.ug.operation.service.ShareSettingService;
import cn.ug.web.controller.BaseController;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/share/setting")
public class ShareSettingController extends BaseController {

    @Autowired
    private ShareSettingService shareSettingService;

    @PostMapping(value = "/save")
    public SerializeObject save(@RequestHeader String accessToken, String requestData) {
        List<ShareSettingBean> objectList = JSONObject.parseArray(requestData,ShareSettingBean.class);
        return shareSettingService.save(objectList);
    }

    @GetMapping(value = "/queryList")
    public SerializeObject queryList(@RequestHeader String accessToken) {
        return shareSettingService.queryList();
    }

    @PostMapping(value = "/update")
    public SerializeObject update(@RequestHeader String accessToken, ShareSettingBean shareSettingBean) {
        return shareSettingService.update(shareSettingBean);
    }

    @PostMapping(value = "/delete")
    public SerializeObject delete(@RequestHeader String accessToken, Integer id) {
        return shareSettingService.delete(id);
    }

    /**
     * 查询分享的开关信息
     * @param accessToken
     * @param type  1.实物金购买成功 2.体验金购买成功 3.回租提单成功 4.出售提单成功 5.提金预约成功 6.商城购买成功 7.金料回购成功
     * @return 0表示不展示分享  1表示展示分享信息
     */
    @GetMapping(value = "/queryShareSwitch")
    public SerializeObject queryShareSwitch(@RequestHeader String accessToken,Integer type) {
        return shareSettingService.queryShareSwitch(type);
    }
}
