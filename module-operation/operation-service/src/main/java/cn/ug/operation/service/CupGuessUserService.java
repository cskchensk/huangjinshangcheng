package cn.ug.operation.service;

import cn.ug.operation.bean.CupGuessUserBean;
import cn.ug.operation.mapper.entity.CupGuessUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CupGuessUserService {
    boolean save(CupGuessUser guessUser);
    CupGuessUserBean getGuessUser(String memberId, String gameId);
    List<CupGuessUserBean> list(String startDate, String endDate, int offset, int size);
    int count(String startDate, String endDate);

    boolean updateGuessResult(String gameId, int result);
    boolean updateMissGuessResult(String gameId, int result);
}
