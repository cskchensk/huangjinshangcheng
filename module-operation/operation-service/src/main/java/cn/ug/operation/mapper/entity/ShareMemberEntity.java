package cn.ug.operation.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class ShareMemberEntity implements Serializable {

    private Integer id;

    /**
     * 抽奖用户ID
     */
    private String memberId;

    /**
     * 奖品ID
     */
    private Integer prizeId;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 奖品领取状态：0：未领取；1：已领取
     */
    private Integer status;

    /**
     * 中奖状态：0：未中奖；1：已中奖
     */
    private Integer rewardsStatus;

    /**
     * 业务号
     */
    private String orderNo;

    private String addTime;

    private String modifyTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(Integer prizeId) {
        this.prizeId = prizeId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRewardsStatus() {
        return rewardsStatus;
    }

    public void setRewardsStatus(Integer rewardsStatus) {
        this.rewardsStatus = rewardsStatus;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
