package cn.ug.operation.service.impl;

import cn.ug.operation.bean.CupJoinUserBean;
import cn.ug.operation.mapper.CupJoinUserMapper;
import cn.ug.operation.mapper.entity.CupJoinUser;
import cn.ug.operation.service.CupJoinUserService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static cn.ug.util.ConstantUtil.NORMAL_DATE_FORMAT;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CupJoinUserServiceImpl implements CupJoinUserService {
    @Autowired
    private CupJoinUserMapper cupJoinUserMapper;

    @Override
    public List<CupJoinUserBean> top50() {
        return cupJoinUserMapper.top50();
    }

    @Override
    public CupJoinUser getJoinUser(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return cupJoinUserMapper.findByMemberId(memberId);
        }
        return null;
    }

    @Override
    public List<CupJoinUserBean> list(String mobile, String name, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        return cupJoinUserMapper.query(params);
    }

    @Override
    public int count(String mobile, String name, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(mobile)) {
            params.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return cupJoinUserMapper.count(params);
    }

    @Override
    public boolean save(CupJoinUser joinUser) {
        if (joinUser != null && StringUtils.isNotBlank(joinUser.getMemberId())) {
            CupJoinUser info = cupJoinUserMapper.findByMemberId(joinUser.getMemberId());
            if (info != null) {
                return true;
            }
            SimpleDateFormat format = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            joinUser.setBalance(3);
            joinUser.setJoinDate(format.format(Calendar.getInstance().getTime()));
            joinUser.setId(UF.getRandomUUID());
            return cupJoinUserMapper.insert(joinUser) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean increasingBalance() {
        return cupJoinUserMapper.increasingBalance() > 0 ? true : false;
    }

    @Override
    public boolean modifyBalance(String memberId, int num) {
        if (StringUtils.isNotBlank(memberId)) {
            return cupJoinUserMapper.updateBalanceByMemberId(memberId, num) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean increasingGotNum(String gameId, int result) {
        if (StringUtils.isNotBlank(gameId)) {
            return cupJoinUserMapper.increasingGotNum(gameId, result) > 0 ? true : false;
        }
        return false;
    }
}
