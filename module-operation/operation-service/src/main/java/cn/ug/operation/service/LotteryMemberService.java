package cn.ug.operation.service;

import cn.ug.operation.bean.LotteryDrawRecord;
import cn.ug.operation.bean.LotteryMemberBean;
import cn.ug.operation.bean.MemberPrizeBean;
import cn.ug.operation.bean.MyLotteryPrizeBean;

import java.util.List;

public interface LotteryMemberService {
    List<MemberPrizeBean> queryForList(int lotteryId, String memberId, int status, String lotteryName, int offset, int size);
    int queryForCount(int lotteryId, String memberId, int status, String lotteryName);

    List<LotteryMemberBean> queryForMemberList(int lotteryStatus, int prizeStatus, String lotteryName, int offset, int size);
    int queryForMemberCount(int lotteryStatus, int prizeStatus, String lotteryName);

    List<MyLotteryPrizeBean> queryForListByMemberId(String memberId, int lotteryId, int offset, int size);
    int countForListByMemberId(String memberId, int lotteryId);

    List<LotteryDrawRecord> recentList();

    /**
     * 当日抽奖次数
     * @param memberId
     * @param lotteryId
     * @return
     */
    int countLotteryNumToday(String memberId, int lotteryId);
}
