package cn.ug.operation.mapper.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class GuessRecord implements Serializable {
    private int id;
    private int activityId;
    private LocalDateTime guessTime;
    private String memberId;
    private double guessPrice;
    private double finalPrice;
    private int guessResult;
    private int shareStatus;
    private double gotGold;
    private double couponAmount;
    private int gotStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGotStatus() {
        return gotStatus;
    }

    public void setGotStatus(int gotStatus) {
        this.gotStatus = gotStatus;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public LocalDateTime getGuessTime() {
        return guessTime;
    }

    public void setGuessTime(LocalDateTime guessTime) {
        this.guessTime = guessTime;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public double getGuessPrice() {
        return guessPrice;
    }

    public void setGuessPrice(double guessPrice) {
        this.guessPrice = guessPrice;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getGuessResult() {
        return guessResult;
    }

    public void setGuessResult(int guessResult) {
        this.guessResult = guessResult;
    }

    public int getShareStatus() {
        return shareStatus;
    }

    public void setShareStatus(int shareStatus) {
        this.shareStatus = shareStatus;
    }

    public double getGotGold() {
        return gotGold;
    }

    public void setGotGold(double gotGold) {
        this.gotGold = gotGold;
    }

    public double getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(double couponAmount) {
        this.couponAmount = couponAmount;
    }
}
