package cn.ug.operation.mapper;

import cn.ug.operation.bean.GuessMemberStatisticsBean;
import cn.ug.operation.mapper.entity.GuessJoinMember;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface GuessJoinMemberMapper {
    List<GuessMemberStatisticsBean> queryForList(Map<String, Object> params);
    int countForCount(Map<String, Object> params);

    int insert(GuessJoinMember record);
    int updateJoinInfo(@Param("memberId")String memberId, @Param("activityId")int activityId,
                       @Param("lastestTime")String lastestTime);
    int updateShareNum(@Param("memberId")String memberId, @Param("activityId")int activityId);
    int updateBalance(@Param("memberId")String memberId, @Param("activityId")int activityId,
                        @Param("num")int num);
    int updateGolds(@Param("memberId")String memberId, @Param("activityId")int activityId,
                        @Param("num")BigDecimal num);
    int updateCoupons(@Param("memberId")String memberId, @Param("activityId")int activityId,
                        @Param("num")int num);
    GuessJoinMember findJoinInfo(@Param("memberId")String memberId, @Param("activityId")int activityId);

    int updateAllMemberBalance(@Param("activityId")int activityId);
}
