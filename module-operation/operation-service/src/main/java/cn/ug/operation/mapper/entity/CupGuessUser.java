package cn.ug.operation.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class CupGuessUser extends BaseEntity implements Serializable {
    private String memberId;
    private String gameId;
    private int guessResult;
    private int finalResult;
    private int guessStatus;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getGuessResult() {
        return guessResult;
    }

    public void setGuessResult(int guessResult) {
        this.guessResult = guessResult;
    }

    public int getFinalResult() {
        return finalResult;
    }

    public void setFinalResult(int finalResult) {
        this.finalResult = finalResult;
    }

    public int getGuessStatus() {
        return guessStatus;
    }

    public void setGuessStatus(int guessStatus) {
        this.guessStatus = guessStatus;
    }
}
