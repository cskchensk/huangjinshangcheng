package cn.ug.operation.mapper.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Lottery implements Serializable {
    private int id;
    private String name;
    private String title;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String rules;
    private int accumulateNum;
    private int defaultNum;
    private int everydayNum;
    private int everydayMaxNum;
    private int grantingMethod;
    private int shareRewards;
    private int shareNum;
    private String shareTitle;
    private String shareImg;
    private String shareRemark;
    private int status;
    private LocalDateTime addTime;
    private LocalDateTime publishTime;
    private LocalDateTime modifyTime;
    private int deleted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public int getAccumulateNum() {
        return accumulateNum;
    }

    public void setAccumulateNum(int accumulateNum) {
        this.accumulateNum = accumulateNum;
    }

    public int getDefaultNum() {
        return defaultNum;
    }

    public void setDefaultNum(int defaultNum) {
        this.defaultNum = defaultNum;
    }

    public int getEverydayNum() {
        return everydayNum;
    }

    public void setEverydayNum(int everydayNum) {
        this.everydayNum = everydayNum;
    }

    public int getEverydayMaxNum() {
        return everydayMaxNum;
    }

    public void setEverydayMaxNum(int everydayMaxNum) {
        this.everydayMaxNum = everydayMaxNum;
    }

    public int getGrantingMethod() {
        return grantingMethod;
    }

    public void setGrantingMethod(int grantingMethod) {
        this.grantingMethod = grantingMethod;
    }

    public int getShareRewards() {
        return shareRewards;
    }

    public void setShareRewards(int shareRewards) {
        this.shareRewards = shareRewards;
    }

    public int getShareNum() {
        return shareNum;
    }

    public void setShareNum(int shareNum) {
        this.shareNum = shareNum;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareImg() {
        return shareImg;
    }

    public void setShareImg(String shareImg) {
        this.shareImg = shareImg;
    }

    public String getShareRemark() {
        return shareRemark;
    }

    public void setShareRemark(String shareRemark) {
        this.shareRemark = shareRemark;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public LocalDateTime getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(LocalDateTime publishTime) {
        this.publishTime = publishTime;
    }

    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
