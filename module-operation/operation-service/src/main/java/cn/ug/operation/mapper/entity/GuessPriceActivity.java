package cn.ug.operation.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class GuessPriceActivity implements Serializable {
    private int id;
    private LocalDateTime addTime;
    private LocalDateTime modifyTime;
    private String name;
    private String number;
    private String startDate;
    private String endDate;
    private int goldWeight;
    private String timePoint;
    private String startGuessPoint;
    private String endGuessPoint;
    private String publishPoint;
    private int shelfState;
    private int deleted;
    private String couponIds;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCouponIds() {
        return couponIds;
    }

    public void setCouponIds(String couponIds) {
        this.couponIds = couponIds;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public LocalDateTime getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(LocalDateTime modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getGoldWeight() {
        return goldWeight;
    }

    public void setGoldWeight(int goldWeight) {
        this.goldWeight = goldWeight;
    }

    public String getTimePoint() {
        return timePoint;
    }

    public void setTimePoint(String timePoint) {
        this.timePoint = timePoint;
    }

    public String getStartGuessPoint() {
        return startGuessPoint;
    }

    public void setStartGuessPoint(String startGuessPoint) {
        this.startGuessPoint = startGuessPoint;
    }

    public String getEndGuessPoint() {
        return endGuessPoint;
    }

    public void setEndGuessPoint(String endGuessPoint) {
        this.endGuessPoint = endGuessPoint;
    }

    public String getPublishPoint() {
        return publishPoint;
    }

    public void setPublishPoint(String publishPoint) {
        this.publishPoint = publishPoint;
    }

    public int getShelfState() {
        return shelfState;
    }

    public void setShelfState(int shelfState) {
        this.shelfState = shelfState;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
