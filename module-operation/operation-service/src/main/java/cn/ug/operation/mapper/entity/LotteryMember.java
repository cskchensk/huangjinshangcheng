package cn.ug.operation.mapper.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class LotteryMember implements Serializable {
    private int id;
    private String memberId;
    private int lotteryId;
    private int prizeId;
    private double amount;
    private LocalDateTime addTime;
    private LocalDateTime getTime;
    private int status;
    private int rewardsStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getGetTime() {
        return getTime;
    }

    public void setGetTime(LocalDateTime getTime) {
        this.getTime = getTime;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getLotteryId() {
        return lotteryId;
    }

    public void setLotteryId(int lotteryId) {
        this.lotteryId = lotteryId;
    }

    public int getPrizeId() {
        return prizeId;
    }

    public void setPrizeId(int prizeId) {
        this.prizeId = prizeId;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRewardsStatus() {
        return rewardsStatus;
    }

    public void setRewardsStatus(int rewardsStatus) {
        this.rewardsStatus = rewardsStatus;
    }
}
