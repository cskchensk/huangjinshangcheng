package cn.ug.operation.service;

import cn.ug.operation.bean.PriceActivityStatistics;
import cn.ug.operation.mapper.entity.GuessJoinMember;
import cn.ug.operation.mapper.entity.GuessPriceActivity;

import java.util.List;

public interface GuessPriceActivityService {
    GuessPriceActivity getActivity(int id);
    GuessPriceActivity getUsableActivity();
    List<GuessPriceActivity> query(String number, String startDate, String endDate, int offset, int size);
    int count(String number, String startDate, String endDate);

    List<PriceActivityStatistics> queryStatisticsList(String number, String startDate, String endDate, int offset, int size);
    PriceActivityStatistics findStatisticsById(int activityId);
    boolean save(GuessPriceActivity activity);
    boolean updateShelfState(Integer[] id, int shelfState);
    boolean removeInBatch(Integer[] id);


}
