package cn.ug.operation.mapper;

import cn.ug.operation.bean.PriceActivityStatistics;
import cn.ug.operation.mapper.entity.GuessPriceActivity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GuessPriceActivityMapper {
    GuessPriceActivity findById(@Param("id")int id);
    GuessPriceActivity findUsableActivity();
    List<GuessPriceActivity> query(Map<String, Object> params);
    List<PriceActivityStatistics> queryStatisticsList(Map<String, Object> params);
    PriceActivityStatistics findStatisticsById(@Param("activityId")int activityId);
    int count(Map<String, Object> params);
    int insert(GuessPriceActivity activity);
    int update(GuessPriceActivity activity);
    int updateShelfState(@Param(value="id")Integer[] id, @Param("shelfState")int shelfState);
    int deleteByIds(@Param(value="id")Integer[] id);
}
