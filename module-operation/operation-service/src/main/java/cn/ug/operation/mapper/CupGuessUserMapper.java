package cn.ug.operation.mapper;

import cn.ug.operation.bean.CupGuessUserBean;
import cn.ug.operation.mapper.entity.CupGuessUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CupGuessUserMapper {
    int insert(CupGuessUser guessUser);
    CupGuessUserBean findByMemberIdAndGameId(@Param("memberId")String memberId, @Param("gameId")String gameId);
    List<CupGuessUserBean> query(Map<String, Object> params);
    List<CupGuessUser> findByMemberId(@Param("memberId")String memberId);
    int count(Map<String, Object> params);
    int countGuessedNum(@Param("type")int type);
    int updateGuessResult(@Param("gameId")String gameId, @Param("result")int result);
    int updateMissGuessResult(@Param("gameId")String gameId, @Param("result")int result);
}
