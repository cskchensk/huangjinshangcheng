package cn.ug.operation.web.controller;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.operation.mapper.entity.CupBallTeam;
import cn.ug.operation.service.CupBallTeamService;
import cn.ug.util.PaginationUtil;
import cn.ug.web.controller.BaseController;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("team")
public class CupBallTeamController extends BaseController {
    @Autowired
    private CupBallTeamService cupBallTeamService;

    @GetMapping(value = "/{id}")
    public SerializeObject get(@RequestHeader String accessToken, @PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObjectError("00000002");
        }
        CupBallTeam entity = cupBallTeamService.getTeam(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    @PostMapping
    public SerializeObject save(@RequestHeader String accessToken, CupBallTeam entity) {
        if(null == entity || StringUtils.isBlank(entity.getName())) {
            return new SerializeObjectError("25000001");
        }
        if (StringUtils.isBlank(entity.getId()) && StringUtils.isBlank(entity.getIcon())) {
            return new SerializeObjectError("25000002");
        }
        if (StringUtils.isBlank(entity.getGroupOf())) {
            return new SerializeObjectError("25000003");
        }
        if (cupBallTeamService.save(entity)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @GetMapping(value = "/list")
    public SerializeObject<DataTable<CupBallTeam>> list(@RequestHeader String accessToken, Page page) {
        int total = cupBallTeamService.count();
        page.setTotal(total);
        if (total > 0) {
            int offset = PaginationUtil.getOffset(page.getPageNum(), page.getPageSize());
            int size = PaginationUtil.getPageSize(page.getPageSize());
            if (page.getPageNum() == 0) {
                offset = 0;
                size = 0;
            }
            List<CupBallTeam> list = cupBallTeamService.list(offset, size);
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<CupBallTeam>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<CupBallTeam>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<CupBallTeam>()));
    }

    @GetMapping(value = "/billboard")
    public SerializeObject<List<JSONObject>> list() {
        List<CupBallTeam> list = cupBallTeamService.list();
        List<JSONObject> result = new ArrayList<JSONObject>();
        if (list != null && list.size() > 0) {
            String group = list.get(0).getGroupOf();
            List<CupBallTeam> groupTeam = new ArrayList<CupBallTeam>();
            for (CupBallTeam team : list) {
                if (StringUtils.equals(team.getGroupOf(), group)) {
                    groupTeam.add(team);
                } else {
                    JSONObject data = new JSONObject();
                    data.put("group", group);
                    data.put("data", groupTeam);
                    result.add(data);

                    group = team.getGroupOf();
                    groupTeam = new ArrayList<CupBallTeam>();
                    groupTeam.add(team);
                }
            }
            if (groupTeam != null && groupTeam.size() > 0) {
                JSONObject data = new JSONObject();
                data.put("group", group);
                data.put("data", groupTeam);
                result.add(data);
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, result);
    }
}
