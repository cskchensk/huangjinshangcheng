package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.ShareSettingEntity;

import java.util.List;
import java.util.Map;

public interface ShareSettingMapper {

    int insert(ShareSettingEntity shareSettingEntity);

    int update(Map requestMap);

    List<ShareSettingEntity> queryList();

    ShareSettingEntity query(int id);

    int delete(int id);

    ShareSettingEntity queryShareSwitch();

    List<ShareSettingEntity> queryByType(Integer type);
}
