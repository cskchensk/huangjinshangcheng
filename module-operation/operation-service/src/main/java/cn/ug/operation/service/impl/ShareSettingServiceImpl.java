package cn.ug.operation.service.impl;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.ensure.Ensure;
import cn.ug.operation.bean.ShareSettingBean;
import cn.ug.operation.mapper.ShareSettingMapper;
import cn.ug.operation.mapper.entity.ShareSettingEntity;
import cn.ug.operation.service.ShareSettingService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ShareSettingServiceImpl implements ShareSettingService {

    @Autowired
    private ShareSettingMapper shareSettingMapper;

    @Override
    public SerializeObject save(List<ShareSettingBean> shareSettingBeanList) {
        if (!CollectionUtils.isEmpty(shareSettingBeanList)){
            for (ShareSettingBean shareSettingBean : shareSettingBeanList){
                if (shareSettingBean.getType() == 1){
                    if (shareSettingBean.getSwitchStatus() == null){
                        return new SerializeObjectError("25000077");
                    }

                    if (shareSettingBean.getSwitchStatus() == 1
                            && StringUtils.isBlank(shareSettingBean.getScene())){
                        return new SerializeObjectError("25000078");
                    }

                }

                if (shareSettingBean.getId()!=null){
                    update(shareSettingBean);
                }else {
                    LinkedHashMap<Integer, List<ShareSettingBean>> groupBy = shareSettingBeanList.stream().collect(Collectors.groupingBy(ShareSettingBean::getType, LinkedHashMap::new, Collectors.toList()));
                    if (groupBy.get(2) == null) {
                        return new SerializeObjectError("25000079");
                    }

                    ShareSettingEntity shareSettingEntity = new ShareSettingEntity();
                    BeanUtils.copyProperties(shareSettingBean, shareSettingEntity);
                    shareSettingEntity.setDeleted(1);
                    int rows = shareSettingMapper.insert(shareSettingEntity);
                    Ensure.that(rows).isLt(1, "00000005");
                }
            }
            return new SerializeObject<>(ResultType.NORMAL, "00000001");
        }
        return new SerializeObjectError("00000005");
    }

    @Override
    public SerializeObject<ShareSettingBean> queryList() {
        List<ShareSettingEntity> shareSettingEntityList = shareSettingMapper.queryList();
        if (!CollectionUtils.isEmpty(shareSettingEntityList)){
            List<ShareSettingBean> resultList = new ArrayList();
            for (ShareSettingEntity shareSettingEntity : shareSettingEntityList){
                ShareSettingBean shareSettingBean = new ShareSettingBean();
                BeanUtils.copyProperties(shareSettingEntity,shareSettingBean);
                resultList.add(shareSettingBean);
            }
            return new SerializeObject(ResultType.NORMAL, resultList);
        }
        return null;
    }

    @Override
    public SerializeObject update(ShareSettingBean shareSettingBean) {
        ShareSettingEntity shareSettingEntity = shareSettingMapper.query(shareSettingBean.getId());
        if (shareSettingEntity==null){
            return new SerializeObjectError("00000003");
        }
        Map requestMap = new HashMap();
        requestMap.put("id",shareSettingBean.getId());
        requestMap.put("switchStatus",shareSettingBean.getSwitchStatus());
        requestMap.put("scene",shareSettingBean.getScene());
        requestMap.put("shareImgRemark",shareSettingBean.getShareImgRemark());
        requestMap.put("shareImg",shareSettingBean.getShareImg());
        requestMap.put("shareTitle",shareSettingBean.getShareTitle());
        requestMap.put("shareRemark",shareSettingBean.getShareRemark());
        int rows = shareSettingMapper.update(requestMap);
        Ensure.that(rows).isLt(1,"00000005");
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }

    @Override
    public SerializeObject delete(Integer id) {
        ShareSettingEntity shareSettingEntity = shareSettingMapper.query(id);
        if (shareSettingEntity==null){
            return new SerializeObjectError("00000003");
        }

        Map requestMap = new HashMap();
        requestMap.put("id",id);
        requestMap.put("deleted",2);
        int rows = shareSettingMapper.update(requestMap);
        Ensure.that(rows).isLt(1,"00000005");
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }

    @Override
    public SerializeObject queryShareSwitch(Integer type) {
        ShareSettingEntity shareSettingEntity = shareSettingMapper.queryShareSwitch();
        Map resultMap = new HashMap();
        resultMap.put("status",0);
        if (shareSettingEntity==null){
            return new SerializeObject<>(ResultType.NORMAL, resultMap);
        }
        if(shareSettingEntity.getSwitchStatus() == 2){
            return new SerializeObject<>(ResultType.NORMAL, resultMap);
        }

        if (StringUtils.isNotBlank(shareSettingEntity.getScene()) &&
                shareSettingEntity.getScene().contains(String.valueOf(type))){
            resultMap.put("status",1);

            List<ShareSettingEntity> shareSettingEntityList = shareSettingMapper.queryByType(2);
            Random random = new java.util.Random();
            int index = random.nextInt(shareSettingEntityList.size());
            ShareSettingEntity settingEntity = shareSettingEntityList.get(index);
            resultMap.put("shareImgRemark",settingEntity.getShareImgRemark());
            resultMap.put("shareImg",settingEntity.getShareImg());
            resultMap.put("shareTitle",settingEntity.getShareTitle());
            resultMap.put("shareRemark",settingEntity.getShareRemark());
            return new SerializeObject<>(ResultType.NORMAL, resultMap);
        }
        return new SerializeObject<>(ResultType.NORMAL, resultMap);
    }
}
