package cn.ug.operation.mapper;

import cn.ug.operation.bean.GameBean;
import cn.ug.operation.mapper.entity.CupBallGame;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CupBallGameMapper {
    int insert(CupBallGame game);
    int update(CupBallGame game);
    CupBallGame findById(@Param("id")String id);
    List<CupBallGame> query(Map<String, Object> params);
    List<GameBean> queryAllForList();
    int count(Map<String, Object> params);
    List<CupBallGame> queryPastGames();
    int updateStatus(@Param("id")String id);
}
