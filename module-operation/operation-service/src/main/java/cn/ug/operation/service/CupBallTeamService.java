package cn.ug.operation.service;

import cn.ug.operation.mapper.entity.CupBallTeam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CupBallTeamService {
    boolean save(CupBallTeam team);
    CupBallTeam getTeam(String id);
    List<CupBallTeam> list(int offset, int size);
    List<CupBallTeam> list();
    int count();

    boolean increaseSuccessNum(String id, int goals);
    boolean increaseFailNum(String id, int goals);
    boolean increaseDogfallNum(String id, int goals);
}
