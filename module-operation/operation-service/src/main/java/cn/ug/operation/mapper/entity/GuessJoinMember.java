package cn.ug.operation.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class GuessJoinMember implements Serializable {
    private int id;
    private String memberId;
    private int activityId;
    private String lastestTime;
    private int shareNum;
    private BigDecimal golds;
    private int coupons;
    private int joinNum;
    private String addTime;
    private int balance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public String getLastestTime() {
        return lastestTime;
    }

    public void setLastestTime(String lastestTime) {
        this.lastestTime = lastestTime;
    }

    public int getShareNum() {
        return shareNum;
    }

    public void setShareNum(int shareNum) {
        this.shareNum = shareNum;
    }

    public BigDecimal getGolds() {
        return golds;
    }

    public void setGolds(BigDecimal golds) {
        this.golds = golds;
    }

    public int getCoupons() {
        return coupons;
    }

    public void setCoupons(int coupons) {
        this.coupons = coupons;
    }

    public int getJoinNum() {
        return joinNum;
    }

    public void setJoinNum(int joinNum) {
        this.joinNum = joinNum;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
