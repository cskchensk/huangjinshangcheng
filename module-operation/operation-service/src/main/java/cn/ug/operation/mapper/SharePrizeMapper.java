package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.SharePrizeEntity;
import cn.ug.operation.mapper.entity.ShareUserTypeSetting;

import java.util.List;
import java.util.Map;

public interface SharePrizeMapper {

    int insert(SharePrizeEntity sharePrizeEntity);

    int update(Map requestMap);

    List<SharePrizeEntity> queryListByGroupId(Integer id);

    int delete(Integer id);

    SharePrizeEntity queryById(Integer id);

    List<ShareUserTypeSetting> queryGroup();

    List<SharePrizeEntity> queryByType(Integer type);

    String queryMemberIdForType(Map params);
}
