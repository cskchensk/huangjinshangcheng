package cn.ug.operation.web.job;

import cn.ug.bean.base.SerializeObject;
import cn.ug.config.RedisGlobalLock;
import cn.ug.feign.PriceService;
import cn.ug.operation.mapper.GuessJoinMemberMapper;
import cn.ug.operation.mapper.entity.GuessPriceActivity;
import cn.ug.operation.service.GuessPriceActivityService;
import cn.ug.operation.service.GuessRecordService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static cn.ug.util.ConstantUtil.COLON;
import static cn.ug.util.ConstantUtil.NORMAL_DATE_FORMAT;

@Component
public class PublishGuessResultTask {
    private Log log = LogFactory.getLog(PublishGuessResultTask.class);
    @Resource
    private RedisGlobalLock redisGlobalLock;
    @Autowired
    private GuessPriceActivityService guessPriceActivityService;
    @Autowired
    private GuessRecordService guessRecordService;
    @Autowired
    private PriceService priceService;
    @Autowired
    private GuessJoinMemberMapper guessJoinMemberMapper;

    @Scheduled(cron = "0 59 23 ? * *")
    //@Scheduled(cron = "0 06 10 ? * *")
    public void increasingBalance() {
        GuessPriceActivity entity = guessPriceActivityService.getUsableActivity();
        if(null == entity || entity.getId() <= 0) {
            return;
        }
        guessJoinMemberMapper.updateAllMemberBalance(entity.getId());
    }

    @Scheduled(fixedRate = 40 * 1000)
    public void publish() {
        GuessPriceActivity entity = guessPriceActivityService.getUsableActivity();
        if(null == entity || entity.getId() <= 0) {
            return;
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");
        int currentPoint = Integer.parseInt(timeFormat.format(new Date()));
        int publishPoint = Integer.parseInt(StringUtils.removeAll(entity.getPublishPoint(), COLON));
        if (publishPoint == currentPoint) {
            /*String key = "StatisticsResultJob:gameResult:" + UF.getFormatDateTime("yyyy-MM-dd", UF.getDateTime());
            if(!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
                return;
            }*/
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            String someday = dateFormat.format(Calendar.getInstance().getTime()) + " " + entity.getTimePoint();
            SerializeObject<Double> result = priceService.getSomedayPrice(someday);
            if (result != null) {
                double price = result.getData();
                if (guessRecordService.updateFinalPrice(entity.getId(), price)) {
                    guessRecordService.updateGotRecord(entity.getId());
                    guessRecordService.updateFailedRecord(entity.getId());
                }
            }
        }
    }
}
