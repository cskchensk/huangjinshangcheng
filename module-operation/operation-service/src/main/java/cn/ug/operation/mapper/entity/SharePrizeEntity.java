package cn.ug.operation.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class SharePrizeEntity implements Serializable{

    private Integer id;

    /**
     * 用户组类型 1.注册未绑卡用户(系统) 2.绑卡未交易用户(系统) 3.已产生交易用户(系统) 4.自定义创建(自定义)
     */
    private Integer groupType;
    /**
     * 用户组id
     */
    private Integer groupId;

    /**
     * 奖品名称
     */
    private String name;

    /**
     * 奖励类型 1.体验金、2.金豆、3.黄金红包、4.回租福利券、5.商城满减券、6.现金奖励
     */
    private Integer type;

    /**
     * 金额（现金金额或黄金克重,额度,金豆）
     */
    private BigDecimal amount;

    /**
     * 数量限制：0：不限制；1：限制
     */
    private Integer quantityLimit;

    /**
     * 奖品数量
     */
    private Integer quantity;

    /**
     * 优惠券Id，多个用","分割'
     */
    private String couponIds;

    /**
     * 中奖概率
     */
    private BigDecimal probability;

    /**
     * 删除 1：否 2：是
     */
    private Integer deleted;

    private String addTime;

    private String modifyTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getQuantityLimit() {
        return quantityLimit;
    }

    public void setQuantityLimit(Integer quantityLimit) {
        this.quantityLimit = quantityLimit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCouponIds() {
        return couponIds;
    }

    public void setCouponIds(String couponIds) {
        this.couponIds = couponIds;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getGroupType() {
        return groupType;
    }

    public void setGroupType(Integer groupType) {
        this.groupType = groupType;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}
