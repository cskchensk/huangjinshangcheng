package cn.ug.operation.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.operation.bean.EverydayPrizeBean;
import cn.ug.operation.bean.LotteryAnalysisDetailBean;
import cn.ug.operation.bean.LotteryBean;
import cn.ug.operation.bean.LotteryJoinMemberBean;
import cn.ug.operation.mapper.entity.LotteryJoinMember;

import java.util.List;

public interface LotteryJoinMemberService {
    List<LotteryJoinMemberBean> queryForStatistics(int lotteryId, String cellphone, String memberName, String channelId,
                                                   String order, String sort, int offset, int size);
    int countForStatistics(int lotteryId, String cellphone, String memberName, String channelId);

    List<LotteryAnalysisDetailBean> queryForAnalysisDetail(int lotteryId, String startDate, String endDate, int offset, int size);
    int countForAnalysisDetail(int lotteryId, String startDate, String endDate);

    List<EverydayPrizeBean> queryForEverydayPrizes(int lotteryId, String drawDate, int offset, int size);
    int countForEverydayPrizes(int lotteryId, String drawDate);

    SerializeObject save(LotteryJoinMember lotteryJoinMember);
    SerializeObject share(String memberId, LotteryBean lottery,int type);
    SerializeObject draw(String memberId, LotteryBean lottery);
    SerializeObject getMemberTimes(String memberId, LotteryBean lottery);
    SerializeObject getDoubleElevenMemberTimes(String memberId, LotteryBean lottery);
    boolean overlayTimes(int lotteryId, int times);
    boolean accumulateTimes(int lotteryId, int times);

    /**
     * 给某用户增加抽奖次数
     * @param lotteryId
     * @param memberId
     * @return
     */
    boolean addMemberLotteryTimes(int lotteryId, String memberId);

    /**
     * 双十一抽奖
     * @param memberId
     * @param lottery
     * @return
     */
    SerializeObject doubleElevenDraw(String memberId, LotteryBean lottery);

    /**
     * 根据用户id，查询用户参加的活动
     * @param memberId
     * @return
     */
    SerializeObject findJoinLotteryByMember(String memberId);
}
