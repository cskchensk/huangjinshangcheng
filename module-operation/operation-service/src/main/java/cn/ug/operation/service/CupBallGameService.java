package cn.ug.operation.service;

import cn.ug.operation.bean.GameBean;
import cn.ug.operation.bean.GameStatisticsBean;
import cn.ug.operation.mapper.entity.CupBallGame;

import java.util.List;

public interface CupBallGameService {
    boolean save(CupBallGame game);
    CupBallGame getGame(String id);
    List<CupBallGame> query(String startDate, String endDate, int offset, int size);
    int count(String startDate, String endDate);
    GameStatisticsBean statisticsGame();
    int getTotalGold();
    List<GameBean> listGames(String memberId);

    List<CupBallGame> listPastGames();
    boolean updateStatus(String id);
}
