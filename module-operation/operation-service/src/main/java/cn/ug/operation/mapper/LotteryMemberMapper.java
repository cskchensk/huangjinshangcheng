package cn.ug.operation.mapper;

import cn.ug.operation.bean.LotteryDrawRecord;
import cn.ug.operation.bean.LotteryMemberBean;
import cn.ug.operation.bean.MemberPrizeBean;
import cn.ug.operation.bean.MyLotteryPrizeBean;
import cn.ug.operation.mapper.entity.LotteryMember;

import java.util.List;
import java.util.Map;

public interface LotteryMemberMapper {
    List<MemberPrizeBean> queryForList(Map<String, Object> params);
    int queryForCount(Map<String, Object> params);

    List<LotteryMemberBean> queryForMemberList(Map<String, Object> params);
    int queryForMemberCount(Map<String, Object> params);

    List<MyLotteryPrizeBean> queryForListByMemberId(Map<String, Object> params);
    int countForListByMemberId(Map<String, Object> params);
    int insert(LotteryMember lotteryMember);
    int update(LotteryMember lotteryMember);

    List<LotteryDrawRecord> recentList();

    int countLotteryNum(Map<String, Object> params);

    int countLotteryNumToday(Map<String, Object> params);
}
