package cn.ug.operation.mapper;

import cn.ug.operation.mapper.entity.CupBallTeam;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CupBallTeamMapper {
    int insert(CupBallTeam team);
    int update(CupBallTeam team);
    CupBallTeam findById(@Param("id")String id);
    List<CupBallTeam> query(Map<String, Object> params);
    List<CupBallTeam> queryAllForList();
    int count();
    int increaseSuccessNum(@Param("id")String id, @Param("goals")int goals);
    int increaseFailNum(@Param("id")String id, @Param("goals")int goals);
    int increaseDogfallNum(@Param("id")String id, @Param("goals")int goals);
}
