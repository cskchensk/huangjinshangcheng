package cn.ug.operation.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.operation.bean.SharePrizeBean;
import cn.ug.operation.bean.ShareSettingBean;
import cn.ug.operation.service.SharePrizeService;
import cn.ug.web.controller.BaseController;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/share/prize")
public class SharePrizeController extends BaseController {

    @Autowired
    private SharePrizeService sharePrizeService;

    @PostMapping(value = "/save")
    public SerializeObject save(@RequestHeader String accessToken,String requestData) {
        List<SharePrizeBean> objectList = JSONObject.parseArray(requestData,SharePrizeBean.class);
        return sharePrizeService.save(objectList);
    }

    @GetMapping(value = "/queryList")
    public SerializeObject queryList(@RequestHeader String accessToken) {
        return sharePrizeService.queryList();
    }

    @PostMapping(value = "/update")
    public SerializeObject update(@RequestHeader String accessToken, SharePrizeBean sharePrizeBean) {
        return sharePrizeService.update(sharePrizeBean);
    }

    @PostMapping(value = "/delete")
    public SerializeObject delete(@RequestHeader String accessToken, Integer id,Integer type) {
        return sharePrizeService.delete(id,type);
    }

}
