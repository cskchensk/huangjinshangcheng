package cn.ug.operation.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.operation.bean.ShareSettingBean;
import cn.ug.operation.mapper.entity.ShareSettingEntity;

import java.util.List;

public interface ShareSettingService {

    SerializeObject save(List<ShareSettingBean> shareSettingBeanList);

    SerializeObject<ShareSettingBean> queryList();

    SerializeObject update(ShareSettingBean shareSettingBean);

    SerializeObject delete(Integer id);

    SerializeObject queryShareSwitch(Integer type);
}
