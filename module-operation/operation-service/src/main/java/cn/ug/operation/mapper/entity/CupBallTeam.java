package cn.ug.operation.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class CupBallTeam extends BaseEntity implements Serializable {
    private String name;
    private String  icon;
    private String groupOf;
    private int successNum;
    private int failNum;
    private int dogfallNum;
    private int goals;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getGroupOf() {
        return groupOf;
    }

    public void setGroupOf(String groupOf) {
        this.groupOf = groupOf;
    }

    public int getSuccessNum() {
        return successNum;
    }

    public void setSuccessNum(int successNum) {
        this.successNum = successNum;
    }

    public int getFailNum() {
        return failNum;
    }

    public void setFailNum(int failNum) {
        this.failNum = failNum;
    }

    public int getDogfallNum() {
        return dogfallNum;
    }

    public void setDogfallNum(int dogfallNum) {
        this.dogfallNum = dogfallNum;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }
}
