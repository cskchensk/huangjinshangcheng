package cn.ug.operation.service;

import cn.ug.bean.base.SerializeObject;

public interface DrawPrizeService {

    SerializeObject draw(String memberId,String orderNo);

    SerializeObject drawRecord(String memberId,String orderNo);
}
