package cn.ug.operation.service;

import cn.ug.operation.bean.*;
import cn.ug.operation.mapper.entity.Lottery;
import cn.ug.operation.mapper.entity.LotteryPrize;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface LotteryService {
    boolean save(LotteryBean lottery, List<LotteryPrize> prizes);

    List<LotteryActivityBean> query(int status, String startDate, String endDate, int offset, int size);
    int count(int status, String startDate, String endDate);
    LotteryBean getLottery(int id);

    List<LotteryAnalysisBean> queryForAnalysis(int status, String startDate, String endDate, int offset, int size);
    int countForAnalysis(int status, String startDate, String endDate);
    LotteryAnalysisBean getLotteryAnalysis(int lotteryId);
    List<LotteryPrize> selectByLotteryId(int lotteryId);

    boolean deleteByIds(int[] id);
    boolean publish(int[] id);
    boolean toFinish(int[] id);

    List<LotteryStatisticsBean> queryForStatistics(String lotteryName, String prizeName, int offset, int size);
    int countForStatistics(String lotteryName, String prizeName);

    List<PrizeStatisticsBean> queryForStatistics(int lotteryId, String prizeName);
    List<Lottery> queryUnderwayLottery();

    List<MemberActivityBean> queryActivity(String memberId, String startDate, String endDate, int offset, int size);
    int countActivity(String memberId, String startDate, String endDate);
}
