package cn.ug.operation.mapper;

import cn.ug.operation.bean.PrizeStatisticsBean;
import cn.ug.operation.mapper.entity.LotteryPrize;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface LotteryPrizeMapper {
    int insert(LotteryPrize prize);
    int deleteByLotteryId(@Param("lotteryId")int lotteryId);
    List<LotteryPrize> selectByLotteryId(@Param("lotteryId")int lotteryId);
    List<PrizeStatisticsBean> queryForStatistics(Map<String, Object> params);
    int decreaseQuantity(@Param("prizeId")int prizeId);
    int updateProbability(@Param("prizeId")int prizeId, @Param("probability")BigDecimal probability);
}
