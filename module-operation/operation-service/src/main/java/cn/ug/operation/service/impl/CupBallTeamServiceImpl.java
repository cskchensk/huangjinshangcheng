package cn.ug.operation.service.impl;

import cn.ug.operation.mapper.CupBallTeamMapper;
import cn.ug.operation.mapper.entity.CupBallTeam;
import cn.ug.operation.service.CupBallTeamService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CupBallTeamServiceImpl implements CupBallTeamService {
    @Autowired
    private CupBallTeamMapper cupBallTeamMapper;

    @Override
    public boolean save(CupBallTeam team) {
        if (team != null) {
            if (StringUtils.isBlank(team.getId())) {
                team.setId(UF.getRandomUUID());
                team.setAddTime(LocalDateTime.now());
                return cupBallTeamMapper.insert(team) > 0 ? true : false;
            } else {
                team.setModifyTime(LocalDateTime.now());
                return cupBallTeamMapper.update(team) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public CupBallTeam getTeam(String id) {
        if (StringUtils.isNotBlank(id)) {
            return cupBallTeamMapper.findById(id);
        }
        return null;
    }

    @Override
    public List<CupBallTeam> list(int offset, int size) {
        Map<String, Object> params = new HashMap<String,Object>();
        params.put("offset", offset);
        params.put("size", size);
        return cupBallTeamMapper.query(params);
    }

    @Override
    public List<CupBallTeam> list() {
        return cupBallTeamMapper.queryAllForList();
    }

    @Override
    public int count() {
        return cupBallTeamMapper.count();
    }

    @Override
    public boolean increaseSuccessNum(String id, int goals) {
        if (StringUtils.isNotBlank(id)) {
            return cupBallTeamMapper.increaseSuccessNum(id, goals) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean increaseFailNum(String id, int goals) {
        if (StringUtils.isNotBlank(id)) {
            return cupBallTeamMapper.increaseFailNum(id, goals) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean increaseDogfallNum(String id, int goals) {
        if (StringUtils.isNotBlank(id)) {
            return cupBallTeamMapper.increaseDogfallNum(id, goals) > 0 ? true : false;
        }
        return false;
    }
}
