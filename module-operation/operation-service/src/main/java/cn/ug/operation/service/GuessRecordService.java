package cn.ug.operation.service;

import cn.ug.operation.bean.EverydayGuessStatisticsBean;
import cn.ug.operation.bean.GuessMemberBean;
import cn.ug.operation.bean.GuessMemberStatisticsBean;
import cn.ug.operation.bean.MemberGuessRecordBean;
import cn.ug.operation.mapper.entity.GuessJoinMember;
import cn.ug.operation.mapper.entity.GuessRecord;

import java.util.List;

public interface GuessRecordService {
    List<EverydayGuessStatisticsBean> queryForRecord(int activityId, int offset, int size);
    int countForRecord(int activityId);

    List<GuessMemberBean> queryForGuessMembers(int activityId, String guessDate, int guessResult, String memberId, String number, int offset, int size);
    int countForGuessMembers(int activityId, String guessDate, int guessResult, String memberId, String number);

    List<GuessMemberStatisticsBean> queryForGuessMemberStatistics(String name, String mobile, int bindedCard, String startTime, String endTime, int offset, int size);
    int countForGuessMemberStatistics(String name, String mobile, int bindedCard, String startTime, String endTime);

    List<MemberGuessRecordBean> queryForListByMemberId(String memberId, int activityId, int offset, int size);
    int queryForCountByMemberId(String memberId, int activityId);
    List<MemberGuessRecordBean> queryForListRecent30();
    Double selectYestodayFinalPrice();
    double selectTodayFinalPrice();
    MemberGuessRecordBean selectTodayRecord(int activityId, String memberId);
    int selectGotNum(int activityId, String someday);
    boolean saveRecord(GuessRecord record);
    boolean share(GuessRecord record, String couponId, String mobile);
    boolean share(String memberId, int activityId, int endPoint);
    GuessRecord findById(int id);

    boolean updateFinalPrice(int activityId, double finalPrice);
    boolean updateGotRecord(int activityId);
    boolean updateFailedRecord(int activityId);

    GuessJoinMember findJoinInfo(String memberId, int activityId);
}
