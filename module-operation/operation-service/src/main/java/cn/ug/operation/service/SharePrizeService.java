package cn.ug.operation.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.operation.bean.SharePrizeBean;

import java.util.List;

public interface SharePrizeService {

    SerializeObject save(List<SharePrizeBean> sharePrizeBeanList);

    SerializeObject queryList();

    SerializeObject update(SharePrizeBean shareSettingBean);

    /**
     * 删除
     * @param id  商品id/用户组id
     * @param type 1.商品id  2.用户组id
     * @return
     */
    SerializeObject delete(Integer id,Integer type);

}
