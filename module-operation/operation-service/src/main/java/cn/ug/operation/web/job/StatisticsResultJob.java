package cn.ug.operation.web.job;

import cn.ug.config.RedisGlobalLock;
import cn.ug.operation.mapper.entity.CupBallGame;
import cn.ug.operation.service.CupBallGameService;
import cn.ug.operation.service.CupBallTeamService;
import cn.ug.operation.service.CupGuessUserService;
import cn.ug.operation.service.CupJoinUserService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static cn.ug.util.ConstantUtil.COLON;

@Component
public class StatisticsResultJob {
    private Log log = LogFactory.getLog(IncreasingBalanceTask.class);
    @Autowired
    private CupBallGameService cupBallGameService;
    @Resource
    private RedisGlobalLock redisGlobalLock;
    @Autowired
    private CupGuessUserService cupGuessUserService;
    @Autowired
    private CupBallTeamService cupBallTeamService;
    @Autowired
    private CupJoinUserService cupJoinUserService;

    //@Scheduled(cron = "0 0 12 * * ?")
    //@Scheduled(fixedRate = 30*60 * 1000)
    public void gameResult() {
        String key = "StatisticsResultJob:gameResult:" + UF.getFormatDateTime("yyyy-MM-dd", UF.getDateTime());
        if(!redisGlobalLock.lock(key, 1, TimeUnit.DAYS)) {
            return;
        }
        List<CupBallGame> games = cupBallGameService.listPastGames();
        if (games == null || games.size() == 0) {
            return;
        }
        for (CupBallGame game : games) {
            if (StringUtils.isBlank(game.getResult())) {
                continue;
            }
            String[] infos = StringUtils.split(game.getResult(), COLON);
            if (infos != null && infos.length == 2) {
                try {
                    int num1 = Integer.parseInt(infos[0]);
                    int num2 = Integer.parseInt(infos[1]);
                    // 1：A队胜；2：平局；3：B队胜
                    if (num1 > num2) {
                        // 更改竞猜用户信息
                        cupGuessUserService.updateGuessResult(game.getId(), 1);
                        cupGuessUserService.updateMissGuessResult(game.getId(), 1);

                        // 更改球队信息
                        cupBallTeamService.increaseSuccessNum(game.getTeamaId(), num1);
                        cupBallTeamService.increaseFailNum(game.getTeambId(), num2);

                        // 更改参与用户猜中次数
                        cupJoinUserService.increasingGotNum(game.getId(), 1);
                    } else if (num1 == num2) {
                        cupGuessUserService.updateGuessResult(game.getId(), 2);
                        cupGuessUserService.updateMissGuessResult(game.getId(), 2);

                        cupBallTeamService.increaseDogfallNum(game.getTeamaId(), num1);
                        cupBallTeamService.increaseDogfallNum(game.getTeambId(), num2);

                        cupJoinUserService.increasingGotNum(game.getId(), 2);
                    } else {
                        cupGuessUserService.updateGuessResult(game.getId(), 3);
                        cupGuessUserService.updateMissGuessResult(game.getId(), 3);

                        cupBallTeamService.increaseFailNum(game.getTeamaId(), num1);
                        cupBallTeamService.increaseSuccessNum(game.getTeambId(), num2);

                        cupJoinUserService.increasingGotNum(game.getId(), 3);
                    }
                    cupBallGameService.updateStatus(game.getId());
                } catch (Exception e) {
                    continue;
                }
            }
        }
    }
}
