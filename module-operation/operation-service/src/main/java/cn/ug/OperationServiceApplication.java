package cn.ug;

import cn.ug.config.AplicationResourceProperties;
import org.dozer.DozerBeanMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import cn.ug.config.Config;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@MapperScan("cn.ug.operation.mapper")
@EnableConfigurationProperties({AplicationResourceProperties.class})
public class OperationServiceApplication {
	@Autowired
	protected AplicationResourceProperties properties;

	@Bean
	@ConfigurationProperties(prefix = "config")
	public Config config(){
		return new Config();
	}

	@Bean
	public DozerBeanMapper dozerBeanMapper(){
		return new DozerBeanMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(OperationServiceApplication.class, args);
	}
}