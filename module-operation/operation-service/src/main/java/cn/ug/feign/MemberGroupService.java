package cn.ug.feign;

import cn.ug.member.api.MemberGroupServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="MEMBER-SERVICE")
public interface MemberGroupService extends MemberGroupServiceApi {
}
