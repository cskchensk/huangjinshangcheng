package cn.ug.feign;

import cn.ug.activity.api.CouponRepertoryServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="ACTIVITY-SERVICE")
public interface CouponRepertoryService extends CouponRepertoryServiceApi {
}
