package cn.ug.feign;

import cn.ug.pay.api.BankCardServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="PAY-SERVICE")
public interface BankCardService  extends BankCardServiceApi {
}
