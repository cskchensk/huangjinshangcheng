package cn.ug.feign;

import cn.ug.product.api.ProductServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="PRODUCT-SERVICE")
public interface ProductService extends ProductServiceApi {
}
