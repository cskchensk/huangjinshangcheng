package cn.ug.feign;

import cn.ug.analyse.api.MemberAnalyseServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="ANALYSE-SERVICE")
public interface MemberAnalyseService extends MemberAnalyseServiceApi {
}
