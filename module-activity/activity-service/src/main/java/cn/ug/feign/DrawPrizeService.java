package cn.ug.feign;

import cn.ug.operation.api.DrawPrizeServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("OPERATION-SERVICE")
public interface DrawPrizeService extends DrawPrizeServiceApi {

}
