package cn.ug.feign;

import cn.ug.mall.api.RateSettingsServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient("MALL-SERVICE")
public interface RateSettingsService extends RateSettingsServiceApi {
}
