package cn.ug.feign;

import cn.ug.pay.api.MemberAccountServiceApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name="PAY-SERVICE")
public interface MemberAccountService extends MemberAccountServiceApi {

}
