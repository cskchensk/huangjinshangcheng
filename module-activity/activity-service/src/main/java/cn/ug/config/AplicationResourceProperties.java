package cn.ug.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix=AplicationResourceProperties.RESOURCE_PREFIX)
public class AplicationResourceProperties {
    public static final String RESOURCE_PREFIX = "system.resource";
    private int invitationIndate;
    private int invitationBindedCardRewards;
    private int invitationFirstInvestmentRewards;
    private String projectId;
    private String projectSecret;
    private String apiUrl;
    private String organName;
    private String organCode;
    private String legalName;
    private String legalIdcard;
    private String address;
    private String lowboomText;
    private String templateUrl;
    private String accessKey;
    private String secretKey;
    private String bucket;
    private String fileDomain;
    private String busTypeUrl;
    private String sceneTypeUrl;
    private String segTypeUrl;
    private String segpropUrl;
    private String simulateUrl;
    private String appendVoucherUrl;
    private String relateUrl;
    private String viewpageUrl;
    private String voucherUrl;
    private String industry;
    private String scene;
    private String segment;
    //车之鸟渠道ID
    private String carChannelId;
    //车之鸟渠道key
    private String carChannelKey;
    private List<String> couponIds;

    public String getCarChannelId() {
        return carChannelId;
    }

    public void setCarChannelId(String carChannelId) {
        this.carChannelId = carChannelId;
    }

    public String getCarChannelKey() {
        return carChannelKey;
    }

    public void setCarChannelKey(String carChannelKey) {
        this.carChannelKey = carChannelKey;
    }

    public List<String> getCouponIds() {
        return couponIds;
    }

    public void setCouponIds(List<String> couponIds) {
        this.couponIds = couponIds;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getVoucherUrl() {
        return voucherUrl;
    }

    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl;
    }

    public String getBusTypeUrl() {
        return busTypeUrl;
    }

    public void setBusTypeUrl(String busTypeUrl) {
        this.busTypeUrl = busTypeUrl;
    }

    public String getSceneTypeUrl() {
        return sceneTypeUrl;
    }

    public void setSceneTypeUrl(String sceneTypeUrl) {
        this.sceneTypeUrl = sceneTypeUrl;
    }

    public String getSegTypeUrl() {
        return segTypeUrl;
    }

    public void setSegTypeUrl(String segTypeUrl) {
        this.segTypeUrl = segTypeUrl;
    }

    public String getSegpropUrl() {
        return segpropUrl;
    }

    public void setSegpropUrl(String segpropUrl) {
        this.segpropUrl = segpropUrl;
    }

    public String getSimulateUrl() {
        return simulateUrl;
    }

    public void setSimulateUrl(String simulateUrl) {
        this.simulateUrl = simulateUrl;
    }

    public String getAppendVoucherUrl() {
        return appendVoucherUrl;
    }

    public void setAppendVoucherUrl(String appendVoucherUrl) {
        this.appendVoucherUrl = appendVoucherUrl;
    }

    public String getRelateUrl() {
        return relateUrl;
    }

    public void setRelateUrl(String relateUrl) {
        this.relateUrl = relateUrl;
    }

    public String getViewpageUrl() {
        return viewpageUrl;
    }

    public void setViewpageUrl(String viewpageUrl) {
        this.viewpageUrl = viewpageUrl;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getFileDomain() {
        return fileDomain;
    }

    public void setFileDomain(String fileDomain) {
        this.fileDomain = fileDomain;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getTemplateUrl() {
        return templateUrl;
    }

    public void setTemplateUrl(String templateUrl) {
        this.templateUrl = templateUrl;
    }

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }

    public String getOrganCode() {
        return organCode;
    }

    public void setOrganCode(String organCode) {
        this.organCode = organCode;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public String getLegalIdcard() {
        return legalIdcard;
    }

    public void setLegalIdcard(String legalIdcard) {
        this.legalIdcard = legalIdcard;
    }

    public String getLowboomText() {
        return lowboomText;
    }

    public void setLowboomText(String lowboomText) {
        this.lowboomText = lowboomText;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectSecret() {
        return projectSecret;
    }

    public void setProjectSecret(String projectSecret) {
        this.projectSecret = projectSecret;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public static String getResourcePrefix() {
        return RESOURCE_PREFIX;
    }

    public int getInvitationIndate() {
        return invitationIndate;
    }

    public void setInvitationIndate(int invitationIndate) {
        this.invitationIndate = invitationIndate;
    }

    public int getInvitationBindedCardRewards() {
        return invitationBindedCardRewards;
    }

    public void setInvitationBindedCardRewards(int invitationBindedCardRewards) {
        this.invitationBindedCardRewards = invitationBindedCardRewards;
    }

    public int getInvitationFirstInvestmentRewards() {
        return invitationFirstInvestmentRewards;
    }

    public void setInvitationFirstInvestmentRewards(int invitationFirstInvestmentRewards) {
        this.invitationFirstInvestmentRewards = invitationFirstInvestmentRewards;
    }
}