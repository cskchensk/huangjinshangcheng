package cn.ug.activity.service.impl;

import cn.ug.activity.bean.CouponRepertoryBean;
import cn.ug.activity.mapper.CouponMemberMapper;
import cn.ug.activity.mapper.CouponRepertoryMapper;
import cn.ug.activity.mapper.entity.CouponRepertory;
import cn.ug.activity.service.CouponRepertoryService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static cn.ug.config.CacheType.OBJECT;

@Service
public class CouponRepertoryServiceImpl extends BaseServiceImpl implements CouponRepertoryService {
    @Autowired
    private CouponRepertoryMapper couponRepertoryMapper;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    private CouponMemberMapper couponMemberMapper;

    @Override
    public boolean save(CouponRepertory couponRepertory) {
        if (couponRepertory != null) {
            if (couponRepertory.getId() > 0) {
                return couponRepertoryMapper.update(couponRepertory) > 0 ? true : false;
            } else {
                couponRepertory.setAddTime(UF.getFormatDateTime(UF.getDateTime()));
                return couponRepertoryMapper.insert(couponRepertory) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public boolean remove(int id) {
        if (id > 0) {
            return couponRepertoryMapper.delete(String.valueOf(id)) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean removeInBatch(int[] id) {
        if (id != null && id.length > 0) {
            String[] ids = new String[id.length];
            for (int i = 0; i < id.length; i++) {
                ids[i] = String.valueOf(id[i]);
            }
            return couponRepertoryMapper.deleteByIds(ids) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public CouponRepertory get(int id) {
        if (id > 0) {
            return couponRepertoryMapper.findById(String.valueOf(id));
        }
        return null;
    }

    @Override
    public List<CouponRepertory> listRecords(String name, int category, int type, String startAddTime, String endAddTime,
                                             String startIndateTime, String endIndateTime, String order, String sort, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(name)) {
            params.put("name", name);
        }
        params.put("category", category);
        params.put("type", type);
        if (StringUtils.isNotBlank(startAddTime)) {
            params.put("startAddTime", startAddTime);
        }
        if (StringUtils.isNotBlank(endAddTime)) {
            params.put("endAddTime", endAddTime);
        }
        if (StringUtils.isNotBlank(startIndateTime)) {
            params.put("startIndateTime", startIndateTime);
        }
        if (StringUtils.isNotBlank(endIndateTime)) {
            params.put("endIndateTime", endIndateTime);
        }
        List<String> orders = Arrays.asList("add_time", "indate_time");
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return couponRepertoryMapper.query(params);
    }

    @Override
    public List<CouponRepertory> listUsableCoupons(int category, int type, int groupOf) {
        return couponRepertoryMapper.queryUsableCoupons(category, type, groupOf);
    }

    @Override
    public int countRecords(String name, int category, int type, String startAddTime, String endAddTime,
                            String startIndateTime, String endIndateTime) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(name)) {
            params.put("name", name);
        }
        params.put("category", category);
        params.put("type", type);
        if (StringUtils.isNotBlank(startAddTime)) {
            params.put("startAddTime", startAddTime);
        }
        if (StringUtils.isNotBlank(endAddTime)) {
            params.put("endAddTime", endAddTime);
        }
        if (StringUtils.isNotBlank(startIndateTime)) {
            params.put("startIndateTime", startIndateTime);
        }
        if (StringUtils.isNotBlank(endIndateTime)) {
            params.put("endIndateTime", endIndateTime);
        }
        return couponRepertoryMapper.count(params);
    }

    @Override
    public boolean modifyStatus(int[] id, int status) {
        if (id == null || id.length == 0) {
            return false;
        }
        if (status == 0 || status == 1) {
            String[] ids = new String[id.length];
            for (int i = 0; i < id.length; i++) {
                ids[i] = String.valueOf(id[i]);
            }
            return couponRepertoryMapper.updateStatus(ids, status) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<CouponRepertory> findByIds(List<Integer> ids) {
        if (ids != null && ids.size() > 0) {
            return couponRepertoryMapper.findByIds(ids);
        }
        return null;
    }

    @Override
    public List<Integer> listForDayGift() {
        return couponRepertoryMapper.listForDayGift();
    }

    @Override
    public List<CouponRepertory> listUsableCouponsByDay(int category, int type, int day,String memberId) {
        List<CouponRepertory> couponRepertoryList = couponRepertoryMapper.listUsableCouponsByDay(category,type,day);
        if(!CollectionUtils.isEmpty(couponRepertoryList) && StringUtils.isNotBlank(memberId)){
            for (CouponRepertory couponRepertory : couponRepertoryList){
                Integer result = couponMemberMapper.queryCountByMemberIdAndCouponId(memberId,couponRepertory.getId());
                if (result!=null && result>0){
                    couponRepertory.setPullStatus(1);
                }
            }
        }
        return couponRepertoryList;
    }
}