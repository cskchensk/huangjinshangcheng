package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.CouponChannel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouponChannelMapper {
    List<CouponChannel> queryForList(@Param("mobile")String mobile);
    CouponChannel findByOrderNO(@Param("orderNO")String orderNO);
    int insert(CouponChannel couponChannel);
    int deleteByMobile(@Param("mobile")String mobile);
}
