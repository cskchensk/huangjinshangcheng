package cn.ug.activity.service.impl;

import cn.ug.activity.bean.ChannelActivationBean;
import cn.ug.activity.mapper.ChannelActivationMapper;
import cn.ug.activity.service.ChannelActivationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChannelActivationServiceImpl implements ChannelActivationService {
    @Autowired
    private ChannelActivationMapper channelActivationMapper;

    @Override
    public ChannelActivationBean findByUuid(String uuid) {
        if (StringUtils.isNoneBlank(uuid)) {
            return channelActivationMapper.findByUuid(uuid);
        }
        return null;
    }

    @Override
    public boolean save(ChannelActivationBean channelActivationBean) {
        if (channelActivationBean != null) {
            return channelActivationMapper.insert(channelActivationBean) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean updateStatus(String uuid) {
        if (StringUtils.isNotBlank(uuid)) {
            return channelActivationMapper.updateStatus(uuid) > 0 ? true : false;
        }
        return false;
    }
}
