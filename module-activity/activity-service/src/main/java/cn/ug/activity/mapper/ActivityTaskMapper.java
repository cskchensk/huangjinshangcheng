package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ActivityTask;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface ActivityTaskMapper extends BaseMapper<ActivityTask>{

    ActivityTask findActivityTask(@Param("memberId") String memberId,@Param("taskType") Integer taskType);

    int exists(@Param("memberId") String memberId, @Param("taskType") Integer taskType);

    int findTotal(@Param("memberId") String memberId);

}
