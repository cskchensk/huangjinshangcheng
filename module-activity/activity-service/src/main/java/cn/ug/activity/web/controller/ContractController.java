package cn.ug.activity.web.controller;

import cn.ug.activity.bean.ContractBean;
import cn.ug.activity.mapper.entity.Contract;
import cn.ug.activity.service.ContractService;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.AplicationResourceProperties;
import cn.ug.core.SerializeObjectError;
import cn.ug.web.controller.BaseController;
import com.alibaba.fastjson.JSONObject;
import com.qiniu.util.Auth;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ContractController extends BaseController {
    @Autowired
    private ContractService contractService;
    @Autowired
    protected AplicationResourceProperties properties;

    @GetMapping(value = "uptoken")
    public SerializeObject getUploadInfo() {
        Auth auth = Auth.create(properties.getAccessKey(), properties.getSecretKey());
        String upToken = auth.uploadToken(properties.getBucket());
        JSONObject result = new JSONObject();
        result.put("uptoken", upToken);
        return new SerializeObject<>(ResultType.NORMAL, result);
    }

    @GetMapping("contract")
    public SerializeObject<ContractBean> get(String orderId) {
        if(StringUtils.isBlank(orderId)) {
            return new SerializeObjectError("00000002");
        }
        Contract entity = contractService.get(orderId);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObjectError("00000003");
        }
        ContractBean bean = new ContractBean();
        bean.setId(entity.getId());
        bean.setOrderId(entity.getOrderId());
        bean.setOriginalUrl(entity.getOriginalUrl());
        bean.setThirdpartyUrl(entity.getThirdpartyUrl());
        return new SerializeObject<ContractBean>(ResultType.NORMAL, bean);
    }
}
