package cn.ug.activity.service;

import cn.ug.activity.bean.ChannelActivationBean;

public interface ChannelActivationService {
    ChannelActivationBean findByUuid(String uuid);
    boolean save(ChannelActivationBean channelActivationBean);
    boolean updateStatus(String uuid);
}
