package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.Contract;
import org.apache.ibatis.annotations.Param;

public interface ContractMapper {
    int insert(Contract contract);
    Contract findByOrderId(@Param("orderId")String orderId);
}
