package cn.ug.activity.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;
import cn.ug.member.bean.response.MemberUserBean;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberInvestmentRewards extends BaseEntity implements Serializable {
    private String memberId;
    private MemberUserBean friend;
    private String orderId;
    private String billId;
    private int type;
    private double amount;
    private int deleted;
    private int activityInvitationId;
    private BigDecimal investmentAmount;

    public MemberInvestmentRewards() {

    }

    public int getActivityInvitationId() {
        return activityInvitationId;
    }

    public void setActivityInvitationId(int activityInvitationId) {
        this.activityInvitationId = activityInvitationId;
    }

    public BigDecimal getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(BigDecimal investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public MemberUserBean getFriend() {
        return friend;
    }

    public void setFriend(MemberUserBean friend) {
        this.friend = friend;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
