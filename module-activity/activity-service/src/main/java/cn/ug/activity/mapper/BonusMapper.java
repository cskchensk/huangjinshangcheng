package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.Bonus;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BonusMapper extends BaseMapper<Bonus> {
    int count(Map<String, Object> params);
    int updateStatus(@Param("id") String[] id, @Param("status") int status);
    List<Bonus> queryUsableBonus();
}