package cn.ug.activity.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DigestHelper {
    private static Logger LOG = LoggerFactory.getLogger(DigestHelper.class);

    public static String getSignature(String data, String key, String algorithm, String encoding) {
        Mac mac = null;
        try {
            mac = Mac.getInstance(algorithm);
            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(encoding), algorithm);
            mac.init(secretKey);
            mac.update(data.getBytes(encoding));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            LOG.info("获取Signature签名信息异常：" + e.getMessage());
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            LOG.info("获取Signature签名信息异常：" + e.getMessage());
            return null;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            LOG.info("获取Signature签名信息异常：" + e.getMessage());
            return null;
        }
        return byte2hex(mac.doFinal());
    }


    public static String getContentMD5(String filePath) {
        byte[] bytes = getFileMD5Bytes128(filePath);
        return new String(Base64.encodeBase64(bytes));
    }

    public static byte[] getFileMD5Bytes128(String filePath) {
        FileInputStream fis = null;
        byte[] md5Bytes = null;
        try {
            File file = new File(filePath);
            fis = new FileInputStream(file);
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fis.read(buffer, 0, 1024)) != -1) {
                md5.update(buffer, 0, length);
            }
            md5Bytes = md5.digest();
            fis.close();
        } catch (FileNotFoundException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
        }
        return md5Bytes;
    }

    public static String getFileSHA256(String filePath) {
        FileInputStream fis = null;
        byte[] md5Bytes = null;
        String hashSHA256 = null;
        try {
            File file = new File(filePath);
            fis = new FileInputStream(file);
            MessageDigest md5 = MessageDigest.getInstance("SHA-256");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fis.read(buffer, 0, 1024)) != -1) {
                md5.update(buffer, 0, length);
            }
            md5Bytes = md5.digest();
            fis.close();
            hashSHA256 = byte2hex(md5Bytes);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOG.info(e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            LOG.info(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            LOG.info(e.getMessage());
        }
        return hashSHA256;
    }

    public static String getStringSHA256(String str) {
        byte[] md5Bytes = null;
        try {
            md5Bytes = str.getBytes("UTF-8");
            MessageDigest messageDigest;
            messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] inputByteArray = str.getBytes();
            messageDigest.update(inputByteArray);
            md5Bytes = messageDigest.digest();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return byte2hex(md5Bytes);
    }

    public static String byte2hex(byte[] data) {
        StringBuilder hash = new StringBuilder();
        String stmp;
        for (int n = 0; data != null && n < data.length; n++) {
            stmp = Integer.toHexString(data[n] & 0XFF);
            if (stmp.length() == 1)
                hash.append('0');
            hash.append(stmp);
        }
        return hash.toString();
    }
}
