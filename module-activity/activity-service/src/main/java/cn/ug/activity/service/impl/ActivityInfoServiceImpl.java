package cn.ug.activity.service.impl;

import cn.ug.activity.bean.ActivityInfoBean;
import cn.ug.activity.mapper.ActivityInfoMapper;
import cn.ug.activity.mapper.entity.ActivityInfo;
import cn.ug.activity.service.ActivityInfoService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ActivityInfoServiceImpl implements ActivityInfoService {
    @Autowired
    private ActivityInfoMapper activityInfoMapper;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public boolean save(ActivityInfoBean entityBean) {
        if (entityBean != null) {
            ActivityInfo entity = dozerBeanMapper.map(entityBean, ActivityInfo.class);
            if (StringUtils.isBlank(entityBean.getId())) {
                entity.setId(UF.getRandomUUID());
                entity.setAddTime(LocalDateTime.now());
                entity.setStartTime(UF.getDate(entityBean.getStartTimeString()));
                entity.setEndTime(UF.getDate(entityBean.getEndTimeString()));
                return activityInfoMapper.insert(entity) > 0 ? true : false;
            } else {
                entity.setStartTime(UF.getDate(entityBean.getStartTimeString()));
                entity.setEndTime(UF.getDate(entityBean.getEndTimeString()));
                entity.setModifyTime(LocalDateTime.now());
                return activityInfoMapper.update(entity) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public ActivityInfoBean get(String id) {
        if (StringUtils.isNotBlank(id)) {
            ActivityInfo info = activityInfoMapper.findById(id);
            if (info == null) {
                return null;
            }
            ActivityInfoBean entityBean = dozerBeanMapper.map(info, ActivityInfoBean.class);
            if (info.getAddTime() != null) {
                entityBean.setAddTimeString(UF.getFormatDateTime(info.getAddTime()));
            }
            if (info.getModifyTime() != null) {
                entityBean.setModifyTimeString(UF.getFormatDateTime(info.getModifyTime()));
            }
            if (info.getStartTime() != null) {
                entityBean.setStartTimeString(UF.getFormatDateTime(info.getStartTime()));
            }
            if (info.getEndTime() != null) {
                entityBean.setEndTimeString(UF.getFormatDateTime(info.getEndTime()));
            }
            return entityBean;
        }
        return null;
    }

    @Override
    public List<ActivityInfoBean> listRecords(String name, String startDate, String endDate, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        params.put("offset", offset);
        params.put("size", size);
        List<ActivityInfo> list = activityInfoMapper.query(params);
        if (list != null && list.size() > 0) {
            List<ActivityInfoBean> beans = new ArrayList<ActivityInfoBean>();
            list.forEach(info -> {
                ActivityInfoBean entityBean = dozerBeanMapper.map(info, ActivityInfoBean.class);
                if (info.getAddTime() != null) {
                    entityBean.setAddTimeString(UF.getFormatDateTime(info.getAddTime()));
                }
                if (info.getModifyTime() != null) {
                    entityBean.setModifyTimeString(UF.getFormatDateTime(info.getModifyTime()));
                }
                if (info.getStartTime() != null) {
                    entityBean.setStartTimeString(UF.getFormatDateTime(info.getStartTime()));
                }
                if (info.getEndTime() != null) {
                    entityBean.setEndTimeString(UF.getFormatDateTime(info.getEndTime()));
                }
                beans.add(entityBean);
            });
            return beans;
        }
        return null;
    }

    @Override
    public int countRecords(String name, String startDate, String endDate) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(startDate)) {
            params.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            params.put("endDate", endDate);
        }
        return activityInfoMapper.count(params);
    }
}