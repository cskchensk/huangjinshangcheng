package cn.ug.activity.service;

import cn.ug.activity.bean.ChannelBean;
import cn.ug.activity.bean.ChannelStatisticsBean;
import cn.ug.activity.bean.FundsRecordBean;
import cn.ug.activity.mapper.entity.ChannelView;
import cn.ug.activity.mapper.entity.CouponChannel;

import java.util.List;

public interface ChannelService {
    boolean save(ChannelBean entityBean);
    boolean remove(String id);
    boolean removeInBatch(String[] id);
    ChannelBean get(String id);
    List<ChannelBean> listRecords(String name, int offset, int size);
    int countRecords(String name);
    List<ChannelStatisticsBean> listRecords(String startDate, String endDate, int offset, int size);
    List<FundsRecordBean> listFundsRecords(int tradeType, String startDate, String endDate, int offset, int size);
    int countFundsRecords(int tradeType, String startDate, String endDate);

    boolean saveChannelView(ChannelView channelView);

    CouponChannel getCouponChannel(String orderNO);
    boolean save(CouponChannel couponChannel);
}
