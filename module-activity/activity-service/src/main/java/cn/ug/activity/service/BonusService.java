package cn.ug.activity.service;

import cn.ug.activity.bean.BonusBean;
import cn.ug.activity.mapper.entity.BonusStrategy;
import cn.ug.activity.mapper.entity.ProductBonusMapping;

import java.util.List;

public interface BonusService {
    boolean save(BonusBean entityBean);
    boolean remove(String id);
    boolean removeInBatch(String[] id);
    BonusBean get(String id);
    List<BonusBean> listRecords(String name, int type, int offset, int size);
    List<BonusBean> listUsableBonus();
    int countRecords(String name, int type);
    boolean modifyStatus(String[] id, int status);

    ProductBonusMapping findByProductId(String productId);

    List<BonusStrategy> listStrategies(String bonusId);
}
