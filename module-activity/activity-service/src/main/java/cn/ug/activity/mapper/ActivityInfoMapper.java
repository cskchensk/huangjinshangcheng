package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ActivityInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ActivityInfoMapper {
    List<ActivityInfo> query(Map<String, Object> params);
    int count(Map<String, Object> params);
    ActivityInfo findById(@Param("id")String id);
    int insert(ActivityInfo activityInfo);
    int update(ActivityInfo activityInfo);
}
