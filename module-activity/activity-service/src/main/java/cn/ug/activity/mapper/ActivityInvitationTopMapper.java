package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ActivityInvitationTop;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityInvitationTopMapper {
    List<ActivityInvitationTop> queryForList(@Param("activityInvitationId")int activityInvitationId);

    int insertInBatch(@Param("items") List<ActivityInvitationTop> items);
    int delete(@Param("activityInvitationId")int activityInvitationId);

    int countByMobile(@Param("mobile")String mobile);

    List<ActivityInvitationTop> queryTop10(@Param("activityInvitationId")int activityInvitationId,
                                           @Param("startDate")String startDate,
                                           @Param("endDate")String endDate);
}
