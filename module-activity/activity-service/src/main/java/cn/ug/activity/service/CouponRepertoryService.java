package cn.ug.activity.service;


import cn.ug.activity.mapper.entity.CouponRepertory;

import java.util.List;

public interface CouponRepertoryService {
    boolean save(CouponRepertory couponRepertory);
    boolean remove(int id);
    boolean removeInBatch(int[] id);
    CouponRepertory get(int id);
    List<CouponRepertory> listRecords(String name, int category, int type, String startAddTime, String endAddTime,
                                      String startIndateTime, String endIndateTime, String order, String sort,  int offset, int size);
    int countRecords(String name, int category, int type, String startAddTime, String endAddTime,
                     String startIndateTime, String endIndateTime);
    List<CouponRepertory> listUsableCoupons(int category, int type, int groupOf);
    boolean modifyStatus(int[] id, int status);

    List<CouponRepertory> findByIds(List<Integer> ids);

    List<Integer> listForDayGift();

    List<CouponRepertory> listUsableCouponsByDay(int category, int type, int day,String memberId);
}
