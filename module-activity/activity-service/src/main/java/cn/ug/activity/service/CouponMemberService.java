package cn.ug.activity.service;

import cn.ug.activity.bean.*;
import cn.ug.activity.mapper.entity.CouponMember;
import cn.ug.member.bean.MemberDetailBean;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface CouponMemberService {
    boolean save(CouponMember couponMember);
    boolean donate(CouponMember couponMember);
    CouponMember get(String id);
    boolean giveCoupons(String userId, int trigger);
    double giveCoupon(String userId, String couponId);
    boolean receiveCoupon(String memberId, String couponId);
    int countCoupons(String couponId);
    List<CouponMemberBean> listRecords(String memberId, int type);
    CouponMemberBean getCouponMemberBean(String id);
    CouponMemberBean selectById(int couponMemberId);

    List<CouponStatisticsBean> listCouponStatistics(int type, String mobile, String name, int startAccumulationNum, int endAccumulationNum, int startUsedNum, int endUsedNum, String order,
                                                    String sort, int offset, int size,BigDecimal startUsedRate,BigDecimal endUsedRate,BigDecimal startDiscountAwardAmount,BigDecimal endDiscountAwardAmount);
    int countCouponStatistics(int type, String mobile, String name, int startAccumulationNum, int endAccumulationNum, int startUsedNum, int endUsedNum,
                              BigDecimal startUsedRate,BigDecimal endUsedRate,BigDecimal startDiscountAwardAmount,BigDecimal endDiscountAwardAmount);
    List<CouponStatisticsBean> listInterestTicketStatistics(String startDate, String endDate, String mobile, int offset, int size);
    int countInterestTicketStatistics(String startDate, String endDate, String mobile);

    List<CouponMemberItemBean> listCoupons(int type, int status, String memberId, String mobile, String name, String startSendDate, String endSendDate, String startIndateTime,
                                           String endIndateTime, String order, String sort, int offset, int size,
                                           String startUsedDate, String endUedDate, BigDecimal startAmount, BigDecimal endAmount);
    int countCoupons(int type, int status, String memberId, String mobile, String name, String startSendDate, String endSendDate, String startIndateTime, String endIndateTime,
                     String startUsedDate,String endUedDate,BigDecimal startAmount,BigDecimal endAmount);
    boolean save(List<CouponMember> couponMembers);

    List<DonationCouponBean> listFinanceCoupons(int type, String startDate, String endDate, int offset, int size);
    int countFinanceCoupons(int type, String startDate, String endDate);
    List<DonationCouponRecordBean> listFinanceRecordCoupons(String grantTime, int type, BigDecimal startAmount, BigDecimal endAmount, String mobile, String name, int offset, int size);
    int countFinanceRecordCoupons(String grantTime, int type, BigDecimal startAmount, BigDecimal endAmount, String mobile, String name);
    List<MemberDonationCouponBean> listFinanceRecordCoupons(String grantTime, int type, String memberId, String startDate, String endDate, int offset, int size);
    int countFinanceRecordCoupons(String grantTime, int type, String memberId, String startDate, String endDate);

    List<GiveOutBean> listGrantStatistics(int timeType, String startDate, String endDate, List<String> channelIds);
    List<GiveOutAverageValueBean> listAvgValueStatistics(int type, int timeType, String startDate, String endDate, List<String> channelIds);
    List<GiveOutFutureValueBean> listTicketFutureValueStatistics(String startDate, String endDate, List<String> channelIds);

    MemberDetailBean getUsableCouponInfo(String memberId);

    List<CouponMemberBean> queryForCouponList(String memberId, List<Integer> couponIds);

    Map queryCouponStatistics(Map<String, Object> param);
}
