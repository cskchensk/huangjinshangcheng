package cn.ug.activity.web.controller;

import cn.ug.activity.bean.ActivityTaskBean;
import cn.ug.activity.service.ActivityTaskService;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.NoticeParamBean;
import cn.ug.msg.bean.response.NoticeManageBean;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * 活动任务
 * @author  ywl
 */
@RestController
@RequestMapping("activityTask")
public class ActivityTaskController {

    @Resource
    private ActivityTaskService activityTaskService;

    /**
     *查询活动任务节点(是否完成、是否领取、产品id)
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "findActivityTask" , method = GET)
    public SerializeObject<List<ActivityTaskBean>> findActivityTask(@RequestHeader(value = "accessToken", required = false) String accessToken) {
        List<ActivityTaskBean> list = activityTaskService.findActivityTask();
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询活动完成数量和第几个未完成
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "findFinishNumber" , method = GET)
    public SerializeObject<Map<String,Object>> findFinishNumber(@RequestHeader(value = "accessToken", required = false) String accessToken) {
        Map<String,Object>  map = activityTaskService.findFinishNumber();
        return new SerializeObject<>(ResultType.NORMAL, map);
    }

    /**
     * 活动去完成
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "toComplete" , method = GET)
    public SerializeObject toComplete(@RequestHeader String accessToken,Integer taskType) {
        activityTaskService.toComplete(taskType);
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }

    /**
     * 活动去领取
     * @param accessToken	            登录成功后分配的Key
     * @return			                分页数据
     */
    @RequestMapping(value = "toReceive" , method = GET)
    public SerializeObject toReceive(@RequestHeader String accessToken,Integer taskType) {
        activityTaskService.toReceive(taskType);
        return new SerializeObject<>(ResultType.NORMAL, "00000001");
    }
}
