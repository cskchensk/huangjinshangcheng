package cn.ug.activity.mapper;

import cn.ug.activity.bean.ChannelStatisticsBean;
import cn.ug.activity.bean.FundsRecordBean;
import cn.ug.activity.mapper.entity.Channel;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ChannelMapper extends BaseMapper<Channel> {
        int count(Map<String, Object> params);
        List<ChannelStatisticsBean> channelStatistics(Map<String, Object> params);
        List<FundsRecordBean> queryFundsRecordForList(Map<String, Object> params);
        int queryFundsRecordForCount(Map<String, Object> params);
}
