package cn.ug.activity.service;

import cn.ug.activity.bean.GiftBean;
import cn.ug.activity.bean.GiftRecordBean;
import cn.ug.activity.bean.GiftStatisticsBean;
import cn.ug.activity.bean.MemberExchangesBean;
import cn.ug.activity.mapper.entity.ActivityCard;
import cn.ug.activity.mapper.entity.ActivityGift;
import cn.ug.activity.mapper.entity.ActivityGiftRecord;
import cn.ug.bean.base.SerializeObject;

import java.util.List;

public interface ActivityGiftService {
    boolean save(ActivityGift gift, List<ActivityCard> cards);
    boolean saveAfterOnline(ActivityGift gift);
    ActivityGift getGift(int giftId);
    boolean hot(int id, int hot);
    boolean updateStatus(int id, int status, String remark, String auditorId, String auditorName);

    List<ActivityGift> query(int status, int type, String name, String startDate, String endDate, int auditStatus, String order, String sort, int offset, int size);
    int count(int status, int type, String name, String startDate, String endDate, int auditStatus);

    List<ActivityGiftRecord> queryRecord(String memberId, int giftId, String giftName, int giftType, String startDate, String endDate, String order, String sort, int offset, int size);
    int countRecord(String memberId, int giftId, String giftName, int giftType, String startDate, String endDate);

    List<MemberExchangesBean> queryForExchanges(String memberMobile, String memberName, String startDate, String endDate, int startNum, int endNum, String order, String sort, int offset, int size);
    int countForExchanges(String memberMobile, String memberName, String startDate, String endDate, int startNum, int endNum);


    List<GiftStatisticsBean> queryForExchanges(String giftName, int startNum, int endNum, int giftType, int status, String sort, int offset, int size);
    int countForExchanges(String giftName, int startNum, int endNum, int giftType, int status);

    List<GiftBean> queryForHotList();
    int countForHot();
    List<GiftBean> queryForAppList(int giftType, int offset, int size);
    int queryForAppCount(int giftType);
    GiftBean findBeanById(int id);

    List<GiftRecordBean> queryForAppList(String memberId, int offset, int size);
    int queryForAppCount(String memberId);
    GiftRecordBean findByOrderNO(String orderNO);
    String exchange(String memberId, int giftId);
    SerializeObject limit(String memberId, int giftId);
}
