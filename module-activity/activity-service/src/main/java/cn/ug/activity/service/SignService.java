package cn.ug.activity.service;

import cn.ug.activity.bean.SignInfoBean;
import cn.ug.activity.mapper.entity.ActivitySign;
import cn.ug.activity.mapper.entity.ActivitySignHistory;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.SerializeObject;

import java.util.Map;

public interface SignService {

    DataTable<ActivitySign> querySignList(int pageNum, int pageSize, String mobile, String name,
                                          Integer totalSignMin, Integer totalSignMax, Integer totalGoldBeanMin, Integer totalGoldBeanMax, Order order);

    DataTable<ActivitySignHistory> historyList(int pageNum, int pageSize, String mobile, String name,Order order);

    SignInfoBean mySignInfo(String memberId);

    SerializeObject sign(String memberId);
}
