package cn.ug.activity.web.controller;

import cn.ug.activity.bean.*;
import cn.ug.activity.mapper.entity.CouponMember;
import cn.ug.activity.mapper.entity.CouponRepertory;
import cn.ug.activity.service.CouponMemberService;
import cn.ug.activity.service.CouponRepertoryService;
import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.*;
import cn.ug.bean.type.ResultType;
import cn.ug.core.SerializeObjectError;
import cn.ug.core.login.LoginHelper;
import cn.ug.feign.MemberAccountService;
import cn.ug.feign.MemberUserService;
import cn.ug.feign.ProductOrderService;
import cn.ug.feign.ProductService;
import cn.ug.member.bean.MemberDetailBean;
import cn.ug.member.bean.response.MemberUserBean;
import cn.ug.pay.bean.response.MemberAccountBean;
import cn.ug.product.bean.response.ProductFindBean;
import cn.ug.util.BigDecimalUtil;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.math.BigDecimal.ROUND_HALF_UP;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("coupon")
public class CouponRepertoryController extends BaseController {
    @Autowired
    private CouponRepertoryService couponRepertoryService;
    @Autowired
    private CouponMemberService couponMemberService;
    @Autowired
    private MemberUserService memberUserService;
    @Autowired
    private MemberAccountService memberAccountService;
    @Autowired
    private ProductOrderService productOrderService;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/usable/info")
    public SerializeObject<MemberDetailBean> getUsableInfo(String memberId) {
        memberId = UF.toString(memberId);
        return new SerializeObject<MemberDetailBean>(ResultType.NORMAL, couponMemberService.getUsableCouponInfo(memberId));
    }

    @GetMapping(value = "/{id}")
    public SerializeObject<CouponRepertory> get(@PathVariable int id) {
        if (id < 1) {
            return new SerializeObjectError("00000002");
        }
        CouponRepertory entity = couponRepertoryService.get(id);
        if (null == entity) {
            return new SerializeObjectError("00000003");
        }
        if (entity.getIndateType() != 0) {
            entity.setIndateDays(0);
        }
        if (entity.getIndateType() != 1) {
            entity.setIndateTime("");
        }
        return new SerializeObject<CouponRepertory>(ResultType.NORMAL, entity);
    }

    /**
     * 根据id获取红包
     * @param id
     * @return
     */
    @GetMapping(value = "/findCouponById")
    public SerializeObject<CouponRepertoryBean> findCouponById( int id) {
        if (id < 1) {
            return new SerializeObjectError("00000002");
        }
        CouponRepertory entity = couponRepertoryService.get(id);
        if (null == entity) {
            return new SerializeObjectError("00000003");
        }
        if (entity.getIndateType() != 0) {
            entity.setIndateDays(0);
        }
        if (entity.getIndateType() != 1) {
            entity.setIndateTime("");
        }
        CouponRepertoryBean bean = dozerBeanMapper.map(entity,CouponRepertoryBean.class);
        return new SerializeObject<CouponRepertoryBean>(ResultType.NORMAL, bean);
    }


    @GetMapping(value = "/member/coupon")
    public SerializeObject<CouponMemberBean> getMemberCoupon(String couponId) {
        return new SerializeObject<CouponMemberBean>(ResultType.NORMAL, couponMemberService.selectById(Integer.parseInt(couponId)));
    }

    @GetMapping(value = "/rewards")
    public SerializeObject getRewards(@RequestHeader String accessToken, String memberId) {
        SerializeObject<MemberAccountBean> entity = memberAccountService.findByMemberId(memberId);
        if (null == entity || entity.getCode() != ResultType.NORMAL || entity.getData() == null) {
            return new SerializeObjectError("22000008");
        }
        MemberAccountBean bean = entity.getData();
        JSONObject data = new JSONObject();
        data.put("couponAmount", bean.getCouponAmount());
        data.put("interestAmount", bean.getInterestAmount());
        //data.put("couponQuantity", couponMemberService.countRecords(memberId, 1, 1));
        //data.put("interestQuantity", couponMemberService.countRecords(memberId, 1, 2));
        return new SerializeObject<>(ResultType.NORMAL, data);
    }

    /**
     * 后台优惠券保存
     *
     * @param accessToken
     * @param entity
     * @return
     */
    @PostMapping
    public SerializeObject saveCoupon(@RequestHeader String accessToken, CouponRepertory entity) {
        if (null == entity || StringUtils.isBlank(entity.getName())) {
            return new SerializeObjectError("22000001");
        }
        if (entity.getTransactionAmount() < 1 || entity.getDiscountAmount() < 1) {
            return new SerializeObjectError("22000119");
        }
        if (StringUtils.isBlank(entity.getUsedCondition())) {
            return new SerializeObjectError("22000120");
        }

        if (entity.getProductType() == null) {
            return new SerializeObjectError("22000147");
        }
        /*if (StringUtils.isNotBlank(entity.getId())) {
            int num = couponMemberService.countCoupons(entity.getId());
            if (num > 0) {
                return new SerializeObjectError("22000024");
            }
        }*/
        if (couponRepertoryService.save(entity)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PostMapping("/consume")
    public SerializeObject consumeCoupon(String couponId, BigDecimal discountAmount, String orderNO) {
        CouponMember couponMember = couponMemberService.get(couponId);
        if (couponMember == null || couponMember.getCouponId() < 1) {
            return new SerializeObjectError("22000004");
        }
        if (couponMember.getStatus() == 1) {
            return new SerializeObjectError("22000005");
        }
        couponMember.setStatus(1);
        couponMember.setOrderNO(orderNO);
        couponMember.setDiscountAmount(discountAmount);
        if (couponMemberService.save(couponMember)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PutMapping
    public SerializeObject delete(@RequestHeader String accessToken, int[] id) {
        if (null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        if (couponRepertoryService.removeInBatch(id)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PutMapping("/lock")
    public SerializeObject lock(String couponId) {
        CouponMember couponMember = couponMemberService.get(couponId);
        if (couponMember == null || couponMember.getCouponId() < 1) {
            return new SerializeObjectError("22000004");
        }
        if (couponMember.getStatus() != 0) {
            return new SerializeObjectError("22000005");
        }
        couponMember.setStatus(2);
        if (couponMemberService.save(couponMember)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @PutMapping("/unlock")
    public SerializeObject unlock(String couponId) {
        CouponMember couponMember = couponMemberService.get(couponId);
        if (couponMember == null || couponMember.getCouponId() < 0) {
            return new SerializeObjectError("22000004");
        }
        if (couponMember.getStatus() != 2) {
            return new SerializeObjectError("22000023");
        }
        couponMember.setStatus(0);
        if (couponMemberService.save(couponMember)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @GetMapping(value = "/usable/list")
    public SerializeObject getUsableList(BigDecimal transactionAmount, int type, Integer leasebackDays, String productId) {
        String memberId = LoginHelper.getLoginId();
        if (StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("22000008");
        }
        if (transactionAmount == null || transactionAmount.doubleValue() <= 0) {
            return new SerializeObjectError("00000002");
        }
        //todo
        ProductFindBean findBean = null;
        if (StringUtils.isNotBlank(productId)) {
            SerializeObject<ProductFindBean> result = productService.queryProductById(productId);
            if (null == result || result.getCode() != ResultType.NORMAL || result.getData() == null) {
                return new SerializeObjectError("22000008");
            }
            findBean = result.getData();
        }

        leasebackDays = leasebackDays == null ? 0 : leasebackDays;
        List<CouponMemberBean> list = couponMemberService.listRecords(memberId, type);
        List<CouponMemberBean> usablenessCoupons = new ArrayList<CouponMemberBean>();
        if (list != null && list.size() > 0) {
            for (CouponMemberBean bean : list) {
                if (findBean != null) {
                    /**
                     * 适用产品类型 0:全部产品  1:实物金产品  2:安稳金产品
                     */
                    if (bean.getProductType() == 1 && findBean.getType() != 6) {
                        continue;
                    }

                    if (bean.getProductType() == 2 && findBean.getType() != 8) {
                        continue;
                    }
                }
                if (bean.getStatus() == 1 && transactionAmount.doubleValue() >= bean.getTransactionAmount()) {
                    if (type == 1 || type == 0) {
                        if (leasebackDays >= bean.getLeasebackDays()) {
                            usablenessCoupons.add(bean);
                        }
                    } else {
                        usablenessCoupons.add(bean);
                    }
                }
            }

        }
        if (usablenessCoupons != null && usablenessCoupons.size() > 1) {
            Collections.sort(usablenessCoupons, new Comparator<CouponMemberBean>() {
                @Override
                public int compare(CouponMemberBean o1, CouponMemberBean o2) {
                    if (o1.getDiscountAmount() > o2.getDiscountAmount()) {
                        return -1;
                    } else if (o1.getDiscountAmount() == o2.getDiscountAmount()) {
                        return 0;
                    }
                    return 1;
                }
            });
        }
        return new SerializeObject<>(ResultType.NORMAL, usablenessCoupons);
    }

    public BigDecimal calculateIncomeAmount(BigDecimal weight, Integer investDay, BigDecimal yearsIncome) {
        if (BigDecimalUtil.isZeroOrNull(weight) || null == investDay || BigDecimalUtil.isZeroOrNull(yearsIncome)) {
            return BigDecimal.ZERO;
        }
        // 收益计算公式 = 购买克重 * 投资期限 * 年化收益 / 100 / 365

        // *投资期限
        BigDecimal income = weight.multiply(new BigDecimal(investDay));
        // *年化收益/100/365
        income = income.multiply(yearsIncome);
        income = income.divide(new BigDecimal("100"), 10, ROUND_HALF_UP);
        income = income.divide(new BigDecimal("365"), 10, ROUND_HALF_UP);

        return BigDecimalUtil.to5Point(income);
    }

    @RequestMapping(value = "/give/coupons", method = POST)
    public SerializeObject giveCoupons(String userId, int trigger, String mobile) {
        if (StringUtils.isBlank(userId)) {
            return new SerializeObjectError("22000008");
        }
        SerializeObject<MemberUserBean> userBean = memberUserService.findById(userId);
        if (null == userBean || userBean.getCode() != ResultType.NORMAL) {
            return new SerializeObjectError("22000008");
        }
        if (couponMemberService.giveCoupons(userId, trigger)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @RequestMapping(value = "/receive/coupon", method = POST)
    public SerializeObject receiveCoupon(@RequestHeader String accessToken, String couponId) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null || StringUtils.isBlank(loginBean.getAccessToken()) || !StringUtils.equals(accessToken, loginBean.getAccessToken())) {
            return new SerializeObject<>(ResultType.UNLOGIN, "00000102");
        }
        String memberId = LoginHelper.getLoginId();
        if (couponMemberService.receiveCoupon(memberId, couponId)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @RequestMapping(value = "/give/coupon", method = POST)
    public SerializeObject giveCoupon(String userId, String couponId) {
        if (StringUtils.isBlank(userId)) {
            return new SerializeObjectError("22000008");
        }
        SerializeObject<MemberUserBean> userBean = memberUserService.findById(userId);
        if (null == userBean || userBean.getCode() != ResultType.NORMAL || userBean.getData() == null) {
            return new SerializeObjectError("22000008");
        }
        double amount = couponMemberService.giveCoupon(userId, couponId);
        if (amount > 0) {
            return new SerializeObject(ResultType.NORMAL, "00000001", amount);
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @PutMapping(value = "/status")
    public SerializeObject enable(@RequestHeader String accessToken, Integer status, int[] id) {
        status = status == null ? 1 : status;
        if (null == id || id.length <= 0) {
            return new SerializeObjectError("00000002");
        }
        if (couponRepertoryService.modifyStatus(id, status)) {
            return new SerializeObject(ResultType.NORMAL, "00000001");
        } else {
            return new SerializeObject(ResultType.ERROR, "00000005");
        }
    }

    @GetMapping(value = "/list")
    public SerializeObject<DataTable<CouponRepertory>> list(@RequestHeader String accessToken, Page page, Order order, String name, Integer category, Integer type, String startAddTime, String endAddTime,
                                                            String startIndateTime, String endIndateTime) {
        name = UF.toString(name);
        startAddTime = UF.toString(startAddTime);
        endAddTime = UF.toString(endAddTime);
        startIndateTime = UF.toString(startIndateTime);
        endIndateTime = UF.toString(endIndateTime);
        category = category == null ? -1 : category;
        type = type == null ? -1 : type;
        int total = couponRepertoryService.countRecords(name, category, type, startAddTime, endAddTime, startIndateTime, endIndateTime);
        page.setTotal(total);
        if (total > 0) {
            List<CouponRepertory> list = couponRepertoryService.listRecords(name, category, type, startAddTime, endAddTime, startIndateTime, endIndateTime,
                    order.getOrder(), order.getSort(), PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<CouponRepertory>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<CouponRepertory>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<CouponRepertory>()));
    }

    /**
     * 财务对账（每日现金红包总表和每日加息券总表）
     *
     * @param accessToken
     * @param type        类型：1--红包；2--加息券；
     * @param page        分页信息
     * @param startDate   开始时间
     * @param endDate     结束时间0
     * @return
     */
    @RequiresPermissions("activity:coupon:finance")
    @GetMapping(value = "/finance/list")
    public SerializeObject<DataTable<DonationCouponBean>> list(@RequestHeader String accessToken, Integer type, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        type = type == null ? 1 : type;
        int total = couponMemberService.countFinanceCoupons(type, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<DonationCouponBean> list = couponMemberService.listFinanceCoupons(type, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<DonationCouponBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<DonationCouponBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<DonationCouponBean>()));
    }

    @RequiresPermissions("activity:coupon:finance")
    @GetMapping(value = "/finance/record/list")
    public SerializeObject<DataTable<DonationCouponRecordBean>> listRecord(@RequestHeader String accessToken, Integer type, Page page, String grantTime, BigDecimal startAmount, BigDecimal endAmount, String mobile, String name) {
        grantTime = UF.toString(grantTime);
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        type = type == null ? 1 : type;
        startAmount = startAmount == null ? new BigDecimal(-1) : startAmount;
        endAmount = endAmount == null ? new BigDecimal(-1) : endAmount;
        int total = couponMemberService.countFinanceRecordCoupons(grantTime, type, startAmount, endAmount, mobile, name);
        page.setTotal(total);
        if (total > 0) {
            List<DonationCouponRecordBean> list = couponMemberService.listFinanceRecordCoupons(grantTime, type, startAmount, endAmount, mobile, name, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<DonationCouponRecordBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<DonationCouponRecordBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<DonationCouponRecordBean>()));
    }

    @RequiresPermissions("activity:coupon:finance")
    @GetMapping(value = "/finance/member/record/list")
    public SerializeObject<DataTable<MemberDonationCouponBean>> listRecord(@RequestHeader String accessToken, Integer type, Page page, String grantTime, String memberId,
                                                                           String startDate, String endDate) {
        grantTime = UF.toString(grantTime);
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        type = type == null ? 1 : type;
        int total = couponMemberService.countFinanceRecordCoupons(grantTime, type, memberId, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<MemberDonationCouponBean> list = couponMemberService.listFinanceRecordCoupons(grantTime, type, memberId, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<MemberDonationCouponBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<MemberDonationCouponBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<MemberDonationCouponBean>()));
    }

    @GetMapping(value = "/finance/member/record/export")
    public void listRecord(HttpServletResponse response, Integer type, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        type = type == null ? 1 : type;
        List<MemberDonationCouponBean> list = couponMemberService.listFinanceRecordCoupons("", type, "", startDate, endDate, 0, 0);
        if (list != null && list.size() > 0) {
            if (type == 1) {
                String[] columnNames = {"序号", "手机号", "用户姓名", "红包编号", "红包返现金额（元）", "红包到账时间"};
                String[] columns = {"index", "mobile", "name", "couponId", "grantAmount", "grantTime"};
                String fileName = "现金红包详表";
                int index = 1;
                for (MemberDonationCouponBean bean : list) {
                    bean.setIndex(index);
                    index++;
                }
                ExportExcelController<MemberDonationCouponBean> export = new ExportExcelController<MemberDonationCouponBean>();
                export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
            } else if (type == 2) {
                String[] columnNames = {"序号", "手机号", "用户姓名", "加息券编号", "加息券利率%", "加息收益(克)", "加息奖励(元)", "奖励到账时间"};
                String[] columns = {"index", "mobile", "name", "couponId", "amount", "grantGold", "grantAmount", "grantTime"};
                String fileName = "加息券详表";
                int index = 1;
                for (MemberDonationCouponBean bean : list) {
                    bean.setIndex(index);
                    index++;
                }
                ExportExcelController<MemberDonationCouponBean> export = new ExportExcelController<MemberDonationCouponBean>();
                export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
            }
        }
    }

    @GetMapping(value = "/member/list")
    public SerializeObject<JSONObject> listMemberCoupon(@RequestHeader String accessToken, Integer type) {
        type = type == null ? 0 : type;
        String memberId = LoginHelper.getLoginId();
        if (StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000102");
        }
        List<CouponMemberBean> list = couponMemberService.listRecords(memberId, type);
        JSONObject result = new JSONObject();
        if (list != null && list.size() > 0) {
            List<CouponMemberBean> usablenessCoupons = new ArrayList<CouponMemberBean>();
            List<CouponMemberBean> outdatedCoupons = new ArrayList<CouponMemberBean>();
            List<CouponMemberBean> usedCoupons = new ArrayList<CouponMemberBean>();
            for (CouponMemberBean bean : list) {
                if (bean.getStatus() == 1) {
                    usablenessCoupons.add(bean);
                } else if (bean.getStatus() == 2) {
                    usedCoupons.add(bean);
                } else if (bean.getStatus() == 3) {
                    outdatedCoupons.add(bean);
                }
            }
            result.put("usablenessCoupons", usablenessCoupons);
            result.put("outdatedCoupons", outdatedCoupons);
            result.put("usedCoupons", usedCoupons);
        }
        return new SerializeObject<>(ResultType.NORMAL, result);
    }

    /**
     * @param accessToken
     * @param type        类型：0-实物金黄金红包；1-回租福利券；2-商城满减券 3-安稳金黄金红包 如果此字段不传则返回所有类型可用优惠券个数
     * @return
     */
    @GetMapping(value = "/member/num")
    public SerializeObject getMemberCouponNum(@RequestHeader String accessToken, Integer type) {
        type = type == null ? -1 : type;
        String memberId = LoginHelper.getLoginId();
        if (StringUtils.isBlank(memberId)) {
            return new SerializeObjectError("00000102");
        }
        List<CouponMemberBean> list = couponMemberService.listRecords(memberId, type == 3 ? 0 : type);
        int num = 0;
        if (list != null && list.size() > 0) {
            if (type != null && type == 3) {
                List<CouponMemberBean> resultList = list.stream().filter(o -> o.getProductType() == 2).collect(Collectors.toList());
                for (CouponMemberBean bean : resultList) {
                    if (bean.getStatus() == 1) {
                        num++;
                    }
                }
            } else {
                for (CouponMemberBean bean : list) {
                    if (bean.getStatus() == 1) {
                        num++;
                    }
                }
            }
        }


        return new SerializeObject<>(ResultType.NORMAL, num);
    }

    @GetMapping(value = "/display/list")
    public SerializeObject<List<CouponRepertory>> list(@RequestHeader String accessToken, Integer type) {
        type = type == null ? 0 : type;
        List<CouponRepertory> list = couponRepertoryService.listUsableCoupons(1, type, -1);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    @GetMapping(value = "/gift/list")
    public SerializeObject<List<CouponRepertory>> listForGift(@RequestHeader String accessToken, Integer type) {
        type = type == null ? 0 : type;
        List<CouponRepertory> list = couponRepertoryService.listUsableCoupons(2, type, -1);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    @GetMapping(value = "/scene/three/list")
    public SerializeObject<List<SceneThreeCouponBean>> listForCoupon(String memberId) {
        memberId = UF.toString(memberId);
        List<SceneThreeCouponBean> beans = new ArrayList<SceneThreeCouponBean>();
        List<Integer> couponIds = new ArrayList<Integer>();
        List<CouponRepertory> list = couponRepertoryService.listUsableCoupons(3, -1, -1);
        if (list != null && list.size() > 0) {
            for (CouponRepertory bean : list) {
                couponIds.add(bean.getId());
                SceneThreeCouponBean couponBean = new SceneThreeCouponBean();
                couponBean.setCouponId(bean.getId());
                couponBean.setCouponName(bean.getName());
                couponBean.setDiscountAmount(bean.getDiscountAmount());
                couponBean.setTransactionAmount(bean.getTransactionAmount());
                beans.add(couponBean);
            }
            if (StringUtils.isNotBlank(memberId)) {
                List<CouponMemberBean> couponMemberBeans = couponMemberService.queryForCouponList(memberId, couponIds);
                if (couponMemberBeans != null && couponMemberBeans.size() > 0) {
                    for (SceneThreeCouponBean bean : beans) {
                        for (CouponMemberBean couponMemberBean : couponMemberBeans) {
                            if (couponMemberBean.getCouponId() == bean.getCouponId()) {
                                bean.setStatus(1);
                                break;
                            }
                        }
                    }
                }
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, beans);
    }

    @GetMapping(value = "/trigger/list")
    public SerializeObject<List<CouponRepertory>> queryForlist(Integer trigger) {
        trigger = trigger == null ? 0 : trigger;
        List<CouponRepertory> list = couponRepertoryService.listUsableCoupons(0, -1, trigger);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * @param accessToken
     * @param page
     * @param order
     * @param type
     * @param mobile
     * @param name
     * @param startAccumulationNum
     * @param endAccumulationNum
     * @param startUsedNum
     * @param endUsedNum
     * @param startUsedRate            黄金红包使用率范围  最小值
     * @param endUsedRate              黄金红包使用率范围  最大值
     * @param startDiscountAwardAmount 折算奖励金额 最小值
     * @param endDiscountAwardAmount   折算奖励金额 最大值
     * @return
     */
    @GetMapping(value = "/statistics")
    public SerializeObject<DataTable<CouponStatisticsBean>> couponsStatistics(@RequestHeader String accessToken, Page page, Order order, Integer type, String mobile, String name, Integer startAccumulationNum, Integer endAccumulationNum, Integer startUsedNum, Integer endUsedNum,
                                                                              BigDecimal startUsedRate, BigDecimal endUsedRate, BigDecimal startDiscountAwardAmount, BigDecimal endDiscountAwardAmount) {
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        type = type == null ? 0 : type;
        startAccumulationNum = startAccumulationNum == null ? -1 : startAccumulationNum;
        endAccumulationNum = endAccumulationNum == null ? -1 : endAccumulationNum;
        startUsedNum = startUsedNum == null ? -1 : startUsedNum;
        endUsedNum = endUsedNum == null ? -1 : endUsedNum;
        int total = couponMemberService.countCouponStatistics(type, mobile, name, startAccumulationNum, endAccumulationNum, startUsedNum, endUsedNum, startUsedRate, endUsedRate, startDiscountAwardAmount, endDiscountAwardAmount);
        page.setTotal(total);
        if (total > 0) {
            List<CouponStatisticsBean> list = couponMemberService.listCouponStatistics(type, mobile, name, startAccumulationNum, endAccumulationNum, startUsedNum, endUsedNum, order.getOrder(), order.getSort(),
                    PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()), startUsedRate, endUsedRate, startDiscountAwardAmount, endDiscountAwardAmount);
            Map resultMap = new HashMap();
            //总人数
            resultMap.put("count", list.size());
            //累计获得黄金红包总个数
            resultMap.put("totalCouponNum", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getCouponNum)));
            //累计获得黄金红包总奖励（mg）
            resultMap.put("totalAwardAmount", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getTotalAwardAmount)));
            //已使用黄金红包总个数
            resultMap.put("totalUsedNum", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getUsedNum)));
            //已使用黄金红包总克重（mg）
            resultMap.put("totalUsedGram", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getUsedGram)));
            //折算奖励总金额
            resultMap.put("totalDiscountAwardAmount", CollectionUtils.isEmpty(list) ? 0 : list.stream().map(CouponStatisticsBean::getDiscountAwardAmount).reduce(BigDecimal.ZERO, BigDecimal::add));
            //未使用黄金红包总个数
            resultMap.put("totalUsablenessNum", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getUsablenessNum)));
            //已过期黄金红包总个数
            resultMap.put("totalInvalidationNum", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getInvalidationNum)));
            return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<CouponStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list, resultMap));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<CouponStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<CouponStatisticsBean>()));
    }

    @GetMapping(value = "/statistics/export")
    public void queryRecordForExport(HttpServletResponse response, Order order, Integer type, String mobile, String name, Integer startAccumulationNum, Integer endAccumulationNum, Integer startUsedNum, Integer endUsedNum,
                                     BigDecimal startUsedRate, BigDecimal endUsedRate, BigDecimal startDiscountAwardAmount, BigDecimal endDiscountAwardAmount) {
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        type = type == null ? 0 : type;
        startAccumulationNum = startAccumulationNum == null ? -1 : startAccumulationNum;
        endAccumulationNum = endAccumulationNum == null ? -1 : endAccumulationNum;
        startUsedNum = startUsedNum == null ? -1 : startUsedNum;
        endUsedNum = endUsedNum == null ? -1 : endUsedNum;
        List<CouponStatisticsBean> list = couponMemberService.listCouponStatistics(type, mobile, name, startAccumulationNum, endAccumulationNum, startUsedNum, endUsedNum, order.getOrder(), order.getSort(), 0, 0,
                startUsedRate, endUsedRate, startDiscountAwardAmount, endDiscountAwardAmount);
        if (list == null) {
            list = new ArrayList<CouponStatisticsBean>();
        }
        if (type == 0) {
            String fileName = "黄金红包统计表";
            String[] columnNames = {"序号", "手机号", "用户姓名", "累计获得黄金红包个数", "累计获得黄金红包奖励(mg)", "黄金红包使用率", "已使用黄金红包个数", "已使用黄金红包克重(mg)", "折算奖励金额（元)", "未使用黄金红包个数", "已过期黄金红包个数"};
            String[] columns = {"index", "mobile", "name", "couponNum", "totalAwardAmount", "usedRate", "usedNum", "usedGram", "discountAwardAmount", "usablenessNum", "invalidationNum"};
            int index = 1;
            for (CouponStatisticsBean bean : list) {
                bean.setIndex(index);
                index++;
            }
            ExportExcelController<CouponStatisticsBean> export = new ExportExcelController<CouponStatisticsBean>();
            //累计获得黄金红包总个数
            int totalCouponNum = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getCouponNum));
            //累计获得黄金红包总奖励（mg）
            int totalAwardAmount = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getTotalAwardAmount));
            //已使用黄金红包总个数
            int totalUsedNum = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getUsedNum));
            //已使用黄金红包总克重（mg）
            int totalUsedGram = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getUsedGram));
            //折算奖励总金额
            BigDecimal totalDiscountAwardAmount = list.stream().map(CouponStatisticsBean::getDiscountAwardAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            //未使用黄金红包总个数
            int totalUsablenessNum = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getUsablenessNum));
            //已过期黄金红包总个数
            int totalInvalidationNum = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponStatisticsBean::getInvalidationNum));

            String[] firstLines = {"", "合计", list.size() + "人", totalCouponNum + "", totalAwardAmount + "", "", totalUsedNum + "", totalUsedGram + "", totalDiscountAwardAmount + "", totalUsablenessNum + "", totalInvalidationNum + ""};
            export.exportUnlikeExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003, firstLines);
        } else if (type == 1) {
            String fileName = "回租福利券统计表";
            String[] columnNames = {"序号", "手机号", "用户姓名", "累计获得福利券个数", "已使用福利券个数", "未使用福利券个数", "已过期福利券个数"};
            String[] columns = {"index", "mobile", "name", "couponNum", "usedNum", "usablenessNum", "invalidationNum"};
            int index = 1;
            for (CouponStatisticsBean bean : list) {
                bean.setIndex(index);
                index++;
            }
            ExportExcelController<CouponStatisticsBean> export = new ExportExcelController<CouponStatisticsBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        } else if (type == 2) {
            String fileName = "商城满减券统计表";
            String[] columnNames = {"序号", "手机号", "用户姓名", "累计获得满减券个数", "已使用满减券个数", "未使用满减券个数", "已过期满减券个数"};
            String[] columns = {"index", "mobile", "name", "couponNum", "usedNum", "usablenessNum", "invalidationNum"};
            int index = 1;
            for (CouponStatisticsBean bean : list) {
                bean.setIndex(index);
                index++;
            }
            ExportExcelController<CouponStatisticsBean> export = new ExportExcelController<CouponStatisticsBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    @RequiresPermissions("activity:coupon:ticket:statistics")
    @GetMapping(value = "/ticket/statistics")
    public SerializeObject<DataTable<CouponStatisticsBean>> couponsTicketStatistics(@RequestHeader String accessToken, Page page, String startDate, String endDate, String mobile) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        mobile = UF.toString(mobile);
        int total = couponMemberService.countInterestTicketStatistics(startDate, endDate, mobile);
        page.setTotal(total);
        if (total > 0) {
            List<CouponStatisticsBean> list = couponMemberService.listInterestTicketStatistics(startDate, endDate, mobile, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            if (list != null && list.size() > 0) {
                for (CouponStatisticsBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getMobile())) {
                        bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) + "****" + StringUtils.substring(bean.getMobile(), 7));
                    }
                }
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<CouponStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<CouponStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<CouponStatisticsBean>()));
    }

    @GetMapping(value = "/give/records")
    public SerializeObject<DataTable<CouponMemberItemBean>> couponsStatistics(@RequestHeader String accessToken, Page page, Order order, Integer type, Integer status, String memberId, String mobile, String name,
                                                                              String startSendDate, String endSendDate, String startIndateTime, String endIndateTime,
                                                                              String startUsedDate, String endUedDate, BigDecimal startAmount, BigDecimal endAmount) {
        memberId = UF.toString(memberId);
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        startSendDate = UF.toString(startSendDate);
        endSendDate = UF.toString(endSendDate);
        startIndateTime = UF.toString(startIndateTime);
        endIndateTime = UF.toString(endIndateTime);
        type = type == null ? 0 : type;
        status = status == null ? 0 : status;
        int total = couponMemberService.countCoupons(type, status, memberId, mobile, name, startSendDate, endSendDate, startIndateTime, endIndateTime,
                startUsedDate, endUedDate, startAmount, endAmount);
        page.setTotal(total);
        if (total > 0) {
            List<CouponMemberItemBean> list = couponMemberService.listCoupons(type, status, memberId, mobile, name, startSendDate, endSendDate, startIndateTime, endIndateTime, order.getOrder(), order.getSort(), PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()),
                    startUsedDate, endUedDate, startAmount, endAmount);
            if (list != null && list.size() > 0) {
                for (CouponMemberItemBean bean : list) {
                    /*if (bean.getStatus() != 1) {
                        bean.setOrderNO("");
                    }*/
                    /*if (StringUtils.isNotBlank(bean.getMobile())) {
                        bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
                    }*/
                }
            }
            if (type == 0) {

                /*//黄金红包总额度（mg）
                resultMap.put("totalAmount", CollectionUtils.isEmpty(list) ? 0 : list.stream().map(CouponMemberItemBean::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add));
                //折算奖励总金额
                resultMap.put("totalDiscountAmount", CollectionUtils.isEmpty(list) ? 0 : list.stream().map(CouponMemberItemBean::getDiscountAmount).reduce(BigDecimal.ZERO, BigDecimal::add));
                //订单克重
                resultMap.put("totalOrderGram", CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponMemberItemBean::getOrderGram)));
                return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<CouponMemberItemBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list, resultMap));
            } else {*/
                Map<String, Object> param = new HashMap<String, Object>();
                if (StringUtils.isNotBlank(startSendDate)) {
                    param.put("startSendDate", startSendDate);
                }
                if (StringUtils.isNotBlank(endSendDate)) {
                    param.put("endSendDate", endSendDate);
                }
                if (StringUtils.isNotBlank(startIndateTime)) {
                    param.put("startIndateTime", startIndateTime);
                }
                if (StringUtils.isNotBlank(endIndateTime)) {
                    param.put("endIndateTime", endIndateTime);
                }
                if (StringUtils.isNotBlank(memberId)) {
                    param.put("memberId", memberId);
                }
                if (StringUtils.isNotBlank(mobile)) {
                    param.put("mobile", mobile);
                }
                if (StringUtils.isNotBlank(name)) {
                    param.put("name", name);
                }

                if (StringUtils.isNotBlank(startUsedDate)) {
                    param.put("startUsedDate", startUsedDate);
                }

                if (StringUtils.isNotBlank(endUedDate)) {
                    param.put("endUedDate", endUedDate);
                }
                param.put("startAmount", startAmount);
                param.put("endAmount", endAmount);

                param.put("type", type);
                param.put("status", status);
                Map resultMap = couponMemberService.queryCouponStatistics(param);
                return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<CouponMemberItemBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list, resultMap));
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, new DataTable<CouponMemberItemBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<CouponMemberItemBean>()));
    }

    /**
     * 查询不同类型的礼品列表
     *
     * @param accessToken
     * @param type        类型：0-黄金红包；1-回租福利券；2-商城满减券
     * @return
     */
    @GetMapping(value = "/gift/type/list")
    public SerializeObject<List<CouponRepertory>> listForTypeGift(@RequestHeader String accessToken, Integer type, Integer leasebackDays) {
        type = type == null ? 0 : type;
        leasebackDays = leasebackDays == null ? 0 : leasebackDays;
        LoginBean loginBean = LoginHelper.getLoginBean();
        if (loginBean == null) {
            List<CouponRepertory> list = couponRepertoryService.listUsableCouponsByDay(3, type, leasebackDays, null);
            return new SerializeObject<>(ResultType.NORMAL, list);
        } else {
            String memberId = LoginHelper.getLoginId();
            List<CouponRepertory> list = couponRepertoryService.listUsableCouponsByDay(3, type, leasebackDays, memberId);
            return new SerializeObject<>(ResultType.NORMAL, list);
        }
    }

    /**
     * 查询黄金红包-领取后发放的 回租期限的天数列表(去重)
     *
     * @param accessToken
     * @return
     */
    @GetMapping(value = "/gift/day/list")
    public SerializeObject<List<Integer>> listForDayGift(@RequestHeader String accessToken) {
        List<Integer> list = couponRepertoryService.listForDayGift();
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    @GetMapping(value = "/give/records/export")
    public void exportData(HttpServletResponse response, Order order, Integer type, Integer status, String memberId, String mobile, String name,
                           String startSendDate, String endSendDate, String startIndateTime, String endIndateTime,
                           String startUsedDate, String endUedDate, BigDecimal startAmount, BigDecimal endAmount) {
        memberId = UF.toString(memberId);
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        startSendDate = UF.toString(startSendDate);
        endSendDate = UF.toString(endSendDate);
        startIndateTime = UF.toString(startIndateTime);
        endIndateTime = UF.toString(endIndateTime);
        type = type == null ? 0 : type;
        status = status == null ? 0 : status;
        List<CouponMemberItemBean> list = couponMemberService.listCoupons(type, status, memberId, mobile, name, startSendDate, endSendDate, startIndateTime, endIndateTime, order.getOrder(), order.getSort(), 0, 0,
                startUsedDate, endUedDate, startAmount, endAmount);
        String fileName = "";
        if (list == null) {
            list = new ArrayList<CouponMemberItemBean>();
        }
        if (list != null && list.size() > 0) {
            if (type == 0) {
                if (StringUtils.isNotBlank(memberId)) {
                    fileName = "用户黄金红包统计表";
                    String[] columnNames = {"序号", "黄金红包名称", "黄金红包编号", "黄金红包额度（mg）", "折算奖励金额", "对应加息年化(%)", "赠送时间", "到期时间", "状态", "使用时间", "关联订单号", "订单克重（克)", "回租周期（天）"};
                    String[] columns = {"index", "couponName", "id", "amount", "discountAmount", "raiseYearIncome", "sendTime", "indateTime", "statusMark", "usedTime", "orderNO", "orderGram", "leasebackdays"};
                    int index = 1;
                    for (CouponMemberItemBean bean : list) {
                        bean.setIndex(index);
                        index++;
                        if (bean.getStatus() == 1) {
                            bean.setStatusMark("未使用");
                        } else if (bean.getStatus() == 2) {
                            bean.setStatusMark("已使用");
                        } else if (bean.getStatus() == 3) {
                            bean.setStatusMark("已过期");
                        }
                    }
                    ExportExcelController<CouponMemberItemBean> export = new ExportExcelController<CouponMemberItemBean>();
                    export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
                } else {
                    fileName = "赠送黄金红包统计表";
                    String[] columnNames = {"序号", "手机号", "用户姓名", "黄金红包名称", "黄金红包编号", "黄金红包额度", "折算奖励金额(元)", "对应加息年化", "赠送时间", "到期时间", "状态", "使用时间", "关联订单号", "订单克重(克)", "回租周期(天）"};
                    String[] columns = {"index", "mobile", "name", "couponName", "id", "amount", "discountAmount", "raiseYearIncome", "sendTime", "indateTime", "statusMark", "usedTime", "orderNO", "orderGram", "leasebackdays"};
                    int index = 1;
                    for (CouponMemberItemBean bean : list) {
                        bean.setIndex(index);
                        index++;
                        if (bean.getStatus() == 1) {
                            bean.setStatusMark("未使用");
                        } else if (bean.getStatus() == 2) {
                            bean.setStatusMark("已使用");
                        } else if (bean.getStatus() == 3) {
                            bean.setStatusMark("已过期");
                        }
                    }
                    ExportExcelController<CouponMemberItemBean> export = new ExportExcelController<CouponMemberItemBean>();
                    //黄金红包总额度
                    BigDecimal totalAmount = list.stream().map(CouponMemberItemBean::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
                    //折算奖励总金额
                    BigDecimal totalDiscountAmount = CollectionUtils.isEmpty(list) ? BigDecimal.ZERO : list.stream().map(CouponMemberItemBean::getDiscountAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
                    //订单总克重
                    int totalOrderGram = CollectionUtils.isEmpty(list) ? 0 : list.stream().collect(Collectors.summingInt(CouponMemberItemBean::getOrderGram));
                    String[] firstLines = {"", "合计", list.size() + "人", "", list.size() + "个", totalAmount + "mg", totalDiscountAmount + "", "", "", "", "", "", "", totalOrderGram + "", ""};
                    export.exportUnlikeExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003, firstLines);
                }
            } else if (type == 1) {
                if (StringUtils.isNotBlank(memberId)) {
                    fileName = "用户回租福利券统计表";
                    String[] columnNames = {"序号", "回租福利券名称", "回租福利券编号", "回租福利券年化 %", "减时天数（天）", "赠送时间", "到期时间", "状态", "使用时间", "关联订单号"};
                    String[] columns = {"index", "couponName", "id", "yearIncome", "amount", "sendTime", "indateTime", "statusMark", "usedTime", "orderNO"};
                    int index = 1;
                    for (CouponMemberItemBean bean : list) {
                        bean.setIndex(index);
                        index++;
                        if (bean.getStatus() == 1) {
                            bean.setStatusMark("未使用");
                        } else if (bean.getStatus() == 2) {
                            bean.setStatusMark("已使用");
                        } else if (bean.getStatus() == 3) {
                            bean.setStatusMark("已过期");
                        }
                    }
                    ExportExcelController<CouponMemberItemBean> export = new ExportExcelController<CouponMemberItemBean>();
                    export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
                } else {
                    fileName = "赠送回租福利券统计表";
                    String[] columnNames = {"序号", "手机号", "用户姓名", "回租福利券名称", "回租福利券编号", "回租福利券年化 %", "减时天数（天）", "赠送时间", "到期时间", "状态", "使用时间", "关联订单号"};
                    String[] columns = {"index", "mobile", "name", "couponName", "id", "yearIncome", "amount", "sendTime", "indateTime", "statusMark", "usedTime", "orderNO"};
                    int index = 1;
                    for (CouponMemberItemBean bean : list) {
                        bean.setIndex(index);
                        index++;
                        if (bean.getStatus() == 1) {
                            bean.setStatusMark("未使用");
                        } else if (bean.getStatus() == 2) {
                            bean.setStatusMark("已使用");
                        } else if (bean.getStatus() == 3) {
                            bean.setStatusMark("已过期");
                        }
                    }
                    ExportExcelController<CouponMemberItemBean> export = new ExportExcelController<CouponMemberItemBean>();
                    export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
                }
            } else if (type == 2) {
                if (StringUtils.isNotBlank(memberId)) {
                    fileName = "用户满减券统计表";
                    String[] columnNames = {"序号", "用户满减券统计表", "商城满减券编号", "满减券额度（元）", "赠送时间", "到期时间", "状态", "使用时间", "关联订单号"};
                    String[] columns = {"index", "couponName", "id", "amount", "sendTime", "indateTime", "statusMark", "usedTime", "orderNO"};
                    int index = 1;
                    for (CouponMemberItemBean bean : list) {
                        bean.setIndex(index);
                        index++;
                        if (bean.getStatus() == 1) {
                            bean.setStatusMark("未使用");
                        } else if (bean.getStatus() == 2) {
                            bean.setStatusMark("已使用");
                        } else if (bean.getStatus() == 3) {
                            bean.setStatusMark("已过期");
                        }
                    }
                    ExportExcelController<CouponMemberItemBean> export = new ExportExcelController<CouponMemberItemBean>();
                    export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
                } else {
                    fileName = "赠送商城满减券统计表";
                    String[] columnNames = {"序号", "手机号", "用户姓名", "满减券名称", "满减券编号", "满减券额度", "赠送时间", "到期时间", "状态", "使用时间", "关联订单号"};
                    String[] columns = {"index", "mobile", "name", "couponName", "id", "amount", "sendTime", "indateTime", "statusMark", "usedTime", "orderNO"};
                    int index = 1;
                    for (CouponMemberItemBean bean : list) {
                        bean.setIndex(index);
                        index++;
                        if (bean.getStatus() == 1) {
                            bean.setStatusMark("未使用");
                        } else if (bean.getStatus() == 2) {
                            bean.setStatusMark("已使用");
                        } else if (bean.getStatus() == 3) {
                            bean.setStatusMark("已过期");
                        }
                    }
                    ExportExcelController<CouponMemberItemBean> export = new ExportExcelController<CouponMemberItemBean>();
                    export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
                }
            }
        }
    }
}
