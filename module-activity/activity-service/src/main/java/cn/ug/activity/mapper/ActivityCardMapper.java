package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ActivityCard;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityCardMapper {
    ActivityCard findById(@Param("id")int id);
    ActivityCard findSomeoneByGiftId(@Param("giftId")int giftId);
    int insertInBatch(@Param("cards") List<ActivityCard> cards);
    int deleteByGiftId(@Param("giftId")int giftId);
    int updateStatus(@Param("id")int id);
}
