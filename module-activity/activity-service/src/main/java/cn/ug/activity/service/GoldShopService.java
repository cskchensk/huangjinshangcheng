package cn.ug.activity.service;

import cn.ug.activity.bean.GoldShopBean;
import cn.ug.activity.mapper.entity.GoldShop;

import java.util.List;

public interface GoldShopService {
    boolean save(GoldShop shop);

    boolean remove(int id);
    GoldShop get(int id);
    List<GoldShop> listRecords(String name, String province, int status, String startTime, String endTime, String order, String sort, int offset, int size);
    int countRecords(String name, String province, int status, String startTime, String endTime);
    boolean modifyStatus(int id, int status);

    List<GoldShopBean> queryForDisplay();
    GoldShopBean getShop(int id);
}
