package cn.ug.activity.service;

import cn.ug.activity.mapper.entity.CouponDonation;

import java.util.List;

public interface CouponDonationService {
    boolean save(CouponDonation couponDonation);
    CouponDonation get(int id);
    List<CouponDonation> listRecords(String couponName, int status, int type, String startTime, String endTime, int startNum, int endNum, String order, String sort, int offset, int size);
    List<CouponDonation> listRecords(String systemDate);
    int countRecords(String couponName, int status, int type, String startTime, String endTime, int startNum, int endNum);
    boolean modifyStatus(int id, int status);
    boolean audit(CouponDonation couponDonation);
}
