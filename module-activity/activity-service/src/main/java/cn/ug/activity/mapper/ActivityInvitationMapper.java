package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ActivityInvitation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ActivityInvitationMapper {
    List<ActivityInvitation> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    ActivityInvitation findById(@Param("id")int id);
    int insert(ActivityInvitation activityInvitation);
    int update(ActivityInvitation activityInvitation);
    int closeActivity(@Param("id")int id);
    int deleteActivity(@Param("id")int id);

    ActivityInvitation selectValidActivity();
}
