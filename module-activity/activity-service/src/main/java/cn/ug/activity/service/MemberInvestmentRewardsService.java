package cn.ug.activity.service;

import cn.ug.activity.bean.*;
import cn.ug.activity.mapper.entity.MemberInvestmentRewards;

import java.math.BigDecimal;
import java.util.List;

public interface MemberInvestmentRewardsService {
    List<MemberInvestmentRewardsBean> list(String memberId, int offset, int size);
    List<MemberInvestmentRewardsBean> listRecent();
    int count(String memberId);
    BigDecimal sum(String memberId);
    boolean save(MemberInvestmentRewards rewards);
    List<RewardsTopBean> listTopThree();

    List<InviteRewardsStatisticsBean> listRewardsRecords(BigDecimal amountPartOne, BigDecimal amountPartTwo, String startDate, String endDate, int offset, int size);
    int countRewardsRecords(BigDecimal amountPartOne, BigDecimal amountPartTwo, String startDate, String endDate);
    List<InviteRewardsRecordStatisticsBean> listRewardsRecords(String memberId, String rewardsDate, String startDate, String endDate, String mobile, String name, int offset, int size);
    int countRewardsRecords(String memberId, String rewardsDate, String startDate, String endDate, String mobile, String name);
    List<InviteRecordsBean> listInviteRecords(String mobile, String startDate, String endDate, int offset, int size);
    int countInviteRecords(String mobile, String startDate, String endDate);
    InviteRecordsBean getInvitation(String memberId);
    List<RecordInfoBean> getRewardsByMemberId(String memberId);
    int getBindCardNum(String memberId);
    List<InvitationBindedCardBean> listInvitationBindedCardRecords(String memberId, int offset, int size);
    int countInvitationBindedCardRecords(String memberId);

    List<InvitationBean> queryMemberRewards(String memberId, String startDate, String endDate, int offset, int size);
    int countMemberRewards(String memberId, String startDate, String endDate);

    List<InvitationRewardsBean> queryInvitationRecords(String memberId, String invitationDate, int offset, int size);
    int countInvitationRecords(String memberId, String invitationDate);

    List<InviteRewardsRecordFriendBean> listFriendTotalRecords(int offset, int size,String memberId);

    List<InviteFriendAwardRecordsBean> listFriendAwardRecords(int offset, int size,String memberId);

    int countAwardRecords(String memberId);

    int countFriendTotalRecords(String memberId);

    double selectInvestmentAmount(String memberId, int activityInvitationId);
    int countQueryInviteRecords(String mobile, String startDate, String endDate);
}