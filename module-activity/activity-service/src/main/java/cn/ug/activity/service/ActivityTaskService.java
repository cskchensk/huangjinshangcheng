package cn.ug.activity.service;

import cn.ug.activity.bean.ActivityTaskBean;

import java.util.List;
import java.util.Map;

public interface ActivityTaskService {

    /**
     * 查询活动任务节点(是否完成、是否领取、产品id)
     * @return
     */
    List<ActivityTaskBean> findActivityTask();

    /**
     * 查询活动完成数量和第几个未完成
     * @return
     */
    Map<String, Object> findFinishNumber();

    /**
     * 活动去完成
     * @param taskType
     */
    void toComplete(Integer taskType);

    /**
     * 获取去领取
     * @param taskType
     */
    void toReceive(Integer taskType);
}
