package cn.ug.activity.mapper.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ChannelView implements Serializable {
    private int id;
    private String channelId;
    private String ip;
    private LocalDateTime addTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }
}
