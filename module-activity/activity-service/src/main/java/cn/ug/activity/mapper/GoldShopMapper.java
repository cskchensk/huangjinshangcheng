package cn.ug.activity.mapper;

import cn.ug.activity.bean.GoldShopBean;
import cn.ug.activity.mapper.entity.GoldShop;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface GoldShopMapper extends BaseMapper<GoldShop> {
    int count(Map<String, Object> params);
    int updateStatus(@Param("id") int id, @Param("status") int status);
    List<GoldShopBean> queryForDisplay();
}
