package cn.ug.activity.mapper;

import cn.ug.activity.bean.*;
import cn.ug.activity.mapper.entity.MemberInvestmentRewards;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface MemberInvestmentRewardsMapper {
    List<MemberInvestmentRewardsBean> query(Map<String, Object> param);
    List<MemberInvestmentRewardsBean> queryrRecentList();
    int count(Map<String, Object> param);
    BigDecimal sum(Map<String, Object> param);
    int insert(MemberInvestmentRewards rewards);
    List<RewardsTopBean> queryTopThree();

    List<InviteRewardsStatisticsBean> queryRewardsStatistics(Map<String, Object> param);
    Integer countRewardsStatistics(Map<String, Object> param);
    List<InviteRewardsRecordStatisticsBean> queryRewardsRecordsStatistics(Map<String, Object> param);
    int countRewardsRecordsStatistics(Map<String, Object> param);
    List<InviteRecordsBean> queryInviteRecords(Map<String, Object> param);
    int countInviteRecords(Map<String, Object> param);
    InviteRecordsBean selectInvitationByMemberId(@Param("memberId")String memberId);
    List<RecordInfoBean> selectRewardsByMemberId(@Param("memberId")String memberId);
    int selectBindCardNum(@Param("memberId")String memberId);
    List<InvitationBindedCardBean> queryInvitationBindedCardRecords(Map<String, Object> param);
    int countInvitationBindedCardRecords(Map<String, Object> param);

    List<GiveOutBean> listRewardsGrantStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                 @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<GiveOutAverageValueBean> listRewardsAvgValueStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                 @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);

    List<InvitationBean> queryMemberRewards(Map<String, Object> params);
    int countMemberRewards(Map<String, Object> params);

    List<InvitationRewardsBean> queryInvitationRecords(Map<String, Object> params);
    int countInvitationRecords(Map<String, Object> params);

    Double selectFriendTradeMoney(@Param("memberId")String memberId);

    List<InviteRewardsRecordFriendBean> listFriendTotalRecords(Map<String, Object> params);

    List<InviteFriendAwardRecordsBean> listFriendAwardRecords(Map<String, Object> params);

    int countAwardRecords(@Param("memberId")String memberId);

    int countFriendTotalRecords(@Param("memberId")String memberId);

    Map<String,Double> countAmount(Map<String, Object> param);

    double selectInvestmentAmount(@Param("memberId")String memberId, @Param("activityInvitationId")int activityInvitationId);
    int countQueryInviteRecords(Map<String, Object> param);

    Double alreadyTotalAmount(Map<String, Object> param);

}