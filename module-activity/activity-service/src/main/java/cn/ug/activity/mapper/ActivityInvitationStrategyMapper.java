package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ActivityInvitationStrategy;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityInvitationStrategyMapper {
    List<ActivityInvitationStrategy> queryForList(@Param("activityInvitationId")int activityInvitationId);
    List<ActivityInvitationStrategy> queryForLatestList();

    int insertInBatch(@Param("strategies") List<ActivityInvitationStrategy> strategies);
    int delete(@Param("activityInvitationId")int activityInvitationId);
}
