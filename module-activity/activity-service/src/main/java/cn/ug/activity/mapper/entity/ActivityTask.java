package cn.ug.activity.mapper.entity;

import java.io.Serializable;

/**
 * 活动任务完成
 */
public class ActivityTask implements Serializable{

    /** id **/
    private String id;
    /** 会员id **/
    private String memberId;
    /** 任务类型 1:了解柚子金 2:认证绑卡 3:7天活动产品 4:预订稳赢金产品 **/
    private Integer taskType;
    /** 是否完成 1:未完成 2:已完成 **/
    private Integer isFinish;
    /** 是否领取 1:未领取 2:已领取 **/
    private Integer isReceive;
    /** 产品id **/
    private String productId;

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public Integer getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Integer isFinish) {
        this.isFinish = isFinish;
    }

    public Integer getIsReceive() {
        return isReceive;
    }

    public void setIsReceive(Integer isReceive) {
        this.isReceive = isReceive;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
