package cn.ug.activity.service.impl;

import cn.ug.activity.bean.GoldShopBean;
import cn.ug.activity.bean.GoldShopServicesBean;
import cn.ug.activity.mapper.GoldShopMapper;
import cn.ug.activity.mapper.GoldShopServicesMapper;
import cn.ug.activity.mapper.entity.GoldShop;
import cn.ug.activity.mapper.entity.GoldShopServices;
import cn.ug.activity.service.GoldShopService;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoldShopServiceImpl implements GoldShopService {
    @Autowired
    private GoldShopMapper goldShopMapper;
    @Autowired
    private GoldShopServicesMapper goldShopServicesMapper;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean save(GoldShop shop) {
        if (shop != null) {
            if (shop.getId() > 0) {
                boolean result = goldShopMapper.update(shop) > 0 ? true : false;
                if (result && shop.getServices() != null && shop.getServices().size() > 0) {
                    goldShopServicesMapper.deleteByShopId(shop.getId());
                    shop.getServices().forEach(service -> {
                        service.setShopId(shop.getId());
                    });
                    int rows = goldShopServicesMapper.insertInBatch(shop.getServices());
                    Ensure.that(rows).isLt(1, "00000005");
                }
            } else {
                shop.setAddTime(UF.getFormatDateTime(LocalDateTime.now()));
                boolean result = goldShopMapper.insert(shop) > 0 ? true : false;
                if (result && shop.getServices() != null && shop.getServices().size() > 0) {
                    shop.getServices().forEach(service -> {
                        service.setShopId(shop.getId());
                    });
                    int rows = goldShopServicesMapper.insertInBatch(shop.getServices());
                    Ensure.that(rows).isLt(1, "00000005");
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(int id) {
        if (id > 0) {
            return goldShopMapper.delete(String.valueOf(id)) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public GoldShop get(int id) {
        if (id > 0) {
            GoldShop shop = goldShopMapper.findById(String.valueOf(id));
            if (shop != null) {
                shop.setServices(goldShopServicesMapper.findByShopId(shop.getId()));
            }
            return shop;
        }
        return null;
    }

    @Override
    public List<GoldShop> listRecords(String name, String province, int status, String startTime, String endTime, String order, String sort, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(province)) {
            params.put("province", province);
        }
        params.put("status", status);
        if (StringUtils.isNotBlank(startTime)) {
            params.put("startTime", startTime);
        }
        if (StringUtils.isNotBlank(endTime)) {
            params.put("endTime", endTime);
        }
        List<String> orders = Arrays.asList("add_time");
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return goldShopMapper.query(params);
    }

    @Override
    public int countRecords(String name, String province, int status, String startTime, String endTime) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name);
        }
        if (StringUtils.isNotBlank(province)) {
            params.put("province", province);
        }
        params.put("status", status);
        if (StringUtils.isNotBlank(startTime)) {
            params.put("startTime", startTime);
        }
        if (StringUtils.isNotBlank(endTime)) {
            params.put("endTime", endTime);
        }
        List<String> orders = Arrays.asList("add_time");
        return goldShopMapper.count(params);
    }

    @Override
    public boolean modifyStatus(int id, int status) {
        if (id > 0 && (status == 0 || status == 1)) {
            return goldShopMapper.updateStatus(id, status) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<GoldShopBean> queryForDisplay() {
        return goldShopMapper.queryForDisplay();
    }

    @Override
    public GoldShopBean getShop(int id) {
        if (id > 0) {
            GoldShop shop = goldShopMapper.findById(String.valueOf(id));
            if (shop != null) {
                GoldShopBean shopBean = dozerBeanMapper.map(shop, GoldShopBean.class);
                List<GoldShopServices> services = goldShopServicesMapper.findByShopId(shop.getId());
                if (services != null && services.size() > 0) {
                    List<GoldShopServicesBean> servicesBeans = dozerBeanMapper.map(services, List.class);
                    shopBean.setServices(servicesBeans);
                }
                return shopBean;
            }
        }
        return null;
    }
}
