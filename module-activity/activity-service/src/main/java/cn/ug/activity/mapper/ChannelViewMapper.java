package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ChannelView;

public interface ChannelViewMapper {
    int insert(ChannelView channelView);
}
