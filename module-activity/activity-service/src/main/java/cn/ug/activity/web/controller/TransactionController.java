package cn.ug.activity.web.controller;

import cn.ug.activity.web.utils.ExcelUtil;
import cn.ug.analyse.bean.response.ActiveMemberAnalyseBean;
import cn.ug.analyse.bean.response.MemberAnalyseBean;
import cn.ug.bean.base.*;
import cn.ug.bean.type.ResultType;
import cn.ug.config.AplicationResourceProperties;
import cn.ug.core.SerializeObjectError;
import cn.ug.feign.MemberAccountService;
import cn.ug.feign.MemberAnalyseService;
import cn.ug.feign.MemberUserService;
import cn.ug.feign.ProductOrderService;
import cn.ug.pay.bean.*;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.ExportExcelController;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static cn.ug.util.ConstantUtil.*;

@RestController
@RequestMapping("transaction")
public class TransactionController {
    @Autowired
    private ProductOrderService productOrderService;
    @Autowired
    protected AplicationResourceProperties properties;
    @Autowired
    private MemberAccountService memberAccountService;
    @Autowired
    private MemberAnalyseService memberAnalyseService;
    @Autowired
    private MemberUserService memberUserService;

    private static Log log = LogFactory.getLog(TransactionController.class);

    @GetMapping(value = "/exchange/gold/amount")
    public SerializeObject<DataTable<OpenPositionTotalBean>> listExchangeGoldRecord(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate, Integer searchType) {
        timeType = timeType == null ? 1 : timeType;
        searchType = searchType == null ? 1 : searchType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            if (timeType == 1) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 2) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 3) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                int week = calendar.get(Calendar.DAY_OF_WEEK);
                int days = 42;
                week--;
                if (week == 0) {
                    week = 7;
                }
                week--;
                days += week;
                calendar.add(Calendar.DAY_OF_YEAR, -days);
                startDate = dateFormat.format(calendar.getTime());
            }
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listExpiredAmountStatistics(timeType, startDate, endDate, searchType);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, startDate + ":" + endDate, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, startDate + ":" + endDate, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<OpenPositionTotalBean>()));
        }
    }

    @GetMapping(value = "/exchange/gold/amount/export")
    public SerializeObject exportExchangeGoldRecord(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate, Integer searchType) {
        timeType = timeType == null ? 1 : timeType;
        searchType = searchType == null ? 1 : searchType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            if (timeType == 1) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 2) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 3) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                int week = calendar.get(Calendar.DAY_OF_WEEK);
                int days = 42;
                week--;
                if (week == 0) {
                    week = 7;
                }
                week--;
                days += week;
                calendar.add(Calendar.DAY_OF_YEAR, -days);
                startDate = dateFormat.format(calendar.getTime());
            }
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listExpiredAmountStatistics(timeType, startDate, endDate, searchType);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (OpenPositionTotalBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, 4 + timeType, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/gold/record")
    public SerializeObject<DataTable<OpenPositionTotalBean>> listGoldRecord(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate) {
        timeType = timeType == null ? 1 : timeType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            if (timeType == 1) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 2) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 3) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                int week = calendar.get(Calendar.DAY_OF_WEEK);
                int days = 42;
                week--;
                if (week == 0) {
                    week = 7;
                }
                week--;
                days += week;
                calendar.add(Calendar.DAY_OF_YEAR, -days);
                startDate = dateFormat.format(calendar.getTime());
            }
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listGoldRecord(timeType, startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, startDate + ":" + endDate, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, startDate + ":" + endDate, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<OpenPositionTotalBean>()));
        }
    }

    @GetMapping(value = "/gold/record/export")
    public SerializeObject exportGoldRecord(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate) {
        timeType = timeType == null ? 1 : timeType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            if (timeType == 1) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 2) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 3) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                int week = calendar.get(Calendar.DAY_OF_WEEK);
                int days = 42;
                week--;
                if (week == 0) {
                    week = 7;
                }
                week--;
                days += week;
                calendar.add(Calendar.DAY_OF_YEAR, -days);
                startDate = dateFormat.format(calendar.getTime());
            }
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listGoldRecord(timeType, startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (OpenPositionTotalBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, 3, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/money/pay")
    public SerializeObject<DataTable<OpenPositionTotalBean>> listMoneyPay(@RequestHeader String accessToken, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listAvgOpenPosition(1, startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<OpenPositionTotalBean>()));
        }
    }

    @GetMapping(value = "/money/pay/export")
    public SerializeObject exportMoneyPay(@RequestHeader String accessToken, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listAvgOpenPosition(1, startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (OpenPositionTotalBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, 1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/total/expired")
    public SerializeObject<DataTable<OpenPositionTotalBean>> listGoldPay(@RequestHeader String accessToken, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listTotalExpired(startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<OpenPositionTotalBean>()));
        }
    }

    @GetMapping(value = "/total/expired/export")
    public SerializeObject exportGoldPay(@RequestHeader String accessToken, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listTotalExpired(startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (OpenPositionTotalBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, 2, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/total/open/position")
    public SerializeObject<DataTable<OpenPositionTotalBean>> listTotalBean(@RequestHeader String accessToken, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listTotalOpenPosition(startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionTotalBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<OpenPositionTotalBean>()));
        }
    }

    @GetMapping(value = "/avg/open/position")
    public SerializeObject<DataTable<OpenPositionPastBean>> listPastBean(@RequestHeader String accessToken, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionPastBean>> data = productOrderService.listAvgOpenPosition(startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionPastBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionPastBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<OpenPositionPastBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<OpenPositionPastBean>()));
        }
    }

    @GetMapping(value = "/total/open/position/export")
    public SerializeObject exportTotalBean(@RequestHeader String accessToken, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionTotalBean>> data = productOrderService.listTotalOpenPosition(startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionTotalBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (OpenPositionTotalBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, -1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/avg/open/position/export")
    public SerializeObject exportAvgBean(@RequestHeader String accessToken, Page page, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<OpenPositionPastBean>> data = productOrderService.listAvgOpenPosition(startDate, endDate);
        if (data != null && data.getData() != null) {
            List<OpenPositionPastBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (OpenPositionPastBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, -1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/new/user/export")
    public SerializeObject exportNewUserList(@RequestHeader String accessToken, String startTime, String endTime, String[] channelIds) {
        startTime = UF.toString(startTime);
        endTime = UF.toString(endTime);
        SerializeObject<List<MemberAnalyseBean>> data = memberAnalyseService.listNewUser(startTime, endTime, channelIds);
        if (data != null && data.getData() != null) {
            List<MemberAnalyseBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (MemberAnalyseBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, -1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/active/user/export")
    public SerializeObject exportActiveUserList(@RequestHeader String accessToken, Integer dayType, String startTime, String endTime, String[] channelIds) {
        dayType = dayType == null ? 1 : dayType;
        startTime = UF.toString(startTime);
        endTime = UF.toString(endTime);
        SerializeObject<List<ActiveMemberAnalyseBean>> data = memberAnalyseService.listActiveUser(dayType, startTime, endTime, channelIds);
        if (data != null && data.getData() != null) {
            List<ActiveMemberAnalyseBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (ActiveMemberAnalyseBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, -1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/total/list")
    public SerializeObject<DataTable<TradeTotalAmountBean>> list(@RequestHeader String accessToken, Integer pageNum, Integer pageSize, Integer timeType, String startDate, String endDate, String[] channelIds, Order order) {
        //默认按月查询 2019-10-16需求,要求进入页面默认查所有数据
        timeType = timeType == null ? 2 : timeType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        String now = dateFormat.format(calendar.getTime());
        if (StringUtils.isBlank(endDate)) {
            endDate = now;
        }
        if (null == pageNum || pageNum <= 0){
            pageNum = 1;
        }
        if (null == pageSize || pageSize <= 0) {
            pageSize = 10;
        }

//        if (StringUtils.isBlank(startDate)) {
//            if (timeType == 1) {
//                calendar.add(Calendar.DAY_OF_YEAR, -6);
//                startDate = dateFormat.format(calendar.getTime());
//            } else if (timeType == 2) {
////                calendar.add(Calendar.MONTH, -1);
////                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//                startDate = "";
//            } else if (timeType == 3) {
//                int week = calendar.get(Calendar.DAY_OF_WEEK);
//                int days = 42;
//                week--;
//                if (week == 0) {
//                    week = 7;
//                }
//                week--;
//                days += week;
//                calendar.add(Calendar.DAY_OF_YEAR, -days);
//                startDate = dateFormat.format(calendar.getTime());
//            }
//        }

        if (StringUtils.isBlank(startDate)) {
            SerializeObject<Date> data = memberUserService.findMinAddTime();
            startDate = dateFormat.format(data.getData());
        }

        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<TradeTotalAmountBean>> data = productOrderService.listTotal(timeType, startDate, endDate, channelIds);
        if (data != null && data.getData() != null) {
            List<TradeTotalAmountBean> beans = data.getData();

            if (order.getOrder().equalsIgnoreCase("tradeAmount")) {
                if (order.getSort().equalsIgnoreCase("desc")) {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getTradeAmount));
                } else {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getTradeAmount).reversed());
                }
            }

            if (order.getOrder().equalsIgnoreCase("regularlyTradeAmount")) {
                if (order.getSort().equalsIgnoreCase("desc")) {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getRegularlyTradeAmount));
                } else {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getRegularlyTradeAmount).reversed());
                }
            }

            if (order.getOrder().equalsIgnoreCase("tbillGram")) {
                if (order.getSort().equalsIgnoreCase("desc")) {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getTbillGram));
                } else {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getTbillGram).reversed());
                }
            }

            if (order.getOrder().equalsIgnoreCase("rechargeAmount")) {
                if (order.getSort().equalsIgnoreCase("desc")) {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getRechargeAmount));
                } else {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getRechargeAmount).reversed());
                }
            }

            if (order.getOrder().equalsIgnoreCase("withdrawAmount")) {
                if (order.getSort().equalsIgnoreCase("desc")) {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getWithdrawAmount));
                } else {
                    Collections.sort(beans, Comparator.comparing(TradeTotalAmountBean::getWithdrawAmount).reversed());
                }
            }

            //page.setTotal(beans.size());
            int total = beans.size();
            if (pageSize > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(pageNum) - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TradeTotalAmountBean>(pageNum, pageSize, total, beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TradeTotalAmountBean>(pageNum, pageSize, 0, new ArrayList<TradeTotalAmountBean>()));
        }
    }

    @GetMapping(value = "/total/export")
    public SerializeObject exportTotalList(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate, String[] channelIds) {
        timeType = timeType == null ? 2 : timeType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        String now = dateFormat.format(calendar.getTime());
        if (StringUtils.isBlank(endDate)) {
            endDate = now;
        }

//        if (StringUtils.isBlank(startDate)) {
//            if (timeType == 1) {
//                calendar.add(Calendar.DAY_OF_YEAR, -6);
//                startDate = dateFormat.format(calendar.getTime());
//            } else if (timeType == 2) {
////                calendar.add(Calendar.MONTH, -1);
////                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//                startDate = "";
//            } else if (timeType == 3) {
//                int week = calendar.get(Calendar.DAY_OF_WEEK);
//                int days = 42;
//                week--;
//                if (week == 0) {
//                    week = 7;
//                }
//                week--;
//                days += week;
//                calendar.add(Calendar.DAY_OF_YEAR, -days);
//                startDate = dateFormat.format(calendar.getTime());
//            }
//        }

        if (StringUtils.isBlank(startDate)) {
            SerializeObject<Date> data = memberUserService.findMinAddTime();
            startDate = dateFormat.format(data.getData());
        }

        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        List<String> channels = null;
        if (channelIds != null && channelIds.length > 0) {
            channels = new ArrayList<String>();
            for (String channelId : channelIds) {
                channels.add(channelId);
            }
        }
        SerializeObject<List<TradeTotalAmountBean>> data = productOrderService.listTotal(timeType, startDate, endDate, channelIds);
        if (data != null && data.getData() != null) {
            List<TradeTotalAmountBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (TradeTotalAmountBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, 0, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/avg/list")
    public SerializeObject<DataTable<TradeAverageValueBean>> listAvg(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate, String[] channelIds) {
        timeType = timeType == null ? 1 : timeType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            if (timeType == 1) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 2) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 3) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                int week = calendar.get(Calendar.DAY_OF_WEEK);
                int days = 42;
                week--;
                if (week == 0) {
                    week = 7;
                }
                week--;
                days += week;
                calendar.add(Calendar.DAY_OF_YEAR, -days);
                startDate = dateFormat.format(calendar.getTime());
            }
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }

        SerializeObject<List<TradeAverageValueBean>> data = productOrderService.listAvg(timeType, startDate, endDate, channelIds);
        if (data != null && data.getData() != null) {
            List<TradeAverageValueBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TradeAverageValueBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TradeAverageValueBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<TradeAverageValueBean>()));
        }
    }

    @GetMapping(value = "/avg/export")
    public SerializeObject exportAvgList(@RequestHeader String accessToken, Page page, Integer timeType, String startDate, String endDate, String[] channelIds) {
        timeType = timeType == null ? 1 : timeType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            if (timeType == 1) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DAY_OF_YEAR, -6);
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 2) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                startDate = dateFormat.format(calendar.getTime());
            } else if (timeType == 3) {
                Calendar calendar = Calendar.getInstance();
                endDate = dateFormat.format(calendar.getTime());
                int week = calendar.get(Calendar.DAY_OF_WEEK);
                int days = 42;
                week--;
                if (week == 0) {
                    week = 7;
                }
                week--;
                days += week;
                calendar.add(Calendar.DAY_OF_YEAR, -days);
                startDate = dateFormat.format(calendar.getTime());
            }
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }
        List<String> channels = null;
        if (channelIds != null && channelIds.length > 0) {
            channels = new ArrayList<String>();
            for (String channelId : channelIds) {
                channels.add(channelId);
            }
        }
        SerializeObject<List<TradeAverageValueBean>> data = productOrderService.listAvg(timeType, startDate, endDate, channelIds);
        if (data != null && data.getData() != null) {
            List<TradeAverageValueBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (TradeAverageValueBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, -1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/future/list")
    public SerializeObject<DataTable<TradeFutureValueBean>> futureFutrueList(@RequestHeader String accessToken, Page page, String startDate, String endDate, String[] channelIds) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }
        SerializeObject<List<TradeFutureValueBean>> data = productOrderService.listFuture(startDate, endDate, channelIds);
        if (data != null && data.getData() != null) {
            List<TradeFutureValueBean> beans = data.getData();
            page.setTotal(beans.size());
            if (page.getPageSize() > 0) {
                beans = beans.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
            }
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TradeFutureValueBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), beans));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataTable<TradeFutureValueBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<TradeFutureValueBean>()));
        }
    }

    @GetMapping(value = "/future/export")
    public SerializeObject exportFutureFutrueList(@RequestHeader String accessToken, String startDate, String endDate, String[] channelIds) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        if (StringUtils.isBlank(startDate) && StringUtils.isBlank(endDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
            Calendar calendar = Calendar.getInstance();
            endDate = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DAY_OF_YEAR, -6);
            startDate = dateFormat.format(calendar.getTime());
        }
        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }
        List<String> channels = null;
        if (channelIds != null && channelIds.length > 0) {
            channels = new ArrayList<String>();
            for (String channelId : channelIds) {
                channels.add(channelId);
            }
        }
        SerializeObject<List<TradeFutureValueBean>> data = productOrderService.listFuture(startDate, endDate, channelIds);
        if (data != null && data.getData() != null) {
            List<TradeFutureValueBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (TradeFutureValueBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, 1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/k/line")
    public SerializeObject getKLine(@RequestHeader String accessToken, String searchDate) {
        searchDate = UF.toString(searchDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
        if (StringUtils.isBlank(searchDate)) {
            searchDate = dateFormat.format(Calendar.getInstance().getTime());
        }
        String temp = searchDate;
        try {
            int searchValue = Integer.parseInt(StringUtils.replaceAll(searchDate, MINUS, BLANK));
            int currentValue = Integer.parseInt(StringUtils.replaceAll(dateFormat.format(Calendar.getInstance().getTime()), MINUS, BLANK));
            if (searchValue > currentValue) {
                return new SerializeObjectError("22000027");
            }
        } catch (Exception e) {
            return new SerializeObjectError("22000026");
        }
        SerializeObject<List<KValueBean>> data = productOrderService.getKLine(searchDate);
        if (data != null && data.getData() != null) {
            List<KValueBean> beans = data.getData();
            return new SerializeObject<>(ResultType.NORMAL, "", beans);
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    @GetMapping(value = "/area/export")
    public SerializeObject exportAreaList(@RequestHeader String accessToken) {
        SerializeObject<List<AreaTransactionBean>> data = memberAccountService.queryTransactionList();
        if (data != null && data.getData() != null) {
            List<AreaTransactionBean> beans = data.getData();
            List<Serializable> list = new LinkedList<Serializable>();
            for (AreaTransactionBean bean : beans) {
                list.add(bean);
            }
            String path = ExcelUtil.writeToExcel(list, -1, properties.getTemplateUrl(), properties.getAccessKey(), properties.getSecretKey(), properties.getBucket(), properties.getFileDomain());
            if (StringUtils.isNotBlank(path)) {
                return new SerializeObject<>(ResultType.NORMAL, "", path);
            } else {
                return new SerializeObjectError("00000005");
            }
        } else {
            return new SerializeObjectError("00000005");
        }
    }

    /**
     * 平台交易总计报表
     * @param accessToken
     * @param page
     * @param timeType
     * @param viewType
     * @param startDate
     * @param endDate
     * @param channelIds
     * @param productIds
     * @param order
     * @return
     */
    @GetMapping(value = "/totalTrade/list")
    public SerializeObject<DataTable<T>> TotalTradeList(@RequestHeader String accessToken,  Integer pageNum, Integer pageSize, Integer timeType, String viewType, String startDate, String endDate, String[] channelIds, Integer[] productType, Order order) {
        //viewType=1 按金额；viewType=2 按克重
        //默认按月查询
        timeType = timeType == null ? 2 : timeType;
        viewType = StringUtils.isBlank(viewType) ? "1" : viewType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        String now = dateFormat.format(calendar.getTime());
        if (StringUtils.isBlank(endDate)) {
            endDate = now;
        }
        if (null == pageNum || pageNum <= 0){
            pageNum = 1;
        }
        if (null == pageSize || pageSize <= 0) {
            pageSize = 10;
        }

//        if (StringUtils.isBlank(startDate)) {
//            if (timeType == 1) {
//                calendar.add(Calendar.DAY_OF_YEAR, -6);
//                startDate = dateFormat.format(calendar.getTime());
//            } else if (timeType == 2) {
////                calendar.add(Calendar.MONTH, -1);
////                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//                startDate = "";
//            } else if (timeType == 3) {
//                int week = calendar.get(Calendar.DAY_OF_WEEK);
//                int days = 42;
//                week--;
//                if (week == 0) {
//                    week = 7;
//                }
//                week--;
//                days += week;
//                calendar.add(Calendar.DAY_OF_YEAR, -days);
//                startDate = dateFormat.format(calendar.getTime());
//            }
//        }

        if (StringUtils.isBlank(startDate)) {
            SerializeObject<Date> data = memberUserService.findMinAddTime();
            startDate = dateFormat.format(data.getData());
        }
        log.info("查询开始时间------------------"+startDate);

        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return new SerializeObjectError("22000025");
        }
        if (StringUtils.isNotBlank(order.getOrder()) && order.getOrder().contains(",")){
            order.setOrder(order.getOrder().replaceAll(",",""));
            order.setSort(order.getSort().replaceAll(",",""));
        }
        //排序，默认按照日期倒序排列
        if (StringUtils.isBlank(order.getOrder())) {
            order.setOrder("time");
            order.setSort("desc");
        }

        SerializeObject<List<TradeTotalStatisticsBean>> data = productOrderService.listTradeTotal(timeType, startDate, endDate, channelIds,productType);

        if (data != null && data.getData() != null) {
            List<TradeTotalStatisticsBean> beans = data.getData();

            if ( "1".equals(viewType)) {
                //合计值
                BigDecimal platformAmountCount = BigDecimal.ZERO;
                BigDecimal buyAmountCount = BigDecimal.ZERO;
                BigDecimal leasebackAmountCount = BigDecimal.ZERO;
                BigDecimal leasebackIncomeCount = BigDecimal.ZERO;
                BigDecimal soldAmountCount = BigDecimal.ZERO;
                BigDecimal soldIncomeCount = BigDecimal.ZERO;
                BigDecimal marketingAmountCount = BigDecimal.ZERO;

                List<TradeTotalAmountStatisticsBean> list = new ArrayList<>();
                for (TradeTotalStatisticsBean amountBean : beans) {
                    TradeTotalAmountStatisticsBean amBean = new TradeTotalAmountStatisticsBean();
                    amBean.setTime(amountBean.getTime());
                    amBean.setBuyTotalAmount(amountBean.getBuyTotalAmount());
                    amBean.setLeasebackTotalAmount(amountBean.getLeasebackTotalAmount());
                    amBean.setLeasebackTotalIncomeAmount(amountBean.getLeasebackTotalIncomeAmount());
                    amBean.setSoldTotalAmount(amountBean.getSoldTotalAmount());
                    amBean.setSoldTotalIncomeAmount(amountBean.getSoldTotalIncomeAmount());
                    amBean.setMarketingTotalAmount(amountBean.getMarketingTotalAmount());
                    BigDecimal platformAmount = amountBean.getBuyTotalAmount().add(amountBean.getLeasebackTotalAmount())
                            .add(amountBean.getLeasebackTotalIncomeAmount()).add(amountBean.getSoldTotalAmount()).add(amountBean.getSoldTotalIncomeAmount())
                            .add(amountBean.getMarketingTotalAmount());
                    amBean.setPlatformTotalAmount(platformAmount);
                    list.add(amBean);

                    platformAmountCount = platformAmountCount.add(platformAmount);
                    buyAmountCount = buyAmountCount.add(amountBean.getBuyTotalAmount());
                    leasebackAmountCount = leasebackAmountCount.add(amountBean.getLeasebackTotalAmount());
                    leasebackIncomeCount = leasebackIncomeCount.add(amountBean.getLeasebackTotalIncomeAmount());
                    soldAmountCount = soldAmountCount.add(amountBean.getSoldTotalAmount());
                    soldIncomeCount = soldIncomeCount.add(amountBean.getSoldTotalIncomeAmount());
                    marketingAmountCount = marketingAmountCount.add(amountBean.getMarketingTotalAmount());
                }

                Map map = new HashMap();
                map.put("platformAmountCount",platformAmountCount);
                map.put("buyAmountCount",buyAmountCount);
                map.put("leasebackAmountCount",leasebackAmountCount);
                map.put("leasebackIncomeCount",leasebackIncomeCount);
                map.put("soldAmountCount",soldAmountCount);
                map.put("soldIncomeCount",soldIncomeCount);
                map.put("marketingAmountCount",marketingAmountCount);

                if (order.getOrder().equalsIgnoreCase("time")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getTime));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getTime).reversed());
                    }
                }
                if (order.getOrder().equalsIgnoreCase("platformTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getPlatformTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getPlatformTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("buyTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getBuyTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getBuyTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalIncomeAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalIncomeAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalIncomeAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("soldTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("soldTotalIncomeAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalIncomeAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalIncomeAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("marketingTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getMarketingTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getMarketingTotalAmount).reversed());
                    }
                }

                //page.setTotal(list.size());
                int total = list.size();
                if (pageSize > 0) {
                    //list = list.stream().skip((PaginationUtil.getPage(page.getPageNum()) - 1) * page.getPageSize()).limit(page.getPageSize()).collect(Collectors.toList());
                    list = list.stream().skip((PaginationUtil.getPage(pageNum) - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
                }

                //return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<TradeTotalAmountStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list,map));
                return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<TradeTotalAmountStatisticsBean>(pageNum, pageSize, total, list,map));
            }

            if ("2".equals(viewType)) {
                //合计值
                BigDecimal platformGramCount = BigDecimal.ZERO;
                Integer buyGramCount = 0;
                Integer leasebackGramCount = 0;
                Integer leasebackIncomeCount = 0;
                Integer soldGramCount = 0;
                BigDecimal marketingGramCount = BigDecimal.ZERO;

                List<TradeTotalGramStatisticsBean> list = new ArrayList<>();
                for (TradeTotalStatisticsBean amountBean : beans) {
                    TradeTotalGramStatisticsBean amBean = new TradeTotalGramStatisticsBean();
                    amBean.setTime(amountBean.getTime());
                    amBean.setBuyTotalGram(amountBean.getBuyTotalGram());
                    amBean.setLeasebackTotalGram(amountBean.getLeasebackTotalGram());
                    amBean.setLeasebackTotalIncomeGram(amountBean.getLeasebackTotalIncomeGram());
                    amBean.setSoldTotalGram(amountBean.getSoldTotalGram());
                    amBean.setMarketingTotalGram(amountBean.getMarketingTotalGram());
                    BigDecimal platformGram = new BigDecimal((amountBean.getBuyTotalGram() + amountBean.getLeasebackTotalGram()
                            + amountBean.getLeasebackTotalIncomeGram() + amountBean.getSoldTotalGram())).add(amountBean.getMarketingTotalGram()).setScale(5, BigDecimal.ROUND_HALF_UP);

                    amBean.setPlatformTotalGram(platformGram);
                    list.add(amBean);

                    platformGramCount = platformGramCount.add(platformGram);
                    buyGramCount = buyGramCount + amountBean.getBuyTotalGram();
                    leasebackGramCount = leasebackGramCount + amountBean.getLeasebackTotalGram();
                    leasebackIncomeCount = leasebackIncomeCount + amountBean.getLeasebackTotalIncomeGram();
                    soldGramCount = soldGramCount + amountBean.getSoldTotalGram();
                    marketingGramCount = marketingGramCount.add(amountBean.getMarketingTotalGram());
                }

                Map map = new HashMap();
                map.put("platformGramCount",platformGramCount.setScale(5, BigDecimal.ROUND_HALF_UP));
                map.put("buyGramCount",buyGramCount);
                map.put("leasebackGramCount",leasebackGramCount);
                map.put("leasebackIncomeCount",leasebackIncomeCount);
                map.put("soldGramCount",soldGramCount);
                map.put("marketingGramCount",marketingGramCount.setScale(5, BigDecimal.ROUND_HALF_UP));

                if (order.getOrder().equalsIgnoreCase("time")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getTime));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getTime).reversed());
                    }
                }
                if (order.getOrder().equalsIgnoreCase("platformTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getPlatformTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getPlatformTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("buyTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getBuyTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getBuyTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalIncomeGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalIncomeGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalIncomeGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("soldTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getSoldTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getSoldTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("marketingTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getMarketingTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getMarketingTotalGram).reversed());
                    }
                }

                //page.setTotal(list.size());
                int total = list.size();
                if (pageSize > 0) {
                    list = list.stream().skip((PaginationUtil.getPage(pageNum) - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
                }

                //return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<TradeTotalGramStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list,map));
                return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<TradeTotalGramStatisticsBean>(pageNum, pageSize, total, list,map));
            }

            return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<TradeTotalStatisticsBean>(pageNum, pageSize, 0, new ArrayList<TradeTotalStatisticsBean>(),new HashMap<>()));
        } else {
            return new SerializeObject<>(ResultType.NORMAL, new DataOtherTable<TradeTotalStatisticsBean>(pageNum, pageSize, 0, new ArrayList<TradeTotalStatisticsBean>(),new HashMap<>()));
        }
    }

    @GetMapping(value = "/totalTrade/export")
    public void exportTradeTotalList(HttpServletResponse response, Integer timeType, String viewType, String startDate, String endDate, String[] channelIds, Integer[] productType,Order order) {
        //viewType=1 按金额；viewType=2 按克重
        //默认按月查询
        timeType = timeType == null ? 2 : timeType;
        viewType = StringUtils.isBlank(viewType) ? "1" : viewType;
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);

        SimpleDateFormat dateFormat = new SimpleDateFormat(NORMAL_DATE_FORMAT);
        Calendar calendar = Calendar.getInstance();
        String now = dateFormat.format(calendar.getTime());
        if (StringUtils.isBlank(endDate)) {
            endDate = now;
        }

//        if (StringUtils.isBlank(startDate)) {
//            if (timeType == 1) {
//                calendar.add(Calendar.DAY_OF_YEAR, -6);
//                startDate = dateFormat.format(calendar.getTime());
//            } else if (timeType == 2) {
////                calendar.add(Calendar.MONTH, -1);
////                calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
//                startDate = "";
//            } else if (timeType == 3) {
//                int week = calendar.get(Calendar.DAY_OF_WEEK);
//                int days = 42;
//                week--;
//                if (week == 0) {
//                    week = 7;
//                }
//                week--;
//                days += week;
//                calendar.add(Calendar.DAY_OF_YEAR, -days);
//                startDate = dateFormat.format(calendar.getTime());
//            }
//        }

        if (StringUtils.isBlank(startDate)) {
            SerializeObject<Date> data = memberUserService.findMinAddTime();
            startDate = dateFormat.format(data.getData());
        }

        if (StringUtils.isBlank(startDate) || StringUtils.isBlank(endDate)) {
            return;
        }

        if (StringUtils.isNotBlank(order.getOrder()) && order.getOrder().contains(",")){
            order.setOrder(order.getOrder().replaceAll(",",""));
            order.setSort(order.getSort().replaceAll(",",""));
        }
        //排序，默认按照日期倒序排列
        if (StringUtils.isBlank(order.getOrder())) {
            order.setOrder("time");
            order.setSort("desc");
        }

        SerializeObject<List<TradeTotalStatisticsBean>> data = productOrderService.listTradeTotal(timeType, startDate, endDate, channelIds,productType);
        if (data != null && data.getData() != null) {
            List<TradeTotalStatisticsBean> beans = data.getData();

            if ( "1".equals(viewType)) {
                //合计值
                BigDecimal platformAmountCount = BigDecimal.ZERO;
                BigDecimal buyAmountCount = BigDecimal.ZERO;
                BigDecimal leasebackAmountCount = BigDecimal.ZERO;
                BigDecimal leasebackIncomeCount = BigDecimal.ZERO;
                BigDecimal soldAmountCount = BigDecimal.ZERO;
                BigDecimal soldIncomeCount = BigDecimal.ZERO;
                BigDecimal marketingAmountCount = BigDecimal.ZERO;

                List<TradeTotalAmountStatisticsBean> list = new ArrayList<>();
                for (TradeTotalStatisticsBean amountBean : beans) {
                    TradeTotalAmountStatisticsBean amBean = new TradeTotalAmountStatisticsBean();
                    amBean.setTime(amountBean.getTime());
                    amBean.setBuyTotalAmount(amountBean.getBuyTotalAmount());
                    amBean.setLeasebackTotalAmount(amountBean.getLeasebackTotalAmount());
                    amBean.setLeasebackTotalIncomeAmount(amountBean.getLeasebackTotalIncomeAmount());
                    amBean.setSoldTotalAmount(amountBean.getSoldTotalAmount());
                    amBean.setSoldTotalIncomeAmount(amountBean.getSoldTotalIncomeAmount());
                    amBean.setMarketingTotalAmount(amountBean.getMarketingTotalAmount());
                    BigDecimal platformAmount = amountBean.getBuyTotalAmount().add(amountBean.getLeasebackTotalAmount())
                            .add(amountBean.getLeasebackTotalIncomeAmount()).add(amountBean.getSoldTotalAmount()).add(amountBean.getSoldTotalIncomeAmount())
                            .add(amountBean.getMarketingTotalAmount());
                    amBean.setPlatformTotalAmount(platformAmount);
                    list.add(amBean);

                    platformAmountCount = platformAmountCount.add(platformAmount);
                    buyAmountCount = buyAmountCount.add(amountBean.getBuyTotalAmount());
                    leasebackAmountCount = leasebackAmountCount.add(amountBean.getLeasebackTotalAmount());
                    leasebackIncomeCount = leasebackIncomeCount.add(amountBean.getLeasebackTotalIncomeAmount());
                    soldAmountCount = soldAmountCount.add(amountBean.getSoldTotalAmount());
                    soldIncomeCount = soldIncomeCount.add(amountBean.getSoldTotalIncomeAmount());
                    marketingAmountCount = marketingAmountCount.add(amountBean.getMarketingTotalAmount());
                }

                if (order.getOrder().equalsIgnoreCase("time")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getTime));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getTime).reversed());
                    }
                }
                if (order.getOrder().equalsIgnoreCase("platformTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getPlatformTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getPlatformTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("buyTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getBuyTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getBuyTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalIncomeAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalIncomeAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getLeasebackTotalIncomeAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("soldTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("soldTotalIncomeAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalIncomeAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getSoldTotalIncomeAmount).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("marketingTotalAmount")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getMarketingTotalAmount));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalAmountStatisticsBean::getMarketingTotalAmount).reversed());
                    }
                }


                String[] columnNames = {"日期", "平台总交易金额", "购买总交易金额", "回租总交易金额", "回租总奖励金额", "金价涨跌幅总收益", "出售总交易金额", "营销红包总金额"};
                String[] columns = {"time", "platformTotalAmount", "buyTotalAmount", "leasebackTotalAmount", "leasebackTotalIncomeAmount", "soldTotalIncomeAmount","soldTotalAmount" , "marketingTotalAmount"};
                String fileName = "平台交易总计报表";

                String[] firstLines = {"合计", platformAmountCount + "元", buyAmountCount + "元",
                        leasebackAmountCount + "元", leasebackIncomeCount + "元", soldIncomeCount + "元"
                        , soldAmountCount + "元", marketingAmountCount + "元"};

                ExportExcelController<TradeTotalAmountStatisticsBean> export = new ExportExcelController<TradeTotalAmountStatisticsBean>();
                export.exportUnlikeExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003, firstLines);
            }

            if ( "2".equals(viewType)) {
                //合计值
                BigDecimal platformGramCount = BigDecimal.ZERO;
                Integer buyGramCount = 0;
                Integer leasebackGramCount = 0;
                Integer leasebackIncomeCount = 0;
                Integer soldGramCount = 0;
                BigDecimal marketingGramCount = BigDecimal.ZERO;

                List<TradeTotalGramStatisticsBean> list = new ArrayList<>();
                for (TradeTotalStatisticsBean amountBean : beans) {
                    TradeTotalGramStatisticsBean amBean = new TradeTotalGramStatisticsBean();
                    amBean.setTime(amountBean.getTime());
                    amBean.setBuyTotalGram(amountBean.getBuyTotalGram());
                    amBean.setLeasebackTotalGram(amountBean.getLeasebackTotalGram());
                    amBean.setLeasebackTotalIncomeGram(amountBean.getLeasebackTotalIncomeGram());
                    amBean.setSoldTotalGram(amountBean.getSoldTotalGram());
                    amBean.setMarketingTotalGram(amountBean.getMarketingTotalGram());
                    BigDecimal platformGram = new BigDecimal((amountBean.getBuyTotalGram() + amountBean.getLeasebackTotalGram()
                            + amountBean.getLeasebackTotalIncomeGram() + amountBean.getSoldTotalGram())).add(amountBean.getMarketingTotalGram()).setScale(5, BigDecimal.ROUND_HALF_UP);;
                    amBean.setPlatformTotalGram(platformGram);
                    list.add(amBean);

                    platformGramCount = platformGramCount.add(platformGram);
                    buyGramCount = buyGramCount + amountBean.getBuyTotalGram();
                    leasebackGramCount = leasebackGramCount + amountBean.getLeasebackTotalGram();
                    leasebackIncomeCount = leasebackIncomeCount + amountBean.getLeasebackTotalIncomeGram();
                    soldGramCount = soldGramCount + amountBean.getSoldTotalGram();
                    marketingGramCount = marketingGramCount.add(amountBean.getMarketingTotalGram());
                }

                if (order.getOrder().equalsIgnoreCase("time")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getTime));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getTime).reversed());
                    }
                }
                if (order.getOrder().equalsIgnoreCase("platformTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getPlatformTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getPlatformTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("buyTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getBuyTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getBuyTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("leasebackTotalIncomeGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalIncomeGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getLeasebackTotalIncomeGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("soldTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getSoldTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getSoldTotalGram).reversed());
                    }
                }

                if (order.getOrder().equalsIgnoreCase("marketingTotalGram")) {
                    if (order.getSort().equalsIgnoreCase("asc")) {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getMarketingTotalGram));
                    } else {
                        Collections.sort(list, Comparator.comparing(TradeTotalGramStatisticsBean::getMarketingTotalGram).reversed());
                    }
                }

                String[] columnNames = {"日期", "平台总交易克重", "购买总交易克重", "回租总交易克重", "回租总奖励克重", "出售总交易克重", "营销红包总克重"};
                String[] columns = {"time", "platformTotalGram", "buyTotalGram", "leasebackTotalGram", "leasebackTotalIncomeGram", "soldTotalGram","marketingTotalGram"};
                String fileName = "平台交易总计报表";

                String[] firstLines = {"合计", platformGramCount.setScale(5, BigDecimal.ROUND_HALF_UP) + "克", buyGramCount + "克",
                        leasebackGramCount + "克", leasebackIncomeCount + "克", soldGramCount + "克"
                        , marketingGramCount.setScale(5, BigDecimal.ROUND_HALF_UP) + "克"};

                ExportExcelController<TradeTotalGramStatisticsBean> export = new ExportExcelController<TradeTotalGramStatisticsBean>();
                export.exportUnlikeExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003, firstLines);

            }

        }
    }
}




