package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.ProductBonusMapping;
import org.apache.ibatis.annotations.Param;

public interface ProductBonusMappingMapper {
    ProductBonusMapping findByProductId(@Param("productId") String productId);
}
