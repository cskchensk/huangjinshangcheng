package cn.ug.activity.service.impl;

import cn.ug.activity.mapper.ActivityInvitationMapper;
import cn.ug.activity.mapper.ActivityInvitationStrategyMapper;
import cn.ug.activity.mapper.ActivityInvitationTopMapper;
import cn.ug.activity.mapper.entity.ActivityInvitation;
import cn.ug.activity.mapper.entity.ActivityInvitationStrategy;
import cn.ug.activity.mapper.entity.ActivityInvitationTop;
import cn.ug.activity.service.ActivityInvitationService;
import cn.ug.core.ensure.Ensure;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ActivityInvitationServiceImpl implements ActivityInvitationService {
    @Autowired
    private ActivityInvitationMapper activityInvitationMapper;
    @Autowired
    private ActivityInvitationStrategyMapper activityInvitationStrategyMapper;
    @Autowired
    private ActivityInvitationTopMapper activityInvitationTopMapper;


    @Override
    public List<ActivityInvitation> query(int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("offset", offset);
        params.put("size", size);
        return activityInvitationMapper.query(params);
    }

    @Override
    public int count(String someday) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(someday)) {
            params.put("someday", someday);
        }
        return activityInvitationMapper.count(params);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int save(ActivityInvitation activityInvitation, List<ActivityInvitationStrategy> strategies) {
        if (activityInvitation != null && strategies != null && strategies.size() > 0) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("someday", activityInvitation.getStartDate());
            params.put("id", activityInvitation.getId());
            int num = activityInvitationMapper.count(params);
            if (num > 0) {
                Ensure.that(true).isTrue("22000102");
            }
            params.put("someday", activityInvitation.getEndDate());
            params.put("id", activityInvitation.getId());
            num = activityInvitationMapper.count(params);
            if (num > 0) {
                Ensure.that(0).isLt(1,"22000103");
            }
            if (activityInvitation.getId() > 0) {
                int rows = activityInvitationMapper.update(activityInvitation);
                if (rows > 0) {
                    rows = activityInvitationStrategyMapper.delete(activityInvitation.getId());
                    if (rows < 1) {
                        Ensure.that(0).isLt(1,"00000005");
                    }
                    for (ActivityInvitationStrategy strategy : strategies) {
                        strategy.setActivityInvitationId(activityInvitation.getId());
                    }
                    rows = activityInvitationStrategyMapper.insertInBatch(strategies);
                    if (rows < 1) {
                        Ensure.that(0).isLt(1,"00000005");
                    }
                    return activityInvitation.getId();
                }
            } else {
                activityInvitation.setAddTime(UF.getFormatDateTime(UF.getDateTime()));
                int rows = activityInvitationMapper.insert(activityInvitation);
                if (rows > 0) {
                    for (ActivityInvitationStrategy strategy : strategies) {
                        strategy.setActivityInvitationId(activityInvitation.getId());
                    }
                    rows = activityInvitationStrategyMapper.insertInBatch(strategies);
                    if (rows < 1) {
                        Ensure.that(0).isLt(1,"00000005");
                    }
                    return activityInvitation.getId();
                }
            }
        }
        return 0;
    }

    @Override
    public boolean save(List<ActivityInvitationTop> tops) {
        if (tops != null && tops.size() > 0) {
            for (ActivityInvitationTop top : tops) {
                int num = activityInvitationTopMapper.countByMobile(top.getMobile());
                if (num > 0) {
                    Ensure.that(true).isTrue("22000114");
                }
            }
            int activityInvitationId = tops.get(0).getActivityInvitationId();
            List<ActivityInvitationTop> activityInvitationTops = activityInvitationTopMapper.queryForList(activityInvitationId);
            if (activityInvitationTops != null && activityInvitationTops.size() > 0) {
                int rows = activityInvitationTopMapper.delete(activityInvitationId);
                Ensure.that(rows).isLt(1,"00000005");
                rows = activityInvitationTopMapper.insertInBatch(tops);
                Ensure.that(rows).isLt(1,"00000005");
                return true;
            } else {
                int rows = activityInvitationTopMapper.insertInBatch(tops);
                Ensure.that(rows).isLt(1,"00000005");
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean close(int activityInvitationId) {
        if (activityInvitationId > 0) {
            return activityInvitationMapper.closeActivity(activityInvitationId) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean delete(int activityInvitationId) {
        if (activityInvitationId > 0) {
            return activityInvitationMapper.deleteActivity(activityInvitationId) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public ActivityInvitation get(int activityInvitationId) {
        if (activityInvitationId > 0) {
            ActivityInvitation activityInvitation = activityInvitationMapper.findById(activityInvitationId);
            if (activityInvitation == null) {
                return null;
            }
            List<ActivityInvitationStrategy> strategies = activityInvitationStrategyMapper.queryForList(activityInvitationId);
            if (strategies != null) {
                activityInvitation.setStrategyies(strategies);
            }
            List<ActivityInvitationTop> tops = activityInvitationTopMapper.queryForList(activityInvitationId);
            if (tops != null) {
                activityInvitation.setTops(tops);
            }
            return activityInvitation;
        }
        return null;
    }

    @Override
    public ActivityInvitation selectValidActivity() {
        return activityInvitationMapper.selectValidActivity();
    }

    @Override
    public List<ActivityInvitationStrategy> queryForList(int activityInvitationId) {
        if (activityInvitationId > 0) {
            return activityInvitationStrategyMapper.queryForList(activityInvitationId);
        }
        return null;
    }

    @Override
    public List<ActivityInvitationStrategy> queryForLatestList() {
        return activityInvitationStrategyMapper.queryForLatestList();
    }

    @Override
    public List<ActivityInvitationTop> queryTop10(int activityInvitationId, String startDate, String endDate) {
        if (activityInvitationId > 0) {
            return activityInvitationTopMapper.queryTop10(activityInvitationId, startDate, endDate);
        }
        return null;
    }
}
