package cn.ug.activity.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class ActivityInvitationStrategy implements Serializable {
    private int id;
    private int activityInvitationId;
    private int mark;
    private BigDecimal investmentAmount;
    private BigDecimal proportion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getActivityInvitationId() {
        return activityInvitationId;
    }

    public void setActivityInvitationId(int activityInvitationId) {
        this.activityInvitationId = activityInvitationId;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public BigDecimal getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(BigDecimal investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public BigDecimal getProportion() {
        return proportion;
    }

    public void setProportion(BigDecimal proportion) {
        this.proportion = proportion;
    }
}
