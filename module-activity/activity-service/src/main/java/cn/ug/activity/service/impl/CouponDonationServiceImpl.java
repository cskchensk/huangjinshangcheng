package cn.ug.activity.service.impl;

import cn.ug.activity.mapper.CouponDonationMapper;
import cn.ug.activity.mapper.entity.CouponDonation;
import cn.ug.activity.service.CouponDonationService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CouponDonationServiceImpl implements CouponDonationService {
    @Autowired
    private CouponDonationMapper couponDonationMapper;

    @Override
    public boolean save(CouponDonation couponDonation) {
        if (couponDonation != null) {
            if (couponDonation.getId() > 0) {
                return couponDonationMapper.update(couponDonation) > 0 ? true : false;
            } else {
                couponDonation.setAddTime(UF.getFormatDateTime(LocalDateTime.now()));
                return couponDonationMapper.insert(couponDonation) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public CouponDonation get(int id) {
        if (id > 0) {
            return couponDonationMapper.findById(id);
        }
        return null;
    }

    @Override
    public List<CouponDonation> listRecords(String couponName, int status, int type, String startTime, String endTime, int startNum, int endNum, String order, String sort, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(couponName)) {
            params.put("couponName", couponName);
        }
        params.put("status", status);
        params.put("type", type);
        if (StringUtils.isNotBlank(startTime)) {
            params.put("startTime", startTime);
        }
        if (StringUtils.isNotBlank(endTime)) {
            params.put("endTime", endTime);
        }
        params.put("startNum", startNum);
        params.put("endNum", endNum);
        List<String> orders = Arrays.asList("donation_time", "total_quantity");
        if (orders.contains(order)) {
            params.put("order", order);
            params.put("sort", "desc");
            if (StringUtils.equalsIgnoreCase(sort, "asc") || StringUtils.equalsIgnoreCase(sort, "desc")) {
                params.put("sort", sort);
            }
        }
        params.put("offset", offset);
        params.put("size", size);
        return couponDonationMapper.query(params);
    }

    @Override
    public List<CouponDonation> listRecords(String systemDate) {
        return couponDonationMapper.queryBySystemDate(systemDate);
    }

    @Override
    public int countRecords(String couponName, int status, int type, String startTime, String endTime, int startNum, int endNum) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(couponName)) {
            params.put("couponName", couponName);
        }
        params.put("status", status);
        params.put("type", type);
        if (StringUtils.isNotBlank(startTime)) {
            params.put("startTime", startTime);
        }
        if (StringUtils.isNotBlank(endTime)) {
            params.put("endTime", endTime);
        }
        params.put("startNum", startNum);
        params.put("endNum", endNum);
        return couponDonationMapper.count(params);
    }

    @Override
    public boolean modifyStatus(int id, int status) {
        if (id > 0) {
            return couponDonationMapper.updateStatus(id, status) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public boolean audit(CouponDonation couponDonation) {
        if (couponDonation != null) {
            return couponDonationMapper.audit(couponDonation) > 0 ? true : false;
        }
        return false;
    }
}
