package cn.ug.activity.mapper.entity;

import java.io.Serializable;
import java.util.List;

public class ActivityInvitation implements Serializable {
    private int id;
    private String name;
    private String startDate;
    private String endDate;
    private int status;
    private String addTime;
    private int deleted;
    private List<ActivityInvitationStrategy> strategyies;
    private List<ActivityInvitationTop> tops;

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ActivityInvitationStrategy> getStrategyies() {
        return strategyies;
    }

    public void setStrategyies(List<ActivityInvitationStrategy> strategyies) {
        this.strategyies = strategyies;
    }

    public List<ActivityInvitationTop> getTops() {
        return tops;
    }

    public void setTops(List<ActivityInvitationTop> tops) {
        this.tops = tops;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
