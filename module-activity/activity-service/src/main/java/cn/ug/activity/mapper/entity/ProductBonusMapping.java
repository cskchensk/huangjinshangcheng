package cn.ug.activity.mapper.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class ProductBonusMapping implements Serializable {
    private String id;
    private String productId;
    private String bonusId;
    private LocalDateTime addTime;

    public ProductBonusMapping() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBonusId() {
        return bonusId;
    }

    public void setBonusId(String bonusId) {
        this.bonusId = bonusId;
    }

    public LocalDateTime getAddTime() {
        return addTime;
    }

    public void setAddTime(LocalDateTime addTime) {
        this.addTime = addTime;
    }
}
