package cn.ug.activity.web.controller;

import cn.ug.activity.bean.*;
import cn.ug.activity.service.MemberInvestmentRewardsService;
import cn.ug.aop.RequiresPermissions;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.pay.bean.FeeBean;
import cn.ug.util.ExportExcelUtil;
import cn.ug.util.PaginationUtil;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static cn.ug.util.ConstantUtil.NORMAL_DATE_FORMAT;

@RestController
@RequestMapping("rewards")
public class MemberInvestmentRewardsController extends BaseController {
    @Autowired
    private MemberInvestmentRewardsService memberInvestmentRewardsService;

    @GetMapping(value = "/list")
    public SerializeObject<DataTable<MemberInvestmentRewardsBean>> list(@RequestHeader String accessToken, Page page, String memberId) {
        memberId = UF.toString(memberId);
        int total = memberInvestmentRewardsService.count(memberId);
        page.setTotal(total);
        if (total > 0) {
            List<MemberInvestmentRewardsBean> list = memberInvestmentRewardsService.list(memberId, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject(ResultType.NORMAL, new DataTable<MemberInvestmentRewardsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<MemberInvestmentRewardsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<MemberInvestmentRewardsBean>()));
    }

    @GetMapping(value = "/member/invitation/list")
    public SerializeObject<DataTable<InvitationBean>> listInvitationRewards(@RequestHeader String accessToken, Page page, String memberId, String startDate, String endDate) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        int total = memberInvestmentRewardsService.countMemberRewards(memberId, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<InvitationBean> list = memberInvestmentRewardsService.queryMemberRewards(memberId, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject(ResultType.NORMAL, new DataTable<InvitationBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InvitationBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InvitationBean>()));
    }

    @GetMapping(value = "/member/invitation/records/list")
    public SerializeObject<DataTable<InvitationRewardsBean>> listInvitationRecordsRewards(@RequestHeader String accessToken, Page page, String memberId, String invitationDate) {
        memberId = UF.toString(memberId);
        invitationDate = UF.toString(invitationDate);
        int total = memberInvestmentRewardsService.countInvitationRecords(memberId, invitationDate);
        page.setTotal(total);
        if (total > 0) {
            List<InvitationRewardsBean> list = memberInvestmentRewardsService.queryInvitationRecords(memberId, invitationDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            if (list != null && list.size() > 0) {
                for (InvitationRewardsBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                        bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                    }
                }
            }
            return new SerializeObject(ResultType.NORMAL, new DataTable<InvitationRewardsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InvitationRewardsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InvitationRewardsBean>()));
    }

    @GetMapping(value = "/member/invitation/records/export")
    public void exportData(HttpServletResponse response, String memberId, String invitationDate) {
        memberId = UF.toString(memberId);
        invitationDate = UF.toString(invitationDate);
        List<InvitationRewardsBean> list = memberInvestmentRewardsService.queryInvitationRecords(memberId, invitationDate, 0, 0);
        if(list != null && list.size() > 0){
            String[] columnNames = { "序号", "好友账号","好友姓名", "好友注册时间", "好友是否绑卡", "好友绑卡时间", "好友投资标的名称", "好友投资金额",
                "好友投资时间", "奖励类型", "奖励金额（元）"};
            String [] columns = {"index",  "friendMobile", "friendName", "friendRegisterTime", "bindedCardMark", "bindedCardTime", "productName",
                    "investAmount", "investTime", "typeMark", "amount"};
            String fileName = "邀请收益明细-"+invitationDate;
            int index = 1;
            for (InvitationRewardsBean bean : list) {
                bean.setIndex(index);
                if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                    bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                }
                if (bean.getBindedCard() == 2) {
                    bean.setBindedCardMark("已绑卡");
                } else {
                    bean.setBindedCardMark("未绑卡");
                }
                if (StringUtils.isBlank(bean.getBindedCardTime())) {
                    bean.setBindedCardTime("N/A");
                }
                if (StringUtils.isBlank(bean.getProductName())) {
                    bean.setProductName("N/A");
                }
                if (bean.getInvestAmount() == null) {
                    bean.setInvestAmount(BigDecimal.ZERO);
                }
                if (StringUtils.isBlank(bean.getInvestTime())) {
                    bean.setInvestTime("N/A");
                }
                if (bean.getType() == 1) {
                    bean.setTypeMark("认证绑卡");
                } else if (bean.getType() == 2) {
                    bean.setTypeMark("首投奖励");
                } else if (bean.getType() == 3) {
                    bean.setTypeMark("投资返现");
                } else {
                    bean.setTypeMark("N/A");
                }
                index++;
            }
            ExportExcelController<InvitationRewardsBean> export = new ExportExcelController<InvitationRewardsBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    @GetMapping(value = "/member/invitation/export")
    public void exportData(HttpServletResponse response, String memberId, String startDate, String endDate) {
        memberId = UF.toString(memberId);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        List<InvitationBean> list = memberInvestmentRewardsService.queryMemberRewards(memberId, startDate, endDate, 0, 0);
        if(list != null && list.size() > 0){
            String[] columnNames = { "日期", "邀请好友数（人）","成功交易好友数（人）", "累计奖励（元）", "绑卡人数统计（人）", "认证绑卡奖励（元）", "首投奖励合计（元）", "投资返现奖励合计（元）"};
            String [] columns = {"invitationDate",  "friends", "friendTradedNum", "totalAmount", "friendBindCardNum", "bindCardAmount", "investAmount", "bonusAmount"};
            String fileName = "邀请收益记录";
            ExportExcelController<InvitationBean> export = new ExportExcelController<InvitationBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        }
    }

    @GetMapping(value = "/statistics")
    public SerializeObject<DataTable<InviteRewardsStatisticsBean>> list(@RequestHeader String accessToken, Page page, BigDecimal amountPartOne, BigDecimal amountPartTwo, String startDate, String endDate) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        int total = memberInvestmentRewardsService.countRewardsRecords(amountPartOne, amountPartTwo, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<InviteRewardsStatisticsBean> list = memberInvestmentRewardsService.listRewardsRecords(amountPartOne, amountPartTwo, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InviteRewardsStatisticsBean>()));
    }

    @GetMapping(value = "/records/statistics")
    public SerializeObject<DataTable<InviteRewardsRecordStatisticsBean>> listRecords(@RequestHeader String accessToken, Page page, String rewardsDate, String startDate, String endDate, String mobile, String name) {
        rewardsDate = UF.toString(rewardsDate);
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        mobile = UF.toString(mobile);
        name = UF.toString(name);
        int total = memberInvestmentRewardsService.countRewardsRecords(null, rewardsDate, startDate, endDate, mobile, name);
        page.setTotal(total);
        if (total > 0) {
            List<InviteRewardsRecordStatisticsBean> list = memberInvestmentRewardsService.listRewardsRecords(null, rewardsDate, startDate, endDate, mobile, name, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsRecordStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsRecordStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InviteRewardsRecordStatisticsBean>()));
    }

    @GetMapping(value = "/records/statistics/export")
    public void export(HttpServletResponse response, String startDate, String endDate, String mobile, String name) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        List<InviteRewardsRecordStatisticsBean> list = memberInvestmentRewardsService.listRewardsRecords(null, null, startDate, endDate, null, null, 0, 0);
        if(list != null && list.size() > 0){
            String[] columnNames = { "序号","邀请人手机号", "邀请人姓名", "被邀请人姓名", "被邀请人手机号", "邀请奖励说明",  "邀请奖励(元)", "邀请成功时间"};
            String [] columns = {"index", "mobile", "name", "friendMobile", "friendName", "remark", "amount", "friendRegisterTime"};
            String fileName = "邀请奖励明细";
            int index = 1;
            for (InviteRewardsRecordStatisticsBean bean : list) {
                bean.setIndex(index);
                index++;
            }
            ExportExcelController<InviteRewardsRecordStatisticsBean> export = new ExportExcelController<InviteRewardsRecordStatisticsBean>();
            export.exportExcel(fileName, fileName, columnNames, columns, list, response, ExportExcelUtil.EXCEL_FILE_2003);
        } else {
            try {
                response.setHeader("Content-type", "text/html;charset=UTF-8");
                response.getWriter().write("<p style=\"color:red;text-align:center;\">请求的数据不存在</p>");
                response.getWriter().flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @RequiresPermissions("activity:invitaion:rewards:records")
    @GetMapping(value = "/invitation/rewards/records")
    public SerializeObject<DataTable<InviteRewardsRecordStatisticsBean>> listRewardsRecords(@RequestHeader String accessToken, Page page, String memberId) {
        memberId = UF.toString(memberId);
        int total = memberInvestmentRewardsService.countRewardsRecords(memberId, null, null, null, null, null);
        page.setTotal(total);
        if (total > 0) {
            List<InviteRewardsRecordStatisticsBean> list = memberInvestmentRewardsService.listRewardsRecords(memberId, null, null, null, null, null, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            if (list != null && list.size() > 0) {
                for (InviteRewardsRecordStatisticsBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                        bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                    }
                }
            }
            return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsRecordStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsRecordStatisticsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InviteRewardsRecordStatisticsBean>()));
    }

    /**
     *
     * 好友交易明细 - 好友累计
     * @param accessToken
     * @param page
     * @param memberId
     * @return
     */
    @RequiresPermissions("activity:invitaion:friendTotal:records")
    @GetMapping(value = "/invitation/friendTotal/records")
    public SerializeObject<DataTable<InviteRewardsRecordFriendBean>> listFriendTotalRecords(@RequestHeader String accessToken, Page page, String memberId) {
        memberId = UF.toString(memberId);
        int total = memberInvestmentRewardsService.countFriendTotalRecords(memberId);
        page.setTotal(total);
        if (total > 0) {
            List<InviteRewardsRecordFriendBean> list = memberInvestmentRewardsService.listFriendTotalRecords(PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()),memberId);
            if (list != null && list.size() > 0) {
                for (InviteRewardsRecordFriendBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                        bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                    }
                }
            }
            return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsRecordFriendBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRewardsRecordFriendBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InviteRewardsRecordFriendBean>()));

    }

    /**
     * 邀请奖励明细
     * @param accessToken
     * @param page
     * @param memberId
     * @return
     */
    @RequiresPermissions("activity:invitaion:friend:award:records")
    @GetMapping(value = "/invitation/friend/award/records")
    public SerializeObject<DataTable<InviteFriendAwardRecordsBean>> listFriendAwardRecords(@RequestHeader String accessToken, Page page, String memberId) {
        memberId = UF.toString(memberId);
        int total = memberInvestmentRewardsService.countAwardRecords(memberId);
        page.setTotal(total);
        if (total > 0) {
            List<InviteFriendAwardRecordsBean> list = memberInvestmentRewardsService.listFriendAwardRecords(PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()),memberId);
            if (list != null && list.size() > 0) {
                for (InviteRewardsRecordFriendBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                        bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                    }
                }
            }
            return new SerializeObject(ResultType.NORMAL, new DataTable<InviteFriendAwardRecordsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InviteFriendAwardRecordsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InviteFriendAwardRecordsBean>()));

    }

    @RequiresPermissions("activity:invitaion:rewards:records")
    @GetMapping(value = "/invitation/rewards/outline")
    public SerializeObject<JSONObject> listRecords(@RequestHeader String accessToken, String memberId) {
        memberId = UF.toString(memberId);
        InviteRecordsBean bean = memberInvestmentRewardsService.getInvitation(memberId);
        if (bean == null) {
            bean = new InviteRecordsBean();
        }
        if (bean.getName() == null) {
            bean.setName("");
        }
        if (StringUtils.isNotBlank(bean.getMobile())) {
            bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
        }
        JSONObject result = JSON.parseObject(JSONObject.toJSONString(bean));
        List<RecordInfoBean> recordInfoBeans = memberInvestmentRewardsService.getRewardsByMemberId(memberId);
        if (recordInfoBeans == null || recordInfoBeans.size() == 0) {
            result.put("bindedCardAmount", 0);
            result.put("firstInvestAmount", 0);
            result.put("bonusAmount", 0);
            result.put("friendTradeMoney",0);
        }
        for (RecordInfoBean recordInfoBean : recordInfoBeans) {
            if (recordInfoBean.getType() == 1) {
                result.put("bindedCardAmount", recordInfoBean.getAmount());
            } else if (recordInfoBean.getType() == 2) {
                result.put("firstInvestAmount", recordInfoBean.getAmount());
            } else if (recordInfoBean.getType() == 3) {
                result.put("bonusAmount", recordInfoBean.getAmount());
            }
        }
        if (!result.containsKey("bindedCardAmount")) {
            result.put("bindedCardAmount", 0);
        }
        if (!result.containsKey("firstInvestAmount")) {
            result.put("firstInvestAmount", 0);
        }
        if (!result.containsKey("bonusAmount")) {
            result.put("bonusAmount", 0);
        }
        if (!result.containsKey("friendTradeMoney")){
            result.put("friendTradeMoney",0);
        }
        return new SerializeObject<>(ResultType.NORMAL, result);
    }

    @RequiresPermissions("activity:invitaion:friend:records")
    @GetMapping(value = "/invitation/outline")
    public SerializeObject<JSONObject> getInvitationOutline(@RequestHeader String accessToken, String memberId) {
        memberId = UF.toString(memberId);
        InviteRecordsBean bean = memberInvestmentRewardsService.getInvitation(memberId);
        if (bean == null) {
            bean = new InviteRecordsBean();
        }
        if (bean.getName() == null) {
            bean.setName("");
        }
        if (StringUtils.isNotBlank(bean.getMobile())) {
            bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
        }
        JSONObject result = JSON.parseObject(JSONObject.toJSONString(bean));
        result.put("bindedCardNum", memberInvestmentRewardsService.getBindCardNum(memberId));
        return new SerializeObject<>(ResultType.NORMAL, result);
    }

    @RequiresPermissions("activity:invitaion:records")
    @GetMapping(value = "/invitation/list")
    public SerializeObject<DataTable<InviteRecordsBean>> listRecords(@RequestHeader String accessToken, Page page, String startDate, String endDate, String mobile) {
        startDate = UF.toString(startDate);
        endDate = UF.toString(endDate);
        mobile = UF.toString(mobile);
        int total = memberInvestmentRewardsService.countQueryInviteRecords(mobile, startDate, endDate);
        page.setTotal(total);
        if (total > 0) {
            List<InviteRecordsBean> list = memberInvestmentRewardsService.listInviteRecords(mobile, startDate, endDate, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));

            if (list != null && list.size() > 0) {
                for (InviteRecordsBean bean : list) {
                    if (StringUtils.isNotBlank(bean.getMobile())) {
                        bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
                    }
                }
            }
            return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRecordsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InviteRecordsBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InviteRecordsBean>()));
    }

    @RequiresPermissions("activity:invitaion:friend:records")
    @GetMapping(value = "/invitation/binded/card/records")
    public SerializeObject<DataTable<InvitationBindedCardBean>> listBindedCardRecords(@RequestHeader String accessToken, Page page, String memberId) {
        memberId = UF.toString(memberId);
        int total = memberInvestmentRewardsService.countInvitationBindedCardRecords(memberId);
        page.setTotal(total);
        if (total > 0) {
            List<InvitationBindedCardBean> list = memberInvestmentRewardsService.listInvitationBindedCardRecords(memberId, PaginationUtil.getOffset(page.getPageNum(), page.getPageSize()), PaginationUtil.getPageSize(page.getPageSize()));
            if (list != null && list.size() > 0) {
                for (InvitationBindedCardBean bean : list) {
                    if (bean.getBindedCard() != 2) {
                        bean.setBindedCardTime("");
                    }
                    if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                        bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                    }
                }
            }
            return new SerializeObject(ResultType.NORMAL, new DataTable<InvitationBindedCardBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), list));
        }
        return new SerializeObject(ResultType.NORMAL, new DataTable<InvitationBindedCardBean>(page.getPageNum(), page.getPageSize(), page.getTotal(), new ArrayList<InvitationBindedCardBean>()));
    }

    @GetMapping(value = "/recent/list")
    public SerializeObject<List<MemberInvestmentRewardsBean>> recentList(){
        List<MemberInvestmentRewardsBean> list = memberInvestmentRewardsService.listRecent();
        if (list != null && list.size() > 0) {
            for (MemberInvestmentRewardsBean bean : list) {
                if (StringUtils.isNotBlank(bean.getFriendMobile())) {
                    bean.setFriendMobile(StringUtils.substring(bean.getFriendMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getFriendMobile(), 7));
                }
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    @GetMapping(value = "/top/list")
    public SerializeObject<List<RewardsTopBean>> topList() {
        List<RewardsTopBean> list = memberInvestmentRewardsService.listTopThree();
        if (list != null && list.size() > 0) {
            for (RewardsTopBean bean : list) {
                if (StringUtils.isNotBlank(bean.getMobile())) {
                    bean.setMobile(StringUtils.substring(bean.getMobile(), 0, 3) +"****"+ StringUtils.substring(bean.getMobile(), 7));
                }
            }
        }
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    @GetMapping(value = "/sum")
    public SerializeObject<BigDecimal> sum(String memberId) {
        memberId = UF.toString(memberId);
        return new SerializeObject<>(ResultType.NORMAL, memberInvestmentRewardsService.sum(memberId));
    }
}
