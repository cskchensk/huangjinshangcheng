package cn.ug.activity.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class Contract extends BaseEntity implements Serializable {
    private String orderId;
    private String thirdpartyUrl;
    private String originalUrl;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getThirdpartyUrl() {
        return thirdpartyUrl;
    }

    public void setThirdpartyUrl(String thirdpartyUrl) {
        this.thirdpartyUrl = thirdpartyUrl;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }
}
