package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.CouponDonation;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CouponDonationMapper {
    List<CouponDonation> query(Map<String, Object> params);
    List<CouponDonation> queryBySystemDate(@Param("systemDate")String systemDate);
    CouponDonation findById(@Param("id")int id);
    int count(Map<String, Object> params);
    int updateStatus(@Param("id")int id, @Param("status") int status);
    int insert(CouponDonation couponDonation);
    int update(CouponDonation couponDonation);
    int audit(@Param("couponDonation")CouponDonation couponDonation);
}
