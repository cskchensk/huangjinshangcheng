package cn.ug.activity.service;

import cn.ug.activity.mapper.entity.Contract;

public interface ContractService {
    boolean save(Contract contract);
    Contract get(String orderId);
}
