package cn.ug.activity.service;

import cn.ug.activity.bean.ActivityInfoBean;

import java.util.List;

public interface ActivityInfoService {
    boolean save(ActivityInfoBean entityBean);
    ActivityInfoBean get(String id);
    List<ActivityInfoBean> listRecords(String name, String startDate, String endDate, int offset, int size);
    int countRecords(String name, String startDate, String endDate);
}
