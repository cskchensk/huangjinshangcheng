package cn.ug.activity.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class ActivityInvitationTop implements Serializable {
    private int id;
    private String mobile;
    private BigDecimal investmentAmount;
    private BigDecimal rewards;
    private int activityInvitationId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public BigDecimal getRewards() {
        return rewards;
    }

    public void setRewards(BigDecimal rewards) {
        this.rewards = rewards;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(BigDecimal investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public int getActivityInvitationId() {
        return activityInvitationId;
    }

    public void setActivityInvitationId(int activityInvitationId) {
        this.activityInvitationId = activityInvitationId;
    }
}
