package cn.ug.activity.mapper;

import cn.ug.activity.bean.*;
import cn.ug.activity.mapper.entity.CouponMember;
import cn.ug.mapper.BaseMapper;
import cn.ug.member.bean.MemberDetailBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CouponMemberMapper extends BaseMapper<CouponMember> {
    int insertInBatch(@Param("coupons") List<CouponMember> coupons);
    List<CouponMemberBean> queryForList(Map<String, Object> params);
    CouponMemberBean selectById(@Param("couponMemberId")int couponMemberId);
    CouponMemberBean findBeanById(@Param("id")String id);
    int queryCouponsForCountByCouponId(@Param("couponId")String couponId);
    List<CouponStatisticsBean> listCouponStatistics(Map<String, Object> param);
    int countCouponStatistics(Map<String, Object> param);
    List<CouponStatisticsBean> listInterestTicketStatistics(Map<String, Object> param);
    int countInterestTicketStatistics(Map<String, Object> param);

    List<CouponMemberItemBean> queryCouponsForList(Map<String, Object> params);
    int queryCouponsForCount(Map<String, Object> params);

    List<DonationCouponBean> queryCouponsForFinanceList(Map<String, Object> param);
    int queryCouponsForFinanceCount(Map<String, Object> param);
    List<DonationCouponRecordBean> queryCouponsForFinanceRecordList(Map<String, Object> param);
    int queryCouponsForFinanceRecordCount(Map<String, Object> param);
    List<MemberDonationCouponBean> queryForFinanceRecordListByMemberId(Map<String, Object> param);
    int queryForFinanceRecordCountByMemberId(Map<String, Object> param);

    List<GiveOutBean> listCouponGrantStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<GiveOutBean> listTicketGrantStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<GiveOutAverageValueBean> listCouponAvgValueStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<GiveOutAverageValueBean> listTicketAvgValueStatistics(@Param("timeType")int timeType, @Param("startDate")String startDate,
                                                @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);
    List<GiveOutFutureValueBean> listTicketFutureValueStatistics(@Param("startDate")String startDate,
                                                @Param("endDate")String endDate, @Param("channelIds")List<String> channelIds);

    MemberDetailBean selectUsableCouponInfo(@Param("memberId")String memberId);

    List<CouponNoticeBean> queryForNoticeList();

    List<CouponMemberBean> queryForCouponList(@Param("memberId")String memberId, @Param("couponIds")List<Integer> couponIds);

    Map queryCouponStatistics(Map<String, Object> params);

    Integer queryCountByMemberIdAndCouponId(@Param("memberId")String memberId, @Param("couponId")int couponId);
}
