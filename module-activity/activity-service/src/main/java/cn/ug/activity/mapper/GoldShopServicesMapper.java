package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.GoldShopServices;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoldShopServicesMapper {
    List<GoldShopServices> findByShopId(@Param("shopId") int shopId);
    int insertInBatch(@Param("services") List<GoldShopServices> services);
    int deleteByShopId(@Param("shopId") int shopId);
}
