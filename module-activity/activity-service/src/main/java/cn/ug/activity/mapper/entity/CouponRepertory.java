package cn.ug.activity.mapper.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class CouponRepertory implements Serializable {
    private int id;
    private String name;
    //类型：0-黄金红包；1-回租福利券；2-商城满减券
    private int type;
    //优惠券赠送方式：0-自动触发赠送；1-手动系统赠送
    private int category;
    //赠送用户组：0-注册未绑卡；1-绑卡未交易；2-已交易用户
    private int groupOf;
    //交易额度
    private int transactionAmount;
    //优惠额度/type=0时为金生金奖励（单位:mg）
    private int discountAmount;
    //使用条件
    private String usedCondition;
    //有效期限类型：0：领取X后过期；1：某天过期
    private int indateType;
    //有效天数（indate_type等于0时必填）
    private int indateDays;
    //有效时间（indate_type等于1时必填）
    private String indateTime;
    //使用须知
    private String remark;
    private String addTime;
    private int status;
    private int deleted;
    //到期是否提醒：1-不提醒；2-提醒
    private int isNotice;
    //提前天数
    private int precedeDays;
    //回租天数
    private int leasebackDays;
    //预计年化收益
    private BigDecimal yearIncome;
    //黄金红包-加息年化收益率
    private BigDecimal raiseYearIncome;

    /**
     * 领取状态0未领取  1已领取
     */
    private int pullStatus = 0;

    /**
     * 适用产品类型 0:全部产品  1:实物金产品  2:安稳金产品
     */
    private Integer productType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public BigDecimal getYearIncome() {
        return yearIncome;
    }

    public void setYearIncome(BigDecimal yearIncome) {
        this.yearIncome = yearIncome;
    }

    public int getIsNotice() {
        return isNotice;
    }

    public void setIsNotice(int isNotice) {
        this.isNotice = isNotice;
    }

    public int getPrecedeDays() {
        return precedeDays;
    }

    public void setPrecedeDays(int precedeDays) {
        this.precedeDays = precedeDays;
    }

    public int getGroupOf() {
        return groupOf;
    }

    public void setGroupOf(int groupOf) {
        this.groupOf = groupOf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public int getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(int discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getUsedCondition() {
        return usedCondition;
    }

    public void setUsedCondition(String usedCondition) {
        this.usedCondition = usedCondition;
    }

    public int getIndateType() {
        return indateType;
    }

    public void setIndateType(int indateType) {
        this.indateType = indateType;
    }

    public int getIndateDays() {
        return indateDays;
    }

    public void setIndateDays(int indateDays) {
        this.indateDays = indateDays;
    }

    public String getIndateTime() {
        return indateTime;
    }

    public void setIndateTime(String indateTime) {
        this.indateTime = indateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public BigDecimal getRaiseYearIncome() {
        return raiseYearIncome;
    }

    public void setRaiseYearIncome(BigDecimal raiseYearIncome) {
        this.raiseYearIncome = raiseYearIncome;
    }

    public int getPullStatus() {
        return pullStatus;
    }

    public void setPullStatus(int pullStatus) {
        this.pullStatus = pullStatus;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }
}
