package cn.ug.activity.service.impl;

import cn.ug.activity.bean.*;
import cn.ug.activity.mapper.MemberInvestmentRewardsMapper;
import cn.ug.activity.mapper.entity.MemberInvestmentRewards;
import cn.ug.activity.service.MemberInvestmentRewardsService;
import cn.ug.enums.RewardsTypeEnum;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberInvestmentRewardsServiceImpl extends BaseServiceImpl implements MemberInvestmentRewardsService {
    @Autowired
    private MemberInvestmentRewardsMapper memberInvestmentRewardsMapper;

    @Override
    public List<MemberInvestmentRewardsBean> list(String memberId, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(memberId)) {
            params.put("memberId", memberId);
        }
        params.put("offset", offset);
        params.put("size", size);
        return memberInvestmentRewardsMapper.query(params);
    }

    @Override
    public List<MemberInvestmentRewardsBean> listRecent() {
        return memberInvestmentRewardsMapper.queryrRecentList();
    }

    @Override
    public int count(String memberId) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(memberId)) {
            params.put("memberId", memberId);
        }
        return memberInvestmentRewardsMapper.count(params);
    }

    @Override
    public BigDecimal sum(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("memberId", memberId);
            BigDecimal amount = memberInvestmentRewardsMapper.sum(params);
            if (amount != null) {
                return amount;
            }
        }
        return new BigDecimal(0);
    }

    @Override
    public boolean save(MemberInvestmentRewards rewards) {
        if (rewards != null) {
            rewards.setId(UF.getRandomUUID());
            rewards.setAddTime(LocalDateTime.now());
            return memberInvestmentRewardsMapper.insert(rewards) > 0 ? true : false;
        }
        return false;
    }

    @Override
    public List<RewardsTopBean> listTopThree() {
        return memberInvestmentRewardsMapper.queryTopThree();
    }

    @Override
    public List<InviteRewardsStatisticsBean> listRewardsRecords(BigDecimal amountPartOne, BigDecimal amountPartTwo, String startDate, String endDate, int offset, int size) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (amountPartOne != null) {
            param.put("amountPartOne", amountPartOne.doubleValue());
        } else {
            param.put("amountPartOne", -1);
        }
        if (amountPartTwo != null) {
            param.put("amountPartTwo", amountPartTwo.doubleValue());
        } else {
            param.put("amountPartTwo", -1);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        param.put("offset", offset);
        param.put("size", size);
        return memberInvestmentRewardsMapper.queryRewardsStatistics(param);
    }

    @Override
    public int countRewardsRecords(BigDecimal amountPartOne, BigDecimal amountPartTwo, String startDate, String endDate) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (amountPartOne != null) {
            param.put("amountPartOne", amountPartOne.doubleValue());
        } else {
            param.put("amountPartOne", -1);
        }
        if (amountPartTwo != null) {
            param.put("amountPartTwo", amountPartTwo.doubleValue());
        } else {
            param.put("amountPartTwo", -1);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        Integer num =  memberInvestmentRewardsMapper.countRewardsStatistics(param);
        return num == null ? 0 : num;
    }

    @Override
    public List<InviteRewardsRecordStatisticsBean> listRewardsRecords(String memberId, String rewardsDate, String startDate, String endDate, String mobile, String name, int offset, int size) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(rewardsDate)) {
            param.put("rewardsDate", rewardsDate);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        if (StringUtils.isNotBlank(mobile)) {
            param.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            param.put("name", name);
        }
        if (StringUtils.isNotBlank(memberId)) {
            param.put("memberId", memberId);
        }
        param.put("offset", offset);
        param.put("size", size);
        List<InviteRewardsRecordStatisticsBean> list = memberInvestmentRewardsMapper.queryRewardsRecordsStatistics(param);
        if (list != null && list.size() > 0) {
            for (InviteRewardsRecordStatisticsBean bean : list) {
                bean.setRemark(RewardsTypeEnum.get(bean.getType()));
            }
            return list;
        }
        return null;
    }

    @Override
    public int countRewardsRecords(String memberId, String rewardsDate, String startDate, String endDate, String mobile, String name) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(rewardsDate)) {
            param.put("rewardsDate", rewardsDate);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        if (StringUtils.isNotBlank(mobile)) {
            param.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(name)) {
            param.put("name", name);
        }
        if (StringUtils.isNotBlank(memberId)) {
            param.put("memberId", memberId);
        }
        return memberInvestmentRewardsMapper.countRewardsRecordsStatistics(param);
    }

    @Override
    public List<InviteRecordsBean> listInviteRecords(String mobile, String startDate, String endDate, int offset, int size) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(mobile)) {
            param.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        param.put("offset", offset);
        param.put("size", size);
        List<InviteRecordsBean> inviteRecordsBeanList = memberInvestmentRewardsMapper.queryInviteRecords(param);
        return inviteRecordsBeanList;
    }

    @Override
    public int countInviteRecords(String mobile, String startDate, String endDate) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(mobile)) {
            param.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        return memberInvestmentRewardsMapper.countInviteRecords(param);
    }

    @Override
    public int countQueryInviteRecords(String mobile, String startDate, String endDate) {
        Map<String, Object> param = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(mobile)) {
            param.put("mobile", mobile);
        }
        if (StringUtils.isNotBlank(startDate)) {
            param.put("startDate", startDate);
        }
        if (StringUtils.isNotBlank(endDate)) {
            param.put("endDate", endDate);
        }
        return memberInvestmentRewardsMapper.countQueryInviteRecords(param);
    }

    @Override
    public InviteRecordsBean getInvitation(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return memberInvestmentRewardsMapper.selectInvitationByMemberId(memberId);
        }
        return null;
    }

    @Override
    public List<RecordInfoBean> getRewardsByMemberId(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return memberInvestmentRewardsMapper.selectRewardsByMemberId(memberId);
        }
        return null;
    }

    @Override
    public int getBindCardNum(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            return memberInvestmentRewardsMapper.selectBindCardNum(memberId);
        }
        return 0;
    }

    @Override
    public List<InvitationBindedCardBean> listInvitationBindedCardRecords(String memberId, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            param.put("offset", offset);
            param.put("size", size);
            return memberInvestmentRewardsMapper.queryInvitationBindedCardRecords(param);
        }
        return null;
    }

    @Override
    public int countInvitationBindedCardRecords(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            return memberInvestmentRewardsMapper.countInvitationBindedCardRecords(param);
        }
        return 0;
    }

    @Override
    public List<InvitationBean> queryMemberRewards(String memberId, String startDate, String endDate, int offset, int size) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                param.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                param.put("endDate", endDate);
            }
            param.put("offset", offset);
            param.put("size", size);
            List<InvitationBean> list = memberInvestmentRewardsMapper.queryMemberRewards(param);
            if (list != null && list.size() > 0) {
                for (InvitationBean bean : list) {
                    BigDecimal totalAmount = bean.getBindCardAmount().add(bean.getInvestAmount()).add(bean.getBonusAmount());
                    bean.setTotalAmount(totalAmount);
                }
            }
            return list;
        }
        return null;
    }

    @Override
    public int countMemberRewards(String memberId, String startDate, String endDate) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            if (StringUtils.isNotBlank(startDate)) {
                param.put("startDate", startDate);
            }
            if (StringUtils.isNotBlank(endDate)) {
                param.put("endDate", endDate);
            }
            return memberInvestmentRewardsMapper.countMemberRewards(param);
        }
        return 0;
    }

    @Override
    public List<InvitationRewardsBean> queryInvitationRecords(String memberId, String invitationDate, int offset, int size) {
        if (StringUtils.isNotBlank(memberId) && StringUtils.isNotBlank(invitationDate)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            param.put("invitationDate", invitationDate);
            param.put("offset", offset);
            param.put("size", size);
            return memberInvestmentRewardsMapper.queryInvitationRecords(param);
        }
        return null;
    }

    @Override
    public int countInvitationRecords(String memberId, String invitationDate) {
        if (StringUtils.isNotBlank(memberId) && StringUtils.isNotBlank(invitationDate)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            param.put("invitationDate", invitationDate);
            return memberInvestmentRewardsMapper.countInvitationRecords(param);
        }
        return 0;
    }

    @Override
    public List<InviteRewardsRecordFriendBean> listFriendTotalRecords(int offset, int size, String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            param.put("offset", offset);
            param.put("size", size);
            List<InviteRewardsRecordFriendBean> list = memberInvestmentRewardsMapper.listFriendTotalRecords(param);
            /*if (!CollectionUtils.isEmpty(list)){
                list.stream().forEach(inviteFriendAwardRecordsBean -> {
                    Map<String, Object> paramReq = new HashMap<String, Object>();
                    paramReq.put("friendId", inviteFriendAwardRecordsBean.getId());
                    paramReq.put("activityInvitationId", inviteFriendAwardRecordsBean.getActivityInvitationId());
                    paramReq.put("addTime",null);
                    paramReq.put("memberId",inviteFriendAwardRecordsBean.getMemberId());
                    Map<String,Double> map = memberInvestmentRewardsMapper.countAmount(paramReq);
                    inviteFriendAwardRecordsBean.setTotalAwardAmount(map==null?BigDecimal.ZERO:new BigDecimal(String.valueOf(map.get("totalAwardAmount"))));
                    inviteFriendAwardRecordsBean.setTotalInvestAmount(map==null?BigDecimal.ZERO:new BigDecimal(String.valueOf(map.get("totalInvestAmount"))));
                });
            }*/
            return list;
        }
        return null;
    }

    @Override
    public List<InviteFriendAwardRecordsBean> listFriendAwardRecords(int offset, int size, String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            Map<String, Object> param = new HashMap<String, Object>();
            param.put("memberId", memberId);
            param.put("offset", offset);
            param.put("size", size);
            List<InviteFriendAwardRecordsBean> list = memberInvestmentRewardsMapper.listFriendAwardRecords(param);
            /*if (!CollectionUtils.isEmpty(list)){
                list.stream().forEach(inviteFriendAwardRecordsBean -> {
                    Map<String, Object> paramReq = new HashMap<String, Object>();
                    paramReq.put("friendId", inviteFriendAwardRecordsBean.getId());
                    paramReq.put("activityInvitationId", inviteFriendAwardRecordsBean.getActivityInvitationId());
                    paramReq.put("addTime",inviteFriendAwardRecordsBean.getAddTime());
                    paramReq.put("memberId",inviteFriendAwardRecordsBean.getMemberId());
                    Map<String,Double> map = memberInvestmentRewardsMapper.countAmount(paramReq);
                    inviteFriendAwardRecordsBean.setTotalAwardAmount(map==null?BigDecimal.ZERO:new BigDecimal(String.valueOf(map.get("totalAwardAmount"))));
                    inviteFriendAwardRecordsBean.setTotalInvestAmount(map==null?BigDecimal.ZERO:new BigDecimal(String.valueOf(map.get("totalInvestAmount"))));
                    inviteFriendAwardRecordsBean.setAlreadyTotalAmount(new BigDecimal(memberInvestmentRewardsMapper.alreadyTotalAmount(paramReq)));
                });
            }*/
            return list;
        }
        return null;
    }

    @Override
    public int countAwardRecords(String memberId) {
        return memberInvestmentRewardsMapper.countAwardRecords(memberId);
    }

    @Override
    public int countFriendTotalRecords(String memberId) {
        return memberInvestmentRewardsMapper.countFriendTotalRecords(memberId);
    }

    @Override
    public double selectInvestmentAmount(String memberId, int activityInvitationId) {
        if (StringUtils.isNotBlank(memberId) && activityInvitationId > 0) {
            return memberInvestmentRewardsMapper.selectInvestmentAmount(memberId, activityInvitationId);
        }
        return 0;
    }
}
