package cn.ug.activity.service.impl;

import cn.ug.activity.bean.ActivityTaskBean;
import cn.ug.activity.mapper.ActivityTaskMapper;
import cn.ug.activity.mapper.entity.ActivityTask;
import cn.ug.activity.service.ActivityTaskService;
import cn.ug.activity.service.CouponMemberService;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.enums.CouponPurposeEnum;
import cn.ug.feign.BankCardService;
import cn.ug.feign.ProductOrderService;
import cn.ug.feign.ProductService;
import cn.ug.product.bean.response.ProductBean;
import cn.ug.product.bean.response.ProductFindBean;
import cn.ug.util.UF;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
public class ActivityTaskServiceImpl implements ActivityTaskService {

    @Resource
    private CouponMemberService couponMemberService;
    @Resource
    private ProductService productService;
    @Resource
    private BankCardService bankCardService;
    @Resource
    private ProductOrderService productOrderService;
    @Resource
    private ActivityTaskMapper activityTaskMapper;

    @Override
    public List<ActivityTaskBean> findActivityTask() {
        LoginBean loginBean = LoginHelper.getLoginBean();

        if(loginBean == null){
            return getActivityTaskList();
        }

        List<ActivityTaskBean> resultList = new ArrayList<ActivityTaskBean>();

        //活动1
        ActivityTask findTask1 = activityTaskMapper.findActivityTask(loginBean.getId(), 1);
        ActivityTaskBean entity1 = new ActivityTaskBean();
        entity1.setTaskType(1);
        if(findTask1 != null){
            entity1.setIsFinish(findTask1.getIsFinish());
            entity1.setIsReceive(findTask1.getIsReceive());
        }else{
            entity1.setIsFinish(1);
            entity1.setIsReceive(1);
        }
        resultList.add(entity1);

        //活动2
        ActivityTask findTask2 = activityTaskMapper.findActivityTask(loginBean.getId(), 2);
        ActivityTaskBean entity2 = new ActivityTaskBean();
        entity2.setTaskType(2);
        if(findTask2 != null){
            entity2.setIsReceive(findTask2.getIsReceive());
        }else{
            entity2.setIsReceive(1);
        }
        //是否完成绑卡
        SerializeObject object = bankCardService.validateBindBankCard(loginBean.getId());
        if(object.getCode() == ResultType.NORMAL) {
           entity2.setIsFinish(2);
        }else{
            entity2.setIsFinish(1);
        }
        resultList.add(entity2);

        //活动3
        ActivityTask findTask3 = activityTaskMapper.findActivityTask(loginBean.getId(), 3);
        ActivityTaskBean entity3 = new ActivityTaskBean();
        entity3.setTaskType(3);
        if(findTask3 != null){
            entity3.setIsReceive(findTask3.getIsReceive());
        }else{
            entity3.setIsReceive(1);
        }
        SerializeObject<ProductFindBean> o = productService.findActivityTaskProduct();
        if(o.getCode() == ResultType.NORMAL) {
            ProductFindBean findBean = o.getData();
            entity3.setProductId(findBean.getId());
            entity3.setProductName(findBean.getName());
        }

        SerializeObject<Integer> object1 = productOrderService.findRegularActivityBuyCount(loginBean.getId());
        if(object1.getCode() == ResultType.NORMAL) {
            Integer total = object1.getData();
            if(total >0){
                entity3.setIsFinish(2);
            }else{
                entity3.setIsFinish(1);
            }
        }

        resultList.add(entity3);

        //活动4
        ActivityTask findTask4 = activityTaskMapper.findActivityTask(loginBean.getId(), 4);
        ActivityTaskBean entity4 = new ActivityTaskBean();
        entity4.setTaskType(4);
        if(findTask4 != null){
            if(entity1.getIsFinish() == 2 && entity2.getIsFinish() == 2 && entity3.getIsFinish() == 2){
                entity4.setIsReceive(2);
            }else{
                entity4.setIsReceive(1);
            }
        }else{
            entity4.setIsReceive(1);
        }
        SerializeObject<Integer> object2 = productOrderService.findRegularBuyCount(loginBean.getId());
        if(object2.getCode() == ResultType.NORMAL) {
            Integer total = object2.getData();
            if(total >0){
                entity4.setIsFinish(2);
            }else{
                entity4.setIsFinish(1);
            }
        }
        resultList.add(entity4);
        return resultList;
    }

    @Override
    public Map<String, Object> findFinishNumber() {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean == null){
            Map<String,Object> resultMap = new HashMap<String,Object>();
            resultMap.put("finishNumber", 0);
            resultMap.put("noFinishTask", 1);
            return resultMap;
        }

        int noFinishTask = 1; //默认第一个活动
        int finishNumber = 0; //完成数量
        List<ActivityTaskBean> list = findActivityTask();
        Set<Integer> set  = new HashSet();
        for(ActivityTaskBean entity:list){
            if(entity.getIsFinish() == 1){
                set.add(entity.getTaskType());
            }else{
                finishNumber ++;
            }
        }

        if(!set.isEmpty()){
            for(Integer number:set){
                noFinishTask = number; break; //取出第一个数据
            }
        }else{
            noFinishTask = 4;
        }
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("finishNumber", finishNumber);
        resultMap.put("noFinishTask", noFinishTask);
        return resultMap;
    }

    @Transactional
    @Override
    public void toComplete(Integer taskType) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("00000102");
        Ensure.that(taskType == null).isTrue("22000101");

        int total = activityTaskMapper.exists(loginBean.getId(), taskType);
        //活动1
        if(taskType == 1){
            if(total == 0){
                ActivityTask activityTask = new ActivityTask();
                activityTask.setId(UF.getRandomUUID());
                activityTask.setMemberId(loginBean.getId());
                activityTask.setTaskType(taskType);
                activityTask.setIsFinish(2);
                activityTask.setIsReceive(1);
                activityTaskMapper.insert(activityTask);
            }
        }
    }

    @Transactional
    @Override
    public void toReceive(Integer taskType) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("00000102");
        Ensure.that(taskType == null).isTrue("22000101");

        ActivityTask findTask1 = activityTaskMapper.findActivityTask(loginBean.getId(), taskType);
        if(findTask1 == null){
            ActivityTask activityTask = new ActivityTask();
            activityTask.setId(UF.getRandomUUID());
            activityTask.setMemberId(loginBean.getId());
            activityTask.setTaskType(taskType);
            activityTask.setIsFinish(2);
            activityTask.setIsReceive(2);
            int total = activityTaskMapper.insert(activityTask);
        }else{
            Map<String,Object> param = new HashMap<String,Object>();
            param.put("id",findTask1.getId());
            param.put("isFinish",2);
            param.put("isReceive",2);
            int total =activityTaskMapper.updateByPrimaryKeySelective(param);
        }
        //活动二(绑卡奖励)
        if(taskType == 2){
            couponMemberService.giveCoupons(loginBean.getId(),CouponPurposeEnum.BREAKTHROUGH_BINDED_CARD.getPurpose());
        //活动三(7日年华奖励)
        }else if(taskType == 3){
            couponMemberService.giveCoupons(loginBean.getId(),CouponPurposeEnum.BREAKTHROUGH_EXPERIENCE.getPurpose());
        //活动四(稳定金奖励)
        }else{
            couponMemberService.giveCoupons(loginBean.getId(),CouponPurposeEnum.BREAKTHROUGH_FINISH.getPurpose());
        }

    }

    /**
     * 未登录情况下
     * @return
     */
    public List<ActivityTaskBean> getActivityTaskList(){
        List<ActivityTaskBean> resultList = new ArrayList<ActivityTaskBean>();

        //活动1
        ActivityTaskBean entity1 = new ActivityTaskBean();
        entity1.setTaskType(1);
        entity1.setIsFinish(1);
        entity1.setIsReceive(1);
        resultList.add(entity1);

        //活动2
        ActivityTaskBean entity2 = new ActivityTaskBean();
        entity2.setTaskType(1);
        entity2.setIsFinish(1);
        entity2.setIsReceive(1);
        resultList.add(entity2);

        //活动3
        ActivityTaskBean entity3 = new ActivityTaskBean();
        entity3.setTaskType(1);
        entity3.setIsFinish(1);
        entity3.setIsReceive(1);
        SerializeObject<ProductFindBean> o = productService.findActivityTaskProduct();
        if(o.getCode() == ResultType.NORMAL) {
            ProductFindBean findBean = o.getData();
            entity3.setProductId(findBean.getId());
            entity3.setProductName(findBean.getName());
        }
        resultList.add(entity3);

        //活动4
        ActivityTaskBean entity4 = new ActivityTaskBean();
        entity4.setTaskType(1);
        entity4.setIsFinish(1);
        entity4.setIsReceive(1);
        resultList.add(entity4);

        return resultList;
    }
}
