package cn.ug.activity.mapper;

import cn.ug.activity.bean.GiftRecordBean;
import cn.ug.activity.bean.MemberExchangesBean;
import cn.ug.activity.mapper.entity.ActivityGiftRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ActivityGiftRecordMapper {
    List<ActivityGiftRecord> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<MemberExchangesBean> queryForExchanges(Map<String, Object> params);
    int countForExchanges(Map<String, Object> params);

    List<GiftRecordBean> queryForAppList(Map<String, Object> params);
    int queryForAppCount(Map<String, Object> params);
    GiftRecordBean findByOrderNO(@Param("orderNO")String orderNO);
    int insert(ActivityGiftRecord record);
}