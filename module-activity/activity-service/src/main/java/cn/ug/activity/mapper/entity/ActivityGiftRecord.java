package cn.ug.activity.mapper.entity;

import java.io.Serializable;

public class ActivityGiftRecord implements Serializable {
    private int id;
    private String orderNO;
    private int giftId;
    private int cardId;
    private String memberId;
    private String addTime;
    private int payBeans;

    private int index;
    private String giftName;
    private int giftType;
    private String giftTypeMark;
    private String memberName;
    private String memberMobile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public int getPayBeans() {
        return payBeans;
    }

    public void setPayBeans(int payBeans) {
        this.payBeans = payBeans;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public String getGiftTypeMark() {
        return giftTypeMark;
    }

    public void setGiftTypeMark(String giftTypeMark) {
        this.giftTypeMark = giftTypeMark;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberMobile() {
        return memberMobile;
    }

    public void setMemberMobile(String memberMobile) {
        this.memberMobile = memberMobile;
    }
}
