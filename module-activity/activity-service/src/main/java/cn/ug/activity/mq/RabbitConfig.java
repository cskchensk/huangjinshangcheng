package cn.ug.activity.mq;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static cn.ug.config.QueueName.QUEUE_ACTIVITY_REWARDS;
import static cn.ug.config.QueueName.QUEUE_COUPON_DONATION_CASH;
import static cn.ug.config.QueueName.QUEUE_ESIGN_CONTRACT;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue giveActivityRewardsQueue() {
        return new Queue(QUEUE_ACTIVITY_REWARDS, true);
    }

    @Bean
    public Queue donationCashQueue() {
        return new Queue(QUEUE_COUPON_DONATION_CASH, true);
    }

    @Bean
    public Queue esignContractQueue() {
        return new Queue(QUEUE_ESIGN_CONTRACT, true);
    }
}
