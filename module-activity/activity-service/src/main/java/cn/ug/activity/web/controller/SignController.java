package cn.ug.activity.web.controller;

import cn.ug.activity.bean.SignInfoBean;
import cn.ug.activity.mapper.entity.ActivitySign;
import cn.ug.activity.mapper.entity.ActivitySignHistory;
import cn.ug.activity.service.SignService;
import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Order;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.util.ExportExcelUtil;
import cn.ug.web.controller.BaseController;
import cn.ug.web.controller.ExportExcelController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 用户签到控制器
 */
@RestController
@RequestMapping("sign")
public class SignController  extends BaseController {

    @Autowired
    private SignService signService;

    /**
     * 用户签到信息列表
     * @param accessToken
     * @param page
     * @param memberMobile  手机号
     * @param memberName    姓名
     * @param totalSignMin 已连续签到天数
     * @param totalSignMax
     * @param totalGoldBeanMin 签到所得金豆数
     * @param totalGoldBeanMax
     * @param order
     * @return
     */
    @GetMapping(value = "/list")
    public SerializeObject list(@RequestHeader String accessToken, Page page, String memberMobile, String memberName,
                                Integer totalSignMin, Integer totalSignMax, Integer totalGoldBeanMin, Integer totalGoldBeanMax,Order order) {
        return new SerializeObject<>(ResultType.NORMAL,signService.querySignList(page.getPageNum(),page.getPageSize(),memberMobile,memberName,
                totalSignMin,totalSignMax,totalGoldBeanMin,totalGoldBeanMax,order));
    }

    /**
     * 签到历史记录查询
     * @param accessToken
     * @param page
     * @param memberMobile 手机号
     * @param memberName   姓名
     * @return
     */
    @GetMapping(value = "/history/list")
    public SerializeObject historyList(@RequestHeader String accessToken, Page page, String memberMobile, String memberName,Order order) {
        return new SerializeObject<>(ResultType.NORMAL,signService.historyList(page.getPageNum(),page.getPageSize(),memberMobile,memberName,order));
    }

    /**
     * 用户签到记录表导出
     * @param memberMobile
     * @param memberName
     * @param totalSignMin
     * @param totalSignMax
     * @param totalGoldBeanMin
     * @param totalGoldBeanMax
     * @param order
     * @param response
     */
    @GetMapping(value = "/list/export")
    public void listExport(String memberMobile, String memberName,
                           Integer totalSignMin, Integer totalSignMax, Integer totalGoldBeanMin,
                           Integer totalGoldBeanMax,Order order,HttpServletResponse response) {
        DataTable<ActivitySign> resultList = signService.querySignList(1,Integer.MAX_VALUE,memberMobile,memberName,
                totalSignMin,totalSignMax,totalGoldBeanMin,totalGoldBeanMax,order);
        String fileName = "用户签到记录表";
        String[] columnNames = { "序号", "手机号","用户姓名", "已连续签到天数（天）","签到所得金豆数（个）"};
        String[] columns = {"index",  "memberMobile", "memberName", "continueSign","totalGoldBean"};
        int index = 1;
        for (ActivitySign activitySign : resultList.getDataList()){
            activitySign.setIndex(index);
            index++;
        }
        ExportExcelController<ActivitySign> export = new ExportExcelController<>();
        export.exportExcel(fileName, fileName, columnNames, columns, resultList.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }

    /**
     * 用户签到记录详表导出
     * @param page
     * @param memberMobile
     * @param memberName
     * @param response
     */
    @GetMapping(value = "/history/list/export")
    public void listExport(Page page, String memberMobile, String memberName,Order order,HttpServletResponse response) {
        DataTable<ActivitySignHistory> resultList = signService.historyList(page.getPageNum(),page.getPageSize(),memberMobile,memberName,order);
        String fileName = "用户签到记录详表";
        String[] columnNames = { "序号", "签到时间","签到获得金豆"};
        String[] columns = {"index", "signDate","goldBean"};
        int index = 1;
        for (ActivitySignHistory activitySignHistory : resultList.getDataList()){
            activitySignHistory.setIndex(index);
            index++;
        }
        ExportExcelController<ActivitySignHistory> export = new ExportExcelController<>();
        export.exportExcel(fileName, fileName, columnNames, columns, resultList.getDataList(), response, ExportExcelUtil.EXCEL_FILE_2003);
    }


    /*************************App*************************************/
    @GetMapping(value = "/query/sign/info")
    public SerializeObject myBean(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        SignInfoBean signInfoBean = signService.mySignInfo(loginBean.getId());
        return new SerializeObject<>(ResultType.NORMAL,signInfoBean);
    }

    /**
     * 用户签到
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "/apply" , method = POST)
    public SerializeObject apply(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        return signService.sign(loginBean.getId());
    }

}
