package cn.ug.activity.service.impl;

import cn.ug.activity.bean.BonusBean;
import cn.ug.activity.mapper.BonusMapper;
import cn.ug.activity.mapper.BonusStrategyMapper;
import cn.ug.activity.mapper.ProductBonusMappingMapper;
import cn.ug.activity.mapper.entity.Bonus;
import cn.ug.activity.mapper.entity.BonusStrategy;
import cn.ug.activity.mapper.entity.ProductBonusMapping;
import cn.ug.activity.service.BonusService;
import cn.ug.aop.RemoveCache;
import cn.ug.aop.SaveCache;
import cn.ug.service.impl.BaseServiceImpl;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.ug.config.CacheType.OBJECT;

@Service
public class BonusServiceImpl extends BaseServiceImpl implements BonusService {
    @Autowired
    private BonusMapper bonusMapper;
    @Autowired
    private BonusStrategyMapper bonusStrategyMapper;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;
    @Autowired
    private ProductBonusMappingMapper productBonusMappingMapper;

    @Override
    public ProductBonusMapping findByProductId(String productId) {
        if (StringUtils.isNotBlank(productId)) {
            return productBonusMappingMapper.findByProductId(productId);
        }
        return null;
    }

    @Override
    public List<BonusStrategy> listStrategies(String bonusId) {
        if (StringUtils.isNotBlank(bonusId)) {
            return bonusStrategyMapper.findByBonusId(bonusId);
        }
        return null;
    }

    @Override
    @RemoveCache(cleanSearch = true)
    public boolean save(BonusBean entityBean) {
        if (entityBean != null) {
            Bonus entity = dozerBeanMapper.map(entityBean, Bonus.class);
            if (StringUtils.isBlank(entityBean.getId())) {
                String id = UF.getRandomUUID();
                entity.setId(id);
                entity.setAddTime(LocalDateTime.now());
                boolean result = bonusMapper.insert(entity) > 0 ? true : false;
                if (result && entity.getStrategies() != null && entity.getStrategies().size() > 0) {
                    entity.getStrategies().forEach(strategy -> {
                        strategy.setId(UF.getRandomUUID());
                        strategy.setBonusId(id);
                    });
                    return bonusStrategyMapper.insertInBatch(entity.getStrategies()) > 0 ? true : false;
                }
            } else {
                entity.setModifyTime(LocalDateTime.now());
                boolean result = bonusMapper.update(entity) > 0 ? true : false;
                if (result && entity.getStrategies() != null && entity.getStrategies().size() > 0) {
                    bonusStrategyMapper.deleteByBonusId(entity.getId());
                    entity.getStrategies().forEach(strategy -> {
                        strategy.setId(UF.getRandomUUID());
                        strategy.setBonusId(entity.getId());
                    });
                    return bonusStrategyMapper.insertInBatch(entity.getStrategies()) > 0 ? true : false;
                }
            }
        }
        return false;
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public boolean remove(String id) {
        if (StringUtils.isNotBlank(id)) {
            boolean result = bonusMapper.delete(id) > 0 ? true : false;
            if (result) {
                bonusStrategyMapper.removeByBonusId(id);
            }
            return result;
        }
        return false;
    }

    @Override
    @RemoveCache(cleanSearch = true, cleanObjectByKeyPosition = 0)
    public boolean removeInBatch(String[] id) {
        if (id != null && id.length > 0) {
            boolean result = bonusMapper.deleteByIds(id) > 0 ? true : false;
            if (result) {
                for (int i = 0; i < id.length; i ++) {
                    bonusStrategyMapper.removeByBonusId(id[i]        );
                }
            }
            return result;
        }
        return false;
    }

    @Override
    @SaveCache(cacheType = OBJECT)
    public BonusBean get(String id) {
        if (StringUtils.isNoneBlank(id)) {
            Bonus entity = bonusMapper.findById(id);
            if (entity == null) {
                return null;
            }
            BonusBean entityBean = dozerBeanMapper.map(entity, BonusBean.class);
            if (entity.getAddTime() != null) {
                entityBean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
            }
            if (entity.getModifyTime() != null) {
                entityBean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
            }
            return entityBean;
        }
        return null;
    }

    @Override
    public List<BonusBean> listUsableBonus() {
        List<Bonus> list = bonusMapper.queryUsableBonus();
        if (list == null || list.size() == 0) {
            return null;
        }
        List<BonusBean> data = new ArrayList<BonusBean>();
        for (Bonus entity : list) {
            BonusBean bean = dozerBeanMapper.map(entity, BonusBean.class);
            if (entity.getAddTime() != null) {
                bean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
            }
            if (entity.getModifyTime() != null) {
                bean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
            }
            data.add(bean);
        }
        return data;
    }

    @Override
    public List<BonusBean> listRecords(String name, int type, int offset, int size) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(name)) {
            params.put("name", name);
        }
        params.put("type", type);
        params.put("offset", offset);
        params.put("size", size);
        List<Bonus> list = bonusMapper.query(params);
        if (list == null || list.size() == 0) {
            return null;
        }
        List<BonusBean> data = new ArrayList<BonusBean>();
        for (Bonus entity : list) {
            BonusBean bean = dozerBeanMapper.map(entity, BonusBean.class);
            if (entity.getAddTime() != null) {
                bean.setAddTimeString(UF.getFormatDateTime(entity.getAddTime()));
            }
            if (entity.getModifyTime() != null) {
                bean.setModifyTimeString(UF.getFormatDateTime(entity.getModifyTime()));
            }
            data.add(bean);
        }
        return data;
    }

    @Override
    public int countRecords(String name, int type) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNoneBlank(name)) {
            params.put("name", name);
        }
        params.put("type", type);
        return bonusMapper.count(params);
    }

    @Override
    public boolean modifyStatus(String[] id, int status) {
        if (id == null || id.length == 0) {
            return false;
        }
        if (status == 2 || status == 1) {
            return bonusMapper.updateStatus(id, status) > 0 ? true : false;
        }
        return false;
    }
}