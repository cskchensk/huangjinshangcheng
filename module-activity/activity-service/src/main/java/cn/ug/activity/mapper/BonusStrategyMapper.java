package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.BonusStrategy;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BonusStrategyMapper {
    List<BonusStrategy> findByBonusId(@Param("bonusId") String bonusId);
    int insertInBatch(@Param("strategies") List<BonusStrategy> strategies);
    int removeByBonusId(@Param("bonusId") String bonusId);
    int deleteByBonusId(@Param("bonusId") String bonusId);
}
