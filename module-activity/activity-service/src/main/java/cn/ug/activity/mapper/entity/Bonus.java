package cn.ug.activity.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Bonus extends BaseEntity implements Serializable {
    private String name;
    private int type;
    private int status;
    private int deleted;
    private List<BonusStrategy> strategies = new ArrayList<BonusStrategy>();

    public Bonus() {

    }

    public List<BonusStrategy> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<BonusStrategy> strategies) {
        this.strategies = strategies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
