package cn.ug.activity.service.impl;

import cn.ug.activity.mapper.ContractMapper;
import cn.ug.activity.mapper.entity.Contract;
import cn.ug.activity.service.ContractService;
import cn.ug.util.UF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ContractServiceImpl implements ContractService {
    @Autowired
    private ContractMapper contractMapper;

    @Override
    public boolean save(Contract contract) {
        if (contract != null) {
            if (StringUtils.isNotBlank(contract.getId())) {
                // TODO nothing.
            } else {
                contract.setId(UF.getRandomUUID());
                contract.setAddTime(LocalDateTime.now());
                return contractMapper.insert(contract) > 0 ? true : false;
            }
        }
        return false;
    }

    @Override
    public Contract get(String orderId) {
        if (StringUtils.isNotBlank(orderId)) {
            return  contractMapper.findByOrderId(orderId);
        }
        return null;
    }
}
