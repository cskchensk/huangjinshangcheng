package cn.ug.activity.service;

import cn.ug.activity.mapper.entity.ActivityInvitation;
import cn.ug.activity.mapper.entity.ActivityInvitationStrategy;
import cn.ug.activity.mapper.entity.ActivityInvitationTop;

import java.util.List;

public interface ActivityInvitationService {
    List<ActivityInvitation> query(int offset, int size);
    int count(String someday);

    int save(ActivityInvitation activityInvitation, List<ActivityInvitationStrategy> strategies);

    boolean save(List<ActivityInvitationTop> tops);

    boolean close(int activityInvitationId);
    boolean delete(int activityInvitationId);

    ActivityInvitation get(int activityInvitationId);

    ActivityInvitation selectValidActivity();

    List<ActivityInvitationStrategy> queryForList(int activityInvitationId);
    List<ActivityInvitationStrategy> queryForLatestList();

    List<ActivityInvitationTop> queryTop10(int activityInvitationId, String startDate, String endDate);



}
