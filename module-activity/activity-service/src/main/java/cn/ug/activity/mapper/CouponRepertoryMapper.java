package cn.ug.activity.mapper;

import cn.ug.activity.mapper.entity.CouponRepertory;
import cn.ug.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CouponRepertoryMapper extends BaseMapper<CouponRepertory> {
    int count(Map<String, Object> params);
    int updateStatus(@Param("id") String[] id, @Param("status") int status);
    List<CouponRepertory> queryUsableCoupons(@Param("category") int category, @Param("type") int type, @Param("groupOf") int groupOf);
    List<CouponRepertory> findByIds(@Param("ids")List<Integer> ids);

    List<Integer> listForDayGift();

    List<CouponRepertory> listUsableCouponsByDay(@Param("category") int category, @Param("type") int type,@Param("day") int day);
}
