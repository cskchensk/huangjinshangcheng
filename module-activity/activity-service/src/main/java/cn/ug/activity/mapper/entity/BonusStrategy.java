package cn.ug.activity.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

public class BonusStrategy extends BaseEntity implements Serializable {
    private int quota;
    private String bonusId;
    private String couponId;
    private double amount;
    private int deleted;

    public BonusStrategy() {

    }

    public int getQuota() {
        return quota;
    }

    public void setQuota(int quota) {
        this.quota = quota;
    }

    public String getBonusId() {
        return bonusId;
    }

    public void setBonusId(String bonusId) {
        this.bonusId = bonusId;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
