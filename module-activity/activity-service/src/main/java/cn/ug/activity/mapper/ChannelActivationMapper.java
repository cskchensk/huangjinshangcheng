package cn.ug.activity.mapper;

import cn.ug.activity.bean.ChannelActivationBean;
import org.apache.ibatis.annotations.Param;

public interface ChannelActivationMapper {
    ChannelActivationBean findByUuid(@Param("uuid")String uuid);
    int updateStatus(@Param("uuid")String uuid);
    int insert(ChannelActivationBean channelActivationBean);
}
