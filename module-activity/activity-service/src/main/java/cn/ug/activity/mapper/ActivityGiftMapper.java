package cn.ug.activity.mapper;

import cn.ug.activity.bean.GiftBean;
import cn.ug.activity.bean.GiftStatisticsBean;
import cn.ug.activity.mapper.entity.ActivityGift;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface ActivityGiftMapper {
    ActivityGift findById(@Param("id")int id);

    int insert(ActivityGift gift);
    int update(ActivityGift gift);
    int updateAfterOnline(ActivityGift gift);
    int hot(@Param("id")int id, @Param("hot")int hot);
    int updateStatus(ActivityGift gift);
    int decrease(@Param("id")int id);

    List<ActivityGift> query(Map<String, Object> params);
    int count(Map<String, Object> params);

    List<GiftStatisticsBean> queryForExchanges(Map<String, Object> params);
    int countForExchanges(Map<String, Object> params);

    List<GiftBean> queryForHotList();
    int countForHot();
    List<GiftBean> queryForAppList(Map<String, Object> params);
    int queryForAppCount(Map<String, Object> params);
    GiftBean findBeanById(@Param("id")int id);
}
