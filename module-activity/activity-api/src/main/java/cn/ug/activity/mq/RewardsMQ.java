package cn.ug.activity.mq;

import java.io.Serializable;
import java.math.BigDecimal;

public class RewardsMQ implements Serializable {
    /**产品id**/
    private String productId;
    /**投资定期产品次数，如果等于1，代表首次投资定期产品**/
    private int investTimes;
    /**投资总金额**/
    private BigDecimal payTotalAmount;
    /**场景：1-代表绑卡（如果是绑卡productId， investTimes，payTotalAmount可为空）；
     *       2-购买实物金完成
     *       3-购买体验金完成
     *       4-回租完成**/
    private int scene;
    /**用户id**/
    private String memberId;
    /** 投资订单号 **/
    private String orderId;
    /** 交易克重 **/
    private BigDecimal amount;
    /** 回租天数 **/
    private int leasebackDays;

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getInvestTimes() {
        return investTimes;
    }

    public void setInvestTimes(int investTimes) {
        this.investTimes = investTimes;
    }

    public BigDecimal getPayTotalAmount() {
        return payTotalAmount;
    }

    public void setPayTotalAmount(BigDecimal payTotalAmount) {
        this.payTotalAmount = payTotalAmount;
    }

    public int getScene() {
        return scene;
    }

    public void setScene(int scene) {
        this.scene = scene;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
