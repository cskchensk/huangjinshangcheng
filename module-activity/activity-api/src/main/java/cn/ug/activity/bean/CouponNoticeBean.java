package cn.ug.activity.bean;

import java.io.Serializable;

public class CouponNoticeBean implements Serializable {
    private String memberId;
    private String mobile;
    private int precedeDays;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getPrecedeDays() {
        return precedeDays;
    }

    public void setPrecedeDays(int precedeDays) {
        this.precedeDays = precedeDays;
    }
}
