package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class CouponStatisticsBean implements Serializable {
    private String memberId;
    private String name;
    private String mobile;
    private int couponNum;
    private int usedNum;
    private int invalidationNum;
    private int usablenessNum;
    //累计获得黄金红包奖励
    private int totalAwardAmount;
    //已使用黄金红包克重
    private int usedGram;
    //折算奖励金额
    private BigDecimal discountAwardAmount;
    //黄金红包使用率
    private BigDecimal usedRate;

    private int index;

    public CouponStatisticsBean() {

    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(int couponNum) {
        this.couponNum = couponNum;
    }

    public int getUsedNum() {
        return usedNum;
    }

    public void setUsedNum(int usedNum) {
        this.usedNum = usedNum;
    }

    public int getInvalidationNum() {
        return invalidationNum;
    }

    public void setInvalidationNum(int invalidationNum) {
        this.invalidationNum = invalidationNum;
    }

    public int getUsablenessNum() {
        return usablenessNum;
    }

    public void setUsablenessNum(int usablenessNum) {
        this.usablenessNum = usablenessNum;
    }

    public int getTotalAwardAmount() {
        return totalAwardAmount;
    }

    public void setTotalAwardAmount(int totalAwardAmount) {
        this.totalAwardAmount = totalAwardAmount;
    }

    public int getUsedGram() {
        return usedGram;
    }

    public void setUsedGram(int usedGram) {
        this.usedGram = usedGram;
    }

    public BigDecimal getDiscountAwardAmount() {
        return discountAwardAmount;
    }

    public void setDiscountAwardAmount(BigDecimal discountAwardAmount) {
        this.discountAwardAmount = discountAwardAmount;
    }

    public BigDecimal getUsedRate() {
        return usedRate;
    }

    public void setUsedRate(BigDecimal usedRate) {
        this.usedRate = usedRate;
    }
}
