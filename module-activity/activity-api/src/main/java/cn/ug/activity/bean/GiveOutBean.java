package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class GiveOutBean implements Serializable {
    private String time;
    private BigDecimal couponAmount;
    private BigDecimal totalCouponAmount;
    private BigDecimal ticketAmount;
    private BigDecimal totalTicketAmount;
    private BigDecimal rewardAmount;
    private BigDecimal totalRewardAmount;

    public GiveOutBean() {

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getTotalCouponAmount() {
        return totalCouponAmount;
    }

    public void setTotalCouponAmount(BigDecimal totalCouponAmount) {
        this.totalCouponAmount = totalCouponAmount;
    }

    public BigDecimal getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(BigDecimal ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public BigDecimal getTotalTicketAmount() {
        return totalTicketAmount;
    }

    public void setTotalTicketAmount(BigDecimal totalTicketAmount) {
        this.totalTicketAmount = totalTicketAmount;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public BigDecimal getTotalRewardAmount() {
        return totalRewardAmount;
    }

    public void setTotalRewardAmount(BigDecimal totalRewardAmount) {
        this.totalRewardAmount = totalRewardAmount;
    }
}
