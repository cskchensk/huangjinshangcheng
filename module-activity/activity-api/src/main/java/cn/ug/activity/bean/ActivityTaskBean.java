package cn.ug.activity.bean;

import java.io.Serializable;

/**
 * 活动任务完成
 */
public class ActivityTaskBean implements Serializable{


    /** 任务类型 1:了解柚子金 2:认证绑卡 3:7天活动产品 4:预订稳赢金产品 **/
    private Integer taskType;
    /** 是否完成 1:未完成 2:已完成 **/
    private Integer isFinish;
    /** 是否领取 1:未领取 2:已领取 **/
    private Integer isReceive;
    /** 产品id **/
    private String productId;
    /** 产品名称 **/
    private String productName;

    public Integer getTaskType() {
        return taskType;
    }

    public void setTaskType(Integer taskType) {
        this.taskType = taskType;
    }

    public Integer getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Integer isFinish) {
        this.isFinish = isFinish;
    }

    public Integer getIsReceive() {
        return isReceive;
    }

    public void setIsReceive(Integer isReceive) {
        this.isReceive = isReceive;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
