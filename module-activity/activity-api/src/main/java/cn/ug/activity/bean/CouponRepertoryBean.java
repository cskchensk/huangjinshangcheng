package cn.ug.activity.bean;

import java.io.Serializable;

public class CouponRepertoryBean implements Serializable {
    private String id;
    private String name;
    private int type;
    private String amount;
    private int indateType;
    private int indateDays;
    private String indateTime;
    private String addTime;
    private String modifyTime;
    private int financePeriod;
    private int donation;
    private int tradeAmountType;
    private int tradeAmount;
    private String useCondition;
    private int increaseDays;
    private int status;
    private int deleted;
    private int purpose;
    private int discountAmount;

    public CouponRepertoryBean() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getIndateType() {
        return indateType;
    }

    public void setIndateType(int indateType) {
        this.indateType = indateType;
    }

    public int getIndateDays() {
        return indateDays;
    }

    public void setIndateDays(int indateDays) {
        this.indateDays = indateDays;
    }

    public String getIndateTime() {
        return indateTime;
    }

    public void setIndateTime(String indateTime) {
        this.indateTime = indateTime;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public int getFinancePeriod() {
        return financePeriod;
    }

    public void setFinancePeriod(int financePeriod) {
        this.financePeriod = financePeriod;
    }

    public int getDonation() {
        return donation;
    }

    public void setDonation(int donation) {
        this.donation = donation;
    }

    public int getTradeAmountType() {
        return tradeAmountType;
    }

    public void setTradeAmountType(int tradeAmountType) {
        this.tradeAmountType = tradeAmountType;
    }

    public int getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(int tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getUseCondition() {
        return useCondition;
    }

    public void setUseCondition(String useCondition) {
        this.useCondition = useCondition;
    }

    public int getIncreaseDays() {
        return increaseDays;
    }

    public void setIncreaseDays(int increaseDays) {
        this.increaseDays = increaseDays;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public int getPurpose() {
        return purpose;
    }

    public void setPurpose(int purpose) {
        this.purpose = purpose;
    }

    public int getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(int discountAmount) {
        this.discountAmount = discountAmount;
    }
}
