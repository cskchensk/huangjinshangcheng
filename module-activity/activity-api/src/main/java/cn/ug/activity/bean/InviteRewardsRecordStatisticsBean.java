package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InviteRewardsRecordStatisticsBean implements Serializable {
    private String id;
    private String memberId;
    private String mobile;
    private String name;
    private String friendMobile;
    private String friendName;
    private int type;
    private BigDecimal amount;
    private String friendRegisterTime;
    private String friendInvestTime;
    private BigDecimal investAmount;
    private String productName;
    private int index;
    private String remark;

    public InviteRewardsRecordStatisticsBean() {

    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(BigDecimal investAmount) {
        this.investAmount = investAmount;
    }

    public String getFriendInvestTime() {
        return friendInvestTime;
    }

    public void setFriendInvestTime(String friendInvestTime) {
        this.friendInvestTime = friendInvestTime;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendMobile() {
        return friendMobile;
    }

    public void setFriendMobile(String friendMobile) {
        this.friendMobile = friendMobile;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getFriendRegisterTime() {
        return friendRegisterTime;
    }

    public void setFriendRegisterTime(String friendRegisterTime) {
        this.friendRegisterTime = friendRegisterTime;
    }
}
