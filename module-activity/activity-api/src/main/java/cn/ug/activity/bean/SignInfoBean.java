package cn.ug.activity.bean;

import java.io.Serializable;
import java.util.List;

public class SignInfoBean implements Serializable {

    /**
     * 我的总金豆数量
     */
    private Integer goldBean;

    /**
     * 累计连续签到天数
     */
    private Integer totalSign;

    /**
     * 连续签到天数
     */
    private Integer continueSign;
    /**
     * 当天签到状态
     * 0：未签到
     * 1：已签到
     */
    private Integer signStatus;

    /**
     * 签到日期
     */
    private List<SignDays> signDaysList;

    public static  class SignDays{
        private String day;

        private Integer goldBean;

        private Integer type;

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public Integer getGoldBean() {
            return goldBean;
        }

        public void setGoldBean(Integer goldBean) {
            this.goldBean = goldBean;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

    }

    public Integer getGoldBean() {
        return goldBean;
    }

    public void setGoldBean(Integer goldBean) {
        this.goldBean = goldBean;
    }

    public Integer getTotalSign() {
        return totalSign;
    }

    public void setTotalSign(Integer totalSign) {
        this.totalSign = totalSign;
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public List<SignDays> getSignDaysList() {
        return signDaysList;
    }

    public void setSignDaysList(List<SignDays> signDaysList) {
        this.signDaysList = signDaysList;
    }

    public Integer getContinueSign() {
        return continueSign;
    }

    public void setContinueSign(Integer continueSign) {
        this.continueSign = continueSign;
    }
}

