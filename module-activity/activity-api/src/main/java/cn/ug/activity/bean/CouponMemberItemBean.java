package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class CouponMemberItemBean implements Serializable {
    private int id;
    private String mobile;
    private String name;
    private String memberId;
    private String sendTime;
    private String indateTime;
    private String couponName;
    private BigDecimal amount;
    private BigDecimal yearIncome;
    private int status;
    private String usedTime;
    private String orderNO;
    //对应加息年化(%)
    private BigDecimal raiseYearIncome;
    //折算奖励金额
    private BigDecimal discountAmount;
    //回租周期（天）
    private Integer leasebackdays;
    //订单克重
    private Integer orderGram;

    private int index;
    private String statusMark;

    public CouponMemberItemBean() {

    }

    public BigDecimal getYearIncome() {
        return yearIncome;
    }

    public void setYearIncome(BigDecimal yearIncome) {
        this.yearIncome = yearIncome;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getStatusMark() {
        return statusMark;
    }

    public void setStatusMark(String statusMark) {
        this.statusMark = statusMark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getIndateTime() {
        return indateTime;
    }

    public void setIndateTime(String indateTime) {
        this.indateTime = indateTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(String usedTime) {
        this.usedTime = usedTime;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public BigDecimal getRaiseYearIncome() {
        return raiseYearIncome;
    }

    public void setRaiseYearIncome(BigDecimal raiseYearIncome) {
        this.raiseYearIncome = raiseYearIncome;
    }

    public Integer getLeasebackdays() {
        return leasebackdays;
    }

    public void setLeasebackdays(Integer leasebackdays) {
        this.leasebackdays = leasebackdays;
    }

    public Integer getOrderGram() {
        return orderGram;
    }

    public void setOrderGram(Integer orderGram) {
        this.orderGram = orderGram;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }
}
