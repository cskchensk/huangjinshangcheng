package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InviteRewardsRecordFriendBean implements Serializable {

    private String id;
    private String memberId;
    private String mobile;
    private String name;
    private String friendMobile;
    private String friendName;
    /**
     * 方案
     */
    private String scheme;

    /**
     * 累计投资金额
     */
    private BigDecimal totalInvestAmount;

    /**
     * 累计奖励金额
     */
    private BigDecimal totalAwardAmount;

    /**
     * 活动邀请id
     */
    private Integer activityInvitationId;

    /**
     * 累计已发奖励
     */
    private BigDecimal alreadyTotalAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendMobile() {
        return friendMobile;
    }

    public void setFriendMobile(String friendMobile) {
        this.friendMobile = friendMobile;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public BigDecimal getTotalInvestAmount() {
        return totalInvestAmount;
    }

    public void setTotalInvestAmount(BigDecimal totalInvestAmount) {
        this.totalInvestAmount = totalInvestAmount;
    }

    public BigDecimal getTotalAwardAmount() {
        return totalAwardAmount;
    }

    public void setTotalAwardAmount(BigDecimal totalAwardAmount) {
        this.totalAwardAmount = totalAwardAmount;
    }

    public Integer getActivityInvitationId() {
        return activityInvitationId;
    }

    public void setActivityInvitationId(Integer activityInvitationId) {
        this.activityInvitationId = activityInvitationId;
    }

    public BigDecimal getAlreadyTotalAmount() {
        return alreadyTotalAmount;
    }

    public void setAlreadyTotalAmount(BigDecimal alreadyTotalAmount) {
        this.alreadyTotalAmount = alreadyTotalAmount;
    }
}
