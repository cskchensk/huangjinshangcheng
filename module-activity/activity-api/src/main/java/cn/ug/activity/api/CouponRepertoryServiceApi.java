package cn.ug.activity.api;

import cn.ug.activity.bean.CouponMemberBean;
import cn.ug.activity.bean.CouponRepertoryBean;
import cn.ug.bean.base.SerializeObject;
import cn.ug.member.bean.MemberDetailBean;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RequestMapping("coupon")
public interface CouponRepertoryServiceApi {

    @GetMapping(value = "/usable/info")
    SerializeObject<MemberDetailBean> getUsableInfo(@RequestParam("memberId")String memberId);

    @GetMapping(value = "/{id}")
    SerializeObject<CouponRepertoryBean> get(@RequestParam("id") String id);

    @PostMapping("/consume")
    SerializeObject consumeCoupon(@RequestParam("couponId") String couponId,
                                  @RequestParam("discountAmount")BigDecimal discountAmount,
                                  @RequestParam("orderNO")String orderNO);

    @PutMapping("/lock")
    SerializeObject lock(@RequestParam("couponId")String couponId);

    @PutMapping("/unlock")
    SerializeObject unlock(@RequestParam("couponId")String couponId);

    @PostMapping(value = "/give/coupons")
    public SerializeObject giveCoupons(@RequestParam("userId")String userId,
                                       @RequestParam("trigger")int trigger,
                                       @RequestParam("mobile")String mobile);

    @PostMapping(value = "/give/coupon")
    SerializeObject giveCoupon(@RequestParam("userId")String userId,
                                      @RequestParam("couponId")String couponId);

    @GetMapping(value = "/member/coupon")
    SerializeObject<CouponMemberBean> getMemberCoupon(@RequestParam("couponId")String couponId);

    @GetMapping(value = "/findCouponById")
    SerializeObject<CouponRepertoryBean> findCouponById(@RequestParam("id") int id);
}
