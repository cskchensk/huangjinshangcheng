package cn.ug.activity.api;

import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("coupon/donation")
public interface CouponDonationServiceApi {
    @GetMapping(value = "/mobiles")
    SerializeObject<String> getMobiles(@RequestParam("id")String id);
}
