package cn.ug.activity.bean;

import java.io.Serializable;

public class GiftStatisticsBean implements Serializable {
    private int giftId;
    private String giftName;
    private int giftType;
    private int status;
    private int exchanges;

    private int index;
    private String giftTypeMark;
    private String statusMark;

    public int getGiftId() {
        return giftId;
    }

    public void setGiftId(int giftId) {
        this.giftId = giftId;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public int getGiftType() {
        return giftType;
    }

    public void setGiftType(int giftType) {
        this.giftType = giftType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getExchanges() {
        return exchanges;
    }

    public void setExchanges(int exchanges) {
        this.exchanges = exchanges;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getGiftTypeMark() {
        return giftTypeMark;
    }

    public void setGiftTypeMark(String giftTypeMark) {
        this.giftTypeMark = giftTypeMark;
    }

    public String getStatusMark() {
        return statusMark;
    }

    public void setStatusMark(String statusMark) {
        this.statusMark = statusMark;
    }
}
