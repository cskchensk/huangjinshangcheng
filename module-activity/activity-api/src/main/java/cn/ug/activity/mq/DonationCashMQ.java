package cn.ug.activity.mq;

import java.io.Serializable;
import java.math.BigDecimal;

public class DonationCashMQ implements Serializable {
    /** couponId 优惠券id
        couponAmount  发放金额（红包金额或加息券折算金额）
        goldAmount    加息券折算克重**/
    private String couponId;
    private BigDecimal couponAmount;
    private BigDecimal goldAmount;
    private String productName;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public BigDecimal getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(BigDecimal couponAmount) {
        this.couponAmount = couponAmount;
    }

    public BigDecimal getGoldAmount() {
        return goldAmount;
    }

    public void setGoldAmount(BigDecimal goldAmount) {
        this.goldAmount = goldAmount;
    }
}
