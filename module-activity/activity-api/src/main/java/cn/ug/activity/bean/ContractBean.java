package cn.ug.activity.bean;

public class ContractBean {
    private String id;
    private String orderId;
    private String thirdpartyUrl;
    private String originalUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getThirdpartyUrl() {
        return thirdpartyUrl;
    }

    public void setThirdpartyUrl(String thirdpartyUrl) {
        this.thirdpartyUrl = thirdpartyUrl;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }
}
