package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberDonationCouponBean implements Serializable {
    private String couponId;
    private String name;
    private String mobile;
    private BigDecimal amount;
    private BigDecimal grantAmount;
    private String grantTime;
    private BigDecimal grantGold;
    private int index;

    public MemberDonationCouponBean() {

    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getGrantAmount() {
        return grantAmount;
    }

    public void setGrantAmount(BigDecimal grantAmount) {
        this.grantAmount = grantAmount;
    }

    public String getGrantTime() {
        return grantTime;
    }

    public void setGrantTime(String grantTime) {
        this.grantTime = grantTime;
    }

    public BigDecimal getGrantGold() {
        return grantGold;
    }

    public void setGrantGold(BigDecimal grantGold) {
        this.grantGold = grantGold;
    }
}
