package cn.ug.activity.mq;

import java.io.Serializable;
import java.math.BigDecimal;

public class ContractMQ implements Serializable {
    // 申购产品id
    private String productId;
    // 申购产品名称
    private String productName;
    // 产品类型
    private int productType;
    // 申购产品理财期限
    private int investDay;
    // 下单日期（yyyy-MM-dd）
    private String tradeDate;
    // 申购会员id
    private String memberId;
    // 申购黄金单价
    private BigDecimal price;
    // 黄金克重
    private BigDecimal weight;
    // 产品规格
    private int specification;
    // 交易总价
    private BigDecimal tradeAmount;
    // 交易订单编号
    private String orderNO;
    // 是否是活动产品
    private int isActivity;
    /*********************安稳金合同需要参数*************************/
    //回租开始时间
    private String lbStartTime;
    //回租结束时间
    private String lbEndTime;
    //预计收益克重
    private BigDecimal incomeGram;
    public ContractMQ() {

    }

    public int getSpecification() {
        return specification;
    }

    public void setSpecification(int specification) {
        this.specification = specification;
    }

    public int getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(int isActivity) {
        this.isActivity = isActivity;
    }

    public String getOrderNO() {
        return orderNO;
    }

    public void setOrderNO(String orderNO) {
        this.orderNO = orderNO;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getInvestDay() {
        return investDay;
    }

    public void setInvestDay(int investDay) {
        this.investDay = investDay;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }


    public BigDecimal getIncomeGram() {
        return incomeGram;
    }

    public void setIncomeGram(BigDecimal incomeGram) {
        this.incomeGram = incomeGram;
    }

    public String getLbStartTime() {
        return lbStartTime;
    }

    public void setLbStartTime(String lbStartTime) {
        this.lbStartTime = lbStartTime;
    }

    public String getLbEndTime() {
        return lbEndTime;
    }

    public void setLbEndTime(String lbEndTime) {
        this.lbEndTime = lbEndTime;
    }
}
