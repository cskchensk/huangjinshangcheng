package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InviteRewardsStatisticsBean implements Serializable {
    private String rewardsDate;
    private BigDecimal rewardsAmount;
    private int inviters;
    private int invitors;

    public InviteRewardsStatisticsBean() {

    }

    public String getRewardsDate() {
        return rewardsDate;
    }

    public void setRewardsDate(String rewardsDate) {
        this.rewardsDate = rewardsDate;
    }

    public BigDecimal getRewardsAmount() {
        return rewardsAmount;
    }

    public void setRewardsAmount(BigDecimal rewardsAmount) {
        this.rewardsAmount = rewardsAmount;
    }

    public int getInviters() {
        return inviters;
    }

    public void setInviters(int inviters) {
        this.inviters = inviters;
    }

    public int getInvitors() {
        return invitors;
    }

    public void setInvitors(int invitors) {
        this.invitors = invitors;
    }
}
