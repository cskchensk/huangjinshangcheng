package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InviteRecordsBean implements Serializable {
    private String memberId;
    private String mobile;
    private String name;
    private int friendNum;
    private int tradeNum;
    private BigDecimal rewards;
    private BigDecimal friendTradeMoney;

    public InviteRecordsBean() {

    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFriendNum() {
        return friendNum;
    }

    public void setFriendNum(int friendNum) {
        this.friendNum = friendNum;
    }

    public int getTradeNum() {
        return tradeNum;
    }

    public void setTradeNum(int tradeNum) {
        this.tradeNum = tradeNum;
    }

    public BigDecimal getRewards() {
        return rewards;
    }

    public void setRewards(BigDecimal rewards) {
        this.rewards = rewards;
    }

    public BigDecimal getFriendTradeMoney() {
        return friendTradeMoney;
    }

    public void setFriendTradeMoney(BigDecimal friendTradeMoney) {
        this.friendTradeMoney = friendTradeMoney;
    }
}
