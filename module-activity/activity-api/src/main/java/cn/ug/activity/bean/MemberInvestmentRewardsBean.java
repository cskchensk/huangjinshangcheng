package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberInvestmentRewardsBean implements Serializable {
    private String id;
    private String memberId;
    private String friendName;
    private String friendMobile;
    private String addTime;
    private BigDecimal amount;
    private int type;
    private BigDecimal tradeAmount;
    private int bindedCard;

    public MemberInvestmentRewardsBean() {

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BigDecimal getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(BigDecimal tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public int getBindedCard() {
        return bindedCard;
    }

    public void setBindedCard(int bindedCard) {
        this.bindedCard = bindedCard;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendMobile() {
        return friendMobile;
    }

    public void setFriendMobile(String friendMobile) {
        this.friendMobile = friendMobile;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
