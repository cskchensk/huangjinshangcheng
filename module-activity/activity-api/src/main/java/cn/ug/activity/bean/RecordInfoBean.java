package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class RecordInfoBean implements Serializable {
    private int type;
    private BigDecimal amount;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
