package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InvitationRewardsBean implements Serializable {
    private String friendMobile;
    private String friendName;
    private String friendRegisterTime;
    private int bindedCard;
    private String bindedCardTime;
    private String productName;
    private BigDecimal investAmount;
    private String investTime;
    private int type;
    private BigDecimal amount;

    private int index;
    private String typeMark;
    private String bindedCardMark;

    public String getFriendMobile() {
        return friendMobile;
    }

    public void setFriendMobile(String friendMobile) {
        this.friendMobile = friendMobile;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendRegisterTime() {
        return friendRegisterTime;
    }

    public void setFriendRegisterTime(String friendRegisterTime) {
        this.friendRegisterTime = friendRegisterTime;
    }

    public int getBindedCard() {
        return bindedCard;
    }

    public void setBindedCard(int bindedCard) {
        this.bindedCard = bindedCard;
    }

    public String getBindedCardTime() {
        return bindedCardTime;
    }

    public void setBindedCardTime(String bindedCardTime) {
        this.bindedCardTime = bindedCardTime;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(BigDecimal investAmount) {
        this.investAmount = investAmount;
    }

    public String getInvestTime() {
        return investTime;
    }

    public void setInvestTime(String investTime) {
        this.investTime = investTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTypeMark() {
        return typeMark;
    }

    public void setTypeMark(String typeMark) {
        this.typeMark = typeMark;
    }

    public String getBindedCardMark() {
        return bindedCardMark;
    }

    public void setBindedCardMark(String bindedCardMark) {
        this.bindedCardMark = bindedCardMark;
    }
}
