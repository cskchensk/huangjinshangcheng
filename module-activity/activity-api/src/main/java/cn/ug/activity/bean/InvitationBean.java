package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class InvitationBean implements Serializable {
    private String invitationDate;
    private int friends;
    private int friendTradedNum;
    private BigDecimal totalAmount;
    private int friendBindCardNum;
    private BigDecimal bindCardAmount;
    private BigDecimal investAmount;
    private BigDecimal bonusAmount;

    public String getInvitationDate() {
        return invitationDate;
    }

    public void setInvitationDate(String invitationDate) {
        this.invitationDate = invitationDate;
    }

    public int getFriends() {
        return friends;
    }

    public void setFriends(int friends) {
        this.friends = friends;
    }

    public int getFriendTradedNum() {
        return friendTradedNum;
    }

    public void setFriendTradedNum(int friendTradedNum) {
        this.friendTradedNum = friendTradedNum;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getFriendBindCardNum() {
        return friendBindCardNum;
    }

    public void setFriendBindCardNum(int friendBindCardNum) {
        this.friendBindCardNum = friendBindCardNum;
    }

    public BigDecimal getBindCardAmount() {
        return bindCardAmount;
    }

    public void setBindCardAmount(BigDecimal bindCardAmount) {
        this.bindCardAmount = bindCardAmount;
    }

    public BigDecimal getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(BigDecimal investAmount) {
        this.investAmount = investAmount;
    }

    public BigDecimal getBonusAmount() {
        return bonusAmount;
    }

    public void setBonusAmount(BigDecimal bonusAmount) {
        this.bonusAmount = bonusAmount;
    }
}
