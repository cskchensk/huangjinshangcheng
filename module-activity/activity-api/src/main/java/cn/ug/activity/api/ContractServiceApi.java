package cn.ug.activity.api;

import cn.ug.activity.bean.ContractBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("contract")
public interface ContractServiceApi {
    @GetMapping()
    SerializeObject<ContractBean> get(@RequestParam("orderId")String orderId);
}
