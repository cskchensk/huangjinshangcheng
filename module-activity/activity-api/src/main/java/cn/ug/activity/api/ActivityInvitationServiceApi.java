package cn.ug.activity.api;

import cn.ug.bean.base.SerializeObject;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("invitation")
public interface ActivityInvitationServiceApi {

    @GetMapping(value = "/valid")
    SerializeObject<JSONObject> getValidActivity();
}
