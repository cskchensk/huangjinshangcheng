package cn.ug.activity.bean;

import java.math.BigDecimal;

public class InviteFriendAwardRecordsBean extends InviteRewardsRecordFriendBean{

    /**
     * 投资金额
     */
    private BigDecimal investAmount;

    /**
     * 奖励金额
     */
    private BigDecimal amount;

    private String addTime;

    public BigDecimal getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(BigDecimal investAmount) {
        this.investAmount = investAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
