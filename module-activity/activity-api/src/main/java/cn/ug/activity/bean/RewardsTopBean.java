package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class RewardsTopBean implements Serializable {
    private String memberId;
    private String mobile;
    private BigDecimal amount;

    public RewardsTopBean() {

    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
