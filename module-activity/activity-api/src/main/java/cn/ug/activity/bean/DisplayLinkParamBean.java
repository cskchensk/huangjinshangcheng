package cn.ug.activity.bean;

import java.io.Serializable;

public class DisplayLinkParamBean implements Serializable {
    private String displayName;
    private String paramName;

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
