package cn.ug.activity.bean;

import java.math.BigDecimal;
import java.util.List;

public class GoldShopBean {
    private int id;
    private String name;
    private String goldIndustry;
    private String branchName;
    private String startTime;
    private String endTime;
    private String province;
    private String city;
    private String area;
    private String address;
    private String contactName;
    private String contactMobile;
    private String enterpriseName;
    private String headImg;
    private String columnImg;
    private BigDecimal longitude;
    private BigDecimal latitude;
    private long distance;
    private List<GoldShopServicesBean> services;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public String getGoldIndustry() {
        return goldIndustry;
    }

    public void setGoldIndustry(String goldIndustry) {
        this.goldIndustry = goldIndustry;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactMobile() {
        return contactMobile;
    }

    public void setContactMobile(String contactMobile) {
        this.contactMobile = contactMobile;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getColumnImg() {
        return columnImg;
    }

    public void setColumnImg(String columnImg) {
        this.columnImg = columnImg;
    }

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public List<GoldShopServicesBean> getServices() {
        return services;
    }

    public void setServices(List<GoldShopServicesBean> services) {
        this.services = services;
    }
}
