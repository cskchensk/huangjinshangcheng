package cn.ug.activity.bean;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

public class ChannelBean extends BaseBean implements Serializable {
    private String name;
    private String code;
    private String username;
    private int deleted;
    private int uv;
    private int amount;

    public ChannelBean() {

    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getUv() {
        return uv;
    }

    public void setUv(int uv) {
        this.uv = uv;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
