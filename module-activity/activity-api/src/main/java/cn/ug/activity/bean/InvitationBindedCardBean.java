package cn.ug.activity.bean;

import java.io.Serializable;

public class InvitationBindedCardBean implements Serializable {
    private String memberId;
    private String mobile;
    private String name;
    private String friendName;
    private String friendMobile;
    private String friendRegisterTime;
    private int bindedCard;
    private String bindedCardTime;
    /**
     * 3 已交易  2已绑卡 1未绑卡
     */
    private int status;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendMobile() {
        return friendMobile;
    }

    public void setFriendMobile(String friendMobile) {
        this.friendMobile = friendMobile;
    }

    public String getFriendRegisterTime() {
        return friendRegisterTime;
    }

    public void setFriendRegisterTime(String friendRegisterTime) {
        this.friendRegisterTime = friendRegisterTime;
    }

    public int getBindedCard() {
        return bindedCard;
    }

    public void setBindedCard(int bindedCard) {
        this.bindedCard = bindedCard;
    }

    public String getBindedCardTime() {
        return bindedCardTime;
    }

    public void setBindedCardTime(String bindedCardTime) {
        this.bindedCardTime = bindedCardTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
