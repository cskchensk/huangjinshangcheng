package cn.ug.activity.api;

import cn.ug.activity.bean.GoldShopBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("shop")
public interface GoldShopServiceApi {

    @GetMapping(value = "/detail")
    SerializeObject<GoldShopBean> getShopDetail(@RequestParam("id")int id);
}
