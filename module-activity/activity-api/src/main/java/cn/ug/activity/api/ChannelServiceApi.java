package cn.ug.activity.api;

import cn.ug.activity.bean.ChannelActivationBean;
import cn.ug.activity.bean.ChannelBean;
import cn.ug.bean.base.SerializeObject;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RequestMapping("channel")
public interface ChannelServiceApi {

    @GetMapping(value = "/{id}")
    SerializeObject<ChannelBean> get(@PathVariable("id") String id);

    @RequestMapping("/activation")
    SerializeObject<ChannelActivationBean> getActivation(@RequestParam("uuid")String uuid);

    @RequestMapping("/activation/transform")
    SerializeObject transform(@RequestParam("uuid")String uuid);
}
