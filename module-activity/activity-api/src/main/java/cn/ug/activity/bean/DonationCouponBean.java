package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class DonationCouponBean implements Serializable {
    private String donationDate;
    private BigDecimal amount;
    private int couponNum;
    private int memberNum;

    public DonationCouponBean() {

    }

    public String getDonationDate() {
        return donationDate;
    }

    public void setDonationDate(String donationDate) {
        this.donationDate = donationDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(int couponNum) {
        this.couponNum = couponNum;
    }

    public int getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(int memberNum) {
        this.memberNum = memberNum;
    }
}