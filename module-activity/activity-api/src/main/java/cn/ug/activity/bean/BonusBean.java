package cn.ug.activity.bean;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.util.List;

public class BonusBean extends BaseBean implements Serializable {
    private String name;
    private int type;
    private int status;
    private int deleted;
    private List<BonusStrategyBean> strategies;

    public BonusBean() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public List<BonusStrategyBean> getStrategies() {
        return strategies;
    }

    public void setStrategies(List<BonusStrategyBean> strategies) {
        this.strategies = strategies;
    }
}
