package cn.ug.activity.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class CouponMemberBean implements Serializable {
    /**优惠券id**/
    private int id;
    private int couponId;
    /**优惠券名称**/
    private String name;
    /**类型：0-黄金红包；1-回租福利券；2-商城满减券**/
    private int type;
    /**优惠额度**/
    private int discountAmount;
    /**交易额度**/
    private  int transactionAmount;
    /**优惠券有效期**/
    private String indateTime;
    /**回租期限**/
    private int leasebackDays;
    /**优惠券状态**/
    private int status;
    //使用条件
    private String usedCondition;
    //金生金加息年化
    private BigDecimal raiseYearIncome;

    /**
     * 适用产品类型 0:全部产品  1:实物金产品  2:安稳金产品
     */
    private Integer productType;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public int getLeasebackDays() {
        return leasebackDays;
    }

    public void setLeasebackDays(int leasebackDays) {
        this.leasebackDays = leasebackDays;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(int discountAmount) {
        this.discountAmount = discountAmount;
    }

    public int getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getIndateTime() {
        return indateTime;
    }

    public void setIndateTime(String indateTime) {
        this.indateTime = indateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUsedCondition() {
        return usedCondition;
    }

    public void setUsedCondition(String usedCondition) {
        this.usedCondition = usedCondition;
    }

    public BigDecimal getRaiseYearIncome() {
        return raiseYearIncome;
    }

    public void setRaiseYearIncome(BigDecimal raiseYearIncome) {
        this.raiseYearIncome = raiseYearIncome;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }
}
