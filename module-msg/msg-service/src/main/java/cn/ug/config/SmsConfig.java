package cn.ug.config;

import java.io.Serializable;

/**
 * 短信配置
 * @author kaiwotech
 */
public class SmsConfig implements Serializable{
	private static final long serialVersionUID 	= 1L;
	/** Access Key ID */
	private String accessKeyId;
	/** Access Key Secret */
	private String accessKeySecret;
	/** 短信签名 */
	private String signName;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getAccessKeyId() {
		return accessKeyId;
	}

	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}

	public String getAccessKeySecret() {
		return accessKeySecret;
	}

	public void setAccessKeySecret(String accessKeySecret) {
		this.accessKeySecret = accessKeySecret;
	}

	public String getSignName() {
		return signName;
	}

	public void setSignName(String signName) {
		this.signName = signName;
	}
}
