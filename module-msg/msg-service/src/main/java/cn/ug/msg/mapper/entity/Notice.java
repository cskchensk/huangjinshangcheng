package cn.ug.msg.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 公告
 * @auther ywl
 */
public class Notice extends BaseEntity implements Serializable {

    /** 标题 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 摘要 **/
    private String remark;
    /** 发送类型  1:全部 2:指定会员**/
    private Integer sendType;
    /** 发送时间类型 1:立即发送 2:指定时间 **/
    private Integer sendTimeType;
    /** 指定发送时间 **/
    private LocalDateTime sendTime;
    /** 发送数量 **/
    private Integer sendNumber;
    /** 署名 **/
    private String author;
    /** 创建用户Id **/
    private String createUserId;
    /** 创建用户**/
    private String createUser;
    /** 删除 1：否 2：是 **/
    private Integer deleted;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getSendTimeType() {
        return sendTimeType;
    }

    public void setSendTimeType(Integer sendTimeType) {
        this.sendTimeType = sendTimeType;
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public Integer getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
