package cn.ug.msg.service.impl;

import cn.ug.config.SmsConfig;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.exception.XRuntimeException;
import cn.ug.msg.bean.SmsBean;
import cn.ug.msg.service.SmsService;
import cn.ug.service.impl.BaseServiceImpl;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.aliyuncs.http.MethodType.POST;


/**
 * @author kaiwotech
 */
@Service
public class SmsServiceImpl extends BaseServiceImpl implements SmsService {
	/** 调用成功 */
	private static final String OK = "OK";
	private ObjectMapper mapper = new ObjectMapper();

	@Resource
	private SmsConfig smsConfig;

	@Override
	public int send(SmsBean entityBean) {
		// 设置超时时间-可自行调整
		System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
		System.setProperty("sun.net.client.defaultReadTimeout", "10000");
		// 初始化ascClient需要的几个参数
		// 短信API产品名称（短信产品名固定，无需修改）
		final String product = "Dysmsapi";
		// 短信API产品域名（接口地址固定，无需修改）
		final String domain = "dysmsapi.aliyuncs.com";
		// 替换成你的AK
		final String accessKeyId = smsConfig.getAccessKeyId();
		final String accessKeySecret = smsConfig.getAccessKeySecret();
		try {
			getLog().info("接收参数：");
			getLog().info(mapper.writeValueAsString(entityBean));

			// 初始化ascClient,暂时不支持多region（请勿修改）
			IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
			DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
			IAcsClient acsClient = new DefaultAcsClient(profile);
			//组装请求对象
			SendSmsRequest request = new SendSmsRequest();
			//使用post提交
			request.setMethod(POST);
			//必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
			request.setPhoneNumbers(entityBean.getPhone());
			//必填:短信签名-可在短信控制台中找到
			request.setSignName(smsConfig.getSignName());
			//必填:短信模板-可在短信控制台中找到
			request.setTemplateCode(entityBean.getType());
			//可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
			//友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
			// request.setTemplateParam("{\"name\":\"Tom\", \"code\":\"123\"}");
			request.setTemplateParam(mapper.writeValueAsString(entityBean.getParamMap()));
			//可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
			//request.setSmsUpExtendCode("90997");
			//可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
			//request.setOutId("yourOutId");
			//请求失败这里会抛ClientException异常
			SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);

			// 返回参数
			getLog().info("返回参数：");
			getLog().info(mapper.writeValueAsString(sendSmsResponse));

			String code = sendSmsResponse.getCode();
			if(StringUtils.isBlank(code)) {
				return 1;
			}
			switch (code) {
				case OK: {
					// 发送成功
					return 0;
				}
				case "isv.BUSINESS_LIMIT_CONTROL": {
					// 触发短信太频繁（业务限流）
					return 2;
				}
				case "isv.MOBILE_NUMBER_ILLEGAL": {
					// 非法手机号
					return 3;
				}
				default:{
					break;
				}
			}
		} catch (Exception e) {
			getLog().error(e.getMessage(), e);
		}

		return 1;
	}

}

