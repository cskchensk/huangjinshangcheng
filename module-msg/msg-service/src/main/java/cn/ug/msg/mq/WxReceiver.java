package cn.ug.msg.mq;

import cn.ug.bean.base.SerializeObject;
import cn.ug.enums.ThirdSourceEnum;
import cn.ug.enums.WxTemplateEnum;
import cn.ug.feign.MemberThirdService;
import cn.ug.member.bean.response.MemberThirdFindBean;
import cn.ug.msg.service.WxMessageNoticeService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

import static cn.ug.config.QueueName.QUEUE_MSG_WX_SEND;
import static cn.ug.util.RedisConstantUtil.WX_CASH_ERROR_KEY;

@Component
@RabbitListener(queues = QUEUE_MSG_WX_SEND)
public class WxReceiver {

    private Log log = LogFactory.getLog(WxReceiver.class);

    @Autowired
    private WxMessageNoticeService wxMessageNoticeService;

    @Autowired
    private MemberThirdService memberThirdService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @RabbitHandler
    public void process(WxMessageParamBean entity) throws Exception{
            log.info("wxReceiver.process");

            String key = WX_CASH_ERROR_KEY + entity.getUuidString();
            String value = redisTemplate.opsForValue().get(key);
            if (StringUtils.isNotBlank(value)) {
                int num = Integer.parseInt(value);
                if (num > 15) {
                    return;
                }
            }

            String numValue = redisTemplate.opsForValue().get(key);
            if (StringUtils.isNotBlank(numValue)) {
                int num = Integer.parseInt(numValue);
                num++;
                redisTemplate.opsForValue().set(key, String.valueOf(num), 3600, TimeUnit.SECONDS);
            } else {
                redisTemplate.opsForValue().set(key, String.valueOf(1), 3600, TimeUnit.SECONDS);
            }

            if (entity.getType() == null) {
                log.error("微信消息发送失败，错误原因：类型不能为空,请求参数:" + entity.toString());
                return;
            }

            WxTemplateEnum wxTemplateEnum = WxTemplateEnum.getWxTemplateByCode(entity.getType());
            if (wxTemplateEnum == null) {
                log.error("类型不匹配,请求参数:" + entity.toString());
                return;
            }

            if (StringUtils.isBlank(entity.getOpenid())) {
                SerializeObject<MemberThirdFindBean> serializeObject = memberThirdService.find(null, entity.getMemberId(), ThirdSourceEnum.WXACCOUNT.getType());
                if (serializeObject.getData() != null)
                    entity.setOpenid(serializeObject.getData().getOpenId());
            } else if (!StringUtils.isBlank(entity.getOpenid())) {
                SerializeObject<MemberThirdFindBean> serializeObject = memberThirdService.find(entity.getOpenid(), null, ThirdSourceEnum.WXACCOUNT.getType());
                if (serializeObject.getData() == null) {
                    log.error("微信消息发送失败，错误原因：该用户尚未绑定微信公众号,请求参数:" + entity.toString());
                    return;
                }
            }

            if (!StringUtils.isBlank(entity.getOpenid())) {
                entity.setTemplateId(wxTemplateEnum.getTemplateId());
                SerializeObject serializeObject = wxMessageNoticeService.sendNotice(entity);
                if (serializeObject.getCode() != 0) {
                    throw new Exception("微信消息发送失败，错误原因：" + serializeObject.getMsg() + ",请求参数:" + entity.toString());
                }

            } else {
                log.error("微信消息发送失败，错误原因：该用户尚未注册微信公众号,请求参数:" + entity.toString());
            }
    }
}
