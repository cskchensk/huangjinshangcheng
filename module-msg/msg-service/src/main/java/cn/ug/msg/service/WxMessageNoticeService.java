package cn.ug.msg.service;

import cn.ug.bean.base.SerializeObject;
import cn.ug.msg.mq.WxMessageParamBean;

public interface WxMessageNoticeService{

    /**发送微信通知**/
    SerializeObject sendNotice(WxMessageParamBean wxMessageParamBean);
}
