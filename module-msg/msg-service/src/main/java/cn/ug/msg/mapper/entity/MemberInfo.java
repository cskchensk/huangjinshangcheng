package cn.ug.msg.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 *  会员消息
 *  @auther ywl
 */
public class MemberInfo extends BaseEntity implements Serializable {

    /** 会员Id **/
    private String memberId;
    /** 类型 **/
    private Integer type;
    /** 标题 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 跳转类型 0:不跳转 1:账户余额 2:我的银行卡 3:订单支付页面 4:投资记录 5:黄金总资产 **/
    private Integer skipType;
    /** 状态 1:未读 2:已读 **/
    private Integer status;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSkipType() {
        return skipType;
    }

    public void setSkipType(Integer skipType) {
        this.skipType = skipType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
