package cn.ug.msg.mapper;

import cn.ug.bean.base.Page;
import cn.ug.mapper.BaseMapper;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.MemberInfoParamBean;
import cn.ug.msg.bean.response.MemberInfoBean;
import cn.ug.msg.bean.response.MemberInfoManageBean;
import cn.ug.msg.bean.response.TempalteManageBean;
import cn.ug.msg.mapper.entity.MemberInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 会员消息管理
 * @auther ywl
 */
@Component
public interface MemberInfoMapper extends BaseMapper<MemberInfo> {

    List<MemberInfoManageBean> queryMemberInfoList(MemberInfoParamBean memberInfoParamBean);

    List<MemberInfoBean> findMemberInfoList(BaseParamBean baseParamBean);

    int updateStatus(Map<String,Object> map);

    int updateAllStatus(@Param("memberId") String memberId);

    int findMyMessage(@Param("memberId") String memberId);
}
