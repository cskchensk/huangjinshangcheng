package cn.ug.msg.service;

/**
 * 会员通知消息
 * @auther ywl
 */
public interface NoticeMemberMappingService {



    /**
     * 更新会员通知为可读
     * @param id   公告id
     * @return
     */
    int updateStatus(String id);

    /**
     * 公告未读数量
     * @return
     */
    int exists();


}
