package cn.ug.msg.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.NoticeParamBean;
import cn.ug.msg.bean.response.NoticeMFindBean;
import cn.ug.msg.bean.response.NoticeManageBean;
import cn.ug.msg.mapper.entity.Notice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 通知管理
 * @author  ywl
 */
@Component
public interface NoticeMapper extends BaseMapper<Notice>{


    List<NoticeManageBean> queryNoticeList(NoticeParamBean noticeParamBean);

    /**
     * 查看会员消息--移动端
     * @param baseParamBean
     * @return
     */
    List<NoticeMFindBean> findList(BaseParamBean baseParamBean);

    /**
     * 查看通知明细--移动端
     * @param id
     * @return
     */
    NoticeMFindBean queryNoticeMById(String id);

    /**
     * 获取头条
     * @return
     */
    NoticeMFindBean findTopLine();

    /**
     * 更新公告状态
     * @param id
     * @param memberId
     */
    void updateStatus(@Param("id") String id, @Param("memberId") String memberId);
}
