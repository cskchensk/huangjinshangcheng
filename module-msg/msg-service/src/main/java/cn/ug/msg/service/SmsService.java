package cn.ug.msg.service;


import cn.ug.msg.bean.SmsBean;

/**
 * 短信管理
 * @author kaiwotech
 */
public interface SmsService {
	
	/**
	 * 发送
	 * 阿里云短信服务接口触发天级流控
	 * 短信验证码 ：使用同一个签名，对同一个手机号码发送短信验证码，支持1条/分钟，5条/小时 ，累计10条/天。
	 * 短信通知： 使用同一个签名和同一个短信模板ID，对同一个手机号码发送短信通知，支持50条/日
	 * @param entityBean	实体
	 * @return 				0:操作成功 1：操作失败 2：触发短信太频繁（触发分钟级流控）3:非法手机号
	 */
	int send(SmsBean entityBean);

}
