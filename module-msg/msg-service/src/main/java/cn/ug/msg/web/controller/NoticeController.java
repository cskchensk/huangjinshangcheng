package cn.ug.msg.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.NoticeParamBean;
import cn.ug.msg.bean.response.*;
import cn.ug.msg.service.NoticeService;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * 消息公告管理
 * @auther ywl
 */
@RestController
@RequestMapping("notice")
public class NoticeController extends BaseController {

    @Autowired
    private NoticeService noticeService;
    @Resource
    private Config config;

    /**
     * 查询消息通知列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param noticeParamBean        请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "queryNoticeList" , method = GET)
    public SerializeObject<DataTable<NoticeManageBean>> queryNoticeList(@RequestHeader String accessToken, NoticeParamBean noticeParamBean) {
        if(noticeParamBean.getPageSize() <= 0) {
            noticeParamBean.setPageSize(config.getPageSize());
        }

        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        noticeParamBean.setType(1);
        DataTable<NoticeManageBean> dataTable = noticeService.query(noticeParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询待发送消息通知列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param noticeParamBean        请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "queryWaitSendNoticeList" , method = GET)
    public SerializeObject<DataTable<NoticeManageBean>> queryWaitSendNoticeList(@RequestHeader String accessToken, NoticeParamBean noticeParamBean) {
        if(noticeParamBean.getPageSize() <= 0) {
            noticeParamBean.setPageSize(config.getPageSize());
        }

        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        noticeParamBean.setType(2);
        DataTable<NoticeManageBean> dataTable = noticeService.query(noticeParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 根据ID查找消息模版(后台)
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "queryNoticeById/{id}", method = GET)
    public SerializeObject queryNoticeById(@RequestHeader(value = "accessToken", required = false) String accessToken,@PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObject(ResultType.NORMAL, "00000002");
        }
        NoticeBean entity = noticeService.queryNoticeById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObject(ResultType.NORMAL, "00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增修改通知(后台)
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, NoticeBean entity) {
        noticeService.save(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除通知(后台)
     * @param accessToken		登录成功后分配的Key
     * @param id			    ID数组
     * @return				    是否操作成功
     */
    @RequestMapping(value = "delete", method = PUT)
    public SerializeObject delete(@RequestHeader String accessToken, String[] id) {
        noticeService.deleteByIds(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询某会员通知消息列表(移动端)
     * @param
     * @return
     */
    @RequestMapping(value = "findList" , method = GET)
    public SerializeObject<List<NoticeMFindBean>> findList(@RequestHeader(value = "accessToken", required = false) String accessToken, BaseParamBean baseParamBean) {

        if(baseParamBean.getPageSize() <= 0) {
            baseParamBean.setPageSize(config.getPageSize());
        }
        List<NoticeMFindBean> list = noticeService.findNoticeList(baseParamBean);
        return new SerializeObject<>(ResultType.NORMAL, list);
    }

    /**
     * 查询某会员通知消息列表(移动端)
     * @param
     * @return
     */
    @RequestMapping(value = "findNoticeList" , method = GET)
    public SerializeObject<DataTable<NoticeMFindBean>> findNoticeList(@RequestHeader(value = "accessToken", required = false) String accessToken, BaseParamBean baseParamBean) {
        if(baseParamBean.getPageSize() <= 0) {
            baseParamBean.setPageSize(config.getPageSize());
        }
        DataTable<NoticeMFindBean> dataTable = noticeService.findList(baseParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 根据ID查找消息模版(移动端)
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "queryNoticeMById/{id}", method = GET)
    public SerializeObject queryNotice2MById(@RequestHeader(value = "accessToken", required = false) String accessToken,@PathVariable  String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObject(ResultType.NORMAL, "00000002");
        }
        NoticeMFindBean entity = noticeService.queryNoticeMById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObject(ResultType.NORMAL, "00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }
    /**
     * 柚子头条(移动端)
     * @return			    记录集
     */
    @RequestMapping(value = "findTopLine", method = GET)
    public SerializeObject findTopLine() {
        NoticeMFindBean entity = noticeService.findTopLine();
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }
}
