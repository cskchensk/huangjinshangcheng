package cn.ug.msg.mapper.entity;

import cn.ug.mapper.entity.BaseEntity;

import java.io.Serializable;

/**
 * 通知会员消息
 * @author  ywl
 */
public class NoticeMemberMapping  extends BaseEntity implements Serializable{

    /** 会员Id **/
    private String memberId;
    /** 通知Id **/
    private String noticeId;
    /** 状态 1:未读 2:已读 **/
    private Integer status;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
