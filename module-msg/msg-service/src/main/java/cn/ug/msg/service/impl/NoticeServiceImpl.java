package cn.ug.msg.service.impl;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.NoticeParamBean;
import cn.ug.msg.bean.request.TemplateParamBean;
import cn.ug.msg.bean.response.NoticeMFindBean;
import cn.ug.msg.bean.response.NoticeBean;
import cn.ug.msg.bean.response.NoticeManageBean;
import cn.ug.msg.bean.status.CommonConstants;
import cn.ug.msg.mapper.NoticeMapper;
import cn.ug.msg.mapper.entity.Notice;
import cn.ug.msg.service.NoticeService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.util.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 消息通知
 * @auther ywl
 */
@Service
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticeMapper noticeMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<NoticeManageBean> query(NoticeParamBean noticeParamBean) {
        com.github.pagehelper.Page<NoticeParamBean> pages = PageHelper.startPage(noticeParamBean.getPageNum(),noticeParamBean.getPageSize());
        List<NoticeManageBean> dataList = noticeMapper.queryNoticeList(noticeParamBean);
        return new DataTable<>(noticeParamBean.getPageNum(), noticeParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public int save(NoticeBean entityBean) {
        //验证不为空
       /* Ensure.that(entityBean.getTitle()).isNull("19000201");*/
        Ensure.that(entityBean.getContent()).isNull("19000202");
        Ensure.that(entityBean.getSendTimeType()).isNull("19000203");
        Ensure.that(entityBean.getSendType()).isNull("19000204");
       /* Ensure.that(entityBean.getAuthor()).isNull("19000205");*/
        if(CommonConstants.NO == entityBean.getSendTimeType()){
            Ensure.that(entityBean.getSendTimeString()).isNull("19000206");
        }
        Notice notice = dozerBeanMapper.map(entityBean, Notice.class);
        if(StringUtils.isNotEmpty(entityBean.getSendTimeString())) {
            notice.setSendTime(UF.getDateTime(entityBean.getSendTimeString()));
        }else{
            notice.setSendTime(UF.getDateTime());
        }

        if(StringUtil.isEmpty(entityBean.getId())){
            //新增
            LoginBean loginBean = LoginHelper.getLoginBean();
            notice.setId(UF.getRandomUUID());
            notice.setCreateUserId(loginBean.getId());
            notice.setCreateUser(loginBean.getName());
            notice.setSendNumber(0);
            noticeMapper.insert(notice);
        }else{
            if(CommonConstants.YES == entityBean.getSendTimeType()) notice.setSendTime(UF.getDateTime());
            noticeMapper.update(notice);
        }
        return 0;
    }

    @Override
    public int deleteByIds(String[] id) {
        if(id == null || id.length<=0){
            return 0;
        }
        noticeMapper.deleteByIds(id);
        return 0;
    }


    @Override
    public NoticeBean queryNoticeById(String id) {
        Ensure.that(id).isNull("");
        Notice notice = noticeMapper.findById(id);
        if(notice != null){
            NoticeBean noticeBean = dozerBeanMapper.map(notice,NoticeBean.class);
            noticeBean.setSendTimeString(UF.getFormatDateTime(notice.getSendTime()));
            noticeBean.setAddTimeString(UF.getFormatDateTime(notice.getAddTime()));
            noticeBean.setModifyTimeString(UF.getFormatDateTime(notice.getModifyTime()));
            return noticeBean;
        }
        return null;
    }

    @Override
    public DataTable<NoticeMFindBean> findList(BaseParamBean baseParamBean) {
        com.github.pagehelper.Page<TemplateParamBean> pages = PageHelper.startPage(baseParamBean.getPageNum(),baseParamBean.getPageSize());
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean != null){
            baseParamBean.setMemberId(loginBean.getId());
        }
        List<NoticeMFindBean> dataList = noticeMapper.findList(baseParamBean);
        return new DataTable<>(baseParamBean.getPageNum(), baseParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public List<NoticeMFindBean> findNoticeList(BaseParamBean baseParamBean) {
        com.github.pagehelper.Page<TemplateParamBean> pages = PageHelper.startPage(baseParamBean.getPageNum(),baseParamBean.getPageSize());
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean != null){
            baseParamBean.setMemberId(loginBean.getId());
        }
        List<NoticeMFindBean> dataList = noticeMapper.findList(baseParamBean);
        return dataList;
    }

    @Override
    public NoticeMFindBean queryNoticeMById(String id) {
        return noticeMapper.queryNoticeMById(id);
    }

    @Override
    public NoticeMFindBean findTopLine() {
        return noticeMapper.findTopLine();
    }

    @Override
    public int updateStatus(String id) {

        return 0;
    }
}
