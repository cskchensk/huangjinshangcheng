package cn.ug.msg.service.impl;

import cn.ug.bean.base.SerializeObject;
import cn.ug.msg.bean.TemplateData;
import cn.ug.msg.bean.WxTemplate;
import cn.ug.msg.mq.WxMessageParamBean;
import cn.ug.msg.mq.WxNotifyData;
import cn.ug.msg.service.WxMessageNoticeService;
import cn.ug.util.RedisConstantUtil;
import cn.ug.util.WxUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
public class WxMessageNoticeServiceImpl implements WxMessageNoticeService {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    private static Logger logger = LoggerFactory.getLogger(WxMessageNoticeServiceImpl.class);

    @Value("${spring.cloud.config.profile}")
    private String environMent;

    @Value("${web.domain.url-dev}")
    private String domainUrlDev;

    @Value("${web.domain.url-test}")
    private String domainUrlTest;

    @Value("${web.domain.url-deta}")
    private String domainUrlDeta;

    @Override
    public SerializeObject sendNotice(WxMessageParamBean entity) {
        String key = RedisConstantUtil.WX_ACCESSTOKEN_KEY;
        String accesstoken = redisTemplate.opsForValue().get(key);
        /**accesstoken缓存查询**/
        if (StringUtils.isBlank(accesstoken)) {
            accesstoken = WxUtil.getAccessToken();
            redisTemplate.opsForValue().set(key, accesstoken.replace(" ", ""), 7200, TimeUnit.SECONDS);
        }
        String url = WxUtil.SEND_WX_TEMPLATE_URL.replace("ACCESS_TOKEN", accesstoken);
        WxTemplate temp = new WxTemplate();
        Map<String, TemplateData> notifyDatas = new HashMap<>();
        if (entity.getTemplateData() != null) {
            temp.setTemplate_id(entity.getTemplateId());
            temp.setTouser(entity.getOpenid());
            if (entity.getTemplateData().getData() != null) {
                Set<Map.Entry<String, WxNotifyData.TemplateDataAttr>> entrys = entity.getTemplateData().getData().entrySet();
                for (Map.Entry<String, WxNotifyData.TemplateDataAttr> entry : entrys) {
                    TemplateData data = new TemplateData();
                    if (StringUtils.isNotEmpty(entry.getValue().getDataColor())) {
                        data.setColor(entry.getValue().getDataColor());
                    } else {
                        data.setColor("#000000");
                    }
                    data.setValue(entry.getValue().getDataValue());
                    notifyDatas.put(entry.getKey(), data);
                }
            }
        }
        temp.setData(notifyDatas);
        String jsonString = JSONObject.toJSONString(temp).toString();
        JSONObject objectSend = WxUtil.doPostStr(url, jsonString);
        logger.info("微信推送响应信息：" + objectSend.toString());
        if ("0".equals(objectSend.getString("errcode"))) {
            return new SerializeObject(0, "发送成功");
        } else {
           /* if (objectSend.getString("code")=="40001"){
                    accesstoken = WxUtil.getAccessToken();
                    redisTemplate.opsForValue().set(key, accesstoken.replace(" ", ""), 7200, TimeUnit.SECONDS);
            }*/
            return new SerializeObject(Integer.valueOf(objectSend.getString("code")), objectSend.getString("errmsg"));
        }
    }
}
