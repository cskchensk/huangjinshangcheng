package cn.ug.msg.web.utils;

public class TemplateUtil {
    public static final String REGISTER = "您的验证码为：#content#，该验证码 5 分钟内有效，请勿泄露于他人。";
    public static final String UPDATE_PASSWORD = "您的验证码为：#content#，该验证码 5 分钟内有效，请勿泄露于他人。";
    public static final String FORGET_PASSWORD = "您的验证码为：#content#，该验证码 5 分钟内有效，请勿泄露于他人。";
    public static final String SHORTCUT_LOGIN = "您的验证码为：#content#，该验证码 5 分钟内有效，请勿泄露于他人。";
    public static final String VERIFY_IDENTIDY = "您的验证码为：#content#，该验证码 5 分钟内有效，请勿泄露于他人。";
}
