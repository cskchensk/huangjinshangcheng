package cn.ug.msg.mq;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static cn.ug.config.QueueName.*;

/**
 * 消息配置
 * @author kaiwotech
 */
@Configuration
public class RabbitConfig {

    /**
     * 短信发送队列
     * @return
     */
    @Bean
    public Queue smsQueue() {
        return new Queue(QUEUE_MSG_SMS_SEND, true);
    }

    /**
     * 消息发送队列（1.短信 2.站内信）
     * @return
     */
    @Bean
    public Queue msgQueue() {
        return new Queue(QUEUE_MSG_SEND, true);
    }

    /**
     * 测试队列
     * @return
     */
    @Bean
    public Queue testQueue(){
        return new Queue(QUEUE_TEST,true);
    }

    /**
     * 短信发送队列
     * @return
     */
    @Bean
    public Queue msgQueueDelayPerMessageTTL() {
        return QueueBuilder.durable(QUEUE_DELAY_PER_MESSAGE_TTL_MSG_SEND)
                .withArgument("x-dead-letter-exchange", DLX_MSG_SEND)
                .withArgument("x-dead-letter-routing-key", QUEUE_MSG_SEND)
                .build();
    }

    /**
     * 延迟短信交换定义
     * @return
     */
    @Bean
    public DirectExchange msgDelayExchange(){
        return new DirectExchange(DLX_MSG_SEND);
    }

    /**
     * 绑定
     * @param msgQueue          消费队列
     * @param msgDelayExchange  DLX
     * @return
     */
    @Bean
    public Binding smsDelayBinding(Queue msgQueue, DirectExchange msgDelayExchange) {
        return BindingBuilder.bind(msgQueue)
                .to(msgDelayExchange)
                .with(QUEUE_MSG_SEND);
    }

    @Bean
    public Queue wxQueue() {
        return new Queue(QUEUE_MSG_WX_SEND, true);
    }

    @Bean
    public Queue wxQueueDelayPerMessageTTL(){
        return QueueBuilder.durable(QUEUE_DELAY_PER_MESSAGE_TTL_MSG_WX_SEND)
                .withArgument("x-dead-letter-exchange", DLX_MSG_WX_SEND)
                .withArgument("x-dead-letter-routing-key", QUEUE_MSG_WX_SEND)
                .build();
    }

    /**
     * 微信延迟短信交换定义
     * @return
     */
    @Bean
    public DirectExchange msgWxDelayExchange(){
        return new DirectExchange(DLX_MSG_WX_SEND);
    }

    /**
     * 绑定
     * @param msgQueue          消费队列
     * @param msgDelayExchange  DLX
     * @return
     */
    @Bean
    public Binding smsWxDelayBinding(Queue msgQueue, DirectExchange msgDelayExchange) {
        return BindingBuilder.bind(msgQueue)
                .to(msgDelayExchange)
                .with(QUEUE_MSG_WX_SEND);
    }

}
