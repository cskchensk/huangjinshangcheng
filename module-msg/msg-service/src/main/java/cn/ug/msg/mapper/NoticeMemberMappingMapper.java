package cn.ug.msg.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.msg.mapper.entity.NoticeMemberMapping;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 会员消息通知管理
 * @auther ywl
 */
@Component
public interface NoticeMemberMappingMapper extends BaseMapper<NoticeMemberMapping>{



    /**
     * 更新通知状态
     * @param map
     * @return
     */
    int updateStatus(Map<String,Object> map);

    /**
     * 查询是否可读
     * @param noticeId
     * @param memberId
     * @return
     */
    int findIsRead(@Param("noticeId") String noticeId, @Param("memberId") String memberId);

    /**
     * 未读数量
     * @param memberId
     * @return
     */
    int exists(@Param("memberId") String memberId);
}
