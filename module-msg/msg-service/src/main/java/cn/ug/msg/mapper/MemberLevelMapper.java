package cn.ug.msg.mapper;

import cn.ug.msg.mapper.entity.MemberLevel;
import cn.ug.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * 帐号管理
 * @author kaiwotech
 */
@Component
public interface MemberLevelMapper extends BaseMapper<MemberLevel> {
	
}