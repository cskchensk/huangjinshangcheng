package cn.ug.msg.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.TemplateParamBean;
import cn.ug.msg.bean.response.TempalteManageBean;
import cn.ug.msg.bean.response.TemplateBean;
import cn.ug.msg.bean.response.TemplateFindBean;
import cn.ug.msg.service.TemplateSerice;
import cn.ug.web.controller.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * 消息模版管理
 * @auther ywl
 */
@RestController
@RequestMapping("template")
public class TemplateController extends BaseController {

    @Autowired
    private TemplateSerice templateSerice;
    @Resource
    private Config config;

    /**
     * 查询消息模版列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param templateParamBean       请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "queryTemplateList" , method = GET)
    public SerializeObject<DataTable<TempalteManageBean>> queryTemplateList(@RequestHeader String accessToken, TemplateParamBean templateParamBean) {
        if(templateParamBean.getPageSize() <= 0) {
            templateParamBean.setPageSize(config.getPageSize());
        }

        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        DataTable<TempalteManageBean> dataTable = templateSerice.query(templateParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 根据ID查找消息模版(后台)
     * @param accessToken	登录成功后分配的Key
     * @param id		    ID
     * @return			    记录集
     */
    @RequestMapping(value = "queryTemplateById/{id}", method = GET)
    public SerializeObject queryTemplateById(@RequestHeader(value = "accessToken", required = false) String accessToken,@PathVariable String id) {
        if(StringUtils.isBlank(id)) {
            return new SerializeObject(ResultType.NORMAL, "00000002");
        }
        TemplateFindBean entity = templateSerice.queryTemplateById(id);
        if(null == entity || StringUtils.isBlank(entity.getId())) {
            return new SerializeObject(ResultType.NORMAL, "00000003");
        }
        return new SerializeObject<>(ResultType.NORMAL, entity);
    }

    /**
     * 新增修改消息模版(后台)
     * @param accessToken		登录成功后分配的Key
     * @param entity		    记录集
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    public SerializeObject save(@RequestHeader String accessToken, TemplateBean entity) {
        templateSerice.save(entity);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 删除模版(后台)
     * @param accessToken		登录成功后分配的Key
     * @return				    是否操作成功
     */
    @RequestMapping(value = "delete/{id}", method = PUT)
    public SerializeObject delete(@RequestHeader String accessToken, @PathVariable String id) {
        templateSerice.delete(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }


}
