package cn.ug.msg.service;

import cn.ug.msg.mq.Msg;

/**
 * 消息发送(短信+站内信)
 */
public interface MsgService {

    /**
     * 消息发送
     * @param msg
     */
    void sendMsg(Msg msg);
}
