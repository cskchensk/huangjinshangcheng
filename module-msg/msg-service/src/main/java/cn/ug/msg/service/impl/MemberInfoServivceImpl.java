package cn.ug.msg.service.impl;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.MemberInfoParamBean;
import cn.ug.msg.bean.request.TemplateParamBean;
import cn.ug.msg.bean.response.MemberInfoBean;
import cn.ug.msg.bean.response.MemberInfoManageBean;
import cn.ug.msg.bean.status.CommonConstants;
import cn.ug.msg.mapper.MemberInfoMapper;
import cn.ug.msg.mapper.NoticeMemberMappingMapper;
import cn.ug.msg.mapper.entity.MemberInfo;
import cn.ug.msg.service.MemberInfoService;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemberInfoServivceImpl implements MemberInfoService {


    @Resource
    private MemberInfoMapper memberInfoMapper;

    @Resource
    private NoticeMemberMappingMapper notice2MMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<MemberInfoManageBean> query(MemberInfoParamBean memberInfoParamBean) {
        com.github.pagehelper.Page<TemplateParamBean> pages = PageHelper.startPage(memberInfoParamBean.getPageNum(),memberInfoParamBean.getPageSize());
        List<MemberInfoManageBean> dataList = memberInfoMapper.queryMemberInfoList(memberInfoParamBean);
        return new DataTable<>(memberInfoParamBean.getPageNum(), memberInfoParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public List<MemberInfoBean> findMemberInfoList(BaseParamBean baseParamBean) {
        com.github.pagehelper.Page<TemplateParamBean> pages = PageHelper.startPage(baseParamBean.getPageNum(),baseParamBean.getPageSize());
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("00000006");
        baseParamBean.setMemberId(loginBean.getId());
        List<MemberInfoBean> dataList = memberInfoMapper.findMemberInfoList(baseParamBean);
        return dataList;
    }

    @Override
    public int save(MemberInfoBean memberInfoBean) {
        MemberInfo memberInfo = dozerBeanMapper.map(memberInfoBean, MemberInfo.class);
        memberInfo.setId(UF.getRandomUUID());
        memberInfoMapper.insert(memberInfo);
        return 0;
    }

    @Override
    public int updateStatus(String id) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("memberId",loginBean.getId());
        map.put("id",id);
        map.put("status",CommonConstants.ReadStatus.YES_READ.getIndex());
        memberInfoMapper.updateStatus(map);
        return 0;
    }

    @Override
    public int exists() {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Map<String,Object> param = new HashMap<String,Object>();
        param.put("memberId",loginBean.getId());
        param.put("status", CommonConstants.ReadStatus.NOT_READ.getIndex());
        int msgNumber = memberInfoMapper.exists(param);
        return msgNumber;
    }

    @Override
    public int updateAllStatus(String memberId) {
        return memberInfoMapper.updateAllStatus(memberId);
    }

    @Override
    public int findMyMessage(String memberId) {
        return memberInfoMapper.findMyMessage(memberId)>0?1:0;
    }
}
