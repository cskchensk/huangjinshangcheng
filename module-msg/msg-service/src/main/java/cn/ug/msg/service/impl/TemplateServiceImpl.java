package cn.ug.msg.service.impl;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.TemplateParamBean;
import cn.ug.msg.bean.response.TempalteManageBean;
import cn.ug.msg.bean.response.TemplateBean;
import cn.ug.msg.bean.response.TemplateFindBean;
import cn.ug.msg.bean.status.CommonConstants;
import cn.ug.msg.mapper.TemplateMapper;
import cn.ug.msg.mapper.entity.Template;
import cn.ug.msg.service.TemplateSerice;
import cn.ug.util.UF;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.util.StringUtil;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TemplateServiceImpl implements TemplateSerice {

    @Resource
    private TemplateMapper templateMapper;

    @Resource
    private DozerBeanMapper dozerBeanMapper;

    @Override
    public DataTable<TempalteManageBean> query(TemplateParamBean templateParamBean) {
        com.github.pagehelper.Page<TemplateParamBean> pages = PageHelper.startPage(templateParamBean.getPageNum(),templateParamBean.getPageSize());
        List<TempalteManageBean> dataList = templateMapper.queryTemplateList(templateParamBean);
        return new DataTable<>(templateParamBean.getPageNum(), templateParamBean.getPageSize(), pages.getTotal(), dataList);
    }

    @Override
    public int save(TemplateBean entityBean) {
        //验证必填参数
        Ensure.that(entityBean.getType()).isNull("19000301");
        Ensure.that(entityBean.getDescribed()).isNull("19000302");
        Ensure.that(entityBean.getContent()).isNull("19000303");
        Ensure.that(entityBean.getIsSms() == null && entityBean.getIsInterLetter() == null).isTrue("19000304");
        if(entityBean.getIsSms() != null && CommonConstants.YES.equals(entityBean.getIsSms())){
            Ensure.that(entityBean.getFilingNumber()).isNull("19000305");
        }
        if(entityBean.getIsInterLetter() != null && CommonConstants.YES.equals(entityBean.getIsInterLetter())){
            Ensure.that(entityBean.getTitle()).isNull("19000306");
        }
        if(entityBean.getIsSms() == null) entityBean.setIsSms(CommonConstants.NO);
        if(entityBean.getIsInterLetter() == null) entityBean.setIsInterLetter(CommonConstants.NO);
        Template template = dozerBeanMapper.map(entityBean, Template.class);
        if(StringUtil.isEmpty(entityBean.getId())){
            //新增记录
            LoginBean loginBean = LoginHelper.getLoginBean();
            template.setId(UF.getRandomUUID());
            template.setCreateUserId(loginBean.getId());
            template.setCreateUser(loginBean.getName());
            templateMapper.insert(template);
        }else{
            templateMapper.update(template);
        }
        return 0;
    }

    @Override
    public int delete(String id) {
        templateMapper.delete(id);
        return 0;
    }

    @Override
    public TemplateFindBean queryTemplateById(String id) {
        Ensure.that(id).isNull("");
        Template template = templateMapper.findById(id);
        if(template == null){
            return null;
        }
        TemplateFindBean entity = dozerBeanMapper.map(template, TemplateFindBean.class);
        entity.setAddTimeString(UF.getFormatDateTime(template.getAddTime()));
        entity.setModifyTimeString(UF.getFormatDateTime(template.getModifyTime()));
        return entity;
    }

    @Override
    public TemplateBean findById(String id) {
        return null;
    }
}
