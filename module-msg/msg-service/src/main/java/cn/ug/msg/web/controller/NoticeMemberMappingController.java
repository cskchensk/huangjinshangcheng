package cn.ug.msg.web.controller;

import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.config.QueueName;
import cn.ug.msg.bean.SmsBean;
import cn.ug.msg.bean.response.NoticeMFindBean;
import cn.ug.msg.mq.Msg;
import cn.ug.msg.mq.Sms;
import cn.ug.msg.service.NoticeMemberMappingService;
import cn.ug.util.UF;
import cn.ug.web.controller.BaseController;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

import static cn.ug.config.QueueName.QUEUE_MSG_SMS_SEND;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 会员通知信息
 * @auther ywl
 */
@RestController
@RequestMapping("noticeMemberMapping")
public class NoticeMemberMappingController extends BaseController{


    @Autowired
    private NoticeMemberMappingService notice2MappingService;

    @Resource
    private Config config;

    @Resource
    private AmqpTemplate amqpTemplate;

    /**
     * 测试
     */
    @RequestMapping(value = "test", method = GET)
    public SerializeObject test() throws JsonProcessingException {
        Msg sms = new Msg();
        //sms.setMobile("18868717896");
        sms.setType(6);
        sms.setMemberId("CXKmEaV2tAyHdjLLwBKTxb");

        Map<String, String> paramMap = new HashMap<>(1);
        paramMap.put("date", "2018年3月12日");
        paramMap.put("amount", "100");
        paramMap.put("usableAmount", "150");
        sms.setParamMap(paramMap);

        amqpTemplate.convertAndSend(QueueName.QUEUE_MSG_SEND, sms);

        return new SerializeObject<>(ResultType.NORMAL);
    }

    /**
     * 更新公告消息状态(移动端)
     * @return
     */
    @RequestMapping(value = "updateStatus/{id}" , method = POST)
    public SerializeObject<Object> updateStatus(@RequestHeader String accessToken, @PathVariable String id) {
        notice2MappingService.updateStatus(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 查询某会员是否有通知(移动端)
     * @return   目前返回数量
     */
    @RequestMapping(value = "exists" , method = GET)
    public SerializeObject<Object> exists(@RequestHeader String accessToken) {
        int num  = notice2MappingService.exists();
        return new SerializeObject<>(ResultType.NORMAL, num);
    }

}
