package cn.ug.msg.service;

import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.MemberInfoParamBean;
import cn.ug.msg.bean.response.MemberInfoBean;
import cn.ug.msg.bean.response.MemberInfoManageBean;

import java.util.List;

public interface MemberInfoService {

    /**
     * 搜索--后台
     * @param memberInfoParamBean	   请求参数
     * @return				            分页数据
     */
    DataTable<MemberInfoManageBean> query(MemberInfoParamBean memberInfoParamBean);

    /**
     * 获取某会员消息列表
     * @param baseParamBean
     * @return
     */
    List<MemberInfoBean> findMemberInfoList(BaseParamBean baseParamBean);

    /**
     * 保存
     * @param memberInfoBean
     * @return
     */
    int save(MemberInfoBean memberInfoBean);


    /**
     * 更新消息状态
     *
     */
    int updateStatus(String id);

    /**
     * 查询某会员是否有消息和通知(移动端)
     * @return
     */
    int exists();

    /**
     * 全部已读
     * @return
     */
    int updateAllStatus(String memberId);

    /**
     * 查询我的-是否有未读的消息
     * @param memberId
     * @return
     */
    int findMyMessage(String memberId);
}
