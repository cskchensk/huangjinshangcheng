package cn.ug.msg.service;

import cn.ug.bean.base.DataTable;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.NoticeParamBean;
import cn.ug.msg.bean.response.*;

import java.util.List;

/**
 * 消息通知
 * @auther ywl
 */
public interface NoticeService {

    /**
     * 搜索
     * @param noticeParamBean	    请求参数
     * @return				            分页数据
     */
    DataTable<NoticeManageBean> query(NoticeParamBean noticeParamBean);

    /**
     * 添加
     * @param entityBean	实体
     * @return 	0:操作成功 1：不能为空 2：数据已存在
     */
    int save(NoticeBean entityBean);


    /**
     * 删除对象
     * @param id	ID数组
     * @return		操作影响的记录数	0：请求参数有误 >=1：操作成功
     */
    int deleteByIds(String[] id);

    /**
     * 查看
     * @param id			用户ID
     * @return				查询
     */
    NoticeBean queryNoticeById(String id);

    /**
     * 获取会员所有通知消息--移动端
     * @param baseParamBean
     * @return
     */
    DataTable<NoticeMFindBean> findList(BaseParamBean baseParamBean);

    /**
     * 获取会员所有通知消息--移动端
     * @param baseParamBean
     * @return
     */
    List<NoticeMFindBean> findNoticeList(BaseParamBean baseParamBean);

    /**
     * 获取消息通知明细
     * @param id
     * @return
     */
    NoticeMFindBean queryNoticeMById(String id);

    /**
     * 获取柚子黄金头条
     * @return
     */
    NoticeMFindBean findTopLine();

    /**
     * 更新消息状态
     *
     */
    int updateStatus(String id);

}
