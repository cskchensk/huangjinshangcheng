package cn.ug.msg.web.controller;

import cn.ug.bean.LoginBean;
import cn.ug.bean.base.DataTable;
import cn.ug.bean.base.Page;
import cn.ug.bean.base.SerializeObject;
import cn.ug.bean.type.ResultType;
import cn.ug.config.Config;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.request.BaseParamBean;
import cn.ug.msg.bean.request.MemberInfoParamBean;
import cn.ug.msg.bean.response.MemberInfoBean;
import cn.ug.msg.bean.response.MemberInfoManageBean;
import cn.ug.msg.service.MemberInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * 会员消息中心
 * @auther ywl
 */
@RestController
@RequestMapping("/memberInfo")
public class MemberInfoController {

    @Autowired
    private MemberInfoService memberInfoService;

    @Resource
    private Config config;
    /**
     * 查询会员消息列表(后台)
     * @param accessToken	            登录成功后分配的Key
     * @param memberInfoParamBean    请求参数
     * @return			                分页数据
     */
    @RequestMapping(value = "queryMemberInfoList" , method = GET)
    public SerializeObject<DataTable<MemberInfoManageBean>> queryMemberInfoList(@RequestHeader String accessToken, MemberInfoParamBean memberInfoParamBean) {
        if(memberInfoParamBean.getPageSize() <= 0) {
            memberInfoParamBean.setPageSize(config.getPageSize());
        }

        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        DataTable<MemberInfoManageBean> dataTable = memberInfoService.query(memberInfoParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询某会员消息列表(移动端)
     *
     * @param
     * @return
     */
    @RequestMapping(value = "findMemberInfoList", method = GET)
    public SerializeObject<List<MemberInfoBean>> findMemberInfoList(@RequestHeader String accessToken, BaseParamBean baseParamBean) {
        if (baseParamBean.getPageSize() <= 0) {
            baseParamBean.setPageSize(config.getPageSize());
        }
        List<MemberInfoBean> dataTable = memberInfoService.findMemberInfoList(baseParamBean);
        return new SerializeObject<>(ResultType.NORMAL, dataTable);
    }

    /**
     * 查询某会员是否有消息和通知(移动端)
     *
     * @return 目前返回数量
     */
    @RequestMapping(value = "exists", method = GET)
    public SerializeObject<Object> exists(@RequestHeader String accessToken) {
        int num = memberInfoService.exists();
        return new SerializeObject<>(ResultType.NORMAL, num);
    }

    /**
     * 更新消息状态(移动端)
     *
     * @return
     */
    @RequestMapping(value = "updateStatus/{id}", method = POST)
    public SerializeObject<Object> updateStatus(@RequestHeader String accessToken, @PathVariable String id) {
        memberInfoService.updateStatus(id);
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 更新全部消息状态(移动端)
     *
     * @return
     */
    @RequestMapping(value = "update/allStatus", method = PUT)
    public SerializeObject<Object> updateStatus(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        memberInfoService.updateAllStatus(loginBean.getId());
        return new SerializeObject(ResultType.NORMAL, "00000001");
    }

    /**
     * 我的-是否有未读的消息
     *
     * @param accessToken
     * @return
     */
    @RequestMapping(value = "find/myMessage", method = GET)
    public SerializeObject<List<MemberInfoBean>> findMyMessage(@RequestHeader String accessToken) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        Ensure.that(loginBean == null).isTrue("15000000");
        return new SerializeObject(ResultType.NORMAL, memberInfoService.findMyMessage(loginBean.getId()));
    }
}
