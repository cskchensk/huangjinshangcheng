package cn.ug.msg.mapper;

import cn.ug.mapper.BaseMapper;
import cn.ug.msg.bean.request.TemplateParamBean;
import cn.ug.msg.bean.response.TempalteManageBean;
import cn.ug.msg.mapper.entity.Template;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 消息模版管理
 * @author  ywl
 */
@Component
public interface TemplateMapper extends BaseMapper<Template> {

    /**
     * 获取模版列表--后台
     * @param templateParamBean
     * @return
     */
    List<TempalteManageBean> queryTemplateList(TemplateParamBean templateParamBean);

    /**
     * 根据类型获取模版
     * @param type
     * @return
     */
    Template findByType(Integer type);

}
