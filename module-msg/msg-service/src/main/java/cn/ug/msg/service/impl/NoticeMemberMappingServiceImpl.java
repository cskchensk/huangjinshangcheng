package cn.ug.msg.service.impl;

import cn.ug.bean.LoginBean;
import cn.ug.core.ensure.Ensure;
import cn.ug.core.login.LoginHelper;
import cn.ug.msg.bean.status.CommonConstants;
import cn.ug.msg.mapper.NoticeMemberMappingMapper;
import cn.ug.msg.mapper.entity.NoticeMemberMapping;
import cn.ug.msg.service.NoticeMemberMappingService;
import cn.ug.util.UF;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class NoticeMemberMappingServiceImpl implements NoticeMemberMappingService {

    @Resource
    private NoticeMemberMappingMapper notice2MMapper;



    @Transactional
    @Override
    public int updateStatus(String id) {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean == null) return 0;

        //验证是否已经读过
        if(notice2MMapper.findIsRead(id, loginBean.getId()) == 0){
            //新增记录
            NoticeMemberMapping entity = new NoticeMemberMapping();
            entity.setId(UF.getRandomUUID());
            entity.setMemberId(loginBean.getId());
            entity.setNoticeId(id);
            notice2MMapper.insert(entity);

            Map<String,Object> map = new HashMap<String,Object>();
            map.put("status", CommonConstants.ReadStatus.YES_READ.getIndex());
            map.put("noticeId",id);
            notice2MMapper.updateStatus(map);
        }
        return 0;
    }

    @Override
    public int exists() {
        LoginBean loginBean = LoginHelper.getLoginBean();
        if(loginBean == null) return 0;
        return notice2MMapper.exists(loginBean.getId());
    }
}
