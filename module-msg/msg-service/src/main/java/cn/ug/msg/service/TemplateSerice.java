package cn.ug.msg.service;

import cn.ug.bean.base.DataTable;
import cn.ug.msg.bean.request.TemplateParamBean;
import cn.ug.msg.bean.response.TempalteManageBean;
import cn.ug.msg.bean.response.TemplateBean;
import cn.ug.msg.bean.response.TemplateFindBean;

public interface TemplateSerice {

    /**
     * 搜索
     * @param templateParamBean	   请求参数
     * @return				            分页数据
     */
    DataTable<TempalteManageBean> query(TemplateParamBean templateParamBean);

    /**
     * 添加
     * @param entityBean	实体
     * @return 	0:操作成功 1：不能为空 2：数据已存在
     */
    int save(TemplateBean entityBean);

    /**
     * 删除
     * @param id  模版ID
     * @return
     */
    int delete(String id);

    /**
     * 查看
     * @param id			用户ID
     * @return				查询
     */
    TemplateFindBean queryTemplateById(String id);

    /**
     * 查询-移动端
     * @param id
     * @return
     */
    TemplateBean findById(String id);

}
