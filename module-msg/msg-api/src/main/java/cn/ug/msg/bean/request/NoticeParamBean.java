package cn.ug.msg.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class NoticeParamBean  extends Page implements Serializable {

    /** 发送类型 1：公告列表 2：待发送公告列表 **/
    private Integer type;
    /** 关键字 **/
    private String keyword;
    /** 起始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
