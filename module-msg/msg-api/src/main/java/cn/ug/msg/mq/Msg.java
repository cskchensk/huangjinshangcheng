package cn.ug.msg.mq;

import java.io.Serializable;
import java.util.Map;

/**
 * 消息发送实体(1.短信 2.站内信)
 * @auther ywl
 */
public class Msg  implements Serializable{

    /** 会员ID **/
    private String memberId;
    /** 手机号码 **/
    private String mobile;
    /** 业务类型 具体看字典表MsgType **/
    private Integer type;
    /** 变量 */
    private Map<String, String> paramMap;
    // 1:短信；2：站内信；3：推送，个多用","分割
    private String noticeType;

    public String getMemberId() {
        return memberId;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(String noticeType) {
        this.noticeType = noticeType;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Map<String, String> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, String> paramMap) {
        this.paramMap = paramMap;
    }
}
