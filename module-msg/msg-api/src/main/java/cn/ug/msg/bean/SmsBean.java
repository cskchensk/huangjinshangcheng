package cn.ug.msg.bean;


import java.util.Map;

/**
 * 短信
 * @author kaiwotech
 */
public class SmsBean implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	/** 类别 */
	private String type;
	/** 短信接收号码 */
	private String phone;
	/** 变量 */
	private Map<String, String> paramMap;

	public SmsBean() {
	}

	public SmsBean(String type) {
		this.type = type;
	}

	public SmsBean(String type, String phone) {
		this.type = type;
		this.phone = phone;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Map<String, String> getParamMap() {
		return paramMap;
	}

	public void setParamMap(Map<String, String> paramMap) {
		this.paramMap = paramMap;
	}
}
