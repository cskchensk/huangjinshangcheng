package cn.ug.msg.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

public class MemberInfoBean  extends BaseBean implements Serializable {

    /** 消息标题 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 跳转类型 0:不跳转 1:账户余额 2:我的银行卡 3:订单支付页面 4:投资记录 5:黄金总资产 **/
    private Integer skipType;
    /** 状态 1:未读 2:已读 **/
    private Integer status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSkipType() {
        return skipType;
    }

    public void setSkipType(Integer skipType) {
        this.skipType = skipType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
