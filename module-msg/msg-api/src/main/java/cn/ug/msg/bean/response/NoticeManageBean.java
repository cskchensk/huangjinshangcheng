package cn.ug.msg.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * 消息通知--后台
 * @auther ywl
 */
public class NoticeManageBean   extends BaseBean implements Serializable {

    /** 标题 **/
    private String title;
    /** 发送数量 **/
    private Integer sendNumber;
    /** 发送时间 **/
    private String sendTimeString;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }

    public String getSendTimeString() {
        return sendTimeString;
    }

    public void setSendTimeString(String sendTimeString) {
        this.sendTimeString = sendTimeString;
    }
}
