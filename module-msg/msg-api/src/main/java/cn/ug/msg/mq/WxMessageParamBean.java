package cn.ug.msg.mq;

import cn.ug.util.UUIDUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 微信消息请求信息
 */
public class WxMessageParamBean implements java.io.Serializable{

    private String memberId;

    private String templateId;

    private WxNotifyData templateData;

    /**
     * 微信的Openid
     */
    private String openid;

    /**
     * 场景类型  详情参考：WxTemplateEnum 枚举类
     */
    private Integer type;

    /**
     * UUID
     */
    private String uuidString = UUIDUtils.uuid();

    public String getMemberId() {
        return memberId;
    }


    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }


    public String getTemplateId() {
        return templateId;
    }


    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }


    public WxNotifyData getTemplateData() {
        return templateData;
    }


    public void setTemplateData(WxNotifyData templateData) {
        this.templateData = templateData;
    }


    public String getOpenid() {
        return openid;
    }


    public void setOpenid(String openid) {
        this.openid = openid;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getUuidString() {
        return uuidString;
    }

    public void setUuidString(String uuidString) {
        this.uuidString = uuidString;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
