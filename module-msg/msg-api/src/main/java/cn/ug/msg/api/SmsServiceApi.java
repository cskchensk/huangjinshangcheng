package cn.ug.msg.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.msg.bean.SmsBean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * 短信
 * @author kaiwotech
 */
@RequestMapping("sms")
public interface SmsServiceApi {

    /**
     * 发送短信
     * @param entity            参数集合
     * @return				    是否操作成功
     */
    @RequestMapping(method = POST)
    SerializeObject send(@RequestBody SmsBean entity);

}
