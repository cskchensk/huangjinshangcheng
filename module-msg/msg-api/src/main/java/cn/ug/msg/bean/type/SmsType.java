package cn.ug.msg.bean.type;

/**
 * 短信类型（短信模板）
 */
public class SmsType {

    /** 注册验证码 */
    public static final String REGISTER_VERIFICATION_CODE           = "SMS_122292632";
    /** 注册成功 */
    public static final String REGISTER_SUCCESS                     = "SMS_122292632";
    /** 修改密码 */
    public static final String UPDATE_PASSWORD_VERIFICATION_CODE    = "SMS_122292632";
    /** 忘记密码 */
    public static final String FORGET_PASSWORD_VERIFICATION_CODE    = "SMS_122292632";
    /** 系统管理员登陆 **/
    public static final String ADMIN_LOGIN_SMS_TYPE    = "SMS_133962373";

    /*满标*/
    public static final String SOLD_OUT    = "SMS_135805765";
    /*募集期已到期*/
    public static final String BECOME_DUE    = "SMS_135805766";
    /*手动下架*/
    public static final String MANUALLY_OPERATION    = "SMS_135805768";
    /*为所有用户赠送优惠券模板*/
    public static final String ALL_USER_GIVE_COUPON    = "SMS_137658528";
    /*批次导入用户赠送优惠券模板*/
    public static final String BATCH_USER_GIVE_COUPON    = "SMS_137658915";

    /*商城商品自动下架*/
    public static final String GOODS_AOTU_UNDER    = "SMS_139233139";
    /*商城商品手动下架*/
    public static final String GOODS_HAND_UNDER    = "SMS_139228242";
}
