package cn.ug.msg.bean.status;

/**
 * 消息常量
 * @author ywl
 */
public class CommonConstants {

	    //消息读状态
		public static enum ReadStatus{
			NOT_READ("未读", 1),  YES_READ("已读", 2);

			private String name;
			private int index;

			private ReadStatus(String name, int index)
			{
				this.name = name;
				this.index = index;
			}

			public String getName()
			{
				return this.name;
			}

			public int getIndex()
			{
				return this.index;
			}

			public static String getName(int index)
			{
				for (ReadStatus c : ReadStatus.values()) {
					if (c.getIndex() == index) {
						return c.name;
					}
				}
				return null;
			}
		}
	//消息类型
	public static  enum MsgType{
		REGISTER("注册", 1),  FORGET_PASSWORD("忘记登录密码", 2), UPDATE_PAY_PASSWORD("修改交易密码", 3),  FORGET_PAY_PASSWORD("忘记交易密码", 4),
		BIND_BANK_CARD("绑卡银行卡", 5), RECHARGE("充值", 6), WITHDRAW("提现", 7), WITHDRAW_SUCCESS("提现成功", 8), WITHDRAW_FAIL("提现失败", 9),
		WAIT_PAY_ORDER("待支付订单", 10), BUY_GOLD("购买黄金", 11), BUY_GOLD_EXPIRE("产品到期(回租到期)", 12), ACCOUNT_PAY_CHANGE("账户支出变动", 13),
		ACCOUNT_INCOME_CHANGE("账户收入变动",14), CURRENT_TO_PERIOD("活期转入定期", 15), SELL_GOLD("卖出黄金(出售提单)", 16), RECHARGE_FAIL("充值失败", 17),
		PAY_FAIL("交易失败", 18), BUY_CURRENT_GOLD("购买活期金(提单成功)", 19), INVITE_BIND_BANK_CARD_AWARD("邀请绑卡奖励", 20),
        INVITE_FIRST_INVEST_AWARD("邀请首投奖励", 21), INVITE_INVIST_RETURN_AWARD("邀请投资返现", 22), BANK_MAINTENANCE("银行维护中", 23),
        BANK_BACK_NORMAL_USE("银行恢复正常使用", 24), PRODUCT_DOWN_SHELF("产品下架短信通知", 25), COUPON_CASH("红包返现", 27), TICKET_CASH("福利券返现", 28),
		GUESS_GOLD_PRICE("黄金奖励转入", 29),MALL_PAY_SUCCESS("商城支付成功", 30),COUPON_NOTIFY("红包赠送通知", 31),TICKET_NOTIFY("福利券赠送通知", 32),
		CASH_REWARDS("现金奖励转入", 33),COUPON_NOTIFICATION("现金红包到账通知", 34),TICKET_NOTIFICATION("福利券到账通知", 35),EXTRACTGOLD_FINISH("提金成功",38),
		EXTRACTGOLD_SUBSCRIBE("提金预约",39),EXTRACTGOLD_CANCEL("取消提金",40),WAITING_PAY_TBILL("生成待支付提单通知", 41), LEASEBACK_WITH_COUPON("回租提醒（使用福利券）", 42),
		LEASEBACK_WITHOUT_COUPON("回租提醒(未使用福利券)", 43), PAY_EXPERIENCE("购买体验金)", 44), EXPERIENCE_BECOME_DUE("体验金到期)", 45), GIVE_COUPON("手动赠送优惠券通知)", 46),
		COUPON_BECOME_DUE("优惠券到期提醒)", 47),EXPERIENCE_SHIFT_TO("体验金转入",48),EXPERIENCE_GOLDBEAN_DUE("金豆到期提醒",49),REGISTER_WELFARE("注册福利通知",51);

		private String name;
		private int index;

		private MsgType(String name, int index)
		{
			this.name = name;
			this.index = index;
		}

		public String getName()
		{
			return this.name;
		}

		public int getIndex()
		{
			return this.index;
		}

		public static String getName(int index)
		{
			for (MsgType c : MsgType.values()) {
				if (c.getIndex() == index) {
					return c.name;
				}
			}
			return null;
		}
	}

		public static final Integer YES = 1;

		public static final Integer NO = 2;

		public static final String REPAYMENT_WAY = "产品还款方式";

		public static final String  STANDARD_WAY = "产品成标方式";

}
