package cn.ug.msg.mq;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.HashMap;
import java.util.Map;

/**
 * 微信通知数据
 */
public class WxNotifyData implements java.io.Serializable{
    private String url = "#";
    private Map<String, TemplateDataAttr> data = new HashMap<String, TemplateDataAttr>();

    public void addDataParam(String dataName, String dataValue, String dataColor) {
        data.put(dataName, new TemplateDataAttr(dataValue, dataColor));
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, TemplateDataAttr> getData() {
        return data;
    }

    public void setData(Map<String, TemplateDataAttr> data) {
        this.data = data;
    }


    static public class TemplateDataAttr implements java.io.Serializable{

        private String dataValue;

        private String dataColor = "#173177";

        public TemplateDataAttr() {
            super();
        }

        public TemplateDataAttr(String dataValue, String dataColor) {
            super();
            this.dataColor = dataColor;
        }

        public String getDataValue() {
            return dataValue;
        }

        public void setDataValue(String dataValue) {
            this.dataValue = dataValue;
        }

        public String getDataColor() {
            return dataColor;
        }

        public void setDataColor(String dataColor) {
            this.dataColor = dataColor;
        }
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }


}
