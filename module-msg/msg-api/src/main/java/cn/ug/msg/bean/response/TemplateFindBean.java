package cn.ug.msg.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

public class TemplateFindBean extends BaseBean implements Serializable {

    /** 场景名称 **/
    private String name;
    /** 场景名称类型 --字典表定义 **/
    private Integer type;
    /** 场景描述 **/
    private String described;
    /** 站内信消息标题 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 跳转类型 **/
    private Integer skipType;
    /** 是否发送短信  1:是 2:不是 **/
    private Integer isSms;
    /** 是否发送站内信  1:是 2:不是 **/
    private Integer isInterLetter;
    /** 第三方备案号 **/
    private String filingNumber;
    /** 创建用户**/
    private String createUser;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescribed() {
        return described;
    }

    public void setDescribed(String described) {
        this.described = described;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSkipType() {
        return skipType;
    }

    public void setSkipType(Integer skipType) {
        this.skipType = skipType;
    }

    public Integer getIsSms() {
        return isSms;
    }

    public void setIsSms(Integer isSms) {
        this.isSms = isSms;
    }

    public Integer getIsInterLetter() {
        return isInterLetter;
    }

    public void setIsInterLetter(Integer isInterLetter) {
        this.isInterLetter = isInterLetter;
    }

    public String getFilingNumber() {
        return filingNumber;
    }

    public void setFilingNumber(String filingNumber) {
        this.filingNumber = filingNumber;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
