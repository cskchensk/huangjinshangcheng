package cn.ug.msg.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class BaseParamBean extends Page implements Serializable {

    private String memberId;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
