package cn.ug.msg.bean.request;

import cn.ug.bean.base.Page;

import java.io.Serializable;

public class MemberInfoParamBean extends Page implements Serializable {

    /** 关键字 **/
    private String keyword;
    /** 接收人 **/
    private String name;
    /** 起始时间 **/
    private String startTime;
    /** 结束时间 **/
    private String endTime;
    /** 排序字段 */
    private String order 	= "";
    /** 排序方式 desc或asc */
    private String sort		= "";

    //-------------------------内部参数---------------------
    /** 会员Id **/
    private String memberId;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
