package cn.ug.msg.api;

import cn.ug.bean.base.SerializeObject;
import cn.ug.msg.mq.WxMessageParamBean;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RequestMapping("wxNotice")
public interface WxMessageNoticeServiceApi {

    @RequestMapping(value = "send", method = POST)
    SerializeObject send(WxMessageParamBean entity);
}
