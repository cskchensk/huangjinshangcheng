package cn.ug.msg.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;

/**
 * 会员通知消息
 * @auther ywl
 */
public class NoticeMFindBean extends BaseBean  implements Serializable{

    /** 标题 **/
    private String title;
    /** 摘要 **/
    private String remark;
    /** 内容 **/
    private String content;
    /** 署名 **/
    private String author;
    /** 公告 **/
    private String status;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
