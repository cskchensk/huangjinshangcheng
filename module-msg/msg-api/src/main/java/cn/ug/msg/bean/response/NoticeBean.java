package cn.ug.msg.bean.response;

import cn.ug.bean.base.BaseBean;

import java.io.Serializable;
import java.time.LocalDateTime;

public class NoticeBean  extends BaseBean implements Serializable {

    /** 标题 **/
    private String title;
    /** 内容 **/
    private String content;
    /** 摘要 **/
    private String remark;
    /** 发送类型  1:全部 2:指定会员**/
    private Integer sendType;
    /** 发送时间类型 1:立即发送 2:指定时间 **/
    private Integer sendTimeType;
    /** 指定发送时间 **/
    private String sendTimeString;
    /** 发送数量 **/
    private Integer sendNumber;
    /** 署名 **/
    private String author;
    /** 创建用户**/
    private String createUser;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getSendTimeType() {
        return sendTimeType;
    }

    public void setSendTimeType(Integer sendTimeType) {
        this.sendTimeType = sendTimeType;
    }

    public String getSendTimeString() {
        return sendTimeString;
    }

    public void setSendTimeString(String sendTimeString) {
        this.sendTimeString = sendTimeString;
    }

    public Integer getSendNumber() {
        return sendNumber;
    }

    public void setSendNumber(Integer sendNumber) {
        this.sendNumber = sendNumber;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
